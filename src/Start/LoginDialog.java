/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Start;

import Menus.MenuModerne;
import Methods.Manipulation;
import Methods.Theme;
import Methods.Variables;
import java.awt.Color;
import java.awt.event.KeyEvent;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BorderFactory;
import javax.swing.InputMap;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.UIManager;

/**
 *
 * @author josephandyfeidje
 */
public class LoginDialog extends javax.swing.JDialog {
    
    Manipulation Mn= new Manipulation();
    Theme th= new Theme();
    /**
     * Creates new form LoginDialog
     */
    public LoginDialog(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        setUndecorated(true);
        
        initComponents();
        this.setLocationRelativeTo(this);
        this.setTitle("Scolaris: "+Variables.Empresa+" : Login");
         th.Icone_JFrame(this);
//         th.Fond_login_JFrame(this);
//         mesaj.setVisible(false);
        
        jPanel4MouseEntered(null);
        jPanel4MouseExited(null);
        jPanel6MouseEntered(null);
        jPanel6MouseExited(null);
        jPanel2MouseEntered(null);
        jPanel2MouseExited(null);
        jPanel3MouseEntered(null);
        jPanel3MouseExited(null);
    }

       //.................................................................panel Label
    private void setPanLabColor(JPanel panel, JLabel label) {
        try {
            // set color to the panel and label acording to the parameters
//        panel.setBackground(new Color(87,148,210));
//        label.setForeground(new Color(255, 255, 255));
            panel.setBorder(BorderFactory.createLineBorder(new Color(87, 148,210)));
            label.setBorder(BorderFactory.createLineBorder(new Color(0, 0,0)));
//        label.updateUI();
        } catch (NullPointerException e) {
        }
        
    }

    private void resetPanLabColor(JPanel panel, JLabel label) {
        try {
            // set color to the panel and label acording to the parameters
//        panel.setBackground(new Color(255, 255, 255));
//        label.setForeground(new Color(87,148,210));
            panel.setBorder(BorderFactory.createLineBorder(new Color(255, 255, 255)));
            label.setBorder(new org.jdesktop.swingx.border.DropShadowBorder());
        } catch (NullPointerException e) {
        }
        
    }
    //..........................................................................Panel Text
    private void setPanTxtColor(JPanel panel, JTextField label) {
        try {
            // set color to the panel and label acording to the parameters
//        panel.setBackground(new Color(204,204,204));
//        label.setBackground(new Color(204, 204, 204));
            panel.setBorder(BorderFactory.createLineBorder(new Color(87, 148,210)));
            label.setBorder(BorderFactory.createLineBorder(new Color(0, 0,0)));
//        label.updateUI();
        } catch (NullPointerException e) {
        }
        
    }

    private void resetPanTxtColor(JPanel panel, JTextField label) {
        try {
            // set color to the panel and label acording to the parameters
//        panel.setBackground(new Color(255, 255, 255));
//        label.setBackground(new Color(255,255,255)); 
            panel.setBorder(BorderFactory.createLineBorder(new Color(255,255,255)));
//            label.setBorder(BorderFactory.createLineBorder(new Color(255, 255,255)));
            label.setBorder(new org.jdesktop.swingx.border.DropShadowBorder());
        } catch (NullPointerException e) {
        }
       
    }
    //...........................................................................Panel Jpassword
    private void setPanPssTxtColor(JPanel panel, JPasswordField label) {
        try {
           // set color to the panel and label acording to the parameters
//        panel.setBackground(new Color(204,204,204));
//        label.setBackground(new Color(204, 204, 204));
          panel.setBorder(BorderFactory.createLineBorder(new Color(87, 148,210)));  
          label.setBorder(BorderFactory.createLineBorder(new Color(0, 0,0)));
        } catch (NullPointerException e) {
        }
        
    }

    private void resetPanPssTxtColor(JPanel panel, JPasswordField label) {
        try {
          // set color to the panel and label acording to the parameters
//        panel.setBackground(new Color(255, 255, 255));
//        label.setBackground(new Color(255,255,255));  
            panel.setBorder(BorderFactory.createLineBorder(new Color(255, 255, 255)));
//            label.setBorder(BorderFactory.createLineBorder(new Color(255, 255,255)));
            label.setBorder(new org.jdesktop.swingx.border.DropShadowBorder());
        } catch (NullPointerException e) {
        }
        
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel5 = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        usuario = new javax.swing.JTextField();
        jPanel3 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        modpas = new javax.swing.JPasswordField();
        jLabel5 = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jPanel6 = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        getContentPane().setLayout(new javax.swing.BoxLayout(getContentPane(), javax.swing.BoxLayout.LINE_AXIS));

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/foto/bag-binders-blank-159497.jpg"))); // NOI18N

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jPanel2MouseExited(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jPanel2MouseEntered(evt);
            }
        });

        jLabel3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/ButtonFoto/icons8-administrator_male.png"))); // NOI18N

        usuario.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        usuario.setBorder(new org.edisoncor.gui.util.DropShadowBorder());
        usuario.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                usuarioMouseMoved(evt);
            }
        });
        usuario.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                usuarioFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                usuarioFocusLost(evt);
            }
        });
        usuario.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                usuarioMouseClicked(evt);
            }
        });
        usuario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                usuarioActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(usuario, javax.swing.GroupLayout.PREFERRED_SIZE, 271, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(usuario, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));
        jPanel3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jPanel3MouseExited(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jPanel3MouseEntered(evt);
            }
        });

        jLabel4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/ButtonFoto/icons8-password.png"))); // NOI18N

        modpas.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        modpas.setBorder(new org.edisoncor.gui.util.DropShadowBorder());
        modpas.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                modpasMouseMoved(evt);
            }
        });
        modpas.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                modpasFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                modpasFocusLost(evt);
            }
        });
        modpas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                modpasActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(modpas, javax.swing.GroupLayout.DEFAULT_SIZE, 271, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, 33, Short.MAX_VALUE)
                    .addComponent(modpas))
                .addContainerGap())
        );

        jLabel5.setFont(new java.awt.Font("Chalkduster", 3, 48)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(87, 148, 210));
        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel5.setText("Scolaris 1.0");

        jPanel4.setBackground(new java.awt.Color(87, 148, 210));
        jPanel4.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jPanel4MouseClicked(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jPanel4MouseExited(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jPanel4MouseEntered(evt);
            }
        });
        jPanel4.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jPanel4KeyPressed(evt);
            }
        });
        jPanel4.setLayout(new java.awt.GridLayout(1, 0));

        jLabel2.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel2.setText("Connectez");
        jLabel2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel2MouseClicked(evt);
            }
        });
        jPanel4.add(jLabel2);

        jPanel6.setBackground(new java.awt.Color(87, 148, 210));
        jPanel6.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jPanel6MouseClicked(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jPanel6MouseExited(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jPanel6MouseEntered(evt);
            }
        });
        jPanel6.setLayout(new java.awt.GridLayout(1, 0));

        jLabel6.setFont(new java.awt.Font("Lucida Grande", 1, 24)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(255, 255, 255));
        jLabel6.setText("X");
        jLabel6.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        jPanel6.add(jLabel6);

        jLabel7.setText("  Copyright (c) 2018 Softmatech. All rights reserved.");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 491, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addContainerGap())
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                .addGap(0, 66, Short.MAX_VALUE)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jPanel2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(jPanel3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGap(52, 52, 52))
                                    .addComponent(jPanel6, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                        .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, 129, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(131, 131, 131))))))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel7)
                        .addContainerGap())))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jLabel1)
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(33, 33, 33)
                .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(18, 18, 18)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(63, 63, 63)
                .addComponent(jLabel7)
                .addContainerGap())
        );

        getContentPane().add(jPanel1);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void usuarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_usuarioActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_usuarioActionPerformed

    private void jPanel4MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel4MouseEntered
        setPanLabColor(jPanel4, jLabel2);
    }//GEN-LAST:event_jPanel4MouseEntered

    private void jPanel4MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel4MouseExited
        resetPanLabColor(jPanel4, jLabel2);
    }//GEN-LAST:event_jPanel4MouseExited

    private void jPanel6MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel6MouseEntered
        setPanLabColor(jPanel6, jLabel6);
    }//GEN-LAST:event_jPanel6MouseEntered

    private void jPanel6MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel6MouseExited
        resetPanLabColor(jPanel6, jLabel6);
    }//GEN-LAST:event_jPanel6MouseExited

    private void jPanel2MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel2MouseEntered
        setPanTxtColor(jPanel2,usuario);
    }//GEN-LAST:event_jPanel2MouseEntered

    private void jPanel2MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel2MouseExited
        resetPanTxtColor(jPanel2, usuario);
    }//GEN-LAST:event_jPanel2MouseExited

    private void usuarioMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_usuarioMouseClicked
    }//GEN-LAST:event_usuarioMouseClicked

    private void usuarioMouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_usuarioMouseMoved
        setPanTxtColor(jPanel2, usuario);
    }//GEN-LAST:event_usuarioMouseMoved

    private void jPanel3MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel3MouseEntered
        setPanPssTxtColor(jPanel3, modpas);
    }//GEN-LAST:event_jPanel3MouseEntered

    private void jPanel3MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel3MouseExited
        resetPanPssTxtColor(jPanel3, modpas);
    }//GEN-LAST:event_jPanel3MouseExited

    private void usuarioFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_usuarioFocusGained
        setPanTxtColor(jPanel2, usuario);
    }//GEN-LAST:event_usuarioFocusGained

    private void modpasFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_modpasFocusGained
        setPanPssTxtColor(jPanel3, modpas);
    }//GEN-LAST:event_modpasFocusGained

    private void modpasMouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_modpasMouseMoved
        setPanPssTxtColor(jPanel3, modpas);
    }//GEN-LAST:event_modpasMouseMoved

    private void jPanel6MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel6MouseClicked
        int rep = JOptionPane.showConfirmDialog(null, "Voulez-vous Sortir du Système", "Avertissement",JOptionPane.YES_NO_OPTION);
                    if(rep == JOptionPane.YES_OPTION){
                        System.exit(0);
                    }
    }//GEN-LAST:event_jPanel6MouseClicked

    private void usuarioFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_usuarioFocusLost
        resetPanTxtColor(jPanel2, usuario);
        
        Mn.VALIDATUSER(usuario.getText(),usuario);
    }//GEN-LAST:event_usuarioFocusLost

    private void modpasFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_modpasFocusLost
        resetPanPssTxtColor(jPanel3, modpas);
    }//GEN-LAST:event_modpasFocusLost

    private void jLabel2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel2MouseClicked
        
    }//GEN-LAST:event_jLabel2MouseClicked

    private void jPanel4KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jPanel4KeyPressed
        InputMap map = new InputMap();
    map.put(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0, false), "pressed");
    map.put(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0, true), "released");
    jPanel4.setInputMap(0, map);
    }//GEN-LAST:event_jPanel4KeyPressed

    private void modpasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_modpasActionPerformed
        Mn.VALIDATUSER(usuario.getText(),usuario);
        jPanel4MouseClicked(null);
    }//GEN-LAST:event_modpasActionPerformed

    private void jPanel4MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel4MouseClicked
        if(usuario.getText().trim().isEmpty()|| modpas.getText().trim().isEmpty()){
//            mesaj.setVisible(true);
            JOptionPane.showMessageDialog(rootPane, "Je vous prie de bien vouloir entrer votre nom d'utilisateur et le code de sécurité.");
//            mesaj.setText("Je vous prie de bien vouloir entrer votre nom d'utilisateur et le code de sécurité.");
            modpas.setText(null);
            usuario.setText(null);
            usuario.requestFocus();
        }else if(usuario.getText().trim().equals("admin@scolaris.edu")&& modpas.getText().trim().equals("admin")){
//            mesaj.setVisible(false);
            Variables.Utilisateurs="Softmatech";
            Variables.Unlock="admin";
            Variables.Acces="Haut";
//            String busness=institution.getSelectedItem().toString();
//            if(busness.equals("Selectionnez")){
//              Variables.Empresa="Défaut";  
//            }else{
// //             Variables.Empresa=institution.getSelectedItem().toString();
//            }
            this.dispose();
            Waitt attend = new Waitt(null, rootPaneCheckingEnabled);
            attend.show();
            
            MenuModerne mm = new MenuModerne();
            mm.show();
        }else{
        String query="select t.nom,t.prenom,u.nom_utilisateur as utilisateur,AES_DECRYPT(u.securite,'llave') as securite,u.niveau_acces as\n" +
        " acces from utilisateur u left join employer e on e.matricule=u.matricule\n" +
        "left join tercero t on t.code_tercero=e.code_tercero where u.nom_utilisateur='"+usuario.getText()+"'"
                        + " and u.estatuses='Actif' and u.securite='"+modpas.getText()+"' ";
//                + " and t.institution='"+institution.getSelectedItem().toString()+"'";
            try {
                ResultSet rs= Mn.SEARCHDATA(query);
                if(rs.first()){
                if(usuario.getText().trim().equals(rs.getString("utilisateur")) &&
                        modpas.getText().trim().equals(rs.getString("securite"))){
//                    mesaj.setVisible(false);
                    Variables.Utilisateurs=rs.getString("nom")+" "+rs.getString("prenom");
                    Variables.Acces=rs.getString("acces");
                    Variables.Unlock=rs.getString("securite");
//                    String busness=institution.getSelectedItem().toString();
//                    if(busness.equals("Selectionnez")){
//                     Variables.Empresa="Défaut";  
//                    }else{
//                      Variables.Empresa=rs.getString("institution");
//                    }
                    //Login.institution.setEnabled(false);
                    this.dispose();
                    Waitt attend= new Waitt(null, rootPaneCheckingEnabled);
                    attend.show();
                    MenuModerne mm = new MenuModerne();
                    mm.show();
                }else{
//                  mesaj.setVisible(true);
                    JOptionPane.showMessageDialog(rootPane, "Je vous prie de bien vouloir entrer votre nom d'utilisateur et le code de sécurité.");
//                  mesaj.setText("Je vous prie de bien vouloir entrer votre nom d'utilisateur et le code de sécurité.");
                  modpas.setText(null);
                  usuario.setText(null);
                  usuario.requestFocus();
                }    
                }else{
//                  mesaj.setVisible(true);
                    JOptionPane.showMessageDialog(rootPane, "Le nom d'utilisateur n'existe pas dans le système ou cet utilisateur est Inactif.");
//                  mesaj.setText("Le nom d'utilisateur n'existe pas dans le système ou cet utilisateur est Inactif.");
                  modpas.setText(null);
                  usuario.setText(null);
                  usuario.requestFocus();
                }
            } catch (SQLException ex) {
                Logger.getLogger(LoginDialog.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }//GEN-LAST:event_jPanel4MouseClicked

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
         for (UIManager.LookAndFeelInfo laf : UIManager
                .getInstalledLookAndFeels()) {
            if ("Nimbus".equals(laf.getName())) {
                UIManager.setLookAndFeel(laf.getClassName());
//                UIManager.getLookAndFeelDefaults().put("DesktopPane[Enabled].backgroundPainter", new DesktopPainter());
//                UIManager.getLookAndFeelDefaults().put("Button.background",new Color(0, 153, 153));
                //UIManager.getLookAndFeelDefaults().put("OptionPane.background", new Color(0, 102, 153));
                UIManager.getLookAndFeelDefaults().put("Button[Focused+MouseOver].backgroundPainter", new Color(0, 107, 163));
                UIManager.getLookAndFeelDefaults().put("Table.alternateRowColor", new Color(87,148,210));
                UIManager.getLookAndFeelDefaults().put("Table[Enabled+Selected].textBackground", Color.black);
                //UIManager.getLookAndFeelDefaults().put("Panel.background", Color.lightGray);
            }
        }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(LoginDialog.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(LoginDialog.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(LoginDialog.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(LoginDialog.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                LoginDialog dialog = new LoginDialog(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPasswordField modpas;
    private javax.swing.JTextField usuario;
    // End of variables declaration//GEN-END:variables
}
