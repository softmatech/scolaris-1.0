
package Start;

import Menus.Menus;
import Methods.Manipulation;
import Methods.Theme;
import Methods.Variables;
import java.awt.event.KeyEvent;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.InputMap;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;

public class Login extends javax.swing.JFrame {
Manipulation Mn= new Manipulation();
Theme th= new Theme();
    public Login() {
        initComponents();
        this.setTitle("Scolaris: "+Variables.Empresa+" : Login");
        this.setLocationRelativeTo(null);
         th.Icone_JFrame(this);
         th.Fond_login_JFrame(this);
         mesaj.setVisible(false);
        // Mn.FULLCOMBO(institution,"nom","institution");
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jSeparator1 = new javax.swing.JSeparator();
        mesaj = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        entrer = new javax.swing.JButton();
        annuler = new javax.swing.JButton();
        jPanel4 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        usuario = new javax.swing.JFormattedTextField();
        jLabel6 = new javax.swing.JLabel();
        modpas = new org.edisoncor.gui.passwordField.PasswordFieldRectBackground();
        jSeparator2 = new javax.swing.JSeparator();
        jSeparator3 = new javax.swing.JSeparator();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setUndecorated(true);
        setResizable(false);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        mesaj.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        mesaj.setForeground(new java.awt.Color(255, 0, 0));
        mesaj.setText("jLabel3");
        getContentPane().add(mesaj, new org.netbeans.lib.awtextra.AbsoluteConstraints(12, 261, 484, -1));

        jPanel1.setBackground(new java.awt.Color(102, 102, 102));

        jLabel3.setBackground(new java.awt.Color(255, 255, 255));
        jLabel3.setFont(new java.awt.Font("Segoe Script", 1, 18)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setText("SCOLARIS");

        jPanel3.setBorder(new org.jdesktop.swingx.border.DropShadowBorder());

        jLabel5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/icons8-graduation_cap_1.png"))); // NOI18N

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel5, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 100, Short.MAX_VALUE)
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel5, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 100, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel3)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(34, 34, 34)
                        .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        getContentPane().add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 510, -1));

        jPanel2.setBackground(new java.awt.Color(102, 102, 102));

        entrer.setBackground(new java.awt.Color(255, 255, 255));
        entrer.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        entrer.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/ENTRER.png"))); // NOI18N
        entrer.setText("Entrer");
        entrer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                entrerActionPerformed(evt);
            }
        });
        entrer.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                entrerKeyPressed(evt);
            }
        });

        annuler.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        annuler.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/Cerrar.png"))); // NOI18N
        annuler.setText("Fermer");
        annuler.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                annulerActionPerformed(evt);
            }
        });
        annuler.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                annulerKeyPressed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(101, 101, 101)
                .addComponent(entrer, javax.swing.GroupLayout.PREFERRED_SIZE, 123, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(43, 43, 43)
                .addComponent(annuler, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(116, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(annuler, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(entrer, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        getContentPane().add(jPanel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 287, -1, -1));

        jPanel4.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/icons8-checked_user_male.png"))); // NOI18N

        usuario.setBackground(new java.awt.Color(238, 238, 238));
        usuario.setHorizontalAlignment(javax.swing.JTextField.LEFT);
        usuario.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        usuario.setMinimumSize(new java.awt.Dimension(6, 28));
        usuario.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                usuarioFocusLost(evt);
            }
        });
        usuario.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                usuarioKeyTyped(evt);
            }
        });

        jLabel6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/icons8-forgot_password_1.png"))); // NOI18N

        modpas.setBackground(new java.awt.Color(238, 238, 238));
        modpas.setDescripcion("Entrer le code de sécurité ici");
        modpas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                modpasActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 396, Short.MAX_VALUE)
            .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel4Layout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel4Layout.createSequentialGroup()
                            .addComponent(jLabel4)
                            .addGap(2, 2, 2)
                            .addComponent(usuario, javax.swing.GroupLayout.PREFERRED_SIZE, 352, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 386, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(jPanel4Layout.createSequentialGroup()
                            .addComponent(jLabel6)
                            .addGap(1, 1, 1)
                            .addComponent(modpas, javax.swing.GroupLayout.PREFERRED_SIZE, 353, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addComponent(jSeparator3, javax.swing.GroupLayout.PREFERRED_SIZE, 386, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGap(0, 0, Short.MAX_VALUE)))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 108, Short.MAX_VALUE)
            .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel4Layout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(usuario, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGap(6, 6, 6)
                    .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(6, 6, 6)
                    .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(modpas, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGap(6, 6, 6)
                    .addComponent(jSeparator3, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, Short.MAX_VALUE)))
        );

        getContentPane().add(jPanel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 140, 400, 110));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void entrerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_entrerActionPerformed
        if(usuario.getText().trim().isEmpty()|| modpas.getText().trim().isEmpty()){
            mesaj.setVisible(true);
            mesaj.setText("Je vous prie de bien vouloir entrer votre nom d'utilisateur et le code de sécurité.");
            modpas.setText(null);
            usuario.setText(null);
            usuario.requestFocus();
        }else if(usuario.getText().trim().equals("admin@scolaris.edu")&& modpas.getText().trim().equals("admin")){
            mesaj.setVisible(false);
            Variables.Utilisateurs="Groupe Technicien Scolaris Pour la Maintenance";
            Variables.Unlock="admin";
            Variables.Acces="Haut";
//            String busness=institution.getSelectedItem().toString();
//            if(busness.equals("Selectionnez")){
//              Variables.Empresa="Défaut";  
//            }else{
// //             Variables.Empresa=institution.getSelectedItem().toString();
//            }
            this.dispose();
            Waitt attend= new Waitt(this, rootPaneCheckingEnabled);
            attend.show();
            Menus mm= new Menus();
            mm.show();
        }else{
        String query="select t.nom,t.prenom,u.nom_utilisateur as utilisateur,AES_DECRYPT(u.securite,'llave') as securite,u.niveau_acces as\n" +
        " acces from utilisateur u left join employer e on e.matricule=u.matricule\n" +
        "left join tercero t on t.code_tercero=e.code_tercero where u.nom_utilisateur='"+usuario.getText()+"'"
                        + " and u.estatuses='Actif' and u.securite='"+modpas.getText()+"' ";
//                + " and t.institution='"+institution.getSelectedItem().toString()+"'";
            try {
                ResultSet rs= Mn.SEARCHDATA(query);
                if(rs.first()){
                if(usuario.getText().trim().equals(rs.getString("utilisateur")) &&
                        modpas.getText().trim().equals(rs.getString("securite"))){
                    mesaj.setVisible(false);
                    Variables.Utilisateurs=rs.getString("nom")+" "+rs.getString("prenom");
                    Variables.Acces=rs.getString("acces");
                    Variables.Unlock=rs.getString("securite");
//                    String busness=institution.getSelectedItem().toString();
//                    if(busness.equals("Selectionnez")){
//                     Variables.Empresa="Défaut";  
//                    }else{
//                      Variables.Empresa=rs.getString("institution");
//                    }
                    //Login.institution.setEnabled(false);
                    this.dispose();
                    Waitt attend= new Waitt(this, rootPaneCheckingEnabled);
                    attend.show();
                    Menus mm= new Menus();
                    mm.show();
                }else{
                  mesaj.setVisible(true);
                  mesaj.setText("Je vous prie de bien vouloir entrer votre nom d'utilisateur et le code de sécurité.");
                  modpas.setText(null);
                  usuario.setText(null);
                  usuario.requestFocus();
                }    
                }else{
                  mesaj.setVisible(true);
                  mesaj.setText("Le nom d'utilisateur n'existe pas dans le système ou cet utilisateur est Inactif.");
                  modpas.setText(null);
                  usuario.setText(null);
                  usuario.requestFocus();
                }
            } catch (SQLException ex) {
                Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }//GEN-LAST:event_entrerActionPerformed

    private void annulerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_annulerActionPerformed
       int rep = JOptionPane.showConfirmDialog(rootPane, "êtres-vous sure de vouloir quitter le systeme", "Avertissement", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
       if(rep == JOptionPane.YES_OPTION){
           System.exit(0);
       }
    }//GEN-LAST:event_annulerActionPerformed

    private void usuarioKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_usuarioKeyTyped

    }//GEN-LAST:event_usuarioKeyTyped

    private void entrerKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_entrerKeyPressed
    InputMap map = new InputMap();
    map.put(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0, false), "pressed");
    map.put(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0, true), "released");
    entrer.setInputMap(0, map);
    }//GEN-LAST:event_entrerKeyPressed

    private void annulerKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_annulerKeyPressed
    InputMap map = new InputMap();
    map.put(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0, false), "pressed");
    map.put(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0, true), "released");
    annuler.setInputMap(0, map);
    }//GEN-LAST:event_annulerKeyPressed

    private void usuarioFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_usuarioFocusLost
       Mn.VALIDATUSER(usuario.getText(),usuario);
    }//GEN-LAST:event_usuarioFocusLost

    private void modpasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_modpasActionPerformed
        entrer.doClick();
    }//GEN-LAST:event_modpasActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Login().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton annuler;
    private javax.swing.JButton entrer;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JLabel mesaj;
    private org.edisoncor.gui.passwordField.PasswordFieldRectBackground modpas;
    private javax.swing.JFormattedTextField usuario;
    // End of variables declaration//GEN-END:variables
}
