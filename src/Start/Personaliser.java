package Start;

import Menus.Menus;
import Methods.Connections;
import Methods.Manipulation;
import Methods.Theme;
import Methods.Variables;
import Processus.Entrer_note;
import static Start.Personaliser.point;
import java.awt.Image;
import java.awt.Point;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Blob;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;

public class Personaliser extends javax.swing.JFrame {
    
Connections db= new Connections().Conectar();
Manipulation Mn= new Manipulation();
Theme th= new Theme();
String[][] data={};

String[] head={" Section"," Equivalence des Matières"};
String[] heads={"Section","Montant"," ","Versement"};
String[] header={"Sigles","Institutions"};

DefaultTableModel md= new DefaultTableModel(data,head);
DefaultTableModel mds= new DefaultTableModel(data,heads);
DefaultTableModel md1= new DefaultTableModel(data,head);
DefaultTableModel mds1= new DefaultTableModel(data,heads);
DefaultTableModel mder= new DefaultTableModel(data,header);

JTextField code= new JTextField();
JTextField route= new JTextField();
File fichier;
String valordeviz="";

    public Personaliser() {
      initComponents();
      this.setTitle("Scolaris: "+Variables.Empresa+" : Paramètre de Personalisation");
      this.setLocationRelativeTo(null);
      th.Icone_JFrame(this);
      Mn.PHONE_FROMAT(telephone);
      tblequivalence.setModel(md);
      tblcollection.setModel(mds);
      tblequivalence1.setModel(md1);
      tblcollection1.setModel(mds1);
      tblconsulta.setModel(mder);
      Nettoyer();
      SelectInstitut();
      Scolaire();
         //----------------------------------------------------------------
     TableColumnModel cmy= tblcollection.getColumnModel();
     TableColumnModel cm= tblconsulta.getColumnModel();
        cmy.getColumn(0).setPreferredWidth(50);
        cmy.getColumn(1).setPreferredWidth(20);
        cmy.getColumn(2).setPreferredWidth(15);
        cmy.getColumn(3).setPreferredWidth(30);
        cm.getColumn(0).setPreferredWidth(30);
        cm.getColumn(1).setPreferredWidth(500);  
    }
    
 
  private void Nettoyer(){
  JTextField[] txt={code,point};
  Mn.NEW_BUTTON(txt, "equivalence","code_equivalence");
  point.setText(null);
  md.setRowCount(0);
  nom.setText(null);
  mail.setText(null);
  adresse.setText(null);
  abrege.setText(null);
  deviz.setText(null);
  foto.setIcon(null);
  cinq.setSelected(false);
  pluscinq.setSelected(false);
  six.setSelected(false);
  plussix.setSelected(false);
  supsix.setSelected(false);
  recherche.setText(null);
  adresse1.setEditable(false);
  deviz1.setEditable(false);
  mail1.setEditable(false);
  telephone1.setEditable(false);
  telephone.setValue(null);
  Mn.PHONE_FROMAT(telephone);
  Calendar cal= Calendar.getInstance();
  int year= cal.get(Calendar.YEAR)-1; 
  annee.setValue(year);
}
    
private void RemplirEqui(){
String query="select section,valeur from equivalence where institution='"+tblconsulta.getValueAt(tblconsulta.getSelectedRow(), 1)+"' order by section asc";
Mn.FULLTABLE(query, tblequivalence1, md1);
}

private void Select(){
    String aux="";
 String querys="select moyenne_aceptable from qualification where institution='"+tblconsulta.getValueAt(tblconsulta.getSelectedRow(), 1)+"'";
        ResultSet rs=Mn.SEARCHDATA(querys);
        try {
            if(rs.first()){
                accepte.setText(rs.getString("moyenne_aceptable"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(Personaliser.class.getName()).log(Level.SEVERE, null, ex);
        }     
}

public void InsertSelect(){
   String aux="";
    if(cinq.isSelected()){
        aux="5.00";
        }else if(pluscinq.isSelected()){
        aux="5.50";
        }else if(six.isSelected()){
        aux="6.00";
        }else if(plussix.isSelected()){
        aux="6.50";
        }else if(supsix.isSelected()){
        aux=">6.50";
        } 
    String query="call Insert_qualification('"+aux+"','"+nom.getText()+"')";
    Mn.MANIPULEDB(query);
}


private void SelectInstitut() {
 String querys="select abrege,nom from institution";
 Mn.FULLTABLE(querys, tblconsulta, mder);     
}

private void SelectInstituts() {
 String querys="select * from institution where abrege='"+tblconsulta.getValueAt(tblconsulta.getSelectedRow(), 0)+"'"
         + " and nom='"+tblconsulta.getValueAt(tblconsulta.getSelectedRow(), 1)+"' ";
        ResultSet rs=Mn.SEARCHDATA(querys);
        try {
            if(rs.first()){
             adresse1.setText(rs.getString("direccion"));
             telephone1.setText(rs.getString("telephone"));
             mail1.setText(rs.getString("e_mail"));
             deviz1.setText(rs.getString("deviz"));
             Blob bl=(Blob)rs.getBlob("logo");
             byte[] data=bl.getBytes(1,(int)bl.length());
                try {
                    Image img=ImageIO.read(new ByteArrayInputStream(data)).getScaledInstance(
                            foto1.getWidth(),foto1.getHeight(),Image.SCALE_DEFAULT);
                    foto1.setIcon(new ImageIcon(img));
                    foto1.updateUI();
                } catch (IOException ex) {
                    Logger.getLogger(Personaliser.class.getName()).log(Level.SEVERE, null, ex);
                }
        } 
        }catch (SQLException ex) {
            Logger.getLogger(Personaliser.class.getName()).log(Level.SEVERE, null, ex);
        }     
}





private void Scolaire(){
    String query="select annee from annee_academique where code_annee\n" +
     "in (select max(code_annee) from annee_academique)";
    ResultSet rs=Mn.SEARCHDATA(query);
    try {
        if(rs.first()){
         anneee.setText(rs.getString("annee"));
        }
    } catch (SQLException ex) {
        Logger.getLogger(Entrer_note.class.getName()).log(Level.SEVERE, null, ex);
    }
    }

private void RemplirCollection(){
String query="select * from collection where institution='"+tblconsulta.getValueAt(tblconsulta.getSelectedRow(), 1)+"' order by section asc";
Mn.FULLTABLE(query, tblcollection, mds1);
}

private void Executer(){
    
        int aaq=Mn.MAXCODE("code_annee","annee_academique");
        String bbq=String.valueOf(annee.getValue())+"-"+String.valueOf(annee.getValue()+1);
        String query="call Insert_academique("+aaq+",'"+bbq+"')";
        Mn.MANIPULEDB(query);  
        int oo = 0;
        String aa=nom.getText();
        String bb=adresse.getText();
        String cc=telephone.getText();
        String dd=mail.getText();
        String ee=abrege.getText();
        String ff=valordeviz;
        FileInputStream fichierfoto=null;
        File nomfoto;
        nomfoto = new File(route.getText());
        try {
            fichierfoto=new FileInputStream(fichier);
           query="call Insert_institut(?,?,?,?,?,?,?,?)";
            System.out.println("vb "+query);
            try {
                PreparedStatement statement= db.getConexion().prepareStatement(query);
                statement.setInt(1,oo);
                statement.setString(2,aa);
                statement.setString(3,bb);
                statement.setString(4,cc);
                statement.setString(5,dd);
                statement.setString(6,ee);
                statement.setBlob(7, fichierfoto);
                statement.setString(8, ff);
                statement.executeUpdate();
            } catch (SQLException ex) {
                Logger.getLogger(Personaliser.class.getName()).log(Level.SEVERE, null, ex);
            }

        } catch (FileNotFoundException | NullPointerException ex) {
            Logger.getLogger(Personaliser.class.getName()).log(Level.SEVERE, null, ex);
        }
        SelectInstitut();
        String querys="select nom from institution";
        ResultSet rs=Mn.SEARCHDATA(querys);
        try {
            if(rs.first()){
                Variables.Empresa=rs.getString("nom");
            }
        } catch (SQLException ex) {
            Logger.getLogger(Personaliser.class.getName()).log(Level.SEVERE, null, ex);
        }
        for(int i=0;i<tblequivalence.getRowCount();i++){
        int aaw=Mn.MAXCODE("code_equivalence","equivalence");
        String bbw=tblequivalence.getValueAt(i, 0).toString();
        String bbww=tblequivalence.getValueAt(i, 1).toString();
        query="call Insert_equivalence("+aaw+",'"+bbw+"','"+bbww+"','"+nom.getText()+"')";
        Mn.MANIPULEDB(query);
        }
        InsertSelect(); 
        Nettoyer();
    
}  
    
    
    
    
    
    
    
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel1 = new javax.swing.JPanel();
        jPanel5 = new javax.swing.JPanel();
        foto = new javax.swing.JLabel();
        subir = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        nom = new org.edisoncor.gui.textField.TextFieldRectBackground();
        jScrollPane3 = new javax.swing.JScrollPane();
        deviz = new javax.swing.JTextArea();
        jLabel11 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        telephone = new javax.swing.JFormattedTextField();
        jLabel10 = new javax.swing.JLabel();
        abrege = new org.edisoncor.gui.textField.TextFieldRectBackground();
        mail = new org.edisoncor.gui.textField.TextFieldRectBackground();
        jLabel4 = new javax.swing.JLabel();
        adresse = new org.edisoncor.gui.textField.TextFieldRectBackground();
        jLabel2 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jPanel6 = new javax.swing.JPanel();
        annee = new com.toedter.calendar.JYearChooser();
        academique = new com.toedter.calendar.JYearChooser();
        actualiser = new javax.swing.JButton();
        jPanel7 = new javax.swing.JPanel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        montant = new org.edisoncor.gui.textField.TextFieldRectBackground();
        devise = new javax.swing.JLabel();
        versement = new org.edisoncor.gui.textField.TextFieldRectBackground();
        jScrollPane4 = new javax.swing.JScrollPane();
        tblcollection = new javax.swing.JTable(){
            public boolean isCellEditable(int rowIndex, int colIndex){
                return false;
            }
        };
        add = new javax.swing.JButton();
        rem = new javax.swing.JButton();
        section = new javax.swing.JComboBox();
        jPanel8 = new javax.swing.JPanel();
        cinq = new javax.swing.JCheckBox();
        pluscinq = new javax.swing.JCheckBox();
        six = new javax.swing.JCheckBox();
        plussix = new javax.swing.JCheckBox();
        supsix = new javax.swing.JCheckBox();
        jPanel9 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tblequivalence = new javax.swing.JTable(){
            public boolean isCellEditable(int rowIndex, int colIndex){
                return false;
            }
        };
        jLabel6 = new javax.swing.JLabel();
        point = new org.edisoncor.gui.textField.TextFieldRectBackground();
        add1 = new javax.swing.JButton();
        rem1 = new javax.swing.JButton();
        jLabel13 = new javax.swing.JLabel();
        section1 = new javax.swing.JComboBox();
        jPanel4 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblconsulta = new javax.swing.JTable(){
            public boolean isCellEditable(int rowIndex, int colIndex){
                return false;
            }
        };
        jLabel5 = new javax.swing.JLabel();
        recherche = new org.edisoncor.gui.textField.TextFieldRectBackground();
        foto1 = new javax.swing.JLabel();
        adresse1 = new org.edisoncor.gui.textField.TextFieldRectBackground();
        mail1 = new org.edisoncor.gui.textField.TextFieldRectBackground();
        jScrollPane5 = new javax.swing.JScrollPane();
        deviz1 = new javax.swing.JTextArea();
        telephone1 = new javax.swing.JFormattedTextField();
        jScrollPane6 = new javax.swing.JScrollPane();
        tblcollection1 = new javax.swing.JTable(){
            public boolean isCellEditable(int rowIndex, int colIndex){
                return false;
            }
        };
        jSeparator1 = new javax.swing.JSeparator();
        jScrollPane7 = new javax.swing.JScrollPane();
        tblequivalence1 = new javax.swing.JTable(){
            public boolean isCellEditable(int rowIndex, int colIndex){
                return false;
            }
        };
        jPanel10 = new javax.swing.JPanel();
        accepte = new javax.swing.JLabel();
        jPanel11 = new javax.swing.JPanel();
        anneee = new javax.swing.JLabel();
        actauliser = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        effacer = new javax.swing.JButton();
        accepter1 = new javax.swing.JButton();
        annuler = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jTabbedPane1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jTabbedPane1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N

        jPanel5.setBorder(javax.swing.BorderFactory.createTitledBorder("Information de l'institution"));

        foto.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        foto.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        foto.setText("PHOTO");
        foto.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        subir.setBackground(new java.awt.Color(255, 255, 255));
        subir.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        subir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/picture.jpg"))); // NOI18N
        subir.setText("Charger le Logo");
        subir.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        subir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                subirActionPerformed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel1.setText("Institution");

        nom.setDescripcion("Entrer le nom ici");

        deviz.setColumns(2);
        deviz.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        deviz.setRows(3);
        deviz.setName(""); // NOI18N
        deviz.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                devizFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                devizFocusLost(evt);
            }
        });
        jScrollPane3.setViewportView(deviz);

        jLabel11.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel11.setText("Devise");

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel3.setText("Téléphone");

        telephone.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        telephone.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N

        jLabel10.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel10.setText("Abrege");

        abrege.setDescripcion("Entrer le nom ici");
        abrege.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                abregeKeyReleased(evt);
            }
        });

        mail.setDescripcion("Entrer le courriel  ici");
        mail.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                mailFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                mailFocusLost(evt);
            }
        });

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel4.setText("E-mail");

        adresse.setDescripcion("Entrer l'adresse ici");
        adresse.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                adresseFocusGained(evt);
            }
        });

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel2.setText("Adresse");

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel2, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel4, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel11, javax.swing.GroupLayout.Alignment.TRAILING))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(nom, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(adresse, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(mail, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jScrollPane3)))
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGap(24, 24, 24)
                        .addComponent(jLabel10)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(abrege, javax.swing.GroupLayout.PREFERRED_SIZE, 153, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(34, 34, 34)
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel5Layout.createSequentialGroup()
                                .addComponent(foto, javax.swing.GroupLayout.PREFERRED_SIZE, 203, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(subir, javax.swing.GroupLayout.PREFERRED_SIZE, 179, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 201, Short.MAX_VALUE))
                            .addComponent(telephone))))
                .addGap(20, 20, 20))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(nom, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2)
                    .addComponent(adresse, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(13, 13, 13)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(mail, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4))
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGap(26, 26, 26)
                        .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(abrege, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel10)
                    .addComponent(jLabel3)
                    .addComponent(telephone, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addComponent(foto, javax.swing.GroupLayout.PREFERRED_SIZE, 166, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGap(73, 73, 73)
                        .addComponent(subir, javax.swing.GroupLayout.PREFERRED_SIZE, 56, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(16, 16, 16))
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel5, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        jTabbedPane1.addTab("Institution", jPanel1);

        jPanel6.setBorder(javax.swing.BorderFactory.createTitledBorder("Année Académique"));

        actualiser.setBackground(new java.awt.Color(255, 255, 255));
        actualiser.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        actualiser.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/icons8-restart.png"))); // NOI18N
        actualiser.setText("Actualiser");
        actualiser.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                actualiserActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(annee, javax.swing.GroupLayout.PREFERRED_SIZE, 144, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(academique, javax.swing.GroupLayout.DEFAULT_SIZE, 126, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(actualiser)
                .addContainerGap())
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(academique, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(annee, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(actualiser, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        jPanel7.setBorder(javax.swing.BorderFactory.createTitledBorder("Collection"));

        jLabel8.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel8.setText("Section:");

        jLabel9.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel9.setText("Motant: ");

        jLabel12.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel12.setText("Versement: ");

        devise.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        devise.setText("Gdes");

        tblcollection.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane4.setViewportView(tblcollection);

        add.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/icons8-circled_down.png"))); // NOI18N
        add.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addActionPerformed(evt);
            }
        });

        rem.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/icons8-up_circular_1.png"))); // NOI18N
        rem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                remActionPerformed(evt);
            }
        });

        section.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        section.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Selectionnez", "Petit", "Primaire", "Secondaire" }));

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addComponent(jLabel8)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(section, javax.swing.GroupLayout.PREFERRED_SIZE, 153, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel9)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(montant, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(devise)
                        .addContainerGap(30, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel7Layout.createSequentialGroup()
                        .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(jPanel7Layout.createSequentialGroup()
                                .addComponent(jLabel12)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(versement, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(rem, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(add, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(5, 5, 5))))
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel9)
                        .addComponent(montant, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(devise))
                    .addComponent(section))
                .addGap(18, 18, 18)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel12)
                            .addComponent(versement, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 205, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addComponent(add, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(rem, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE))))
        );

        jPanel8.setBorder(javax.swing.BorderFactory.createTitledBorder("Moyenne Générale Requise"));

        cinq.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        cinq.setText("5.00");
        cinq.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                cinqMouseClicked(evt);
            }
        });

        pluscinq.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        pluscinq.setText("5.50");
        pluscinq.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                pluscinqMouseClicked(evt);
            }
        });

        six.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        six.setText("6.00");
        six.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                sixMouseClicked(evt);
            }
        });

        plussix.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        plussix.setText("6.50");
        plussix.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                plussixMouseClicked(evt);
            }
        });

        supsix.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        supsix.setText(" Superieur que  6.50");
        supsix.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                supsixActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addGap(79, 79, 79)
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel8Layout.createSequentialGroup()
                        .addComponent(supsix)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel8Layout.createSequentialGroup()
                        .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(cinq)
                            .addComponent(six))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel8Layout.createSequentialGroup()
                                .addGap(1, 1, 1)
                                .addComponent(pluscinq))
                            .addComponent(plussix))
                        .addGap(70, 70, 70))))
        );
        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cinq)
                    .addComponent(pluscinq))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(six)
                    .addComponent(plussix))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(supsix))
        );

        jPanel9.setBorder(javax.swing.BorderFactory.createTitledBorder("Equivalence"));

        tblequivalence.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tblequivalence.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                tblequivalenceMousePressed(evt);
            }
        });
        jScrollPane2.setViewportView(tblequivalence);

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel6.setText("Valeur");

        point.setDescripcion("Entrer le point ici");
        point.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                pointFocusGained(evt);
            }
        });

        add1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/icons8-circled_down.png"))); // NOI18N
        add1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                add1ActionPerformed(evt);
            }
        });

        rem1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/icons8-up_circular_1.png"))); // NOI18N
        rem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rem1ActionPerformed(evt);
            }
        });

        jLabel13.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel13.setText("Section:");

        section1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        section1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Selectionnez", "Petit", "Primaire", "Secondaire" }));

        javax.swing.GroupLayout jPanel9Layout = new javax.swing.GroupLayout(jPanel9);
        jPanel9.setLayout(jPanel9Layout);
        jPanel9Layout.setHorizontalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel9Layout.createSequentialGroup()
                        .addComponent(jLabel13)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(section1, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel6)
                        .addGap(4, 4, 4)
                        .addComponent(point, javax.swing.GroupLayout.PREFERRED_SIZE, 133, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 454, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(rem1, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(add1, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );
        jPanel9Layout.setVerticalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addGap(8, 8, 8)
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel9Layout.createSequentialGroup()
                        .addComponent(add1, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(rem1, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(jPanel9Layout.createSequentialGroup()
                        .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel9Layout.createSequentialGroup()
                                .addComponent(jLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(26, 26, 26))
                            .addGroup(jPanel9Layout.createSequentialGroup()
                                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(point, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabel6))
                                    .addComponent(section1, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(13, 13, 13)))
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 233, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap())))
        );

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jPanel9, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(29, 29, 29)
                        .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Informations Pédagogiques", jPanel2);

        tblconsulta.setBackground(new java.awt.Color(255, 204, 153));
        tblconsulta.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tblconsulta.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                tblconsultaMousePressed(evt);
            }
        });
        jScrollPane1.setViewportView(tblconsulta);

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel5.setText("Institution");

        recherche.setBackground(new java.awt.Color(255, 204, 153));
        recherche.setDescripcion("Entrer le nom ici");
        recherche.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                rechercheKeyReleased(evt);
            }
        });

        foto1.setBackground(new java.awt.Color(153, 255, 153));
        foto1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        foto1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        foto1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        adresse1.setBackground(new java.awt.Color(153, 255, 153));
        adresse1.setDescripcion("Entrer l'adresse ici");
        adresse1.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                adresse1FocusGained(evt);
            }
        });

        mail1.setBackground(new java.awt.Color(153, 255, 153));
        mail1.setDescripcion("Entrer le courriel  ici");
        mail1.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                mail1FocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                mail1FocusLost(evt);
            }
        });

        deviz1.setBackground(new java.awt.Color(153, 255, 153));
        deviz1.setColumns(2);
        deviz1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        deviz1.setRows(3);
        deviz1.setName(""); // NOI18N
        deviz1.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                deviz1FocusGained(evt);
            }
        });
        jScrollPane5.setViewportView(deviz1);

        telephone1.setBackground(new java.awt.Color(153, 255, 153));
        telephone1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        telephone1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N

        tblcollection1.setBackground(new java.awt.Color(153, 255, 153));
        tblcollection1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane6.setViewportView(tblcollection1);

        tblequivalence1.setBackground(new java.awt.Color(153, 255, 153));
        tblequivalence1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tblequivalence1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                tblequivalence1MousePressed(evt);
            }
        });
        jScrollPane7.setViewportView(tblequivalence1);

        jPanel10.setBackground(new java.awt.Color(153, 255, 153));
        jPanel10.setBorder(javax.swing.BorderFactory.createTitledBorder("Moyenne Générale Requise"));

        accepte.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        accepte.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        accepte.setVerticalAlignment(javax.swing.SwingConstants.TOP);

        javax.swing.GroupLayout jPanel10Layout = new javax.swing.GroupLayout(jPanel10);
        jPanel10.setLayout(jPanel10Layout);
        jPanel10Layout.setHorizontalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(accepte, javax.swing.GroupLayout.DEFAULT_SIZE, 142, Short.MAX_VALUE)
        );
        jPanel10Layout.setVerticalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel10Layout.createSequentialGroup()
                .addComponent(accepte, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 10, Short.MAX_VALUE))
        );

        jPanel11.setBackground(new java.awt.Color(153, 255, 153));
        jPanel11.setBorder(javax.swing.BorderFactory.createTitledBorder("Année Active"));

        anneee.setBackground(new java.awt.Color(153, 255, 153));
        anneee.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        anneee.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        anneee.setText("0000 - 0000");

        javax.swing.GroupLayout jPanel11Layout = new javax.swing.GroupLayout(jPanel11);
        jPanel11.setLayout(jPanel11Layout);
        jPanel11Layout.setHorizontalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(anneee, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 162, Short.MAX_VALUE)
        );
        jPanel11Layout.setVerticalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel11Layout.createSequentialGroup()
                .addComponent(anneee, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        actauliser.setBackground(new java.awt.Color(255, 255, 255));
        actauliser.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        actauliser.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/icons8-restart.png"))); // NOI18N
        actauliser.setText("Rafraîchir");
        actauliser.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        actauliser.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                actauliserActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(jLabel5)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(recherche, javax.swing.GroupLayout.PREFERRED_SIZE, 860, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(jScrollPane6, javax.swing.GroupLayout.DEFAULT_SIZE, 425, Short.MAX_VALUE)
                            .addComponent(telephone1, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(mail1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(adresse1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jScrollPane5))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel4Layout.createSequentialGroup()
                                .addComponent(jPanel11, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jPanel10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jScrollPane7, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(foto1, javax.swing.GroupLayout.DEFAULT_SIZE, 191, Short.MAX_VALUE)
                            .addComponent(actauliser, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addContainerGap())
            .addComponent(jSeparator1)
            .addComponent(jScrollPane1)
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(recherche, javax.swing.GroupLayout.DEFAULT_SIZE, 36, Short.MAX_VALUE)
                    .addComponent(jLabel5))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 117, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 4, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(adresse1, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(13, 13, 13)
                        .addComponent(mail1, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(telephone1, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jScrollPane6, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(foto1, javax.swing.GroupLayout.PREFERRED_SIZE, 166, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(28, 28, 28)
                        .addComponent(actauliser, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jPanel10, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jPanel11, javax.swing.GroupLayout.PREFERRED_SIZE, 62, Short.MAX_VALUE))
                        .addGap(18, 18, 18)
                        .addComponent(jScrollPane7, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)))
                .addGap(24, 24, 24))
        );

        jTabbedPane1.addTab("Voir Les Institutions ", jPanel4);

        jPanel3.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        effacer.setBackground(new java.awt.Color(255, 255, 255));
        effacer.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        effacer.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/effacer.jpg"))); // NOI18N
        effacer.setText("Effacer");
        effacer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                effacerActionPerformed(evt);
            }
        });

        accepter1.setBackground(new java.awt.Color(255, 255, 255));
        accepter1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        accepter1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/aceptar.png"))); // NOI18N
        accepter1.setText("OK");
        accepter1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                accepter1ActionPerformed(evt);
            }
        });

        annuler.setBackground(new java.awt.Color(255, 255, 255));
        annuler.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        annuler.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/deletered.png"))); // NOI18N
        annuler.setText("Annuler");
        annuler.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                annulerActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(accepter1, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(27, 27, 27)
                .addComponent(effacer, javax.swing.GroupLayout.PREFERRED_SIZE, 131, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(28, 28, 28)
                .addComponent(annuler)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addGroup(jPanel3Layout.createSequentialGroup()
                            .addGap(1, 1, 1)
                            .addComponent(effacer, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addComponent(accepter1, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(annuler, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(14, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane1)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(270, 270, 270))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 509, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 7, Short.MAX_VALUE)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void effacerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_effacerActionPerformed
        int mesaj=JOptionPane.showConfirmDialog(null,"Seulement les équivalences qu’on peut effacer. Voulez-vous effacer une équivalence?",
            "Avertissement", JOptionPane.YES_OPTION, JOptionPane.WARNING_MESSAGE);
        if(mesaj==JOptionPane.YES_OPTION){
            if(tblequivalence.getSelectedRow()==-1){
                JOptionPane.showMessageDialog(null,"Vous devriez sélectionner une équivalence dans la table équivalence.");
            }else{
                String query="delete from equivalence where valeur='"+tblequivalence.getValueAt(tblequivalence.getSelectedRow(),0).toString()+"'";
                Mn.MANIPULEDB(query);
                Nettoyer();
                RemplirEqui();
            }
        }
    }//GEN-LAST:event_effacerActionPerformed

    private void accepter1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_accepter1ActionPerformed

        for(int a=0;a<tblcollection.getRowCount();a++){
            int dx = 0;
            String aa=tblcollection.getValueAt(a,0).toString();
            String bb=tblcollection.getValueAt(a,1).toString();
            String cc=tblcollection.getValueAt(a,2).toString();
            String dd=Mn.illegalCharacterOnString(tblcollection.getValueAt(a,3).toString());
            String query="call Insert_collection("+dx+",'"+aa+"','"+bb+"','"+cc+"','"+dd+"','"+nom.getText()+"')";
            System.out.println(query);
            Mn.MANIPULEDB(query);

        }
        Executer();
        Mn.EMPTYTABLE(tblcollection);
//        RemplirCollection();
        int aaa=JOptionPane.showConfirmDialog(null,"S'il-vous plait veuiller reinicier le programme.","Avis",JOptionPane.YES_OPTION);
        if(aaa==JOptionPane.YES_OPTION){
            this.dispose();
            setDefaultCloseOperation(EXIT_ON_CLOSE);
            Splash sp=new Splash();
            sp.show();
        }
    }//GEN-LAST:event_accepter1ActionPerformed

    private void annulerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_annulerActionPerformed
        int aa=JOptionPane.showConfirmDialog(null,"Etes-vous sur de ne vouloir personaliser le programme?","Question",JOptionPane.YES_OPTION);
        if(aa==JOptionPane.YES_OPTION){
            String query="select nom from institution";
            ResultSet rs=Mn.SEARCHDATA(query);
            try {
                if(rs.first()){
                    Variables.Empresa=rs.getString("nom");
                }
            } catch (SQLException ex) {
                Logger.getLogger(Splash.class.getName()).log(Level.SEVERE, null, ex);
            }
            if(Variables.Empresa==""){
                Variables.Empresa="Defaut";
            }
            this.dispose();
            Menus lg= new Menus();
            lg.show();
        }
    }//GEN-LAST:event_annulerActionPerformed

    private void subirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_subirActionPerformed
        int resultado;
        Charger ch= new Charger();
        FileNameExtensionFilter filtro= new FileNameExtensionFilter("JPG y PNG","jpg","png");
        ch.cargarphoto.setFileFilter(filtro);
        resultado=ch.cargarphoto.showOpenDialog(null);
        if(JFileChooser.APPROVE_OPTION==resultado){
            fichier=ch.cargarphoto.getSelectedFile();
            try {
                ImageIcon icon = new ImageIcon(fichier.toString());
                Icon icono= new ImageIcon(icon.getImage().getScaledInstance(
                    foto.getWidth(), foto.getHeight(),Image.SCALE_DEFAULT));
            foto.setText(null);
            route.setText(fichier.getAbsolutePath());
            foto.setIcon(icono);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Erreur de charger l'image."+e);
        }
        }
    }//GEN-LAST:event_subirActionPerformed

    private void devizFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_devizFocusGained
//        deviz.setLineWrap (true);
//        deviz.setWrapStyleWord(true);
//        deviz=new JTextArea(5,15);
    }//GEN-LAST:event_devizFocusGained

    private void abregeKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_abregeKeyReleased
        abrege.setText(abrege.getText().toUpperCase());
    }//GEN-LAST:event_abregeKeyReleased

    private void mailFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_mailFocusGained
        // TODO add your handling code here:
    }//GEN-LAST:event_mailFocusGained

    private void mailFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_mailFocusLost
        Mn.VALIDATEMAIL(mail.getText(), mail);
    }//GEN-LAST:event_mailFocusLost

    private void adresseFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_adresseFocusGained

    }//GEN-LAST:event_adresseFocusGained

    private void addActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addActionPerformed
        String[] vect={section.getSelectedItem().toString(),montant.getText(),devise.getText(),versement.getText()};
        Mn.ADD_TABLE(mds, vect);
        section.setSelectedItem(null);
        montant.setText(null);
        versement.setText(null);
    }//GEN-LAST:event_addActionPerformed

    private void remActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_remActionPerformed
        Mn.REMOVE_TABLE(tblcollection, mds);
    }//GEN-LAST:event_remActionPerformed

    private void cinqMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_cinqMouseClicked
        pluscinq.setSelected(false);
        six.setSelected(false);
        plussix.setSelected(false);
    }//GEN-LAST:event_cinqMouseClicked

    private void pluscinqMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_pluscinqMouseClicked
        cinq.setSelected(false);
        six.setSelected(false);
        plussix.setSelected(false);
    }//GEN-LAST:event_pluscinqMouseClicked

    private void sixMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_sixMouseClicked
        cinq.setSelected(false);
        pluscinq.setSelected(false);
        plussix.setSelected(false);
    }//GEN-LAST:event_sixMouseClicked

    private void plussixMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_plussixMouseClicked
        cinq.setSelected(false);
        pluscinq.setSelected(false);
        six.setSelected(false);
    }//GEN-LAST:event_plussixMouseClicked

    private void supsixActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_supsixActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_supsixActionPerformed

    private void tblequivalenceMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblequivalenceMousePressed
        JTable table =(JTable) evt.getSource();
        Point pt = evt.getPoint();
        int row = table.rowAtPoint(pt);
        if (evt.getClickCount() == 2) {
            point.setText(tblequivalence.getValueAt(tblequivalence.getSelectedRow(), 0).toString());
            code.setText(String.valueOf(Mn.SEARCHCODE("code_equivalence","valeur",point.getText(),"equivalence")));
        }
    }//GEN-LAST:event_tblequivalenceMousePressed

    private void pointFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_pointFocusGained
        // TODO add your handling code here:
    }//GEN-LAST:event_pointFocusGained

    private void add1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_add1ActionPerformed
        if(point.getText().isEmpty()){
           JOptionPane.showMessageDialog(null,"Veuillez bien vérifier si la zone de texte est remplir.");
        }else{
        String[] vect={section1.getSelectedItem().toString(),point.getText()};
        Mn.ADD_TABLE(md, vect);
        point.setText(null); 
        section1.setSelectedItem(null);
        }
    }//GEN-LAST:event_add1ActionPerformed

    private void rem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rem1ActionPerformed
        Mn.REMOVE_TABLE(tblequivalence, md);
    }//GEN-LAST:event_rem1ActionPerformed

    private void adresse1FocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_adresse1FocusGained
        // TODO add your handling code here:
    }//GEN-LAST:event_adresse1FocusGained

    private void mail1FocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_mail1FocusGained
        // TODO add your handling code here:
    }//GEN-LAST:event_mail1FocusGained

    private void mail1FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_mail1FocusLost
        // TODO add your handling code here:
    }//GEN-LAST:event_mail1FocusLost

    private void deviz1FocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_deviz1FocusGained
        // TODO add your handling code here:
    }//GEN-LAST:event_deviz1FocusGained

    private void tblequivalence1MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblequivalence1MousePressed
        // TODO add your handling code here:
    }//GEN-LAST:event_tblequivalence1MousePressed

    private void tblconsultaMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblconsultaMousePressed
        JTable table =(JTable) evt.getSource();
        Point pt = evt.getPoint();
        int row = table.rowAtPoint(pt);
        if (evt.getClickCount() == 2) {
         SelectInstituts();
         RemplirCollection();
         RemplirEqui();
         Select();
        }
    }//GEN-LAST:event_tblconsultaMousePressed

    private void actauliserActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_actauliserActionPerformed
        SelectInstitut();
        recherche.setText(null);
        mail1.setText(null);
        adresse1.setText(null);
        deviz1.setText("");
        telephone1.setValue("");
        md1.setRowCount(0);
        mds1.setRowCount(0);
        accepte.setText(null);
        foto1.setIcon(null);
    }//GEN-LAST:event_actauliserActionPerformed

    private void rechercheKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_rechercheKeyReleased
       if(recherche.getText().isEmpty()){
           SelectInstitut();
       }else{
           String querys="select abrege,nom from institution where abrege like '%"+recherche.getText()+"%'"
                   + " or nom like '%"+recherche.getText()+"%'";
           Mn.FULLTABLE(querys, tblconsulta, mder); 
       }
    }//GEN-LAST:event_rechercheKeyReleased

    private void devizFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_devizFocusLost
        valordeviz=deviz.getText();
        deviz.setLineWrap (true);
        deviz.setWrapStyleWord(true);
        deviz=new JTextArea(5,15);
    }//GEN-LAST:event_devizFocusLost

    private void actualiserActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_actualiserActionPerformed
       int aaq=Mn.MAXCODE("code_annee","annee_academique");
        String bbq=String.valueOf(annee.getValue())+"-"+String.valueOf(annee.getValue()+1);
        String query="call Insert_academique("+aaq+",'"+bbq+"')";
        Mn.MANIPULEDB(query); 
        JOptionPane.showMessageDialog(null,"Succès de l'actualisation.");
    }//GEN-LAST:event_actualiserActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Personaliser.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Personaliser.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Personaliser.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Personaliser.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Personaliser().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private org.edisoncor.gui.textField.TextFieldRectBackground abrege;
    private com.toedter.calendar.JYearChooser academique;
    private javax.swing.JLabel accepte;
    private javax.swing.JButton accepter1;
    private javax.swing.JButton actauliser;
    private javax.swing.JButton actualiser;
    private javax.swing.JButton add;
    private javax.swing.JButton add1;
    private org.edisoncor.gui.textField.TextFieldRectBackground adresse;
    private org.edisoncor.gui.textField.TextFieldRectBackground adresse1;
    private com.toedter.calendar.JYearChooser annee;
    private javax.swing.JLabel anneee;
    private javax.swing.JButton annuler;
    private javax.swing.JCheckBox cinq;
    private javax.swing.JLabel devise;
    private javax.swing.JTextArea deviz;
    private javax.swing.JTextArea deviz1;
    private javax.swing.JButton effacer;
    private javax.swing.JLabel foto;
    private javax.swing.JLabel foto1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel11;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JScrollPane jScrollPane7;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JTabbedPane jTabbedPane1;
    private org.edisoncor.gui.textField.TextFieldRectBackground mail;
    private org.edisoncor.gui.textField.TextFieldRectBackground mail1;
    private org.edisoncor.gui.textField.TextFieldRectBackground montant;
    private org.edisoncor.gui.textField.TextFieldRectBackground nom;
    private javax.swing.JCheckBox pluscinq;
    private javax.swing.JCheckBox plussix;
    public static org.edisoncor.gui.textField.TextFieldRectBackground point;
    private org.edisoncor.gui.textField.TextFieldRectBackground recherche;
    private javax.swing.JButton rem;
    private javax.swing.JButton rem1;
    private javax.swing.JComboBox section;
    private javax.swing.JComboBox section1;
    private javax.swing.JCheckBox six;
    private javax.swing.JButton subir;
    private javax.swing.JCheckBox supsix;
    private javax.swing.JTable tblcollection;
    private javax.swing.JTable tblcollection1;
    private javax.swing.JTable tblconsulta;
    private javax.swing.JTable tblequivalence;
    private javax.swing.JTable tblequivalence1;
    private javax.swing.JFormattedTextField telephone;
    private javax.swing.JFormattedTextField telephone1;
    private org.edisoncor.gui.textField.TextFieldRectBackground versement;
    // End of variables declaration//GEN-END:variables
}
