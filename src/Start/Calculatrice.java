package Start;
import Methods.Kalkilatris;
import Methods.Manipulation;

public class Calculatrice extends javax.swing.JInternalFrame {
Kalkilatris kal= new Kalkilatris();
Manipulation Mn= new Manipulation();
String legend="";
    public Calculatrice() {
        initComponents();
        this.setTitle("Scolaris: Calculatrice");
        entrada.requestFocus();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        entrada = new javax.swing.JTextField();
        plus = new javax.swing.JButton();
        div = new javax.swing.JButton();
        moins = new javax.swing.JButton();
        actualizar = new javax.swing.JButton();
        res = new javax.swing.JButton();
        un = new javax.swing.JButton();
        expo = new javax.swing.JButton();
        racine = new javax.swing.JButton();
        effacer = new javax.swing.JButton();
        quatre = new javax.swing.JButton();
        deux = new javax.swing.JButton();
        cinq = new javax.swing.JButton();
        trois = new javax.swing.JButton();
        six = new javax.swing.JButton();
        sept = new javax.swing.JButton();
        huit = new javax.swing.JButton();
        neuf = new javax.swing.JButton();
        zero = new javax.swing.JButton();
        jSeparator1 = new javax.swing.JSeparator();
        mult = new javax.swing.JButton();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        sortir = new javax.swing.JMenuItem();

        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        entrada.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        entrada.setHorizontalAlignment(javax.swing.JTextField.RIGHT);

        plus.setBackground(new java.awt.Color(153, 0, 153));
        plus.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        plus.setText("+");
        plus.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                plusMouseEntered(evt);
            }
        });
        plus.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                plusActionPerformed(evt);
            }
        });

        div.setBackground(new java.awt.Color(153, 0, 153));
        div.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        div.setText("/");
        div.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                divMouseEntered(evt);
            }
        });
        div.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                divActionPerformed(evt);
            }
        });

        moins.setBackground(new java.awt.Color(153, 0, 153));
        moins.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        moins.setText("-");
        moins.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                moinsMouseEntered(evt);
            }
        });
        moins.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                moinsActionPerformed(evt);
            }
        });

        actualizar.setBackground(new java.awt.Color(204, 0, 0));
        actualizar.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        actualizar.setText("AC");
        actualizar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                actualizarMouseEntered(evt);
            }
        });
        actualizar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                actualizarActionPerformed(evt);
            }
        });

        res.setBackground(new java.awt.Color(255, 255, 0));
        res.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        res.setText("=");
        res.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                resActionPerformed(evt);
            }
        });

        un.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        un.setText("1");
        un.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                unActionPerformed(evt);
            }
        });

        expo.setBackground(new java.awt.Color(153, 0, 153));
        expo.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        expo.setText("^");
        expo.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                expoMouseEntered(evt);
            }
        });
        expo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                expoActionPerformed(evt);
            }
        });

        racine.setBackground(new java.awt.Color(153, 0, 153));
        racine.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        racine.setText("√");
        racine.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                racineMouseEntered(evt);
            }
        });
        racine.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                racineActionPerformed(evt);
            }
        });

        effacer.setBackground(new java.awt.Color(0, 153, 51));
        effacer.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        effacer.setText("EF");
        effacer.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                effacerMouseEntered(evt);
            }
        });
        effacer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                effacerActionPerformed(evt);
            }
        });

        quatre.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        quatre.setText("4");
        quatre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                quatreActionPerformed(evt);
            }
        });

        deux.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        deux.setText("2");
        deux.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deuxActionPerformed(evt);
            }
        });

        cinq.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        cinq.setText("5");
        cinq.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cinqActionPerformed(evt);
            }
        });

        trois.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        trois.setText("3");
        trois.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                troisActionPerformed(evt);
            }
        });

        six.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        six.setText("6");
        six.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sixActionPerformed(evt);
            }
        });

        sept.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        sept.setText("7");
        sept.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                septActionPerformed(evt);
            }
        });

        huit.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        huit.setText("8");
        huit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                huitActionPerformed(evt);
            }
        });

        neuf.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        neuf.setText("9");
        neuf.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                neufActionPerformed(evt);
            }
        });

        zero.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        zero.setForeground(new java.awt.Color(0, 0, 204));
        zero.setText("0");
        zero.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                zeroActionPerformed(evt);
            }
        });

        mult.setBackground(new java.awt.Color(153, 0, 153));
        mult.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        mult.setText("*");
        mult.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                multMouseEntered(evt);
            }
        });
        mult.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                multActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 344, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(entrada)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addComponent(un, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(deux, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(trois, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(zero, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(res, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(quatre, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(cinq, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(six, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(div, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(moins, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(actualizar)
                                .addGap(18, 18, 18)
                                .addComponent(effacer, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(sept, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(huit, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(neuf, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(mult, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(plus, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(0, 0, Short.MAX_VALUE)
                                .addComponent(expo, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(racine, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(entrada, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 26, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(effacer, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(racine, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(expo, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(actualizar, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(sept, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(huit, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(neuf, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(plus, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(mult, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(quatre, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(cinq, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(six, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(div, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(moins, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(res, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(un, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(deux, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(trois, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(zero, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        jMenu1.setText("Action");

        sortir.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_S, java.awt.event.InputEvent.CTRL_MASK));
        sortir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/salir.png"))); // NOI18N
        sortir.setText("Quitter");
        sortir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sortirActionPerformed(evt);
            }
        });
        jMenu1.add(sortir);

        jMenuBar1.add(jMenu1);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(4, 4, 4)
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void plusActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_plusActionPerformed
       entrada.setText(entrada.getText()+""+plus.getText());
       entrada.requestFocus();
    }//GEN-LAST:event_plusActionPerformed

    private void actualizarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_actualizarActionPerformed
        entrada.setText(null);
       entrada.requestFocus();
    }//GEN-LAST:event_actualizarActionPerformed

    private void resActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_resActionPerformed
       entrada.setText(String.valueOf(kal.RESULTAT(entrada.getText())));
       entrada.requestFocus();
    }//GEN-LAST:event_resActionPerformed

    private void moinsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_moinsActionPerformed
       entrada.setText(entrada.getText()+""+moins.getText());
       entrada.requestFocus();
    }//GEN-LAST:event_moinsActionPerformed

    private void divActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_divActionPerformed
       entrada.setText(entrada.getText()+""+div.getText());
       entrada.requestFocus();
    }//GEN-LAST:event_divActionPerformed

    private void sortirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_sortirActionPerformed
        Mn.EXIT_BUTTON(this);
    }//GEN-LAST:event_sortirActionPerformed

    private void unActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_unActionPerformed
        entrada.setText(entrada.getText()+""+un.getText());
       entrada.requestFocus();
    }//GEN-LAST:event_unActionPerformed

    private void expoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_expoActionPerformed
        entrada.setText(entrada.getText()+""+expo.getText());
       entrada.requestFocus();
    }//GEN-LAST:event_expoActionPerformed

    private void racineActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_racineActionPerformed
      entrada.setText(entrada.getText()+""+racine.getText());
       entrada.requestFocus();
    }//GEN-LAST:event_racineActionPerformed

    private void effacerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_effacerActionPerformed
   if(entrada.getText().length()!=0){
            entrada.setText(entrada.getText().substring(0, entrada.getText().length()-1));
        }
   entrada.requestFocus();
    }//GEN-LAST:event_effacerActionPerformed

    private void quatreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_quatreActionPerformed
      entrada.setText(entrada.getText()+""+quatre.getText());
       entrada.requestFocus();
    }//GEN-LAST:event_quatreActionPerformed

    private void deuxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deuxActionPerformed
        entrada.setText(entrada.getText()+""+deux.getText());
       entrada.requestFocus();
    }//GEN-LAST:event_deuxActionPerformed

    private void cinqActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cinqActionPerformed
        entrada.setText(entrada.getText()+""+cinq.getText());
       entrada.requestFocus();
    }//GEN-LAST:event_cinqActionPerformed

    private void troisActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_troisActionPerformed
        entrada.setText(entrada.getText()+""+trois.getText());
       entrada.requestFocus();
    }//GEN-LAST:event_troisActionPerformed

    private void sixActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_sixActionPerformed
       entrada.setText(entrada.getText()+""+six.getText());
       entrada.requestFocus();
    }//GEN-LAST:event_sixActionPerformed

    private void septActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_septActionPerformed
       entrada.setText(entrada.getText()+""+sept.getText());
       entrada.requestFocus();
    }//GEN-LAST:event_septActionPerformed

    private void huitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_huitActionPerformed
       entrada.setText(entrada.getText()+""+huit.getText());
       entrada.requestFocus();
    }//GEN-LAST:event_huitActionPerformed

    private void neufActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_neufActionPerformed
        entrada.setText(entrada.getText()+""+neuf.getText());
       entrada.requestFocus();
    }//GEN-LAST:event_neufActionPerformed

    private void zeroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_zeroActionPerformed
       entrada.setText(entrada.getText()+""+zero.getText());
       entrada.requestFocus();
    }//GEN-LAST:event_zeroActionPerformed

    private void multActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_multActionPerformed
       entrada.setText(entrada.getText()+""+mult.getText());
       entrada.requestFocus();
    }//GEN-LAST:event_multActionPerformed

    private void actualizarMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_actualizarMouseEntered
        actualizar.setToolTipText("Re-initialiser les opérations.");
    }//GEN-LAST:event_actualizarMouseEntered

    private void effacerMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_effacerMouseEntered
      effacer.setToolTipText("Effacer une expréssion.");
    }//GEN-LAST:event_effacerMouseEntered

    private void expoMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_expoMouseEntered
       expo.setToolTipText("Format: a^n ");
    }//GEN-LAST:event_expoMouseEntered

    private void racineMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_racineMouseEntered
     racine.setToolTipText("Format: √a ");
    }//GEN-LAST:event_racineMouseEntered

    private void multMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_multMouseEntered
        mult.setToolTipText("Format: a*b ");
    }//GEN-LAST:event_multMouseEntered

    private void plusMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_plusMouseEntered
       plus.setToolTipText("Format: a+b ");
    }//GEN-LAST:event_plusMouseEntered

    private void divMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_divMouseEntered
         div.setToolTipText("Format: a/b ");
    }//GEN-LAST:event_divMouseEntered

    private void moinsMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_moinsMouseEntered
        moins.setToolTipText("Format: a-b ");
    }//GEN-LAST:event_moinsMouseEntered

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Calculatrice.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Calculatrice.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Calculatrice.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Calculatrice.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Calculatrice().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton actualizar;
    private javax.swing.JButton cinq;
    private javax.swing.JButton deux;
    private javax.swing.JButton div;
    private javax.swing.JButton effacer;
    private javax.swing.JTextField entrada;
    private javax.swing.JButton expo;
    private javax.swing.JButton huit;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JButton moins;
    private javax.swing.JButton mult;
    private javax.swing.JButton neuf;
    private javax.swing.JButton plus;
    private javax.swing.JButton quatre;
    private javax.swing.JButton racine;
    private javax.swing.JButton res;
    private javax.swing.JButton sept;
    private javax.swing.JButton six;
    private javax.swing.JMenuItem sortir;
    private javax.swing.JButton trois;
    private javax.swing.JButton un;
    private javax.swing.JButton zero;
    // End of variables declaration//GEN-END:variables
}
