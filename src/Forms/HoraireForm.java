/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Forms;

import Consults.Employe;
import Methods.Manipulation;
import Methods.Theme;
import Methods.Variables;
import com.ezware.oxbow.swingbits.table.filter.TableRowFilterSupport;
import java.awt.Color;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.util.Arrays;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BorderFactory;
import javax.swing.JOptionPane;
import javax.swing.JSpinner;
import javax.swing.SpinnerDateModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;

/**
 *
 * @author josephandyfeidje
 */
public class HoraireForm extends javax.swing.JFrame {

   Manipulation Mn = new Manipulation();
    Theme th = new Theme();
    String[][] data = {};
    String[] head = {"Classes", "Matières", "Heures", "Devise"};
    String[] heads = {" Jours", " Classes", " Matières", "N.d'hres", " Devise", "Entrer", "Sortie"};
    DefaultTableModel md = new DefaultTableModel(data, head);
    DefaultTableModel mds = new DefaultTableModel(data, heads);
    int cantheure = 0;
    int equivalheure = 0;
    String materia = "";
    String clase = "";
    int conteur = 0;
    TableModelListener listeners;
    
    public HoraireForm() {
        initComponents();
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setLocationRelativeTo(this);
        this.setTitle("Scolaris: " + Variables.Empresa);
        nouveauActionPerformed(null);
        tblmatiere.setModel(md);
        tblhoraire.setModel(mds);
        mesajel.setVisible(false);
        jours.setSelectedItem(null);
        Nettoyer();
        TableColumnModel cmyy = tblmatiere.getColumnModel();
        TableColumnModel cm = tblhoraire.getColumnModel();
        cm.getColumn(0).setPreferredWidth(30);
        cm.getColumn(1).setPreferredWidth(30);
        cm.getColumn(2).setPreferredWidth(70);
        cm.getColumn(3).setPreferredWidth(5);
        cm.getColumn(4).setPreferredWidth(10);
        cm.getColumn(5).setPreferredWidth(50);
        cm.getColumn(6).setPreferredWidth(3);
        cmyy.getColumn(0).setPreferredWidth(70);
        cmyy.getColumn(1).setPreferredWidth(90);
        cmyy.getColumn(2).setPreferredWidth(5);
        cmyy.getColumn(3).setPreferredWidth(10);

        TableRowFilterSupport.forTable(tblhoraire).searchable(true).apply();

        Calendar cal = Calendar.getInstance();
        cal.setTime(Mn.HORA("08:00:00"));
        jSpinner1.setModel(new SpinnerDateModel(cal.getTime(), null, null, Calendar.HOUR));
        jSpinner2.setModel(new SpinnerDateModel(cal.getTime(), null, null, Calendar.HOUR));
        JSpinner.DateEditor in = new JSpinner.DateEditor(jSpinner1, "HH:mm:ss ");
        JSpinner.DateEditor out = new JSpinner.DateEditor(jSpinner2, "HH:mm:ss ");
        jSpinner1.setEditor(in);
        jSpinner2.setEditor(out);
        jSpinner1.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                horaAutomated();
            }
        });
        gridAsure();
        tblhoraire.getModel().addTableModelListener(tablemodel());
        tableHorraireChangeListener();
        tableMatiereChangeListener();
        
        effacer.setEnabled(false);
    }

    private void tableHorraireChangeListener() {
        tblhoraire.getSelectionModel().addListSelectionListener(new ListSelectionListener() {

            @Override
            public void valueChanged(ListSelectionEvent e) {
                if(tblhoraire.getRowCount() > 0){
                    try {
                jours.setSelectedItem(tblhoraire.getValueAt(tblhoraire.getSelectedRow(), 0).toString());
                heure.setText(tblhoraire.getValueAt(tblhoraire.getSelectedRow(), 3).toString());
                jSpinner1.setValue(tblhoraire.getValueAt(tblhoraire.getSelectedRow(), 5));
                    } catch (IndexOutOfBoundsException eb) {
                    }
                
                }
            }
        });
    }

    private void tableMatiereChangeListener() {
        tblmatiere.getSelectionModel().addListSelectionListener(new ListSelectionListener() {

            @Override
            public void valueChanged(ListSelectionEvent e) {
                if (tblmatiere.getSelectedRow() >= 0) {
                    gridAsure();

                    int reste = 0;
                    conteur = 0;
                    cantheure = Integer.parseInt(tblmatiere.getValueAt(tblmatiere.getSelectedRow(), 2).toString());
                    System.out.println("mnb " + cantheure);
                    clase = tblmatiere.getValueAt(tblmatiere.getSelectedRow(), 0).toString();
                    materia = tblmatiere.getValueAt(tblmatiere.getSelectedRow(), 1).toString();
                    equivalheure = 0;
                    mat.setText(tblmatiere.getValueAt(tblmatiere.getSelectedRow(), 1).toString());
                    semaine.setText(tblmatiere.getValueAt(tblmatiere.getSelectedRow(), 2).toString());
                    clas.setText(tblmatiere.getValueAt(tblmatiere.getSelectedRow(), 0).toString());
                    if (tblhoraire.getRowCount() == 0) {
                        restant.setText(cantheure + "");
                    } else {
                        for (int i = 0; i < tblhoraire.getRowCount(); i++) {
                            if (tblhoraire.getValueAt(i, 1).equals(clase) && tblhoraire.getValueAt(i, 2).equals(materia)) {
                                int aux = Integer.parseInt(tblhoraire.getValueAt(i, 3).toString());
                                conteur = conteur + aux;
                                reste = cantheure - conteur;
                            }
                        }
                        restant.setText(reste + "");
                    }
                    System.out.println("Quntite heure par semaine: " + cantheure);
                }
            }
        });
    }

    private void saveTemp() {
        String code = "HorProf#"+matricule.getText().trim();
        String jou = jours.getSelectedItem().toString();
        String cls = clas.getText().trim();
        String mati = mat.getText().trim();
        int nbr_h = Integer.parseInt(heure.getText().trim());
        String devise = "Heure(s)";
        String hr_in = Mn.HOURFormat(jSpinner1.getValue());
        String hr_out = Mn.HOURFormat(jSpinner2.getValue());

        String sql = "call Insert_horaire_temp('" + code + "','" + jou + "','" + cls + "','" + mati + "','" + nbr_h + "','" + devise + "','" + hr_in + "','" + hr_out + "')";
        System.out.println(sql);
        Mn.MANIPULEDB(sql);

    }

    private TableModelListener tablemodel() {
        listeners = new TableModelListener() {

            @Override
            public void tableChanged(TableModelEvent e) {
                jLabel11.setText(String.valueOf(tblhoraire.getRowCount()));
            }
        };
        return listeners;
    }

    private boolean isGridEqualValues(Object[] array) {
        if (tblhoraire.getRowCount() > 0) {
            int h = array.length;
            Object[] array2 = new Object[h];

            for (int i = 0; i < tblhoraire.getColumnCount(); i++) {
                array2[i] = tblhoraire.getValueAt(tblhoraire.getRowCount() - 1, i).toString();
                System.out.println(i + " " + array2[i]);
            }
            System.out.println("ccc " + Arrays.toString(array2));
        }
        return true;
    }

    private void gridAsure() {
        if (tblmatiere.getSelectedRow() > -1) {
            jours.setEnabled(true);
        } else {
            jours.setEnabled(false);
        }
    }

    private String searchRedundant() {
        String cod = "HorProf#"+matricule.getText().trim();
        String crs = "";
        String sql = "select matiere,classe,jour,heure_in,heure_out from horaire_temp where jour = '" + jours.getSelectedItem().toString() + "' and code_horraire = '"+cod+"' ";
        System.out.println("bg " + sql);
        ResultSet rs = Mn.Rezulta(sql);

        try {
            while (rs.next()) {
                String mat = rs.getString("matiere");
                String cls = rs.getString("classe");
                String jr = rs.getString("jour");
                Time hr_in = rs.getTime("heure_in");
                Time hr_out = rs.getTime("heure_out");

                Calendar cal = Calendar.getInstance();
                cal.setTime(hr_in);

                Calendar cal2 = Calendar.getInstance();
                cal2.setTime(hr_out);

                Calendar calAp = Calendar.getInstance();
                calAp.setTime(Mn.HORA(Mn.HOURFormat(jSpinner1)));

                Calendar calAv = Calendar.getInstance();
                calAv.setTime(Mn.HORA(Mn.HOURFormat(jSpinner2)));

                calAp.add(Calendar.MINUTE, 1);
                System.out.println("calap " + calAp.getTime());
                calAv.add(Calendar.MINUTE, -1);
                System.out.println("calav " + calAv.getTime());

                System.out.println("dtd " + Mn.HOURFormat(cal.getTime()));

                String hr_in2 = Mn.HOURFormat(cal.getTime());
                String hr_out2 = Mn.HOURFormat(cal2.getTime());

                String sqld = "SELECT matiere,classe,heure_in,heure_out FROM horaire_temp where code_horraire = '"+cod+"' and jour = '" + jr + "' and ((heure_in  between '" + Mn.HOURFormat(calAp.getTime()) + "'  and '" + Mn.HOURFormat(calAv.getTime()) + "') or (heure_out  between '" + Mn.HOURFormat(calAp.getTime()) + "'  and '" + Mn.HOURFormat(calAv.getTime()) + "') "
                        + " or ( '" + Mn.HOURFormat(calAp.getTime()) + "'  between heure_in  and heure_out) or ( '" + Mn.HOURFormat(calAv.getTime()) + "'  between heure_in  and heure_out))";
                System.out.println("sqld " + sqld);
                ResultSet rsd = Mn.Rezulta(sqld);
                while (rsd.next()) {
                    mat = rsd.getString("matiere");
                    hr_in = rsd.getTime("heure_in");
                    hr_out = rsd.getTime("heure_out");

                    Calendar cal3 = Calendar.getInstance();
                    cal3.setTime(Mn.HORA(Mn.HOURFormat(jSpinner2)));

              //JOptionPane.showMessageDialog(null, "hr in "+hr_in+" cal "+Mn.HOURFormat(cal3.getTime()));
                    //if(hr_in == cal3.getTime()){
                    crs += mat + " " + cls + " " + jr + " " + hr_in + "-" + hr_out + "\n";
                    //}
                }

            }
        } catch (SQLException ex) {
            Logger.getLogger(Horaire.class.getName()).log(Level.SEVERE, null, ex);
        }

        return crs;
    }

    private int horaAutomated() {

        int d = 0;
        if (!heure.getText().isEmpty()) {
            Calendar cal = Calendar.getInstance();
            cal.setTime(Mn.HORA(Mn.HOURFormat(jSpinner1.getValue())));
            int hh = Integer.parseInt(heure.getText().trim());
            System.out.println("cal " + Mn.HOURFormat(cal.getTime()) + " hh " + hh);
            cal.add(Calendar.HOUR, hh);
            System.out.println("dtd " + Mn.HOURFormat(cal.getTime()));
            jSpinner2.setValue(Mn.HORA(Mn.HOURFormat(cal.getTime())));
        }
        return d;
    }

    private void RemplirCours() {
        String aa = matricule.getText();
        String query = "select classe,matieres,heure,deviz from professeur where matricule='" + aa + "' ";
        Mn.FULLTABLE(query, tblmatiere, md);
    }

    private void RemplirHoraire() {
        try {
            String aa = "HorProf#"+matricule.getText();
            String query = "select jour,classe,matiere,nbre_heure,devise,heure_in,heure_out  from horaire where code_horraire = '"+aa+"' ";
            ResultSet rs = Mn.Rezulta(query);
            while (rs.next()) {
                String jr = rs.getString("jour");
                String cl = rs.getString("classe");
                String mt = rs.getString("matiere");
                int nbr = rs.getInt("nbre_heure");
                String dv = rs.getString("devise");
                Time hr_in = rs.getTime("heure_in");
                Time hr_out = rs.getTime("heure_out");

                String sql = " call Insert_horaire_temp ('" + aa + "','" + jr + "','" + cl + "','" + mt + "'," + nbr + ",'" + dv + "','" + hr_in + "','" + hr_out + "') ";
                Mn.MANIPULEDB(sql);
            }

            String query_ = "select jour,classe,matiere,nbre_heure,devise,heure_in,heure_out  from horaire_temp where code_horraire ='" + aa + "' ";
            Mn.FULLTABLE(query_, tblhoraire, mds);
            jLabel15.setText("Nombre(s) de registre(s) : "+tblhoraire.getRowCount());
        } catch (SQLException ex) {
            Logger.getLogger(Horaire.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void Nettoyer() {
        Mn.EMPTYTABLE(tblhoraire);
        Mn.EMPTYTABLE(tblmatiere);
        matricule.setValue(null);
        nom.setText("Nom d'employer");
        jours.setSelectedItem("Selectionnez");
        heure.setText(null);
        mat.setText(null);
        semaine.setText(null);
        restant.setText(null);
        clas.setText(null);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jPanel5 = new javax.swing.JPanel();
        nom = new javax.swing.JLabel();
        matricule = new javax.swing.JFormattedTextField();
        jPanel6 = new javax.swing.JPanel();
        jPanel7 = new javax.swing.JPanel();
        jPanel8 = new javax.swing.JPanel();
        mesajel = new javax.swing.JLabel();
        jPanel9 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblmatiere = new javax.swing.JTable(){
            public boolean isCellEditable(int rowIndex, int colIndex){
                return false;
            }
        };
        jPanel10 = new javax.swing.JPanel();
        jPanel11 = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        mat = new javax.swing.JLabel();
        jPanel12 = new javax.swing.JPanel();
        jLabel9 = new javax.swing.JLabel();
        clas = new javax.swing.JLabel();
        jPanel13 = new javax.swing.JPanel();
        jLabel8 = new javax.swing.JLabel();
        semaine = new javax.swing.JLabel();
        jPanel14 = new javax.swing.JPanel();
        jLabel10 = new javax.swing.JLabel();
        restant = new javax.swing.JLabel();
        jPanel15 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tblhoraire = new javax.swing.JTable(){
            public boolean isCellEditable(int rowIndex, int colIndex){
                return false;
            }
        };
        jPanel16 = new javax.swing.JPanel();
        jLabel15 = new javax.swing.JLabel();
        jPanel17 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        jours = new javax.swing.JComboBox();
        jLabel11 = new javax.swing.JLabel();
        heure = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        jSpinner1 = new javax.swing.JSpinner();
        jLabel13 = new javax.swing.JLabel();
        jSpinner2 = new javax.swing.JSpinner();
        jPanel18 = new javax.swing.JPanel();
        add = new javax.swing.JPanel();
        jLabel14 = new javax.swing.JLabel();
        rem = new javax.swing.JPanel();
        jLabel16 = new javax.swing.JLabel();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        enregistrer = new javax.swing.JMenuItem();
        nouveau = new javax.swing.JMenuItem();
        effacer = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        jMenuItem4 = new javax.swing.JMenuItem();
        jMenuItem5 = new javax.swing.JMenuItem();
        jMenuItem6 = new javax.swing.JMenuItem();
        jMenuItem7 = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(new java.awt.CardLayout());

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));

        jPanel2.setBackground(new java.awt.Color(87, 148, 210));
        jPanel2.setBorder(new org.edisoncor.gui.util.DropShadowBorder());

        jLabel5.setBackground(new java.awt.Color(255, 255, 255));
        jLabel5.setFont(new java.awt.Font("Lucida Grande", 1, 18)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(255, 255, 255));
        jLabel5.setText("Registre d'horaire(s)");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel5)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(26, Short.MAX_VALUE))
        );

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));

        jLabel1.setText("Matricule : ");

        jPanel4.setBackground(new java.awt.Color(255, 255, 255));
        jPanel4.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jPanel4MouseClicked(evt);
            }
        });
        jPanel4.setLayout(new java.awt.CardLayout());

        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/ButtonFoto/icons8-human_esearch_rogram.png"))); // NOI18N
        jPanel4.add(jLabel2, "card2");

        jPanel5.setBackground(new java.awt.Color(255, 255, 255));
        jPanel5.setLayout(new java.awt.CardLayout());

        nom.setText("Nom du professeur");
        jPanel5.add(nom, "card2");

        matricule.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                matriculeKeyTyped(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                matriculeKeyReleased(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(matricule, javax.swing.GroupLayout.PREFERRED_SIZE, 191, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, 265, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(301, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel5, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1)
                            .addComponent(matricule, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );

        jPanel6.setBackground(new java.awt.Color(87, 148, 210));

        jPanel7.setBackground(new java.awt.Color(255, 255, 255));
        jPanel7.setLayout(new java.awt.CardLayout());

        jPanel8.setBackground(new java.awt.Color(255, 255, 255));
        jPanel8.setLayout(new java.awt.CardLayout());

        mesajel.setFont(new java.awt.Font("Times New Roman", 1, 13)); // NOI18N
        mesajel.setForeground(new java.awt.Color(87, 148, 210));
        mesajel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        mesajel.setText("Statut du professeur");
        mesajel.setBorder(new org.edisoncor.gui.util.DropShadowBorder());
        jPanel8.add(mesajel, "card2");

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addGap(291, 291, 291)
                .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, 317, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jPanel8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel7, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 26, Short.MAX_VALUE))
                .addContainerGap())
        );

        jPanel9.setLayout(new java.awt.CardLayout());

        tblmatiere.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tblmatiere);

        jPanel9.add(jScrollPane1, "card2");

        jPanel10.setBackground(new java.awt.Color(255, 255, 255));

        jPanel11.setBackground(new java.awt.Color(87, 148, 210));
        jPanel11.setBorder(new org.edisoncor.gui.util.DropShadowBorder());

        jLabel6.setBackground(new java.awt.Color(255, 255, 255));
        jLabel6.setText("Matieres: ");

        mat.setBackground(new java.awt.Color(255, 255, 255));
        mat.setFont(new java.awt.Font("Lucida Grande", 1, 13)); // NOI18N
        mat.setForeground(new java.awt.Color(255, 255, 255));
        mat.setText("jLabel7");

        javax.swing.GroupLayout jPanel11Layout = new javax.swing.GroupLayout(jPanel11);
        jPanel11.setLayout(jPanel11Layout);
        jPanel11Layout.setHorizontalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 299, Short.MAX_VALUE)
            .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel11Layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(jLabel6)
                    .addGap(5, 5, 5)
                    .addComponent(mat, javax.swing.GroupLayout.DEFAULT_SIZE, 221, Short.MAX_VALUE)
                    .addContainerGap()))
        );
        jPanel11Layout.setVerticalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 25, Short.MAX_VALUE)
            .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel11Layout.createSequentialGroup()
                    .addGap(3, 3, 3)
                    .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel6)
                        .addComponent(mat, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );

        jPanel12.setBackground(new java.awt.Color(87, 148, 210));
        jPanel12.setBorder(new org.edisoncor.gui.util.DropShadowBorder());

        jLabel9.setText("Classe: ");

        clas.setFont(new java.awt.Font("Lucida Grande", 1, 13)); // NOI18N
        clas.setForeground(new java.awt.Color(255, 255, 255));
        clas.setText("jLabel7");

        javax.swing.GroupLayout jPanel12Layout = new javax.swing.GroupLayout(jPanel12);
        jPanel12.setLayout(jPanel12Layout);
        jPanel12Layout.setHorizontalGroup(
            jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 299, Short.MAX_VALUE)
            .addGroup(jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel12Layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(jLabel9)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(clas, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addContainerGap()))
        );
        jPanel12Layout.setVerticalGroup(
            jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 26, Short.MAX_VALUE)
            .addGroup(jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel12Layout.createSequentialGroup()
                    .addGap(4, 4, 4)
                    .addGroup(jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel9)
                        .addComponent(clas))
                    .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );

        jPanel13.setBackground(new java.awt.Color(87, 148, 210));
        jPanel13.setBorder(new org.edisoncor.gui.util.DropShadowBorder());

        jLabel8.setText("Qtites d'hres/semaine:");

        semaine.setFont(new java.awt.Font("Lucida Grande", 1, 13)); // NOI18N
        semaine.setForeground(new java.awt.Color(255, 255, 255));
        semaine.setText("jLabel7");

        javax.swing.GroupLayout jPanel13Layout = new javax.swing.GroupLayout(jPanel13);
        jPanel13.setLayout(jPanel13Layout);
        jPanel13Layout.setHorizontalGroup(
            jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 299, Short.MAX_VALUE)
            .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel13Layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(jLabel8)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(semaine, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addContainerGap()))
        );
        jPanel13Layout.setVerticalGroup(
            jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 27, Short.MAX_VALUE)
            .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel13Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel8)
                        .addComponent(semaine))
                    .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );

        jPanel14.setBackground(new java.awt.Color(87, 148, 210));
        jPanel14.setBorder(new org.edisoncor.gui.util.DropShadowBorder());

        jLabel10.setText("Qtites d'hres Restante: ");

        restant.setFont(new java.awt.Font("Lucida Grande", 1, 13)); // NOI18N
        restant.setForeground(new java.awt.Color(255, 255, 255));
        restant.setText("jLabel7");

        javax.swing.GroupLayout jPanel14Layout = new javax.swing.GroupLayout(jPanel14);
        jPanel14.setLayout(jPanel14Layout);
        jPanel14Layout.setHorizontalGroup(
            jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 299, Short.MAX_VALUE)
            .addGroup(jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel14Layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(jLabel10)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(restant, javax.swing.GroupLayout.DEFAULT_SIZE, 186, Short.MAX_VALUE)
                    .addContainerGap()))
        );
        jPanel14Layout.setVerticalGroup(
            jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 27, Short.MAX_VALUE)
            .addGroup(jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel14Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addGroup(jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(restant)
                        .addComponent(jLabel10))
                    .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );

        javax.swing.GroupLayout jPanel10Layout = new javax.swing.GroupLayout(jPanel10);
        jPanel10.setLayout(jPanel10Layout);
        jPanel10Layout.setHorizontalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
            .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel10Layout.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jPanel11, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jPanel12, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jPanel13, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jPanel14, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addContainerGap()))
        );
        jPanel10Layout.setVerticalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 163, Short.MAX_VALUE)
            .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel10Layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(jPanel11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(jPanel12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(jPanel13, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(jPanel14, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );

        jPanel15.setLayout(new java.awt.CardLayout());

        tblhoraire.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane2.setViewportView(tblhoraire);

        jPanel15.add(jScrollPane2, "card2");

        jPanel16.setBackground(new java.awt.Color(255, 255, 255));
        jPanel16.setLayout(new java.awt.CardLayout());

        jLabel15.setText("Nombre(s) de registre(s) : 0");
        jPanel16.add(jLabel15, "card2");

        jPanel17.setBackground(new java.awt.Color(87, 148, 210));

        jLabel4.setFont(new java.awt.Font("Lucida Grande", 1, 13)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(255, 255, 255));
        jLabel4.setText("Jours : ");

        jours.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Selectionnez", "Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi" }));
        jours.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                joursItemStateChanged(evt);
            }
        });

        jLabel11.setFont(new java.awt.Font("Lucida Grande", 1, 13)); // NOI18N
        jLabel11.setForeground(new java.awt.Color(255, 255, 255));
        jLabel11.setText("Heure : ");

        heure.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                heureFocusLost(evt);
            }
        });
        heure.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                heureActionPerformed(evt);
            }
        });
        heure.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                heureKeyReleased(evt);
            }
        });

        jLabel12.setFont(new java.awt.Font("Lucida Grande", 1, 13)); // NOI18N
        jLabel12.setForeground(new java.awt.Color(255, 255, 255));
        jLabel12.setText("De : ");

        jSpinner1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jSpinner1KeyReleased(evt);
            }
        });

        jLabel13.setFont(new java.awt.Font("Lucida Grande", 1, 13)); // NOI18N
        jLabel13.setForeground(new java.awt.Color(255, 255, 255));
        jLabel13.setText("A : ");

        jPanel18.setBackground(new java.awt.Color(153, 255, 255));
        jPanel18.setLayout(new java.awt.GridLayout(1, 0));

        add.setBackground(new java.awt.Color(255, 255, 255));
        add.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                addMouseClicked(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                addMouseExited(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                addMouseEntered(evt);
            }
        });
        add.setLayout(new java.awt.CardLayout());

        jLabel14.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/ButtonFoto/icons8-chevron_down_round.png"))); // NOI18N
        add.add(jLabel14, "card2");

        jPanel18.add(add);

        rem.setBackground(new java.awt.Color(255, 255, 255));
        rem.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                remMouseClicked(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                remMouseExited(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                remMouseEntered(evt);
            }
        });
        rem.setLayout(new java.awt.CardLayout());

        jLabel16.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/ButtonFoto/icons8-circled_chevron_up.png"))); // NOI18N
        rem.add(jLabel16, "card2");

        jPanel18.add(rem);

        javax.swing.GroupLayout jPanel17Layout = new javax.swing.GroupLayout(jPanel17);
        jPanel17.setLayout(jPanel17Layout);
        jPanel17Layout.setHorizontalGroup(
            jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel17Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jours, javax.swing.GroupLayout.PREFERRED_SIZE, 175, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel11)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(heure, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel12)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSpinner1, javax.swing.GroupLayout.PREFERRED_SIZE, 123, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel13)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSpinner2, javax.swing.GroupLayout.PREFERRED_SIZE, 122, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(189, Short.MAX_VALUE))
            .addGroup(jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel17Layout.createSequentialGroup()
                    .addGap(709, 709, 709)
                    .addComponent(jPanel18, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(135, Short.MAX_VALUE)))
        );
        jPanel17Layout.setVerticalGroup(
            jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel17Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(jours, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel11)
                    .addComponent(heure, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel12)
                    .addComponent(jSpinner1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel13)
                    .addComponent(jSpinner2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel17Layout.createSequentialGroup()
                    .addGap(7, 7, 7)
                    .addComponent(jPanel18, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(8, Short.MAX_VALUE)))
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel17, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel15, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jPanel9, javax.swing.GroupLayout.PREFERRED_SIZE, 519, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel10, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                    .addContainerGap(718, Short.MAX_VALUE)
                    .addComponent(jPanel16, javax.swing.GroupLayout.PREFERRED_SIZE, 182, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap()))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel9, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jPanel10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 13, Short.MAX_VALUE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel17, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel15, javax.swing.GroupLayout.PREFERRED_SIZE, 235, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(24, 24, 24))
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                    .addContainerGap(642, Short.MAX_VALUE)
                    .addComponent(jPanel16, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap()))
        );

        getContentPane().add(jPanel1, "card2");

        jMenu1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/ButtonFoto/icons8-menu.png"))); // NOI18N
        jMenu1.setText("Action");

        enregistrer.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/ButtonFoto/icons8-upload_to_cloud.png"))); // NOI18N
        enregistrer.setText("Enregistrer");
        enregistrer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                enregistrerActionPerformed(evt);
            }
        });
        jMenu1.add(enregistrer);

        nouveau.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/ButtonFoto/icons8-new_by_copy.png"))); // NOI18N
        nouveau.setText("Nouveau");
        nouveau.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nouveauActionPerformed(evt);
            }
        });
        jMenu1.add(nouveau);

        effacer.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/ButtonFoto/icons8-trash.png"))); // NOI18N
        effacer.setText("Effacer");
        effacer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                effacerActionPerformed(evt);
            }
        });
        jMenu1.add(effacer);

        jMenuBar1.add(jMenu1);

        jMenu2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/ButtonFoto/icons8-menu_squared.png"))); // NOI18N
        jMenu2.setText("Edition");

        jMenuItem4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/ButtonFoto/icons8-view_file.png"))); // NOI18N
        jMenuItem4.setText("Consulter");
        jMenu2.add(jMenuItem4);

        jMenuItem5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/ButtonFoto/icons8-edit_file.png"))); // NOI18N
        jMenuItem5.setText("Modifier");
        jMenuItem5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem5ActionPerformed(evt);
            }
        });
        jMenu2.add(jMenuItem5);

        jMenuItem6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/ButtonFoto/icons8-new_by_copy.png"))); // NOI18N
        jMenuItem6.setText("Ajouter Matière");
        jMenu2.add(jMenuItem6);

        jMenuItem7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/ButtonFoto/icons8-help.png"))); // NOI18N
        jMenuItem7.setText("Aide");
        jMenu2.add(jMenuItem7);

        jMenuBar1.add(jMenu2);

        setJMenuBar(jMenuBar1);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jMenuItem5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem5ActionPerformed
        String aq = matricule.getText();
        String ab = "";
        String lalal = "";
        for (int i = 0; i < tblhoraire.getRowCount(); i++) {
            String qq = tblhoraire.getValueAt(i, 0).toString();
            String qqq = tblhoraire.getValueAt(i, 1).toString();
            String ddd = tblhoraire.getValueAt(i, 2).toString();
            String eee = tblhoraire.getValueAt(i, 3).toString();
            String fff = tblhoraire.getValueAt(i, 4).toString();
            String ggg = tblhoraire.getValueAt(i, 5).toString();
            String hhh = tblhoraire.getValueAt(i, 6).toString();
            String hola = "select matricule from horaire where jour='" + qq + "' and matiere='" + ddd + "'";
            ResultSet rss = Mn.SEARCHDATA(hola);
            try {
                if (rss.first()) {
                    ab = rss.getString("matricule");
                }
            } catch (SQLException ex) {
                Logger.getLogger(Eleves.class.getName()).log(Level.SEVERE, null, ex);
            }
            if (matricule.getText().equals(ab)) {
                lalal = "update horaire set matricule='" + aq + "',classe='" + qqq + "',matiere='" + ddd + "',heure_in='" + eee + "',heure_out='" + hhh + "' "
                        + "where matiere='" + ddd + "' and jour='" + qq + "'";
                Mn.MANIPULEDB(lalal);
                ab = "";
            } else {
                lalal = "call Insert_horaire('" + aq + "','" + qq + "','" + qqq + "','" + ddd + "','" + eee + "','" + fff + "','" + ggg + "','" + hhh + "')";
                Mn.MANIPULEDB(lalal);
            }
        }
        nouveauActionPerformed(evt);
    }//GEN-LAST:event_jMenuItem5ActionPerformed

    private void jSpinner1KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jSpinner1KeyReleased
        horaAutomated();
    }//GEN-LAST:event_jSpinner1KeyReleased

    private void addMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_addMouseExited
        //        jPanel5.setBackground(new Color(255, 255, 255));
        add.setBorder(BorderFactory.createLineBorder(new Color(255, 255,255)));
    }//GEN-LAST:event_addMouseExited

    private void addMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_addMouseEntered
        // [87,148,210]
        //        jPanel5.setBackground(new Color(204, 204, 204));
        add.setBorder(BorderFactory.createLineBorder(new Color(87, 148,210)));
    }//GEN-LAST:event_addMouseEntered

    private void remMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_remMouseExited
        //        jPanel6.setBackground(new Color(255, 255, 255));
        rem.setBorder(BorderFactory.createLineBorder(new Color(255, 255,255)));
    }//GEN-LAST:event_remMouseExited

    private void remMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_remMouseEntered
        //        jPanel6.setBackground(new Color(204, 204, 204));
        rem.setBorder(BorderFactory.createLineBorder(new Color(87, 148,210)));
    }//GEN-LAST:event_remMouseEntered

    private void enregistrerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_enregistrerActionPerformed
        String aq = "HorProf#"+matricule.getText();
        String ab = "";
        String lalal = "";

        String sql = "delete from horaire where code_horraire = '" + aq + "' ";
//        Mn.MANIPULEDB(sql);
        for (int i = 0; i < tblhoraire.getRowCount(); i++) {
            String qq = tblhoraire.getValueAt(i, 0).toString();
            String qqq = tblhoraire.getValueAt(i, 1).toString();
            String ddd = tblhoraire.getValueAt(i, 2).toString();
            String eee = tblhoraire.getValueAt(i, 3).toString();
            String fff = tblhoraire.getValueAt(i, 4).toString();
            String ggg = tblhoraire.getValueAt(i, 5).toString();
            String hhh = tblhoraire.getValueAt(i, 6).toString();

            lalal = "call Insert_horaire('" + aq + "','" + qq + "','" + qqq + "','" + ddd + "','" + eee + "','" + fff + "','" + ggg + "','" + hhh + "')";
                    System.out.println(lalal);
            Mn.MANIPULEDB(lalal);
            
            sql = "call professeur_has_horaire_procedure ('"+matricule.getText().trim()+"','"+aq+"','"+qq+"','"+qqq+"','"+ddd+"')";
            System.out.println("sql "+sql);
            Mn.MANIPULEDB(sql);
        }
        nouveauActionPerformed(evt);
        String sqls = "truncate table horaire_temp";
        Mn.MANIPULEDB(sqls);
    }//GEN-LAST:event_enregistrerActionPerformed

    private void effacerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_effacerActionPerformed
        String val = Mn.SELLECT_BUTTON(Variables.Seleccionado1, tblhoraire);
        String val1 = tblhoraire.getValueAt(tblhoraire.getSelectedRow(), 2).toString();
        String query = "delete from horaire where matricule='" + matricule.getText() + "' and jour='" + val + "' and matiere='" + val1 + "'";
        System.out.println(query);
        Mn.MANIPULEDB(query);
        RemplirHoraire();
    }//GEN-LAST:event_effacerActionPerformed

    private void heureKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_heureKeyReleased
        horaAutomated();
    }//GEN-LAST:event_heureKeyReleased

    private void addMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_addMouseClicked
        if (tblmatiere.getSelectedRow() >= 0) {
            String st = searchRedundant();
            if (st.equals("")) {
                String[] vect = {jours.getSelectedItem().toString(), clase, materia, heure.getText(), "Heures", Mn.HOURFormat(jSpinner1), Mn.HOURFormat(jSpinner2)};

                if (heure.getText().isEmpty()) {
                } else if (tblhoraire.getRowCount() == 0) {
                    equivalheure = equivalheure + Integer.parseInt(heure.getText());
                    if (equivalheure <= cantheure) {
                        Mn.ADD_TABLE(mds, vect);
                        saveTemp();
                        jours.setSelectedItem(null);
                        heure.setText(null);
                    } else {
                        JOptionPane.showMessageDialog(null, "mnj" + "Vous avez depasser la quantité d'heure par semaine pour cette matière");
                        equivalheure = equivalheure - Integer.parseInt(heure.getText());
                        heure.setText(null);
                    }
                } else {
                    equivalheure = equivalheure + Integer.parseInt(heure.getText());
                    System.out.println("aqui " + equivalheure + " cab " + cantheure);
                    if (equivalheure <= cantheure) {
                        Mn.ADD_TABLE(mds, vect);
                        saveTemp();
                        int reste = 0;
                        conteur = 0;
                        cantheure = Integer.parseInt(tblmatiere.getValueAt(tblmatiere.getSelectedRow(), 2).toString());
                        System.out.println("cnt " + cantheure);
                        clase = tblmatiere.getValueAt(tblmatiere.getSelectedRow(), 0).toString();
                        materia = tblmatiere.getValueAt(tblmatiere.getSelectedRow(), 1).toString();
                        if (tblhoraire.getRowCount() == 0) {
                            restant.setText(cantheure + "");
                        } else {
                            for (int i = 0; i < tblhoraire.getRowCount(); i++) {
                                if (tblhoraire.getValueAt(i, 1).equals(clase) && tblhoraire.getValueAt(i, 2).equals(materia)) {
                                    int aux = Integer.parseInt(tblhoraire.getValueAt(i, 3).toString());
                                    conteur = conteur + aux;
                                    reste = cantheure - conteur;
                                }
                            }
                            restant.setText(reste + "");
                        }
                        jours.setSelectedItem(null);
                        heure.setText(null);
                    } else {
                        JOptionPane.showMessageDialog(null, "cc" + "Vous avez depasser la quantité d'heure par semaine pour cette matière");
                        equivalheure = equivalheure - Integer.parseInt(heure.getText());
                        heure.setText(null);
                    }
                }
            } else {
                String gr = "Possibilite de croisement avec ==> \n ";
                JOptionPane.showMessageDialog(this, gr + st);
            }
        } else {
            JOptionPane.showMessageDialog(null, "Desole vous devez selectionner une registre dans la table des matieres");
        }
    }//GEN-LAST:event_addMouseClicked

    private void remMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_remMouseClicked
        Mn.REMOVE_TABLE(tblhoraire, mds);
    }//GEN-LAST:event_remMouseClicked

    private void jPanel4MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel4MouseClicked
        Variables.ut = "";
        Employe emp = new Employe(null, rootPaneCheckingEnabled);
        emp.setVisible(true);
        matricule.setText(Variables.ut);
        matriculeKeyReleased(null);
    }//GEN-LAST:event_jPanel4MouseClicked

    private void heureFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_heureFocusLost
        int conteur = 0;
        int reste = 0;
        String classs = null;
        String matye = null;
        int lheure = 0;
        try {
            classs = tblmatiere.getValueAt(tblmatiere.getSelectedRow(), 0).toString();
            matye = tblmatiere.getValueAt(tblmatiere.getSelectedRow(), 1).toString();
            lheure = Integer.parseInt(tblmatiere.getValueAt(tblmatiere.getSelectedRow(), 2).toString());
        } catch (ArrayIndexOutOfBoundsException e) {
        }

        if (tblhoraire.getRowCount() == 0) {
            try {
                conteur = conteur + Integer.parseInt(heure.getText());
            } catch (NumberFormatException e) {
            }

        } else {
            for (int i = 0; i < tblhoraire.getRowCount(); i++) {
                if (tblhoraire.getValueAt(i, 1).equals(classs) && tblhoraire.getValueAt(i, 2).equals(matye)) {
                    int aux = Integer.parseInt(tblhoraire.getValueAt(i, 3).toString());
                    conteur = conteur + aux;
                    reste = lheure - conteur;
                }
            }
            try {
                conteur = conteur + Integer.parseInt(heure.getText());
                if (conteur <= lheure) {
                    conteur = conteur + Integer.parseInt(heure.getText());
                } else {
                    JOptionPane.showMessageDialog(null, "Desole, il reste seulement " + reste + " heures par semaine pour ce professeur.");
                }

            } catch (NumberFormatException e) {
            }

        }
    }//GEN-LAST:event_heureFocusLost

    private void heureActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_heureActionPerformed
        addMouseClicked(null);
    }//GEN-LAST:event_heureActionPerformed

    private void joursItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_joursItemStateChanged
        if (jours.getSelectedIndex() > 0) {
            heure.setEnabled(true);
            jSpinner1.setEnabled(true);
            jSpinner2.setEnabled(true);
            add.setEnabled(true);
            rem.setEnabled(true);
        } else {
            heure.setEnabled(false);
            jSpinner1.setEnabled(false);
            jSpinner2.setEnabled(false);
            add.setEnabled(false);
            rem.setEnabled(false);
        }
    }//GEN-LAST:event_joursItemStateChanged

    private void nouveauActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nouveauActionPerformed
        Nettoyer();
    }//GEN-LAST:event_nouveauActionPerformed

    private void matriculeKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_matriculeKeyReleased
         matricule.setText(matricule.getText().toUpperCase());
        String query = "select nom,prenom from show_employes where matricule = '" + matricule.getText() + "' ";
        System.out.println("hmm "+query);
        ResultSet rs = Mn.SEARCHDATA(query);
        try {
            while (rs.next()) {
                nom.setText(rs.getString("nom") + " " + rs.getString("prenom") +"   ");
                String sesa = "select description from type_employer te join employer_vs_type et on(te.idtype_employer = et.idtype_employer)\n " +
" where et.matricule='"+matricule.getText().trim()+"' and description = 'Professeur' group by description";
                System.out.println("SESSA : "+sesa);
                ResultSet rss = Mn.SEARCHDATA(sesa);
                if (rss.next()) {
                    if (rss.getString("description").equals("Professeur")) {
                        mesajel.setVisible(false);
                    } else {
                        nom.setText(rs.getString("nom") + " " + rs.getString("prenom"));
                        mesajel.setVisible(true);
                    }
                } else {
                    nom.setText(rs.getString("nom") + " " + rs.getString("prenom"));
                    mesajel.setVisible(true);
                }

            }
        } catch (SQLException ex) {
            Logger.getLogger(Professeur.class.getName()).log(Level.SEVERE, null, ex);
        }
        RemplirCours();
        RemplirHoraire();
    }//GEN-LAST:event_matriculeKeyReleased

    private void matriculeKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_matriculeKeyTyped
        Mn.ONLYNUMBERS(evt);
        matricule.requestFocus();
    }//GEN-LAST:event_matriculeKeyTyped

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(HoraireForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(HoraireForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(HoraireForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(HoraireForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new HoraireForm().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel add;
    private javax.swing.JLabel clas;
    private javax.swing.JMenuItem effacer;
    private javax.swing.JMenuItem enregistrer;
    private javax.swing.JTextField heure;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem4;
    private javax.swing.JMenuItem jMenuItem5;
    private javax.swing.JMenuItem jMenuItem6;
    private javax.swing.JMenuItem jMenuItem7;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel11;
    private javax.swing.JPanel jPanel12;
    private javax.swing.JPanel jPanel13;
    private javax.swing.JPanel jPanel14;
    private javax.swing.JPanel jPanel15;
    private javax.swing.JPanel jPanel16;
    private javax.swing.JPanel jPanel17;
    private javax.swing.JPanel jPanel18;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JSpinner jSpinner1;
    private javax.swing.JSpinner jSpinner2;
    private javax.swing.JComboBox jours;
    private javax.swing.JLabel mat;
    private javax.swing.JFormattedTextField matricule;
    private javax.swing.JLabel mesajel;
    private javax.swing.JLabel nom;
    private javax.swing.JMenuItem nouveau;
    private javax.swing.JPanel rem;
    private javax.swing.JLabel restant;
    private javax.swing.JLabel semaine;
    private javax.swing.JTable tblhoraire;
    private javax.swing.JTable tblmatiere;
    // End of variables declaration//GEN-END:variables
}
