/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Forms;

import Methods.Manipulation;
import Methods.Theme;
import Methods.Variables;
import Start.Personaliser;
import com.ezware.oxbow.swingbits.table.filter.TableRowFilterSupport;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.EventQueue;
import java.awt.event.KeyEvent;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BorderFactory;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;

/**
 *
 * @author josephandyfeidje
 */
public class MatiereForm extends javax.swing.JFrame {
Manipulation Mn = new Manipulation();
    Theme th = new Theme();
    String[][] data = {};
    String[] head = {" Matières", " Sur", "Status"};
    String[] heads = {" Matières Composante", " Sur"};
    DefaultTableModel md = new DefaultTableModel(data, head);
    DefaultTableModel mds = new DefaultTableModel(data, heads);
    JTextField code = new JTextField();
    int contador = 0;
    int kalkil = 0;
    
    
    public MatiereForm() {
        initComponents();
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setLocationRelativeTo(this);
        isSystemSet();
        setSize(1053, 577);
        this.setTitle("Scolaris: " + Variables.Empresa + " : Registre des Matières");
        String query = "select * from equivalence where institution='" + Variables.Empresa + "' group by valeur";
        System.out.println("cd " + query);
        Mn.FULLCOMBO(query, equivalence, "valeur");
        nouveau.doClick();
        tblmatiere.setModel(md);
        tblmatiere1.setModel(mds);
        RemplirMatiere();
        //--------------------------------------------------------------
        TableColumnModel cm = tblmatiere.getColumnModel();
        TableColumnModel cm1 = tblmatiere1.getColumnModel();
        cm.getColumn(0).setPreferredWidth(250);
        cm.getColumn(1).setPreferredWidth(15);
        cm.getColumn(2).setPreferredWidth(30);
        cm1.getColumn(0).setPreferredWidth(265);
        cm1.getColumn(1).setPreferredWidth(30);
        composition.setEnabled(false);
        surcomp.setEnabled(false);
        add.setEnabled(false);
        rem.setEnabled(false);
        jLabel5.setEnabled(false);
        jLabel6.setEnabled(false);
        mesaj.setVisible(false);
        mesaj1.setVisible(false);
        mesaj2.setVisible(false);
        
        effacer.setEnabled(false);
        estatusItemStateChanged(null);
        tableMatiereChangeListener();
        TableRowFilterSupport.forTable(tblmatiere).searchable(true).apply();
        TableRowFilterSupport.forTable(tblmatiere1).searchable(true).apply();
        recherche.setName("description");
        setSize(1005, 700);
    }
    
    private boolean isSystemSet(){
        boolean result = false;
        try {
            
            String sql = "select * from show_matiere ";
            
            ResultSet rs = Mn.Rezulta(sql);
            if(rs.absolute(1)){
                result = true;
            }
            else{
                JOptionPane.showMessageDialog(rootPane, "System is not Set for use please go to Outils ==> Personaliser and fill the required field");
                this.dispose();
                new Personaliser().show();
            }
        } catch (SQLException ex) {
            Logger.getLogger(Matiere.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }
    
    private void  autoCompleteEvent(KeyEvent evt,final JTextField textfield){
        switch(evt.getKeyCode()){
            case KeyEvent.VK_BACK_SPACE:
                break;
                case KeyEvent.VK_ENTER:
                    break;
                    default:
                        EventQueue.invokeLater(new Runnable() {

            @Override
            public void run() {
            String txt = textfield.getText();
                Mn.autoComplete(txt,"matiere",textfield.getName(),textfield);
            }
        });
        }
    }
    
    private void tableMatiereChangeListener(){
            tblmatiere.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
            if(tblmatiere.getRowCount() > 0){ 
            Mn.removeDatosOntable(mds);
            setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
            String lestado = "";
            matiere.setText(tblmatiere.getValueAt(tblmatiere.getSelectedRow(), 0).toString());
            equivalence.setSelectedItem(tblmatiere.getValueAt(tblmatiere.getSelectedRow(), 1).toString());
            code.setText(String.valueOf(Mn.SEARCHCODE("code_matiere", "description", Mn.illegalCharacterOnString(matiere.getText()), "matiere")));
            lestado = tblmatiere.getValueAt(tblmatiere.getSelectedRow(), 2).toString();
            int aa = 0;
            if(equivalence.getSelectedIndex() >0 ){
            aa = Integer.parseInt(equivalence.getSelectedItem().toString());
            }
            String query = "select * from show_matiere_compose where matiere='" + Mn.illegalCharacterOnString(matiere.getText()) + "'";
            ResultSet rs = Mn.SEARCHDATA(query);
            try {
                if (rs.next()) {
                    String query1 = "select * from show_matiere_compose where matiere='" + Mn.illegalCharacterOnString(matiere.getText()) + "'";
                    rs = Mn.SEARCHDATA(query1);
                    mds.setRowCount(0);
                    while (rs.next()) {
                        String[] vect = {rs.getString("compose"), rs.getString("sur")};
                        Mn.ADD_TABLE(mds, vect);
                    }
                    for (int i = 0; i < tblmatiere1.getRowCount(); i++) {
                        kalkil = kalkil + Integer.parseInt(tblmatiere1.getValueAt(i, 1).toString());
                    }
                    if (aa == kalkil) {
                        rem.setEnabled(true);
                        mesaj.setVisible(true);
                        mesaj1.setVisible(true);
                        mesaj2.setVisible(true);                        
                        estatus.setEnabled(false);
                        kalkil = 0;
                    } else {
                        mesaj.setVisible(false);
                        mesaj1.setVisible(false);
                        mesaj2.setVisible(false);                        
                        estatus.setEnabled(true);
                        estatus.setSelected(true);                        
                    }
                } else {
                    rem.setEnabled(false);
                    mesaj.setVisible(false);
                    mesaj1.setVisible(false);
                    mesaj2.setVisible(false);                    
                    estatus.setEnabled(true);
                    estatus.setSelected(false);
                }
                
            } catch (SQLException ex) {
                Logger.getLogger(Matiere.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        }
            }
        });
        setCursor(Cursor.getDefaultCursor());
        
    }
    
    private void Nettoyer() {
        matiere.setText(null);
        estatus.setSelected(false);
        estatus.setSelected(false);
        mds.setRowCount(0);
        rem.setEnabled(false);
        mesaj.setVisible(false);
        mesaj1.setVisible(false);
        mesaj2.setVisible(false);
    }
    
    private void RemplirMatiere() {
        setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        String query = "select * from show_matiere order by matiere asc";
        Mn.FULLTABLE(query, tblmatiere, md);
        setCursor(Cursor.getDefaultCursor());
    }
    
    private String ReturnIstitution() {
        setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        String res = "";
        String query = "select nom from institution";
        ResultSet rs = Mn.SEARCHDATA(query);
        try {
            if (rs.first()) {
                res = rs.getString("nom");
            }
        } catch (SQLException ex) {
            Logger.getLogger(Eleves.class.getName()).log(Level.SEVERE, null, ex);
        }
        setCursor(Cursor.getDefaultCursor());
        return res;
    }
    
    private void Insert_compose() {
        
//        String sql = "delete from matiere_compose where matiere = '"+Mn.illegalCharacterOnString(matiere.getText().trim())+"' ";
//        Mn.MANIPULEDB(sql);
        int as =0;
        String sql = "Select code_matiere from matiere where description = '"+matiere.getText().trim()+"' ";
        ResultSet rs = Mn.Rezulta(sql);
        try {
            if(rs.last()){
                as = rs.getInt("code_matiere");
            }
        } catch (SQLException ex) {
            Logger.getLogger(Matiere.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        for (int i = 0; i < tblmatiere1.getRowCount(); i++) {
            setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
            
            String query = "call Insert_matiere_compose("+as+",'" + Mn.illegalCharacterOnString(tblmatiere1.getValueAt(i, 0).toString()) + "'"
                    + "," + tblmatiere1.getValueAt(i, 1).toString() + ")";
            Mn.MANIPULEDB(query);
        }
        setCursor(Cursor.getDefaultCursor());
    }


    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jTextField4 = new javax.swing.JTextField();
        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jLabel5_ = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        matiere = new javax.swing.JTextField();
        jCheckBox1 = new javax.swing.JCheckBox();
        jPanel4 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        recherche = new javax.swing.JTextField();
        jPanel5 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        equivalence = new javax.swing.JComboBox();
        estatus = new javax.swing.JCheckBox();
        jLabel5 = new javax.swing.JLabel();
        composition = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        surcomp = new javax.swing.JTextField();
        jPanel18 = new javax.swing.JPanel();
        add = new javax.swing.JPanel();
        jLabel14 = new javax.swing.JLabel();
        rem = new javax.swing.JPanel();
        jLabel16 = new javax.swing.JLabel();
        jPanel9 = new javax.swing.JPanel();
        jPanel6 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblmatiere = new javax.swing.JTable(){
            public boolean isCellEditable(int rowIndex, int colIndex){
                return false;
            }
        };
        jPanel7 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tblmatiere1 = new javax.swing.JTable(){
            public boolean isCellEditable(int rowIndex, int colIndex){
                return false;
            }
        };
        jPanel8 = new javax.swing.JPanel();
        mesaj = new javax.swing.JLabel();
        mesaj1 = new javax.swing.JLabel();
        mesaj2 = new javax.swing.JLabel();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        enregistrer = new javax.swing.JMenuItem();
        nouveau = new javax.swing.JMenuItem();
        effacer = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        jMenuItem4 = new javax.swing.JMenuItem();
        modifier = new javax.swing.JMenuItem();
        jMenuItem6 = new javax.swing.JMenuItem();
        jMenuItem7 = new javax.swing.JMenuItem();

        jTextField4.setText("jTextField4");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(new java.awt.CardLayout());

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));

        jPanel2.setBackground(new java.awt.Color(87, 148, 210));
        jPanel2.setBorder(new org.edisoncor.gui.util.DropShadowBorder());

        jLabel5_.setBackground(new java.awt.Color(255, 255, 255));
        jLabel5_.setFont(new java.awt.Font("Lucida Grande", 1, 18)); // NOI18N
        jLabel5_.setForeground(new java.awt.Color(255, 255, 255));
        jLabel5_.setText("Registre de Matière(s)");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel5_)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel5_, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(26, Short.MAX_VALUE))
        );

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));

        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel1.setText("Matière : ");

        matiere.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                matiereActionPerformed(evt);
            }
        });
        matiere.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                matiereKeyReleased(evt);
            }
        });

        jCheckBox1.setText("Matière Composée");
        jCheckBox1.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                jCheckBox1ItemStateChanged(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(matiere, javax.swing.GroupLayout.PREFERRED_SIZE, 335, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jCheckBox1)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(matiere, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jCheckBox1))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel4.setBackground(new java.awt.Color(255, 255, 255));

        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel2.setText("Recherche : ");

        recherche.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                rechercheKeyReleased(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(recherche, javax.swing.GroupLayout.PREFERRED_SIZE, 396, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(recherche, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel5.setBackground(new java.awt.Color(255, 255, 255));

        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel3.setText("Equivalence : ");

        estatus.setText("Status");
        estatus.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                estatusItemStateChanged(evt);
            }
        });
        estatus.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                estatusActionPerformed(evt);
            }
        });

        jLabel5.setText("Composition : ");

        jLabel6.setText("Sur  ");

        surcomp.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                surcompActionPerformed(evt);
            }
        });

        jPanel18.setBackground(new java.awt.Color(153, 255, 255));
        jPanel18.setLayout(new java.awt.GridLayout(1, 0));

        add.setBackground(new java.awt.Color(255, 255, 255));
        add.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                addMouseClicked(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                addMouseExited(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                addMouseEntered(evt);
            }
        });
        add.setLayout(new java.awt.CardLayout());

        jLabel14.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/ButtonFoto/icons8-chevron_down_round.png"))); // NOI18N
        add.add(jLabel14, "card2");

        jPanel18.add(add);

        rem.setBackground(new java.awt.Color(255, 255, 255));
        rem.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                remMouseClicked(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                remMouseExited(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                remMouseEntered(evt);
            }
        });
        rem.setLayout(new java.awt.CardLayout());

        jLabel16.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/ButtonFoto/icons8-circled_chevron_up.png"))); // NOI18N
        rem.add(jLabel16, "card2");

        jPanel18.add(rem);

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(equivalence, javax.swing.GroupLayout.PREFERRED_SIZE, 219, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(estatus)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel5)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(composition, javax.swing.GroupLayout.PREFERRED_SIZE, 186, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel6)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(surcomp, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(218, Short.MAX_VALUE))
            .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel5Layout.createSequentialGroup()
                    .addGap(780, 780, 780)
                    .addComponent(jPanel18, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(164, Short.MAX_VALUE)))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(equivalence, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(estatus)
                    .addComponent(jLabel5)
                    .addComponent(composition, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel6)
                    .addComponent(surcomp, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(7, Short.MAX_VALUE))
            .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel5Layout.createSequentialGroup()
                    .addGap(10, 10, 10)
                    .addComponent(jPanel18, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );

        jPanel9.setBackground(new java.awt.Color(255, 255, 255));
        jPanel9.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jPanel6.setLayout(new java.awt.CardLayout());

        tblmatiere.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tblmatiere);

        jPanel6.add(jScrollPane1, "card2");

        jPanel7.setLayout(new java.awt.CardLayout());

        tblmatiere1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tblmatiere1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblmatiere1MouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(tblmatiere1);

        jPanel7.add(jScrollPane2, "card2");

        jPanel8.setBackground(new java.awt.Color(87, 148, 210));
        jPanel8.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel8.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        mesaj.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        mesaj.setForeground(new java.awt.Color(255, 255, 255));
        mesaj.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        mesaj.setText("On ne peut pas ajouter plus Composition pour cette matière,");
        jPanel8.add(mesaj, new org.netbeans.lib.awtextra.AbsoluteConstraints(2, 2, 452, -1));

        mesaj1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        mesaj1.setForeground(new java.awt.Color(255, 255, 255));
        mesaj1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        mesaj1.setText("Parce que la somme des composition de la table ci dessus est");
        jPanel8.add(mesaj1, new org.netbeans.lib.awtextra.AbsoluteConstraints(2, 21, 452, -1));

        mesaj2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        mesaj2.setForeground(new java.awt.Color(255, 255, 255));
        mesaj2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        mesaj2.setText("égale a son  équivalence.");
        jPanel8.add(mesaj2, new org.netbeans.lib.awtextra.AbsoluteConstraints(2, 42, 452, -1));

        javax.swing.GroupLayout jPanel9Layout = new javax.swing.GroupLayout(jPanel9);
        jPanel9.setLayout(jPanel9Layout);
        jPanel9Layout.setHorizontalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 995, Short.MAX_VALUE)
            .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel9Layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, 524, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(466, Short.MAX_VALUE)))
            .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel9Layout.createSequentialGroup()
                    .addContainerGap(540, Short.MAX_VALUE)
                    .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap()))
            .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel9Layout.createSequentialGroup()
                    .addContainerGap(534, Short.MAX_VALUE)
                    .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap()))
        );
        jPanel9Layout.setVerticalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 417, Short.MAX_VALUE)
            .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel9Layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, 395, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(16, Short.MAX_VALUE)))
            .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel9Layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, 319, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(92, Short.MAX_VALUE)))
            .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel9Layout.createSequentialGroup()
                    .addGap(333, 333, 333)
                    .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, 68, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(16, Short.MAX_VALUE)))
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel1Layout.createSequentialGroup()
                    .addComponent(jPanel9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addContainerGap()))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(447, Short.MAX_VALUE))
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                    .addContainerGap(215, Short.MAX_VALUE)
                    .addComponent(jPanel9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(18, Short.MAX_VALUE)))
        );

        getContentPane().add(jPanel1, "card2");

        jMenu1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/ButtonFoto/icons8-menu.png"))); // NOI18N
        jMenu1.setText("Action");

        enregistrer.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/ButtonFoto/icons8-upload_to_cloud.png"))); // NOI18N
        enregistrer.setText("Enregistrer");
        enregistrer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                enregistrerActionPerformed(evt);
            }
        });
        jMenu1.add(enregistrer);

        nouveau.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/ButtonFoto/icons8-new_by_copy.png"))); // NOI18N
        nouveau.setText("Nouveau");
        nouveau.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nouveauActionPerformed(evt);
            }
        });
        jMenu1.add(nouveau);

        effacer.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/ButtonFoto/icons8-trash.png"))); // NOI18N
        effacer.setText("Effacer");
        effacer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                effacerActionPerformed(evt);
            }
        });
        jMenu1.add(effacer);

        jMenuBar1.add(jMenu1);

        jMenu2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/ButtonFoto/icons8-menu_squared.png"))); // NOI18N
        jMenu2.setText("Edition");

        jMenuItem4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/ButtonFoto/icons8-view_file.png"))); // NOI18N
        jMenuItem4.setText("Consulter");
        jMenu2.add(jMenuItem4);

        modifier.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/ButtonFoto/icons8-edit_file.png"))); // NOI18N
        modifier.setText("Modifier");
        modifier.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                modifierActionPerformed(evt);
            }
        });
        jMenu2.add(modifier);

        jMenuItem6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/ButtonFoto/icons8-new_by_copy.png"))); // NOI18N
        jMenuItem6.setText("Ajouter Matière");
        jMenu2.add(jMenuItem6);

        jMenuItem7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/ButtonFoto/icons8-help.png"))); // NOI18N
        jMenuItem7.setText("Aide");
        jMenu2.add(jMenuItem7);

        jMenuBar1.add(jMenu2);

        setJMenuBar(jMenuBar1);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void estatusActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_estatusActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_estatusActionPerformed

    private void addMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_addMouseExited
        //        jPanel5.setBackground(new Color(255, 255, 255));
        add.setBorder(BorderFactory.createLineBorder(new Color(255, 255,255)));
    }//GEN-LAST:event_addMouseExited

    private void addMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_addMouseEntered
        // [87,148,210]
        //        jPanel5.setBackground(new Color(204, 204, 204));
        add.setBorder(BorderFactory.createLineBorder(new Color(87, 148,210)));
    }//GEN-LAST:event_addMouseEntered

    private void remMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_remMouseExited
        //        jPanel6.setBackground(new Color(255, 255, 255));
        rem.setBorder(BorderFactory.createLineBorder(new Color(255, 255,255)));
    }//GEN-LAST:event_remMouseExited

    private void remMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_remMouseEntered
        //        jPanel6.setBackground(new Color(204, 204, 204));
        rem.setBorder(BorderFactory.createLineBorder(new Color(87, 148,210)));
    }//GEN-LAST:event_remMouseEntered

    private void modifierActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_modifierActionPerformed
        setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        int aa = Integer.parseInt(code.getText());
        String bb = matiere.getText();
        String cc = Mn.SEARCHCODE("code_equivalence", "valeur", equivalence.getSelectedItem().toString(), "equivalence");
        String dd = Mn.INSERT_CHECKEDED(estatus);
        String ee = ReturnIstitution();
        String query = "call Insert_matiere(" + aa + ",'" + Mn.illegalCharacterOnString(bb) + "'," + Mn.illegalCharacterOnString(cc) + ",'" + Mn.illegalCharacterOnString(dd) + "','" + Mn.illegalCharacterOnString(ee) + "')";
        Mn.MODIFIER(query);
        Insert_compose();
        RemplirMatiere();
        nouveau.doClick();
        equivalence.setSelectedItem("Selectionnez");
        setCursor(Cursor.getDefaultCursor());
    }//GEN-LAST:event_modifierActionPerformed

    private void nouveauActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nouveauActionPerformed
        JTextField[] txt = {code, matiere};
        Mn.NEW_BUTTON(txt, "matiere", "code_matiere");
        equivalence.setSelectedItem("Selectionnez");
        Nettoyer();
    }//GEN-LAST:event_nouveauActionPerformed

    private void enregistrerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_enregistrerActionPerformed
        setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        
        if(estatus.getText().equals("Inactive")){
            int rep = JOptionPane.showConfirmDialog(rootPane,"Vous estes sur le point d'enregistrer une matiere inactive Voulez-vous continuer","Avertisement",JOptionPane.YES_NO_OPTION);
            if(rep == JOptionPane.YES_OPTION){
                
        int aa = Integer.parseInt(code.getText());
        String bb = matiere.getText();
        String cc = Mn.SEARCHCODE("code_equivalence", "valeur", equivalence.getSelectedItem().toString(), "equivalence");
        String dd = Mn.INSERT_CHECKEDED(estatus);
        String ee = ReturnIstitution();
        String query = "call Insert_matiere(" + aa + ",'" + Mn.illegalCharacterOnString(bb) + "'," + cc + ",'" + dd + "','" + ee + "')";
        String command = "select description from matiere where description like '%" + Mn.illegalCharacterOnString(matiere.getText()) + "%'";
        ResultSet rs = Mn.SEARCHDATA(command);
        try {
            if (rs.first()) {
                if (rs.getString("description").equals(matiere.getText())) {
                    JOptionPane.showMessageDialog(null, "Il existe deja une matiere avec ce nom,s'il vous plait enregistrer avec un autre nom.");
                }
            } else {
                System.out.println("query " + query);
                if (equivalence.getSelectedIndex() <= 0) {
                    JOptionPane.showMessageDialog(rootPane, "Desolé vous devez selectionnez une equivalence");
                    equivalence.requestFocus();
                    return;
                } else {
                    System.out.println("query "+query);
                    Mn.SAVE_BUTTON(query);
                    Insert_compose();
                    RemplirMatiere();
                    nouveau.doClick();
                    equivalence.setSelectedItem("Selectionnez");
                }
            }            
        } catch (SQLException ex) {
            Logger.getLogger(Matiere.class.getName()).log(Level.SEVERE, null, ex);
        }
        }
        else{
            return;
        }
        }
        else{
            
        int aa = Integer.parseInt(code.getText());
        String bb = matiere.getText();
        String cc = Mn.SEARCHCODE("code_equivalence", "valeur", equivalence.getSelectedItem().toString(), "equivalence");
        String dd = Mn.INSERT_CHECKEDED(estatus);
        String ee = ReturnIstitution();
        String query = "call Insert_matiere(" + aa + ",'" + Mn.illegalCharacterOnString(bb) + "'," + cc + ",'" + dd + "','" + ee + "')";
        String command = "select description from matiere where description like '%" + Mn.illegalCharacterOnString(matiere.getText()) + "%'";
        ResultSet rs = Mn.SEARCHDATA(command);
        try {
            if (rs.first()) {
                if (rs.getString("description").equals(matiere.getText())) {
                    JOptionPane.showMessageDialog(null, "Il existe deja une matiere avec ce nom,s'il vous plait enregistrer avec un autre nom.");
                }
            } else {
                System.out.println("query " + query);
                if (equivalence.getSelectedIndex() <= 0) {
                    JOptionPane.showMessageDialog(rootPane, "Desolé vous devez selectionnez une equivalence");
                    equivalence.requestFocus();
                    return;
                } else {
                    System.out.println("query "+query);
                    Mn.SAVE_BUTTON(query);
                    Insert_compose();
                    RemplirMatiere();
                    nouveau.doClick();
                    equivalence.setSelectedItem("Selectionnez");
                }
            }            
        } catch (SQLException ex) {
            Logger.getLogger(Matiere.class.getName()).log(Level.SEVERE, null, ex);
        }
            
            
        }
        setCursor(Cursor.getDefaultCursor());
    }//GEN-LAST:event_enregistrerActionPerformed

    private void effacerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_effacerActionPerformed
        Mn.DELETE_BUTTON(code, "matiere", "code_matiere");
        String query = "select * from Show_matiere_compose where matiere='" + matiere.getText() + "'";
        ResultSet rs = Mn.SEARCHDATA(query);
        try {
            if (rs.next()) {
                String query1 = "delete from matiere_compose where matiere='" + matiere.getText() + "'";
                Mn.MANIPULEDB(query1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Matiere.class.getName()).log(Level.SEVERE, null, ex);
        }
        RemplirMatiere();
        nouveau.doClick();
    }//GEN-LAST:event_effacerActionPerformed

    private void rechercheKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_rechercheKeyReleased
        autoCompleteEvent(evt, recherche);
        String query = "select * from show_matiere where matiere like '%" + recherche.getText() + "%'order by matiere asc";
        Mn.FULLTABLE(query, tblmatiere, md);
    }//GEN-LAST:event_rechercheKeyReleased

    private void estatusItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_estatusItemStateChanged
        
    }//GEN-LAST:event_estatusItemStateChanged

    private void addMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_addMouseClicked
       setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        
        String[] vect = {composition.getText(), surcomp.getText()};
        String lol = "";
        contador = contador + Integer.parseInt(surcomp.getText().trim());
        
        for (int i = 0; i < tblmatiere1.getRowCount(); i++) {
            if (composition.getText().equals(tblmatiere1.getValueAt(i, 0).toString())) {
                JOptionPane.showMessageDialog(this, "Cette matiere est deja existe dans la table, s'il vous plait veuillez choisir une autre.");
                composition.setText(null);
                surcomp.setText(null);
                lol = "";
            }
        }
        if (!composition.getText().equals("") && tblmatiere1.getRowCount() >= 0) {
            lol = "ajouter";
        }
        if (lol.equals("ajouter")) {
            int aa = 0;
            if(equivalence.getSelectedIndex() > 0){
            aa = Integer.parseInt(equivalence.getSelectedItem().toString());
            }
            if (contador <= aa) {
                Mn.ADD_TABLE(mds, vect);
                composition.setText(null);
                surcomp.setText(null);
                if (contador == aa) {
                    mesaj.setVisible(true);
                    mesaj1.setVisible(true);
                    mesaj2.setVisible(true);
                    estatus.setSelected(false);
                    estatus.setEnabled(false);
                    rem.setEnabled(true);
                }
            } else {
                JOptionPane.showMessageDialog(this, "Cette matiere est deja existe dans la table, s'il vous plait veuillez choisir une autre.");                
                contador = contador - Integer.parseInt(surcomp.getText());
                surcomp.setText(null);
            }
        }
        setCursor(Cursor.getDefaultCursor());
    }//GEN-LAST:event_addMouseClicked

    private void remMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_remMouseClicked
        Mn.REMOVE_TABLE(tblmatiere1, mds);
        contador = contador - Integer.parseInt(surcomp.getText());
        estatus.setEnabled(true);
        estatus.setSelected(true);
        mesaj.setVisible(false);
        mesaj.setVisible(false);
        mesaj.setVisible(false);
    }//GEN-LAST:event_remMouseClicked

    private void tblmatiere1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblmatiere1MouseClicked
        composition.setText(tblmatiere1.getValueAt(tblmatiere1.getSelectedRow(), 0).toString());
        surcomp.setText(tblmatiere1.getValueAt(tblmatiere1.getSelectedRow(), 1).toString());
    }//GEN-LAST:event_tblmatiere1MouseClicked

    private void matiereActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_matiereActionPerformed
        enregistrer.doClick();
    }//GEN-LAST:event_matiereActionPerformed

    private void matiereKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_matiereKeyReleased
        Mn.autoComplete(matiere.getText(), "matiere", "description", matiere);
    }//GEN-LAST:event_matiereKeyReleased

    private void surcompActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_surcompActionPerformed
        addMouseClicked(null);
    }//GEN-LAST:event_surcompActionPerformed

    private void jCheckBox1ItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_jCheckBox1ItemStateChanged
        if (jCheckBox1.isSelected()) {
            composition.setEnabled(true);
            surcomp.setEnabled(true);
            add.setEnabled(true);
            rem.setEnabled(true);
            jLabel5.setEnabled(true);
            jLabel6.setEnabled(true);
        } else {
            composition.setEnabled(false);
            surcomp.setEnabled(false);
            add.setEnabled(false);
            rem.setEnabled(false);
            jLabel5.setEnabled(false);
            jLabel6.setEnabled(false);
        }
    }//GEN-LAST:event_jCheckBox1ItemStateChanged

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MatiereForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MatiereForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MatiereForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MatiereForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new MatiereForm().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel add;
    private javax.swing.JTextField composition;
    private javax.swing.JMenuItem effacer;
    private javax.swing.JMenuItem enregistrer;
    private javax.swing.JComboBox equivalence;
    private javax.swing.JCheckBox estatus;
    private javax.swing.JCheckBox jCheckBox1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel5_;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem4;
    private javax.swing.JMenuItem jMenuItem6;
    private javax.swing.JMenuItem jMenuItem7;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel18;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTextField jTextField4;
    private javax.swing.JTextField matiere;
    private javax.swing.JLabel mesaj;
    private javax.swing.JLabel mesaj1;
    private javax.swing.JLabel mesaj2;
    private javax.swing.JMenuItem modifier;
    private javax.swing.JMenuItem nouveau;
    private javax.swing.JTextField recherche;
    private javax.swing.JPanel rem;
    private javax.swing.JTextField surcomp;
    private javax.swing.JTable tblmatiere;
    private javax.swing.JTable tblmatiere1;
    // End of variables declaration//GEN-END:variables
}
