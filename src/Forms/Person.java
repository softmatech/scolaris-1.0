package Forms;

import Methods.Manipulation;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Image;
import java.awt.Point;
import java.awt.Toolkit;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.Date;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import javax.swing.text.DefaultFormatterFactory;
import javax.swing.text.MaskFormatter;
import Consults.*;
import Methods.Variables;
import javax.swing.ImageIcon;

/**
 *
 * @author josephandyfeidje
 */
public class Person extends javax.swing.JInternalFrame {

    Manipulation Mn = new Manipulation();
    String[][] data = {};
    String[] head = {"Courrier(s)"};
    DefaultTableModel md = new DefaultTableModel(data, head);
    
    String[][] datas = {};
    String[] heads = {"Telephone(s)"};
    DefaultTableModel mds = new DefaultTableModel(datas, heads);
    
    String[][] datass = {};
    String[] headss = {"Nom","Prenom","Telephone(s)"};
    DefaultTableModel mdss = new DefaultTableModel(datass, headss);
    
    String sani=""; String san="";
    int cod = 0;
    /**
     * Creates new form Person
     */
    public Person() {
        initComponents();
        setTitle("Registre de Personnes");
        Mn.FULLCOMBO("select description from etat_civil", etatt, "description");
        Mn.FULLCOMBO("select description from type_document", type_doc, "description");
        Mn.FULLCOMBO("select description from nationalite", nationalite, "description");
        Mn.FULLCOMBO("select description from ville", ville, "description");
        Mn.FULLCOMBO("select description from departement", departement, "description");
        jTable1.setModel(md);
        jTable2.setModel(mds);
        jTable3.setModel(mdss);
        naissance.setLocale(Locale.FRENCH);
        code.setEditable(false); 
        docExpSecure();
        nuevo();
        
        String fichero  = System.getProperty("user.dir")+"/src/Pictures/icons8-detective.png";
        System.out.println("cd "+fichero);
        Image image = Toolkit.getDefaultToolkit().createImage(fichero); 
        Cursor cursor = Toolkit.getDefaultToolkit().createCustomCursor(image, new Point(10,10),"aa"); 
        code.setCursor(cursor);

        try {
            tel.setFormatterFactory(new DefaultFormatterFactory(new MaskFormatter("(###) #### ####")));
            tel1.setFormatterFactory(new DefaultFormatterFactory(new MaskFormatter("(###) #### ####")));
        } catch (ParseException ex) {
            Logger.getLogger(Person.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private void photoIcon(){
        String fichero  = System.getProperty("user.dir")+"/src/Pictures/icons8-user_male_circle_filled.png";
        System.out.println("cd "+fichero);
        Image image = Toolkit.getDefaultToolkit().createImage(fichero); 
        ImageIcon imageicon = new ImageIcon(image);
        
        if(photo.getIcon() == null){
            photo.setIcon(imageicon);
            photo.setText("Double click pour Telecharger une Photo");
        }
    }
    
    private void SanFocus(){
        try {
            String wa = code.getText().trim();
            ResultSet er = Mn.Rezulta("select sang from show_sanguins where code='"+wa+"' ");
            if(er.first()){
                String tu = er.getString("sang");
                if (tu.equals("A+")){
                    ap.setSelected(true);
                }
                else if (tu.equals("A-")){
                    am.setSelected(true);
                }
                else if (tu.equals("B+")){
                    bp.setSelected(true);
                }
                else if (tu.equals("B-")){
                    bm.setSelected(true);
                }
                else if (tu.equals("O+")){
                    op.setSelected(true);
                }
                else if (tu.equals("O-")){
                    om.setSelected(true);
                }
                else if (tu.equals("AB+")){
                    abp.setSelected(true);
                }
                else if (tu.equals("AB-")){
                    abm.setSelected(true);
                }
                else{
                    aut.setSelected(true);
                }
            }   } catch (SQLException ex) {
            Logger.getLogger(Person.class.getName()).log(Level.SEVERE, null, ex);
        }
}
    
    private void ChekeSan(){
        
        if(aut.isSelected()){
            spec.setText("Veuillez specifier SVP");
            spec.setEnabled(true);
            sani="autres";
        }
        else if(am.isSelected()){
            spec.setText("");
            spec.setEnabled(false);
            sani="A-";
        }
        
        else if(ap.isSelected()){
            spec.setText("");
            spec.setEnabled(false);
            sani="A+";
        }
        
        else if(abm.isSelected()){
            spec.setText("");
            spec.setEnabled(false);
            sani="AB-";
        }
        else if(abp.isSelected()){
            spec.setText("");
            spec.setEnabled(false);
            sani="AB+";
        }
        else if(om.isSelected()){
            spec.setText("");
            spec.setEnabled(false);
            sani="O-";
        }
        else if(op.isSelected()){
            spec.setText("");
            spec.setEnabled(false);
            sani="O+";
        }
        else if(bm.isSelected()){
            spec.setText("");
            spec.setEnabled(false);
            sani="B-";
        }
        else if(bp.isSelected()){
            spec.setText("");
            spec.setEnabled(false);
            sani="B+";
        }
        else {
            spec.setText("");
            spec.setEnabled(false);
        }
    }
    
    private void isCodeNull(){
        if(code.getText().trim().isEmpty()){
            code.setText("PerSC#");
        }
    }
    
    private void modificationEnabled(){
        nom.setEnabled(true);
        prenom.setEnabled(true);
        sexe.setEnabled(true);
        nationalite.setEnabled(true);
        etatt.setEnabled(true);
        type_doc.setEnabled(true);
        id.setEnabled(true);
        date_emission.setEnabled(true);
        date_expiration.setEnabled(true);
        lieu.setEnabled(true);
        naissance.setEnabled(true);
        adresse.setEnabled(true);
        email.setEnabled(true);
        tel.setEnabled(true);
        add.setEnabled(true);
        add1.setEnabled(true);
        jTable1.setEnabled(true);
        jTable2.setEnabled(true);
        jTable3.setEnabled(true);
        enregistrer.setEnabled(true);
        
        ville.setEnabled(true);
        departement.setEnabled(true);
        contact_nom.setEnabled(true);
        contact_prenom.setEnabled(true);
        tel1.setEnabled(true);
        add2.setEnabled(true);
        
        ap.setEnabled(true);
        am.setEnabled(true);
        bp.setEnabled(true);
        bm.setEnabled(true);
        op.setEnabled(true);
        om.setEnabled(true);
        abp.setEnabled(true);
        abm.setEnabled(true);
        aut.setEnabled(true);
    }
    
    private void modificationDisabled(){
        nom.setEditable(false);
        prenom.setEditable(false);
        sexe.setEnabled(false);
        nationalite.setEnabled(false);
        etatt.setEnabled(false);
        type_doc.setEnabled(false);
        id.setEditable(false);
        date_emission.setEnabled(false);
        date_expiration.setEnabled(false);
        lieu.setEditable(false);
        naissance.setEnabled(false);
        adresse.setEditable(false);
        email.setEditable(false);
        tel.setEditable(false);
        add.setEnabled(false);
        add1.setEnabled(false);
        jTable1.setEnabled(false);
        jTable2.setEnabled(false);
        jTable3.setEnabled(false);
        enregistrer.setEnabled(false);
        
        ville.setEnabled(false);
        departement.setEnabled(false);
        contact_nom.setEnabled(false);
        contact_prenom.setEnabled(false);
        tel1.setEnabled(false);
        add2.setEnabled(false);
        
        ap.setEnabled(false);
        am.setEnabled(false);
        bp.setEnabled(false);
        bm.setEnabled(false);
        op.setEnabled(false);
        om.setEnabled(false);
        abp.setEnabled(false);
        abm.setEnabled(false);
        aut.setEnabled(false);
    }
    
    private void nuevo(){
        cod = 0;
        isCodeNull();
        nom.setText(null);
        prenom.setText(null);
        sexe.setSelectedIndex(0);
        nationalite.setSelectedIndex(0);
        etatt.setSelectedIndex(0);
        type_doc.setSelectedIndex(0);
        id.setText(null);
        date_emission.setDate(null);
        date_expiration.setDate(null);
        lieu.setText(null);
        naissance.setDate(null);
        adresse.setText(null);
        photo.setIcon(null);
        ville.setSelectedIndex(0);
        departement.setSelectedIndex(0);

        
        Mn.removeDatosOntable(md);
        Mn.removeDatosOntable(mds);
        Mn.removeDatosOntable(mdss);
        photoIcon();
        buttonGroup1.clearSelection();
    }
    
    private void focusLost(){
        nuevo();
        try {
            String sql = "SELECT * FROM tercero_general_view where code = '"+code.getText().trim()+"' ";
            System.out.println(sql);
            ResultSet rs = Mn.Rezulta(sql);
            if(rs.last()){
                modificationDisabled();
                nom.setText(rs.getString("nom"));
                prenom.setText(rs.getString("prenom"));
                sexe.setSelectedItem(rs.getString("sexe"));
                nationalite.setSelectedItem(rs.getString("nationalite"));
                etatt.setSelectedItem(rs.getString("etat_civil"));
                type_doc.setSelectedItem(rs.getString("type_document"));
                id.setText(rs.getString("iddocument"));
                date_emission.setDate(rs.getDate("date_emission"));
                date_expiration.setDate(rs.getDate("date_expiration"));
                lieu.setText(rs.getString("lieu_naissance"));
                naissance.setDate(rs.getDate("date_naissance"));
                adresse.setText(rs.getString("adresse"));
                
                //image
                sql = "select imagecol from show_image where code = '"+code.getText().trim()+"'  ";
                Mn.ShowPhotos(sql, photo);
                
                //email
                sql = "select mail from show_email where code = '"+code.getText().trim()+"' ";
                ResultSet rse = Mn.Rezulta(sql);
                while (rse.next()) {                    
                    String vect[] = {rse.getString("mail")};
                    md.addRow(vect);
                }
                
                //telephones
                sql = "SELECT phone_number FROM show_telephones where code = '"+code.getText().trim()+"' ";
                ResultSet rst = Mn.Rezulta(sql);
                while (rst.next()) {                    
                    String vect[] = {rst.getString("phone_number")};
                    mds.addRow(vect);
                }
                //ville
                sql = "select description from show_tercero_ville where code = '"+code.getText().trim()+"' ";
                System.out.println("tervill "+sql);
                ResultSet rstv = Mn.Rezulta(sql);
                try {
                    if(rstv.last()){
                    if(!rstv.getString("description").isEmpty()){
                    ville.setSelectedItem(rstv.getString("description"));
                    }
                    }
                } catch (NullPointerException e) {
                }
                
                //groupe_sanguins
                SanFocus();
                
                //contact_urgence
                sql = "select * from show_contact_urgence where code  = '"+code.getText().trim()+"' ";
                System.out.println("contact_urgence "+sql);
                ResultSet rscv = Mn.Rezulta(sql);
                while (rscv.next()) {                    
                    String vect[]={rscv.getString("nom"),rscv.getString("prenom"),rscv.getString("telephone")};
                    Mn.ADD_TABLE(mdss, vect);
                }
            }   } catch (SQLException ex) {
            Logger.getLogger(Person.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void save(){
        if(nom.getText().trim().isEmpty()){
            JOptionPane.showMessageDialog(this, "Désolé ce champ ne doit pas être vide");
            jTabbedPane1.setSelectedIndex(0);
            nom.setBackground(Color.red);
            nom.requestFocus();
            return;
        }
        
        if(prenom.getText().trim().isEmpty()){
            JOptionPane.showMessageDialog(this, "Désolé ce champ ne doit pas être vide");
            jTabbedPane1.setSelectedIndex(0);
            prenom.setBackground(Color.red);
            prenom.requestFocus();
            return;
        }
        
        if(sexe.getSelectedIndex()<=0){
            JOptionPane.showMessageDialog(this, "Désolé vous devez selectionner un element valide");
            jTabbedPane1.setSelectedIndex(0);
            sexe.setBackground(Color.red);
            sexe.requestFocus();
            return;
        }
        
        if(nationalite.getSelectedIndex()<=0){
            JOptionPane.showMessageDialog(this, "Désolé vous devez selectionner un element valide");
            jTabbedPane1.setSelectedIndex(0);
            nationalite.setBackground(Color.red);
            nationalite.requestFocus();
            return;
        }
        
        if(etatt.getSelectedIndex()<=0){
            JOptionPane.showMessageDialog(this, "Désolé vous devez selectionner un element valide");
            jTabbedPane1.setSelectedIndex(0);
            etatt.setBackground(Color.red);
            etatt.requestFocus();
            return;
        }
        
        if(type_doc.getSelectedIndex()<=0){
            JOptionPane.showMessageDialog(this, "Désolé vous devez selectionner un element valide");
            jTabbedPane1.setSelectedIndex(0);
            type_doc.setBackground(Color.red);
            type_doc.requestFocus();
            return;
        }
        
        try {
            if(id.getText().isEmpty() || id.getValue().toString().isEmpty()){
            JOptionPane.showMessageDialog(this, "Désolé ce champ ne doit pas être vide");
            jTabbedPane1.setSelectedIndex(0);
            id.setBackground(Color.red);
            id.requestFocus();
            return;
        }
        } catch (NullPointerException e) {
        }
        
        
        if(lieu.getText().trim().isEmpty()){
            JOptionPane.showMessageDialog(this, "Désolé ce champ ne doit pas être vide");
            jTabbedPane1.setSelectedIndex(0);
            lieu.setBackground(Color.red);
            lieu.requestFocus();
            return;
        }
        
        if(jTable2.getRowCount() == 0){
            JOptionPane.showMessageDialog(this, "Désolé ce champ ne doit pas être vide (Téléphone)");
            jTabbedPane1.setSelectedIndex(0);
            tel.setBackground(Color.red);
            tel.requestFocus();
            return;
        }
        
        if(ville.getSelectedIndex()<=0){
            JOptionPane.showMessageDialog(this, "Désolé vous devez selectionner un element valide");
            jTabbedPane1.setSelectedIndex(1);
            ville.setBackground(Color.red);
            ville.requestFocus();
            return;
        }
        
        if(departement.getSelectedIndex()<=0){
            JOptionPane.showMessageDialog(this, "Désolé vous devez selectionner un element valide");
            jTabbedPane1.setSelectedIndex(1);
            departement.setBackground(Color.red);
            departement.requestFocus();
            return;
        }
        
        if(sani.isEmpty()){
            JOptionPane.showMessageDialog(this, "Désolé vous devez selectionner un groupe sanguins ");
            jTabbedPane1.setSelectedIndex(1);
            ap.requestFocus();
            return;
        }
        
        //proceso de salvar
        
        //tercero
        cod = 0;
        String codter = code.getText().trim();
        String name = nom.getText().trim();
        String prenam = prenom.getText().trim();
        String sql = "call tercero_procedure ("+cod+",'"+codter+"','"+name+"','"+prenam+"') ";
        System.out.println("sql "+sql);
        Mn.MANIPULEDB(sql);
        //select code for tercero
        cod = getTerceroCode(name, prenam);
        //personal_data
        String sx = sexe.getSelectedItem().toString().trim();
        String nais = Mn.dateToMysqlDate(naissance.getDate());
        String adrs = adresse.getText().trim();
        int idnat = Mn.getCodeFromCombo("nationalite", "idnationalite", nationalite);
        int idetat = Mn.getCodeFromCombo("etat_civil", "idetat_civil", etatt);
        String dateNais = Mn.dateToMysqlDate(naissance.getDate());
        String lieuNais = lieu.getText().trim();
        
        sql = "call data_personal_procedure("+cod+",'"+codter+"','"+sx+"','"+nais+"','"+adrs+"','"+idnat+"','"+idetat+"','"+dateNais+"','"+lieuNais+"') ";
        System.out.println("Personal Data "+sql);
        Mn.MANIPULEDB(sql);
        //document
        String iddoc = id.getText().trim();
        int typDoc = Mn.getCodeFromCombo("type_document", "idtype_document", type_doc);
        String date_Em = Mn.dateToMysqlDate(date_emission.getDate());
        String date_Ex = Mn.dateToMysqlDate(date_expiration.getDate());
        
        sql = "call document_procedure ("+cod+",'"+codter+"','"+iddoc+"') ";
        System.out.println("Document "+sql);
        Mn.MANIPULEDB(sql);
        
        sql = "call document_detail_procedure ('"+iddoc+"',"+typDoc+",'"+date_Em+"','"+date_Ex+"') ";
        System.out.println("Detail Document "+sql);
        Mn.MANIPULEDB(sql);
        
        //image
        
        Mn.InsertPhotos(cod, codter, Mn.lp, Mn.longeur, photo);
        //email
        if(jTable1.getRowCount() > 0){
            for (int i = 0; i < jTable1.getRowCount(); i++) {
                sql = "call email_procedure ("+cod+",'"+codter+"','"+jTable1.getValueAt(i, 0).toString().trim()+"') ";
                System.out.println("email "+sql);
                Mn.MANIPULEDB(sql);
            }
        }
        //telephone
        if(jTable2.getRowCount() > 0){
            for (int i = 0; i < jTable2.getRowCount(); i++) {
                sql = "call telephon_procedure ("+cod+",'"+codter+"','"+jTable2.getValueAt(i, 0).toString().trim()+"') ";
                System.out.println("tel "+sql);
                Mn.MANIPULEDB(sql);
            }
        }
        //tercero_ville
        int codvil = Mn.getCodeFromCombo("ville", "idville", ville);
        sql = "call tercero_has_ville_procedure ("+cod+",'"+codter+"','"+codvil+"')";
        System.out.println("vil "+sql);
        Mn.MANIPULEDB(sql);
        //tercero_sanguins
        sql = "call sanguins_procedure ("+cod+",'"+codter+"','"+sani+"')";
        System.out.println("sang "+sql);
        Mn.MANIPULEDB(sql);
        //contact_urgence
        for (int i = 0; i < jTable3.getRowCount(); i++) {
            sql = "call contact_urgence_procedure ("+cod+",'"+codter+"','"+jTable3.getValueAt(i, 0).toString().trim()+"','"+jTable3.getValueAt(i, 1).toString().trim()+"','"+jTable3.getValueAt(i, 2).toString().trim()+"') "; 
            System.out.println("sql "+sql);
            Mn.MANIPULEDB(sql);
        }
        
        nuevo();
    }
    
    private int getTerceroCode(String name,String prename){
        int code = 0;
        String sql = "select code from tercero where nom = '"+name+"' and prenom = '"+prename+"' ";
        System.out.println("getTerceroCode "+sql);
        ResultSet rs = Mn.Rezulta(sql);
        try {
            if(rs.last()){
                code = rs.getInt("code");
            }
        } catch (SQLException ex) {
            Logger.getLogger(Person.class.getName()).log(Level.SEVERE, null, ex);
        }
        return code;
    }
    
    private void naissanceSecure(){
        naissance.getDateEditor().addPropertyChangeListener(new PropertyChangeListener() {

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                if(naissance.getDate() == null){
                    JOptionPane.showMessageDialog(null, "Désolé vous devez inseree une date");
                    naissance.setBackground(Color.red);
            }
            }
        });
    }
    
    private void docEmiSecure(){
        date_emission.getDateEditor().addPropertyChangeListener(new PropertyChangeListener() {

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                if(date_emission.getDate() == null){
                    JOptionPane.showMessageDialog(null, "Désolé vous devez inseree une date");
                    date_emission.setBackground(Color.red);
            }
            }
        });
    }
                
    private void docExpSecure(){
        date_expiration.getDateEditor().addPropertyChangeListener(new PropertyChangeListener() {

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                if(date_expiration.getDate() != null){
                if((date_expiration.getDate().before(Mn.dateMathMonth(new Date(), 6)))){
                JOptionPane.showMessageDialog(rootPane, "Désolé cet Document n'est pas valide il doit avoir au moins 6 mois" );
                date_expiration.setBackground(Color.RED);
                date_expiration.requestFocus();
                }
                }
            }
        });
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPopupMenu1 = new javax.swing.JPopupMenu();
        jMenuItem2 = new javax.swing.JMenuItem();
        buttonGroup1 = new javax.swing.ButtonGroup();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel1 = new javax.swing.JPanel();
        jPanel4 = new javax.swing.JPanel();
        jLabel15 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        jLabel20 = new javax.swing.JLabel();
        jLabel21 = new javax.swing.JLabel();
        jLabel22 = new javax.swing.JLabel();
        jLabel23 = new javax.swing.JLabel();
        jLabel24 = new javax.swing.JLabel();
        jLabel25 = new javax.swing.JLabel();
        jLabel26 = new javax.swing.JLabel();
        jLabel27 = new javax.swing.JLabel();
        jPanel5 = new javax.swing.JPanel();
        code = new org.edisoncor.gui.textField.TextFieldRectBackground();
        nom = new org.edisoncor.gui.textField.TextFieldRectBackground();
        prenom = new org.edisoncor.gui.textField.TextFieldRectBackground();
        sexe = new javax.swing.JComboBox();
        etatt = new javax.swing.JComboBox();
        jPanel7 = new javax.swing.JPanel();
        type_doc = new javax.swing.JComboBox();
        id = new javax.swing.JFormattedTextField();
        date_emission = new com.toedter.calendar.JDateChooser();
        date_expiration = new com.toedter.calendar.JDateChooser();
        jPanel8 = new javax.swing.JPanel();
        lieu = new org.edisoncor.gui.textField.TextFieldRectBackground();
        naissance = new com.toedter.calendar.JDateChooser();
        adresse = new org.edisoncor.gui.textField.TextFieldRectBackground();
        nationalite = new javax.swing.JComboBox();
        jPanel9 = new javax.swing.JPanel();
        email = new org.edisoncor.gui.textField.TextFieldRectBackground();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        add = new javax.swing.JButton();
        jPanel6 = new javax.swing.JPanel();
        photo = new javax.swing.JLabel();
        jPanel11 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTable2 = new javax.swing.JTable();
        add1 = new javax.swing.JButton();
        tel = new javax.swing.JFormattedTextField();
        jPanel2 = new javax.swing.JPanel();
        jPanel10 = new javax.swing.JPanel();
        jLabel28 = new javax.swing.JLabel();
        jLabel29 = new javax.swing.JLabel();
        jPanel12 = new javax.swing.JPanel();
        ville = new javax.swing.JComboBox();
        departement = new javax.swing.JComboBox();
        jPanel13 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        ap = new javax.swing.JCheckBox();
        am = new javax.swing.JCheckBox();
        bp = new javax.swing.JCheckBox();
        bm = new javax.swing.JCheckBox();
        om = new javax.swing.JCheckBox();
        op = new javax.swing.JCheckBox();
        abp = new javax.swing.JCheckBox();
        abm = new javax.swing.JCheckBox();
        aut = new javax.swing.JCheckBox();
        spec = new javax.swing.JTextField();
        jPanel14 = new javax.swing.JPanel();
        contact_nom = new org.edisoncor.gui.textField.TextFieldRectBackground();
        contact_prenom = new org.edisoncor.gui.textField.TextFieldRectBackground();
        tel1 = new javax.swing.JFormattedTextField();
        jScrollPane3 = new javax.swing.JScrollPane();
        jTable3 = new javax.swing.JTable();
        add2 = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        enregistrer = new javax.swing.JMenuItem();
        nouveau = new javax.swing.JMenuItem();
        effacer = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        modifier = new javax.swing.JMenuItem();
        jMenuItem1 = new javax.swing.JMenuItem();

        jMenuItem2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/icons8-up_circular_1.png"))); // NOI18N
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem2ActionPerformed(evt);
            }
        });
        jPopupMenu1.add(jMenuItem2);

        setClosable(true);
        setIconifiable(true);
        getContentPane().setLayout(new javax.swing.BoxLayout(getContentPane(), javax.swing.BoxLayout.LINE_AXIS));

        jTabbedPane1.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jTabbedPane1.setTabPlacement(javax.swing.JTabbedPane.LEFT);

        jPanel4.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel15.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel15.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel15.setText("Code :");

        jLabel16.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel16.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel16.setText("Nom :");

        jLabel17.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel17.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel17.setText("Prenom :");

        jLabel18.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel18.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel18.setText("Sexe :");

        jLabel19.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel19.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel19.setText("Nationalité :");

        jLabel20.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel20.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel20.setText("Etat Civil :");

        jLabel21.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel21.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel21.setText("Type de document :");

        jLabel22.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel22.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel22.setText("Id document :");

        jLabel23.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel23.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel23.setText("Date d'émission :");

        jLabel24.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel24.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel24.setText("Date d'éxpiration :");

        jLabel25.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel25.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel25.setText("Lieu de Naissance :");

        jLabel26.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel26.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel26.setText("Date de Naissance :");

        jLabel27.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel27.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel27.setText("Adresse :");

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel21, javax.swing.GroupLayout.DEFAULT_SIZE, 146, Short.MAX_VALUE)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel15, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel16, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel17, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel18, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel19, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel20, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addComponent(jLabel22, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel23, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel24, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel25, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel26, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel27, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addComponent(jLabel15, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel16, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel17, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel18, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel19, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel20, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(12, 12, 12)
                .addComponent(jLabel21, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel22, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel23, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel24, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(26, 26, 26)
                .addComponent(jLabel25, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel26, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel27, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jPanel5.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        code.setDescripcion("Entrer le nom de l'élève ici");
        code.setName("code"); // NOI18N
        code.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                codeMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                codeMouseEntered(evt);
            }
        });
        code.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                codeActionPerformed(evt);
            }
        });
        code.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                codeKeyReleased(evt);
            }
        });

        nom.setDescripcion("Entrer le nom de l'élève ici");
        nom.setName("prenom"); // NOI18N
        nom.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                nomFocusGained(evt);
            }
        });
        nom.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                nomKeyReleased(evt);
            }
        });

        prenom.setDescripcion("Entrer le nom de l'élève ici");
        prenom.setName("prenom"); // NOI18N
        prenom.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                prenomFocusGained(evt);
            }
        });
        prenom.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                prenomKeyReleased(evt);
            }
        });

        sexe.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        sexe.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Selectionnez", "Féminin", "Masculin", "Autres" }));
        sexe.setName("sexe"); // NOI18N
        sexe.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                sexeItemStateChanged(evt);
            }
        });
        sexe.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                sexeFocusGained(evt);
            }
        });

        etatt.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        etatt.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Selectionnez", "Féminin", "Masculin", "Autres" }));
        etatt.setName("sexe"); // NOI18N
        etatt.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                etattItemStateChanged(evt);
            }
        });
        etatt.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                etattFocusGained(evt);
            }
        });

        jPanel7.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        type_doc.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        type_doc.setName("sexe"); // NOI18N
        type_doc.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                type_docItemStateChanged(evt);
            }
        });
        type_doc.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                type_docFocusGained(evt);
            }
        });

        id.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        id.setName("id"); // NOI18N
        id.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                idFocusGained(evt);
            }
        });
        id.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                idKeyReleased(evt);
            }
        });

        date_emission.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                date_emissionFocusGained(evt);
            }
        });
        date_emission.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                date_emissionMouseClicked(evt);
            }
        });

        date_expiration.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                date_expirationFocusGained(evt);
            }
        });

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(type_doc, 0, 333, Short.MAX_VALUE)
            .addComponent(date_emission, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(id)
            .addComponent(date_expiration, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(type_doc, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(id, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(date_emission, javax.swing.GroupLayout.DEFAULT_SIZE, 34, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(date_expiration, javax.swing.GroupLayout.DEFAULT_SIZE, 34, Short.MAX_VALUE)
                .addContainerGap())
        );

        jPanel8.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        lieu.setDescripcion("Entrer le nom de l'élève ici");
        lieu.setName("prenom"); // NOI18N
        lieu.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                lieuKeyReleased(evt);
            }
        });

        adresse.setDescripcion("Entrer le nom de l'élève ici");
        adresse.setName("prenom"); // NOI18N
        adresse.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                adresseKeyReleased(evt);
            }
        });

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(lieu, javax.swing.GroupLayout.DEFAULT_SIZE, 333, Short.MAX_VALUE)
            .addComponent(naissance, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(adresse, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addComponent(lieu, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(naissance, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(adresse, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        nationalite.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        nationalite.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Selectionnez", "Féminin", "Masculin", "Autres" }));
        nationalite.setName("sexe"); // NOI18N
        nationalite.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                nationaliteItemStateChanged(evt);
            }
        });
        nationalite.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                nationaliteFocusGained(evt);
            }
        });

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(code, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(nom, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(prenom, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(sexe, 0, 337, Short.MAX_VALUE)
            .addComponent(etatt, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(nationalite, 0, 337, Short.MAX_VALUE)
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addComponent(code, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(nom, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(prenom, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(sexe, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(nationalite, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(etatt, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        jPanel9.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        email.setDescripcion("Entrer le nom de l'élève ici");
        email.setName("prenom"); // NOI18N
        email.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                emailActionPerformed(evt);
            }
        });
        email.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                emailKeyReleased(evt);
            }
        });

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null},
                {null},
                {null},
                {null}
            },
            new String [] {
                "Title 1"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable1.setComponentPopupMenu(jPopupMenu1);
        jScrollPane1.setViewportView(jTable1);

        add.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/icons8-circled_down.png"))); // NOI18N
        add.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel9Layout = new javax.swing.GroupLayout(jPanel9);
        jPanel9.setLayout(jPanel9Layout);
        jPanel9Layout.setHorizontalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addComponent(email, javax.swing.GroupLayout.PREFERRED_SIZE, 272, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(add, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel9Layout.setVerticalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(email, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(add, javax.swing.GroupLayout.DEFAULT_SIZE, 32, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 94, Short.MAX_VALUE))
        );

        jPanel6.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        jPanel6.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        photo.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        photo.setText("Telecharger une photo");
        photo.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        photo.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                photoMouseClicked(evt);
            }
        });
        jPanel6.add(photo, new org.netbeans.lib.awtextra.AbsoluteConstraints(3, 3, 330, 223));

        jPanel11.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jTable2.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null},
                {null},
                {null},
                {null}
            },
            new String [] {
                "Title 1"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane2.setViewportView(jTable2);

        add1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/icons8-circled_down.png"))); // NOI18N
        add1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                add1ActionPerformed(evt);
            }
        });

        tel.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                telFocusLost(evt);
            }
        });
        tel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                telActionPerformed(evt);
            }
        });
        tel.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                telKeyReleased(evt);
            }
        });

        javax.swing.GroupLayout jPanel11Layout = new javax.swing.GroupLayout(jPanel11);
        jPanel11.setLayout(jPanel11Layout);
        jPanel11Layout.setHorizontalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
            .addGroup(jPanel11Layout.createSequentialGroup()
                .addComponent(tel, javax.swing.GroupLayout.PREFERRED_SIZE, 272, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(add1, javax.swing.GroupLayout.PREFERRED_SIZE, 42, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel11Layout.setVerticalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel11Layout.createSequentialGroup()
                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(add1, javax.swing.GroupLayout.DEFAULT_SIZE, 32, Short.MAX_VALUE)
                    .addComponent(tel))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 94, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel11, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Données Personnelles", jPanel1);

        jPanel10.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel28.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel28.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel28.setText("Ville(Adresse) : ");

        jLabel29.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel29.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel29.setText("Departement : ");

        javax.swing.GroupLayout jPanel10Layout = new javax.swing.GroupLayout(jPanel10);
        jPanel10.setLayout(jPanel10Layout);
        jPanel10Layout.setHorizontalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel28, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jLabel29, javax.swing.GroupLayout.DEFAULT_SIZE, 156, Short.MAX_VALUE)
        );
        jPanel10Layout.setVerticalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel10Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel28, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel29, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jPanel12.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        ville.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        ville.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Selectionnez", "Féminin", "Masculin", "Autres" }));
        ville.setName("sexe"); // NOI18N
        ville.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                villeItemStateChanged(evt);
            }
        });
        ville.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                villeFocusGained(evt);
            }
        });

        departement.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        departement.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Selectionnez", "Féminin", "Masculin", "Autres" }));
        departement.setName("sexe"); // NOI18N
        departement.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                departementItemStateChanged(evt);
            }
        });
        departement.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                departementFocusGained(evt);
            }
        });

        javax.swing.GroupLayout jPanel12Layout = new javax.swing.GroupLayout(jPanel12);
        jPanel12.setLayout(jPanel12Layout);
        jPanel12Layout.setHorizontalGroup(
            jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel12Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(ville, javax.swing.GroupLayout.PREFERRED_SIZE, 251, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(departement, javax.swing.GroupLayout.PREFERRED_SIZE, 251, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel12Layout.setVerticalGroup(
            jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel12Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(ville, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(departement, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel13.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel1.setText("            Groupes Sanguins");

        buttonGroup1.add(ap);
        ap.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        ap.setText("A+");
        ap.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                apItemStateChanged(evt);
            }
        });

        buttonGroup1.add(am);
        am.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        am.setText("A-");
        am.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                amItemStateChanged(evt);
            }
        });
        am.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                amActionPerformed(evt);
            }
        });

        buttonGroup1.add(bp);
        bp.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        bp.setText("B+");
        bp.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                bpItemStateChanged(evt);
            }
        });
        bp.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bpActionPerformed(evt);
            }
        });

        buttonGroup1.add(bm);
        bm.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        bm.setText("B-");
        bm.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                bmItemStateChanged(evt);
            }
        });

        buttonGroup1.add(om);
        om.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        om.setText("O-");
        om.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                omItemStateChanged(evt);
            }
        });

        buttonGroup1.add(op);
        op.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        op.setText("O+");
        op.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                opItemStateChanged(evt);
            }
        });

        buttonGroup1.add(abp);
        abp.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        abp.setText("AB+");
        abp.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                abpItemStateChanged(evt);
            }
        });

        buttonGroup1.add(abm);
        abm.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        abm.setText("AB-");
        abm.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                abmItemStateChanged(evt);
            }
        });

        buttonGroup1.add(aut);
        aut.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        aut.setText("Autres");
        aut.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                autItemStateChanged(evt);
            }
        });

        javax.swing.GroupLayout jPanel13Layout = new javax.swing.GroupLayout(jPanel13);
        jPanel13.setLayout(jPanel13Layout);
        jPanel13Layout.setHorizontalGroup(
            jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel13Layout.createSequentialGroup()
                .addContainerGap(45, Short.MAX_VALUE)
                .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel13Layout.createSequentialGroup()
                        .addComponent(aut)
                        .addGap(41, 41, 41)
                        .addComponent(spec, javax.swing.GroupLayout.PREFERRED_SIZE, 129, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel13Layout.createSequentialGroup()
                            .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(abp)
                                .addComponent(bp)
                                .addComponent(op))
                            .addGap(41, 41, 41)
                            .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(om)
                                .addComponent(abm)
                                .addComponent(bm)))
                        .addComponent(ap)
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 235, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel13Layout.createSequentialGroup()
                            .addComponent(am)
                            .addGap(101, 101, 101))))
                .addGap(40, 40, 40))
        );
        jPanel13Layout.setVerticalGroup(
            jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel13Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(ap)
                    .addComponent(am))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(bm)
                    .addComponent(bp, javax.swing.GroupLayout.Alignment.TRAILING))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(op)
                    .addComponent(om))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(abm)
                    .addComponent(abp))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 15, Short.MAX_VALUE)
                .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(spec, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(aut))
                .addContainerGap())
        );

        jPanel14.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        contact_nom.setDescripcion("Entrer le nom de l'élève ici");
        contact_nom.setName("prenom"); // NOI18N
        contact_nom.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                contact_nomFocusGained(evt);
            }
        });
        contact_nom.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                contact_nomKeyReleased(evt);
            }
        });

        contact_prenom.setDescripcion("Entrer le nom de l'élève ici");
        contact_prenom.setName("prenom"); // NOI18N
        contact_prenom.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                contact_prenomFocusGained(evt);
            }
        });
        contact_prenom.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                contact_prenomKeyReleased(evt);
            }
        });

        tel1.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                tel1FocusLost(evt);
            }
        });
        tel1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tel1ActionPerformed(evt);
            }
        });
        tel1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                tel1KeyReleased(evt);
            }
        });

        jTable3.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane3.setViewportView(jTable3);

        add2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/icons8-circled_down.png"))); // NOI18N
        add2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                add2ActionPerformed(evt);
            }
        });

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel2.setText("Contact en cas d'urgence ");

        javax.swing.GroupLayout jPanel14Layout = new javax.swing.GroupLayout(jPanel14);
        jPanel14.setLayout(jPanel14Layout);
        jPanel14Layout.setHorizontalGroup(
            jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane3)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel14Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addGroup(jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel14Layout.createSequentialGroup()
                        .addComponent(contact_nom, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(contact_prenom, javax.swing.GroupLayout.PREFERRED_SIZE, 151, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(tel1, javax.swing.GroupLayout.PREFERRED_SIZE, 219, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(add2, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap())
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel14Layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addGap(181, 181, 181))))
        );
        jPanel14Layout.setVerticalGroup(
            jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel14Layout.createSequentialGroup()
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(contact_nom, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(contact_prenom, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(tel1, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(add2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 88, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(24, 24, 24))
        );

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jPanel10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(jPanel14, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel13, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(2, 2, 2)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel13, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jPanel10, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jPanel12, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel14, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(0, 293, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Informations Complémentaires", jPanel2);

        jPanel3.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 899, Short.MAX_VALUE)
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 563, Short.MAX_VALUE)
        );

        jTabbedPane1.addTab("Historiques", jPanel3);

        getContentPane().add(jTabbedPane1);

        jMenuBar1.setBackground(new java.awt.Color(102, 102, 102));

        jMenu1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/fichier.jpg"))); // NOI18N
        jMenu1.setText("Action");
        jMenu1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N

        enregistrer.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_E, java.awt.event.InputEvent.CTRL_MASK));
        enregistrer.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/icons8-save_close.png"))); // NOI18N
        enregistrer.setText("Enrégistrer");
        enregistrer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                enregistrerActionPerformed(evt);
            }
        });
        jMenu1.add(enregistrer);

        nouveau.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_N, java.awt.event.InputEvent.CTRL_MASK));
        nouveau.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/icons8-add_list.png"))); // NOI18N
        nouveau.setText("Nouveau");
        nouveau.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nouveauActionPerformed(evt);
            }
        });
        jMenu1.add(nouveau);

        effacer.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_D, java.awt.event.InputEvent.CTRL_MASK));
        effacer.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/icons8-erase.png"))); // NOI18N
        effacer.setText("Effacer");
        effacer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                effacerActionPerformed(evt);
            }
        });
        jMenu1.add(effacer);

        jMenuBar1.add(jMenu1);

        jMenu2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/editer.jpg"))); // NOI18N
        jMenu2.setText("Edition");
        jMenu2.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N

        modifier.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_M, java.awt.event.InputEvent.CTRL_MASK));
        modifier.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/icons8-refresh.png"))); // NOI18N
        modifier.setText("Modifier");
        modifier.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                modifierActionPerformed(evt);
            }
        });
        jMenu2.add(modifier);

        jMenuItem1.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F1, 0));
        jMenuItem1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/icons8-help.png"))); // NOI18N
        jMenuItem1.setText("Aide");
        jMenu2.add(jMenuItem1);

        jMenuBar1.add(jMenu2);

        setJMenuBar(jMenuBar1);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void enregistrerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_enregistrerActionPerformed
        save();
    }//GEN-LAST:event_enregistrerActionPerformed

    private void nouveauActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nouveauActionPerformed
        cod = 0;
        code.setText(null);
        nuevo();
        modificationEnabled();
    }//GEN-LAST:event_nouveauActionPerformed

    private void effacerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_effacerActionPerformed
        
    }//GEN-LAST:event_effacerActionPerformed

    private void modifierActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_modifierActionPerformed
        
    }//GEN-LAST:event_modifierActionPerformed

    private void codeKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_codeKeyReleased
    }//GEN-LAST:event_codeKeyReleased

    private void nomKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_nomKeyReleased
        nom.setBackground(Color.white);
    }//GEN-LAST:event_nomKeyReleased

    private void prenomKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_prenomKeyReleased
        prenom.setBackground(Color.white);
    }//GEN-LAST:event_prenomKeyReleased

    private void type_docItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_type_docItemStateChanged
        type_doc.setBackground(Color.white);
        if(type_doc.getSelectedIndex()>0){
            if(type_doc.getSelectedItem().toString().equals("Carte Electorale")){
                try {
                    id.setValue(null);
                    id.setFormatterFactory(new DefaultFormatterFactory(new MaskFormatter("##-##-##-####-##-#####")));
                    id.requestFocus();
                } catch (ParseException ex) {
                    Logger.getLogger(Person.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            else{
                if (type_doc.getSelectedItem().toString().equals("Passeport")){
                    try {
                        id.setValue(null);
                        id.setFormatterFactory(new DefaultFormatterFactory(new MaskFormatter("AA#######")));
                        id.requestFocus();
                    } catch (ParseException ex) {
                        Logger.getLogger(Person.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                else{
                    try {
                        id.setValue(null);
                        id.setFormatterFactory(new DefaultFormatterFactory(new MaskFormatter("**********************")));
                        id.requestFocus();
                    } catch (ParseException ex) {
                        Logger.getLogger(Person.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        }
    }//GEN-LAST:event_type_docItemStateChanged

    private void lieuKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_lieuKeyReleased
        lieu.setBackground(Color.white);
    }//GEN-LAST:event_lieuKeyReleased

    private void emailKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_emailKeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_emailKeyReleased

    private void photoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_photoMouseClicked
        if(evt.getClickCount() == 2){
            Mn.Photos(photo, code.getText().trim());
        }
    }//GEN-LAST:event_photoMouseClicked

    private void addActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addActionPerformed
        if(Mn.VALIDATEMAIL(email.getText(), email)){
            String[] tex = {email.getText().trim()};
            if(Mn.isElementExistInGrid(jTable1, tex)){
                JOptionPane.showMessageDialog(this, "Cet element existe a été inserer dans la grille");
                return;
            }
            else{
            String vect[]={email.getText().trim()};
                    Mn.ADD_TABLE(md, vect);
                    email.setText(null);
            }
        }
    }//GEN-LAST:event_addActionPerformed

    private void emailActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_emailActionPerformed
        add.doClick();
    }//GEN-LAST:event_emailActionPerformed

    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem2ActionPerformed
         Mn.REMOVE_TABLE(jTable1, md);
    }//GEN-LAST:event_jMenuItem2ActionPerformed

    private void telActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_telActionPerformed
        add1.doClick();
    }//GEN-LAST:event_telActionPerformed

    private void telFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_telFocusLost
        // TODO add your handling code here:
    }//GEN-LAST:event_telFocusLost

    private void telKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_telKeyReleased
        
            
    }//GEN-LAST:event_telKeyReleased

    private void add1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_add1ActionPerformed
        try {
        tel.setBackground(Color.white);
        String vect[]={tel.getText().trim()};
        if(tel.getText().isEmpty()){
            JOptionPane.showMessageDialog(this, "Désolé ce champ ne doit pas être vide");
            tel.setBackground(Color.red);
            tel.requestFocus();
            return;
        }
        else{
            String[] tex = {tel.getText().trim()};
        if(Mn.isElementExistInGrid(jTable2,tex )){
            JOptionPane.showMessageDialog(this, "Cet element existe a été inserer dans la grille");
            return;
        }
        else{
            
            String sql = "select * from tercero_vs_telephones where phone_number = '"+tel.getText().trim()+"' ";
            System.out.println("phone control : "+sql);
            ResultSet rs = Mn.Rezulta(sql);
            if(rs.last()){
                JOptionPane.showMessageDialog(null, "Ce numero de telephone a été deja enregistrer pour une autre personnes");
                tel.setBackground(Color.yellow);
                return;
            }
            else{
                    Mn.ADD_TABLE(mds, vect);
                    tel.setValue(null);
            }
        }
        }
        } catch (NullPointerException e) {
        } catch (SQLException ex) {
            Logger.getLogger(Person.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_add1ActionPerformed

    private void date_emissionMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_date_emissionMouseClicked
        docEmiSecure();
    }//GEN-LAST:event_date_emissionMouseClicked

    private void nomFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_nomFocusGained
    }//GEN-LAST:event_nomFocusGained

    private void prenomFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_prenomFocusGained
    }//GEN-LAST:event_prenomFocusGained

    private void sexeFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_sexeFocusGained
    }//GEN-LAST:event_sexeFocusGained

    private void nationaliteFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_nationaliteFocusGained
    }//GEN-LAST:event_nationaliteFocusGained

    private void etattFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_etattFocusGained
    }//GEN-LAST:event_etattFocusGained

    private void type_docFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_type_docFocusGained
    }//GEN-LAST:event_type_docFocusGained

    private void idFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_idFocusGained
    }//GEN-LAST:event_idFocusGained

    private void date_emissionFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_date_emissionFocusGained
    }//GEN-LAST:event_date_emissionFocusGained

    private void date_expirationFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_date_expirationFocusGained
        // TODO add your handling code here:
    }//GEN-LAST:event_date_expirationFocusGained

    private void sexeItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_sexeItemStateChanged
        sexe.setBackground(Color.white);
    }//GEN-LAST:event_sexeItemStateChanged

    private void nationaliteItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_nationaliteItemStateChanged
        nationalite.setBackground(Color.white);
    }//GEN-LAST:event_nationaliteItemStateChanged

    private void etattItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_etattItemStateChanged
        etatt.setBackground(Color.white);
    }//GEN-LAST:event_etattItemStateChanged

    private void idKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_idKeyReleased
        try {
            id.setBackground(Color.white);
            
            String sql = "select * from document where iddocument = '"+id.getText().trim()+"' ";
            System.out.println("check Doc : "+sql);
            ResultSet rs = Mn.Rezulta(sql);
            if(rs.last()){
                JOptionPane.showMessageDialog(null, "Ce document apartient deja a quelqu'un d'autre pouvez-vous le verifier");
                id.setBackground(Color.yellow);
                return;
            }
        } catch (SQLException ex) {
            Logger.getLogger(Person.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_idKeyReleased

    private void adresseKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_adresseKeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_adresseKeyReleased

    private void codeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_codeActionPerformed
        
    }//GEN-LAST:event_codeActionPerformed

    private void codeMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_codeMouseClicked
        if(evt.getClickCount() == 2){
        consultPerson cp = new consultPerson(null, true);
        cp.show();
        cp.pack();
        code.setText(Variables.code_tercero);
            System.out.println("code "+Variables.code_tercero);
        focusLost();
        }
    }//GEN-LAST:event_codeMouseClicked

    private void codeMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_codeMouseEntered
        // TODO add your handling code here:
    }//GEN-LAST:event_codeMouseEntered

    private void villeItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_villeItemStateChanged
        if(ville.getSelectedIndex() > 0 ){
            try {
                String sql = "select d.description from departement d join ville v on(d.iddepartement = v.iddepartement) where  v.description = '"+Mn.illegalCharacterOnString(ville.getSelectedItem().toString().trim())+"'  ";
                System.out.println("sql "+sql);
                ResultSet rs = Mn.Rezulta(sql);
                if(rs.last()){
                    String dep = rs.getString("description");
                    departement.setSelectedItem(dep);
                }
            } catch (SQLException ex) {
                Logger.getLogger(Person.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }//GEN-LAST:event_villeItemStateChanged

    private void villeFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_villeFocusGained
        // TODO add your handling code here:
    }//GEN-LAST:event_villeFocusGained

    private void departementItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_departementItemStateChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_departementItemStateChanged

    private void departementFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_departementFocusGained
        // TODO add your handling code here:
    }//GEN-LAST:event_departementFocusGained

    private void apItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_apItemStateChanged
        ChekeSan();
    }//GEN-LAST:event_apItemStateChanged

    private void amItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_amItemStateChanged
        ChekeSan();
    }//GEN-LAST:event_amItemStateChanged

    private void amActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_amActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_amActionPerformed

    private void bpItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_bpItemStateChanged
        ChekeSan();
    }//GEN-LAST:event_bpItemStateChanged

    private void bpActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bpActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_bpActionPerformed

    private void bmItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_bmItemStateChanged
        ChekeSan();
    }//GEN-LAST:event_bmItemStateChanged

    private void omItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_omItemStateChanged
        ChekeSan();
    }//GEN-LAST:event_omItemStateChanged

    private void opItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_opItemStateChanged
        ChekeSan();
    }//GEN-LAST:event_opItemStateChanged

    private void abpItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_abpItemStateChanged
        ChekeSan();
    }//GEN-LAST:event_abpItemStateChanged

    private void abmItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_abmItemStateChanged
        ChekeSan();
    }//GEN-LAST:event_abmItemStateChanged

    private void autItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_autItemStateChanged
        ChekeSan();
    }//GEN-LAST:event_autItemStateChanged

    private void contact_nomFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_contact_nomFocusGained
        // TODO add your handling code here:
    }//GEN-LAST:event_contact_nomFocusGained

    private void contact_nomKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_contact_nomKeyReleased
        contact_nom.setBackground(Color.white);
    }//GEN-LAST:event_contact_nomKeyReleased

    private void contact_prenomFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_contact_prenomFocusGained
        // TODO add your handling code here:
    }//GEN-LAST:event_contact_prenomFocusGained

    private void contact_prenomKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_contact_prenomKeyReleased
        contact_prenom.setBackground(Color.white);
    }//GEN-LAST:event_contact_prenomKeyReleased

    private void tel1FocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_tel1FocusLost
        // TODO add your handling code here:
    }//GEN-LAST:event_tel1FocusLost

    private void tel1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tel1ActionPerformed
        add2.doClick();
    }//GEN-LAST:event_tel1ActionPerformed

    private void tel1KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tel1KeyReleased
        tel1.setBackground(Color.white);
    }//GEN-LAST:event_tel1KeyReleased

    private void add2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_add2ActionPerformed
        if(contact_nom.getText().trim().isEmpty()){
            JOptionPane.showMessageDialog(null, "Désolé ce champ ne doit pas être vide");
            contact_nom.setBackground(Color.red);
            contact_nom.requestFocus();
            return;
        }
        
        if(contact_prenom.getText().trim().isEmpty()){
            JOptionPane.showMessageDialog(null, "Désolé ce champ ne doit pas être vide");
            contact_prenom.setBackground(Color.red);
            contact_prenom.requestFocus();
            return;
        }
        
        if(tel1.getText().trim().isEmpty()){
            JOptionPane.showMessageDialog(null, "Désolé ce champ ne doit pas être vide");
            tel1.setBackground(Color.red);
            tel1.requestFocus();
            return;
        }
        
        try {
            
        String vect[]={contact_nom.getText().trim(),contact_prenom.getText().trim(),tel1.getText().trim()};
        String[] tex = {tel1.getText().trim()};
        if(Mn.isElementExistInGrid(jTable3, tex)){
            JOptionPane.showMessageDialog(this, "Cet element existe a été inserer dans la grille");
            return;
        }
        else{
                    Mn.ADD_TABLE(mdss, vect);
                    contact_nom.setText(null);
                    contact_prenom.setText(null);
                    tel1.setText(null);
        }
        } catch (NullPointerException e) {
        }
    }//GEN-LAST:event_add2ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JCheckBox abm;
    private javax.swing.JCheckBox abp;
    private javax.swing.JButton add;
    private javax.swing.JButton add1;
    private javax.swing.JButton add2;
    private org.edisoncor.gui.textField.TextFieldRectBackground adresse;
    private javax.swing.JCheckBox am;
    private javax.swing.JCheckBox ap;
    private javax.swing.JCheckBox aut;
    private javax.swing.JCheckBox bm;
    private javax.swing.JCheckBox bp;
    private javax.swing.ButtonGroup buttonGroup1;
    private org.edisoncor.gui.textField.TextFieldRectBackground code;
    private org.edisoncor.gui.textField.TextFieldRectBackground contact_nom;
    private org.edisoncor.gui.textField.TextFieldRectBackground contact_prenom;
    private com.toedter.calendar.JDateChooser date_emission;
    private com.toedter.calendar.JDateChooser date_expiration;
    private javax.swing.JComboBox departement;
    private javax.swing.JMenuItem effacer;
    private org.edisoncor.gui.textField.TextFieldRectBackground email;
    private javax.swing.JMenuItem enregistrer;
    private javax.swing.JComboBox etatt;
    private javax.swing.JFormattedTextField id;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel11;
    private javax.swing.JPanel jPanel12;
    private javax.swing.JPanel jPanel13;
    private javax.swing.JPanel jPanel14;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JPopupMenu jPopupMenu1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JTable jTable1;
    private javax.swing.JTable jTable2;
    private javax.swing.JTable jTable3;
    private org.edisoncor.gui.textField.TextFieldRectBackground lieu;
    private javax.swing.JMenuItem modifier;
    private com.toedter.calendar.JDateChooser naissance;
    private javax.swing.JComboBox nationalite;
    private org.edisoncor.gui.textField.TextFieldRectBackground nom;
    private javax.swing.JMenuItem nouveau;
    private javax.swing.JCheckBox om;
    private javax.swing.JCheckBox op;
    private javax.swing.JLabel photo;
    private org.edisoncor.gui.textField.TextFieldRectBackground prenom;
    private javax.swing.JComboBox sexe;
    private javax.swing.JTextField spec;
    private javax.swing.JFormattedTextField tel;
    private javax.swing.JFormattedTextField tel1;
    private javax.swing.JComboBox type_doc;
    private javax.swing.JComboBox ville;
    // End of variables declaration//GEN-END:variables
}
