/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Forms;

import Methods.GenerateMatricule;
import Methods.Manipulation;
import Methods.Security;
import Methods.Theme;
import Methods.Variables;
import Processus.Entrer_note;
import com.ezware.oxbow.swingbits.table.filter.TableRowFilterSupport;
import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.EventQueue;
import java.awt.Image;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumnModel;
import org.edisoncor.gui.textField.TextFieldRectBackground;

/**
 *
 * @author josephandyfeidje
 */
public class EmployerForm extends javax.swing.JFrame {
Manipulation Mn = new Manipulation();
    Theme th = new Theme();
    GenerateMatricule gm = new GenerateMatricule();
    Security sc = new Security();
    int codemp = 0;
    String[][] data = {};
    String[] head = {"Nom", " Prénom", "Document", "Matricule", "Adresse", "Téléphone", "E-mail", "Status"};
    String[] heads = {"Type Employer", "Status"};
    String[] champ = {"estatuses", "code_employer"};
    DefaultTableModel md = new DefaultTableModel(data, head);
    DefaultTableModel mds = new DefaultTableModel(data, heads);
    TableCellRenderer render = new TableCellRenderer() {

        public Component getTableCellRendererComponent(JTable table, Object value, boolean idSelected, boolean hasFocus, int row, int column) {
            JLabel lbl = new JLabel(value == null ? "" : value.toString());
            if (column == 3 && row >= 0) {
                lbl.setHorizontalAlignment(SwingConstants.LEFT);
                lbl.setOpaque(true);
                lbl.setBackground(Color.green);
            }

            if (column == 7 && value.toString().equals("Inactif") && row >= 0) {
                lbl.setHorizontalAlignment(SwingConstants.LEFT);
                lbl.setOpaque(true);
                lbl.setForeground(Color.red);
            }
            return lbl;
        }
    };
    JTextField coode = new JTextField();
    
    public EmployerForm() {
        initComponents();
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        TableRowFilterSupport.forTable(tbleleve).searchable(true).apply();
        tbleleve.getSelectionModel().setSelectionMode(ListSelectionModel.SINGLE_SELECTION );
        setSize(1280, 613);
        jPanel9.setSize(261, 23);
        this.setTitle("Scolaris: " + Variables.Empresa + " : Registre d'Employer");
        code.setEditable(false);
        nouveau.doClick();
        matricule.setText(gm.MatriculeProf());
        matricule.setEditable(false);
        tbleleve.setModel(md);
        tblemp.setModel(mds);
        Consulter();
        Mn.FULLCOMBO("select description from type_document", tpdocument, "description");
        Mn.FULLCOMBO("select description from type_employer", tpemployer, "description");
        //---------------------------------------------------------------------
        TableColumnModel cm = tbleleve.getColumnModel();
        cm.getColumn(0).setPreferredWidth(50);
        cm.getColumn(1).setPreferredWidth(90);
        cm.getColumn(2).setPreferredWidth(80);
        cm.getColumn(3).setPreferredWidth(30);
        cm.getColumn(4).setPreferredWidth(90);
        cm.getColumn(5).setPreferredWidth(40);
        cm.getColumn(6).setPreferredWidth(100);
        cm.getColumn(7).setPreferredWidth(3);
        tbleleve.getColumnModel().getColumn(3).setCellRenderer(render);
        tbleleve.getColumnModel().getColumn(7).setCellRenderer(render);
        //------------------------------------------------------------------------------
        TableRowFilterSupport.forTable(tbleleve).searchable(true).apply();
        TableRowFilterSupport.forTable(tblemp).searchable(true).apply();
        effacer.setEnabled(false);
        tableEleveChangeListener();
        naissance.setLocale(Locale.FRENCH);
        prenom.setName("prenom");
        nom.setName("nom");
        adresse.setName("adresse");
        
        String fichero  = System.getProperty("user.dir")+"/src/Pictures/icons8-detective.png";
        System.out.println("cd "+fichero);
        Image image = Toolkit.getDefaultToolkit().createImage(fichero); 
        Cursor cursor = Toolkit.getDefaultToolkit().createCustomCursor(image, new Point(10,10),"aa"); 
        code.setCursor(cursor);
        System.out.println("siz "+getSize());
        setSize(1034, 732);
        setLocationRelativeTo(this);
    }

   private void photoIcon(){
        String fichero  = System.getProperty("user.dir")+"/src/Pictures/icons8-user_male_circle_filled.png";
        System.out.println("cd "+fichero);
        Image image = Toolkit.getDefaultToolkit().createImage(fichero); 
        ImageIcon imageicon = new ImageIcon(image);
        
        if(photo.getIcon() == null){
            photo.setIcon(imageicon);
            photo.setText("Double click pour Telecharger une Photo");
        }
    }
    
    private int getEmployerCode(String matricule){
        int code = 0;
        String sql = "select code_employer from employer where matricule = '"+matricule+"' ";
        ResultSet rs = Mn.Rezulta(sql);
        try {
            if(rs.last()){
                code = rs.getInt("code_employer");
            }
        } catch (SQLException ex) {
            Logger.getLogger(Employer.class.getName()).log(Level.SEVERE, null, ex);
        }
        return code;
    }
    
    private void focusLost(){
        try {
            String sql = "SELECT * FROM tercero_general_view where code = '"+code.getText().trim()+"' ";
            System.out.println(sql);
            ResultSet rs = Mn.Rezulta(sql);
            if(rs.last()){
                modificationDisabled();
                nom.setText(rs.getString("nom"));
                prenom.setText(rs.getString("prenom"));
                sexe.setSelectedItem(rs.getString("sexe"));
//                nationalite.setSelectedItem(rs.getString("nationalite"));
                etatcivil.setSelectedItem(rs.getString("etat_civil"));
                tpdocument.setSelectedItem(rs.getString("type_document"));
                document.setText(rs.getString("iddocument"));
//                date_emission.setDate(rs.getDate("date_emission"));
//                date_expiration.setDate(rs.getDate("date_expiration"));
//                lieu.setText(rs.getString("lieu_naissance"));
                naissance.setDate(rs.getDate("date_naissance"));
                adresse.setText(rs.getString("adresse"));
                
                //image
                sql = "select imagecol from show_image where code = '"+code.getText().trim()+"'  ";
                Mn.ShowPhotos(sql, photo);
                
                
                //email
                sql = "select mail from show_email where code = '"+code.getText().trim()+"' ";
                ResultSet rse = Mn.Rezulta(sql);
                if(rse.first()) { 
                    mail.setText(rse.getString("mail"));
                }
                
                //telephones
                sql = "SELECT phone_number FROM show_telephones where code = '"+code.getText().trim()+"' ";
                ResultSet rst = Mn.Rezulta(sql);
                if(rst.first()) { 
                   teleleve.setText(rst.getString("phone_number"));
                }
                
                //sanguins
            ResultSet er = Mn.Rezulta("select sang from show_sanguins where code='"+code.getText().trim()+"' ");
            if(er.first()){
                sanguins.setSelectedItem(er.getString("sang"));
            }
                
            }  
        } catch (SQLException ex) {
            Logger.getLogger(Person.class.getName()).log(Level.SEVERE, null, ex);
        }
    }


    private void  autoCompleteEvent(KeyEvent evt,final TextFieldRectBackground textfield){
        switch(evt.getKeyCode()){
            case KeyEvent.VK_BACK_SPACE:
                break;
                case KeyEvent.VK_ENTER:
                    break;
                    default:
                        EventQueue.invokeLater(new Runnable() {

            @Override
            public void run() {
            String txt = textfield.getText();
                Mn.autoComplete(txt,"tercero",textfield.getName(),textfield);
            }
        });
        }
    }
    
    private void tableEleveChangeListener() throws ArrayIndexOutOfBoundsException{
        tbleleve.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
            if (code.getText().isEmpty()) {
                Nettoyer();
            } 
            else {
                if(tbleleve.getRowCount() > 0){
                String aa = tbleleve.getValueAt(tbleleve.getSelectedRow(), 3).toString();
                String query = "select * from Show_employers where matricule='" + aa + "'";
                ResultSet rs = Mn.SEARCHDATA(query);
                try {
                    if (rs.first()) {
                        System.out.println("size "+getSize());
                        modificationDisabled();
                        tpemployer.setEnabled(false);
                        state.setEnabled(false);
                        estatus.setEnabled(false);
                        code.setText(rs.getString("code_employer"));
                        coode.setText(rs.getString("code_tercero"));
                        prenom.setText(rs.getString("nom"));
                        nom.setText(rs.getString("prenom"));
                        adresse.setText(rs.getString("adresse"));
                        teleleve.setText(rs.getString("telephone"));
                        matricule.setText(rs.getString("matricule"));
                        document.setText(rs.getString("document"));
                        naissance.setDate(rs.getDate("naissance"));
                        mail.setText(rs.getString("mail"));
                        etatcivil.setSelectedItem(rs.getString("etat_civil"));
                        tpdocument.setSelectedItem(rs.getString("tpdocument"));
                        sexe.setSelectedItem(rs.getString("sexe"));
                        sanguins.setSelectedItem(rs.getString("sang"));
                        
                        estatus.setSelected(Mn.CONSULT_CHECKED("Show_employers", code, champ));
                        prenom.requestFocus();
                        RemplirEmployer();
                        
                        String cd = "";
                        String sql = "select code from show_empleado where matricule = '"+aa+"' ";
                        ResultSet rsv = Mn.Rezulta(sql);
                        if(rsv.last()){
                          cd = rsv.getString("code");
                        }
                        Mn.ShowPhotos("SELECT * FROM show_image where code  = '"+cd+"' ", photo);
                    }
                } catch (SQLException ex) {
                    Logger.getLogger(Eleves.class.getName()).log(Level.SEVERE, null, ex);
                }
        }
            }
            }
        });
    }
    
    private void Nettoyer() {
        prenom.setText(null);
        nom.setText(null);
        adresse.setText(null);
        teleleve.setValue(null);
        Mn.PHONE_FROMAT(teleleve);
        mail.setText(null);
        naissance.setCalendar(null);
        sexe.setSelectedItem("Selectionnez");
        tpdocument.setSelectedItem("Selectionnez");
        estatus.setSelected(false);
        tpemployer.setSelectedItem("Selectionnez");
        etatcivil.setSelectedItem("Selectionnez");
        sanguins.setSelectedItem("Selectionnez");
        document.setText(null);
        matricule.setText(gm.MatriculeProf());
        Mn.EMPTYTABLE(tblemp);
        photo.setIcon(null); photo.updateUI();
        photoIcon();
    }
    
    private void modificationDisabled(){
        prenom.setEditable(false);
        nom.setEditable(false);
        adresse.setEditable(false);
        teleleve.setEditable(false);
        mail.setEditable(false);
        naissance.setEnabled(false);
        sexe.setEnabled(false);
        tpdocument.setEnabled(false);
//        estatus.setEnabled(false);
//        tpemployer.setEnabled(false);
        etatcivil.setEnabled(false);
        sanguins.setEnabled(false);
        document.setEditable(false);
        matricule.setEditable(false);
    }
    
    private void modificationEnabled(){
        prenom.setEditable(true);
        nom.setEditable(true);
        adresse.setEditable(true);
        teleleve.setEditable(true);
        mail.setEditable(true);
        naissance.setEnabled(true);
        sexe.setEnabled(true);
        tpdocument.setEnabled(true);
        estatus.setEnabled(true);
        tpemployer.setEnabled(true);
        etatcivil.setEnabled(true);
        sanguins.setEnabled(true);
        document.setEditable(true);
        matricule.setEditable(true);
        state.setEnabled(true);
    }

    private void Consulter() {
        String select = "";
        String query = "select * from Show_employer";
        ResultSet rs = Mn.SEARCHDATA(query);
        try {
            Mn.removeDatosOntable(md);
            while (rs.next()) {
                boolean etat = rs.getBoolean("estatuses");
                if (etat == true) {
                    select = "Actif";
                } else {
                    select = "Inactif";
                }
                String[] vect = {rs.getString("nom"), rs.getString("prenom"), rs.getString("document"), rs.getString("matricule"),
                    rs.getString("adresse"), rs.getString("telephone"), rs.getString("mail"), select};
                Mn.ADD_TABLE(md, vect);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Entrer_note.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void RemplirEmployer() {
        String aa = tbleleve.getValueAt(tbleleve.getSelectedRow(), 3).toString();
        String query = "select description,estatuses from type_employer te join employer_vs_type et on(te.idtype_employer = et.idtype_employer)\n" +
" where et.matricule='"+aa+"' ";
        Mn.FULLTABLE(query, tblemp, mds);
        jLabel18.setText("Nombre(s) de registre(s) : "+tbleleve.getRowCount());
    }

    private String ReturnIstitution() {
        String res = "";
        String query = "select nom from institution";
        ResultSet rs = Mn.SEARCHDATA(query);
        try {
            if (rs.first()) {
                res = rs.getString("nom");
            }
        } catch (SQLException ex) {
            Logger.getLogger(Eleves.class.getName()).log(Level.SEVERE, null, ex);
        }
        return res;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPasswordField1 = new javax.swing.JPasswordField();
        jPanel1 = new javax.swing.JPanel();
        jPanel9 = new javax.swing.JPanel();
        photo = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        code = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        matricule = new javax.swing.JTextField();
        tpemployer = new javax.swing.JComboBox();
        state = new javax.swing.JCheckBox();
        jPanel4 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        prenom = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        etatcivil = new javax.swing.JComboBox();
        jPanel5 = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        nom = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        tpdocument = new javax.swing.JComboBox();
        jPanel6 = new javax.swing.JPanel();
        jLabel9 = new javax.swing.JLabel();
        sexe = new javax.swing.JComboBox();
        jLabel10 = new javax.swing.JLabel();
        document = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        sanguins = new javax.swing.JComboBox();
        jPanel7 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblemp = new javax.swing.JTable();
        jPanel8 = new javax.swing.JPanel();
        jLabel8 = new javax.swing.JLabel();
        adresse = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        teleleve = new javax.swing.JFormattedTextField();
        estatus = new javax.swing.JCheckBox();
        jPanel10 = new javax.swing.JPanel();
        jLabel13 = new javax.swing.JLabel();
        naissance = new com.toedter.calendar.JDateChooser();
        jLabel14 = new javax.swing.JLabel();
        mail = new javax.swing.JTextField();
        jPanel11 = new javax.swing.JPanel();
        jPanel12 = new javax.swing.JPanel();
        jLabel15 = new javax.swing.JLabel();
        recherche = new javax.swing.JTextField();
        jPanel13 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tbleleve = new javax.swing.JTable(){
            public boolean isCellEditable(int rowIndex, int colIndex){
                return false;
            }
        };
        jPanel14 = new javax.swing.JPanel();
        jLabel18 = new javax.swing.JLabel();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        enregistrer = new javax.swing.JMenuItem();
        nouveau = new javax.swing.JMenuItem();
        effacer = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        jMenuItem4 = new javax.swing.JMenuItem();
        modifier = new javax.swing.JMenuItem();
        jMenuItem6 = new javax.swing.JMenuItem();
        jMenuItem7 = new javax.swing.JMenuItem();

        jPasswordField1.setText("jPasswordField1");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(new java.awt.CardLayout());

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel9.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));

        photo.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        photo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/ButtonFoto/icons8-camera.png"))); // NOI18N
        photo.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        photo.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                photoMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanel9Layout = new javax.swing.GroupLayout(jPanel9);
        jPanel9.setLayout(jPanel9Layout);
        jPanel9Layout.setHorizontalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(photo, javax.swing.GroupLayout.PREFERRED_SIZE, 235, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        jPanel9Layout.setVerticalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(photo, javax.swing.GroupLayout.PREFERRED_SIZE, 221, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        jPanel1.add(jPanel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(6, 109, 241, 224));

        jPanel2.setBackground(new java.awt.Color(87, 148, 210));
        jPanel2.setBorder(new org.edisoncor.gui.util.DropShadowBorder());

        jLabel5.setBackground(new java.awt.Color(255, 255, 255));
        jLabel5.setFont(new java.awt.Font("Lucida Grande", 1, 18)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(255, 255, 255));
        jLabel5.setText("Registre d'employé(s)");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel5)
                .addContainerGap(808, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(26, Short.MAX_VALUE))
        );

        jPanel1.add(jPanel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(6, 6, 1022, -1));

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));

        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel1.setText("Code : ");

        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel2.setText("Matricule : ");

        tpemployer.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Selectionnez", "Directeur", "Professeur", "Secrétaire", "Infirmière", "Contable", "Censeur", "Sécurité", "Surveillant", "Autres" }));

        state.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        state.setText("Status");
        state.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                stateItemStateChanged(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 76, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(code, javax.swing.GroupLayout.PREFERRED_SIZE, 172, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(matricule, javax.swing.GroupLayout.PREFERRED_SIZE, 159, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(tpemployer, javax.swing.GroupLayout.PREFERRED_SIZE, 175, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(state)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(code, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2)
                    .addComponent(matricule, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(tpemployer, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(state))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel1.add(jPanel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(257, 84, -1, -1));

        jPanel4.setBackground(new java.awt.Color(255, 255, 255));

        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel3.setText("Prénom : ");

        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel4.setText("Etat Civil : ");

        etatcivil.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Selectionnez", "Marié(e)", "Divorcé(e)", "Célibataire", "Veuf(ve)", "Autres" }));

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(prenom, javax.swing.GroupLayout.PREFERRED_SIZE, 172, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(etatcivil, javax.swing.GroupLayout.PREFERRED_SIZE, 181, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(prenom, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4)
                    .addComponent(etatcivil, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel1.add(jPanel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(257, 130, -1, -1));

        jPanel5.setBackground(new java.awt.Color(255, 255, 255));

        jLabel6.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel6.setText("Nom : ");

        jLabel7.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel7.setText("Type Doc : ");

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 72, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(nom, javax.swing.GroupLayout.DEFAULT_SIZE, 178, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(tpdocument, javax.swing.GroupLayout.PREFERRED_SIZE, 159, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(16, 16, 16))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(nom, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel7)
                    .addComponent(tpdocument, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel1.add(jPanel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(257, 176, 530, -1));

        jPanel6.setBackground(new java.awt.Color(255, 255, 255));

        jLabel9.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel9.setText("Sexe : ");

        sexe.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Selectionnez", "Féminin", "Masculin", "Autres" }));

        jLabel10.setText("Document : ");

        jLabel11.setText("Groupe Sanguins : ");

        sanguins.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Selectionnez", "A+", "A-", "B+", "B-", "O+", "O-", "AB+", "AB-", "Autres" }));

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 68, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(sexe, javax.swing.GroupLayout.PREFERRED_SIZE, 159, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel10)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(document, javax.swing.GroupLayout.PREFERRED_SIZE, 167, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 150, Short.MAX_VALUE)
                .addComponent(sanguins, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
            .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel6Layout.createSequentialGroup()
                    .addContainerGap(508, Short.MAX_VALUE)
                    .addComponent(jLabel11)
                    .addContainerGap(145, Short.MAX_VALUE)))
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel6Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9)
                    .addComponent(sexe, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel10)
                    .addComponent(document, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(sanguins, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
            .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel6Layout.createSequentialGroup()
                    .addContainerGap(12, Short.MAX_VALUE)
                    .addComponent(jLabel11)
                    .addContainerGap(12, Short.MAX_VALUE)))
        );

        jPanel1.add(jPanel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(257, 222, -1, 39));

        jPanel7.setLayout(new java.awt.GridLayout(1, 0));

        tblemp.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tblemp);

        jPanel7.add(jScrollPane1);

        jPanel1.add(jPanel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(788, 130, 240, 75));

        jPanel8.setBackground(new java.awt.Color(255, 255, 255));

        jLabel8.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel8.setText("Adresse : ");

        jLabel12.setText("Telephone : ");

        estatus.setText("Status");
        estatus.setName("estatus"); // NOI18N
        estatus.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                estatusItemStateChanged(evt);
            }
        });

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 76, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(adresse, javax.swing.GroupLayout.PREFERRED_SIZE, 211, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel12)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(teleleve, javax.swing.GroupLayout.PREFERRED_SIZE, 189, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(estatus)
                .addContainerGap(117, Short.MAX_VALUE))
        );
        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(adresse, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel12)
                    .addComponent(teleleve, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(estatus, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel1.add(jPanel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(257, 267, 771, -1));

        jPanel10.setBackground(new java.awt.Color(255, 255, 255));

        jLabel13.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel13.setText("Naissance : ");

        jLabel14.setText("Email : ");

        javax.swing.GroupLayout jPanel10Layout = new javax.swing.GroupLayout(jPanel10);
        jPanel10.setLayout(jPanel10Layout);
        jPanel10Layout.setHorizontalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel10Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, 76, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(naissance, javax.swing.GroupLayout.PREFERRED_SIZE, 203, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel14)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(mail, javax.swing.GroupLayout.PREFERRED_SIZE, 228, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(193, Short.MAX_VALUE))
        );
        jPanel10Layout.setVerticalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel10Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(naissance, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel13, javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel14)
                        .addComponent(mail, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(8, Short.MAX_VALUE))
        );

        jPanel1.add(jPanel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(257, 312, 770, 40));

        jPanel11.setBackground(new java.awt.Color(255, 255, 255));

        jPanel12.setBackground(new java.awt.Color(87, 148, 210));

        jLabel15.setText("Recherche : ");

        recherche.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                rechercheKeyReleased(evt);
            }
        });

        javax.swing.GroupLayout jPanel12Layout = new javax.swing.GroupLayout(jPanel12);
        jPanel12.setLayout(jPanel12Layout);
        jPanel12Layout.setHorizontalGroup(
            jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel12Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel15)
                .addGap(335, 335, 335))
            .addGroup(jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel12Layout.createSequentialGroup()
                    .addContainerGap(692, Short.MAX_VALUE)
                    .addComponent(recherche, javax.swing.GroupLayout.PREFERRED_SIZE, 324, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap()))
        );
        jPanel12Layout.setVerticalGroup(
            jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel12Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel15, javax.swing.GroupLayout.DEFAULT_SIZE, 32, Short.MAX_VALUE))
            .addGroup(jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel12Layout.createSequentialGroup()
                    .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(recherche, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap()))
        );

        jPanel13.setLayout(new java.awt.CardLayout());

        tbleleve.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane2.setViewportView(tbleleve);

        jPanel13.add(jScrollPane2, "card2");

        jPanel14.setBackground(new java.awt.Color(255, 255, 255));
        jPanel14.setLayout(new java.awt.CardLayout());

        jLabel18.setText("Nombre(s) de registre(s) : 0");
        jPanel14.add(jLabel18, "card2");

        javax.swing.GroupLayout jPanel11Layout = new javax.swing.GroupLayout(jPanel11);
        jPanel11.setLayout(jPanel11Layout);
        jPanel11Layout.setHorizontalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel11Layout.createSequentialGroup()
                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jPanel12, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel13, javax.swing.GroupLayout.DEFAULT_SIZE, 1022, Short.MAX_VALUE))
                .addContainerGap())
            .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel11Layout.createSequentialGroup()
                    .addGap(0, 830, Short.MAX_VALUE)
                    .addComponent(jPanel14, javax.swing.GroupLayout.PREFERRED_SIZE, 198, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );
        jPanel11Layout.setVerticalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel11Layout.createSequentialGroup()
                .addGap(14, 14, 14)
                .addComponent(jPanel12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel13, javax.swing.GroupLayout.PREFERRED_SIZE, 253, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(67, 67, 67))
            .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel11Layout.createSequentialGroup()
                    .addContainerGap(308, Short.MAX_VALUE)
                    .addComponent(jPanel14, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(54, Short.MAX_VALUE)))
        );

        jPanel1.add(jPanel11, new org.netbeans.lib.awtextra.AbsoluteConstraints(6, 345, -1, 330));

        getContentPane().add(jPanel1, "card2");

        jMenu1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/ButtonFoto/icons8-menu.png"))); // NOI18N
        jMenu1.setText("Action");

        enregistrer.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/ButtonFoto/icons8-upload_to_cloud.png"))); // NOI18N
        enregistrer.setText("Enregistrer");
        enregistrer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                enregistrerActionPerformed(evt);
            }
        });
        jMenu1.add(enregistrer);

        nouveau.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/ButtonFoto/icons8-new_by_copy.png"))); // NOI18N
        nouveau.setText("Nouveau");
        nouveau.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nouveauActionPerformed(evt);
            }
        });
        jMenu1.add(nouveau);

        effacer.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/ButtonFoto/icons8-trash.png"))); // NOI18N
        effacer.setText("Effacer");
        effacer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                effacerActionPerformed(evt);
            }
        });
        jMenu1.add(effacer);

        jMenuBar1.add(jMenu1);

        jMenu2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/ButtonFoto/icons8-menu_squared.png"))); // NOI18N
        jMenu2.setText("Edition");

        jMenuItem4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/ButtonFoto/icons8-view_file.png"))); // NOI18N
        jMenuItem4.setText("Consulter");
        jMenu2.add(jMenuItem4);

        modifier.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/ButtonFoto/icons8-edit_file.png"))); // NOI18N
        modifier.setText("Modifier");
        modifier.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                modifierActionPerformed(evt);
            }
        });
        jMenu2.add(modifier);

        jMenuItem6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/ButtonFoto/icons8-new_by_copy.png"))); // NOI18N
        jMenuItem6.setText("Ajouter Matière");
        jMenu2.add(jMenuItem6);

        jMenuItem7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/ButtonFoto/icons8-help.png"))); // NOI18N
        jMenuItem7.setText("Aide");
        jMenu2.add(jMenuItem7);

        jMenuBar1.add(jMenu2);

        setJMenuBar(jMenuBar1);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void photoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_photoMouseClicked

    }//GEN-LAST:event_photoMouseClicked

    private void stateItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_stateItemStateChanged
        if(state.isSelected()){
            state.setText("Active");
        }
        else{
            state.setText("Inactive");
        }
    }//GEN-LAST:event_stateItemStateChanged

    private void estatusItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_estatusItemStateChanged
        if(estatus.isSelected()){
            estatus.setText("Active");
        }
        else{
            estatus.setText("Inactive");
        }
    }//GEN-LAST:event_estatusItemStateChanged

    private void enregistrerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_enregistrerActionPerformed
         String mat = matricule.getText().trim();
        String pas = sc.password();
        String jjj = Mn.INSERT_CHECKED(estatus);
        
        String query = "call Insert_employer ("+codemp+",'"+mat+"',"+Variables.code+",'"+Variables.code_terceross+"','"+pas+"',"+jjj+") ";
        System.out.println("quer "+query);
        Mn.MANIPULEDB(query);
        
        int cd = getEmployerCode(mat);
        for (int i = 0; i < tblemp.getRowCount(); i++) {
        int type = Mn.getCodeFromString("type_employer", "idtype_employer",tblemp.getValueAt(i, 0).toString().trim());
        String state = tblemp.getValueAt(i, 1).toString().trim();
        query = "call employer_vs_type_procedure ("+cd+",'"+mat+"',"+type+",'"+state+"') ";
            System.out.println("quer "+query);
        Mn.MANIPULEDB(query);
        
        }
        
        Consulter();
        nouveau.doClick();
    }//GEN-LAST:event_enregistrerActionPerformed

    private void nouveauActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nouveauActionPerformed
        JTextField[] txt = {code, prenom};
        codemp = 0;
        Mn.NEW_BUTTON(txt, "employer", "code_employer");
        Nettoyer();
        modificationEnabled();
    }//GEN-LAST:event_nouveauActionPerformed

    private void effacerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_effacerActionPerformed
        Mn.DELETE_BUTTON(code, "employer", "code_employer");
        Consulter();
        nouveau.doClick();
    }//GEN-LAST:event_effacerActionPerformed

    private void modifierActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_modifierActionPerformed
        int aa = Integer.parseInt(coode.getText());
        int ab = 0;
        int aab = 0;
        String lalal = "";
        int abb = Integer.parseInt(code.getText());
        String bb = prenom.getText();
        String cc = nom.getText();
        String dd = adresse.getText();
        String ee = teleleve.getText();
        String ff = sexe.getSelectedItem().toString();
        String gg = ReturnIstitution();
        String querys = "select code_employer from employer where code_employer=" + code.getText() + "";
        ResultSet rs = Mn.SEARCHDATA(querys);
        try {
            if (rs.first()) {
                ab = rs.getInt("code_employer");
            }
        } catch (SQLException ex) {
            Logger.getLogger(Eleves.class.getName()).log(Level.SEVERE, null, ex);
        }
        if (abb == ab) {
            aa = ab;
        }
        String query = "call Insert_tercero(" + aa + ",'" + bb + "','" + cc + "','" + ff + "','" + dd + "','" + ee + "','" + gg + "')";
        Mn.MANIPULEDB(query);
        int aaa = Integer.parseInt(code.getText());
        //-----------------------------------------------------------------------------
        String hola = "select code_employer from employer where code_employer=" + code.getText() + "";
        ResultSet rss = Mn.SEARCHDATA(hola);
        try {
            if (rss.first()) {
                ab = rss.getInt("code_employer");
            }
        } catch (SQLException ex) {
            Logger.getLogger(Eleves.class.getName()).log(Level.SEVERE, null, ex);
        }
        if (abb == ab) {
            aa = ab;
            for (int i = 0; i < tblemp.getRowCount(); i++) {
                String qq = tblemp.getValueAt(i, 0).toString();
                String qqq = tblemp.getValueAt(i, 1).toString();
                String lol = "select code_employer from employer_vs_type where type_employer='" + qq + "' ";
                ResultSet res = Mn.SEARCHDATA(lol);
                try {
                    if (res.next()) {
                        aab = res.getInt("code_employer");
                    }
                } catch (SQLException ex) {
                    Logger.getLogger(Employer.class.getName()).log(Level.SEVERE, null, ex);
                }
                if (aaa == aab) {
                    lalal = "update employer_vs_type set code_employer=" + aaa + ",type_employer='" + qq + "',estatuses='" + qqq + "' where type_employer='" + qq + "'";
                    aab = 0;
                } else {
                    lalal = "call Insert_type(" + aaa + ",'" + qq + "','" + qqq + "')";
                }
                Mn.MANIPULEDB(lalal);
            }
        } else {
            for (int i = 0; i < tblemp.getRowCount(); i++) {
                String qq = tblemp.getValueAt(i, 0).toString();
                String qqq = tblemp.getValueAt(i, 1).toString();
                String lala = "call Insert_type(" + aaa + ",'" + qq + "','" + qqq + "')";
                Mn.MANIPULEDB(lala);
            }
        }
        //-----------------------------------------------------------------------------

        String bbb = matricule.getText();
        String ccc = Mn.INSERTDATECHOOSER(naissance);
        String ddd = etatcivil.getSelectedItem().toString();
        String fff = document.getText();
        String ggg = mail.getText();
        String ffff = tpdocument.getSelectedItem().toString();
        String iii = Mn.DATEHOURNOW();
        String jjj = Mn.INSERT_CHECKED(estatus);
        String pas = sc.password();
        query = "call Insert_employer(" + aaa + "," + aa + ",'" + bbb + "','" + pas + "','" + ccc + "','" + fff + "','" + ffff + "','" + ddd + "','" + ggg + "',"
                + "'" + iii + "'," + jjj + ")";
        // System.out.println(query);
        Mn.MODIFIER(query);
        String san = sanguins.getSelectedItem().toString();
        Mn.MANIPULEDB("call Insert_sanguins (" + aa + ",'" + san + "')");
        Consulter();
        nouveau.doClick();
    }//GEN-LAST:event_modifierActionPerformed

    private void rechercheKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_rechercheKeyReleased
         String select = "";
        String query = "select nom,prenom,document,matricule,adresse,telephone,mail,estatuses from Show_employers "
                + " where nom like '%" + recherche.getText() + "%' or prenom like '%" + recherche.getText() + "%'"
                + " or matricule like '%" + recherche.getText() + "%' or document like '%" + recherche.getText() + "%'";
        ResultSet rs = Mn.SEARCHDATA(query);
        try {
            md.setRowCount(0);
            while (rs.next()) {
                boolean etat = rs.getBoolean("estatuses");
                if (etat == true) {
                    select = "Actif";
                } else {
                    select = "Inactif";
                }
                String[] vect = {rs.getString("nom"), rs.getString("prenom"), rs.getString("document"), rs.getString("matricule"),
                    rs.getString("adresse"), rs.getString("telephone"), rs.getString("mail"), select};
                Mn.ADD_TABLE(md, vect);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Entrer_note.class.getName()).log(Level.SEVERE, null, ex);
        }
    }                                     

    private void addActionPerformed(java.awt.event.ActionEvent evt) {                                    
        String lestado = "";
        if (state.isSelected()) {
            lestado = "Actif";
        } else {
            lestado = "Inactif";
        }
        String[] vect = {tpemployer.getSelectedItem().toString(), lestado};
        Mn.ADD_TABLE(mds, vect);
        tpemployer.setSelectedItem(null);
        state.setSelected(false);
    }//GEN-LAST:event_rechercheKeyReleased

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(EmployerForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(EmployerForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(EmployerForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(EmployerForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new EmployerForm().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField adresse;
    private javax.swing.JTextField code;
    private javax.swing.JTextField document;
    private javax.swing.JMenuItem effacer;
    private javax.swing.JMenuItem enregistrer;
    private javax.swing.JCheckBox estatus;
    private javax.swing.JComboBox etatcivil;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem4;
    private javax.swing.JMenuItem jMenuItem6;
    private javax.swing.JMenuItem jMenuItem7;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel11;
    private javax.swing.JPanel jPanel12;
    private javax.swing.JPanel jPanel13;
    private javax.swing.JPanel jPanel14;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JPasswordField jPasswordField1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTextField mail;
    private javax.swing.JTextField matricule;
    private javax.swing.JMenuItem modifier;
    private com.toedter.calendar.JDateChooser naissance;
    private javax.swing.JTextField nom;
    private javax.swing.JMenuItem nouveau;
    private javax.swing.JLabel photo;
    private javax.swing.JTextField prenom;
    private javax.swing.JTextField recherche;
    private javax.swing.JComboBox sanguins;
    private javax.swing.JComboBox sexe;
    private javax.swing.JCheckBox state;
    private javax.swing.JTable tbleleve;
    private javax.swing.JTable tblemp;
    private javax.swing.JFormattedTextField teleleve;
    private javax.swing.JComboBox tpdocument;
    private javax.swing.JComboBox tpemployer;
    // End of variables declaration//GEN-END:variables
}
