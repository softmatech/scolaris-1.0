package Forms;

import static Menus.Menus.desktop;
import Methods.Manipulation;
import Methods.Variables;
import com.ezware.oxbow.swingbits.table.filter.TableRowFilterSupport;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;

public class OuvragesClasse extends javax.swing.JInternalFrame {
    Manipulation Mn= new Manipulation();
    String[][] data={};
    String[] head={"Classes","Ouvrages"};
    String[] head1={"Ouvrages Existents"};
    DefaultTableModel md=new DefaultTableModel(data, head);
    DefaultTableModel md1=new DefaultTableModel(data, head1);
    
    public OuvragesClasse() {
        initComponents();
         this.setTitle("Scolaris: "+Variables.Empresa+" : Registre des Ouvrages par Matière");
        nouveau.doClick();
        tbllivre.setModel(md);
        tblliv.setModel(md1);
         TableColumnModel cm= tbllivre.getColumnModel();
        cm.getColumn(0).setPreferredWidth(100);
        cm.getColumn(1).setPreferredWidth(200);
        TableRowFilterSupport.forTable(tblliv).searchable(true).apply();
        TableRowFilterSupport.forTable(tbllivre).searchable(true).apply();
        Mn.FULLCOMBO("select description from classe", classe, "description");
    }
    
    private void Nettoyer(){
     classe.setSelectedItem("Selectionner");
     matiere.setSelectedItem(null);
     ouvrage.setSelectedItem(null);
     md.setRowCount(0);
     md1.setRowCount(0);
     cant.setText("0");
     cant1.setText("0");
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        tbllivre = new javax.swing.JTable(){
            public boolean isCellEditable(int rowIndex, int colIndex){
                return false;
            }
        };
        jScrollPane2 = new javax.swing.JScrollPane();
        tblliv = new javax.swing.JTable(){
            public boolean isCellEditable(int rowIndex, int colIndex){
                return false;
            }
        };
        jLabel4 = new javax.swing.JLabel();
        classe = new javax.swing.JComboBox();
        jLabel2 = new javax.swing.JLabel();
        matiere = new javax.swing.JComboBox();
        jLabel6 = new javax.swing.JLabel();
        ouvrage = new javax.swing.JComboBox();
        add = new javax.swing.JButton();
        rem = new javax.swing.JButton();
        cant = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        cant1 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        enregistrer = new javax.swing.JMenuItem();
        nouveau = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        ajoutersection = new javax.swing.JMenuItem();
        modifier = new javax.swing.JMenuItem();
        jMenuItem1 = new javax.swing.JMenuItem();

        setClosable(true);
        setIconifiable(true);

        tbllivre.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tbllivre);

        tblliv.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane2.setViewportView(tblliv);

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel4.setText("Classe ");

        classe.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        classe.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                classeItemStateChanged(evt);
            }
        });
        classe.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                classeFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                classeFocusLost(evt);
            }
        });

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel2.setText("Matière");

        matiere.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        matiere.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                matiereItemStateChanged(evt);
            }
        });
        matiere.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                matiereFocusGained(evt);
            }
        });
        matiere.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                matiereMouseExited(evt);
            }
        });

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel6.setText("Ouvrages");

        ouvrage.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N

        add.setBackground(new java.awt.Color(255, 255, 255));
        add.setForeground(new java.awt.Color(204, 51, 0));
        add.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/Bajar.png"))); // NOI18N
        add.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addActionPerformed(evt);
            }
        });

        rem.setBackground(new java.awt.Color(255, 255, 255));
        rem.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/Subir.png"))); // NOI18N
        rem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                remActionPerformed(evt);
            }
        });

        cant.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        cant.setForeground(new java.awt.Color(0, 102, 51));
        cant.setText("0");

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(0, 102, 51));
        jLabel3.setText("Ouvrages");

        cant1.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        cant1.setForeground(new java.awt.Color(204, 51, 0));
        cant1.setText("0");

        jLabel7.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(204, 51, 0));
        jLabel7.setText("Ouvrages");

        jMenuBar1.setBackground(new java.awt.Color(102, 102, 102));

        jMenu1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/fichier.jpg"))); // NOI18N
        jMenu1.setText("Action");
        jMenu1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N

        enregistrer.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_E, java.awt.event.InputEvent.CTRL_MASK));
        enregistrer.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/icons8-save_close.png"))); // NOI18N
        enregistrer.setText("Enrégistrer");
        enregistrer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                enregistrerActionPerformed(evt);
            }
        });
        jMenu1.add(enregistrer);

        nouveau.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_N, java.awt.event.InputEvent.CTRL_MASK));
        nouveau.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/icons8-add_list.png"))); // NOI18N
        nouveau.setText("Nouveau");
        nouveau.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nouveauActionPerformed(evt);
            }
        });
        jMenu1.add(nouveau);

        jMenuBar1.add(jMenu1);

        jMenu2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/editer.jpg"))); // NOI18N
        jMenu2.setText("Edition");
        jMenu2.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N

        ajoutersection.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_A, java.awt.event.InputEvent.ALT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        ajoutersection.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/icons8-health_book.png"))); // NOI18N
        ajoutersection.setText("Ajouter Ouvrages");
        ajoutersection.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ajoutersectionActionPerformed(evt);
            }
        });
        jMenu2.add(ajoutersection);

        modifier.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_M, java.awt.event.InputEvent.CTRL_MASK));
        modifier.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/icons8-refresh.png"))); // NOI18N
        modifier.setText("Modifier");
        modifier.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                modifierActionPerformed(evt);
            }
        });
        jMenu2.add(modifier);

        jMenuItem1.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F1, 0));
        jMenuItem1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/icons8-help.png"))); // NOI18N
        jMenuItem1.setText("Aide");
        jMenu2.add(jMenuItem1);

        jMenuBar1.add(jMenu2);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(18, 18, 18)
                .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 53, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(classe, javax.swing.GroupLayout.PREFERRED_SIZE, 266, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(21, 21, 21)
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(matiere, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(169, 169, 169))
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(8, 8, 8)
                                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 426, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(jLabel6)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(ouvrage, javax.swing.GroupLayout.PREFERRED_SIZE, 265, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(add, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(rem, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(cant)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel3)
                        .addGap(327, 327, 327)))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(cant1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel7))
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 431, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(matiere, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel2))
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(classe, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(ouvrage, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(add, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(rem, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 361, Short.MAX_VALUE))
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 411, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cant)
                    .addComponent(jLabel3)
                    .addComponent(cant1)
                    .addComponent(jLabel7))
                .addContainerGap(20, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void classeItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_classeItemStateChanged
        Mn.FULLSCOMBO(matiere, "matiere", "classe", classe.getSelectedItem().toString(), "show_matiere_vs_classe");
    }//GEN-LAST:event_classeItemStateChanged

    private void classeFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_classeFocusGained
        //     Mn.FULLSCOMBO(classe, "description", "section", section.getSelectedItem().toString(), "classe");

    }//GEN-LAST:event_classeFocusGained

    private void classeFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_classeFocusLost
        //     Mn.FULLSCOMBO(classe, "description", "section", section.getSelectedItem().toString(), "classe");
    }//GEN-LAST:event_classeFocusLost

    private void matiereItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_matiereItemStateChanged
       String query="select description from show_ouvrages where institution='"+Variables.Empresa+"'";
        Mn.FULLCOMBO(query,ouvrage,"description");
        
        String sql="select description from ouvrages_vs_matiere oc left join matiere c on c.code_matiere = oc.code_matiere "
                + " where c.description='"+matiere.getSelectedItem()+"' and institution='"+Variables.Empresa+"' ";
        Mn.FULLTABLE(sql, tblliv, md1);
        cant1.setText(String.valueOf(tblliv.getRowCount()));
    }//GEN-LAST:event_matiereItemStateChanged

    private void matiereFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_matiereFocusGained
//        Remplir();

    }//GEN-LAST:event_matiereFocusGained

    private void matiereMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_matiereMouseExited

    }//GEN-LAST:event_matiereMouseExited

    private void addActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addActionPerformed
        String[] vect={matiere.getSelectedItem().toString(),ouvrage.getSelectedItem().toString()};
        Mn.ADD_TABLE(md, vect);
        cant.setText(String.valueOf(tbllivre.getRowCount()));
        
    }//GEN-LAST:event_addActionPerformed

    private void remActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_remActionPerformed
       Mn.REMOVE_TABLE(tbllivre, md);
    }//GEN-LAST:event_remActionPerformed

    private void nouveauActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nouveauActionPerformed
       Nettoyer();
    }//GEN-LAST:event_nouveauActionPerformed

    private void enregistrerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_enregistrerActionPerformed
        for(int i=0;i<tbllivre.getRowCount();i++){
            String query="SELECT code from show_ouvrages where description='"+Mn.illegalCharacterOnString(tbllivre.getValueAt(i,1).toString())+"' and institution='"+Variables.Empresa+"'";
            System.out.println("1 "+query);
            String cod=Mn.SEARCHCODE(query,"code"); System.out.println("cod : "+cod);
            String querys="select code_matiere from matiere where description='"+Mn.illegalCharacterOnString(tbllivre.getValueAt(i,0).toString())+"' and institution='"+Variables.Empresa+"'";
            System.out.println("2 "+querys);
            String cods=Mn.SEARCHCODE(querys,"code_matiere"); System.out.println("cods : "+cods);
            String sql="call insert_ouvrages_vs_matiere ("+cod+","+cods+") ";
            System.out.println(sql);
            Mn.MANIPULEDB(sql);

        }
        nouveau.doClick();
    }//GEN-LAST:event_enregistrerActionPerformed

    private void modifierActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_modifierActionPerformed
        for(int i=0;i<tbllivre.getRowCount();i++){
            String query="select code_classe from classe where description='"+tbllivre.getValueAt(i,0)+"' and institution='"+Variables.Empresa+"'";
            String cod=Mn.SEARCHCODE(query,"code_classe");
            String querys="select code from ouvrages where description='"+tbllivre.getValueAt(i,1)+"' and institution='"+Variables.Empresa+"'";
            String cods=Mn.SEARCHCODE(querys,"code");
            String cmd="select code_classe,code from ouvrages_vs_classe where code_classe="+cod+" and code="+cods+" and institution='"+Variables.Empresa+"'";
            ResultSet rs=Mn.SEARCHDATA(cmd);
            try {
                if(rs.first()){
                    query="update ouvrages_vs_classe set code_classe="+cod+" and code="+cods+" where code_classe="+cod+" and code="+cods+" and institution='"+Variables.Empresa+"'";
                    //System.out.println(query);
                    Mn.MANIPULEDB(query);
                }else{
                   String sql="call Insert_ouvrages_vs_classe("+cod+","+cods+",'"+Variables.Empresa+"')";
                    //System.out.println(sql);
                    Mn.MANIPULEDB(sql);
                }
            } catch (SQLException ex) {
                Logger.getLogger(OuvragesClasse.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
        nouveau.doClick();
    }//GEN-LAST:event_modifierActionPerformed

    private void ajoutersectionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ajoutersectionActionPerformed
        Ouvrages pr= new Ouvrages();
        desktop.add(pr);
        int x=(desktop.getWidth()/2)- pr.getWidth()/2;
        int y=(desktop.getHeight()/2)-pr.getHeight()/2;
        pr.setLocation(x, y);
        pr.toFront();
        pr.setVisible(true);
    }//GEN-LAST:event_ajoutersectionActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton add;
    private javax.swing.JMenuItem ajoutersection;
    private javax.swing.JLabel cant;
    private javax.swing.JLabel cant1;
    public static javax.swing.JComboBox classe;
    private javax.swing.JMenuItem enregistrer;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    public static javax.swing.JComboBox matiere;
    private javax.swing.JMenuItem modifier;
    private javax.swing.JMenuItem nouveau;
    private javax.swing.JComboBox ouvrage;
    private javax.swing.JButton rem;
    private javax.swing.JTable tblliv;
    private javax.swing.JTable tbllivre;
    // End of variables declaration//GEN-END:variables
}
