package Forms;

import Methods.Manipulation;
import Methods.Rapport;
import Methods.Theme;
import Methods.Variables;
import Processus.Entrer_note;
import Start.Waitts;
import com.ezware.oxbow.swingbits.table.filter.TableRowFilterSupport;
import java.awt.Color;
import java.awt.Component;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumnModel;

public class Collection extends javax.swing.JInternalFrame {
Manipulation Mn= new Manipulation();
Theme th= new Theme();
Rapport Rp=new Rapport();
String[][] data={};
String[] heads={" Matricule"," Nom"," Prénom"," Classes"," Versement"," Date"," Balance"};
DefaultTableModel mds= new DefaultTableModel(data,heads);
  TableCellRenderer render= new TableCellRenderer() {
 public Component getTableCellRendererComponent(JTable table,Object value,boolean idSelected,boolean hasFocus,int row,int column){
     JLabel lbl= new JLabel(value==null? "": value.toString());
      if(column==0 && row>=0){
         lbl.setHorizontalAlignment(SwingConstants.LEFT);
         lbl.setOpaque(true);
         lbl.setBackground(Color.green);
     }
       if(column==4 && row>=0){
         lbl.setHorizontalAlignment(SwingConstants.LEFT);
         lbl.setOpaque(true);
         lbl.setBackground(Color.yellow);
     }
     if(column==6 && row>=0){
         lbl.setHorizontalAlignment(SwingConstants.LEFT);
         lbl.setOpaque(true);
         lbl.setBackground(Color.yellow);
     }
      if(column==6 && value.toString().equals("0 Gdes") && row>=0){
         lbl.setHorizontalAlignment(SwingConstants.LEFT);
         lbl.setOpaque(true);
         lbl.setBackground(Color.red);
     }
     return lbl;
 }
};
    public Collection() {
        initComponents();
        this.setTitle("Scolaris: "+Variables.Empresa+" : Collection ");
        Mn.FULLCOMBO(classe,"description","classe");
        classe.addItem("Selectionnez");
        classe.setSelectedItem("Selectionnez");
         Scolaire();
        date.setText( Mn.DATEHOURNOW());
//        Mn.MATRICULE_FORMAT(matricule);
        tblcollection.setModel(mds);
        RemplirCollection();
      //----------------------------------------------------------------
   TableColumnModel cmy= tblcollection.getColumnModel();
        cmy.getColumn(0).setPreferredWidth(15);
        cmy.getColumn(1).setPreferredWidth(70);
        cmy.getColumn(2).setPreferredWidth(90);
        cmy.getColumn(3).setPreferredWidth(50);
        cmy.getColumn(4).setPreferredWidth(10);
        cmy.getColumn(5).setPreferredWidth(120);
        cmy.getColumn(6).setPreferredWidth(10);
         tblcollection.getColumnModel().getColumn(0).setCellRenderer(render);
          tblcollection.getColumnModel().getColumn(4).setCellRenderer(render);
        tblcollection.getColumnModel().getColumn(6).setCellRenderer(render);
        mesaj.setVisible(false);
        TableRowFilterSupport.forTable(tblcollection).searchable(true).apply();
    }

   private void Scolaire(){
    String query="select annee from annee_academique where code_annee\n" +
     "in (select max(code_annee) from annee_academique)";
    ResultSet rs=Mn.SEARCHDATA(query);
    try {
        if(rs.first()){
         academique.setText(rs.getString("annee"));
        }
    } catch (SQLException ex) {
        Logger.getLogger(Entrer_note.class.getName()).log(Level.SEVERE, null, ex);
    }
    }
   
   private void RemplirCollection(){
       String query="select distinct e.matricule,t.nom,t.prenom,cl.description as classe,c.versement,c.date_versement as date,c.balance \n" +
"from inscription i left join collecter c on (c.code = i.code and c.code_tercero = i.code_tercero) left join classe cl on i.code_classe\n" +
"=cl.code_classe left join tercero t on (t.code = i.code and t.code_tercero = i.code_tercero) left join eleve e on "
+ " (e.code = i.code and e.code_tercero = i.code_tercero) where c.anne='"+academique.getText()+"' order by nom,date_versement ";
       ResultSet rs=Mn.SEARCHDATA(query);
    try {
        mds.setRowCount(0);
        while(rs.next()){
           String[] vect={rs.getString("matricule"),rs.getString("nom"),rs.getString("prenom"),rs.getString("classe"),rs.getString("versement")+" Gdes",
           rs.getString("date"),rs.getString("balance")+" Gdes"} ;
           Mn.ADD_TABLE(mds, vect);
        }
    } catch (SQLException ex) {
        Logger.getLogger(Collection.class.getName()).log(Level.SEVERE, null, ex);
    }
   }
   
   private void ImprimerCollection(){
   Waitts attend= new Waitts(null, rootPaneCheckingEnabled);
   attend.show();
   Rp.SENDQUERY("Collection",matricule.getText(),academique.getText());
   }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        date = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        academique = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        jPanel3 = new javax.swing.JPanel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        versement = new org.edisoncor.gui.textField.TextFieldRectBackground();
        nom = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        matricule = new javax.swing.JTextField();
        jPanel5 = new javax.swing.JPanel();
        mesaj = new javax.swing.JLabel();
        jPanel6 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        classe = new javax.swing.JComboBox();
        jLabel1 = new javax.swing.JLabel();
        recherche = new org.edisoncor.gui.textField.TextFieldRectBackground();
        jSeparator2 = new javax.swing.JSeparator();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblcollection = new javax.swing.JTable(){
            public boolean isCellEditable(int rowIndex, int colIndex){
                return false;
            }
        };
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        Payer = new javax.swing.JMenuItem();
        jMenu5 = new javax.swing.JMenu();
        jMenuItem1 = new javax.swing.JMenuItem();

        setClosable(true);
        setIconifiable(true);
        addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                formMouseMoved(evt);
            }
        });

        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        date.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        date.setText("Date");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(date, javax.swing.GroupLayout.PREFERRED_SIZE, 425, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(34, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(date)
        );

        jPanel2.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel2.setText("Année Académique: ");

        academique.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        academique.setText("0");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(academique, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(academique))
                .addGap(0, 0, Short.MAX_VALUE))
        );

        jPanel3.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel7.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel7.setText("Matricule:");

        jLabel8.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel8.setText("Versement:");

        jLabel10.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel10.setText("Élève:");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(0, 20, Short.MAX_VALUE)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel10, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel7, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel8, javax.swing.GroupLayout.Alignment.TRAILING)))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addComponent(jLabel7)
                .addGap(18, 18, 18)
                .addComponent(jLabel10)
                .addGap(18, 18, 18)
                .addComponent(jLabel8)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel4.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        versement.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                versementFocusLost(evt);
            }
        });
        versement.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                versementKeyReleased(evt);
            }
        });

        nom.setBackground(new java.awt.Color(255, 255, 204));
        nom.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        nom.setForeground(new java.awt.Color(0, 0, 255));
        nom.setText("Nom de l'élève");
        nom.setOpaque(true);

        jLabel9.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel9.setText("Gourdes");

        matricule.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                matriculeKeyReleased(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(nom, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel4Layout.createSequentialGroup()
                                .addComponent(versement, javax.swing.GroupLayout.PREFERRED_SIZE, 199, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel9))
                            .addComponent(matricule, javax.swing.GroupLayout.PREFERRED_SIZE, 265, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(matricule, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 9, Short.MAX_VALUE)
                .addComponent(nom, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(versement, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel9))
                .addContainerGap())
        );

        jPanel5.setBackground(new java.awt.Color(255, 204, 204));
        jPanel5.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        mesaj.setBackground(new java.awt.Color(255, 255, 204));
        mesaj.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        mesaj.setForeground(new java.awt.Color(0, 102, 0));
        mesaj.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        mesaj.setText("jLabel4");
        mesaj.setOpaque(true);

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(mesaj, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(mesaj, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        jPanel6.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel3.setText("Classes:");
        jLabel3.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        classe.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        classe.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Selectionnez" }));

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel1.setText("Recherche:");
        jLabel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        recherche.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        recherche.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                rechercheKeyReleased(evt);
            }
        });

        tblcollection.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tblcollection);

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(classe, javax.swing.GroupLayout.PREFERRED_SIZE, 274, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 45, Short.MAX_VALUE)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(recherche, javax.swing.GroupLayout.PREFERRED_SIZE, 414, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(16, 16, 16))
            .addComponent(jSeparator2)
            .addComponent(jScrollPane1)
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(recherche, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(classe, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 214, Short.MAX_VALUE))
        );

        jMenu1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/fichier.jpg"))); // NOI18N
        jMenu1.setText("Action");
        jMenu1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N

        Payer.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_E, java.awt.event.InputEvent.CTRL_MASK));
        Payer.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/icons8-save_close.png"))); // NOI18N
        Payer.setText("Payer");
        Payer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                PayerActionPerformed(evt);
            }
        });
        jMenu1.add(Payer);

        jMenuBar1.add(jMenu1);

        jMenu5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/editer.jpg"))); // NOI18N
        jMenu5.setText("Edition");
        jMenu5.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N

        jMenuItem1.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F1, 0));
        jMenuItem1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/icons8-help.png"))); // NOI18N
        jMenuItem1.setText("Aide");
        jMenu5.add(jMenuItem1);

        jMenuBar1.add(jMenu5);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
            .addComponent(jSeparator1)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void rechercheKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_rechercheKeyReleased
        recherche.setText(recherche.getText().toUpperCase());
        if(recherche.getText().isEmpty() && classe.getSelectedItem().toString().equals("Selectionnez")){
            RemplirCollection();
        }else if(!recherche.getText().isEmpty() && !classe.getSelectedItem().toString().equals("Selectionnez")){
    String query="select distinct e.matricule,t.nom,t.prenom,cl.description as classe,c.versement,c.date_versement as date,c.balance \n" +
"from inscription i left join collecter c on (c.code = i.code and c.code_tercero = i.code_tercero) left join classe cl on i.code_classe\n" +
"=cl.code_classe left join tercero t on (t.code = i.code and t.code_tercero = i.code_tercero) left join eleve e on "
+ " (e.code = i.code and e.code_tercero = i.code_tercero)  where c.anne='"+academique.getText()+"' and e.matricule like '%"+recherche.getText()+"%' and cl.description"
            + "='"+classe.getSelectedItem().toString()+"' or t.nom like '%"+recherche.getText()+"%'"
   + " or t.prenom like '%"+recherche.getText()+"%' or c.date_versement like '%"+recherche.getText()+"%' order by nom,date_versement ";
       ResultSet rs=Mn.SEARCHDATA(query);
    try {
        mds.setRowCount(0);
        while(rs.next()){
            String[] vect={rs.getString("matricule"),rs.getString("nom"),rs.getString("prenom"),rs.getString("classe"),rs.getString("versement")+" Gdes",
           rs.getString("date"),rs.getString("balance")+" Gdes"} ;
           Mn.ADD_TABLE(mds, vect);
        }
    } catch (SQLException ex) {
        Logger.getLogger(Collection.class.getName()).log(Level.SEVERE, null, ex);
    }}else if(recherche.getText().isEmpty() && !classe.getSelectedItem().toString().equals("Selectionnez")){
    String query="select distinct e.matricule,t.nom,t.prenom,cl.description as classe,c.versement,c.date_versement as date,c.balance \n" +
"from inscription i left join collecter c on (c.code = i.code and c.code_tercero = i.code_tercero) left join classe cl on i.code_classe\n" +
"=cl.code_classe left join tercero t on (t.code = i.code and t.code_tercero = i.code_tercero) left join eleve e on "
+ " (e.code = i.code and e.code_tercero = i.code_tercero)  where c.anne='"+academique.getText()+"' and cl.description='"+classe.getSelectedItem().toString()+"' order by nom,date_versement ";
       ResultSet rs=Mn.SEARCHDATA(query);
    try {
        mds.setRowCount(0);
        while(rs.next()){
            String[] vect={rs.getString("matricule"),rs.getString("nom"),rs.getString("prenom"),rs.getString("classe"),rs.getString("versement")+" Gdes",
           rs.getString("date"),rs.getString("balance")+" Gdes"} ;
           Mn.ADD_TABLE(mds, vect);
        }
    } catch (SQLException ex) {
        Logger.getLogger(Collection.class.getName()).log(Level.SEVERE, null, ex);
    }
    }else if(!recherche.getText().isEmpty() && classe.getSelectedItem().toString().equals("Selectionnez")){
    String query="select distinct e.matricule,t.nom,t.prenom,cl.description as classe,c.versement,c.date_versement as date,c.balance \n" +
"from inscription i left join collecter c on (c.code = i.code and c.code_tercero = i.code_tercero) left join classe cl on i.code_classe\n" +
"=cl.code_classe left join tercero t on (t.code = i.code and t.code_tercero = i.code_tercero) left join eleve e on "
+ " (e.code = i.code and e.code_tercero = i.code_tercero)  where c.anne='"+academique.getText()+"' and e.matricule like '%"+recherche.getText()+"%' and cl.description"
            + "='"+classe.getSelectedItem().toString()+"' or t.nom like '%"+recherche.getText()+"%'"
   + " or t.prenom like '%"+recherche.getText()+"%' or c.date_versement like '%"+recherche.getText()+"%' order by nom,date_versement ";
       ResultSet rs=Mn.SEARCHDATA(query);
    try {
        mds.setRowCount(0);
        while(rs.next()){
            String[] vect={rs.getString("matricule"),rs.getString("nom"),rs.getString("prenom"),rs.getString("classe"),rs.getString("versement")+" Gdes",
           rs.getString("date"),rs.getString("balance")+" Gdes"} ;
           Mn.ADD_TABLE(mds, vect);
        }
    } catch (SQLException ex) {
        Logger.getLogger(Collection.class.getName()).log(Level.SEVERE, null, ex);
    }}
    }//GEN-LAST:event_rechercheKeyReleased

    private void versementFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_versementFocusLost

    }//GEN-LAST:event_versementFocusLost

    private void versementKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_versementKeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_versementKeyReleased

    private void formMouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_formMouseMoved
       date.setText( Mn.DATEHOURNOW());
    }//GEN-LAST:event_formMouseMoved

    private void PayerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_PayerActionPerformed
       String section="";
       double montant=0.0;
       double balance=0.0;
       String versment="";
       int quantite=0;
       int bb=0;
       String xs="";
       String aa=matricule.getText();
        String query = "select e.code,e.code_tercero from eleve e left join tercero t on  (t.code = e.code and t.code_tercero = e.code_tercero) "
          + "join eleve_has_institution ehi on (e.code = ehi.code and e.code_tercero = ehi.code_tercero) "
          + "join institution i on(ehi.idinstitution = i.idinstitution) where e.matricule='"+aa+"' and i.nom = '"+Variables.Empresa+"'";
  
      ResultSet rsrs = Mn.Rezulta(query);
    try {
        if(rsrs.last()){
            bb = rsrs.getInt("code");
            xs = rsrs.getString("code_tercero");
        }
    } catch (SQLException ex) {
        Logger.getLogger(Collection.class.getName()).log(Level.SEVERE, null, ex);
    }
    
     query="select c.section from classe c left join inscription i on i.code_classe=c.code_classe \n" +
       "where i.code="+bb+"  and i.code_tercero='"+xs+"' ";
       ResultSet rs=Mn.SEARCHDATA(query);
    try {
        if(rs.first()){
          section=rs.getString("section");
        }
    } catch (SQLException ex) {
        Logger.getLogger(Collection.class.getName()).log(Level.SEVERE, null, ex);
    }
     
    String querys="select montant,versement from collection where section='"+section+"'";
       ResultSet rss=Mn.SEARCHDATA(querys);
    try {
        if(rss.first()){
          montant=rss.getDouble("montant");
          versment=rss.getString("versement");
        }
    } catch (SQLException ex) {
        Logger.getLogger(Collection.class.getName()).log(Level.SEVERE, null, ex);
    }
    
     String queerys="select balance,quantite_versement  from collecter where matricule='"+matricule.getText()+"' and anne='"+academique.getText()+"'";
       ResultSet rsss=Mn.SEARCHDATA(queerys);
    try {
        if(rsss.last()){
             if(rsss.getDouble("balance")>=0){
              balance=rsss.getDouble("balance")-Double.valueOf(versement.getText()); 
              quantite=rsss.getInt("quantite_versement");
            if(quantite<montant){
              quantite++;  
            }
            }
        }else
        { 
            quantite++;  
            balance=montant-Double.valueOf(versement.getText());  
        }
    } catch (SQLException ex) {
        Logger.getLogger(Collection.class.getName()).log(Level.SEVERE, null, ex);
    }
    
    double cc=Double.valueOf(versement.getText());
    String dd=date.getText(); 
    String annee=academique.getText();
    String sql="call Insert_collecter('"+aa+"','"+bb+"','"+xs+"','"+cc+"','"+quantite+"','"+dd+"','"+balance+"','"+annee+"')";
    if(balance>=0){
      mesaj.setVisible(false);
      Mn.MANIPULEDB(sql);  
    }else{
        mesaj.setVisible(true);
        mesaj.setText("Cet(te) élève est en règle avec l'économat.");
    }
    RemplirCollection();
    //ImprimerCollection();
    matricule.setText(null);
    nom.setText("Nom de l'élève");
    versement.setText(null);
    }//GEN-LAST:event_PayerActionPerformed

    private void matriculeKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_matriculeKeyReleased
         matricule.setText(matricule.getText().toUpperCase());
        String query="select t.nom,t.prenom from tercero t left join eleve e "
        + "on (e.code=t.code and e.code_tercero=t.code_tercero) where e.matricule like '%"+matricule.getText()+"%' ";
        ResultSet rs=Mn.SEARCHDATA(query);
        try {
            while(rs.next()){
                nom.setText(rs.getString("nom")+" "+rs.getString("prenom"));
                mesaj.setVisible(false);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Professeur.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_matriculeKeyReleased

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Collection.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Collection.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Collection.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Collection.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Collection().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuItem Payer;
    private javax.swing.JLabel academique;
    private javax.swing.JComboBox classe;
    private javax.swing.JLabel date;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu5;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JTextField matricule;
    private javax.swing.JLabel mesaj;
    private javax.swing.JLabel nom;
    private org.edisoncor.gui.textField.TextFieldRectBackground recherche;
    private javax.swing.JTable tblcollection;
    private org.edisoncor.gui.textField.TextFieldRectBackground versement;
    // End of variables declaration//GEN-END:variables
}
