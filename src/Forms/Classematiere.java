package Forms;
import Consults.Consclassmatiere;
import static Menus.Menus.desktop;
import Methods.*;
import java.awt.Cursor;
import java.awt.event.MouseEvent;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import Methods.Theme;

public class Classematiere extends javax.swing.JInternalFrame {
Manipulation Mn= new Manipulation();
Theme th= new Theme();
public static String[][] data={};
public static String[] head={" Matières"};
public static String[] headd={" Matières Existante"};
public static DefaultTableModel md= new DefaultTableModel(data,head);
public static DefaultTableModel mdd= new DefaultTableModel(data,headd);

    public Classematiere() {
        initComponents();
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        this.setTitle("Scolaris: "+Variables.Empresa+" : Registre des Classes");
        tblmatiere.setModel(md);
        tblmatiere1.setModel(mdd);
        String query="select m.description as matiere, mc.compose from matiere m left join matiere_compose mc on m.description=mc.matiere";
        Mn.FULLCOMBO(matiere,"description","matiere");
        nouveau.doClick();
    }
    private void Nettoyer(){
    classe.setSelectedItem("Selectionnez");
    section.setSelectedItem("Selectionnez");
    matiere.setSelectedItem("Selectionnez");
    mdd.setRowCount(0);
    md.setRowCount(0);
    }
    
    public void Remplir(){
        String bb=classe.getSelectedItem().toString();
        if(!bb.equals("Selectionnez")){
       String query="select * from show_matieres where classe = '"+classe.getSelectedItem().toString().trim()+"' and section='"+section.getSelectedItem().toString().trim()+"'"
                     + " and institution='"+Variables.Empresa+"' group by matiere order by matiere";
         System.out.println(query);
         ResultSet rs=Mn.SEARCHDATA(query);
           try {
              mdd.setRowCount(0);
              md.setRowCount(0);
              while(rs.next()){
              String[] vect={rs.getString("matiere")};
              Mn.ADD_TABLE(mdd, vect);
              Mn.ADD_TABLE(md, vect);
              }
              } catch (SQLException ex) {
                Logger.getLogger(Classematiere.class.getName()).log(Level.SEVERE, null, ex);
              }
            }
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonSeven1 = new org.edisoncor.gui.button.ButtonSeven();
        buttonTask1 = new org.edisoncor.gui.button.ButtonTask();
        buttonPopup1 = new org.edisoncor.gui.button.ButtonPopup();
        buttonTask2 = new org.edisoncor.gui.button.ButtonTask();
        buttonIpod1 = new org.edisoncor.gui.button.ButtonIpod();
        buttonIcon1 = new org.edisoncor.gui.button.ButtonIcon();
        jMenuItem1 = new javax.swing.JMenuItem();
        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblmatiere = new javax.swing.JTable(){
            public boolean isCellEditable(int rowIndex, int colIndex){
                return false;
            }
        };
        add = new javax.swing.JButton();
        rem = new javax.swing.JButton();
        jLabel7 = new javax.swing.JLabel();
        cantmat = new javax.swing.JLabel();
        matiere = new javax.swing.JComboBox();
        jLabel2 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tblmatiere1 = new javax.swing.JTable(){
            public boolean isCellEditable(int rowIndex, int colIndex){
                return false;
            }
        };
        section = new javax.swing.JComboBox();
        classe = new javax.swing.JComboBox();
        filtrer = new javax.swing.JButton();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        enregistrer = new javax.swing.JMenuItem();
        nouveau = new javax.swing.JMenuItem();
        effacer = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        consulter = new javax.swing.JMenuItem();
        modifier = new javax.swing.JMenuItem();
        ajoutersection = new javax.swing.JMenuItem();
        jMenuItem2 = new javax.swing.JMenuItem();

        buttonSeven1.setText("buttonSeven1");

        buttonPopup1.setText("buttonPopup1");

        buttonIpod1.setText("buttonIpod1");

        buttonIcon1.setText("buttonIcon1");

        jMenuItem1.setText("jMenuItem1");

        setClosable(true);
        setIconifiable(true);

        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        tblmatiere.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tblmatiere);

        add.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/Bajar.png"))); // NOI18N
        add.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addActionPerformed(evt);
            }
        });

        rem.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/Subir.png"))); // NOI18N
        rem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                remActionPerformed(evt);
            }
        });

        jLabel7.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel7.setText("Matières");

        cantmat.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        cantmat.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        cantmat.setText("0");

        matiere.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        matiere.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                matiereFocusGained(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 312, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(matiere, javax.swing.GroupLayout.PREFERRED_SIZE, 223, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(add, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(rem, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(cantmat, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel7)
                .addGap(43, 43, 43))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(add, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(rem, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(matiere, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 304, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(cantmat))
                .addContainerGap())
        );

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel2.setText("Classe");

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel4.setText("Section");

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(0, 102, 255));
        jLabel6.setText("CLIQUER ICI POUR VIDER LA TABLE.");
        jLabel6.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel6MouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jLabel6MouseEntered(evt);
            }
        });

        jPanel2.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        tblmatiere1.setBackground(new java.awt.Color(255, 204, 204));
        tblmatiere1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane2.setViewportView(tblmatiere1);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 297, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 398, Short.MAX_VALUE)
                .addContainerGap())
        );

        section.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        section.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Selectionnez", "Petit", "Primaire", "Secondaire" }));
        section.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                sectionItemStateChanged(evt);
            }
        });
        section.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                sectionFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                sectionFocusLost(evt);
            }
        });

        classe.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        classe.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                classeItemStateChanged(evt);
            }
        });
        classe.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                classeFocusGained(evt);
            }
        });
        classe.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                classeMouseExited(evt);
            }
        });

        filtrer.setBackground(new java.awt.Color(255, 255, 255));
        filtrer.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        filtrer.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/filter.png"))); // NOI18N
        filtrer.setText("Filtrage des Matières par Classe");
        filtrer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                filtrerActionPerformed(evt);
            }
        });

        jMenu1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/fichier.jpg"))); // NOI18N
        jMenu1.setText("Action");
        jMenu1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N

        enregistrer.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_E, java.awt.event.InputEvent.CTRL_MASK));
        enregistrer.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/icons8-save_close.png"))); // NOI18N
        enregistrer.setText("Enrégistrer");
        enregistrer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                enregistrerActionPerformed(evt);
            }
        });
        jMenu1.add(enregistrer);

        nouveau.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_N, java.awt.event.InputEvent.CTRL_MASK));
        nouveau.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/icons8-add_list.png"))); // NOI18N
        nouveau.setText("Nouveau");
        nouveau.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nouveauActionPerformed(evt);
            }
        });
        jMenu1.add(nouveau);

        effacer.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_D, java.awt.event.InputEvent.CTRL_MASK));
        effacer.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/icons8-erase.png"))); // NOI18N
        effacer.setText("Effacer");
        effacer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                effacerActionPerformed(evt);
            }
        });
        jMenu1.add(effacer);

        jMenuBar1.add(jMenu1);

        jMenu2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/editer.jpg"))); // NOI18N
        jMenu2.setText("Edition");
        jMenu2.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N

        consulter.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_W, java.awt.event.InputEvent.CTRL_MASK));
        consulter.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/icons8-search.png"))); // NOI18N
        consulter.setText("Consulter");
        consulter.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                consulterActionPerformed(evt);
            }
        });
        jMenu2.add(consulter);

        modifier.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_M, java.awt.event.InputEvent.CTRL_MASK));
        modifier.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/icons8-refresh.png"))); // NOI18N
        modifier.setText("Modifier");
        modifier.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                modifierActionPerformed(evt);
            }
        });
        jMenu2.add(modifier);

        ajoutersection.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_A, java.awt.event.InputEvent.ALT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        ajoutersection.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/icons8-test_failed.png"))); // NOI18N
        ajoutersection.setText("Ajouter matière");
        ajoutersection.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ajoutersectionActionPerformed(evt);
            }
        });
        jMenu2.add(ajoutersection);

        jMenuItem2.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F1, 0));
        jMenuItem2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/icons8-help.png"))); // NOI18N
        jMenuItem2.setText("Aide");
        jMenu2.add(jMenuItem2);

        jMenuBar1.add(jMenu2);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(filtrer, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(10, 10, 10)
                                .addComponent(jLabel6)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(section, javax.swing.GroupLayout.PREFERRED_SIZE, 253, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(classe, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(section, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel2))
                    .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(classe, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(filtrer, javax.swing.GroupLayout.DEFAULT_SIZE, 50, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 390, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel6))
                    .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void nouveauActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nouveauActionPerformed
        Nettoyer();
    }//GEN-LAST:event_nouveauActionPerformed

    private void enregistrerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_enregistrerActionPerformed
//        int aa=Integer.parseInt(code.getText());
        String bb=classe.getSelectedItem().toString();
        String cc=section.getSelectedItem().toString();
        int dd=Mn.SEARCHCODE("code_classe","description","section",bb,cc,"classe");
        for(int i=0;i<tblmatiere.getRowCount();i++){
         String xx=tblmatiere.getValueAt(i,0).toString();
         String xxx=Mn.SEARCHCODE("code_matiere","description",xx,"matiere");
         if(xxx==""){
           xx=Mn.SEARCHCODE("matiere","compose",tblmatiere.getValueAt(i,0).toString(),"matiere_compose");
           xxx=Mn.SEARCHCODE("code_matiere","description",xx,"matiere");  
         }
         String mm="call Insert_classematiere("+dd+","+xxx+")";
            System.out.println(mm);
         Mn.MANIPULEDB(mm);
        }
        nouveau.doClick();
    }//GEN-LAST:event_enregistrerActionPerformed

    private void ajoutersectionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ajoutersectionActionPerformed
        Matiere pr= new Matiere();
        desktop.add(pr);
        int x=(desktop.getWidth()/2)- pr.getWidth()/2;
        int y=(desktop.getHeight()/2)-pr.getHeight()/2;
        pr.setLocation(x, y);
        pr.toFront();
        pr.setVisible(true);
    }//GEN-LAST:event_ajoutersectionActionPerformed

    private void effacerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_effacerActionPerformed
//     int aa=Integer.parseInt(code.getText());
         String bb=classe.getSelectedItem().toString();
        String cc=section.getSelectedItem().toString();
        int dd=Mn.SEARCHCODE("code_classe","description","section",bb,cc,"classe");
     String xx=Mn.SELLECT_BUTTON(Variables.Seleccionado1, tblmatiere);
     String xxx=Mn.SEARCHCODE("code_matiere","description",xx,"matiere");
     String query="delete from matiere_vs_classe where code_classe='"+dd+"'and code_matiere='"+xxx+"'";
     Mn.MANIPULEDB(query);
       try {
                
            query="select * from show_matieres where classe like '%"+classe.getSelectedItem().toString()+"%' and section='"+section.getSelectedItem().toString()+"' and institution="
                    + "'"+Variables.Empresa+"'"+ " group by matiere order by matiere";
            ResultSet rs=Mn.SEARCHDATA(query);
            try {
                mdd.setRowCount(0);
                while(rs.next()){
                 String[] vect={rs.getString("matiere")};
                 Mn.ADD_TABLE(mdd, vect);
                }
            } catch (SQLException ex) {
                Logger.getLogger(Classematiere.class.getName()).log(Level.SEVERE, null, ex);
            }
                
            } catch (Exception e) {
            }
     nouveau.doClick();
    }//GEN-LAST:event_effacerActionPerformed

    private void modifierActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_modifierActionPerformed
        //int aa=Integer.parseInt(code.getText());
        String bb=classe.getSelectedItem().toString();
        String cc=section.getSelectedItem().toString();
        int dd=Mn.SEARCHCODE("code_classe","description","section",bb,cc,"classe");
        for(int i=0;i<tblmatiere.getRowCount();i++){
             String xx=tblmatiere.getValueAt(i,0).toString();
           String xxx=Mn.SEARCHCODE("code_matiere","description",xx,"matiere");
           if(xxx==""){
           xx=Mn.SEARCHCODE("matiere","compose",tblmatiere.getValueAt(i,0).toString(),"matiere_compose");
           xxx=Mn.SEARCHCODE("code_matiere","description",xx,"matiere");  
         }
           String kk="delete from matiere_vs_classe where code_classe='"+dd+"'and code_matiere='"+xxx+"'";
            System.out.println(kk);
           Mn.MANIPULEDB(kk);
        }
        for(int i=0;i<tblmatiere.getRowCount();i++){
           String xx=tblmatiere.getValueAt(i,0).toString();
           String xxx=Mn.SEARCHCODE("code_matiere","description",xx,"matiere");
           if(xxx==""){
           xx=Mn.SEARCHCODE("matiere","compose",tblmatiere.getValueAt(i,0).toString(),"matiere_compose");
           xxx=Mn.SEARCHCODE("code_matiere","description",xx,"matiere");  
         }
           String mm="call Insert_classematiere("+dd+","+xxx+")";
         Mn.MANIPULEDB(mm);
        }
        nouveau.doClick();
        
    }//GEN-LAST:event_modifierActionPerformed

    private void consulterActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_consulterActionPerformed
      Consclassmatiere cs= new Consclassmatiere();
       Menus.Menus.desktop.add(cs);
       int x=(desktop.getWidth()/2)- cs.getWidth()/2;
       int y=(desktop.getHeight()/2)-cs.getHeight()/2;
       cs.setLocation(x, y);
       cs.toFront();
       cs.setVisible(true);
    }//GEN-LAST:event_consulterActionPerformed

    private void addActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addActionPerformed
        String[] vect={matiere.getSelectedItem().toString()};
        String lol="";
        for(int i=0; i<tblmatiere.getRowCount();i++){
            if(matiere.getSelectedItem().toString().equals(tblmatiere.getValueAt(i, 0).toString())){
             JOptionPane.showMessageDialog(this,"Cette matiere est deja existe dans la table, s'il vous plait veuillez choisir une autre.");
             matiere.setSelectedItem("Selectionnez");
             lol="";
            }
        }
        if(!matiere.getSelectedItem().toString().equals("Selectionnez") && tblmatiere.getRowCount()>=0) {
               lol="ajouter";     
            }
        if(lol.equals("ajouter")){
         Mn.ADD_TABLE(md, vect);
        cantmat.setText(String.valueOf(tblmatiere.getRowCount()));
        matiere.setSelectedItem("Selectionnez");  
        }
    }//GEN-LAST:event_addActionPerformed

    private void remActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_remActionPerformed
        Mn.REMOVE_TABLE(tblmatiere, md);
         cantmat.setText(String.valueOf(tblmatiere.getRowCount()));
    }//GEN-LAST:event_remActionPerformed

    private void jLabel6MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel6MouseClicked
       Mn.EMPTYTABLE(tblmatiere);
       cantmat.setText(String.valueOf(tblmatiere.getRowCount()));
    }//GEN-LAST:event_jLabel6MouseClicked

    private void sectionItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_sectionItemStateChanged
      Mn.FULLSCOMBO(classe, "description", "section", section.getSelectedItem().toString(), "classe");

    }//GEN-LAST:event_sectionItemStateChanged

    private void jLabel6MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel6MouseEntered
        setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        enableEvents(MouseEvent.MOUSE_EVENT_MASK);
    }//GEN-LAST:event_jLabel6MouseEntered

    private void classeItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_classeItemStateChanged
      
    }//GEN-LAST:event_classeItemStateChanged

    private void sectionFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_sectionFocusGained
//     Mn.FULLSCOMBO(classe, "description", "section", section.getSelectedItem().toString(), "classe");
     
    }//GEN-LAST:event_sectionFocusGained

    private void sectionFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_sectionFocusLost
//     Mn.FULLSCOMBO(classe, "description", "section", section.getSelectedItem().toString(), "classe");
    }//GEN-LAST:event_sectionFocusLost

    private void classeFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_classeFocusGained
//      Remplir(); 
     
    }//GEN-LAST:event_classeFocusGained

    private void classeMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_classeMouseExited
      
    }//GEN-LAST:event_classeMouseExited

    private void filtrerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_filtrerActionPerformed
       String aa=section.getSelectedItem().toString();
        if(aa.isEmpty()||aa.equals("Selectionnez")){
            classe.setSelectedItem(null);
        }else{
            Remplir();  
            cantmat.setText(String.valueOf(tblmatiere.getRowCount()));
        }
    }//GEN-LAST:event_filtrerActionPerformed

    private void matiereFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_matiereFocusGained
        Mn.FULLCOMBO(matiere,"description","matiere");
    }//GEN-LAST:event_matiereFocusGained

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Classematiere.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Classematiere.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Classematiere.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Classematiere.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Classematiere().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton add;
    private javax.swing.JMenuItem ajoutersection;
    private org.edisoncor.gui.button.ButtonIcon buttonIcon1;
    private org.edisoncor.gui.button.ButtonIpod buttonIpod1;
    private org.edisoncor.gui.button.ButtonPopup buttonPopup1;
    private org.edisoncor.gui.button.ButtonSeven buttonSeven1;
    private org.edisoncor.gui.button.ButtonTask buttonTask1;
    private org.edisoncor.gui.button.ButtonTask buttonTask2;
    private javax.swing.JLabel cantmat;
    public static javax.swing.JComboBox classe;
    private javax.swing.JMenuItem consulter;
    private javax.swing.JMenuItem effacer;
    private javax.swing.JMenuItem enregistrer;
    public static javax.swing.JButton filtrer;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JComboBox matiere;
    private javax.swing.JMenuItem modifier;
    private javax.swing.JMenuItem nouveau;
    private javax.swing.JButton rem;
    public static javax.swing.JComboBox section;
    public static javax.swing.JTable tblmatiere;
    public static javax.swing.JTable tblmatiere1;
    // End of variables declaration//GEN-END:variables
}
