package Forms;

import Methods.Manipulation;
import Methods.MiRender;
import Methods.Rapport;
import Methods.Theme;
import Methods.Variables;
import Processus.Entrer_note;
import Start.Waitts;
import com.ezware.oxbow.swingbits.table.filter.TableRowFilterSupport;
import java.awt.Color;
import java.awt.Component;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumnModel;

public class Paiement extends javax.swing.JInternalFrame {
Manipulation Mn= new Manipulation();
Rapport Rp=new Rapport();
Theme th= new Theme();
String[][] data={};
String[] heads={" Matricule"," Nom"," Prénom"," Date"};
DefaultTableModel mds= new DefaultTableModel(data,heads);

    TableCellRenderer render= new TableCellRenderer() {
 public Component getTableCellRendererComponent(JTable table,Object value,boolean idSelected,boolean hasFocus,int row,int column){
     JLabel lbl= new JLabel(value==null? "": value.toString());
     if(column==0 && row>=0){
         lbl.setHorizontalAlignment(SwingConstants.LEFT);
         lbl.setOpaque(true);
         lbl.setBackground(Color.green);
     }
      if(column>0 && !value.toString().isEmpty()&& row>=0){
         lbl.setHorizontalAlignment(SwingConstants.LEFT);
         lbl.setOpaque(true);
         lbl.setBackground(Color.red);
         lbl.setForeground(Color.yellow);
     }
     return lbl;
 }
};
    public Paiement() {
        initComponents();
        this.setTitle("Scolaris: "+Variables.Empresa+" : Paiement ");
         Scolaire();
        date.setText( Mn.DATEHOURNOW());
        tblpaiement.setModel(mds);
        tblpaiement.setDefaultRenderer(Object.class,new MiRender());
        RemplirCollection();
    //----------------------------------------------------------------
   TableColumnModel cmy= tblpaiement.getColumnModel();
        cmy.getColumn(0).setPreferredWidth(35); 
        cmy.getColumn(1).setPreferredWidth(100);
        cmy.getColumn(2).setPreferredWidth(100);
        cmy.getColumn(3).setPreferredWidth(150);
        tblpaiement.getColumnModel().getColumn(3).setCellRenderer(render);
        TableRowFilterSupport.forTable(tblpaiement).searchable(true).apply();
        
    }

       private void Scolaire(){
    String query="select annee from annee_academique where code_annee\n" +
     "in (select max(code_annee) from annee_academique)";
    ResultSet rs=Mn.SEARCHDATA(query);
    try {
        if(rs.first()){
         academique.setText(rs.getString("annee"));
        }
    } catch (SQLException ex) {
        Logger.getLogger(Entrer_note.class.getName()).log(Level.SEVERE, null, ex);
    }
    }
    
   
   private void RemplirCollection(){
       String query="select e.matricule,t.nom,t.prenom,p.date from employer e left join tercero t on (t.code=e.code and t.code_tercero=e.code_tercero)"
               + " left join paiement p on (p.code=e.code and p.code_tercero=e.code_tercero) where p.annee='"+academique.getText()+"' order by matricule ";
       ResultSet rs=Mn.SEARCHDATA(query);
    try {
        mds.setRowCount(0);
        while(rs.next()){
           String[] vect={rs.getString("matricule"),rs.getString("nom"),rs.getString("prenom"),rs.getString("date")} ;
           Mn.ADD_TABLE(mds, vect);
        }
    } catch (SQLException ex) {
        Logger.getLogger(Paiement.class.getName()).log(Level.SEVERE, null, ex);
    }
   }
       
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        jLabel2 = new javax.swing.JLabel();
        academique = new javax.swing.JLabel();
        date = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        nom = new javax.swing.JLabel();
        mois = new com.toedter.calendar.JMonthChooser();
        status = new javax.swing.JLabel();
        matricule = new javax.swing.JTextField();
        jPanel4 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblpaiement = new javax.swing.JTable(){
            public boolean isCellEditable(int rowIndex, int colIndex){
                return false;
            }
        };
        busqueda = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        jMenuBar1 = new javax.swing.JMenuBar();
        payer = new javax.swing.JMenu();
        save = new javax.swing.JMenuItem();
        jMenu5 = new javax.swing.JMenu();
        aide = new javax.swing.JMenuItem();

        jLabel1.setText("jLabel1");

        setClosable(true);
        setIconifiable(true);
        addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                formMouseMoved(evt);
            }
        });

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel2.setText("Année Académique: ");
        jLabel2.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        academique.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        academique.setText("0");
        academique.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        date.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        date.setText("Date");
        date.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        date.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                dateMouseMoved(evt);
            }
        });

        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel7.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel7.setText("Matricule:");

        jLabel8.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel8.setText("Élève:");

        jLabel10.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel10.setText("Mois:");

        jLabel9.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel9.setText("Status:");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel7, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel8, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel10, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel9, javax.swing.GroupLayout.Alignment.TRAILING))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel7)
                .addGap(18, 18, 18)
                .addComponent(jLabel8)
                .addGap(18, 18, 18)
                .addComponent(jLabel9)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel10)
                .addGap(21, 21, 21))
        );

        jPanel2.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        nom.setBackground(new java.awt.Color(255, 255, 204));
        nom.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        nom.setForeground(new java.awt.Color(0, 0, 255));
        nom.setText("Nom de l'employer");
        nom.setOpaque(true);

        mois.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        mois.setPreferredSize(new java.awt.Dimension(250, 20));

        status.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        matricule.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                matriculeKeyReleased(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(mois, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(nom, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addContainerGap())
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(matricule, javax.swing.GroupLayout.PREFERRED_SIZE, 195, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(status, javax.swing.GroupLayout.PREFERRED_SIZE, 204, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 0, Short.MAX_VALUE))))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(4, 4, 4)
                .addComponent(matricule, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(nom)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(status, javax.swing.GroupLayout.DEFAULT_SIZE, 23, Short.MAX_VALUE)
                .addGap(11, 11, 11)
                .addComponent(mois, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jPanel4.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        tblpaiement.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tblpaiement);

        busqueda.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        busqueda.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                busquedaKeyReleased(evt);
            }
        });

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel3.setText("Recherche : ");
        jLabel3.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(busqueda, javax.swing.GroupLayout.PREFERRED_SIZE, 480, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(busqueda)
                    .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, 29, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 267, Short.MAX_VALUE))
        );

        payer.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/fichier.jpg"))); // NOI18N
        payer.setText("Action");
        payer.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N

        save.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_E, java.awt.event.InputEvent.CTRL_MASK));
        save.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/icons8-save_close.png"))); // NOI18N
        save.setText("Executer");
        save.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveActionPerformed(evt);
            }
        });
        payer.add(save);

        jMenuBar1.add(payer);

        jMenu5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/editer.jpg"))); // NOI18N
        jMenu5.setText("Edition");
        jMenu5.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N

        aide.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F1, 0));
        aide.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/icons8-help.png"))); // NOI18N
        aide.setText("Aide");
        aide.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                aideActionPerformed(evt);
            }
        });
        jMenu5.add(aide);

        jMenuBar1.add(jMenu5);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addComponent(date, javax.swing.GroupLayout.PREFERRED_SIZE, 425, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(308, 308, 308)
                .addComponent(jLabel2)
                .addGap(6, 6, 6)
                .addComponent(academique, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 999, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(11, 11, 11)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(date)
                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(academique, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void dateMouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_dateMouseMoved
      date.setText( Mn.DATEHOURNOW());
    }//GEN-LAST:event_dateMouseMoved

    private void formMouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_formMouseMoved
       date.setText( Mn.DATEHOURNOW());
    }//GEN-LAST:event_formMouseMoved

    private void saveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveActionPerformed
        int aa=0;
        String xs="";
        String ann=academique.getText();
        
        String query = "select e.code,e.code_tercero from employer e left join tercero t on  (t.code = e.code and t.code_tercero = e.code_tercero) "
                + " where e.matricule='"+matricule.getText()+"'";
  
            ResultSet rsrs = Mn.Rezulta(query);
          try {
              if(rsrs.last()){
                  aa = rsrs.getInt("code");
                  xs = rsrs.getString("code_tercero");
              }
          } catch (SQLException ex) {
              Logger.getLogger(Collection.class.getName()).log(Level.SEVERE, null, ex);
          }
        
        
        query="call Insert_paiement("+aa+",'"+xs+"','"+Mn.DATEHOURNOW()+"','"+ann+"')";
        Mn.MANIPULEDB(query);
        RemplirCollection();
        matricule.setText(null);
        nom.setText("Nom de l'employer");
        status.setText(null);
        Waitts attend= new Waitts(null, rootPaneCheckingEnabled);
        attend.show();
       // Rp.SENDQUERY("Paiements",matricule.getText(),academique.getText());
    }//GEN-LAST:event_saveActionPerformed

    private void matriculeKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_matriculeKeyReleased
        matricule.setText(matricule.getText().toUpperCase());
        String query="select t.nom,t.prenom,e.estatuses from tercero t left join employer e "
        + "on (e.code=t.code and e.code_tercero=t.code_tercero) where e.matricule = '"+matricule.getText()+"' ";
        ResultSet rs=Mn.SEARCHDATA(query);
        try {
            while(rs.next()){
                nom.setText(rs.getString("nom")+" "+rs.getString("prenom"));
                if(rs.getBoolean("estatuses")==true){
                    status.setForeground(Color.green);
                    status.setText("Actif");
                }else{
                    status.setForeground(Color.red);
                    status.setText("Inactif");
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(Professeur.class.getName()).log(Level.SEVERE, null, ex);
        }
        String querys="select e.matricule,t.nom,t.prenom,p.date from employer e left join tercero t on (t.code=e.code and t.code_tercero=e.code_tercero)"
               + " left join paiement p on (p.code=e.code and p.code_tercero=e.code_tercero) "
                + "where e.matricule like '%"+matricule.getText()+"%' and p.annee='"+academique.getText()+"' order by matricule ";
       ResultSet rss=Mn.SEARCHDATA(querys);
    try {
        mds.setRowCount(0);
        while(rss.next()){
           String[] vect={rs.getString("matricule"),rs.getString("nom"),rs.getString("prenom"),rs.getString("date")} ;
           Mn.ADD_TABLE(mds, vect);
        }
        if(tblpaiement.getRowCount()==0){
            RemplirCollection();
        }
    } catch (SQLException ex) {
        Logger.getLogger(Paiement.class.getName()).log(Level.SEVERE, null, ex);
    }
    }//GEN-LAST:event_matriculeKeyReleased

    private void busquedaKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_busquedaKeyReleased
       busqueda.setText(busqueda.getText().toUpperCase());
            if(busqueda.getText().isEmpty()){
              String query="select e.matricule,t.nom,t.prenom,p.date from employer e left join tercero t on (t.code=e.code and t.code_tercero=e.code_tercero)"
             + " left join paiement p on (p.code=e.code and p.code_tercero=e.code_tercero) where p.annee='"+academique.getText()+"'  order by matricule ";
             ResultSet rs=Mn.SEARCHDATA(query);
         try {
             mds.setRowCount(0);
             while(rs.next()){
                String[] vect={rs.getString("matricule"),rs.getString("nom"),rs.getString("prenom"),rs.getString("date")} ;
                Mn.ADD_TABLE(mds, vect);
             }
         } catch (SQLException ex) {
             Logger.getLogger(Paiement.class.getName()).log(Level.SEVERE, null, ex);
         }   
       }else{
        String query="select e.matricule,t.nom,t.prenom,p.date from employer e left join tercero t on (t.code=e.code and t.code_tercero=e.code_tercero)"
        + " left join paiement p on (p.code=e.code and p.code_tercero=e.code_tercero) where p.annee='"+academique.getText()+"' "
        + " and e.matricule like '%"+busqueda.getText()+"%' or t.nom like '%"+busqueda.getText()+"%' or t.prenom like '%"+busqueda.getText()+"%' order by matricule ";
        ResultSet rs=Mn.SEARCHDATA(query);
    try {
        mds.setRowCount(0);
        while(rs.next()){
           String[] vect={rs.getString("matricule"),rs.getString("nom"),rs.getString("prenom"),rs.getString("date")} ;
           Mn.ADD_TABLE(mds, vect);
        }
    } catch (SQLException ex) {
        Logger.getLogger(Paiement.class.getName()).log(Level.SEVERE, null, ex);
    } 
       }

    }//GEN-LAST:event_busquedaKeyReleased

    private void aideActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_aideActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_aideActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Paiement.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Paiement.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Paiement.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Paiement.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Paiement().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel academique;
    private javax.swing.JMenuItem aide;
    private javax.swing.JTextField busqueda;
    private javax.swing.JLabel date;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JMenu jMenu5;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JTextField matricule;
    private com.toedter.calendar.JMonthChooser mois;
    private javax.swing.JLabel nom;
    private javax.swing.JMenu payer;
    private javax.swing.JMenuItem save;
    private javax.swing.JLabel status;
    private javax.swing.JTable tblpaiement;
    // End of variables declaration//GEN-END:variables
}
