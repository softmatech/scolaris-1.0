/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Forms;

import Methods.Manipulation;
import Methods.Rapport;
import Methods.Theme;
import Methods.Variables;
import Processus.Entrer_note;
import Start.Waitts;
import com.ezware.oxbow.swingbits.table.filter.TableRowFilterSupport;
import java.awt.Color;
import java.awt.Component;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumnModel;
import javax.swing.JOptionPane;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

/**
 *
 * @author josephandyfeidje
 */
public class CollectionForm extends javax.swing.JFrame {
Manipulation Mn= new Manipulation();
Theme th= new Theme();
Rapport Rp=new Rapport();
String mat,name_ = "";
String[][] data={};
String[] heads={" Matricule"," Nom"," Prénom"," Classes"," Versement"," Date"," Balance"};
//.............................................................................................
String[][] data2 = {};
String[] heads2 = {" Date de Versement"," Versement"," Balance"," Classes"};
//...........................................................................................
DefaultTableModel mds= new DefaultTableModel(data,heads);
DefaultTableModel mds2= new DefaultTableModel(data2,heads2);
  TableCellRenderer render= new TableCellRenderer() {
 public Component getTableCellRendererComponent(JTable table,Object value,boolean idSelected,boolean hasFocus,int row,int column){
     JLabel lbl= new JLabel(value==null? "": value.toString());
      if(column==0 && row>=0){
         lbl.setHorizontalAlignment(SwingConstants.LEFT);
         lbl.setOpaque(true);
         lbl.setBackground(Color.green);
     }
       if(column==4 && row>=0){
         lbl.setHorizontalAlignment(SwingConstants.LEFT);
         lbl.setOpaque(true);
         lbl.setBackground(Color.yellow);
     }
     if(column==6 && row>=0){
         lbl.setHorizontalAlignment(SwingConstants.LEFT);
         lbl.setOpaque(true);
         lbl.setBackground(Color.yellow);
     }
      if(column==6 && value.toString().equals("0 Gdes") && row>=0){
         lbl.setHorizontalAlignment(SwingConstants.LEFT);
         lbl.setOpaque(true);
         lbl.setBackground(Color.red);
     }
     return lbl;
 }
};
    public CollectionForm() {
        initComponents();
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setLocationRelativeTo(this);
        this.setTitle("Scolaris: "+Variables.Empresa+" : Collection ");
        Mn.FULLCOMBO(classe,"description","classe");
        classe.addItem("Selectionnez");
        classe.setSelectedItem("Selectionnez");
         Scolaire();
        date.setText( Mn.DATEHOURNOW());
//        Mn.MATRICULE_FORMAT(matricule);
        tblcollection.setModel(mds);
        jTable1.setModel(mds2);
        RemplirCollection();
        tableChangeListener();
      //----------------------------------------------------------------
   TableColumnModel cmy= tblcollection.getColumnModel();
        cmy.getColumn(0).setPreferredWidth(15);
        cmy.getColumn(1).setPreferredWidth(70);
        cmy.getColumn(2).setPreferredWidth(90);
        cmy.getColumn(3).setPreferredWidth(50);
        cmy.getColumn(4).setPreferredWidth(10);
        cmy.getColumn(5).setPreferredWidth(120);
        cmy.getColumn(6).setPreferredWidth(10);
         tblcollection.getColumnModel().getColumn(0).setCellRenderer(render);
          tblcollection.getColumnModel().getColumn(4).setCellRenderer(render);
        tblcollection.getColumnModel().getColumn(6).setCellRenderer(render);
        mesaj.setVisible(false);
        TableRowFilterSupport.forTable(tblcollection).searchable(true).apply();
    }
    
    private void tableChangeListener() {
        try {

        } catch (ArrayIndexOutOfBoundsException e) {
        }

        tblcollection.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                try {
                 mat = tblcollection.getValueAt(tblcollection.getSelectedRow(), 0).toString();
                 name_ = tblcollection.getValueAt(tblcollection.getSelectedRow(), 1).toString() + " "+tblcollection.getValueAt(tblcollection.getSelectedRow(), 2).toString();
                 matricule.setText(mat);
                 nom.setText(name_);
                } catch (IndexOutOfBoundsException | NullPointerException ec) {
                }

            }
        });
    }

   private void Scolaire(){
    String query="select annee from annee_academique where code_annee\n" +
     "in (select max(code_annee) from annee_academique)";
    ResultSet rs=Mn.SEARCHDATA(query);
    try {
        if(rs.first()){
         academique.setText(rs.getString("annee"));
        }
    } catch (SQLException ex) {
        Logger.getLogger(Entrer_note.class.getName()).log(Level.SEVERE, null, ex);
    }
    }
   
   private double getOldVersement(String matricule){
       double versement = 0.00;
       String sql = "select versement from show_collecter where matricule = '"+matricule+"' ";
       
       ResultSet rs = Mn.Rezulta(sql);
    try {
        if(rs.last()){
            versement = rs.getDouble("versement");
        }
    } catch (SQLException ex) {
        Logger.getLogger(CollectionForm.class.getName()).log(Level.SEVERE, null, ex);
    }
       return versement;
   }
   
   private void RemplirDetailsCollection(String mat){
 String query="select * from show_details_collecter where matricule = '"+mat+"' and anne='"+academique.getText()+"' order by nom,date_versement ";
       System.out.println("query---->> "+query);      
 ResultSet rs=Mn.SEARCHDATA(query);
    try {
        mds.setRowCount(0);
        while(rs.next()){
           String[] vect={rs.getString("date_versement"),rs.getString("versement")+" Gdes",
           rs.getString("balance")+" Gdes",rs.getString("classe")} ;
           Mn.ADD_TABLE(mds2, vect);
           //jLabel10.setText(Mn.tableRowcount(tblcollection));
        }
        if(mds2.getRowCount() == 0 ){
            JOptionPane.showMessageDialog(null, "Pas de correspondance pour dans la grille");
        }
    } catch (SQLException ex) {
        Logger.getLogger(Collection.class.getName()).log(Level.SEVERE, null, ex);
    }
   }
   
   private void RemplirCollection(String mat){
 String query="select * from show_collecter where matricule = '"+mat+"' and anne='"+academique.getText()+"' order by nom,date_versement ";
       System.out.println("query---->> "+query);      
 ResultSet rs=Mn.SEARCHDATA(query);
    try {
        mds.setRowCount(0);
        while(rs.next()){
           String[] vect={rs.getString("matricule"),rs.getString("nom"),rs.getString("prenom"),rs.getString("classe"),rs.getString("versement")+" Gdes",
           rs.getString("date_versement"),rs.getString("balance")+" Gdes"} ;
           Mn.ADD_TABLE(mds, vect);
           jLabel10.setText(Mn.tableRowcount(tblcollection));
        }
        if(mds.getRowCount() == 0 ){
            JOptionPane.showMessageDialog(null, "Pas de correspondance pour paiement");
        }
    } catch (SQLException ex) {
        Logger.getLogger(Collection.class.getName()).log(Level.SEVERE, null, ex);
    }
   }
   
      private void RemplirCollection(){
 String query="select * from show_collecter where anne='"+academique.getText()+"' order by nom,date_versement desc ";
       System.out.println("query---->> "+query);      
 ResultSet rs=Mn.SEARCHDATA(query);
    try {
        mds.setRowCount(0);
        while(rs.next()){
           String[] vect={rs.getString("matricule"),rs.getString("nom"),rs.getString("prenom"),rs.getString("classe"),rs.getString("versement")+" Gdes",
           rs.getString("date_versement"),rs.getString("balance")+" Gdes"} ;
           Mn.ADD_TABLE(mds, vect);
        }
        jLabel10.setText(Mn.tableRowcount(tblcollection));
    } catch (SQLException ex) {
        Logger.getLogger(Collection.class.getName()).log(Level.SEVERE, null, ex);
    }
   }
   
   private void ImprimerCollection(){
   Waitts attend= new Waitts(null, rootPaneCheckingEnabled);
   attend.show();
   Rp.SENDQUERY("Collection",matricule.getText(),academique.getText());
   }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPopupMenu1 = new javax.swing.JPopupMenu();
        jMenuItem2 = new javax.swing.JMenuItem();
        jSeparator1 = new javax.swing.JPopupMenu.Separator();
        jMenuItem3 = new javax.swing.JMenuItem();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        date = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        academique = new javax.swing.JLabel();
        jPanel5 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        matricule = new javax.swing.JTextField();
        nom = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        versement = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        SEARCH = new javax.swing.JButton();
        jPanel6 = new javax.swing.JPanel();
        mesaj = new javax.swing.JLabel();
        jPanel7 = new javax.swing.JPanel();
        jLabel8 = new javax.swing.JLabel();
        classe = new javax.swing.JComboBox();
        jLabel9 = new javax.swing.JLabel();
        recherche = new javax.swing.JTextField();
        jPanel8 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblcollection = new javax.swing.JTable(){
            public boolean isCellEditable(int rowIndex, int colIndex){
                return false;
            }
        };
        jPanel12 = new javax.swing.JPanel();
        jLabel10 = new javax.swing.JLabel();
        jPanel9 = new javax.swing.JPanel();
        jPanel10 = new javax.swing.JPanel();
        recherch = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        nameLabel = new javax.swing.JLabel();
        jPanel11 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable(){
            public boolean isCellEditable(int rowIndex, int colIndex){
                return false;
            }
        };
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenuItem1 = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        jMenuItem7 = new javax.swing.JMenuItem();

        jMenuItem2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/ButtonFoto/icons8-connection_sync.png"))); // NOI18N
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem2ActionPerformed(evt);
            }
        });
        jPopupMenu1.add(jMenuItem2);
        jPopupMenu1.add(jSeparator1);

        jMenuItem3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/ButtonFoto/icons8-human_esearch_rogram.png"))); // NOI18N
        jMenuItem3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem3ActionPerformed(evt);
            }
        });
        jPopupMenu1.add(jMenuItem3);

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(new javax.swing.BoxLayout(getContentPane(), javax.swing.BoxLayout.LINE_AXIS));

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));

        jPanel2.setBackground(new java.awt.Color(87, 148, 210));
        jPanel2.setBorder(new org.edisoncor.gui.util.DropShadowBorder());

        jLabel5.setBackground(new java.awt.Color(255, 255, 255));
        jLabel5.setFont(new java.awt.Font("Lucida Grande", 1, 18)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(255, 255, 255));
        jLabel5.setText("Paiement");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel5)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(26, Short.MAX_VALUE))
        );

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));
        jPanel3.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        date.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        date.setText("Date");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(date, javax.swing.GroupLayout.DEFAULT_SIZE, 369, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(date)
        );

        jPanel4.setBackground(new java.awt.Color(255, 255, 255));
        jPanel4.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel2.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        jLabel2.setText("Année Académique: ");

        academique.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        academique.setText("0");

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel2)
                .addGap(18, 18, 18)
                .addComponent(academique, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(academique))
                .addGap(0, 0, Short.MAX_VALUE))
        );

        jPanel5.setBackground(new java.awt.Color(255, 255, 255));
        jPanel5.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jPanel5MouseExited(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jPanel5MouseEntered(evt);
            }
        });

        jLabel1.setText("Matricule : ");

        matricule.setBorder(new org.edisoncor.gui.util.DropShadowBorder());
        matricule.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                matriculeMouseMoved(evt);
            }
        });
        matricule.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                matriculeFocusLost(evt);
            }
        });
        matricule.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                matriculeKeyReleased(evt);
            }
        });

        nom.setText("Nom de l'elève : ");

        jLabel4.setText("Versement : ");

        versement.setBorder(new org.edisoncor.gui.util.DropShadowBorder());
        versement.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                versementActionPerformed(evt);
            }
        });

        jLabel6.setText("Gdes");

        SEARCH.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/ButtonFoto/icons8-human_esearch_rogram.png"))); // NOI18N
        SEARCH.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                SEARCHActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(matricule, javax.swing.GroupLayout.DEFAULT_SIZE, 172, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(SEARCH, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(nom, javax.swing.GroupLayout.PREFERRED_SIZE, 274, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(versement, javax.swing.GroupLayout.PREFERRED_SIZE, 113, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel6)
                .addGap(45, 45, 45))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(SEARCH, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(nom, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(versement)
                            .addComponent(jLabel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addContainerGap())
                    .addComponent(matricule, javax.swing.GroupLayout.Alignment.TRAILING)))
        );

        jPanel6.setBackground(new java.awt.Color(87, 148, 210));
        jPanel6.setLayout(new java.awt.GridLayout(1, 0));

        mesaj.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        mesaj.setForeground(new java.awt.Color(255, 255, 255));
        mesaj.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        mesaj.setText("........");
        jPanel6.add(mesaj);

        jPanel7.setBackground(new java.awt.Color(255, 255, 255));
        jPanel7.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jPanel7MouseExited(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jPanel7MouseEntered(evt);
            }
        });

        jLabel8.setText("Classe : ");

        classe.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Selectionnez" }));

        jLabel9.setText("Recherche : ");

        recherche.setBorder(new org.edisoncor.gui.util.DropShadowBorder());
        recherche.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                rechercheMouseMoved(evt);
            }
        });
        recherche.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rechercheActionPerformed(evt);
            }
        });
        recherche.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                rechercheKeyReleased(evt);
            }
        });

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel8)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(classe, javax.swing.GroupLayout.PREFERRED_SIZE, 241, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel9)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(recherche, javax.swing.GroupLayout.PREFERRED_SIZE, 279, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel8)
                        .addComponent(classe, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel9))
                    .addComponent(recherche, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel8.setComponentPopupMenu(jPopupMenu1);
        jPanel8.setLayout(new java.awt.CardLayout());

        jScrollPane1.setComponentPopupMenu(jPopupMenu1);

        tblcollection.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tblcollection.setComponentPopupMenu(jPopupMenu1);
        jScrollPane1.setViewportView(tblcollection);

        jPanel8.add(jScrollPane1, "card2");

        jPanel12.setBackground(new java.awt.Color(255, 255, 255));
        jPanel12.setLayout(new java.awt.CardLayout());

        jLabel10.setText("Nombre(s) de registre(s) : 0");
        jPanel12.add(jLabel10, "card2");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jPanel8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel2, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel5, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel6, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel7, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                        .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                    .addContainerGap(686, Short.MAX_VALUE)
                    .addComponent(jPanel12, javax.swing.GroupLayout.PREFERRED_SIZE, 188, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap()))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, 296, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                    .addContainerGap(510, Short.MAX_VALUE)
                    .addComponent(jPanel12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap()))
        );

        jTabbedPane1.addTab("Paiement", jPanel1);

        jPanel9.setBackground(new java.awt.Color(255, 255, 255));

        jPanel10.setBackground(new java.awt.Color(87, 148, 210));
        jPanel10.setBorder(new org.edisoncor.gui.util.DropShadowBorder());

        jLabel3.setFont(new java.awt.Font("Lucida Grande", 1, 18)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel3.setText("Recherche");

        nameLabel.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        nameLabel.setForeground(new java.awt.Color(255, 255, 255));
        nameLabel.setText(".");

        javax.swing.GroupLayout jPanel10Layout = new javax.swing.GroupLayout(jPanel10);
        jPanel10.setLayout(jPanel10Layout);
        jPanel10Layout.setHorizontalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel10Layout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addComponent(nameLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(recherch, javax.swing.GroupLayout.PREFERRED_SIZE, 288, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel10Layout.setVerticalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel10Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel10Layout.createSequentialGroup()
                        .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(recherch, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel3))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(nameLabel, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        jPanel11.setLayout(new javax.swing.BoxLayout(jPanel11, javax.swing.BoxLayout.LINE_AXIS));

        jScrollPane2.setBackground(new java.awt.Color(204, 204, 204));

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane2.setViewportView(jTable1);

        jPanel11.add(jScrollPane2);

        javax.swing.GroupLayout jPanel9Layout = new javax.swing.GroupLayout(jPanel9);
        jPanel9.setLayout(jPanel9Layout);
        jPanel9Layout.setHorizontalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel10, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel11, javax.swing.GroupLayout.DEFAULT_SIZE, 868, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel9Layout.setVerticalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addComponent(jPanel10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel11, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        jTabbedPane1.addTab("Historiques", jPanel9);

        getContentPane().add(jTabbedPane1);
        jTabbedPane1.getAccessibleContext().setAccessibleName("Paiement");

        jMenu1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/ButtonFoto/icons8-menu.png"))); // NOI18N
        jMenu1.setText("Action");

        jMenuItem1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/ButtonFoto/icons8-upload_to_cloud.png"))); // NOI18N
        jMenuItem1.setText("Enregistrer");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem1);

        jMenuBar1.add(jMenu1);

        jMenu2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/ButtonFoto/icons8-menu_squared.png"))); // NOI18N
        jMenu2.setText("Edition");

        jMenuItem7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/ButtonFoto/icons8-help.png"))); // NOI18N
        jMenuItem7.setText("Aide");
        jMenu2.add(jMenuItem7);

        jMenuBar1.add(jMenu2);

        setJMenuBar(jMenuBar1);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void rechercheActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rechercheActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_rechercheActionPerformed

    private void rechercheKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_rechercheKeyReleased
        recherche.setText(recherche.getText().toUpperCase());
        if(recherche.getText().isEmpty() && classe.getSelectedItem().toString().equals("Selectionnez")){
            //RemplirCollection();
        }else if(!recherche.getText().isEmpty() && !classe.getSelectedItem().toString().equals("Selectionnez")){
    String query="select distinct e.matricule,t.nom,t.prenom,cl.description as classe,c.versement,c.date_versement as date,c.balance \n" +
"from inscription i left join collecter c on (c.code = i.code and c.code_tercero = i.code_tercero) left join classe cl on i.code_classe\n" +
"=cl.code_classe left join tercero t on (t.code = i.code and t.code_tercero = i.code_tercero) left join eleve e on "
+ " (e.code = i.code and e.code_tercero = i.code_tercero)  where c.anne='"+academique.getText()+"' and e.matricule like '%"+recherche.getText()+"%' and cl.description"
            + "='"+classe.getSelectedItem().toString()+"' or t.nom like '%"+recherche.getText()+"%'"
   + " or t.prenom like '%"+recherche.getText()+"%' or c.date_versement like '%"+recherche.getText()+"%' order by nom,date_versement ";
       ResultSet rs=Mn.SEARCHDATA(query);
    try {
        mds.setRowCount(0);
        while(rs.next()){
            String[] vect={rs.getString("matricule"),rs.getString("nom"),rs.getString("prenom"),rs.getString("classe"),rs.getString("versement")+" Gdes",
           rs.getString("date"),rs.getString("balance")+" Gdes"} ;
           Mn.ADD_TABLE(mds, vect);
        }
    } catch (SQLException ex) {
        Logger.getLogger(Collection.class.getName()).log(Level.SEVERE, null, ex);
    }}else if(recherche.getText().isEmpty() && !classe.getSelectedItem().toString().equals("Selectionnez")){
    String query="select distinct e.matricule,t.nom,t.prenom,cl.description as classe,c.versement,c.date_versement as date,c.balance \n" +
"from inscription i left join collecter c on (c.code = i.code and c.code_tercero = i.code_tercero) left join classe cl on i.code_classe\n" +
"=cl.code_classe left join tercero t on (t.code = i.code and t.code_tercero = i.code_tercero) left join eleve e on "
+ " (e.code = i.code and e.code_tercero = i.code_tercero)  where c.anne='"+academique.getText()+"' and cl.description='"+classe.getSelectedItem().toString()+"' order by nom,date_versement ";
       ResultSet rs=Mn.SEARCHDATA(query);
    try {
        mds.setRowCount(0);
        while(rs.next()){
            String[] vect={rs.getString("matricule"),rs.getString("nom"),rs.getString("prenom"),rs.getString("classe"),rs.getString("versement")+" Gdes",
           rs.getString("date"),rs.getString("balance")+" Gdes"} ;
           Mn.ADD_TABLE(mds, vect);
        }
    } catch (SQLException ex) {
        Logger.getLogger(Collection.class.getName()).log(Level.SEVERE, null, ex);
    }
    }else if(!recherche.getText().isEmpty() && classe.getSelectedItem().toString().equals("Selectionnez")){
    String query="select distinct e.matricule,t.nom,t.prenom,cl.description as classe,c.versement,c.date_versement as date,c.balance \n" +
"from inscription i left join collecter c on (c.code = i.code and c.code_tercero = i.code_tercero) left join classe cl on i.code_classe\n" +
"=cl.code_classe left join tercero t on (t.code = i.code and t.code_tercero = i.code_tercero) left join eleve e on "
+ " (e.code = i.code and e.code_tercero = i.code_tercero)  where c.anne='"+academique.getText()+"' and e.matricule like '%"+recherche.getText()+"%' and cl.description"
            + "='"+classe.getSelectedItem().toString()+"' or t.nom like '%"+recherche.getText()+"%'"
   + " or t.prenom like '%"+recherche.getText()+"%' or c.date_versement like '%"+recherche.getText()+"%' order by nom,date_versement ";
       ResultSet rs=Mn.SEARCHDATA(query);
    try {
        mds.setRowCount(0);
        while(rs.next()){
            String[] vect={rs.getString("matricule"),rs.getString("nom"),rs.getString("prenom"),rs.getString("classe"),rs.getString("versement")+" Gdes",
           rs.getString("date"),rs.getString("balance")+" Gdes"} ;
           Mn.ADD_TABLE(mds, vect);
        }
    } catch (SQLException ex) {
        Logger.getLogger(Collection.class.getName()).log(Level.SEVERE, null, ex);
    }}
    }//GEN-LAST:event_rechercheKeyReleased

    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
        proceedToPayment();
    }//GEN-LAST:event_jMenuItem1ActionPerformed

    private void matriculeKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_matriculeKeyReleased
        
    }//GEN-LAST:event_matriculeKeyReleased

    private void jPanel5MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel5MouseEntered
        Mn.setPanTxtColor(jPanel5,matricule);
        Mn.setPanTxtColor(jPanel5, versement);
    }//GEN-LAST:event_jPanel5MouseEntered

    private void jPanel5MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel5MouseExited
        Mn.resetPanTxtColor(jPanel5,matricule);
        Mn.resetPanTxtColor(jPanel5, versement);
    }//GEN-LAST:event_jPanel5MouseExited

    private void matriculeMouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_matriculeMouseMoved
        jPanel5MouseEntered(evt);
    }//GEN-LAST:event_matriculeMouseMoved

    private void jPanel7MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel7MouseEntered
        Mn.setPanTxtColor(jPanel7,recherche);
    }//GEN-LAST:event_jPanel7MouseEntered

    private void rechercheMouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_rechercheMouseMoved
        jPanel7MouseEntered(evt);
    }//GEN-LAST:event_rechercheMouseMoved

    private void jPanel7MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel7MouseExited
        Mn.resetPanTxtColor(jPanel7, recherche);
    }//GEN-LAST:event_jPanel7MouseExited

    private void SEARCHActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_SEARCHActionPerformed
        searchMatriculeData(matricule.getText().trim());
    }//GEN-LAST:event_SEARCHActionPerformed

    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem2ActionPerformed
        RemplirCollection();
    }//GEN-LAST:event_jMenuItem2ActionPerformed

    private void matriculeFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_matriculeFocusLost
        searchMatriculeData(matricule.getText().trim());
    }//GEN-LAST:event_matriculeFocusLost

    private void jMenuItem3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem3ActionPerformed
        if(tblcollection.getSelectedRow() >= 0){
            jTabbedPane1.setSelectedIndex(1);
            nameLabel.setText("Matricule : "+mat+"      Nom : "+name_);
        RemplirDetailsCollection(mat);
        }
        else{
            JOptionPane.showMessageDialog(null, "Desolé");
        }
    }//GEN-LAST:event_jMenuItem3ActionPerformed

    private void versementActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_versementActionPerformed
        proceedToPayment();
    }//GEN-LAST:event_versementActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(CollectionForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(CollectionForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(CollectionForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(CollectionForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new CollectionForm().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton SEARCH;
    private javax.swing.JLabel academique;
    private javax.swing.JComboBox classe;
    private javax.swing.JLabel date;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JMenuItem jMenuItem3;
    private javax.swing.JMenuItem jMenuItem7;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel11;
    private javax.swing.JPanel jPanel12;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JPopupMenu jPopupMenu1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JPopupMenu.Separator jSeparator1;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JTable jTable1;
    private javax.swing.JTextField matricule;
    private javax.swing.JLabel mesaj;
    private javax.swing.JLabel nameLabel;
    private javax.swing.JLabel nom;
    private javax.swing.JTextField recherch;
    private javax.swing.JTextField recherche;
    private javax.swing.JTable tblcollection;
    private javax.swing.JTextField versement;
    // End of variables declaration//GEN-END:variables

    private void searchMatriculeData(String _matricule) {
        matricule.setText(_matricule.toUpperCase());
        nom.setText(null);
        String query="select t.nom,t.prenom from tercero t left join eleve e "
        + "on (e.code=t.code and e.code_tercero=t.code_tercero) where e.matricule = '"+_matricule+"'";
        System.out.println("ke -->> "+query);
        ResultSet rs=Mn.SEARCHDATA(query);
        try {
            if(rs.next()){
            if(rs.last()){
                nom.setText(rs.getString("nom")+" "+rs.getString("prenom"));
                RemplirCollection(_matricule);
                mesaj.setVisible(false);
            }
            }
            else{
                JOptionPane.showMessageDialog(null,"Pas de correspondance dans le Système");
                //Mn.EMPTYTABLE(tblcollection);
            }
                
        } catch (SQLException ex) {
            Logger.getLogger(Professeur.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void proceedToPayment() {
        String section="";
       double montant=0.0;
       double balance=0.0;
       String versment="";
       double total_versement = 0.0;
       int quantite=0;
       int bb=0;
       String xs="";
       String aa=matricule.getText();
        String query = "select e.code,e.code_tercero from eleve e left join tercero t on  (t.code = e.code and t.code_tercero = e.code_tercero) "
          + "join eleve_has_institution ehi on (e.code = ehi.code and e.code_tercero = ehi.code_tercero) "
          + "join institution i on(ehi.idinstitution = i.idinstitution) where e.matricule='"+aa+"' and i.nom = '"+Variables.Empresa+"'";
  
        System.out.println("que---->> "+query);  
      ResultSet rsrs = Mn.Rezulta(query);
    try {
        if(rsrs.last()){
            bb = rsrs.getInt("code");
            xs = rsrs.getString("code_tercero");
        }
    } catch (SQLException ex) {
        Logger.getLogger(Collection.class.getName()).log(Level.SEVERE, null, ex);
    }
    
     query="select c.section from classe c left join inscription i on i.code_classe=c.code_classe \n" +
       "where i.code="+bb+"  and i.code_tercero='"+xs+"' ";
       ResultSet rs=Mn.SEARCHDATA(query);
    try {
        if(rs.first()){
          section=rs.getString("section");
        }
    } catch (SQLException ex) {
        Logger.getLogger(Collection.class.getName()).log(Level.SEVERE, null, ex);
    }
     
    String querys="select montant,versement from collection where section='"+section+"'";
        System.out.println("queryyyy--->> "+querys);
       ResultSet rss=Mn.SEARCHDATA(querys);
    try {
        if(rss.first()){
          montant=rss.getDouble("montant");
          versment=rss.getString("versement"); System.out.println("vrsment -->> "+versment);
        }
    } catch (SQLException ex) {
        Logger.getLogger(Collection.class.getName()).log(Level.SEVERE, null, ex);
    }
    
     String queerys="select balance,quantite_versement  from collecter where matricule='"+matricule.getText()+"' and anne='"+academique.getText()+"'";
       ResultSet rsss=Mn.SEARCHDATA(queerys);
    try {
        if(rsss.last()){
             if(rsss.getDouble("balance")>=0){
              balance=rsss.getDouble("balance")-Double.valueOf(versement.getText()); 
              total_versement = getOldVersement(aa)  + Double.valueOf(versement.getText());
              quantite=rsss.getInt("quantite_versement");
            if(quantite < montant){
              quantite++;  
            }
            }
        }else
        { 
            quantite++;  
            balance = montant - Double.valueOf(versement.getText());  
        }
    } catch (SQLException ex) {
        Logger.getLogger(Collection.class.getName()).log(Level.SEVERE, null, ex);
    }
    
    double cc = Double.valueOf(versement.getText());
    //Date dd = new Date(); 
    String annee=academique.getText();
    String sql = "call Insert_collecter('"+aa+"','"+bb+"','"+xs+"','"+total_versement+"','"+quantite+"','"+balance+"','"+annee+"')";
    String sql2 = "call Insert_details_collecter('"+aa+"','"+bb+"','"+xs+"','"+cc+"','"+quantite+"','"+balance+"','"+annee+"')";
        
    System.out.println("Insert---->> "+sql);
    if(balance >= 0){
      mesaj.setVisible(false);
      Mn.MANIPULEDB(sql);
      Mn.MANIPULEDB(sql2);
    }else{
        mesaj.setVisible(true);
        mesaj.setText("Cet(te) élève est en règle avec l'économat.");
    }
    RemplirCollection(matricule.getText().trim());
    //ImprimerCollection();
    matricule.setText(null);
    nom.setText("Nom de l'élève");
    versement.setText(null);
    }
}
