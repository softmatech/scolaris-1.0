package Forms;

import Methods.Manipulation;
import Methods.Variables;
import com.ezware.oxbow.swingbits.table.filter.TableRowFilterSupport;
import java.awt.Point;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;


public class Ouvrages extends javax.swing.JInternalFrame {
    Manipulation Mn= new Manipulation();
    String[][] data={};
    String[] head={"Code","Ouvrages","Auteurs"};
    DefaultTableModel md= new DefaultTableModel(data, head);
    public Ouvrages() {
        initComponents();
        this.setTitle("Scolaris: "+Variables.Empresa+" : Registre d' ouvrages");
        nouveau.doClick();
        tblouvrage.setModel(md);
       code.setEditable(false);
         TableColumnModel cm= tblouvrage.getColumnModel();
        cm.getColumn(0).setPreferredWidth(5);
        cm.getColumn(1).setPreferredWidth(200);
        cm.getColumn(2).setPreferredWidth(30);
        nouveau.doClick();
        Ramplir();
TableRowFilterSupport.forTable(tblouvrage).searchable(true).apply();
    }
    
     private void Nettoyer(){
       ouvrage.setText(null);
       auteur.setText(null);
       recherche.setText(null);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        jScrollPane1 = new javax.swing.JScrollPane();
        tblouvrage = new javax.swing.JTable(){
            public boolean isCellEditable(int rowIndex, int colIndex){
                return false;
            }
        };
        jSeparator1 = new javax.swing.JSeparator();
        jPanel1 = new javax.swing.JPanel();
        code = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        ouvrage = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        auteur = new javax.swing.JTextField();
        jPanel3 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        recherche = new javax.swing.JTextField();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        enregistrer = new javax.swing.JMenuItem();
        nouveau = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        modifier = new javax.swing.JMenuItem();
        jMenuItem1 = new javax.swing.JMenuItem();

        setClosable(true);
        setIconifiable(true);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        tblouvrage.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tblouvrage.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                tblouvrageMousePressed(evt);
            }
        });
        jScrollPane1.setViewportView(tblouvrage);

        getContentPane().add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 152, 695, 312));
        getContentPane().add(jSeparator1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 100, 695, -1));

        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel1.setLayout(new java.awt.GridBagLayout());

        code.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridheight = 2;
        gridBagConstraints.ipadx = 248;
        gridBagConstraints.ipady = 19;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(5, 12, 7, 341);
        jPanel1.add(code, gridBagConstraints);

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel1.setText("Code");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.ipadx = 43;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(16, 2, 0, 0);
        jPanel1.add(jLabel1, gridBagConstraints);

        getContentPane().add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 0, 680, 50));

        jPanel2.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel2.setLayout(new java.awt.GridBagLayout());

        ouvrage.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridheight = 2;
        gridBagConstraints.ipadx = 295;
        gridBagConstraints.ipady = 19;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(6, 4, 6, 0);
        jPanel2.add(ouvrage, gridBagConstraints);

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel2.setText("Ouvrage");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.ipadx = 27;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(17, 4, 0, 0);
        jPanel2.add(jLabel2, gridBagConstraints);

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel3.setText("Auteur");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(17, 12, 0, 0);
        jPanel2.add(jLabel3, gridBagConstraints);

        auteur.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridheight = 2;
        gridBagConstraints.ipadx = 212;
        gridBagConstraints.ipady = 19;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(6, 12, 6, 14);
        jPanel2.add(auteur, gridBagConstraints);

        getContentPane().add(jPanel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 50, 680, 50));

        jPanel3.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel3.setLayout(new java.awt.GridBagLayout());

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel5.setText("Rechèrche");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.ipadx = 7;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(17, 2, 0, 0);
        jPanel3.add(jLabel5, gridBagConstraints);

        recherche.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        recherche.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                rechercheKeyReleased(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridheight = 2;
        gridBagConstraints.ipadx = 583;
        gridBagConstraints.ipady = 19;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(6, 12, 6, 9);
        jPanel3.add(recherche, gridBagConstraints);

        getContentPane().add(jPanel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 100, 680, 50));

        jMenuBar1.setBackground(new java.awt.Color(102, 102, 102));

        jMenu1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/fichier.jpg"))); // NOI18N
        jMenu1.setText("Action");
        jMenu1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N

        enregistrer.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_E, java.awt.event.InputEvent.CTRL_MASK));
        enregistrer.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/icons8-save_close.png"))); // NOI18N
        enregistrer.setText("Enrégistrer");
        enregistrer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                enregistrerActionPerformed(evt);
            }
        });
        jMenu1.add(enregistrer);

        nouveau.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_N, java.awt.event.InputEvent.CTRL_MASK));
        nouveau.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/icons8-add_list.png"))); // NOI18N
        nouveau.setText("Nouveau");
        nouveau.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nouveauActionPerformed(evt);
            }
        });
        jMenu1.add(nouveau);

        jMenuBar1.add(jMenu1);

        jMenu2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/editer.jpg"))); // NOI18N
        jMenu2.setText("Edition");
        jMenu2.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N

        modifier.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_M, java.awt.event.InputEvent.CTRL_MASK));
        modifier.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/icons8-refresh.png"))); // NOI18N
        modifier.setText("Modifier");
        modifier.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                modifierActionPerformed(evt);
            }
        });
        jMenu2.add(modifier);

        jMenuItem1.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F1, 0));
        jMenuItem1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/icons8-help.png"))); // NOI18N
        jMenuItem1.setText("Aide");
        jMenu2.add(jMenuItem1);

        jMenuBar1.add(jMenu2);

        setJMenuBar(jMenuBar1);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void nouveauActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nouveauActionPerformed
      Nettoyer();
        JTextField[] txt={code,ouvrage};
        Mn.NEW_BUTTON(txt, "ouvrages", "code");
    }//GEN-LAST:event_nouveauActionPerformed
    
    private void Ramplir(){
        String query="select code,description,auteur from show_ouvrages where institution='"+Variables.Empresa+"'";
        Mn.FULLTABLE(query, tblouvrage, md);
    }
    private void enregistrerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_enregistrerActionPerformed
       String sql="call Insert_ouvrages("+code.getText()+",'"+Mn.illegalCharacterOnString(ouvrage.getText())+"','"+auteur.getText()+"')";
       Mn.SAVE_BUTTON(sql);
       
       int ins = Mn.getCodeFromString("institution", "idinstitution", Variables.Empresa,"nom");
       sql = "call insert_ouvrages_institution ("+Integer.parseInt(code.getText().trim())+","+ins+") ";
       Mn.MANIPULEDB(sql);
        nouveau.doClick();
         Ramplir();
    }//GEN-LAST:event_enregistrerActionPerformed

    private void modifierActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_modifierActionPerformed
     String sql="call Insert_ouvrages("+code+",'"+ouvrage.getText()+"','"+auteur.getText()+"','"+Variables.Empresa+"')";
       Mn.MODIFIER(sql);
        nouveau.doClick();
         Ramplir();
    }//GEN-LAST:event_modifierActionPerformed

    private void tblouvrageMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblouvrageMousePressed
       JTable table= (JTable)evt.getSource();
      Point pt= evt.getPoint();
      int row=table.rowAtPoint(pt);
      if(evt.getClickCount()==2){
          code.setText(tblouvrage.getValueAt(tblouvrage.getSelectedRow(), 0).toString());
          ouvrage.setText(tblouvrage.getValueAt(tblouvrage.getSelectedRow(), 1).toString());
          auteur.setText(tblouvrage.getValueAt(tblouvrage.getSelectedRow(), 2).toString());
      }
    }//GEN-LAST:event_tblouvrageMousePressed

    private void rechercheKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_rechercheKeyReleased
      String query="select code,description,auteur from ouvrages where description like '%"+recherche.getText()+"%' or auteur like '%"+recherche.getText()+"%'"
              + " and  institution='"+Variables.Empresa+"'";
        Mn.FULLTABLE(query, tblouvrage, md);
    }//GEN-LAST:event_rechercheKeyReleased


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField auteur;
    private javax.swing.JTextField code;
    private javax.swing.JMenuItem enregistrer;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JMenuItem modifier;
    private javax.swing.JMenuItem nouveau;
    private javax.swing.JTextField ouvrage;
    private javax.swing.JTextField recherche;
    private javax.swing.JTable tblouvrage;
    // End of variables declaration//GEN-END:variables
}
