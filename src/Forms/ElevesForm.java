/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Forms;

import Consults.consultPerson_no_eleve;
import Consults.consult_no_eleves;
import Methods.DragListener;
import Methods.GenerateMatricule;
import Methods.Manipulation;
import Methods.Security;
import Methods.Theme;
import Methods.Variables;
import Processus.Entrer_note;
import com.ezware.oxbow.swingbits.table.filter.TableRowFilterSupport;
import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.EventQueue;
import java.awt.Image;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.dnd.DropTarget;
import java.awt.event.KeyEvent;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumnModel;
import org.edisoncor.gui.textField.TextField;
import org.edisoncor.gui.textField.TextFieldRectBackground;

/**
 *
 * @author josephandyfeidje
 */
public class ElevesForm extends javax.swing.JFrame {
Manipulation Mn = new Manipulation();
    Security sc = new Security();
    Theme th = new Theme();
    public static String path;
    int aaa,cod = 0;
    String codter = null;
    
    GenerateMatricule gm = new GenerateMatricule();
    String[][] data = {};
    String[] head = {"Nom", " Prénom", "Matricule", "Personne Responsable", "Téléphone", "Lien"};
    DefaultTableModel md = new DefaultTableModel(data, head);

    TableCellRenderer render = new TableCellRenderer() {
        public Component getTableCellRendererComponent(JTable table, Object value, boolean idSelected, boolean hasFocus, int row, int column) {
            JLabel lbl = new JLabel(value == null ? "" : value.toString());
            if (column == 2 && row >= 0) {
                lbl.setHorizontalAlignment(SwingConstants.LEFT);
                lbl.setOpaque(true);
                lbl.setBackground(Color.green);
            }
            return lbl;
        }
    };

    JTextField coode = new JTextField();
    JTextField code = new JTextField();
    
    
    public ElevesForm() {
        initComponents();
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setLocationRelativeTo(this);
        estatusItemStateChanged(null);
//        JComponent comp = this;
        secureState();
        this.setTitle("Scolaris: " + Variables.Empresa + " : Registre des Elèves");
        nouveau.doClick();
        matricules.setText(gm.MatriculeCode());
        code.setEditable(false);
        matricules.setEditable(false);
        tbleleve.setModel(md);
        
        nom.setEditable(false);
        prenom.setEditable(false);
        sexe.setEnabled(false);
        adresse.setEditable(false);
        teleleve.setEditable(false);
        sanguins.setEnabled(false);
        matricules.setEditable(false);
        naissance.setEnabled(false);
        
        Consulter();
        //---------------------------------------------------------------------
        TableColumnModel cm = tbleleve.getColumnModel();
        cm.getColumn(0).setPreferredWidth(50);
        cm.getColumn(1).setPreferredWidth(90);
        cm.getColumn(2).setPreferredWidth(20);
        cm.getColumn(3).setPreferredWidth(80);
        cm.getColumn(4).setPreferredWidth(40);
        cm.getColumn(5).setPreferredWidth(10);
        tbleleve.getColumnModel().getColumn(2).setCellRenderer(render);
        search();
        
        tbleleve.getSelectionModel().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        effacer.setEnabled(false);
        tableEleveChangeListener();
        TableRowFilterSupport.forTable(tbleleve).searchable(true).apply();
        prenom.setName("prenom");
        nom.setName("nom");
        adresse.setName("adresse");
        connectDragDrop();
        naissance.setLocale(Locale.FRENCH);
        
        String fichero  = System.getProperty("user.dir")+"/src/Pictures/icons8-detective.png";
        System.out.println("cd "+fichero);
        Image image = Toolkit.getDefaultToolkit().createImage(fichero); 
        Cursor cursor = Toolkit.getDefaultToolkit().createCustomCursor(image, new Point(10,10),"aa"); 
        nom.setCursor(cursor); prenom.setCursor(cursor);  responsable.setCursor(cursor);
    }
    
        private void search() {
        String query = "select code,nom,prenom,matricule,responsable,lien_parente as lien, institution \n" +
"from show_eleves where institution ='" + Variables.Empresa + "' and nom like '%" + recherche.getText() + "%' or prenom like '%" + recherche.getText() + "%'"
                + " or matricule like '%" + recherche.getText() + "%' or responsable like '%" + recherche.getText() + "%'"
                + " order by code desc";
        ResultSet rs = Mn.SEARCHDATA(query);
        try {
            md.setRowCount(0);
            while (rs.next()) {
                String[] vect = {rs.getString("nom"), rs.getString("prenom"), rs.getString("matricule"),
                    rs.getString("responsable"), null, rs.getString("lien")};
                Mn.ADD_TABLE(md, vect);
                jLabel15.setText("Nombre(s) de registre(s) : "+tbleleve.getRowCount());
            }
        } catch (SQLException ex) {
            Logger.getLogger(Entrer_note.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void photoIcon(){
        String fichero  = System.getProperty("user.dir")+"/src/Pictures/ButtonFoto/icons8-camera.png";
        System.out.println("cd "+fichero);
        Image image = Toolkit.getDefaultToolkit().createImage(fichero); 
        ImageIcon imageicon = new ImageIcon(image);
        
        if(photo.getIcon() == null){
            photo.setIcon(imageicon);
//            photo.setText("Double click pour Telecharger une Photo");
        }
    }
    
    private void focusLost(){
        
        try {
            String sql = "SELECT * FROM tercero_general_view where code = '"+Variables.code_tercero+"' ";
            System.out.println(sql);
            ResultSet rs = Mn.Rezulta(sql);
            if(rs.last()){
                nom.setText(rs.getString("nom"));
                prenom.setText(rs.getString("prenom"));
                sexe.setSelectedItem(rs.getString("sexe"));
                naissance.setDate(rs.getDate("date_naissance"));
                adresse.setText(rs.getString("adresse"));
                
                //image
                try {
                   sql = "select imagecol from show_image where code = '"+Variables.code_tercero+"'  ";
                Mn.ShowPhotos(sql, photo); 
                } catch (NullPointerException e) {
                }
                
                
                //sanguins
            ResultSet er = Mn.Rezulta("select sang from show_sanguins where code='"+Variables.code_tercero+"' ");
            if(er.first()){
                sanguins.setSelectedItem(er.getString("sang"));
            }
                //.................
            //telephones
                sql = "SELECT phone_number FROM show_telephones where code = '"+Variables.code_tercero+"' ";
                ResultSet rst = Mn.Rezulta(sql);
                if (rst.first()) {
                    teleleve.setText(rst.getString("phone_number"));
                }
            }   
        } catch (SQLException ex) {
            Logger.getLogger(Person.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private void photoNull(){
        try {
            if(photo.getIcon() == null || photo.getText().isEmpty()){
//            photo.setText("Double Click pour Telecharger une photo");
        }
        } catch (NullPointerException e) {
        }
        
    }
    
    private void connectDragDrop(){
        DragListener d = new DragListener(photo, path);
        //SET TARGET TO JFRAME
         new DropTarget(this,d);
    }
    
     private void  autoCompleteEvent(KeyEvent evt,final TextFieldRectBackground textfield){
        switch(evt.getKeyCode()){
            case KeyEvent.VK_BACK_SPACE:
                break;
                case KeyEvent.VK_ENTER:
                    break;
                    default:
                        EventQueue.invokeLater(new Runnable() {

            @Override
            public void run() {
            String txt = textfield.getText();
                Mn.autoComplete(txt,"tercero",textfield.getName(),textfield);
            }
        });
        }
    }
    
     private void  autoCompleteEvent(KeyEvent evt,final JTextField textfield){
        switch(evt.getKeyCode()){
            case KeyEvent.VK_BACK_SPACE:
                break;
                case KeyEvent.VK_ENTER:
                    break;
                    default:
                        EventQueue.invokeLater(new Runnable() {

            @Override
            public void run() {
            String txt = textfield.getText();
                Mn.autoComplete(txt,"tercero",textfield.getName(),textfield);
            }
        });
        }
    }
     
    private void tableEleveChangeListener(){
        tbleleve.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                if(tbleleve.getRowCount() > 0){
                try {
                    //
                    String aa = "";
                      aa = tbleleve.getValueAt(tbleleve.getSelectedRow(), 2).toString();  
                    
                    System.out.println(tbleleve.getSelectedRow()+" "+aa);
                    String sql = "select code from show_eleve where matricule = '"+aa+"' ";
                    ResultSet rs = Mn.Rezulta(sql);
                    if(rs.last()){
                        Variables.code_tercero = rs.getString("code");
                    }
                    
                    focusLost();
                    String[] champ = {"estatus", "matricule"};
                    code.setText(aa);
                    estatus.setSelected(Mn.CONSULT_CHECKED("Show_eleves", code, champ));
                    prenom.requestFocus();
                    
                    try {
                        Mn.ShowPhotos("SELECT * FROM image where code  = '"+aa+"' ", photo);
                    } catch (NullPointerException ex) {
                    }
                    
                    
                    //
                    
                    // Autres
                        String xc = "select cond_sante,responsable,lien_parente from show_eleves where matricule = '"+aa+"' ";
                        ResultSet rss  = Mn.Rezulta(xc);
                        if(rss.last()){
                        sante.setSelectedItem(rss.getString("cond_sante"));
                        responsable.setText(rss.getString("responsable"));
                        lien.setSelectedItem(rss.getString("lien_parente"));
                        }
                        
                        //telparent
                        String cv = "select telephone from show_eleve where matricule = '"+aa+"' ";
                        ResultSet rsd = Mn.Rezulta(cv);
                        if(rsd.last()){
                        telparent.setText(rsd.getString("telephone"));
                        }
                        
                } catch (SQLException ex) {
                    Logger.getLogger(Eleves.class.getName()).log(Level.SEVERE, null, ex);
                }
                        
                }
        }
        });
    }

    private Object identify() {

        int j = 0;
        int d = (jPanel1.getComponents().length);
        System.out.println(d);

        ArrayList<Object> compName = new ArrayList<>();

        for (int i = 0; i < d; i++) {
            if (jPanel1.getComponent(i) instanceof org.edisoncor.gui.textField.TextFieldRectBackground) {
                System.out.println(i + "yes");
                compName.add(jPanel1.getComponent(i).getName());
            } else if (jPanel1.getComponent(i) instanceof javax.swing.JComboBox) {
                compName.add(jPanel1.getComponent(i).getName());
            } else if (jPanel1.getComponent(i) instanceof javax.swing.JFormattedTextField) {
                compName.add(jPanel1.getComponent(i).getName());
            } else if (jPanel1.getComponent(i) instanceof com.toedter.calendar.JDateChooser) {
                compName.add(jPanel1.getComponent(i).getName());
            } else if (jPanel1.getComponent(i) instanceof javax.swing.JCheckBox) {
                compName.add(jPanel1.getComponent(i).getName());
            }

            j++;
        }

        System.out.println(compName);
        return compName;
    }

    private void secureState() {
        TextField textField;
        identify();
    }

    private void Nettoyer() {
        prenom.setText(null);
        nom.setText(null);
        adresse.setText(null);
        teleleve.setValue(null);
        Mn.PHONE_FROMAT(teleleve);
        telparent.setValue(null);
        Mn.PHONE_FROMAT(telparent);
        naissance.setCalendar(null);
        responsable.setText(null);
        estatus.setSelected(false);
        lien.setSelectedItem("Selectionnez");
        sexe.setSelectedItem("Selectionnez");
        sante.setSelectedItem("Selectionnez");
        sanguins.setSelectedItem("Selectionnez");
        matricules.setText(gm.MatriculeCode());
        photo.setIcon(null); photo.updateUI();
        photoIcon();
    }

    private void Consulter() {
        String query = "select code,nom,prenom,matricule,responsable,lien_parente as lien, institution \n" +
"from show_eleves where institution ='" + Variables.Empresa + "'"
                + " order by code desc";
        System.out.println("cx "+query);
        ResultSet rs = Mn.SEARCHDATA(query);
        try {
            Mn.removeDatosOntable(md);
            while (rs.next()) {
                String[] vect = {rs.getString("nom"), rs.getString("prenom"), rs.getString("matricule"),
                    rs.getString("responsable"),null, rs.getString("lien")};
                Mn.ADD_TABLE(md, vect);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Entrer_note.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jCheckBox1 = new javax.swing.JCheckBox();
        jPopupMenu1 = new javax.swing.JPopupMenu();
        jMenuItem2 = new javax.swing.JMenuItem();
        jSeparator1 = new javax.swing.JPopupMenu.Separator();
        jMenuItem3 = new javax.swing.JMenuItem();
        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        jPanel8 = new javax.swing.JPanel();
        photo = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        prenom = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        matricules = new javax.swing.JTextField();
        jPanel4 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        nom = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        naissance = new com.toedter.calendar.JDateChooser();
        jPanel5 = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        sexe = new javax.swing.JComboBox();
        sante = new javax.swing.JComboBox();
        jPanel6 = new javax.swing.JPanel();
        jLabel8 = new javax.swing.JLabel();
        adresse = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        responsable = new javax.swing.JTextField();
        jPanel7 = new javax.swing.JPanel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        teleleve = new javax.swing.JFormattedTextField();
        telparent = new javax.swing.JFormattedTextField();
        jPanel9 = new javax.swing.JPanel();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        sanguins = new javax.swing.JComboBox();
        lien = new javax.swing.JComboBox();
        estatus = new javax.swing.JCheckBox();
        jPanel10 = new javax.swing.JPanel();
        jPanel11 = new javax.swing.JPanel();
        jLabel14 = new javax.swing.JLabel();
        recherche = new javax.swing.JTextField();
        jPanel12 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tbleleve = new javax.swing.JTable(){
            public boolean isCellEditable(int rowIndex, int colIndex){
                return false;
            }
        };
        jPanel13 = new javax.swing.JPanel();
        jLabel15 = new javax.swing.JLabel();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        enregistrer = new javax.swing.JMenuItem();
        nouveau = new javax.swing.JMenuItem();
        effacer = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        jMenuItem4 = new javax.swing.JMenuItem();
        modifier = new javax.swing.JMenuItem();
        jMenuItem6 = new javax.swing.JMenuItem();
        jMenuItem7 = new javax.swing.JMenuItem();

        jCheckBox1.setText("jCheckBox1");

        jMenuItem2.setText("Telecharger Photo....");
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem2ActionPerformed(evt);
            }
        });
        jPopupMenu1.add(jMenuItem2);
        jPopupMenu1.add(jSeparator1);

        jMenuItem3.setText("Effacer...");
        jMenuItem3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem3ActionPerformed(evt);
            }
        });
        jPopupMenu1.add(jMenuItem3);

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(new java.awt.CardLayout());

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));

        jPanel2.setBackground(new java.awt.Color(87, 148, 210));
        jPanel2.setBorder(new org.edisoncor.gui.util.DropShadowBorder());

        jLabel5.setBackground(new java.awt.Color(255, 255, 255));
        jLabel5.setFont(new java.awt.Font("Lucida Grande", 1, 18)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(255, 255, 255));
        jLabel5.setText("Registre d'élève(s)");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel5)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(26, Short.MAX_VALUE))
        );

        jPanel8.setBackground(new java.awt.Color(255, 255, 255));
        jPanel8.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        jPanel8.setLayout(new java.awt.BorderLayout());

        photo.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        photo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/ButtonFoto/icons8-camera.png"))); // NOI18N
        photo.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        photo.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                photoMouseClicked(evt);
            }
        });
        jPanel8.add(photo, java.awt.BorderLayout.CENTER);

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));
        jPanel3.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                jPanel3MouseMoved(evt);
            }
        });
        jPanel3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jPanel3MouseExited(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jPanel3MouseEntered(evt);
            }
        });

        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel1.setText("Prenom :");

        prenom.setBorder(new org.edisoncor.gui.util.DropShadowBorder());
        prenom.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                prenomMouseClicked(evt);
            }
        });
        prenom.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                prenomKeyReleased(evt);
            }
        });

        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel2.setText("Matricule : ");

        matricules.setBorder(new org.edisoncor.gui.util.DropShadowBorder());

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(prenom, javax.swing.GroupLayout.PREFERRED_SIZE, 215, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 76, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(matricules, javax.swing.GroupLayout.PREFERRED_SIZE, 219, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(prenom)
                    .addComponent(jLabel1)))
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(matricules, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(0, 0, Short.MAX_VALUE))
        );

        jPanel4.setBackground(new java.awt.Color(255, 255, 255));
        jPanel4.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                jPanel4MouseMoved(evt);
            }
        });
        jPanel4.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jPanel4MouseExited(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jPanel4MouseEntered(evt);
            }
        });

        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel3.setText("Nom :");

        nom.setBorder(new org.edisoncor.gui.util.DropShadowBorder());
        nom.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                nomMouseClicked(evt);
            }
        });
        nom.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                nomKeyReleased(evt);
            }
        });

        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel4.setText("Naissance : ");

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(nom, javax.swing.GroupLayout.PREFERRED_SIZE, 215, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 76, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(naissance, javax.swing.GroupLayout.PREFERRED_SIZE, 213, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel4)
                            .addComponent(jLabel3))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(naissance, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
            .addComponent(nom)
        );

        jPanel5.setBackground(new java.awt.Color(255, 255, 255));

        jLabel6.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel6.setText("Sexe :");

        jLabel7.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel7.setText("Santé : ");

        sexe.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Selectionnez", "Féminin", "Masculin", "Autres" }));

        sante.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Selectionnez", "Allergique", "Non Allergique", "Malaria", "Typhoides", "Typho-malaria", "Autres" }));

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(sexe, javax.swing.GroupLayout.PREFERRED_SIZE, 209, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 76, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(sante, javax.swing.GroupLayout.PREFERRED_SIZE, 206, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(jLabel6)
                .addComponent(sexe, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(jLabel7)
                .addComponent(sante, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        jPanel6.setBackground(new java.awt.Color(255, 255, 255));

        jLabel8.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel8.setText("Adresse :");

        adresse.setBorder(new org.edisoncor.gui.util.DropShadowBorder());

        jLabel9.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel9.setText("Responsable : ");

        responsable.setBorder(new org.edisoncor.gui.util.DropShadowBorder());
        responsable.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                responsableMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel8)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(adresse, javax.swing.GroupLayout.PREFERRED_SIZE, 215, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 76, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(responsable, javax.swing.GroupLayout.PREFERRED_SIZE, 219, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(32, Short.MAX_VALUE))
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addComponent(adresse, javax.swing.GroupLayout.DEFAULT_SIZE, 27, Short.MAX_VALUE)
                        .addGap(1, 1, 1))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel8)
                        .addComponent(jLabel9)))
                .addGap(7, 7, 7))
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addComponent(responsable, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        jPanel7.setBackground(new java.awt.Color(255, 255, 255));

        jLabel10.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel10.setText("Tel Elève : ");

        jLabel11.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel11.setText("Tel parent : ");

        teleleve.setBorder(new org.edisoncor.gui.util.DropShadowBorder());

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, 56, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(teleleve, javax.swing.GroupLayout.PREFERRED_SIZE, 215, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(telparent)
                .addContainerGap())
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel10)
                    .addComponent(jLabel11))
                .addContainerGap(19, Short.MAX_VALUE))
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(teleleve, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(telparent, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 0, Short.MAX_VALUE))
        );

        jPanel9.setBackground(new java.awt.Color(255, 255, 255));

        jLabel12.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel12.setText("Groupe Sanguins : ");

        jLabel13.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel13.setText("Lien : ");

        sanguins.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Selectionnez", "A+", "A-", "B+", "B-", "O+", "O-", "AB+", "AB-", "Autres" }));

        lien.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Selectionnez", "Père", "Mère", "Soeur", "Frères", "Cousin", "Cousine", "Oncle", "Tante", "Voisin", "Voisine", "Amis", "Autres" }));

        estatus.setText("Status");
        estatus.setName("estatus"); // NOI18N
        estatus.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                estatusItemStateChanged(evt);
            }
        });

        javax.swing.GroupLayout jPanel9Layout = new javax.swing.GroupLayout(jPanel9);
        jPanel9.setLayout(jPanel9Layout);
        jPanel9Layout.setHorizontalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addContainerGap(50, Short.MAX_VALUE)
                .addComponent(jLabel12)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(sanguins, javax.swing.GroupLayout.PREFERRED_SIZE, 215, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel13)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lien, javax.swing.GroupLayout.PREFERRED_SIZE, 212, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(estatus))
        );
        jPanel9Layout.setVerticalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(estatus, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lien, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel13)
                    .addComponent(sanguins, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel12))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel10.setBackground(new java.awt.Color(255, 255, 255));

        jPanel11.setBackground(new java.awt.Color(87, 148, 210));
        jPanel11.setBorder(new org.edisoncor.gui.util.DropShadowBorder());

        jLabel14.setText("Recherche : ");

        recherche.setBorder(new org.edisoncor.gui.util.DropShadowBorder());
        recherche.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                rechercheKeyReleased(evt);
            }
        });

        javax.swing.GroupLayout jPanel11Layout = new javax.swing.GroupLayout(jPanel11);
        jPanel11.setLayout(jPanel11Layout);
        jPanel11Layout.setHorizontalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel11Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel14)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(recherche, javax.swing.GroupLayout.PREFERRED_SIZE, 389, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel11Layout.setVerticalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
            .addGroup(jPanel11Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel11Layout.createSequentialGroup()
                        .addComponent(jLabel14)
                        .addContainerGap())
                    .addComponent(recherche, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        jPanel12.setLayout(new java.awt.CardLayout());

        tbleleve.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tbleleve);

        jPanel12.add(jScrollPane1, "card2");

        javax.swing.GroupLayout jPanel10Layout = new javax.swing.GroupLayout(jPanel10);
        jPanel10.setLayout(jPanel10Layout);
        jPanel10Layout.setHorizontalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel10Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel11, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel12, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel10Layout.setVerticalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel10Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel12, javax.swing.GroupLayout.DEFAULT_SIZE, 246, Short.MAX_VALUE)
                .addContainerGap())
        );

        jPanel13.setBackground(new java.awt.Color(255, 255, 255));
        jPanel13.setLayout(new java.awt.CardLayout());

        jLabel15.setText("Nombre(s) de registre(s) : 0");
        jPanel13.add(jLabel15, "card2");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(219, 219, 219)
                        .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(219, 219, 219)
                        .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(219, 219, 219)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jPanel6, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jPanel7, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jPanel10, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jPanel9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel1Layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, 208, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(635, Short.MAX_VALUE)))
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                    .addContainerGap(664, Short.MAX_VALUE)
                    .addComponent(jPanel13, javax.swing.GroupLayout.PREFERRED_SIZE, 179, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap()))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel9, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(16, Short.MAX_VALUE))
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                    .addContainerGap(101, Short.MAX_VALUE)
                    .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, 180, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(370, Short.MAX_VALUE)))
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                    .addContainerGap(629, Short.MAX_VALUE)
                    .addComponent(jPanel13, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap()))
        );

        getContentPane().add(jPanel1, "card2");

        jMenu1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/ButtonFoto/icons8-menu.png"))); // NOI18N
        jMenu1.setText("Action");

        enregistrer.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/ButtonFoto/icons8-upload_to_cloud.png"))); // NOI18N
        enregistrer.setText("Enregistrer");
        enregistrer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                enregistrerActionPerformed(evt);
            }
        });
        jMenu1.add(enregistrer);

        nouveau.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/ButtonFoto/icons8-new_by_copy.png"))); // NOI18N
        nouveau.setText("Nouveau");
        nouveau.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nouveauActionPerformed(evt);
            }
        });
        jMenu1.add(nouveau);

        effacer.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/ButtonFoto/icons8-trash.png"))); // NOI18N
        effacer.setText("Effacer");
        effacer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                effacerActionPerformed(evt);
            }
        });
        jMenu1.add(effacer);

        jMenuBar1.add(jMenu1);

        jMenu2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/ButtonFoto/icons8-menu_squared.png"))); // NOI18N
        jMenu2.setText("Edition");

        jMenuItem4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/ButtonFoto/icons8-view_file.png"))); // NOI18N
        jMenuItem4.setText("Consulter");
        jMenu2.add(jMenuItem4);

        modifier.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/ButtonFoto/icons8-edit_file.png"))); // NOI18N
        modifier.setText("Modifier");
        modifier.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                modifierActionPerformed(evt);
            }
        });
        jMenu2.add(modifier);

        jMenuItem6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/ButtonFoto/icons8-new_by_copy.png"))); // NOI18N
        jMenuItem6.setText("Ajouter ");
        jMenu2.add(jMenuItem6);

        jMenuItem7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/ButtonFoto/icons8-help.png"))); // NOI18N
        jMenuItem7.setText("Aide");
        jMenu2.add(jMenuItem7);

        jMenuBar1.add(jMenu2);

        setJMenuBar(jMenuBar1);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void modifierActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_modifierActionPerformed
        int aa = Integer.parseInt(coode.getText());
        String bb = prenom.getText();
        String cc = nom.getText();
        String dd = adresse.getText();
        String ee = teleleve.getText();
        String ff = sexe.getSelectedItem().toString();
//        String gg=ReturnIstitution();
        String query = "call Insert_tercero(" + aa + ",'" + cc + "','" + bb + "','" + ff + "','" + dd + "','" + ee + "','" + Variables.Empresa + "')";
        Mn.MANIPULEDB(query);
        //-----------------------------------------------------------------------------
        int aaa = Integer.parseInt(code.getText());
        String bbb = matricules.getText();
        String ccc = Mn.INSERTDATECHOOSER(naissance);
        String ddd = sante.getSelectedItem().toString();
        String fff = responsable.getText();
        String ggg = telparent.getText();
        String hhh = lien.getSelectedItem().toString();
        String iii = Mn.DATEHOURNOW();
        String jjj = Mn.INSERT_CHECKED(estatus);
        String pas = sc.password();
        String email = "";
        query = "call Insert_eleve(" + aaa + "," + aa + ",'" + bbb + "','" + email + "','" + pas + "','" + ccc + "','" + ddd + "','" + fff + "','" + ggg + "','" + hhh + "','" + iii + "'," + jjj + ")";
        //System.out.println(query);
        Mn.MODIFIER(query);
        String san = sanguins.getSelectedItem().toString();
        Mn.MANIPULEDB("call Insert_sanguins (" + aa + ",'" + san + "')");
        Consulter();
        nouveau.doClick();
    }//GEN-LAST:event_modifierActionPerformed

    private void photoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_photoMouseClicked
        //        if(evt.getClickCount() == 2){
            //            Mn.Photos(photo, matricules.getText().trim());
            //        }
    }//GEN-LAST:event_photoMouseClicked

    private void estatusItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_estatusItemStateChanged
        if(estatus.isSelected()){
            estatus.setText("Active");
        }
        else{
            estatus.setText("Inactive");
        }
    }//GEN-LAST:event_estatusItemStateChanged

    private void nouveauActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nouveauActionPerformed
        JTextField[] txt = {code, prenom};
//        Mn.NEW_BUTTON(txt, "eleve", "code_eleve");
        Nettoyer();
    }//GEN-LAST:event_nouveauActionPerformed

    private void enregistrerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_enregistrerActionPerformed
         aaa = Mn.MAXCODE("code_eleve", "eleve");
        String bbb = matricules.getText();
        String ccc = "";
        String ddd = sante.getSelectedItem().toString();
        String fff = responsable.getText();
        String ggg = telparent.getText();
        String hhh = lien.getSelectedItem().toString();
        String iii = Mn.DATEHOURNOW();
        String jjj = Mn.INSERT_CHECKED(estatus);
        String pas = sc.password();
        String email = "";
        
        //eleves
        String query = "call insert_eleve ("+aaa+",'"+bbb+"',"+Variables.code+",'"+Variables.code_terceross+"','"+pas+"','"+ddd+"',"+jjj+" )";
        System.out.println("query "+query);
        Mn.MANIPULEDB(query);
        
        //eleve responsables
        query = "call insert_responsables ('"+bbb+"',"+Variables.codresp+",'"+Variables.codter_resp+"','"+hhh+"') ";
        System.out.println("qu "+query);
        Mn.MANIPULEDB(query);
        
       //eleves_has institution
        int idi = 0;
        query = "select idinstitution from institution where nom = '"+Variables.Empresa+"' ";
        ResultSet rsg = Mn.Rezulta(query);
        try {
            if(rsg.last()){
                idi = rsg.getInt("idinstitution");
            }
        } catch (SQLException ex) {
            Logger.getLogger(Eleves.class.getName()).log(Level.SEVERE, null, ex);
        }

        query = "call eleve_has_institution_procedure ('"+bbb+"',"+Variables.code+",'"+Variables.code_terceross+"','"+idi+"') ";
        System.out.println("institution : "+query);
        Mn.MANIPULEDB(query);
        
        Consulter();
        nouveau.doClick();
    }//GEN-LAST:event_enregistrerActionPerformed

    private void effacerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_effacerActionPerformed
        if (prenom.getText().isEmpty()) {
            JOptionPane.showConfirmDialog(null, "Désolé! fait double clique sur l'élève dans la table ci-dessous pour éffacer.");
        } else {
            String query = "delete from tercero where code_tercero='" + coode.getText() + "'";
            Mn.MANIPULEDB(query);
            Mn.DELETE_BUTTON(code, "eleve", "code_eleve");
        }
        Consulter();
        nouveau.doClick();
    }//GEN-LAST:event_effacerActionPerformed

    private void rechercheKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_rechercheKeyReleased
        search();
    }//GEN-LAST:event_rechercheKeyReleased

    private void prenomKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_prenomKeyReleased
        autoCompleteEvent(evt, prenom);
    }//GEN-LAST:event_prenomKeyReleased

    private void nomKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_nomKeyReleased
        autoCompleteEvent(evt, nom);
    }//GEN-LAST:event_nomKeyReleased

    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem2ActionPerformed
        //        Mn.Photos(photo, matricules.getText().trim());
    }//GEN-LAST:event_jMenuItem2ActionPerformed

    private void jMenuItem3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem3ActionPerformed
        photo.setIcon(null);
        photo.updateUI();
        photoNull();
    }//GEN-LAST:event_jMenuItem3ActionPerformed

    private void prenomMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_prenomMouseClicked
        if(evt.getClickCount() == 2){
            consult_no_eleves cp = new consult_no_eleves(null, true);
        cp.show();
        cp.pack();
        code.setText(Variables.code_tercero);
            System.out.println("code "+Variables.code_tercero);
            cod = Variables.code;
            codter = Variables.code_terceross;
        focusLost();
        }
    }//GEN-LAST:event_prenomMouseClicked

    private void nomMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_nomMouseClicked
        prenomMouseClicked(evt);
    }//GEN-LAST:event_nomMouseClicked

    private void responsableMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_responsableMouseClicked
        if(evt.getClickCount() == 2){
            try {
                
                consultPerson_no_eleve cp = new consultPerson_no_eleve(null, true);
                cp.show();
                cp.pack();
                if(code.getText().trim().equals(Variables.code_terceros_resp)){
                    JOptionPane.showMessageDialog(this, "Vous ne pouvez pas choisir la même personne pour être responsable");
                    return;
                }
                else{
                responsable.setText(Variables.nomReponsab);
                //tel
                String sql = "SELECT phone_number FROM show_telephones where code = '"+Variables.code_terceros_resp+"' ";
                    System.out.println("Terceros : "+sql);
                ResultSet rst = Mn.Rezulta(sql);
                if (rst.first()) {
                    telparent.setText(rst.getString("phone_number"));
                }
        
//        focusLost();
                }
            } catch (SQLException ex) {
                Logger.getLogger(Eleves.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }//GEN-LAST:event_responsableMouseClicked

    private void jPanel3MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel3MouseEntered
        Mn.setPanTxtColor(jPanel3, prenom);
        Mn.setPanTxtColor(jPanel3, matricules);
    }//GEN-LAST:event_jPanel3MouseEntered

    private void jPanel3MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel3MouseExited
        Mn.resetPanTxtColor(jPanel3, prenom);
        Mn.resetPanTxtColor(jPanel3, matricules);
    }//GEN-LAST:event_jPanel3MouseExited

    private void jPanel3MouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel3MouseMoved
        jPanel3MouseEntered(evt);
    }//GEN-LAST:event_jPanel3MouseMoved

    private void jPanel4MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel4MouseEntered
        Mn.setPanTxtColor(jPanel4, nom);
    }//GEN-LAST:event_jPanel4MouseEntered

    private void jPanel4MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel4MouseExited
        Mn.resetPanTxtColor(jPanel4, nom);
    }//GEN-LAST:event_jPanel4MouseExited

    private void jPanel4MouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel4MouseMoved
        jPanel4MouseEntered(evt);
    }//GEN-LAST:event_jPanel4MouseMoved

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ElevesForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ElevesForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ElevesForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ElevesForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new ElevesForm().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField adresse;
    private javax.swing.JMenuItem effacer;
    private javax.swing.JMenuItem enregistrer;
    private javax.swing.JCheckBox estatus;
    private javax.swing.JCheckBox jCheckBox1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JMenuItem jMenuItem3;
    private javax.swing.JMenuItem jMenuItem4;
    private javax.swing.JMenuItem jMenuItem6;
    private javax.swing.JMenuItem jMenuItem7;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel11;
    private javax.swing.JPanel jPanel12;
    private javax.swing.JPanel jPanel13;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JPopupMenu jPopupMenu1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JPopupMenu.Separator jSeparator1;
    private javax.swing.JComboBox lien;
    private javax.swing.JTextField matricules;
    private javax.swing.JMenuItem modifier;
    private com.toedter.calendar.JDateChooser naissance;
    private javax.swing.JTextField nom;
    private javax.swing.JMenuItem nouveau;
    private javax.swing.JLabel photo;
    private javax.swing.JTextField prenom;
    private javax.swing.JTextField recherche;
    private javax.swing.JTextField responsable;
    private javax.swing.JComboBox sanguins;
    private javax.swing.JComboBox sante;
    private javax.swing.JComboBox sexe;
    private javax.swing.JTable tbleleve;
    private javax.swing.JFormattedTextField teleleve;
    private javax.swing.JFormattedTextField telparent;
    // End of variables declaration//GEN-END:variables
}
