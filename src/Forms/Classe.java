package Forms;

import Consults.Cons_classe;
import static Menus.Menus.desktop;
import Methods.*;
import com.ezware.oxbow.swingbits.table.filter.TableRowFilterSupport;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;
import Methods.Theme;

public class Classe extends javax.swing.JInternalFrame {

    Manipulation Mn = new Manipulation();
    Theme th = new Theme();
    String[][] data = {};
    String[] head = {" Classes","Section"};
    DefaultTableModel md = new DefaultTableModel(data, head);
    public static String acces="";
    
    public static JTextField code = new JTextField();

    public Classe() {
        initComponents();
        this.setTitle("Scolaris: " + Variables.Empresa + " : Registre des Classes");
        tblmatiere.setModel(md);
        nouveau.doClick();
        effacer.setEnabled(false);
        TableRowFilterSupport.forTable(tblmatiere).searchable(true).apply();
    }

    private boolean isGridRedundance(String text){
        
        boolean res = false;
        for (int i = 0; i < tblmatiere.getRowCount(); i++) {
            if(tblmatiere.getValueAt(i, 0).toString().equals(text)){
                res = true;
            }
        }
        return res;
    }
    
    private boolean isDatosExist(String text){
        boolean res = false;
        try {
            String sql = "select description from classe where description = '"+text+"' ";
            ResultSet rs = Mn.Rezulta(sql);
            if(rs.last()){ 
                res = true;
            }
        } catch (SQLException ex) {
            Logger.getLogger(Classe.class.getName()).log(Level.SEVERE, null, ex);
        }
        return res;
    }
    
    private void Nettoyer() {
        classe.setValue(null);
        section.setSelectedItem("Selectionnez");
        md.setRowCount(0);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        buttonSeven1 = new org.edisoncor.gui.button.ButtonSeven();
        buttonTask1 = new org.edisoncor.gui.button.ButtonTask();
        buttonPopup1 = new org.edisoncor.gui.button.ButtonPopup();
        buttonTask2 = new org.edisoncor.gui.button.ButtonTask();
        buttonIpod1 = new org.edisoncor.gui.button.ButtonIpod();
        buttonIcon1 = new org.edisoncor.gui.button.ButtonIcon();
        jMenuItem1 = new javax.swing.JMenuItem();
        jPanel1 = new javax.swing.JPanel();
        section = new javax.swing.JComboBox();
        jLabel4 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        rem = new javax.swing.JButton();
        add = new javax.swing.JButton();
        classe = new javax.swing.JFormattedTextField();
        jLabel2 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblmatiere = new javax.swing.JTable(){
            public boolean isCellEditable(int rowIndex, int colIndex){
                return false;
            }
        };
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        enregistrer = new javax.swing.JMenuItem();
        nouveau = new javax.swing.JMenuItem();
        effacer = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        consulter = new javax.swing.JMenuItem();
        modifier = new javax.swing.JMenuItem();
        jMenuItem2 = new javax.swing.JMenuItem();
        ajoutersection = new javax.swing.JMenuItem();

        buttonSeven1.setText("buttonSeven1");

        buttonPopup1.setText("buttonPopup1");

        buttonIpod1.setText("buttonIpod1");

        buttonIcon1.setText("buttonIcon1");

        jMenuItem1.setText("jMenuItem1");

        setClosable(true);
        setIconifiable(true);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel1.setLayout(new java.awt.GridBagLayout());

        section.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        section.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Selectionnez", "Petit", "Primaire", "Secondaire" }));
        section.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                sectionItemStateChanged(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.ipadx = 232;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 1, 4, 198);
        jPanel1.add(section, gridBagConstraints);

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel4.setText("Section : ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.ipady = 9;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 4, 0);
        jPanel1.add(jLabel4, gridBagConstraints);

        getContentPane().add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, -1, -1));

        jPanel2.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel2.setLayout(new java.awt.GridBagLayout());

        rem.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/icons8-up_circular_1.png"))); // NOI18N
        rem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                remActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridheight = 2;
        gridBagConstraints.ipadx = -17;
        gridBagConstraints.ipady = 3;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(3, 6, 0, 0);
        jPanel2.add(rem, gridBagConstraints);

        add.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/icons8-circled_down.png"))); // NOI18N
        add.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridheight = 3;
        gridBagConstraints.ipadx = -14;
        gridBagConstraints.ipady = 4;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(3, 6, 0, 0);
        jPanel2.add(add, gridBagConstraints);

        classe.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        classe.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                classeFocusGained(evt);
            }
        });
        classe.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                classeActionPerformed(evt);
            }
        });
        classe.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                classeKeyReleased(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridheight = 3;
        gridBagConstraints.ipadx = 418;
        gridBagConstraints.ipady = 13;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(3, 12, 0, 0);
        jPanel2.add(classe, gridBagConstraints);

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel2.setText("Classe : ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.ipadx = 7;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(11, 0, 0, 0);
        jPanel2.add(jLabel2, gridBagConstraints);

        getContentPane().add(jPanel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 42, 618, -1));

        jPanel3.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel3.setLayout(new javax.swing.BoxLayout(jPanel3, javax.swing.BoxLayout.LINE_AXIS));

        tblmatiere.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tblmatiere);

        jPanel3.add(jScrollPane1);

        getContentPane().add(jPanel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 89, 681, 333));

        jMenu1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/fichier.jpg"))); // NOI18N
        jMenu1.setText("Action");
        jMenu1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N

        enregistrer.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_E, java.awt.event.InputEvent.CTRL_MASK));
        enregistrer.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/icons8-save_close.png"))); // NOI18N
        enregistrer.setText("Enrégistrer");
        enregistrer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                enregistrerActionPerformed(evt);
            }
        });
        jMenu1.add(enregistrer);

        nouveau.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_N, java.awt.event.InputEvent.CTRL_MASK));
        nouveau.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/icons8-add_list.png"))); // NOI18N
        nouveau.setText("Nouveau");
        nouveau.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nouveauActionPerformed(evt);
            }
        });
        jMenu1.add(nouveau);

        effacer.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_D, java.awt.event.InputEvent.CTRL_MASK));
        effacer.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/icons8-erase.png"))); // NOI18N
        effacer.setText("Effacer");
        effacer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                effacerActionPerformed(evt);
            }
        });
        jMenu1.add(effacer);

        jMenuBar1.add(jMenu1);

        jMenu2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/editer.jpg"))); // NOI18N
        jMenu2.setText("Edition");
        jMenu2.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N

        consulter.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_W, java.awt.event.InputEvent.CTRL_MASK));
        consulter.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/icons8-search.png"))); // NOI18N
        consulter.setText("Consulter");
        consulter.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                consulterActionPerformed(evt);
            }
        });
        jMenu2.add(consulter);

        modifier.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_M, java.awt.event.InputEvent.CTRL_MASK));
        modifier.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/icons8-refresh.png"))); // NOI18N
        modifier.setText("Modifier");
        modifier.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                modifierActionPerformed(evt);
            }
        });
        jMenu2.add(modifier);

        jMenuItem2.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F1, 0));
        jMenuItem2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/icons8-help.png"))); // NOI18N
        jMenuItem2.setText("Aide");
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem2ActionPerformed(evt);
            }
        });
        jMenu2.add(jMenuItem2);

        ajoutersection.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_A, java.awt.event.InputEvent.ALT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        ajoutersection.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/icons8-test_failed.png"))); // NOI18N
        ajoutersection.setText("Ajouter matière");
        ajoutersection.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ajoutersectionActionPerformed(evt);
            }
        });
        jMenu2.add(ajoutersection);

        jMenuBar1.add(jMenu2);

        setJMenuBar(jMenuBar1);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void nouveauActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nouveauActionPerformed
        JTextField[] txt = {code, classe};
        Mn.NEW_BUTTON(txt, "classe", "code_classe");
        Nettoyer();
    }//GEN-LAST:event_nouveauActionPerformed

    private void enregistrerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_enregistrerActionPerformed
        for (int i = 0; i < tblmatiere.getRowCount(); i++) {
            code.setText(String.valueOf(Mn.MAXCODE("code_classe", "classe")));
            int aa = Integer.parseInt(code.getText());
            String bb = tblmatiere.getValueAt(i, 0).toString();
            String cc = tblmatiere.getValueAt(i, 1).toString();
            if(!isDatosExist(bb)){
            String query = "call Insert_classe(" + aa + ",'" + bb + "','" + cc + "','" + Variables.Empresa + "')";
            Mn.MANIPULEDB(query);
            }
        }
        nouveau.doClick();
    }//GEN-LAST:event_enregistrerActionPerformed

    private void ajoutersectionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ajoutersectionActionPerformed
        Matiere pr = new Matiere();
        desktop.add(pr);
        int x = (desktop.getWidth() / 2) - pr.getWidth() / 2;
        int y = (desktop.getHeight() / 2) - pr.getHeight() / 2;
        pr.setLocation(x, y);
        pr.toFront();
        pr.setVisible(true);
    }//GEN-LAST:event_ajoutersectionActionPerformed

    private void effacerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_effacerActionPerformed
        int aa = Integer.parseInt(code.getText());
        String xx = Mn.SELLECT_BUTTON(Variables.Seleccionado1, tblmatiere);
        String xxx = Mn.SEARCHCODE("code_matiere", "description", xx, "matiere");
        String query = "delete from classe_vs_matiere where code_classe='" + aa + "'and code_matiere='" + xxx + "'";
        Mn.MANIPULEDB(query);
        try {

            query = "select * from show_matieres where classe like '%" + classe.getText() + "%' and section='" + section.getSelectedItem().toString() + "'"
                    + " group by matiere order by matiere";
            ResultSet rs = Mn.SEARCHDATA(query);
            try {
                md.setRowCount(0);
                while (rs.next()) {
                    String[] vect = {rs.getString("matiere")};
                    Mn.ADD_TABLE(md, vect);
                }
            } catch (SQLException ex) {
                Logger.getLogger(Classe.class.getName()).log(Level.SEVERE, null, ex);
            }

        } catch (Exception e) {
        }
        nouveau.doClick();
    }//GEN-LAST:event_effacerActionPerformed

    private void modifierActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_modifierActionPerformed


        int aa = Integer.parseInt(code.getText());
        String bb = classe.getText();
        String cc = section.getSelectedItem().toString();
        for (int i = 0; i < tblmatiere.getRowCount(); i++) {
            String xx = tblmatiere.getValueAt(i, 0).toString();
            String xxx = Mn.SEARCHCODE("code_matiere", "description", xx, "matiere");
            String kk = "delete from classe_vs_matiere where code_classe='" + aa + "'and code_matiere='" + xxx + "'";
            Mn.MANIPULEDB(kk);
        }
        for (int i = 0; i < tblmatiere.getRowCount(); i++) {
            String xx = tblmatiere.getValueAt(i, 0).toString();
            String xxx = Mn.SEARCHCODE("code_matiere", "description", xx, "matiere");
            String mm = "call Insert_classematiere(" + aa + "," + xxx + ")";
            Mn.MANIPULEDB(mm);
        }
        String query = "call Insert_classe(" + aa + ",'" + bb + "','" + cc + "')";
        Mn.MODIFIER(query);
        nouveau.doClick();

    }//GEN-LAST:event_modifierActionPerformed

    private void consulterActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_consulterActionPerformed
        Cons_classe cs = new Cons_classe();
        Menus.Menus.desktop.add(cs);
        int x = (desktop.getWidth() / 2) - cs.getWidth() / 2;
        int y = (desktop.getHeight() / 2) - cs.getHeight() / 2;
        cs.setLocation(x, y);
        cs.toFront();
        cs.setVisible(true);
    }//GEN-LAST:event_consulterActionPerformed

    private void addActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addActionPerformed
        if(isGridRedundance(classe.getValue().toString())){
            JOptionPane.showMessageDialog(rootPane, "Désolé Cette classe est dejà enregistrer dans le système", "Infos",JOptionPane.INFORMATION_MESSAGE);
        }
        else {
        if(acces.equals("libre")){
          String query="select description as classe,section from classe where section='"+section.getSelectedItem().toString()+"' "
                  + "and institution='"+Variables.Empresa+"'";
          ResultSet rs=Mn.SEARCHDATA(query);
            try {
                 md.setRowCount(0);
                while(rs.next()){
                    String vect[]={rs.getString("classe"),rs.getString("section")};
                    Mn.ADD_TABLE(md, vect);
                } 
                acces="";
            } catch (SQLException ex) {
                Logger.getLogger(Classe.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        else{
        String[] vect={classe.getText(),section.getSelectedItem().toString()};
        if(!section.getSelectedItem().toString().equals("Selectionnez") ) {
             Mn.ADD_TABLE(md, vect);
             classe.setValue(null);
            }
        else{
            JOptionPane.showMessageDialog(this,"S'il vous plait séléctionner une séction.");
        }
        }
        }
    }//GEN-LAST:event_addActionPerformed

    private void remActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_remActionPerformed
        Mn.REMOVE_TABLE(tblmatiere, md);
    }//GEN-LAST:event_remActionPerformed

    private void classeKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_classeKeyReleased
        Mn.autoComplete(classe.getText(), "classe", "description", classe);
    }//GEN-LAST:event_classeKeyReleased

    private void sectionItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_sectionItemStateChanged
        if (section.getSelectedItem() == null) {
            classe.setValue(null);
        } else if (section.getSelectedItem().equals("Petit")) {
            classe.setValue(null);
            Mn.CLASSE_PETIT_FORMAT(classe);
        } else if (section.getSelectedItem().equals("Primaire")) {
            classe.setValue(null);
            Mn.CLASSE_PRIMAIRE_FORMAT(classe);
        } else if (section.getSelectedItem().equals("Secondaire")) {
            classe.setValue(null);
            Mn.CLASSE_SECONDAIRE_FORMAT(classe);
        }
        String sql = "select description,section from classe where section = '"+section.getSelectedItem().toString()+"' ";
        System.out.println("cd "+sql);
        Mn.FULLTABLE(sql, tblmatiere, md);
    }//GEN-LAST:event_sectionItemStateChanged

    private void classeFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_classeFocusGained
        // TODO add your handling code here:
    }//GEN-LAST:event_classeFocusGained

    private void classeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_classeActionPerformed
        add.doClick();
    }//GEN-LAST:event_classeActionPerformed

    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jMenuItem2ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Classe.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Classe.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Classe.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Classe.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Classe().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    public static javax.swing.JButton add;
    private javax.swing.JMenuItem ajoutersection;
    private org.edisoncor.gui.button.ButtonIcon buttonIcon1;
    private org.edisoncor.gui.button.ButtonIpod buttonIpod1;
    private org.edisoncor.gui.button.ButtonPopup buttonPopup1;
    private org.edisoncor.gui.button.ButtonSeven buttonSeven1;
    private org.edisoncor.gui.button.ButtonTask buttonTask1;
    private org.edisoncor.gui.button.ButtonTask buttonTask2;
    public static javax.swing.JFormattedTextField classe;
    private javax.swing.JMenuItem consulter;
    private javax.swing.JMenuItem effacer;
    private javax.swing.JMenuItem enregistrer;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JMenuItem modifier;
    private javax.swing.JMenuItem nouveau;
    private javax.swing.JButton rem;
    public static javax.swing.JComboBox section;
    public static javax.swing.JTable tblmatiere;
    // End of variables declaration//GEN-END:variables
}
