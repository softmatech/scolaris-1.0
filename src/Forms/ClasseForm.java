/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Forms;

import Consults.Cons_classe;
import static Menus.Menus.desktop;
import Methods.Manipulation;
import Methods.Theme;
import Methods.Variables;
import com.ezware.oxbow.swingbits.table.filter.TableRowFilterSupport;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author josephandyfeidje
 */
public class ClasseForm extends javax.swing.JFrame {
    
    Manipulation Mn = new Manipulation();
    Theme th = new Theme();
    String[][] data = {};
    String[] head = {" Classes","Section"};
    DefaultTableModel md = new DefaultTableModel(data, head);
    public static String acces="";
    public static JTextField code = new JTextField();
    
    public ClasseForm() {
        initComponents();
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setLocationRelativeTo(this);
        this.setTitle("Scolaris: " + Variables.Empresa + " : Registre des Classes");
        tblmatiere.setModel(md);
        nouveau.doClick();
        nouveau.setEnabled(false);
        TableRowFilterSupport.forTable(tblmatiere).searchable(true).apply();
    }

    private boolean isGridRedundance(String text){
        
        boolean res = false;
        for (int i = 0; i < tblmatiere.getRowCount(); i++) {
            if(tblmatiere.getValueAt(i, 0).toString().equals(text)){
                res = true;
            }
        }
        return res;
    }
    
    private boolean isTextExistInDatabase(String text){
        boolean res = false;
        try {
            String sql = "select description from classe where description = '"+text+"' ";
            System.out.println("sql---> "+sql);
            ResultSet rs = Mn.Rezulta(sql);
            if(rs.last()){
        System.out.println("description "+ rs.getString("description") +"text-->> "+text);
                res = true;
            }
        } catch (SQLException ex) {
            Logger.getLogger(ClasseForm.class.getName()).log(Level.SEVERE, null, ex);
        }
        return res;
    }
    
    
    private void Nettoyer() {
        try {
        classe.setText(null);
        section.setSelectedItem("Selectionnez");
        md.setRowCount(0);
        } catch (Exception e) {
        }
        
    }


    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        section = new javax.swing.JComboBox();
        jLabel2 = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        jPanel5 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        jPanel6 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        classe = new javax.swing.JTextField();
        jPanel7 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblmatiere = new javax.swing.JTable(){
            public boolean isCellEditable(int rowIndex, int colIndex){
                return false;
            }
        };
        jPanel8 = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenuItem1 = new javax.swing.JMenuItem();
        nouveau = new javax.swing.JMenuItem();
        effacer = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        consulter = new javax.swing.JMenuItem();
        modifier = new javax.swing.JMenuItem();
        ajouter = new javax.swing.JMenuItem();
        jMenuItem7 = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(new javax.swing.BoxLayout(getContentPane(), javax.swing.BoxLayout.LINE_AXIS));

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));

        jPanel2.setBackground(new java.awt.Color(87, 148, 210));
        jPanel2.setBorder(new org.edisoncor.gui.util.DropShadowBorder());

        jLabel5.setBackground(new java.awt.Color(255, 255, 255));
        jLabel5.setFont(new java.awt.Font("Lucida Grande", 1, 18)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(255, 255, 255));
        jLabel5.setText("Registre de Classe(s)");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel5)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(26, Short.MAX_VALUE))
        );

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));
        jPanel3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jPanel3MouseExited(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jPanel3MouseEntered(evt);
            }
        });

        jLabel1.setText("Section : ");

        section.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Selectionnez", "Petit", "Primaire", "Secondaire" }));
        section.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                sectionItemStateChanged(evt);
            }
        });

        jLabel2.setText("Classe : ");

        jPanel4.setBackground(new java.awt.Color(153, 255, 255));
        jPanel4.setLayout(new java.awt.GridLayout(1, 0));

        jPanel5.setBackground(new java.awt.Color(255, 255, 255));
        jPanel5.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jPanel5MouseClicked(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jPanel5MouseExited(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jPanel5MouseEntered(evt);
            }
        });
        jPanel5.setLayout(new java.awt.CardLayout());

        jLabel3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/ButtonFoto/icons8-chevron_down_round.png"))); // NOI18N
        jLabel3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel3MousePressed(evt);
            }
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel3MouseClicked(evt);
            }
        });
        jPanel5.add(jLabel3, "card2");

        jPanel4.add(jPanel5);

        jPanel6.setBackground(new java.awt.Color(255, 255, 255));
        jPanel6.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jPanel6MouseClicked(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jPanel6MouseExited(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jPanel6MouseEntered(evt);
            }
        });
        jPanel6.setLayout(new java.awt.CardLayout());

        jLabel4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/ButtonFoto/icons8-circled_chevron_up.png"))); // NOI18N
        jPanel6.add(jLabel4, "card2");

        jPanel4.add(jPanel6);

        classe.setBorder(new org.edisoncor.gui.util.DropShadowBorder());
        classe.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                classeMouseMoved(evt);
            }
        });
        classe.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                classeActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(section, javax.swing.GroupLayout.PREFERRED_SIZE, 182, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(classe, javax.swing.GroupLayout.PREFERRED_SIZE, 169, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGap(3, 3, 3)
                        .addComponent(classe, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel1)
                        .addComponent(section, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel2))
                    .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel7.setLayout(new javax.swing.BoxLayout(jPanel7, javax.swing.BoxLayout.LINE_AXIS));

        tblmatiere.setAutoCreateRowSorter(true);
        tblmatiere.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tblmatiere.setShowHorizontalLines(false);
        tblmatiere.setShowVerticalLines(false);
        jScrollPane1.setViewportView(tblmatiere);

        jPanel7.add(jScrollPane1);

        jPanel8.setBackground(new java.awt.Color(255, 255, 255));
        jPanel8.setLayout(new java.awt.CardLayout());

        jLabel6.setText("Nombre(s) de registre(s) : 0");
        jPanel8.add(jLabel6, "card2");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(jPanel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(7, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, 180, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, 199, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        getContentPane().add(jPanel1);

        jMenu1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/ButtonFoto/icons8-menu.png"))); // NOI18N
        jMenu1.setText("Action");

        jMenuItem1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/ButtonFoto/icons8-upload_to_cloud.png"))); // NOI18N
        jMenuItem1.setText("Enregistrer");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem1);

        nouveau.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/ButtonFoto/icons8-new_by_copy.png"))); // NOI18N
        nouveau.setText("Nouveau");
        nouveau.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nouveauActionPerformed(evt);
            }
        });
        jMenu1.add(nouveau);

        effacer.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/ButtonFoto/icons8-trash.png"))); // NOI18N
        effacer.setText("Effacer");
        effacer.setEnabled(false);
        effacer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                effacerActionPerformed(evt);
            }
        });
        jMenu1.add(effacer);

        jMenuBar1.add(jMenu1);

        jMenu2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/ButtonFoto/icons8-menu_squared.png"))); // NOI18N
        jMenu2.setText("Edition");

        consulter.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/ButtonFoto/icons8-view_file.png"))); // NOI18N
        consulter.setText("Consulter");
        consulter.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                consulterActionPerformed(evt);
            }
        });
        jMenu2.add(consulter);

        modifier.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/ButtonFoto/icons8-edit_file.png"))); // NOI18N
        modifier.setText("Modifier");
        modifier.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                modifierActionPerformed(evt);
            }
        });
        jMenu2.add(modifier);

        ajouter.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/ButtonFoto/icons8-new_by_copy.png"))); // NOI18N
        ajouter.setText("Ajouter Matière");
        ajouter.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ajouterActionPerformed(evt);
            }
        });
        jMenu2.add(ajouter);

        jMenuItem7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/ButtonFoto/icons8-help.png"))); // NOI18N
        jMenuItem7.setText("Aide");
        jMenu2.add(jMenuItem7);

        jMenuBar1.add(jMenu2);

        setJMenuBar(jMenuBar1);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jPanel5MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel5MouseEntered
        Mn.panelSetMouseEntered(jPanel5);
    }//GEN-LAST:event_jPanel5MouseEntered

    private void jPanel5MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel5MouseExited
        Mn.panelSetMouseExited(jPanel5);
    }//GEN-LAST:event_jPanel5MouseExited

    private void jPanel6MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel6MouseEntered

        Mn.panelSetMouseEntered(jPanel6);
    }//GEN-LAST:event_jPanel6MouseEntered

    private void jPanel6MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel6MouseExited

        Mn.panelSetMouseExited(jPanel6);
    }//GEN-LAST:event_jPanel6MouseExited

    private void modifierActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_modifierActionPerformed
        int aa = Integer.parseInt(code.getText());
        String bb = classe.getText();
        String cc = section.getSelectedItem().toString();
        for (int i = 0; i < tblmatiere.getRowCount(); i++) {
            String xx = tblmatiere.getValueAt(i, 0).toString();
            String xxx = Mn.SEARCHCODE("code_matiere", "description", xx, "matiere");
            String kk = "delete from classe_vs_matiere where code_classe='" + aa + "'and code_matiere='" + xxx + "'";
            Mn.MANIPULEDB(kk);
        }
        for (int i = 0; i < tblmatiere.getRowCount(); i++) {
            String xx = tblmatiere.getValueAt(i, 0).toString();
            String xxx = Mn.SEARCHCODE("code_matiere", "description", xx, "matiere");
            String mm = "call Insert_classematiere(" + aa + "," + xxx + ")";
            Mn.MANIPULEDB(mm);
        }
        String query = "call Insert_classe(" + aa + ",'" + bb + "','" + cc + "')";
        Mn.MODIFIER(query);
        nouveau.doClick();
    }//GEN-LAST:event_modifierActionPerformed

    private void jPanel3MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel3MouseEntered
        Mn.setPanTxtColor(jPanel3, classe);
    }//GEN-LAST:event_jPanel3MouseEntered

    private void jPanel3MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel3MouseExited
        Mn.resetPanTxtColor(jPanel3, classe);
    }//GEN-LAST:event_jPanel3MouseExited

    private void nouveauActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nouveauActionPerformed
        JTextField[] txt = {code, classe};
//        Mn.NEW_BUTTON(txt, "classe", "code_classe");
        Nettoyer();
    }//GEN-LAST:event_nouveauActionPerformed

    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
        for (int i = 0; i < tblmatiere.getRowCount(); i++) {
            code.setText(String.valueOf(Mn.MAXCODE("code_classe", "classe")));
            int aa = Integer.parseInt(code.getText());
            String bb = tblmatiere.getValueAt(i, 0).toString();
            String cc = tblmatiere.getValueAt(i, 1).toString();
            if(!isTextExistInDatabase(bb)){
            String query = "call Insert_classe(" + aa + ",'" + bb + "','" + cc + "','" + Variables.Empresa + "')";
            Mn.MANIPULEDB(query);
            }
        }
        jLabel6.setText(Mn.tableRowcount(tblmatiere));
        JOptionPane.showMessageDialog(rootPane, "Classe enregistrer avec succès");
        nouveau.doClick();
    }//GEN-LAST:event_jMenuItem1ActionPerformed

    private void ajouterActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ajouterActionPerformed
        Matiere pr = new Matiere();
        desktop.add(pr);
        int x = (desktop.getWidth() / 2) - pr.getWidth() / 2;
        int y = (desktop.getHeight() / 2) - pr.getHeight() / 2;
        pr.setLocation(x, y);
        pr.toFront();
        pr.setVisible(true);
    }//GEN-LAST:event_ajouterActionPerformed

    private void effacerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_effacerActionPerformed
        int aa = Integer.parseInt(code.getText());
        String xx = Mn.SELLECT_BUTTON(Variables.Seleccionado1, tblmatiere);
        String xxx = Mn.SEARCHCODE("code_matiere", "description", xx, "matiere");
        String query = "delete from classe_vs_matiere where code_classe='" + aa + "'and code_matiere='" + xxx + "'";
        Mn.MANIPULEDB(query);
        try {

            query = "select * from show_matieres where classe like '%" + classe.getText() + "%' and section='" + section.getSelectedItem().toString() + "'"
                    + " group by matiere order by matiere";
            ResultSet rs = Mn.SEARCHDATA(query);
            try {
                md.setRowCount(0);
                while (rs.next()) {
                    String[] vect = {rs.getString("matiere")};
                    Mn.ADD_TABLE(md, vect);
                }
            } catch (SQLException ex) {
                Logger.getLogger(ClasseForm.class.getName()).log(Level.SEVERE, null, ex);
            }

        } catch (Exception e) {
        }
        nouveau.doClick();
    }//GEN-LAST:event_effacerActionPerformed

    private void consulterActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_consulterActionPerformed
        Cons_classe cs = new Cons_classe();
        Menus.Menus.desktop.add(cs);
        int x = (desktop.getWidth() / 2) - cs.getWidth() / 2;
        int y = (desktop.getHeight() / 2) - cs.getHeight() / 2;
        cs.setLocation(x, y);
        cs.toFront();
        cs.setVisible(true);
    }//GEN-LAST:event_consulterActionPerformed

    private void jPanel5MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel5MouseClicked
        addClasseToGrid(classe);
    }//GEN-LAST:event_jPanel5MouseClicked

    private void jPanel6MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel6MouseClicked
        Mn.REMOVE_TABLE(tblmatiere, md);
    }//GEN-LAST:event_jPanel6MouseClicked

    private void sectionItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_sectionItemStateChanged
//        try {
//            if (section.getSelectedItem() == null) {
//            classe.setValue(null);
//        } else if (section.getSelectedItem().equals("Petit")) {
//            classe.setValue(null);
//            Mn.CLASSE_PETIT_FORMAT(classe);
//        } else if (section.getSelectedItem().equals("Primaire")) {
//            classe.setValue(null);
//            Mn.CLASSE_PRIMAIRE_FORMAT(classe);
//        } else if (section.getSelectedItem().equals("Secondaire")) {
//            classe.setValue(null);
//            Mn.CLASSE_SECONDAIRE_FORMAT(classe);
//        }
//        
//        } catch (Exception e) {
//        }
        String sql = "select description,section from classe where section = '"+section.getSelectedItem().toString()+"' ";
        Mn.FULLTABLE(sql, tblmatiere, md);
        jLabel6.setText("Nombre(s) de registre(s) : "+tblmatiere.getRowCount());
    }//GEN-LAST:event_sectionItemStateChanged

    private void jLabel3MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel3MouseClicked

        addClasseToGrid(classe);
    }//GEN-LAST:event_jLabel3MouseClicked

    private void jLabel3MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel3MousePressed
       
    }//GEN-LAST:event_jLabel3MousePressed

    private void classeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_classeActionPerformed
        addClasseToGrid(classe);
    }//GEN-LAST:event_classeActionPerformed

    private void classeMouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_classeMouseMoved
        jPanel3MouseEntered(evt);
    }//GEN-LAST:event_classeMouseMoved

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ClasseForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ClasseForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ClasseForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ClasseForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new ClasseForm().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuItem ajouter;
    private javax.swing.JTextField classe;
    private javax.swing.JMenuItem consulter;
    private javax.swing.JMenuItem effacer;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem7;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JMenuItem modifier;
    private javax.swing.JMenuItem nouveau;
    private javax.swing.JComboBox section;
    private javax.swing.JTable tblmatiere;
    // End of variables declaration//GEN-END:variables

    private void addClasseToGrid(JTextField Text) {
               
        String kl = "";
        try {
           kl = Text.getText().trim(); 
        } catch (NullPointerException e) {
            System.out.println("nulll Error");
        }
        
            if(kl.equals("")){
         JOptionPane.showMessageDialog(rootPane, "Ce champ ne doit pas etre vide");
         Text.requestFocus();
        }
            else{
        
        if(isGridRedundance(kl)){
            JOptionPane.showMessageDialog(rootPane, "Désolé Cette classe est dejà enregistrer dans le système", "Infos",JOptionPane.INFORMATION_MESSAGE);
        }
        else {
        if(acces.equals("libre")){
          String query="select description as classe,section from classe where section='"+section.getSelectedItem().toString()+"' "
                  + "and institution='"+Variables.Empresa+"'";
          ResultSet rs=Mn.SEARCHDATA(query);
            try {
                 md.setRowCount(0);
                while(rs.next()){
                    String vect[]={rs.getString("classe"),rs.getString("section")};
                    Mn.ADD_TABLE(md, vect);
                } 
                acces="";
            } catch (SQLException ex) {
                Logger.getLogger(ClasseForm.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        else{
        String[] vect={Text.getText(),section.getSelectedItem().toString()};
        if(!section.getSelectedItem().toString().equals("Selectionnez") ) {
             Mn.ADD_TABLE(md, vect);
             Text.setText(null);
            }
        else{
            JOptionPane.showMessageDialog(this,"S'il vous plait séléctionner une séction.");
        }
        }
        }
            }
            jLabel6.setText(Mn.tableRowcount(tblmatiere));
    }
}
