package Forms;

import Methods.Manipulation;
import Methods.Theme;
import Methods.Variables;
import Start.Personaliser;
import com.ezware.oxbow.swingbits.table.filter.TableRowFilterSupport;
import java.awt.Cursor;
import java.awt.EventQueue;
import java.awt.event.KeyEvent;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;
import org.edisoncor.gui.textField.TextFieldRectBackground;

public class Matiere extends javax.swing.JInternalFrame {

    Manipulation Mn = new Manipulation();
    Theme th = new Theme();
    String[][] data = {};
    String[] head = {" Matières", " Sur", "Status"};
    String[] heads = {" Matières Composante", " Sur"};
    DefaultTableModel md = new DefaultTableModel(data, head);
    DefaultTableModel mds = new DefaultTableModel(data, heads);
    JTextField code = new JTextField();
    int contador = 0;
    int kalkil = 0;
    
    public Matiere() {
        initComponents();
        isSystemSet();
        setSize(1053, 577);
        this.setTitle("Scolaris: " + Variables.Empresa + " : Registre des Matières");
        String query = "select * from equivalence where institution='" + Variables.Empresa + "' group by valeur";
        System.out.println("cd " + query);
        Mn.FULLCOMBO(query, equivalence, "valeur");
        nouveau.doClick();
        tblmatiere.setModel(md);
        tblmatiere1.setModel(mds);
        RemplirMatiere();
        //--------------------------------------------------------------
        TableColumnModel cm = tblmatiere.getColumnModel();
        TableColumnModel cm1 = tblmatiere1.getColumnModel();
        cm.getColumn(0).setPreferredWidth(250);
        cm.getColumn(1).setPreferredWidth(15);
        cm.getColumn(2).setPreferredWidth(30);
        cm1.getColumn(0).setPreferredWidth(265);
        cm1.getColumn(1).setPreferredWidth(30);
        composition.setEnabled(false);
        surcomp.setEnabled(false);
        add.setEnabled(false);
        rem.setEnabled(false);
        jLabel5.setEnabled(false);
        jLabel6.setEnabled(false);
        mesaj.setVisible(false);
        mesaj1.setVisible(false);
        mesaj2.setVisible(false);
        
        effacer.setEnabled(false);
        estatusItemStateChanged(null);
        tableMatiereChangeListener();
        TableRowFilterSupport.forTable(tblmatiere).searchable(true).apply();
        TableRowFilterSupport.forTable(tblmatiere1).searchable(true).apply();
        recherche.setName("description");
    }
    
    private boolean isSystemSet(){
        boolean result = false;
        try {
            
            String sql = "select * from show_matiere ";
            
            ResultSet rs = Mn.Rezulta(sql);
            if(rs.absolute(1)){
                result = true;
            }
            else{
                JOptionPane.showMessageDialog(rootPane, "System is not Set for use please go to Outils ==> Personaliser and fill the required field");
                this.dispose();
                new Personaliser().show();
            }
        } catch (SQLException ex) {
            Logger.getLogger(Matiere.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }
    
    private void  autoCompleteEvent(KeyEvent evt,final TextFieldRectBackground textfield){
        switch(evt.getKeyCode()){
            case KeyEvent.VK_BACK_SPACE:
                break;
                case KeyEvent.VK_ENTER:
                    break;
                    default:
                        EventQueue.invokeLater(new Runnable() {

            @Override
            public void run() {
            String txt = textfield.getText();
                Mn.autoComplete(txt,"matiere",textfield.getName(),textfield);
            }
        });
        }
    }
    
    private void tableMatiereChangeListener(){
            tblmatiere.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
            if(tblmatiere.getRowCount() > 0){ 
            Mn.removeDatosOntable(mds);
            setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
            String lestado = "";
            matiere.setText(tblmatiere.getValueAt(tblmatiere.getSelectedRow(), 0).toString());
            equivalence.setSelectedItem(tblmatiere.getValueAt(tblmatiere.getSelectedRow(), 1).toString());
            code.setText(String.valueOf(Mn.SEARCHCODE("code_matiere", "description", Mn.illegalCharacterOnString(matiere.getText()), "matiere")));
            lestado = tblmatiere.getValueAt(tblmatiere.getSelectedRow(), 2).toString();
            int aa = 0;
            if(equivalence.getSelectedIndex() >0 ){
            aa = Integer.parseInt(equivalence.getSelectedItem().toString());
            }
            String query = "select * from show_matiere_compose where matiere='" + Mn.illegalCharacterOnString(matiere.getText()) + "'";
            ResultSet rs = Mn.SEARCHDATA(query);
            try {
                if (rs.next()) {
                    String query1 = "select * from show_matiere_compose where matiere='" + Mn.illegalCharacterOnString(matiere.getText()) + "'";
                    rs = Mn.SEARCHDATA(query1);
                    mds.setRowCount(0);
                    while (rs.next()) {
                        String[] vect = {rs.getString("compose"), rs.getString("sur")};
                        Mn.ADD_TABLE(mds, vect);
                    }
                    for (int i = 0; i < tblmatiere1.getRowCount(); i++) {
                        kalkil = kalkil + Integer.parseInt(tblmatiere1.getValueAt(i, 1).toString());
                    }
                    if (aa == kalkil) {
                        rem.setEnabled(true);
                        mesaj.setVisible(true);
                        mesaj1.setVisible(true);
                        mesaj2.setVisible(true);                        
                        estatus1.setEnabled(false);
                        kalkil = 0;
                    } else {
                        mesaj.setVisible(false);
                        mesaj1.setVisible(false);
                        mesaj2.setVisible(false);                        
                        estatus1.setEnabled(true);
                        estatus1.setSelected(true);                        
                    }
                } else {
                    rem.setEnabled(false);
                    mesaj.setVisible(false);
                    mesaj1.setVisible(false);
                    mesaj2.setVisible(false);                    
                    estatus1.setEnabled(true);
                    estatus1.setSelected(false);
                }
                
            } catch (SQLException ex) {
                Logger.getLogger(Matiere.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        }
            }
        });
        setCursor(Cursor.getDefaultCursor());
        
    }
    
    private void Nettoyer() {
        matiere.setText(null);
        estatus.setSelected(false);
        estatus1.setSelected(false);
        mds.setRowCount(0);
        rem.setEnabled(false);
        mesaj.setVisible(false);
        mesaj1.setVisible(false);
        mesaj2.setVisible(false);
    }
    
    private void RemplirMatiere() {
        setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        String query = "select * from show_matiere order by matiere asc";
        Mn.FULLTABLE(query, tblmatiere, md);
        setCursor(Cursor.getDefaultCursor());
    }
    
    private String ReturnIstitution() {
        setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        String res = "";
        String query = "select nom from institution";
        ResultSet rs = Mn.SEARCHDATA(query);
        try {
            if (rs.first()) {
                res = rs.getString("nom");
            }
        } catch (SQLException ex) {
            Logger.getLogger(Eleves.class.getName()).log(Level.SEVERE, null, ex);
        }
        setCursor(Cursor.getDefaultCursor());
        return res;
    }
    
    private void Insert_compose() {
        
//        String sql = "delete from matiere_compose where matiere = '"+Mn.illegalCharacterOnString(matiere.getText().trim())+"' ";
//        Mn.MANIPULEDB(sql);
        int as =0;
        String sql = "Select code_matiere from matiere where description = '"+matiere.getText().trim()+"' ";
        ResultSet rs = Mn.Rezulta(sql);
        try {
            if(rs.last()){
                as = rs.getInt("code_matiere");
            }
        } catch (SQLException ex) {
            Logger.getLogger(Matiere.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        for (int i = 0; i < tblmatiere1.getRowCount(); i++) {
            setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
            
            String query = "call Insert_matiere_compose("+as+",'" + Mn.illegalCharacterOnString(tblmatiere1.getValueAt(i, 0).toString()) + "'"
                    + "," + tblmatiere1.getValueAt(i, 1).toString() + ")";
            Mn.MANIPULEDB(query);
        }
        setCursor(Cursor.getDefaultCursor());
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        jSeparator1 = new javax.swing.JSeparator();
        jPanel1 = new javax.swing.JPanel();
        estatus1 = new javax.swing.JCheckBox();
        matiere = new org.edisoncor.gui.textField.TextFieldRectBackground();
        jLabel2 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        add = new javax.swing.JButton();
        surcomp = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        composition = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        estatus = new javax.swing.JCheckBox();
        equivalence = new javax.swing.JComboBox();
        jLabel3 = new javax.swing.JLabel();
        rem = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        recherche = new org.edisoncor.gui.textField.TextFieldRectBackground();
        jPanel6 = new javax.swing.JPanel();
        jPanel5 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblmatiere = new javax.swing.JTable(){
            public boolean isCellEditable(int rowIndex, int colIndex){
                return false;
            }
        };
        jPanel4 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tblmatiere1 = new javax.swing.JTable(){
            public boolean isCellEditable(int rowIndex, int colIndex){
                return false;
            }
        };
        jPanel7 = new javax.swing.JPanel();
        mesaj = new javax.swing.JLabel();
        mesaj1 = new javax.swing.JLabel();
        mesaj2 = new javax.swing.JLabel();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        enregistrer = new javax.swing.JMenuItem();
        nouveau = new javax.swing.JMenuItem();
        effacer = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        modifier = new javax.swing.JMenuItem();
        jMenuItem1 = new javax.swing.JMenuItem();

        setClosable(true);
        setIconifiable(true);

        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        estatus1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        estatus1.setForeground(new java.awt.Color(0, 0, 204));
        estatus1.setText("Matière Composée");
        estatus1.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                estatus1ItemStateChanged(evt);
            }
        });

        matiere.setDescripcion("Entrer les matière ici");
        matiere.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                matiereFocusGained(evt);
            }
        });
        matiere.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                matiereActionPerformed(evt);
            }
        });
        matiere.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                matiereKeyReleased(evt);
            }
        });

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel2.setText("Matière : ");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 96, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(7, 7, 7)
                .addComponent(matiere, javax.swing.GroupLayout.PREFERRED_SIZE, 391, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(2, 2, 2)
                .addComponent(estatus1, javax.swing.GroupLayout.PREFERRED_SIZE, 165, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(4, 4, 4)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(estatus1, javax.swing.GroupLayout.DEFAULT_SIZE, 30, Short.MAX_VALUE)
                    .addComponent(matiere, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );

        jPanel2.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        add.setBackground(new java.awt.Color(255, 153, 153));
        add.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/icons8-circled_down.png"))); // NOI18N
        add.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addActionPerformed(evt);
            }
        });

        surcomp.setBackground(new java.awt.Color(255, 204, 204));
        surcomp.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        surcomp.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                surcompActionPerformed(evt);
            }
        });

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(0, 0, 204));
        jLabel6.setText("Sur");

        composition.setBackground(new java.awt.Color(255, 204, 204));
        composition.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(0, 0, 204));
        jLabel5.setText("Composition");

        estatus.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        estatus.setText("Status");
        estatus.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                estatusItemStateChanged(evt);
            }
        });
        estatus.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                estatusActionPerformed(evt);
            }
        });

        equivalence.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        equivalence.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                equivalenceActionPerformed(evt);
            }
        });

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel3.setText("Equivalence : ");

        rem.setBackground(new java.awt.Color(255, 153, 153));
        rem.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/icons8-up_circular_1.png"))); // NOI18N
        rem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                remActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(2, 2, 2)
                .addComponent(jLabel3)
                .addGap(12, 12, 12)
                .addComponent(equivalence, javax.swing.GroupLayout.PREFERRED_SIZE, 293, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(2, 2, 2)
                .addComponent(estatus)
                .addGap(8, 8, 8)
                .addComponent(jLabel5)
                .addGap(12, 12, 12)
                .addComponent(composition, javax.swing.GroupLayout.PREFERRED_SIZE, 235, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(12, 12, 12)
                .addComponent(jLabel6)
                .addGap(12, 12, 12)
                .addComponent(surcomp, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addComponent(add, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addComponent(rem, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGap(3, 3, 3)
                                .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGap(3, 3, 3)
                                .addComponent(estatus, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGap(2, 2, 2)
                                .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGap(3, 3, 3)
                                .addComponent(composition, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGap(2, 2, 2)
                                .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGap(3, 3, 3)
                                .addComponent(surcomp, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGap(2, 2, 2)
                                .addComponent(add, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGap(2, 2, 2)
                                .addComponent(rem, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(equivalence))
                .addContainerGap())
        );

        jPanel3.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel3.setLayout(new java.awt.GridBagLayout());

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel4.setText("Recherche : ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.ipadx = 16;
        gridBagConstraints.ipady = 19;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(9, 6, 14, 0);
        jPanel3.add(jLabel4, gridBagConstraints);

        recherche.setBackground(new java.awt.Color(255, 255, 153));
        recherche.setDescripcion("Entrer les matière ici");
        recherche.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        recherche.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                rechercheFocusGained(evt);
            }
        });
        recherche.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                rechercheKeyReleased(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.ipadx = 459;
        gridBagConstraints.ipady = 21;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(9, 3, 14, 468);
        jPanel3.add(recherche, gridBagConstraints);

        jPanel6.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jPanel5.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel5.setLayout(new javax.swing.BoxLayout(jPanel5, javax.swing.BoxLayout.LINE_AXIS));

        tblmatiere.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tblmatiere.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                tblmatiereMousePressed(evt);
            }
        });
        jScrollPane1.setViewportView(tblmatiere);

        jPanel5.add(jScrollPane1);

        jPanel4.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel4.setLayout(new javax.swing.BoxLayout(jPanel4, javax.swing.BoxLayout.LINE_AXIS));

        tblmatiere1.setBackground(new java.awt.Color(255, 204, 204));
        tblmatiere1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tblmatiere1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                tblmatiere1MousePressed(evt);
            }
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblmatiere1MouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(tblmatiere1);

        jPanel4.add(jScrollPane2);

        jPanel7.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        mesaj.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        mesaj.setForeground(new java.awt.Color(255, 0, 0));
        mesaj.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        mesaj.setText("On ne peut pas ajouter plus Composition pour cette matière,");

        mesaj1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        mesaj1.setForeground(new java.awt.Color(255, 0, 0));
        mesaj1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        mesaj1.setText("Parce que la somme des composition de la table ci dessus est");

        mesaj2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        mesaj2.setForeground(new java.awt.Color(255, 0, 0));
        mesaj2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        mesaj2.setText("égale a son  équivalence.");

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(mesaj, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(mesaj1, javax.swing.GroupLayout.DEFAULT_SIZE, 417, Short.MAX_VALUE)
                    .addComponent(mesaj2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addComponent(mesaj)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(mesaj1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(mesaj2)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
            .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel6Layout.createSequentialGroup()
                    .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, 563, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, 435, Short.MAX_VALUE)
                    .addContainerGap()))
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel6Layout.createSequentialGroup()
                .addContainerGap(268, Short.MAX_VALUE)
                .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
            .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel6Layout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, 330, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, 260, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGap(0, 0, Short.MAX_VALUE)))
        );

        jMenu1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/fichier.jpg"))); // NOI18N
        jMenu1.setText("Action");
        jMenu1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N

        enregistrer.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_E, java.awt.event.InputEvent.CTRL_MASK));
        enregistrer.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/icons8-save_close.png"))); // NOI18N
        enregistrer.setText("Enrégistrer");
        enregistrer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                enregistrerActionPerformed(evt);
            }
        });
        jMenu1.add(enregistrer);

        nouveau.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_N, java.awt.event.InputEvent.CTRL_MASK));
        nouveau.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/icons8-add_list.png"))); // NOI18N
        nouveau.setText("Nouveau");
        nouveau.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nouveauActionPerformed(evt);
            }
        });
        jMenu1.add(nouveau);

        effacer.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_D, java.awt.event.InputEvent.CTRL_MASK));
        effacer.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/icons8-erase.png"))); // NOI18N
        effacer.setText("Effacer");
        effacer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                effacerActionPerformed(evt);
            }
        });
        jMenu1.add(effacer);

        jMenuBar1.add(jMenu1);

        jMenu2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/editer.jpg"))); // NOI18N
        jMenu2.setText("Edition");
        jMenu2.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N

        modifier.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_M, java.awt.event.InputEvent.CTRL_MASK));
        modifier.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/icons8-refresh.png"))); // NOI18N
        modifier.setText("Modifier");
        modifier.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                modifierActionPerformed(evt);
            }
        });
        jMenu2.add(modifier);

        jMenuItem1.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F1, 0));
        jMenuItem1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/icons8-help.png"))); // NOI18N
        jMenuItem1.setText("Aide");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        jMenu2.add(jMenuItem1);

        jMenuBar1.add(jMenu2);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(12, 12, 12)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, 1024, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jSeparator1))
                .addGap(73, 73, 73))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 7, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void nouveauActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nouveauActionPerformed
        JTextField[] txt = {code, matiere};
        Mn.NEW_BUTTON(txt, "matiere", "code_matiere");
        equivalence.setSelectedItem("Selectionnez");
        Nettoyer();
    }//GEN-LAST:event_nouveauActionPerformed

    private void enregistrerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_enregistrerActionPerformed
        setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        
        if(estatus.getText().equals("Inactive")){
            int rep = JOptionPane.showConfirmDialog(rootPane,"Vous estes sur le point d'enregistrer une matiere inactive Voulez-vous continuer","Avertisement",JOptionPane.YES_NO_OPTION);
            if(rep == JOptionPane.YES_OPTION){
                
        int aa = Integer.parseInt(code.getText());
        String bb = matiere.getText();
        String cc = Mn.SEARCHCODE("code_equivalence", "valeur", equivalence.getSelectedItem().toString(), "equivalence");
        String dd = Mn.INSERT_CHECKEDED(estatus);
        String ee = ReturnIstitution();
        String query = "call Insert_matiere(" + aa + ",'" + Mn.illegalCharacterOnString(bb) + "'," + cc + ",'" + dd + "','" + ee + "')";
        String command = "select description from matiere where description like '%" + Mn.illegalCharacterOnString(matiere.getText()) + "%'";
        ResultSet rs = Mn.SEARCHDATA(command);
        try {
            if (rs.first()) {
                if (rs.getString("description").equals(matiere.getText())) {
                    JOptionPane.showMessageDialog(null, "Il existe deja une matiere avec ce nom,s'il vous plait enregistrer avec un autre nom.");
                }
            } else {
                System.out.println("query " + query);
                if (equivalence.getSelectedIndex() <= 0) {
                    JOptionPane.showMessageDialog(rootPane, "Desolé vous devez selectionnez une equivalence");
                    equivalence.requestFocus();
                    return;
                } else {
                    System.out.println("query "+query);
                    Mn.SAVE_BUTTON(query);
                    Insert_compose();
                    RemplirMatiere();
                    nouveau.doClick();
                    equivalence.setSelectedItem("Selectionnez");
                }
            }            
        } catch (SQLException ex) {
            Logger.getLogger(Matiere.class.getName()).log(Level.SEVERE, null, ex);
        }
        }
        else{
            return;
        }
        }
        else{
            
        int aa = Integer.parseInt(code.getText());
        String bb = matiere.getText();
        String cc = Mn.SEARCHCODE("code_equivalence", "valeur", equivalence.getSelectedItem().toString(), "equivalence");
        String dd = Mn.INSERT_CHECKEDED(estatus);
        String ee = ReturnIstitution();
        String query = "call Insert_matiere(" + aa + ",'" + Mn.illegalCharacterOnString(bb) + "'," + cc + ",'" + dd + "','" + ee + "')";
        String command = "select description from matiere where description like '%" + Mn.illegalCharacterOnString(matiere.getText()) + "%'";
        ResultSet rs = Mn.SEARCHDATA(command);
        try {
            if (rs.first()) {
                if (rs.getString("description").equals(matiere.getText())) {
                    JOptionPane.showMessageDialog(null, "Il existe deja une matiere avec ce nom,s'il vous plait enregistrer avec un autre nom.");
                }
            } else {
                System.out.println("query " + query);
                if (equivalence.getSelectedIndex() <= 0) {
                    JOptionPane.showMessageDialog(rootPane, "Desolé vous devez selectionnez une equivalence");
                    equivalence.requestFocus();
                    return;
                } else {
                    System.out.println("query "+query);
                    Mn.SAVE_BUTTON(query);
                    Insert_compose();
                    RemplirMatiere();
                    nouveau.doClick();
                    equivalence.setSelectedItem("Selectionnez");
                }
            }            
        } catch (SQLException ex) {
            Logger.getLogger(Matiere.class.getName()).log(Level.SEVERE, null, ex);
        }
            
            
        }
        setCursor(Cursor.getDefaultCursor());
    }//GEN-LAST:event_enregistrerActionPerformed

    private void effacerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_effacerActionPerformed
        
        Mn.DELETE_BUTTON(code, "matiere", "code_matiere");
        String query = "select * from Show_matiere_compose where matiere='" + matiere.getText() + "'";
        ResultSet rs = Mn.SEARCHDATA(query);
        try {
            if (rs.next()) {
                String query1 = "delete from matiere_compose where matiere='" + matiere.getText() + "'";
                Mn.MANIPULEDB(query1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Matiere.class.getName()).log(Level.SEVERE, null, ex);
        }
        RemplirMatiere();
        nouveau.doClick();
        
    }//GEN-LAST:event_effacerActionPerformed

    private void modifierActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_modifierActionPerformed
        setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        int aa = Integer.parseInt(code.getText());
        String bb = matiere.getText();
        String cc = Mn.SEARCHCODE("code_equivalence", "valeur", equivalence.getSelectedItem().toString(), "equivalence");
        String dd = Mn.INSERT_CHECKEDED(estatus);
        String ee = ReturnIstitution();
        String query = "call Insert_matiere(" + aa + ",'" + Mn.illegalCharacterOnString(bb) + "'," + Mn.illegalCharacterOnString(cc) + ",'" + Mn.illegalCharacterOnString(dd) + "','" + Mn.illegalCharacterOnString(ee) + "')";
        Mn.MODIFIER(query);
        Insert_compose();
        RemplirMatiere();
        nouveau.doClick();
        equivalence.setSelectedItem("Selectionnez");
        setCursor(Cursor.getDefaultCursor());
    }//GEN-LAST:event_modifierActionPerformed

    private void tblmatiereMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblmatiereMousePressed
        
    }//GEN-LAST:event_tblmatiereMousePressed

    private void matiereFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_matiereFocusGained
        // TODO add your handling code here:
    }//GEN-LAST:event_matiereFocusGained

    private void rechercheFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_rechercheFocusGained
        // TODO add your handling code here:
    }//GEN-LAST:event_rechercheFocusGained

    private void rechercheKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_rechercheKeyReleased
        autoCompleteEvent(evt, recherche);
        String query = "select * from show_matiere where matiere like '%" + recherche.getText() + "%'order by matiere asc";
        Mn.FULLTABLE(query, tblmatiere, md);
    }//GEN-LAST:event_rechercheKeyReleased

    private void tblmatiere1MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblmatiere1MousePressed
        
    }//GEN-LAST:event_tblmatiere1MousePressed

    private void estatus1ItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_estatus1ItemStateChanged
        
        if (estatus1.isSelected()) {
            composition.setEnabled(true);
            surcomp.setEnabled(true);
            add.setEnabled(true);
            rem.setEnabled(true);
            jLabel5.setEnabled(true);
            jLabel6.setEnabled(true);
        } else {
            composition.setEnabled(false);
            surcomp.setEnabled(false);
            add.setEnabled(false);
            rem.setEnabled(false);
            jLabel5.setEnabled(false);
            jLabel6.setEnabled(false);
        }
    }//GEN-LAST:event_estatus1ItemStateChanged

    private void addActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addActionPerformed
        setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        
        String[] vect = {composition.getText(), surcomp.getText()};
        String lol = "";
        contador = contador + Integer.parseInt(surcomp.getText().trim());
        
        for (int i = 0; i < tblmatiere1.getRowCount(); i++) {
            if (composition.getText().equals(tblmatiere1.getValueAt(i, 0).toString())) {
                JOptionPane.showMessageDialog(this, "Cette matiere est deja existe dans la table, s'il vous plait veuillez choisir une autre.");
                composition.setText(null);
                surcomp.setText(null);
                lol = "";
            }
        }
        if (!composition.getText().equals("") && tblmatiere1.getRowCount() >= 0) {
            lol = "ajouter";
        }
        if (lol.equals("ajouter")) {
            int aa = 0;
            if(equivalence.getSelectedIndex() > 0){
            aa = Integer.parseInt(equivalence.getSelectedItem().toString());
            }
            if (contador <= aa) {
                Mn.ADD_TABLE(mds, vect);
                composition.setText(null);
                surcomp.setText(null);
                if (contador == aa) {
                    mesaj.setVisible(true);
                    mesaj1.setVisible(true);
                    mesaj2.setVisible(true);
                    estatus1.setSelected(false);
                    estatus1.setEnabled(false);
                    rem.setEnabled(true);
                }
            } else {
                JOptionPane.showMessageDialog(this, "Cette matiere est deja existe dans la table, s'il vous plait veuillez choisir une autre.");                
                contador = contador - Integer.parseInt(surcomp.getText());
                surcomp.setText(null);
            }
        }
        setCursor(Cursor.getDefaultCursor());
    }//GEN-LAST:event_addActionPerformed

    private void remActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_remActionPerformed
        Mn.REMOVE_TABLE(tblmatiere1, mds);
        contador = contador - Integer.parseInt(surcomp.getText());
        estatus1.setEnabled(true);
        estatus1.setSelected(true);
        mesaj.setVisible(false);
        mesaj1.setVisible(false);
        mesaj2.setVisible(false);
        
    }//GEN-LAST:event_remActionPerformed

    private void tblmatiere1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblmatiere1MouseClicked
        composition.setText(tblmatiere1.getValueAt(tblmatiere1.getSelectedRow(), 0).toString());
        surcomp.setText(tblmatiere1.getValueAt(tblmatiere1.getSelectedRow(), 1).toString());
    }//GEN-LAST:event_tblmatiere1MouseClicked

    private void equivalenceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_equivalenceActionPerformed
//        if (equivalence.getSelectedIndex() > 0 && !estatus1.isSelected()) {
//            enregistrer.doClick();
//        }
    }//GEN-LAST:event_equivalenceActionPerformed

    private void matiereActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_matiereActionPerformed
        enregistrer.doClick();
    }//GEN-LAST:event_matiereActionPerformed

    private void estatusActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_estatusActionPerformed
        
    }//GEN-LAST:event_estatusActionPerformed

    private void estatusItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_estatusItemStateChanged
        estatus.setText(Mn.checkString(estatus, "Active", "Inactive"));
    }//GEN-LAST:event_estatusItemStateChanged

    private void matiereKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_matiereKeyReleased
        Mn.autoComplete(matiere.getText(), "matiere", "description", matiere);
    }//GEN-LAST:event_matiereKeyReleased

    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jMenuItem1ActionPerformed

    private void surcompActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_surcompActionPerformed
        add.doClick();
    }//GEN-LAST:event_surcompActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Matiere.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Matiere.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Matiere.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Matiere.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Matiere().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton add;
    private javax.swing.JTextField composition;
    private javax.swing.JMenuItem effacer;
    private javax.swing.JMenuItem enregistrer;
    private javax.swing.JComboBox equivalence;
    private javax.swing.JCheckBox estatus;
    private javax.swing.JCheckBox estatus1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JSeparator jSeparator1;
    public static org.edisoncor.gui.textField.TextFieldRectBackground matiere;
    private javax.swing.JLabel mesaj;
    private javax.swing.JLabel mesaj1;
    private javax.swing.JLabel mesaj2;
    private javax.swing.JMenuItem modifier;
    private javax.swing.JMenuItem nouveau;
    public static org.edisoncor.gui.textField.TextFieldRectBackground recherche;
    private javax.swing.JButton rem;
    private javax.swing.JTextField surcomp;
    private javax.swing.JTable tblmatiere;
    private javax.swing.JTable tblmatiere1;
    // End of variables declaration//GEN-END:variables
}
