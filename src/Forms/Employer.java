package Forms;

import Consults.consultPerson;
import Methods.GenerateMatricule;
import Methods.Manipulation;
import Methods.Security;
import Methods.Theme;
import Methods.Variables;
import Processus.Entrer_note;
import com.ezware.oxbow.swingbits.table.filter.TableRowFilterSupport;
import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.EventQueue;
import java.awt.Image;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumnModel;
import org.edisoncor.gui.textField.TextFieldRectBackground;

public class Employer extends javax.swing.JInternalFrame {

    Manipulation Mn = new Manipulation();
    Theme th = new Theme();
    GenerateMatricule gm = new GenerateMatricule();
    Security sc = new Security();
    int codemp = 0;
    String[][] data = {};
    String[] head = {"Nom", " Prénom", "Document", "Matricule", "Adresse", "Téléphone", "E-mail", "Status"};
    String[] heads = {"Type Employer", "Status"};
    String[] champ = {"estatuses", "code_employer"};
    DefaultTableModel md = new DefaultTableModel(data, head);
    DefaultTableModel mds = new DefaultTableModel(data, heads);
    TableCellRenderer render = new TableCellRenderer() {

        public Component getTableCellRendererComponent(JTable table, Object value, boolean idSelected, boolean hasFocus, int row, int column) {
            JLabel lbl = new JLabel(value == null ? "" : value.toString());
            if (column == 3 && row >= 0) {
                lbl.setHorizontalAlignment(SwingConstants.LEFT);
                lbl.setOpaque(true);
                lbl.setBackground(Color.green);
            }

            if (column == 7 && value.toString().equals("Inactif") && row >= 0) {
                lbl.setHorizontalAlignment(SwingConstants.LEFT);
                lbl.setOpaque(true);
                lbl.setForeground(Color.red);
            }
            return lbl;
        }
    };
    JTextField coode = new JTextField();

    public Employer() {
        initComponents();
        TableRowFilterSupport.forTable(tbleleve).searchable(true).apply();
        tbleleve.getSelectionModel().setSelectionMode(ListSelectionModel.SINGLE_SELECTION );
        setSize(1280, 613);
        jPanel9.setSize(261, 23);
        this.setTitle("Scolaris: " + Variables.Empresa + " : Registre d'Employer");
        code.setEditable(false);
        nouveau.doClick();
        matricule.setText(gm.MatriculeProf());
        matricule.setEditable(false);
        tbleleve.setModel(md);
        tblemp.setModel(mds);
        Consulter();
        Mn.FULLCOMBO("select description from type_document", tpdocument, "description");
        Mn.FULLCOMBO("select description from type_employer", tpemployer, "description");
        //---------------------------------------------------------------------
        TableColumnModel cm = tbleleve.getColumnModel();
        cm.getColumn(0).setPreferredWidth(50);
        cm.getColumn(1).setPreferredWidth(90);
        cm.getColumn(2).setPreferredWidth(80);
        cm.getColumn(3).setPreferredWidth(30);
        cm.getColumn(4).setPreferredWidth(90);
        cm.getColumn(5).setPreferredWidth(40);
        cm.getColumn(6).setPreferredWidth(100);
        cm.getColumn(7).setPreferredWidth(3);
        tbleleve.getColumnModel().getColumn(3).setCellRenderer(render);
        tbleleve.getColumnModel().getColumn(7).setCellRenderer(render);
        //------------------------------------------------------------------------------
        TableRowFilterSupport.forTable(tbleleve).searchable(true).apply();
        TableRowFilterSupport.forTable(tblemp).searchable(true).apply();
        effacer.setEnabled(false);
        tableEleveChangeListener();
        naissance.setLocale(Locale.FRENCH);
        prenom.setName("prenom");
        nom.setName("nom");
        adresse.setName("adresse");
        
        String fichero  = System.getProperty("user.dir")+"/src/Pictures/icons8-detective.png";
        System.out.println("cd "+fichero);
        Image image = Toolkit.getDefaultToolkit().createImage(fichero); 
        Cursor cursor = Toolkit.getDefaultToolkit().createCustomCursor(image, new Point(10,10),"aa"); 
        code.setCursor(cursor);
        System.out.println("siz "+getSize());
    }
    
    private void photoIcon(){
        String fichero  = System.getProperty("user.dir")+"/src/Pictures/icons8-user_male_circle_filled.png";
        System.out.println("cd "+fichero);
        Image image = Toolkit.getDefaultToolkit().createImage(fichero); 
        ImageIcon imageicon = new ImageIcon(image);
        
        if(photo.getIcon() == null){
            photo.setIcon(imageicon);
            photo.setText("Double click pour Telecharger une Photo");
        }
    }
    
    private int getEmployerCode(String matricule){
        int code = 0;
        String sql = "select code_employer from employer where matricule = '"+matricule+"' ";
        ResultSet rs = Mn.Rezulta(sql);
        try {
            if(rs.last()){
                code = rs.getInt("code_employer");
            }
        } catch (SQLException ex) {
            Logger.getLogger(Employer.class.getName()).log(Level.SEVERE, null, ex);
        }
        return code;
    }
    
    private void focusLost(){
        try {
            String sql = "SELECT * FROM tercero_general_view where code = '"+code.getText().trim()+"' ";
            System.out.println(sql);
            ResultSet rs = Mn.Rezulta(sql);
            if(rs.last()){
                modificationDisabled();
                nom.setText(rs.getString("nom"));
                prenom.setText(rs.getString("prenom"));
                sexe.setSelectedItem(rs.getString("sexe"));
//                nationalite.setSelectedItem(rs.getString("nationalite"));
                etatcivil.setSelectedItem(rs.getString("etat_civil"));
                tpdocument.setSelectedItem(rs.getString("type_document"));
                document.setText(rs.getString("iddocument"));
//                date_emission.setDate(rs.getDate("date_emission"));
//                date_expiration.setDate(rs.getDate("date_expiration"));
//                lieu.setText(rs.getString("lieu_naissance"));
                naissance.setDate(rs.getDate("date_naissance"));
                adresse.setText(rs.getString("adresse"));
                
                //image
                sql = "select imagecol from show_image where code = '"+code.getText().trim()+"'  ";
                Mn.ShowPhotos(sql, photo);
                
                
                //email
                sql = "select mail from show_email where code = '"+code.getText().trim()+"' ";
                ResultSet rse = Mn.Rezulta(sql);
                if(rse.first()) { 
                    mail.setText(rse.getString("mail"));
                }
                
                //telephones
                sql = "SELECT phone_number FROM show_telephones where code = '"+code.getText().trim()+"' ";
                ResultSet rst = Mn.Rezulta(sql);
                if(rst.first()) { 
                   teleleve.setText(rst.getString("phone_number"));
                }
                
                //sanguins
            ResultSet er = Mn.Rezulta("select sang from show_sanguins where code='"+code.getText().trim()+"' ");
            if(er.first()){
                sanguins.setSelectedItem(er.getString("sang"));
            }
                
            }  
        } catch (SQLException ex) {
            Logger.getLogger(Person.class.getName()).log(Level.SEVERE, null, ex);
        }
    }


    private void  autoCompleteEvent(KeyEvent evt,final TextFieldRectBackground textfield){
        switch(evt.getKeyCode()){
            case KeyEvent.VK_BACK_SPACE:
                break;
                case KeyEvent.VK_ENTER:
                    break;
                    default:
                        EventQueue.invokeLater(new Runnable() {

            @Override
            public void run() {
            String txt = textfield.getText();
                Mn.autoComplete(txt,"tercero",textfield.getName(),textfield);
            }
        });
        }
    }
    
    private void tableEleveChangeListener() throws ArrayIndexOutOfBoundsException{
        tbleleve.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
            if (code.getText().isEmpty()) {
                Nettoyer();
            } 
            else {
                if(tbleleve.getRowCount() > 0){
                String aa = tbleleve.getValueAt(tbleleve.getSelectedRow(), 3).toString();
                String query = "select * from Show_employers where matricule='" + aa + "'";
                ResultSet rs = Mn.SEARCHDATA(query);
                try {
                    if (rs.first()) {
                        System.out.println("size "+getSize());
                        modificationDisabled();
                        tpemployer.setEnabled(false);
                        state.setEnabled(false);
                        estatus.setEnabled(false);
                        code.setText(rs.getString("code_employer"));
                        coode.setText(rs.getString("code_tercero"));
                        prenom.setText(rs.getString("nom"));
                        nom.setText(rs.getString("prenom"));
                        adresse.setText(rs.getString("adresse"));
                        teleleve.setText(rs.getString("telephone"));
                        matricule.setText(rs.getString("matricule"));
                        document.setText(rs.getString("document"));
                        naissance.setDate(rs.getDate("naissance"));
                        mail.setText(rs.getString("mail"));
                        etatcivil.setSelectedItem(rs.getString("etat_civil"));
                        tpdocument.setSelectedItem(rs.getString("tpdocument"));
                        sexe.setSelectedItem(rs.getString("sexe"));
                        sanguins.setSelectedItem(rs.getString("sang"));
                        
                        estatus.setSelected(Mn.CONSULT_CHECKED("Show_employers", code, champ));
                        prenom.requestFocus();
                        RemplirEmployer();
                        
                        String cd = "";
                        String sql = "select code from show_empleado where matricule = '"+aa+"' ";
                        ResultSet rsv = Mn.Rezulta(sql);
                        if(rsv.last()){
                          cd = rsv.getString("code");
                        }
                        Mn.ShowPhotos("SELECT * FROM show_image where code  = '"+cd+"' ", photo);
                    }
                } catch (SQLException ex) {
                    Logger.getLogger(Eleves.class.getName()).log(Level.SEVERE, null, ex);
                }
        }
            }
            }
        });
    }
    
    private void Nettoyer() {
        prenom.setText(null);
        nom.setText(null);
        adresse.setText(null);
        teleleve.setValue(null);
        Mn.PHONE_FROMAT(teleleve);
        mail.setText(null);
        naissance.setCalendar(null);
        sexe.setSelectedItem("Selectionnez");
        tpdocument.setSelectedItem("Selectionnez");
        estatus.setSelected(false);
        tpemployer.setSelectedItem("Selectionnez");
        etatcivil.setSelectedItem("Selectionnez");
        sanguins.setSelectedItem("Selectionnez");
        document.setText(null);
        matricule.setText(gm.MatriculeProf());
        Mn.EMPTYTABLE(tblemp);
        photo.setIcon(null); photo.updateUI();
        photoIcon();
    }
    
    private void modificationDisabled(){
        prenom.setEditable(false);
        nom.setEditable(false);
        adresse.setEditable(false);
        teleleve.setEditable(false);
        mail.setEditable(false);
        naissance.setEnabled(false);
        sexe.setEnabled(false);
        tpdocument.setEnabled(false);
//        estatus.setEnabled(false);
//        tpemployer.setEnabled(false);
        etatcivil.setEnabled(false);
        sanguins.setEnabled(false);
        document.setEditable(false);
        matricule.setEditable(false);
    }
    
    private void modificationEnabled(){
        prenom.setEditable(true);
        nom.setEditable(true);
        adresse.setEditable(true);
        teleleve.setEditable(true);
        mail.setEditable(true);
        naissance.setEnabled(true);
        sexe.setEnabled(true);
        tpdocument.setEnabled(true);
        estatus.setEnabled(true);
        tpemployer.setEnabled(true);
        etatcivil.setEnabled(true);
        sanguins.setEnabled(true);
        document.setEditable(true);
        matricule.setEditable(true);
        state.setEnabled(true);
    }

    private void Consulter() {
        String select = "";
        String query = "select * from Show_employer";
        ResultSet rs = Mn.SEARCHDATA(query);
        try {
            Mn.removeDatosOntable(md);
            while (rs.next()) {
                boolean etat = rs.getBoolean("estatuses");
                if (etat == true) {
                    select = "Actif";
                } else {
                    select = "Inactif";
                }
                String[] vect = {rs.getString("nom"), rs.getString("prenom"), rs.getString("document"), rs.getString("matricule"),
                    rs.getString("adresse"), rs.getString("telephone"), rs.getString("mail"), select};
                Mn.ADD_TABLE(md, vect);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Entrer_note.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void RemplirEmployer() {
        String aa = tbleleve.getValueAt(tbleleve.getSelectedRow(), 3).toString();
        String query = "select description,estatuses from type_employer te join employer_vs_type et on(te.idtype_employer = et.idtype_employer)\n" +
" where et.matricule='"+aa+"' ";
        Mn.FULLTABLE(query, tblemp, mds);
    }

    private String ReturnIstitution() {
        String res = "";
        String query = "select nom from institution";
        ResultSet rs = Mn.SEARCHDATA(query);
        try {
            if (rs.first()) {
                res = rs.getString("nom");
            }
        } catch (SQLException ex) {
            Logger.getLogger(Eleves.class.getName()).log(Level.SEVERE, null, ex);
        }
        return res;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        code = new org.edisoncor.gui.textField.TextFieldRectBackground();
        prenom = new org.edisoncor.gui.textField.TextFieldRectBackground();
        nom = new org.edisoncor.gui.textField.TextFieldRectBackground();
        sexe = new javax.swing.JComboBox();
        adresse = new org.edisoncor.gui.textField.TextFieldRectBackground();
        naissance = new com.toedter.calendar.JDateChooser();
        jPanel4 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jPanel5 = new javax.swing.JPanel();
        matricule = new org.edisoncor.gui.textField.TextFieldRectBackground();
        etatcivil = new javax.swing.JComboBox();
        tpdocument = new javax.swing.JComboBox();
        document = new javax.swing.JFormattedTextField();
        teleleve = new javax.swing.JFormattedTextField();
        mail = new org.edisoncor.gui.textField.TextFieldRectBackground();
        jPanel6 = new javax.swing.JPanel();
        tpemployer = new javax.swing.JComboBox();
        state = new javax.swing.JCheckBox();
        add = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        tblemp = new javax.swing.JTable(){
            public boolean isCellEditable(int rowIndex, int colIndex){
                return false;
            }
        };
        rem = new javax.swing.JButton();
        sanguins = new javax.swing.JComboBox();
        jLabel17 = new javax.swing.JLabel();
        estatus = new javax.swing.JCheckBox();
        jPanel7 = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        tbleleve = new javax.swing.JTable(){
            public boolean isCellEditable(int rowindex,int colIndex){
                return false;
            }
        };
        jPanel8 = new javax.swing.JPanel();
        jLabel8 = new javax.swing.JLabel();
        recherche = new org.edisoncor.gui.textField.TextFieldRectBackground();
        jPanel9 = new javax.swing.JPanel();
        photo = new javax.swing.JLabel();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        enregistrer = new javax.swing.JMenuItem();
        nouveau = new javax.swing.JMenuItem();
        effacer = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        modifier = new javax.swing.JMenuItem();
        jMenuItem1 = new javax.swing.JMenuItem();

        setClosable(true);
        setIconifiable(true);

        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel2.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel2.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel1.setText("Code : ");
        jPanel2.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(38, 8, -1, -1));

        jLabel15.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel15.setText("prénom : ");
        jPanel2.add(jLabel15, new org.netbeans.lib.awtextra.AbsoluteConstraints(19, 46, -1, -1));

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel2.setText("Nom : ");
        jPanel2.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 90, -1, -1));

        jLabel16.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel16.setText("Sexe : ");
        jPanel2.add(jLabel16, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 137, -1, -1));

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel3.setText("Adresse : ");
        jPanel2.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(18, 185, -1, -1));

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel6.setText("Naissance : ");
        jPanel2.add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(4, 220, -1, 32));

        jPanel1.add(jPanel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, 90, 260));

        jPanel3.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel3.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        code.setDescripcion("Entrer le code ici");
        code.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                codeFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                codeFocusLost(evt);
            }
        });
        code.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                codeMouseClicked(evt);
            }
        });
        jPanel3.add(code, new org.netbeans.lib.awtextra.AbsoluteConstraints(2, 2, 186, 32));

        prenom.setDescripcion("Entrer le nom de l'élève ici");
        prenom.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                prenomKeyReleased(evt);
            }
        });
        jPanel3.add(prenom, new org.netbeans.lib.awtextra.AbsoluteConstraints(2, 43, 186, 32));

        nom.setDescripcion("Entrer le prénom de l'élève ici");
        nom.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                nomKeyReleased(evt);
            }
        });
        jPanel3.add(nom, new org.netbeans.lib.awtextra.AbsoluteConstraints(2, 87, 186, 32));

        sexe.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        sexe.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Selectionnez", "Féminin", "Masculin", "Autres" }));
        jPanel3.add(sexe, new org.netbeans.lib.awtextra.AbsoluteConstraints(2, 131, 186, 35));

        adresse.setDescripcion("Entrer l' adresse ici");
        adresse.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                adresseKeyReleased(evt);
            }
        });
        jPanel3.add(adresse, new org.netbeans.lib.awtextra.AbsoluteConstraints(2, 182, 186, 32));

        naissance.setDateFormatString("EEEE dd MMM yyyy");
        naissance.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        naissance.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                naissanceFocusLost(evt);
            }
        });
        jPanel3.add(naissance, new org.netbeans.lib.awtextra.AbsoluteConstraints(2, 223, 186, 32));

        jPanel1.add(jPanel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 10, 190, 260));

        jPanel4.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel4.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel5.setText("Matricule : ");
        jPanel4.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(2, 7, 86, 31));

        jLabel7.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel7.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel7.setText("Etat Civil : ");
        jPanel4.add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(2, 56, 83, 18));

        jLabel14.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel14.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel14.setText("Type Doc. : ");
        jPanel4.add(jLabel14, new org.netbeans.lib.awtextra.AbsoluteConstraints(6, 100, -1, 35));

        jLabel13.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel13.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel13.setText("Document : ");
        jPanel4.add(jLabel13, new org.netbeans.lib.awtextra.AbsoluteConstraints(3, 155, -1, -1));

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel4.setText("Telephone : ");
        jPanel4.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(2, 189, -1, 32));

        jLabel10.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel10.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel10.setText("E-mail : ");
        jPanel4.add(jLabel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(2, 235, 86, -1));

        jPanel1.add(jPanel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(290, 10, 90, 260));

        jPanel5.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel5.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        matricule.setDescripcion("Entrer le matricule de l'élève ici");
        jPanel5.add(matricule, new org.netbeans.lib.awtextra.AbsoluteConstraints(2, 3, 196, 32));

        etatcivil.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        etatcivil.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Selectionnez", "Marié(e)", "Divorcé(e)", "Célibataire", "Veuf(ve)", "Autres" }));
        jPanel5.add(etatcivil, new org.netbeans.lib.awtextra.AbsoluteConstraints(2, 41, 196, 39));

        tpdocument.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jPanel5.add(tpdocument, new org.netbeans.lib.awtextra.AbsoluteConstraints(2, 92, 196, 40));
        jPanel5.add(document, new org.netbeans.lib.awtextra.AbsoluteConstraints(2, 144, 196, 31));

        teleleve.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jPanel5.add(teleleve, new org.netbeans.lib.awtextra.AbsoluteConstraints(2, 186, 196, 32));

        mail.setDescripcion("Entrer le nom de l'élève ici");
        mail.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                mailFocusLost(evt);
            }
        });
        jPanel5.add(mail, new org.netbeans.lib.awtextra.AbsoluteConstraints(2, 224, 196, 32));

        jPanel1.add(jPanel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 10, 200, 260));

        jPanel6.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel6.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        tpemployer.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        tpemployer.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Selectionnez", "Directeur", "Professeur", "Secrétaire", "Infirmière", "Contable", "Censeur", "Sécurité", "Surveillant", "Autres" }));
        jPanel6.add(tpemployer, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 225, 33));

        state.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        state.setText("Status");
        state.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                stateItemStateChanged(evt);
            }
        });
        jPanel6.add(state, new org.netbeans.lib.awtextra.AbsoluteConstraints(230, 0, -1, -1));

        add.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/Bajar.png"))); // NOI18N
        add.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addActionPerformed(evt);
            }
        });
        jPanel6.add(add, new org.netbeans.lib.awtextra.AbsoluteConstraints(260, 40, 41, 39));

        tblemp.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tblemp.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                tblempMousePressed(evt);
            }
        });
        jScrollPane2.setViewportView(tblemp);

        jPanel6.add(jScrollPane2, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 40, 256, 79));

        rem.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/Subir.png"))); // NOI18N
        rem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                remActionPerformed(evt);
            }
        });
        jPanel6.add(rem, new org.netbeans.lib.awtextra.AbsoluteConstraints(260, 80, 42, 39));

        sanguins.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        sanguins.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Selectionnez", "A+", "A-", "B+", "B-", "O+", "O-", "AB+", "AB-", "Autres" }));
        jPanel6.add(sanguins, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 130, 173, 36));

        jLabel17.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel17.setText("Groupe Sanguins");
        jPanel6.add(jLabel17, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 140, -1, -1));

        estatus.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        estatus.setText("Status");
        estatus.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                estatusItemStateChanged(evt);
            }
        });
        jPanel6.add(estatus, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 170, -1, 32));

        jPanel1.add(jPanel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(580, 10, 320, 260));

        jPanel7.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel7.setLayout(new javax.swing.BoxLayout(jPanel7, javax.swing.BoxLayout.LINE_AXIS));

        tbleleve.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane3.setViewportView(tbleleve);

        jPanel7.add(jScrollPane3);

        jPanel8.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel8.setLayout(new java.awt.GridBagLayout());

        jLabel8.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel8.setText("Recherche");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(10, 12, 0, 0);
        jPanel8.add(jLabel8, gridBagConstraints);

        recherche.setDescripcion("Entrer le matricule de l'élève ici");
        recherche.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                rechercheKeyReleased(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridheight = 2;
        gridBagConstraints.ipadx = 458;
        gridBagConstraints.ipady = 16;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(4, 18, 4, 13);
        jPanel8.add(recherche, gridBagConstraints);

        jPanel9.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        jPanel9.setLayout(new java.awt.BorderLayout());

        photo.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        photo.setText("Double click pour Telecharger une Photo");
        photo.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        photo.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                photoMouseClicked(evt);
            }
        });
        jPanel9.add(photo, java.awt.BorderLayout.CENTER);

        jMenu1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/fichier.jpg"))); // NOI18N
        jMenu1.setText("Action");
        jMenu1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N

        enregistrer.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_E, java.awt.event.InputEvent.CTRL_MASK));
        enregistrer.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/icons8-save_close.png"))); // NOI18N
        enregistrer.setText("Enrégistrer");
        enregistrer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                enregistrerActionPerformed(evt);
            }
        });
        jMenu1.add(enregistrer);

        nouveau.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_N, java.awt.event.InputEvent.CTRL_MASK));
        nouveau.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/icons8-add_list.png"))); // NOI18N
        nouveau.setText("Nouveau");
        nouveau.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nouveauActionPerformed(evt);
            }
        });
        jMenu1.add(nouveau);

        effacer.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_D, java.awt.event.InputEvent.CTRL_MASK));
        effacer.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/icons8-erase.png"))); // NOI18N
        effacer.setText("Effacer");
        effacer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                effacerActionPerformed(evt);
            }
        });
        jMenu1.add(effacer);

        jMenuBar1.add(jMenu1);

        jMenu2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/editer.jpg"))); // NOI18N
        jMenu2.setText("Edition");
        jMenu2.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N

        modifier.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_M, java.awt.event.InputEvent.CTRL_MASK));
        modifier.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/icons8-refresh.png"))); // NOI18N
        modifier.setText("Modifier");
        modifier.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                modifierActionPerformed(evt);
            }
        });
        jMenu2.add(modifier);

        jMenuItem1.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F1, 0));
        jMenuItem1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/icons8-help.png"))); // NOI18N
        jMenuItem1.setText("Aide");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        jMenu2.add(jMenuItem1);

        jMenuBar1.add(jMenu2);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jPanel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jPanel9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, 562, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 275, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(20, 20, 20)
                        .addComponent(jPanel9, javax.swing.GroupLayout.PREFERRED_SIZE, 240, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(5, 5, 5)
                .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, 220, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void nouveauActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nouveauActionPerformed
        JTextField[] txt = {code, prenom};
        codemp = 0;
        Mn.NEW_BUTTON(txt, "employer", "code_employer");
        Nettoyer();
        modificationEnabled();
    }//GEN-LAST:event_nouveauActionPerformed

    private void enregistrerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_enregistrerActionPerformed

//        int aa = Mn.MAXCODE("code_tercero", "tercero");
        
        String mat = matricule.getText().trim();
        String pas = sc.password();
        String jjj = Mn.INSERT_CHECKED(estatus);
        
        String query = "call Insert_employer ("+codemp+",'"+mat+"',"+Variables.code+",'"+Variables.code_terceross+"','"+pas+"',"+jjj+") ";
        System.out.println("quer "+query);
        Mn.MANIPULEDB(query);
        
        int cd = getEmployerCode(mat);
        for (int i = 0; i < tblemp.getRowCount(); i++) {
        int type = Mn.getCodeFromString("type_employer", "idtype_employer",tblemp.getValueAt(i, 0).toString().trim());
        String state = tblemp.getValueAt(i, 1).toString().trim();
        query = "call employer_vs_type_procedure ("+cd+",'"+mat+"',"+type+",'"+state+"') ";
            System.out.println("quer "+query);
        Mn.MANIPULEDB(query);
        
        }
        
        Consulter();
        nouveau.doClick();
    }//GEN-LAST:event_enregistrerActionPerformed

    private void effacerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_effacerActionPerformed
        Mn.DELETE_BUTTON(code, "employer", "code_employer");
        Consulter();
        nouveau.doClick();
    }//GEN-LAST:event_effacerActionPerformed

    private void modifierActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_modifierActionPerformed
        int aa = Integer.parseInt(coode.getText());
        int ab = 0;
        int aab = 0;
        String lalal = "";
        int abb = Integer.parseInt(code.getText());
        String bb = prenom.getText();
        String cc = nom.getText();
        String dd = adresse.getText();
        String ee = teleleve.getText();
        String ff = sexe.getSelectedItem().toString();
        String gg = ReturnIstitution();
        String querys = "select code_employer from employer where code_employer=" + code.getText() + "";
        ResultSet rs = Mn.SEARCHDATA(querys);
        try {
            if (rs.first()) {
                ab = rs.getInt("code_employer");
            }
        } catch (SQLException ex) {
            Logger.getLogger(Eleves.class.getName()).log(Level.SEVERE, null, ex);
        }
        if (abb == ab) {
            aa = ab;
        }
        String query = "call Insert_tercero(" + aa + ",'" + bb + "','" + cc + "','" + ff + "','" + dd + "','" + ee + "','" + gg + "')";
        Mn.MANIPULEDB(query);
        int aaa = Integer.parseInt(code.getText());
        //-----------------------------------------------------------------------------
        String hola = "select code_employer from employer where code_employer=" + code.getText() + "";
        ResultSet rss = Mn.SEARCHDATA(hola);
        try {
            if (rss.first()) {
                ab = rss.getInt("code_employer");
            }
        } catch (SQLException ex) {
            Logger.getLogger(Eleves.class.getName()).log(Level.SEVERE, null, ex);
        }
        if (abb == ab) {
            aa = ab;
            for (int i = 0; i < tblemp.getRowCount(); i++) {
                String qq = tblemp.getValueAt(i, 0).toString();
                String qqq = tblemp.getValueAt(i, 1).toString();
                String lol = "select code_employer from employer_vs_type where type_employer='" + qq + "' ";
                ResultSet res = Mn.SEARCHDATA(lol);
                try {
                    if (res.next()) {
                        aab = res.getInt("code_employer");
                    }
                } catch (SQLException ex) {
                    Logger.getLogger(Employer.class.getName()).log(Level.SEVERE, null, ex);
                }
                if (aaa == aab) {
                    lalal = "update employer_vs_type set code_employer=" + aaa + ",type_employer='" + qq + "',estatuses='" + qqq + "' where type_employer='" + qq + "'";
                    aab = 0;
                } else {
                    lalal = "call Insert_type(" + aaa + ",'" + qq + "','" + qqq + "')";
                }
                Mn.MANIPULEDB(lalal);
            }
        } else {
            for (int i = 0; i < tblemp.getRowCount(); i++) {
                String qq = tblemp.getValueAt(i, 0).toString();
                String qqq = tblemp.getValueAt(i, 1).toString();
                String lala = "call Insert_type(" + aaa + ",'" + qq + "','" + qqq + "')";
                Mn.MANIPULEDB(lala);
            }
        }
        //-----------------------------------------------------------------------------

        String bbb = matricule.getText();
        String ccc = Mn.INSERTDATECHOOSER(naissance);
        String ddd = etatcivil.getSelectedItem().toString();
        String fff = document.getText();
        String ggg = mail.getText();
        String ffff = tpdocument.getSelectedItem().toString();
        String iii = Mn.DATEHOURNOW();
        String jjj = Mn.INSERT_CHECKED(estatus);
        String pas = sc.password();
        query = "call Insert_employer(" + aaa + "," + aa + ",'" + bbb + "','" + pas + "','" + ccc + "','" + fff + "','" + ffff + "','" + ddd + "','" + ggg + "',"
                + "'" + iii + "'," + jjj + ")";
        // System.out.println(query);
        Mn.MODIFIER(query);
        String san = sanguins.getSelectedItem().toString();
        Mn.MANIPULEDB("call Insert_sanguins (" + aa + ",'" + san + "')");
        Consulter();
        nouveau.doClick();
    }//GEN-LAST:event_modifierActionPerformed

    private void codeFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_codeFocusGained

    }//GEN-LAST:event_codeFocusGained

    private void codeFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_codeFocusLost

    }//GEN-LAST:event_codeFocusLost

    private void naissanceFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_naissanceFocusLost

    }//GEN-LAST:event_naissanceFocusLost

    private void rechercheKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_rechercheKeyReleased
        String select = "";
        String query = "select nom,prenom,document,matricule,adresse,telephone,mail,estatuses from Show_employers "
                + " where nom like '%" + recherche.getText() + "%' or prenom like '%" + recherche.getText() + "%'"
                + " or matricule like '%" + recherche.getText() + "%' or document like '%" + recherche.getText() + "%'";
        ResultSet rs = Mn.SEARCHDATA(query);
        try {
            md.setRowCount(0);
            while (rs.next()) {
                boolean etat = rs.getBoolean("estatuses");
                if (etat == true) {
                    select = "Actif";
                } else {
                    select = "Inactif";
                }
                String[] vect = {rs.getString("nom"), rs.getString("prenom"), rs.getString("document"), rs.getString("matricule"),
                    rs.getString("adresse"), rs.getString("telephone"), rs.getString("mail"), select};
                Mn.ADD_TABLE(md, vect);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Entrer_note.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_rechercheKeyReleased

    private void addActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addActionPerformed
        String lestado = "";
        if (state.isSelected()) {
            lestado = "Actif";
        } else {
            lestado = "Inactif";
        }
        String[] vect = {tpemployer.getSelectedItem().toString(), lestado};
        Mn.ADD_TABLE(mds, vect);
        tpemployer.setSelectedItem(null);
        state.setSelected(false);
    }//GEN-LAST:event_addActionPerformed

    private void remActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_remActionPerformed
        Mn.REMOVE_TABLE(tblemp, mds);
    }//GEN-LAST:event_remActionPerformed

    private void mailFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_mailFocusLost
        Mn.VALIDATEMAIL(mail.getText(), mail);
    }//GEN-LAST:event_mailFocusLost

    private void tblempMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblempMousePressed
        JTable table = (JTable) evt.getSource();
        Point pt = evt.getPoint();
        int row = table.rowAtPoint(pt);
        if (evt.getClickCount() == 2) {
            tpemployer.setSelectedItem(tblemp.getValueAt(tblemp.getSelectedRow(), 0).toString());
            String lestado = tblemp.getValueAt(tblemp.getSelectedRow(), 1).toString();
            if (lestado.equals("Actif")) {
                state.setSelected(true);
            } else {
                state.setSelected(false);
            }
            mds.removeRow(tblemp.getSelectedRow());
        }
    }//GEN-LAST:event_tblempMousePressed

    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jMenuItem1ActionPerformed

    private void prenomKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_prenomKeyReleased
        autoCompleteEvent(evt, prenom);
    }//GEN-LAST:event_prenomKeyReleased

    private void nomKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_nomKeyReleased
        autoCompleteEvent(evt, nom);
    }//GEN-LAST:event_nomKeyReleased

    private void adresseKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_adresseKeyReleased
        autoCompleteEvent(evt, adresse);
    }//GEN-LAST:event_adresseKeyReleased

    private void photoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_photoMouseClicked
        
    }//GEN-LAST:event_photoMouseClicked

    private void codeMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_codeMouseClicked
        if(evt.getClickCount() == 2){
        consultPerson cp = new consultPerson(null, true);
        cp.show();
        cp.pack();
        code.setText(Variables.code_tercero);
            System.out.println("code "+Variables.code_tercero);
        focusLost();
        }
    }//GEN-LAST:event_codeMouseClicked

    private void stateItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_stateItemStateChanged
        if(state.isSelected()){
            state.setText("Active");
        }
        else{
            state.setText("Inactive");
        }
    }//GEN-LAST:event_stateItemStateChanged

    private void estatusItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_estatusItemStateChanged
        if(estatus.isSelected()){
            estatus.setText("Active");
        }
        else{
            estatus.setText("Inactive");
        }
    }//GEN-LAST:event_estatusItemStateChanged

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Employer.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Employer.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Employer.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Employer.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Employer().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton add;
    private org.edisoncor.gui.textField.TextFieldRectBackground adresse;
    public static org.edisoncor.gui.textField.TextFieldRectBackground code;
    private javax.swing.JFormattedTextField document;
    private javax.swing.JMenuItem effacer;
    private javax.swing.JMenuItem enregistrer;
    private javax.swing.JCheckBox estatus;
    private javax.swing.JComboBox etatcivil;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private org.edisoncor.gui.textField.TextFieldRectBackground mail;
    private org.edisoncor.gui.textField.TextFieldRectBackground matricule;
    private javax.swing.JMenuItem modifier;
    private com.toedter.calendar.JDateChooser naissance;
    private org.edisoncor.gui.textField.TextFieldRectBackground nom;
    private javax.swing.JMenuItem nouveau;
    private javax.swing.JLabel photo;
    private org.edisoncor.gui.textField.TextFieldRectBackground prenom;
    private org.edisoncor.gui.textField.TextFieldRectBackground recherche;
    private javax.swing.JButton rem;
    private javax.swing.JComboBox sanguins;
    private javax.swing.JComboBox sexe;
    private javax.swing.JCheckBox state;
    private javax.swing.JTable tbleleve;
    private javax.swing.JTable tblemp;
    private javax.swing.JFormattedTextField teleleve;
    private javax.swing.JComboBox tpdocument;
    private javax.swing.JComboBox tpemployer;
    // End of variables declaration//GEN-END:variables
}
