package Forms;

import Methods.Manipulation;
import Methods.Theme;
import Methods.Variables;
import java.awt.Color;
import java.awt.Component;
import java.awt.Point;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumnModel;

public class Utilisateurs extends javax.swing.JInternalFrame {

    Manipulation Mn = new Manipulation();
    Theme th = new Theme();
    String[][] data = {};
    String[] head = {" Type Employer", "Status"};
    String[] heads = {" Matricule", " Nom", " Prénom", " Nom Utilisateur", " Niv. Accès", "Status"};
    DefaultTableModel md = new DefaultTableModel(data, head);
    DefaultTableModel mds = new DefaultTableModel(data, heads);
    TableCellRenderer render = new TableCellRenderer() {
        public Component getTableCellRendererComponent(JTable table, Object value, boolean idSelected, boolean hasFocus, int row, int column) {
            JLabel lbl = new JLabel(value == null ? "" : value.toString());
            if (column == 0 && row >= 0) {
                lbl.setHorizontalAlignment(SwingConstants.LEFT);
                lbl.setOpaque(true);
                lbl.setBackground(Color.green);
            }
            if (column >= 3 && column <= 4 && row >= 0) {
                lbl.setHorizontalAlignment(SwingConstants.LEFT);
                lbl.setOpaque(true);
                lbl.setBackground(Color.yellow);
            }
            if (column == 5 && value.toString().equals("Inatctif") && row >= 0) {
                lbl.setHorizontalAlignment(SwingConstants.LEFT);
                lbl.setOpaque(true);
                lbl.setForeground(Color.red);
            }

            return lbl;
        }
    };
    JTextField code = new JTextField();

    public Utilisateurs() {
        initComponents();
        setSize(760, 556);
        this.setTitle("Scolaris: " + Variables.Empresa + " : Registre des Utilisateurs");
        //this.setLocationRelativeTo(null);
        //th.Icone_JFrame(this);
        nouveau3.doClick();
//        Mn.MATRICULES_FORMAT(matricule);
//        Mn.USER_FORMAT(nomutil);
        tbluser.setModel(mds);
        tblemployer.setModel(md);
        RemplirUser();
        TableColumnModel cm = tbluser.getColumnModel();
        TableColumnModel cmy = tblemployer.getColumnModel();
        cm.getColumn(0).setPreferredWidth(30);
        cm.getColumn(1).setPreferredWidth(70);
        cm.getColumn(2).setPreferredWidth(80);
        cm.getColumn(3).setPreferredWidth(125);
        cm.getColumn(4).setPreferredWidth(10);
        cm.getColumn(5).setPreferredWidth(5);
        cmy.getColumn(0).setPreferredWidth(90);
        cmy.getColumn(1).setPreferredWidth(10);
        tbluser.getColumnModel().getColumn(0).setCellRenderer(render);
        tbluser.getColumnModel().getColumn(3).setCellRenderer(render);
        tbluser.getColumnModel().getColumn(4).setCellRenderer(render);
        tbluser.getColumnModel().getColumn(5).setCellRenderer(render);
        securite.setEnabled(false);
        confirmation.setEnabled(false);
        
        effacer.setEnabled(false);
    }

    private void RemplirEmployer() {
        String query = "select et.type_employer,et.estatuses from employer_vs_type et left join employer e on e.code_employer=et.code_employer "
                + " where e.matricule='" + matricule.getText() + "'";
        Mn.FULLTABLE(query, tblemployer, md);
    }

    private void Nettoyer() {
        matricule.setText(null);
        Mn.EMPTYTABLE(tblemployer);
        nom.setText("Nom d'employer");
        nomutil.setText(null);
        securite.setText(null);
        confirmation.setText(null);
        acces.setSelectedItem(null);
        etat.setSelected(false);
        failed.setVisible(false);
        failed1.setVisible(false);
        okk.setVisible(false);
    }

    private void RemplirUser() {
        String query = "select u.matricule,t.nom,t.prenom,u.nom_utilisateur,u.niveau_acces,u.estatuses from utilisateur u left join employer e on "
                + "e.matricule=u.matricule left join tercero t on t.code_tercero=e.code_tercero ";
        Mn.FULLTABLE(query, tbluser, mds);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        recherche = new org.edisoncor.gui.textField.TextFieldRectBackground();
        jLabel1 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        jSeparator2 = new javax.swing.JSeparator();
        ok = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tblemployer = new javax.swing.JTable(){
            public boolean isCellEditable(int rowIndex, int colIndex){
                return false;
            }
        };
        jPanel2 = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        matricule = new org.edisoncor.gui.textField.TextFieldRectImage();
        nom = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        nomutil = new javax.swing.JFormattedTextField();
        securite = new org.edisoncor.gui.passwordField.PasswordFieldRectBackground();
        confirmation = new org.edisoncor.gui.passwordField.PasswordFieldRectBackground();
        acces = new javax.swing.JComboBox();
        etat = new javax.swing.JCheckBox();
        jPanel5 = new javax.swing.JPanel();
        failed1 = new javax.swing.JLabel();
        failed = new javax.swing.JLabel();
        okk = new javax.swing.JLabel();
        jPanel6 = new javax.swing.JPanel();
        jPanel7 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tbluser = new javax.swing.JTable(){
            public boolean isCellEditable(int rowIndex, int colIndex){
                return false;
            }
        };
        jMenuBar4 = new javax.swing.JMenuBar();
        jMenu4 = new javax.swing.JMenu();
        enregistrer = new javax.swing.JMenuItem();
        nouveau3 = new javax.swing.JMenuItem();
        effacer = new javax.swing.JMenuItem();
        jMenu5 = new javax.swing.JMenu();
        modifier = new javax.swing.JMenuItem();
        jMenuItem1 = new javax.swing.JMenuItem();

        setClosable(true);
        setIconifiable(true);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        recherche.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        recherche.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                rechercheKeyReleased(evt);
            }
        });
        getContentPane().add(recherche, new org.netbeans.lib.awtextra.AbsoluteConstraints(328, 289, 410, 32));

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel1.setText("Recherche");
        getContentPane().add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(238, 295, -1, -1));
        getContentPane().add(jSeparator1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 281, 750, -1));
        getContentPane().add(jSeparator2, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 83, 750, -1));

        ok.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        getContentPane().add(ok, new org.netbeans.lib.awtextra.AbsoluteConstraints(738, 196, -1, 33));

        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel1.setLayout(new javax.swing.BoxLayout(jPanel1, javax.swing.BoxLayout.LINE_AXIS));

        tblemployer.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane2.setViewportView(tblemployer);

        jPanel1.add(jScrollPane2);

        getContentPane().add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(470, 10, 270, 70));

        jPanel2.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel6.setText("Matricule");
        jPanel2.add(jLabel6);

        matricule.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                matriculeKeyReleased(evt);
            }
        });
        jPanel2.add(matricule);

        nom.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        nom.setForeground(new java.awt.Color(0, 0, 204));
        nom.setText("Nom d'employer");
        jPanel2.add(nom);

        getContentPane().add(jPanel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 10, 470, 50));

        jPanel3.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel3.setLayout(new java.awt.GridBagLayout());

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel2.setText("Nom Utilisateur : ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.ipadx = 114;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(14, 2, 0, 2);
        jPanel3.add(jLabel2, gridBagConstraints);

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel3.setText("Code Sécurité :");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.ipadx = 131;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(27, 2, 0, 2);
        jPanel3.add(jLabel3, gridBagConstraints);

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel5.setText("Confirmee Sécurité : ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.ipadx = 91;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(34, 2, 0, 2);
        jPanel3.add(jLabel5, gridBagConstraints);

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel4.setText("Niveau Acces : ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.ipadx = 132;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(33, 2, 14, 2);
        jPanel3.add(jLabel4, gridBagConstraints);

        getContentPane().add(jPanel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 90, 240, 190));

        jPanel4.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel4.setLayout(new java.awt.GridBagLayout());

        nomutil.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        nomutil.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                nomutilFocusLost(evt);
            }
        });
        nomutil.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                nomutilKeyTyped(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.ipadx = 291;
        gridBagConstraints.ipady = 12;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 0, 3);
        jPanel4.add(nomutil, gridBagConstraints);

        securite.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.ipadx = 288;
        gridBagConstraints.ipady = 16;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(12, 2, 0, 3);
        jPanel4.add(securite, gridBagConstraints);

        confirmation.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        confirmation.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                confirmationFocusLost(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = 3;
        gridBagConstraints.ipadx = 288;
        gridBagConstraints.ipady = 16;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(18, 2, 0, 3);
        jPanel4.add(confirmation, gridBagConstraints);

        acces.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        acces.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Haut", "Moyen", "Bas" }));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.gridheight = 2;
        gridBagConstraints.ipadx = 142;
        gridBagConstraints.ipady = 9;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(15, 2, 0, 0);
        jPanel4.add(acces, gridBagConstraints);

        etat.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        etat.setText("Status");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(20, 2, 0, 0);
        jPanel4.add(etat, gridBagConstraints);

        getContentPane().add(jPanel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 90, 310, 190));

        jPanel5.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel5.setLayout(new java.awt.GridBagLayout());

        failed1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        failed1.setForeground(new java.awt.Color(255, 0, 0));
        failed1.setText("Ce Nom est déja utilisé ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(30, 9, 0, 0);
        jPanel5.add(failed1, gridBagConstraints);

        failed.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        failed.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/passwordFailed.jpg"))); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.ipadx = 147;
        gridBagConstraints.ipady = 5;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(20, 15, 0, 9);
        jPanel5.add(failed, gridBagConstraints);

        okk.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/passwordOk.jpg"))); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.ipadx = 147;
        gridBagConstraints.ipady = 5;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(18, 15, 31, 9);
        jPanel5.add(okk, gridBagConstraints);

        getContentPane().add(jPanel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(550, 90, 200, 190));

        jPanel6.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        jPanel6.setLayout(new java.awt.GridBagLayout());
        getContentPane().add(jPanel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 280, 750, 50));

        jPanel7.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel7.setLayout(new javax.swing.BoxLayout(jPanel7, javax.swing.BoxLayout.LINE_AXIS));

        tbluser.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tbluser.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                tbluserMousePressed(evt);
            }
        });
        jScrollPane1.setViewportView(tbluser);

        jPanel7.add(jScrollPane1);

        getContentPane().add(jPanel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 330, 750, 160));

        jMenu4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/fichier.jpg"))); // NOI18N
        jMenu4.setText("Action");

        enregistrer.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_E, java.awt.event.InputEvent.CTRL_MASK));
        enregistrer.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/icons8-save_close.png"))); // NOI18N
        enregistrer.setText("Enrégistrer");
        enregistrer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                enregistrerActionPerformed(evt);
            }
        });
        jMenu4.add(enregistrer);

        nouveau3.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_N, java.awt.event.InputEvent.CTRL_MASK));
        nouveau3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/icons8-add_list.png"))); // NOI18N
        nouveau3.setText("Nouveau");
        nouveau3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nouveau3ActionPerformed(evt);
            }
        });
        jMenu4.add(nouveau3);

        effacer.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_D, java.awt.event.InputEvent.CTRL_MASK));
        effacer.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/icons8-erase.png"))); // NOI18N
        effacer.setText("Effacer");
        effacer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                effacerActionPerformed(evt);
            }
        });
        jMenu4.add(effacer);

        jMenuBar4.add(jMenu4);

        jMenu5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/editer.jpg"))); // NOI18N
        jMenu5.setText("Edition");

        modifier.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_M, java.awt.event.InputEvent.CTRL_MASK));
        modifier.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/icons8-refresh.png"))); // NOI18N
        modifier.setText("Modifier");
        modifier.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                modifierActionPerformed(evt);
            }
        });
        jMenu5.add(modifier);

        jMenuItem1.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F1, 0));
        jMenuItem1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/icons8-help.png"))); // NOI18N
        jMenuItem1.setText("Aide");
        jMenu5.add(jMenuItem1);

        jMenuBar4.add(jMenu5);

        setJMenuBar(jMenuBar4);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void nouveau3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nouveau3ActionPerformed
        JTextField[] txt = {code, matricule};
        Mn.NEW_BUTTON(txt, "utilisateur", "code_utilisateur");
        //System.out.println(code.getText());
        Nettoyer();
    }//GEN-LAST:event_nouveau3ActionPerformed

    private void enregistrerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_enregistrerActionPerformed
        String aa = code.getText();
        String bb = matricule.getText();
        String cc = nomutil.getText().trim();
        String dd = securite.getText();
        String ee = acces.getSelectedItem().toString();
        String ff = "";
        if (etat.isSelected()) {
            ff = "Actif";
        } else {
            ff = "Inactif";
        }
        String lalal = "call Insert_utilisateur(" + aa + ",'" + bb + "','" + cc + "',AES_ENCRYPT('" + dd + "','llave'),'" + ee + "','" + ff + "')";
        //System.out.println(lalal);
        Mn.SAVE_BUTTON(lalal);
        RemplirUser();
        nouveau3.doClick();
    }//GEN-LAST:event_enregistrerActionPerformed

    private void effacerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_effacerActionPerformed
        Mn.DELETE_BUTTON(code, "utilisateur", "code_utilisateur");
        RemplirUser();
        nouveau3.doClick();
    }//GEN-LAST:event_effacerActionPerformed

    private void modifierActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_modifierActionPerformed
        String aa = code.getText();
        String bb = matricule.getText();
        String cc = nomutil.getText();
        String dd = securite.getText();
        String ee = acces.getSelectedItem().toString();
        String ff = "";
        if (etat.isSelected()) {
            ff = "Actif";
        } else {
            ff = "Inactif";
        }
        String lalal = "call Insert_utilisateur(" + aa + ",'" + bb + "','" + cc + "','" + dd + "','" + ee + "','" + ff + "')";
        Mn.MODIFIER(lalal);
        RemplirUser();
        nouveau3.doClick();
    }//GEN-LAST:event_modifierActionPerformed

    private void confirmationFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_confirmationFocusLost
        if (securite.getText().equals(confirmation.getText())) {
            okk.setVisible(true);
            failed.setVisible(false);
            acces.setEnabled(true);
        } else {
            failed.setVisible(true);
            okk.setVisible(false);
            confirmation.setText(null);
            acces.setEnabled(false);
        }
    }//GEN-LAST:event_confirmationFocusLost

    private void nomutilFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_nomutilFocusLost
        nomutil.setText(nomutil.getText() + "@scolaris.edu");
        String query = "select nom_utilisateur from utilisateur where nom_utilisateur = '" + nomutil.getText() + "' ";
        ResultSet rs = Mn.SEARCHDATA(query);
        try {
            if (rs.next()) {
                failed1.setVisible(true);
                nomutil.requestFocus();
            } else {
                failed1.setVisible(false);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Professeur.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_nomutilFocusLost

    private void rechercheKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_rechercheKeyReleased
        recherche.setText(recherche.getText().toUpperCase());
        String query = "select u.matricule,t.nom,t.prenom,u.nom_utilisateur,u.niveau_acces,u.estatuses from utilisateur u left join employer e on "
                + "e.matricule=u.matricule left join tercero t on t.code_tercero=e.code_tercero where u.matricule like '%" + recherche.getText() + "%' or "
                + " t.nom like '%" + recherche.getText() + "%' or prenom like '%" + recherche.getText() + "%' or  u.nom_utilisateur like '%" + recherche.getText() + "%' or"
                + " u.niveau_acces like '%" + recherche.getText() + "%' or  u.estatuses like '%" + recherche.getText() + "%'";
        Mn.FULLTABLE(query, tbluser, mds);
    }//GEN-LAST:event_rechercheKeyReleased

    private void nomutilKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_nomutilKeyTyped
        if (nomutil.getText().trim().equals("@scolaris.edu") || nomutil.getText().trim().equals(null)) {
            securite.setEnabled(false);
            confirmation.setEnabled(false);
        } else {
            securite.setEnabled(true);
            confirmation.setEnabled(true);
        }
    }//GEN-LAST:event_nomutilKeyTyped

    private void tbluserMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbluserMousePressed
        JTable table = (JTable) evt.getSource();
        Point pt = evt.getPoint();
        int row = table.rowAtPoint(pt);
        if (evt.getClickCount() == 2) {
            if (code.getText().isEmpty()) {
                Nettoyer();
            } else {
                String aa = tbluser.getValueAt(tbluser.getSelectedRow(), 0).toString();
                String query = "SELECT * FROM utilisateur where matricule='" + aa + "'";
                ResultSet rs = Mn.SEARCHDATA(query);
                try {
                    if (rs.first()) {
                        code.setText(rs.getString("code_utilisateur"));
                        matricule.setText(rs.getString("matricule"));
                        nomutil.setText(rs.getString("nom_utilisateur"));
                        securite.setText(rs.getString("securite"));
                        acces.setSelectedItem(rs.getString("niveau_acces"));
                        String[] champ = {"estatuses", "code_utilisateur"};
                        etat.setSelected(Mn.CONSULT_CHECKED("utilisateur", code, champ));
                        RemplirEmployer();
                        nomutil.setEnabled(false);
                        securite.setEnabled(true);
                        confirmation.setEnabled(true);
                    }
                } catch (SQLException ex) {
                    Logger.getLogger(Utilisateurs.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }//GEN-LAST:event_tbluserMousePressed

    private void matriculeKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_matriculeKeyReleased
        matricule.setText(matricule.getText().toUpperCase());
        String query = "select t.nom,t.prenom from tercero t left join employer e "
                + "on e.code_tercero=t.code_tercero where e.matricule = '" + matricule.getText() + "' ";
        ResultSet rs = Mn.SEARCHDATA(query);
        try {
            while (rs.next()) {
                nom.setText(rs.getString("nom") + " " + rs.getString("prenom"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(Professeur.class.getName()).log(Level.SEVERE, null, ex);
        }
        RemplirEmployer();
    }//GEN-LAST:event_matriculeKeyReleased

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Utilisateurs.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Utilisateurs.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Utilisateurs.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Utilisateurs.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Utilisateurs().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox acces;
    private org.edisoncor.gui.passwordField.PasswordFieldRectBackground confirmation;
    private javax.swing.JMenuItem effacer;
    private javax.swing.JMenuItem enregistrer;
    private javax.swing.JCheckBox etat;
    private javax.swing.JLabel failed;
    private javax.swing.JLabel failed1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JMenu jMenu4;
    private javax.swing.JMenu jMenu5;
    private javax.swing.JMenuBar jMenuBar4;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private org.edisoncor.gui.textField.TextFieldRectImage matricule;
    private javax.swing.JMenuItem modifier;
    private javax.swing.JLabel nom;
    private javax.swing.JFormattedTextField nomutil;
    private javax.swing.JMenuItem nouveau3;
    private javax.swing.JLabel ok;
    private javax.swing.JLabel okk;
    private org.edisoncor.gui.textField.TextFieldRectBackground recherche;
    private org.edisoncor.gui.passwordField.PasswordFieldRectBackground securite;
    private javax.swing.JTable tblemployer;
    private javax.swing.JTable tbluser;
    // End of variables declaration//GEN-END:variables
}
