package Forms;

import Consults.*;
import Methods.Manipulation;
import Methods.Theme;
import Methods.Variables;
import com.ezware.oxbow.swingbits.table.filter.TableRowFilterSupport;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.util.Arrays;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.JSpinner;
import javax.swing.SpinnerDateModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;

public class Horaire extends javax.swing.JInternalFrame {

    Manipulation Mn = new Manipulation();
    Theme th = new Theme();
    String[][] data = {};
    String[] head = {"Classes", "Matières", "Heures", "Devise"};
    String[] heads = {" Jours", " Classes", " Matières", "N.d'hres", " Devise", "Entrer", "Sortie"};
    DefaultTableModel md = new DefaultTableModel(data, head);
    DefaultTableModel mds = new DefaultTableModel(data, heads);
    int cantheure = 0;
    int equivalheure = 0;
    String materia = "";
    String clase = "";
    int conteur = 0;
    TableModelListener listeners;

    public Horaire() {

        initComponents();
        this.setTitle("Scolaris: " + Variables.Empresa + " : Horaire de distribution d'heure");
        nouveau3.doClick();
        tblmatiere.setModel(md);
        tblhoraire.setModel(mds);
        mesajel.setVisible(false);
        jours.setSelectedItem(null);
        Nettoyer();
        TableColumnModel cmyy = tblmatiere.getColumnModel();
        TableColumnModel cm = tblhoraire.getColumnModel();
        cm.getColumn(0).setPreferredWidth(30);
        cm.getColumn(1).setPreferredWidth(30);
        cm.getColumn(2).setPreferredWidth(70);
        cm.getColumn(3).setPreferredWidth(5);
        cm.getColumn(4).setPreferredWidth(10);
        cm.getColumn(5).setPreferredWidth(50);
        cm.getColumn(6).setPreferredWidth(3);
        cmyy.getColumn(0).setPreferredWidth(70);
        cmyy.getColumn(1).setPreferredWidth(90);
        cmyy.getColumn(2).setPreferredWidth(5);
        cmyy.getColumn(3).setPreferredWidth(10);

        TableRowFilterSupport.forTable(tblhoraire).searchable(true).apply();

        Calendar cal = Calendar.getInstance();
        cal.setTime(Mn.HORA("08:00:00"));
        jSpinner1.setModel(new SpinnerDateModel(cal.getTime(), null, null, Calendar.HOUR));
        jSpinner2.setModel(new SpinnerDateModel(cal.getTime(), null, null, Calendar.HOUR));
        JSpinner.DateEditor in = new JSpinner.DateEditor(jSpinner1, "HH:mm:ss ");
        JSpinner.DateEditor out = new JSpinner.DateEditor(jSpinner2, "HH:mm:ss ");
        jSpinner1.setEditor(in);
        jSpinner2.setEditor(out);
        jSpinner1.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                horaAutomated();
            }
        });
        gridAsure();
        tblhoraire.getModel().addTableModelListener(tablemodel());
        tableHorraireChangeListener();
        tableMatiereChangeListener();
        
        effacer.setEnabled(false);
    }

    private void tableHorraireChangeListener() {
        tblhoraire.getSelectionModel().addListSelectionListener(new ListSelectionListener() {

            @Override
            public void valueChanged(ListSelectionEvent e) {
                if(tblhoraire.getRowCount() > 0){
                    try {
                jours.setSelectedItem(tblhoraire.getValueAt(tblhoraire.getSelectedRow(), 0).toString());
                heure.setText(tblhoraire.getValueAt(tblhoraire.getSelectedRow(), 3).toString());
                jSpinner1.setValue(tblhoraire.getValueAt(tblhoraire.getSelectedRow(), 5));
                    } catch (IndexOutOfBoundsException eb) {
                    }
                
                }
            }
        });
    }

    private void tableMatiereChangeListener() {
        tblmatiere.getSelectionModel().addListSelectionListener(new ListSelectionListener() {

            @Override
            public void valueChanged(ListSelectionEvent e) {
                if (tblmatiere.getSelectedRow() >= 0) {
                    gridAsure();

                    int reste = 0;
                    conteur = 0;
                    cantheure = Integer.parseInt(tblmatiere.getValueAt(tblmatiere.getSelectedRow(), 2).toString());
                    System.out.println("mnb " + cantheure);
                    clase = tblmatiere.getValueAt(tblmatiere.getSelectedRow(), 0).toString();
                    materia = tblmatiere.getValueAt(tblmatiere.getSelectedRow(), 1).toString();
                    equivalheure = 0;
                    mat.setText(tblmatiere.getValueAt(tblmatiere.getSelectedRow(), 1).toString());
                    semaine.setText(tblmatiere.getValueAt(tblmatiere.getSelectedRow(), 2).toString());
                    clas.setText(tblmatiere.getValueAt(tblmatiere.getSelectedRow(), 0).toString());
                    if (tblhoraire.getRowCount() == 0) {
                        restant.setText(cantheure + "");
                    } else {
                        for (int i = 0; i < tblhoraire.getRowCount(); i++) {
                            if (tblhoraire.getValueAt(i, 1).equals(clase) && tblhoraire.getValueAt(i, 2).equals(materia)) {
                                int aux = Integer.parseInt(tblhoraire.getValueAt(i, 3).toString());
                                conteur = conteur + aux;
                                reste = cantheure - conteur;
                            }
                        }
                        restant.setText(reste + "");
                    }
                    System.out.println("Quntite heure par semaine: " + cantheure);
                }
            }
        });
    }

    private void saveTemp() {
        String code = "HorProf#"+matricule.getText().trim();
        String jou = jours.getSelectedItem().toString();
        String cls = clas.getText().trim();
        String mati = mat.getText().trim();
        int nbr_h = Integer.parseInt(heure.getText().trim());
        String devise = "Heure(s)";
        String hr_in = Mn.HOURFormat(jSpinner1.getValue());
        String hr_out = Mn.HOURFormat(jSpinner2.getValue());

        String sql = "call Insert_horaire_temp('" + code + "','" + jou + "','" + cls + "','" + mati + "','" + nbr_h + "','" + devise + "','" + hr_in + "','" + hr_out + "')";
        System.out.println(sql);
        Mn.MANIPULEDB(sql);

    }

    private TableModelListener tablemodel() {
        listeners = new TableModelListener() {

            @Override
            public void tableChanged(TableModelEvent e) {
                jLabel11.setText(String.valueOf(tblhoraire.getRowCount()));
            }
        };
        return listeners;
    }

    private boolean isGridEqualValues(Object[] array) {
        if (tblhoraire.getRowCount() > 0) {
            int h = array.length;
            Object[] array2 = new Object[h];

            for (int i = 0; i < tblhoraire.getColumnCount(); i++) {
                array2[i] = tblhoraire.getValueAt(tblhoraire.getRowCount() - 1, i).toString();
                System.out.println(i + " " + array2[i]);
            }
            System.out.println("ccc " + Arrays.toString(array2));
        }
        return true;
    }

    private void gridAsure() {
        if (tblmatiere.getSelectedRow() > -1) {
            jours.setEnabled(true);
        } else {
            jours.setEnabled(false);
        }
    }

    private String searchRedundant() {
        String cod = "HorProf#"+matricule.getText().trim();
        String crs = "";
        String sql = "select matiere,classe,jour,heure_in,heure_out from horaire_temp where jour = '" + jours.getSelectedItem().toString() + "' and code_horraire = '"+cod+"' ";
        System.out.println("bg " + sql);
        ResultSet rs = Mn.Rezulta(sql);

        try {
            while (rs.next()) {
                String mat = rs.getString("matiere");
                String cls = rs.getString("classe");
                String jr = rs.getString("jour");
                Time hr_in = rs.getTime("heure_in");
                Time hr_out = rs.getTime("heure_out");

                Calendar cal = Calendar.getInstance();
                cal.setTime(hr_in);

                Calendar cal2 = Calendar.getInstance();
                cal2.setTime(hr_out);

                Calendar calAp = Calendar.getInstance();
                calAp.setTime(Mn.HORA(Mn.HOURFormat(jSpinner1)));

                Calendar calAv = Calendar.getInstance();
                calAv.setTime(Mn.HORA(Mn.HOURFormat(jSpinner2)));

                calAp.add(Calendar.MINUTE, 1);
                System.out.println("calap " + calAp.getTime());
                calAv.add(Calendar.MINUTE, -1);
                System.out.println("calav " + calAv.getTime());

                System.out.println("dtd " + Mn.HOURFormat(cal.getTime()));

                String hr_in2 = Mn.HOURFormat(cal.getTime());
                String hr_out2 = Mn.HOURFormat(cal2.getTime());

                String sqld = "SELECT matiere,classe,heure_in,heure_out FROM horaire_temp where code_horraire = '"+cod+"' and jour = '" + jr + "' and ((heure_in  between '" + Mn.HOURFormat(calAp.getTime()) + "'  and '" + Mn.HOURFormat(calAv.getTime()) + "') or (heure_out  between '" + Mn.HOURFormat(calAp.getTime()) + "'  and '" + Mn.HOURFormat(calAv.getTime()) + "') "
                        + " or ( '" + Mn.HOURFormat(calAp.getTime()) + "'  between heure_in  and heure_out) or ( '" + Mn.HOURFormat(calAv.getTime()) + "'  between heure_in  and heure_out))";
                System.out.println("sqld " + sqld);
                ResultSet rsd = Mn.Rezulta(sqld);
                while (rsd.next()) {
                    mat = rsd.getString("matiere");
                    hr_in = rsd.getTime("heure_in");
                    hr_out = rsd.getTime("heure_out");

                    Calendar cal3 = Calendar.getInstance();
                    cal3.setTime(Mn.HORA(Mn.HOURFormat(jSpinner2)));

              //JOptionPane.showMessageDialog(null, "hr in "+hr_in+" cal "+Mn.HOURFormat(cal3.getTime()));
                    //if(hr_in == cal3.getTime()){
                    crs += mat + " " + cls + " " + jr + " " + hr_in + "-" + hr_out + "\n";
                    //}
                }

            }
        } catch (SQLException ex) {
            Logger.getLogger(Horaire.class.getName()).log(Level.SEVERE, null, ex);
        }

        return crs;
    }

    private int horaAutomated() {

        int d = 0;
        if (!heure.getText().isEmpty()) {
            Calendar cal = Calendar.getInstance();
            cal.setTime(Mn.HORA(Mn.HOURFormat(jSpinner1.getValue())));
            int hh = Integer.parseInt(heure.getText().trim());
            System.out.println("cal " + Mn.HOURFormat(cal.getTime()) + " hh " + hh);
            cal.add(Calendar.HOUR, hh);
            System.out.println("dtd " + Mn.HOURFormat(cal.getTime()));
            jSpinner2.setValue(Mn.HORA(Mn.HOURFormat(cal.getTime())));
        }
        return d;
    }

    private void RemplirCours() {
        String aa = matricule.getText();
        String query = "select classe,matieres,heure,deviz from professeur where matricule='" + aa + "' ";
        Mn.FULLTABLE(query, tblmatiere, md);
    }

    private void RemplirHoraire() {
        try {
            String aa = "HorProf#"+matricule.getText();
            String query = "select jour,classe,matiere,nbre_heure,devise,heure_in,heure_out  from horaire where code_horraire = '"+aa+"' ";
            ResultSet rs = Mn.Rezulta(query);
            while (rs.next()) {
                String jr = rs.getString("jour");
                String cl = rs.getString("classe");
                String mt = rs.getString("matiere");
                int nbr = rs.getInt("nbre_heure");
                String dv = rs.getString("devise");
                Time hr_in = rs.getTime("heure_in");
                Time hr_out = rs.getTime("heure_out");

                String sql = " call Insert_horaire_temp ('" + aa + "','" + jr + "','" + cl + "','" + mt + "'," + nbr + ",'" + dv + "','" + hr_in + "','" + hr_out + "') ";
                Mn.MANIPULEDB(sql);
            }

            String query_ = "select jour,classe,matiere,nbre_heure,devise,heure_in,heure_out  from horaire_temp where code_horraire ='" + aa + "' ";
            Mn.FULLTABLE(query_, tblhoraire, mds);
        } catch (SQLException ex) {
            Logger.getLogger(Horaire.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void Nettoyer() {
        Mn.EMPTYTABLE(tblhoraire);
        Mn.EMPTYTABLE(tblmatiere);
        matricule.setValue(null);
        nom.setText("Nom d'employer");
        jours.setSelectedItem("Selectionnez");
        heure.setText(null);
        mat.setText(null);
        semaine.setText(null);
        restant.setText(null);
        clas.setText(null);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel2 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        matricule = new javax.swing.JFormattedTextField();
        jButton1 = new javax.swing.JButton();
        nom = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblmatiere = new javax.swing.JTable(){
            public boolean isCellEditable(int rowIndex, int colIndex){
                return false;
            }
        };
        jPanel5 = new javax.swing.JPanel();
        mesajel = new javax.swing.JLabel();
        jPanel6 = new javax.swing.JPanel();
        clas = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        mat = new javax.swing.JLabel();
        semaine = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        restant = new javax.swing.JLabel();
        jPanel7 = new javax.swing.JPanel();
        rem = new javax.swing.JButton();
        add = new javax.swing.JButton();
        jSpinner2 = new javax.swing.JSpinner();
        jLabel5 = new javax.swing.JLabel();
        jSpinner1 = new javax.swing.JSpinner();
        jLabel2 = new javax.swing.JLabel();
        heure = new org.edisoncor.gui.textField.TextFieldRectBackground();
        jLabel1 = new javax.swing.JLabel();
        jours = new javax.swing.JComboBox();
        jLabel4 = new javax.swing.JLabel();
        jPanel8 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tblhoraire = new javax.swing.JTable(){
            public boolean isCellEditable(int rowIndex, int colIndex){
                return false;
            }
        };
        jPanel1 = new javax.swing.JPanel();
        jLabel7 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jMenuBar4 = new javax.swing.JMenuBar();
        jMenu4 = new javax.swing.JMenu();
        enregistrer = new javax.swing.JMenuItem();
        nouveau3 = new javax.swing.JMenuItem();
        effacer = new javax.swing.JMenuItem();
        jMenu5 = new javax.swing.JMenu();
        modifier = new javax.swing.JMenuItem();
        jMenuItem1 = new javax.swing.JMenuItem();

        setClosable(true);
        setIconifiable(true);

        jPanel2.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jPanel3.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel3.setLayout(new org.jdesktop.swingx.HorizontalLayout());

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel3.setText("Matricule : ");
        jPanel3.add(jLabel3);

        matricule.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        matricule.setPreferredSize(new java.awt.Dimension(177, 34));
        matricule.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                matriculeFocusLost(evt);
            }
        });
        matricule.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                matriculeKeyTyped(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                matriculeKeyReleased(evt);
            }
        });
        jPanel3.add(matricule);

        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/consulta.png"))); // NOI18N
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jPanel3.add(jButton1);

        nom.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        nom.setForeground(new java.awt.Color(0, 0, 255));
        nom.setText("Nom du professeur");
        jPanel3.add(nom);

        jPanel4.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel4.setPreferredSize(new java.awt.Dimension(100, 100));
        jPanel4.setLayout(new javax.swing.BoxLayout(jPanel4, javax.swing.BoxLayout.LINE_AXIS));

        tblmatiere.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tblmatiere.setPreferredSize(new java.awt.Dimension(486, 80));
        tblmatiere.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblmatiereMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tblmatiere);

        jPanel4.add(jScrollPane1);

        jPanel5.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel5.setLayout(new org.jdesktop.swingx.HorizontalLayout());

        mesajel.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        mesajel.setForeground(new java.awt.Color(204, 0, 0));
        mesajel.setText("Cet employer n'est pas un Professeur, s'il-vous plait  veuillez choisis un autre.");
        jPanel5.add(mesajel);

        jPanel6.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        clas.setFont(new java.awt.Font("Lucida Grande", 1, 13)); // NOI18N
        clas.setForeground(new java.awt.Color(0, 0, 255));
        clas.setText("jLabel7");

        jLabel9.setText("Classe: ");

        jLabel6.setText("Matieres: ");

        mat.setFont(new java.awt.Font("Lucida Grande", 1, 13)); // NOI18N
        mat.setForeground(new java.awt.Color(0, 0, 255));
        mat.setText("jLabel7");

        semaine.setFont(new java.awt.Font("Lucida Grande", 1, 13)); // NOI18N
        semaine.setForeground(new java.awt.Color(51, 153, 0));
        semaine.setText("jLabel7");

        jLabel8.setText("Qtites d'hres/semaine:");

        jLabel10.setText("Qtites d'hres Restante: ");

        restant.setFont(new java.awt.Font("Lucida Grande", 1, 13)); // NOI18N
        restant.setForeground(new java.awt.Color(204, 0, 0));
        restant.setText("jLabel7");

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel8)
                    .addComponent(jLabel10))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(restant, javax.swing.GroupLayout.DEFAULT_SIZE, 186, Short.MAX_VALUE)
                    .addComponent(semaine, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addComponent(jLabel9)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(clas, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(mat, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap(57, Short.MAX_VALUE)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(mat, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9)
                    .addComponent(clas))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(semaine))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(restant)
                    .addComponent(jLabel10))
                .addContainerGap())
        );

        jPanel7.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        rem.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/Subir.png"))); // NOI18N
        rem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                remActionPerformed(evt);
            }
        });

        add.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/Bajar.png"))); // NOI18N
        add.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addActionPerformed(evt);
            }
        });

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel5.setText("A");

        jSpinner1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jSpinner1KeyReleased(evt);
            }
        });

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel2.setText("De");

        heure.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        heure.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        heure.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                heureFocusLost(evt);
            }
        });
        heure.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                heureActionPerformed(evt);
            }
        });
        heure.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                heureKeyTyped(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                heureKeyReleased(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel1.setText("Heure");

        jours.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jours.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Selectionnez", "Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi" }));
        jours.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                joursItemStateChanged(evt);
            }
        });

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel4.setText("Jours");

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jours, javax.swing.GroupLayout.PREFERRED_SIZE, 142, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(heure, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSpinner1, javax.swing.GroupLayout.PREFERRED_SIZE, 123, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSpinner2, javax.swing.GroupLayout.PREFERRED_SIZE, 122, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(add, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(rem, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(add, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(heure, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel1)
                        .addComponent(jours, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel4))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jSpinner1, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jSpinner2, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(rem, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel8.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel8.setLayout(new javax.swing.BoxLayout(jPanel8, javax.swing.BoxLayout.LINE_AXIS));

        tblhoraire.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tblhoraire.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                tblhoraireMousePressed(evt);
            }
        });
        jScrollPane2.setViewportView(tblhoraire);

        jPanel8.add(jScrollPane2);

        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel1.setLayout(new org.jdesktop.swingx.HorizontalLayout());

        jLabel7.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        jLabel7.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel7.setText("Total of Rows : ");
        jPanel1.add(jLabel7);

        jLabel11.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jLabel11.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jPanel1.add(jLabel11);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 177, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(jPanel7, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(jPanel2Layout.createSequentialGroup()
                            .addGap(14, 14, 14)
                            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGroup(jPanel2Layout.createSequentialGroup()
                                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, 561, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, 561, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGap(6, 6, 6)
                                    .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addComponent(jPanel8, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addGap(0, 2, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(6, 6, 6)
                        .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(3, 3, 3)
                .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(12, 12, 12)
                .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        jMenu4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/fichier.jpg"))); // NOI18N
        jMenu4.setText("Action");

        enregistrer.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_E, java.awt.event.InputEvent.CTRL_MASK));
        enregistrer.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/icons8-save_close.png"))); // NOI18N
        enregistrer.setText("Enrégistrer");
        enregistrer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                enregistrerActionPerformed(evt);
            }
        });
        jMenu4.add(enregistrer);

        nouveau3.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_N, java.awt.event.InputEvent.CTRL_MASK));
        nouveau3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/icons8-add_list.png"))); // NOI18N
        nouveau3.setText("Nouveau");
        nouveau3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nouveau3ActionPerformed(evt);
            }
        });
        jMenu4.add(nouveau3);

        effacer.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_D, java.awt.event.InputEvent.CTRL_MASK));
        effacer.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/icons8-erase.png"))); // NOI18N
        effacer.setText("Effacer");
        effacer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                effacerActionPerformed(evt);
            }
        });
        jMenu4.add(effacer);

        jMenuBar4.add(jMenu4);

        jMenu5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/editer.jpg"))); // NOI18N
        jMenu5.setText("Edition");

        modifier.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_M, java.awt.event.InputEvent.CTRL_MASK));
        modifier.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/icons8-refresh.png"))); // NOI18N
        modifier.setText("Modifier");
        modifier.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                modifierActionPerformed(evt);
            }
        });
        jMenu5.add(modifier);

        jMenuItem1.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F1, 0));
        jMenuItem1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/icons8-help.png"))); // NOI18N
        jMenuItem1.setText("Aide");
        jMenu5.add(jMenuItem1);

        jMenuBar4.add(jMenu5);

        setJMenuBar(jMenuBar4);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(24, 24, 24))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void nouveau3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nouveau3ActionPerformed
        Nettoyer();
    }//GEN-LAST:event_nouveau3ActionPerformed

    private void enregistrerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_enregistrerActionPerformed

        String aq = "HorProf#"+matricule.getText();
        String ab = "";
        String lalal = "";

        String sql = "delete from horaire where code_horraire = '" + aq + "' ";
//        Mn.MANIPULEDB(sql);
        for (int i = 0; i < tblhoraire.getRowCount(); i++) {
            String qq = tblhoraire.getValueAt(i, 0).toString();
            String qqq = tblhoraire.getValueAt(i, 1).toString();
            String ddd = tblhoraire.getValueAt(i, 2).toString();
            String eee = tblhoraire.getValueAt(i, 3).toString();
            String fff = tblhoraire.getValueAt(i, 4).toString();
            String ggg = tblhoraire.getValueAt(i, 5).toString();
            String hhh = tblhoraire.getValueAt(i, 6).toString();

            lalal = "call Insert_horaire('" + aq + "','" + qq + "','" + qqq + "','" + ddd + "','" + eee + "','" + fff + "','" + ggg + "','" + hhh + "')";
                    System.out.println(lalal);
            Mn.MANIPULEDB(lalal);
            
            sql = "call professeur_has_horaire_procedure ('"+matricule.getText().trim()+"','"+aq+"','"+qq+"','"+qqq+"','"+ddd+"')";
            System.out.println("sql "+sql);
            Mn.MANIPULEDB(sql);
        }
        nouveau3.doClick();
        String sqls = "truncate table horaire_temp";
        Mn.MANIPULEDB(sqls);
    }//GEN-LAST:event_enregistrerActionPerformed

    private void effacerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_effacerActionPerformed

        String val = Mn.SELLECT_BUTTON(Variables.Seleccionado1, tblhoraire);
        String val1 = tblhoraire.getValueAt(tblhoraire.getSelectedRow(), 2).toString();
        String query = "delete from horaire where matricule='" + matricule.getText() + "' and jour='" + val + "' and matiere='" + val1 + "'";
        System.out.println(query);
        Mn.MANIPULEDB(query);
        RemplirHoraire();
    }//GEN-LAST:event_effacerActionPerformed

    private void modifierActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_modifierActionPerformed
        String aq = matricule.getText();
        String ab = "";
        String lalal = "";
        for (int i = 0; i < tblhoraire.getRowCount(); i++) {
            String qq = tblhoraire.getValueAt(i, 0).toString();
            String qqq = tblhoraire.getValueAt(i, 1).toString();
            String ddd = tblhoraire.getValueAt(i, 2).toString();
            String eee = tblhoraire.getValueAt(i, 3).toString();
            String fff = tblhoraire.getValueAt(i, 4).toString();
            String ggg = tblhoraire.getValueAt(i, 5).toString();
            String hhh = tblhoraire.getValueAt(i, 6).toString();
            String hola = "select matricule from horaire where jour='" + qq + "' and matiere='" + ddd + "'";
            ResultSet rss = Mn.SEARCHDATA(hola);
            try {
                if (rss.first()) {
                    ab = rss.getString("matricule");
                }
            } catch (SQLException ex) {
                Logger.getLogger(Eleves.class.getName()).log(Level.SEVERE, null, ex);
            }
            if (matricule.getText().equals(ab)) {
                lalal = "update horaire set matricule='" + aq + "',classe='" + qqq + "',matiere='" + ddd + "',heure_in='" + eee + "',heure_out='" + hhh + "' "
                        + "where matiere='" + ddd + "' and jour='" + qq + "'";
                Mn.MANIPULEDB(lalal);
                ab = "";
            } else {
                lalal = "call Insert_horaire('" + aq + "','" + qq + "','" + qqq + "','" + ddd + "','" + eee + "','" + fff + "','" + ggg + "','" + hhh + "')";
                Mn.MANIPULEDB(lalal);
            }
        }
        nouveau3.doClick();
    }//GEN-LAST:event_modifierActionPerformed

    private void heureKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_heureKeyReleased
        horaAutomated();
    }//GEN-LAST:event_heureKeyReleased

    private void matriculeFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_matriculeFocusLost

    }//GEN-LAST:event_matriculeFocusLost

    private void matriculeKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_matriculeKeyReleased
        matricule.setText(matricule.getText().toUpperCase());
        String query = "select nom,prenom from show_employes where matricule = '" + matricule.getText() + "' ";
        System.out.println("hmm "+query);
        ResultSet rs = Mn.SEARCHDATA(query);
        try {
            while (rs.next()) {
                nom.setText(rs.getString("nom") + " " + rs.getString("prenom") +"   ");
                String sesa = "select description from type_employer te join employer_vs_type et on(te.idtype_employer = et.idtype_employer)\n " +
" where et.matricule='"+matricule.getText().trim()+"' and description = 'Professeur' group by description";
                System.out.println("SESSA : "+sesa);
                ResultSet rss = Mn.SEARCHDATA(sesa);
                if (rss.next()) {
                    if (rss.getString("description").equals("Professeur")) {
                        mesajel.setVisible(false);
                    } else {
                        nom.setText(rs.getString("nom") + " " + rs.getString("prenom"));
                        mesajel.setVisible(true);
                    }
                } else {
                    nom.setText(rs.getString("nom") + " " + rs.getString("prenom"));
                    mesajel.setVisible(true);
                }

            }
        } catch (SQLException ex) {
            Logger.getLogger(Professeur.class.getName()).log(Level.SEVERE, null, ex);
        }
        RemplirCours();
        RemplirHoraire();
    }//GEN-LAST:event_matriculeKeyReleased

    private void matriculeKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_matriculeKeyTyped
        Mn.ONLYNUMBERS(evt);
        matricule.requestFocus();
    }//GEN-LAST:event_matriculeKeyTyped

    private void addActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addActionPerformed

        if (tblmatiere.getSelectedRow() >= 0) {
            String st = searchRedundant();
            if (st.equals("")) {
                String[] vect = {jours.getSelectedItem().toString(), clase, materia, heure.getText(), "Heures", Mn.HOURFormat(jSpinner1), Mn.HOURFormat(jSpinner2)};

                if (heure.getText().isEmpty()) {
                } else if (tblhoraire.getRowCount() == 0) {
                    equivalheure = equivalheure + Integer.parseInt(heure.getText());
                    if (equivalheure <= cantheure) {
                        Mn.ADD_TABLE(mds, vect);
                        saveTemp();
                        jours.setSelectedItem(null);
                        heure.setText(null);
                    } else {
                        JOptionPane.showMessageDialog(null, "mnj" + "Vous avez depasser la quantité d'heure par semaine pour cette matière");
                        equivalheure = equivalheure - Integer.parseInt(heure.getText());
                        heure.setText(null);
                    }
                } else {
                    equivalheure = equivalheure + Integer.parseInt(heure.getText());
                    System.out.println("aqui " + equivalheure + " cab " + cantheure);
                    if (equivalheure <= cantheure) {
                        Mn.ADD_TABLE(mds, vect);
                        saveTemp();
                        int reste = 0;
                        conteur = 0;
                        cantheure = Integer.parseInt(tblmatiere.getValueAt(tblmatiere.getSelectedRow(), 2).toString());
                        System.out.println("cnt " + cantheure);
                        clase = tblmatiere.getValueAt(tblmatiere.getSelectedRow(), 0).toString();
                        materia = tblmatiere.getValueAt(tblmatiere.getSelectedRow(), 1).toString();
                        if (tblhoraire.getRowCount() == 0) {
                            restant.setText(cantheure + "");
                        } else {
                            for (int i = 0; i < tblhoraire.getRowCount(); i++) {
                                if (tblhoraire.getValueAt(i, 1).equals(clase) && tblhoraire.getValueAt(i, 2).equals(materia)) {
                                    int aux = Integer.parseInt(tblhoraire.getValueAt(i, 3).toString());
                                    conteur = conteur + aux;
                                    reste = cantheure - conteur;
                                }
                            }
                            restant.setText(reste + "");
                        }
                        jours.setSelectedItem(null);
                        heure.setText(null);
                    } else {
                        JOptionPane.showMessageDialog(null, "cc" + "Vous avez depasser la quantité d'heure par semaine pour cette matière");
                        equivalheure = equivalheure - Integer.parseInt(heure.getText());
                        heure.setText(null);
                    }
                }
            } else {
                String gr = "Possibilite de croisement avec ==> \n ";
                JOptionPane.showMessageDialog(this, gr + st);
            }
        } else {
            JOptionPane.showMessageDialog(null, "Desole vous devez selectionner une registre dans la table des matieres");
        }
    }//GEN-LAST:event_addActionPerformed

    private void remActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_remActionPerformed
        Mn.REMOVE_TABLE(tblhoraire, mds);
    }//GEN-LAST:event_remActionPerformed

    private void tblmatiereMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblmatiereMouseClicked
        //JOptionPane.showMessageDialog(null, tblmatiere.getSelectedRow()+" "+tblmatiere.getSelectedRow());


    }//GEN-LAST:event_tblmatiereMouseClicked

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        Variables.ut = "";
        Employe emp = new Employe(null, rootPaneCheckingEnabled);
        emp.setVisible(true);
        matricule.setText(Variables.ut);
        matriculeKeyReleased(null);
    }//GEN-LAST:event_jButton1ActionPerformed

    private void heureKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_heureKeyTyped

    }//GEN-LAST:event_heureKeyTyped

    private void heureFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_heureFocusLost
        int conteur = 0;
        int reste = 0;
        String classs = null;
        String matye = null;
        int lheure = 0;
        try {
            classs = tblmatiere.getValueAt(tblmatiere.getSelectedRow(), 0).toString();
            matye = tblmatiere.getValueAt(tblmatiere.getSelectedRow(), 1).toString();
            lheure = Integer.parseInt(tblmatiere.getValueAt(tblmatiere.getSelectedRow(), 2).toString());
        } catch (ArrayIndexOutOfBoundsException e) {
        }

        if (tblhoraire.getRowCount() == 0) {
            try {
                conteur = conteur + Integer.parseInt(heure.getText());
            } catch (NumberFormatException e) {
            }

        } else {
            for (int i = 0; i < tblhoraire.getRowCount(); i++) {
                if (tblhoraire.getValueAt(i, 1).equals(classs) && tblhoraire.getValueAt(i, 2).equals(matye)) {
                    int aux = Integer.parseInt(tblhoraire.getValueAt(i, 3).toString());
                    conteur = conteur + aux;
                    reste = lheure - conteur;
                }
            }
            try {
                conteur = conteur + Integer.parseInt(heure.getText());
                if (conteur <= lheure) {
                    conteur = conteur + Integer.parseInt(heure.getText());
                } else {
                    JOptionPane.showMessageDialog(null, "Desole, il reste seulement " + reste + " heures par semaine pour ce professeur.");
                }

            } catch (NumberFormatException e) {
            }

        }
    }//GEN-LAST:event_heureFocusLost

    private void tblhoraireMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblhoraireMousePressed


    }//GEN-LAST:event_tblhoraireMousePressed

    private void heureActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_heureActionPerformed
        add.doClick();
    }//GEN-LAST:event_heureActionPerformed

    private void jSpinner1KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jSpinner1KeyReleased
        horaAutomated();
    }//GEN-LAST:event_jSpinner1KeyReleased

    private void joursItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_joursItemStateChanged

        if (jours.getSelectedIndex() > 0) {
            heure.setEnabled(true);
            jSpinner1.setEnabled(true);
            jSpinner2.setEnabled(true);
            add.setEnabled(true);
            rem.setEnabled(true);
        } else {
            heure.setEnabled(false);
            jSpinner1.setEnabled(false);
            jSpinner2.setEnabled(false);
            add.setEnabled(false);
            rem.setEnabled(false);
        }
    }//GEN-LAST:event_joursItemStateChanged

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Horaire.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Horaire.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Horaire.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Horaire.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Horaire().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton add;
    private javax.swing.JLabel clas;
    private javax.swing.JMenuItem effacer;
    private javax.swing.JMenuItem enregistrer;
    private org.edisoncor.gui.textField.TextFieldRectBackground heure;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JMenu jMenu4;
    private javax.swing.JMenu jMenu5;
    private javax.swing.JMenuBar jMenuBar4;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JSpinner jSpinner1;
    private javax.swing.JSpinner jSpinner2;
    private javax.swing.JComboBox jours;
    private javax.swing.JLabel mat;
    private javax.swing.JFormattedTextField matricule;
    private javax.swing.JLabel mesajel;
    private javax.swing.JMenuItem modifier;
    private javax.swing.JLabel nom;
    private javax.swing.JMenuItem nouveau3;
    private javax.swing.JButton rem;
    private javax.swing.JLabel restant;
    private javax.swing.JLabel semaine;
    private javax.swing.JTable tblhoraire;
    private javax.swing.JTable tblmatiere;
    // End of variables declaration//GEN-END:variables
}
