
package Forms;

import Methods.Connections;
import Methods.Manipulation;
import Methods.Theme;
import Methods.Variables;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JTextField;

public class Notification extends javax.swing.JInternalFrame {
Connections db= new Connections().Conectar();
Manipulation Mn= new Manipulation();
Theme th= new Theme();
JTextField route= new JTextField();
File fichier;
    public Notification() {
        initComponents();
        this.setTitle("Scolaris: "+Variables.Empresa+" : Registre de Programme des Classes");
        nouveau.doClick();
    }

     private void Nettoyer(){
        section.setSelectedItem("Selectionnez");
        classe.setSelectedItem("Selectionnez");
        objectif.setText(null);
        message.setText(null);
    }
     
     private void Executer(){
        int aaq=Mn.MAXCODE("code_notification","notification");
       String bbq=objectif.getText();
       String ccq=message.getText();
       String ddq=Mn.DATEHOURNOW();
       String querys="insert into notification(code_notification,objectif,mesaj,date_notification) "
               + "values("+aaq+",'"+bbq+"','"+ccq+"','"+ddq+"')";
         System.out.println(querys);
       Mn.MANIPULEDB(querys);
     //-------------------------------------------------------------------------------- 
       FileInputStream fichierfoto=null;
       String fileName = System.getProperty("user.dir")+"/src/Pictures/unread.png";
       String query="select matricule from show_matricule where section='"+section.getSelectedItem().toString()+"' and classe='"+classe.getSelectedItem().toString()+"'";
       ResultSet rs=Mn.SEARCHDATA(query);
       
    try {
        while(rs.next()){
           int bb=Mn.SEARCHCODE("code_classe","description","section",classe.getSelectedItem().toString(),section.getSelectedItem().toString(),"classe");
           String cc=rs.getString("matricule");
           String dd="Non Lu";
            fichier= new File(fileName);
            try {
                fichierfoto=new FileInputStream(fichier);
            } catch (FileNotFoundException ex) {
                Logger.getLogger(Notification.class.getName()).log(Level.SEVERE, null, ex);
            }
            query="call Insert_detaille_notification(?,?,?,?,?)";
            try {
                PreparedStatement statement= db.getConexion().prepareStatement(query);
                statement.setInt(1,aaq);
                statement.setInt(2,bb);
                statement.setString(3,cc);
                statement.setBlob(4,fichierfoto);
                statement.setString(5,dd);
                statement.executeUpdate();
            } catch (SQLException ex) {
                Logger.getLogger(Notification.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        }
    } catch (SQLException ex) {
        Logger.getLogger(Notification.class.getName()).log(Level.SEVERE, null, ex);
    }

}
     
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        message = new javax.swing.JTextArea();
        jLabel4 = new javax.swing.JLabel();
        section = new javax.swing.JComboBox();
        jLabel5 = new javax.swing.JLabel();
        classe = new javax.swing.JComboBox();
        jSeparator1 = new javax.swing.JSeparator();
        jLabel6 = new javax.swing.JLabel();
        objectif = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        enregistrer = new javax.swing.JMenuItem();
        nouveau = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        modifier = new javax.swing.JMenuItem();
        jMenuItem2 = new javax.swing.JMenuItem();

        setClosable(true);
        setIconifiable(true);

        message.setColumns(20);
        message.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        message.setRows(5);
        message.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        message.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                messageFocusGained(evt);
            }
        });
        jScrollPane1.setViewportView(message);

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel4.setText("Section");

        section.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        section.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Selectionnez", "Petit", "Primaire", "Secondaire" }));
        section.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                sectionItemStateChanged(evt);
            }
        });

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel5.setText("Classe");

        classe.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        classe.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Selectionnez", "Petit", "Primaire", "Secondaire" }));
        classe.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                classeItemStateChanged(evt);
            }
        });

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel6.setText("Objectif: ");

        objectif.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N

        jLabel7.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel7.setText("Message: ");

        jMenu1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/fichier.jpg"))); // NOI18N
        jMenu1.setText("Action");
        jMenu1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N

        enregistrer.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_E, java.awt.event.InputEvent.CTRL_MASK));
        enregistrer.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/guardar.png"))); // NOI18N
        enregistrer.setText("Enrégistrer");
        enregistrer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                enregistrerActionPerformed(evt);
            }
        });
        jMenu1.add(enregistrer);

        nouveau.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_N, java.awt.event.InputEvent.CTRL_MASK));
        nouveau.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/new.png"))); // NOI18N
        nouveau.setText("Nouveau");
        nouveau.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nouveauActionPerformed(evt);
            }
        });
        jMenu1.add(nouveau);

        jMenuBar1.add(jMenu1);

        jMenu2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/editer.jpg"))); // NOI18N
        jMenu2.setText("Edition");
        jMenu2.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N

        modifier.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_M, java.awt.event.InputEvent.CTRL_MASK));
        modifier.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/actualizar.jpg"))); // NOI18N
        modifier.setText("Modifier");
        modifier.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                modifierActionPerformed(evt);
            }
        });
        jMenu2.add(modifier);

        jMenuItem2.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F1, 0));
        jMenuItem2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/Ayuda.jpg"))); // NOI18N
        jMenuItem2.setText("Aide");
        jMenu2.add(jMenuItem2);

        jMenuBar1.add(jMenu2);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSeparator1)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane1)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel6)
                                    .addComponent(jLabel7)
                                    .addComponent(objectif, javax.swing.GroupLayout.PREFERRED_SIZE, 451, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(0, 0, Short.MAX_VALUE)))
                        .addContainerGap())
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(section, 0, 332, Short.MAX_VALUE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel5)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(classe, javax.swing.GroupLayout.PREFERRED_SIZE, 321, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(23, 23, 23))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(section, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(classe, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel6)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(objectif, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel7)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 331, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void sectionItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_sectionItemStateChanged
         if(section.getSelectedItem().equals("Selectionnez")){
            classe.setSelectedItem("Selectionnez");
        }else if(section.getSelectedItem().equals("Petit")){
           Mn.FULLCOMBO(classe, "description", "section", section.getSelectedItem().toString(),"classe");
        }else if(section.getSelectedItem().equals("Primaire")){
            Mn.FULLCOMBO(classe, "description", "section", section.getSelectedItem().toString(),"classe");
        }else if(section.getSelectedItem().equals("Secondaire")){
           Mn.FULLCOMBO(classe, "description", "section", section.getSelectedItem().toString(),"classe");
        }
    }//GEN-LAST:event_sectionItemStateChanged

    private void classeItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_classeItemStateChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_classeItemStateChanged

    private void nouveauActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nouveauActionPerformed
       Nettoyer();
    }//GEN-LAST:event_nouveauActionPerformed

    private void enregistrerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_enregistrerActionPerformed
        Executer();
        nouveau.doClick();
    }//GEN-LAST:event_enregistrerActionPerformed

    private void modifierActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_modifierActionPerformed
//        int aa=Integer.parseInt(code.getText());
//        String bb=classe.getText();
//        String cc=section.getSelectedItem().toString();
//        for(int i=0;i<tblmatiere.getRowCount();i++){
//            String xx=tblmatiere.getValueAt(i,0).toString();
//            String xxx=Mn.SEARCHCODE("code_matiere","description",xx,"matiere");
//            String kk="delete from classe_vs_matiere where code_classe='"+aa+"'and code_matiere='"+xxx+"'";
//            Mn.MANIPULEDB(kk);
//        }
//        for(int i=0;i<tblmatiere.getRowCount();i++){
//            String xx=tblmatiere.getValueAt(i,0).toString();
//            String xxx=Mn.SEARCHCODE("code_matiere","description",xx,"matiere");
//            String mm="call Insert_classematiere("+aa+","+xxx+")";
//            Mn.MANIPULEDB(mm);
//        }
//        String query="call Insert_classe("+aa+",'"+bb+"','"+cc+"')";
//        Mn.MODIFIER(query);
        nouveau.doClick();

    }//GEN-LAST:event_modifierActionPerformed

    private void messageFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_messageFocusGained
      // message.setLineWrap (true);
       //message.setWrapStyleWord(true);
       //message=new JTextArea(10,50);
    }//GEN-LAST:event_messageFocusGained


    // Variables declaration - do not modify//GEN-BEGIN:variables
    public static javax.swing.JComboBox classe;
    private javax.swing.JMenuItem enregistrer;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JTextArea message;
    private javax.swing.JMenuItem modifier;
    private javax.swing.JMenuItem nouveau;
    private javax.swing.JTextField objectif;
    public static javax.swing.JComboBox section;
    // End of variables declaration//GEN-END:variables
}
