/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Forms;

import Start.Login;
import Start.LoginDialog;
import java.awt.Color;
import javax.swing.UIManager;
/**
 *
 * @author josephandyfeidje
 */
public class Themes extends javax.swing.JDialog {

    /**
     * Creates new form Theme
     */
    public Themes(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        setLocationRelativeTo(this);
        classique.setSelected(true);
        setPanelButtonColor();
    }
    
    private void setPanelButtonColor(){
        if(classique.isSelected()){
        panelButton.setBackground(new Color(255,255,255));
        jLabel4.setForeground(new Color(0, 0, 0));
        }
        else if (moderne.isSelected()){
        panelButton.setBackground(new Color(87,148,210));
        jLabel4.setForeground(new Color(255,255,255));
        }
    }

    private void setTheme(){
        if(classique.isSelected()){
            this.dispose();
            new Login().show();
        }
        else if (moderne.isSelected()){
            this.dispose();
            new LoginDialog(null, rootPaneCheckingEnabled).show();
        }
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        panelModerne = new javax.swing.JPanel();
        moderne = new javax.swing.JRadioButton();
        jLabel3 = new javax.swing.JLabel();
        panelClassique = new javax.swing.JPanel();
        classique = new javax.swing.JRadioButton();
        jLabel2 = new javax.swing.JLabel();
        panelButton = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        getContentPane().setLayout(new java.awt.CardLayout());

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));

        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("Veuillez Choisir un thème s'il vous plait");

        panelModerne.setBackground(new java.awt.Color(87, 148, 210));

        buttonGroup1.add(moderne);
        moderne.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        moderne.setForeground(new java.awt.Color(255, 255, 255));
        moderne.setText("Moderne");
        moderne.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        moderne.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                moderneActionPerformed(evt);
            }
        });

        jLabel3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/ButtonFoto/icons8-overview_pages_3_filled.png"))); // NOI18N

        javax.swing.GroupLayout panelModerneLayout = new javax.swing.GroupLayout(panelModerne);
        panelModerne.setLayout(panelModerneLayout);
        panelModerneLayout.setHorizontalGroup(
            panelModerneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelModerneLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(moderne, javax.swing.GroupLayout.PREFERRED_SIZE, 175, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        panelModerneLayout.setVerticalGroup(
            panelModerneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelModerneLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(panelModerneLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(moderne, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        buttonGroup1.add(classique);
        classique.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        classique.setText("Classique");
        classique.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        classique.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                classiqueActionPerformed(evt);
            }
        });

        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/ButtonFoto/icons8-template.png"))); // NOI18N

        javax.swing.GroupLayout panelClassiqueLayout = new javax.swing.GroupLayout(panelClassique);
        panelClassique.setLayout(panelClassiqueLayout);
        panelClassiqueLayout.setHorizontalGroup(
            panelClassiqueLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelClassiqueLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(classique, javax.swing.GroupLayout.PREFERRED_SIZE, 175, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        panelClassiqueLayout.setVerticalGroup(
            panelClassiqueLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelClassiqueLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(panelClassiqueLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(classique, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        panelButton.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        panelButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                panelButtonMouseClicked(evt);
            }
        });
        panelButton.setLayout(new java.awt.CardLayout());

        jLabel4.setFont(new java.awt.Font("Lucida Grande", 1, 18)); // NOI18N
        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel4.setText("Start");
        panelButton.add(jLabel4, "card2");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 345, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 2, Short.MAX_VALUE))
                    .addComponent(panelModerne, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(panelClassique, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel1Layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(panelButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addContainerGap()))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(panelClassique, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(panelModerne, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(73, Short.MAX_VALUE))
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                    .addContainerGap(254, Short.MAX_VALUE)
                    .addComponent(panelButton, javax.swing.GroupLayout.PREFERRED_SIZE, 53, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap()))
        );

        getContentPane().add(jPanel1, "card2");

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void moderneActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_moderneActionPerformed
        panelButton.setBackground(new Color(87,148,210));
        jLabel4.setForeground(new Color(255,255,255));
    }//GEN-LAST:event_moderneActionPerformed

    private void classiqueActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_classiqueActionPerformed
        panelButton.setBackground(new Color(255,255,255));
        jLabel4.setForeground(new Color(0, 0, 0));
    }//GEN-LAST:event_classiqueActionPerformed

    private void panelButtonMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_panelButtonMouseClicked
        setTheme();
    }//GEN-LAST:event_panelButtonMouseClicked

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (UIManager.LookAndFeelInfo laf : UIManager
                .getInstalledLookAndFeels()) {
            if ("Nimbus".equals(laf.getName())) {
                UIManager.setLookAndFeel(laf.getClassName());
//                UIManager.getLookAndFeelDefaults().put("DesktopPane[Enabled].backgroundPainter", new DesktopPainter());
//                UIManager.getLookAndFeelDefaults().put("Button.background",new Color(0, 153, 153));
                //UIManager.getLookAndFeelDefaults().put("OptionPane.background", new Color(0, 102, 153));
                UIManager.getLookAndFeelDefaults().put("Button[Focused+MouseOver].backgroundPainter", new Color(0, 107, 163));
                UIManager.getLookAndFeelDefaults().put("Table.alternateRowColor", new Color(87,148,210));
                UIManager.getLookAndFeelDefaults().put("Table[Enabled+Selected].textBackground", Color.black);
                //UIManager.getLookAndFeelDefaults().put("Panel.background", Color.lightGray);
            }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Themes.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Themes.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Themes.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Themes.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                Themes dialog = new Themes(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JRadioButton classique;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JRadioButton moderne;
    private javax.swing.JPanel panelButton;
    private javax.swing.JPanel panelClassique;
    private javax.swing.JPanel panelModerne;
    // End of variables declaration//GEN-END:variables
}
