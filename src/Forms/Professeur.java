package Forms;

import Consults.Employe;
import Methods.*;
import com.ezware.oxbow.swingbits.table.filter.TableRowFilterSupport;
import java.awt.Color;
import java.awt.Component;
import java.awt.Point;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumnModel;
import Methods.Theme;

public class Professeur extends javax.swing.JInternalFrame {

    Manipulation Mn = new Manipulation();
    Theme th = new Theme();
    String[][] data = {};
    String[] head = {" Matières", " Classe", " Heure(s)"};
    String[] heads = {" Matricule", " Matières", " Classe", " Heure(s)"};
    String[] headd = {" Matricule", " Nom", " Prénom"};
    DefaultTableModel md = new DefaultTableModel(data, head);
    DefaultTableModel mds = new DefaultTableModel(data, heads);
    DefaultTableModel mdd = new DefaultTableModel(data, headd);
    TableCellRenderer render = new TableCellRenderer() {
        public Component getTableCellRendererComponent(JTable table, Object value, boolean idSelected, boolean hasFocus, int row, int column) {
            JLabel lbl = new JLabel(value == null ? "" : value.toString());
            if (column == 0 && row >= 0) {
                lbl.setHorizontalAlignment(SwingConstants.LEFT);
                lbl.setOpaque(true);
                lbl.setBackground(Color.green);
            }
            return lbl;
        }
    };

    public Professeur() {
        initComponents();
        this.setTitle("Scolaris: " + Variables.Empresa + " :Cours par Professeur");
        tblmatiere.setModel(md);
        tblprofesseur.setModel(mdd);
        tblcours.setModel(mds);
        Mn.FULLCOMBO(classe, "description", "classe");
        Mn.FULLCOMBO(matiere, "description", "matiere");
        // Mn.MATRICULES_FORMAT(matricule);
        Nettoyer();
        RemplirProfesseur();
        mesajel.setVisible(false);
        //---------------------------------------------------------------------
        TableColumnModel cm = tblmatiere.getColumnModel();
        TableColumnModel cmy = tblprofesseur.getColumnModel();
        TableColumnModel cmyy = tblcours.getColumnModel();
        cm.getColumn(0).setPreferredWidth(120);
        cm.getColumn(1).setPreferredWidth(50);
        cm.getColumn(2).setPreferredWidth(5);
        cmy.getColumn(0).setPreferredWidth(30);
        cmy.getColumn(1).setPreferredWidth(70);
        cmy.getColumn(2).setPreferredWidth(70);
        cmyy.getColumn(0).setPreferredWidth(30);
        cmyy.getColumn(1).setPreferredWidth(90);
        cmyy.getColumn(2).setPreferredWidth(70);
        cmyy.getColumn(3).setPreferredWidth(5);
        tblcours.getColumnModel().getColumn(0).setCellRenderer(render);
        tblprofesseur.getColumnModel().getColumn(0).setCellRenderer(render);
        TableRowFilterSupport.forTable(tblcours).searchable(true).apply();
        TableRowFilterSupport.forTable(tblmatiere).searchable(true).apply();
        TableRowFilterSupport.forTable(tblprofesseur).searchable(true).apply();
        effacer.setEnabled(false);
    }

    private void Nettoyer() {
        matiere.addItem("Selectionnez");
        classe.addItem("Selectionnez");
        matiere.setSelectedItem("Selectionnez");
        classe.setSelectedItem("Selectionnez");
        matricule.setValue(null);
        //Mn.MATRICULES_FORMAT(matricule);
        heure.setText(null);
        nom.setText("Nom du professeur");
        matricule.requestFocus();
        Mn.EMPTYTABLE(tblcours);
    }

    private void RemplirProfesseur() {
        String query = "select distinct e.matricule,t.nom,t.prenom from employer_vs_type p join employer e on e.matricule\n"
                + "=p.matricule join tercero t on (t.code = e.code and t.code_tercero=e.code_tercero) "
                + "join type_employer tp on (p.idtype_employer = tp.idtype_employer) "
                + "where tp.description ='Professeur' order by nom asc";
        System.out.println("dff "+query);
        Mn.FULLTABLE(query, tblprofesseur, mdd);
    }

    private void RemplirMatieres() {
        Mn.EMPTYTABLE(tblmatiere);
        String aa = tblprofesseur.getValueAt(tblprofesseur.getSelectedRow(), 0).toString();
        String query = "select matieres,classe,heure,deviz from professeur where matricule='" + aa + "'  ";
        ResultSet rs = Mn.SEARCHDATA(query);
        try {
            while (rs.next()) {
                String[] vect = {rs.getString("matieres"), rs.getString("classe"), rs.getString("heure") + " " + rs.getString("deviz")};
                Mn.ADD_TABLE(md, vect);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Professeur.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void RemplirCours() {
        String aa = matricule.getText();
        String query = "select matricule,matieres,classe,heure,deviz from professeur where matricule='" + aa + "'";
        Mn.FULLTABLE(query, tblcours, mds);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel1 = new javax.swing.JPanel();
        add = new javax.swing.JButton();
        rem = new javax.swing.JButton();
        jLabel32 = new javax.swing.JLabel();
        jLabel33 = new javax.swing.JLabel();
        jLabel34 = new javax.swing.JLabel();
        heure = new org.edisoncor.gui.textField.TextFieldRectBackground();
        deviz = new javax.swing.JLabel();
        mesajel = new javax.swing.JLabel();
        matiere = new javax.swing.JComboBox();
        classe = new javax.swing.JComboBox();
        jPanel3 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        matricule = new javax.swing.JFormattedTextField();
        jButton1 = new javax.swing.JButton();
        nom = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        jPanel5 = new javax.swing.JPanel();
        jPanel6 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblcours = new javax.swing.JTable(){
            public boolean isCellEditable(int rowIndex, int colIndex){
                return false;
            }
        };
        jPanel2 = new javax.swing.JPanel();
        jPanel7 = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        tblprofesseur = new javax.swing.JTable(){
            public boolean isCellEditable(int rowIndex, int colIndex){
                return false;
            }
        };
        jPanel8 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tblmatiere = new javax.swing.JTable(){
            public boolean isCellEditable(int rowIndex, int colIndex){
                return false;
            }
        };
        jMenuBar4 = new javax.swing.JMenuBar();
        jMenu4 = new javax.swing.JMenu();
        enregistrer = new javax.swing.JMenuItem();
        nouveau3 = new javax.swing.JMenuItem();
        effacer = new javax.swing.JMenuItem();
        jMenu5 = new javax.swing.JMenu();
        modifier = new javax.swing.JMenuItem();
        jMenuItem1 = new javax.swing.JMenuItem();

        setClosable(true);
        setIconifiable(true);

        jTabbedPane1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jTabbedPane1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTabbedPane1MouseClicked(evt);
            }
        });

        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        add.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/Bajar.png"))); // NOI18N
        add.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addActionPerformed(evt);
            }
        });
        jPanel1.add(add, new org.netbeans.lib.awtextra.AbsoluteConstraints(223, 141, 41, 30));

        rem.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/Subir.png"))); // NOI18N
        rem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                remActionPerformed(evt);
            }
        });
        jPanel1.add(rem, new org.netbeans.lib.awtextra.AbsoluteConstraints(282, 141, 42, -1));

        jLabel32.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel32.setText("Matières ");
        jPanel1.add(jLabel32, new org.netbeans.lib.awtextra.AbsoluteConstraints(12, 98, -1, -1));

        jLabel33.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel33.setText("Classes");
        jPanel1.add(jLabel33, new org.netbeans.lib.awtextra.AbsoluteConstraints(363, 98, -1, -1));

        jLabel34.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel34.setText("Heure");
        jPanel1.add(jLabel34, new org.netbeans.lib.awtextra.AbsoluteConstraints(18, 152, -1, -1));

        heure.setDescripcion("");
        jPanel1.add(heure, new org.netbeans.lib.awtextra.AbsoluteConstraints(72, 141, -1, 30));

        deviz.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        deviz.setText("Heure(s)");
        jPanel1.add(deviz, new org.netbeans.lib.awtextra.AbsoluteConstraints(153, 152, -1, -1));

        mesajel.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        mesajel.setForeground(new java.awt.Color(204, 0, 0));
        mesajel.setText("Cet employer n'est pas un professeur, s'il-vous plait  veuillez choisis un autre.");
        jPanel1.add(mesajel, new org.netbeans.lib.awtextra.AbsoluteConstraints(7, 58, 510, -1));

        matiere.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        matiere.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Selectionnez", " " }));
        jPanel1.add(matiere, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 85, 250, 38));

        classe.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        classe.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Selectionnez", " " }));
        jPanel1.add(classe, new org.netbeans.lib.awtextra.AbsoluteConstraints(425, 85, 250, 38));

        jPanel3.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel3.setLayout(new java.awt.GridBagLayout());

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel3.setText("Matricule");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(12, 12, 0, 0);
        jPanel3.add(jLabel3, gridBagConstraints);

        matricule.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        matricule.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                matriculeFocusLost(evt);
            }
        });
        matricule.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                matriculeKeyTyped(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                matriculeKeyReleased(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridheight = 2;
        gridBagConstraints.ipadx = 173;
        gridBagConstraints.ipady = 15;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(3, 12, 3, 0);
        jPanel3.add(matricule, gridBagConstraints);

        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/consulta.png"))); // NOI18N
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridheight = 2;
        gridBagConstraints.ipadx = -18;
        gridBagConstraints.ipady = -4;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(3, 6, 3, 0);
        jPanel3.add(jButton1, gridBagConstraints);

        nom.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        nom.setForeground(new java.awt.Color(0, 0, 255));
        nom.setText("Nom du professeur");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.ipadx = 35;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(12, 18, 0, 53);
        jPanel3.add(nom, gridBagConstraints);

        jPanel1.add(jPanel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 10, 530, 40));

        jPanel4.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel4.setLayout(new java.awt.GridBagLayout());
        jPanel1.add(jPanel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 80, 680, 50));

        jPanel5.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel5.setLayout(new java.awt.GridBagLayout());
        jPanel1.add(jPanel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 140, 680, 40));

        jPanel6.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel6.setLayout(new javax.swing.BoxLayout(jPanel6, javax.swing.BoxLayout.LINE_AXIS));

        tblcours.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tblcours.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                tblcoursMousePressed(evt);
            }
        });
        jScrollPane1.setViewportView(tblcours);

        jPanel6.add(jScrollPane1);

        jPanel1.add(jPanel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 190, 670, 220));

        jTabbedPane1.addTab("COURS PAR PROFESSEUR", jPanel1);

        jPanel2.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel7.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel7.setLayout(new javax.swing.BoxLayout(jPanel7, javax.swing.BoxLayout.LINE_AXIS));

        tblprofesseur.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tblprofesseur.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblprofesseurMouseClicked(evt);
            }
        });
        jScrollPane3.setViewportView(tblprofesseur);

        jPanel7.add(jScrollPane3);

        jPanel2.add(jPanel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, 670, 220));

        jPanel8.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel8.setLayout(new javax.swing.BoxLayout(jPanel8, javax.swing.BoxLayout.LINE_AXIS));

        tblmatiere.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane2.setViewportView(tblmatiere);

        jPanel8.add(jScrollPane2);

        jPanel2.add(jPanel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 240, 670, 180));

        jTabbedPane1.addTab("CONSULTATION", jPanel2);

        jMenu4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/fichier.jpg"))); // NOI18N
        jMenu4.setText("Action");

        enregistrer.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_E, java.awt.event.InputEvent.CTRL_MASK));
        enregistrer.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/icons8-save_close.png"))); // NOI18N
        enregistrer.setText("Enrégistrer");
        enregistrer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                enregistrerActionPerformed(evt);
            }
        });
        jMenu4.add(enregistrer);

        nouveau3.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_N, java.awt.event.InputEvent.CTRL_MASK));
        nouveau3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/icons8-add_list.png"))); // NOI18N
        nouveau3.setText("Nouveau");
        nouveau3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nouveau3ActionPerformed(evt);
            }
        });
        jMenu4.add(nouveau3);

        effacer.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_D, java.awt.event.InputEvent.CTRL_MASK));
        effacer.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/icons8-erase.png"))); // NOI18N
        effacer.setText("Effacer");
        effacer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                effacerActionPerformed(evt);
            }
        });
        jMenu4.add(effacer);

        jMenuBar4.add(jMenu4);

        jMenu5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/editer.jpg"))); // NOI18N
        jMenu5.setText("Edition");

        modifier.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_M, java.awt.event.InputEvent.CTRL_MASK));
        modifier.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/icons8-refresh.png"))); // NOI18N
        modifier.setText("Modifier");
        modifier.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                modifierActionPerformed(evt);
            }
        });
        jMenu5.add(modifier);

        jMenuItem1.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F1, 0));
        jMenuItem1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/icons8-help.png"))); // NOI18N
        jMenuItem1.setText("Aide");
        jMenu5.add(jMenuItem1);

        jMenuBar4.add(jMenu5);

        setJMenuBar(jMenuBar4);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane1)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 453, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void nouveau3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nouveau3ActionPerformed
        Nettoyer();
        RemplirProfesseur();
        Mn.EMPTYTABLE(tblmatiere);
    }//GEN-LAST:event_nouveau3ActionPerformed

    private void enregistrerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_enregistrerActionPerformed

        String ab = "";
        String lalal = "";
        for (int i = 0; i < tblcours.getRowCount(); i++) {
            String qq = tblcours.getValueAt(i, 0).toString();
            String qqq = tblcours.getValueAt(i, 1).toString();
            String ddd = tblcours.getValueAt(i, 2).toString();
            String eee = tblcours.getValueAt(i, 3).toString();
            String fff = "Heure(s)";
            String hola = "select matricule from professeur where matieres='" + qqq + "' and classe='" + ddd + "'";
            ResultSet rss = Mn.SEARCHDATA(hola);
            try {
                if (rss.first()) {
                    ab = rss.getString("matricule");
                }
            } catch (SQLException ex) {
                Logger.getLogger(Eleves.class.getName()).log(Level.SEVERE, null, ex);
            }
            if (matricule.getText().equals(ab)) {
                lalal = "update professeur set matricule='" + qq + "',matieres='" + qqq + "',classe='" + ddd + "',heure=" + eee + " "
                        + "where matieres='" + qqq + "' and classe='" + ddd + "'";
                Mn.MANIPULEDB(lalal);
                ab = "";
            } else {
                lalal = "call insert_professeur('"+qq+"','"+qqq+"','"+ddd+"','"+eee+"','"+fff+"')";
                Mn.MANIPULEDB(lalal);
            }
        }
        nouveau3.doClick();
    }//GEN-LAST:event_enregistrerActionPerformed

    private void effacerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_effacerActionPerformed
        String mat = Mn.SELLECT_BUTTON(Variables.Seleccionado1, tblcours);
        String mati = tblcours.getValueAt(tblcours.getSelectedRow(), 1).toString();
        String clas = tblcours.getValueAt(tblcours.getSelectedRow(), 2).toString();
        String query = "delete from  professeur where matricule='" + mat + "' and matieres='" + mati + "' and classe='" + clas + "'";
        //System.out.println(query);
        Mn.MANIPULEDB(query);
        nouveau3.doClick();
    }//GEN-LAST:event_effacerActionPerformed

    private void modifierActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_modifierActionPerformed
        String ab = "";
        String lalal = "";
        for (int i = 0; i < tblcours.getRowCount(); i++) {
            String qq = tblcours.getValueAt(i, 0).toString();
            String qqq = tblcours.getValueAt(i, 1).toString();
            String ddd = tblcours.getValueAt(i, 2).toString();
            String eee = tblcours.getValueAt(i, 3).toString();
            String fff = "Heure(s)";
            String hola = "select matricule from professeur where matieres='" + qqq + "' and classe='" + ddd + "'";
            ResultSet rss = Mn.SEARCHDATA(hola);
            try {
                if (rss.first()) {
                    ab = rss.getString("matricule");
                }
            } catch (SQLException ex) {
                Logger.getLogger(Eleves.class.getName()).log(Level.SEVERE, null, ex);
            }
            if (matricule.getText().equals(ab)) {
                lalal = "update professeur set matricule='" + qq + "',matieres='" + qqq + "',classe='" + ddd + "',heure=" + eee + " "
                        + "where matieres='" + qqq + "' and classe='" + ddd + "' ";
                Mn.MANIPULEDB(lalal);
                ab = "";
            } else {
                lalal = "call Insert_professeur('" + qq + "','" + qqq + "','" + ddd + "','" + eee + "','" + fff + "')";
                Mn.MANIPULEDB(lalal);
            }
        }
        nouveau3.doClick();
    }//GEN-LAST:event_modifierActionPerformed

    private void addActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addActionPerformed
        String[] vect = {matricule.getText(), matiere.getSelectedItem().toString(), classe.getSelectedItem().toString(), heure.getText(), deviz.getText()};
        Mn.ADD_TABLE(mds, vect);
        classe.setSelectedItem(null);
        heure.setText(null);

    }//GEN-LAST:event_addActionPerformed

    private void remActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_remActionPerformed
        Mn.REMOVE_TABLE(tblcours, mds);
    }//GEN-LAST:event_remActionPerformed

    private void matriculeFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_matriculeFocusLost

    }//GEN-LAST:event_matriculeFocusLost

    private void matriculeKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_matriculeKeyReleased
        matricule.setText(matricule.getText().toUpperCase());
        String query = "select nom,prenom from show_employes where matricule = '" + matricule.getText() + "' ";
        ResultSet rs = Mn.SEARCHDATA(query);
        try {
            while (rs.next()) {
                nom.setText(rs.getString("nom") + " " + rs.getString("prenom"));
                String sesa = "select description from type_employer te join employer_vs_type et on(te.idtype_employer = et.idtype_employer)\n" +
" where et.matricule='"+matricule.getText().trim()+"' and description = 'Professeur' group by description";
                System.out.println("Sesa : "+sesa);
                ResultSet rss = Mn.SEARCHDATA(sesa);
                if (rss.next()) {
                    if (rss.getString("description").equals("Professeur")) {
                        mesajel.setVisible(false);
                        Mn.FULLCOMBO(classe, "description", "classe");
                        matiere.setSelectedItem(null);
                        Mn.FULLCOMBO(matiere, "description", "matiere");
                        classe.setSelectedItem(null);
                    } else {
                        nom.setText(rs.getString("nom") + " " + rs.getString("prenom"));
                        mesajel.setVisible(true);
                        matiere.removeAllItems();
                        classe.removeAllItems();
                    }
                } else {
                    nom.setText(rs.getString("nom") + " " + rs.getString("prenom"));
                    mesajel.setVisible(true);
                    matiere.removeAllItems();
                    classe.removeAllItems();
                }

            }
        } catch (SQLException ex) {
            Logger.getLogger(Professeur.class.getName()).log(Level.SEVERE, null, ex);
        }
        RemplirCours();
    }//GEN-LAST:event_matriculeKeyReleased

    private void matriculeKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_matriculeKeyTyped
        Mn.ONLYNUMBERS(evt);
        matricule.requestFocus();
    }//GEN-LAST:event_matriculeKeyTyped

    private void jTabbedPane1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTabbedPane1MouseClicked
        RemplirProfesseur();
    }//GEN-LAST:event_jTabbedPane1MouseClicked

    private void tblprofesseurMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblprofesseurMouseClicked
        RemplirMatieres();
    }//GEN-LAST:event_tblprofesseurMouseClicked

    private void tblcoursMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblcoursMousePressed
        JTable table = (JTable) evt.getSource();
        Point pt = evt.getPoint();
        int row = table.rowAtPoint(pt);
        if (evt.getClickCount() == 2) {
            matiere.setSelectedItem(tblcours.getValueAt(tblcours.getSelectedRow(), 1).toString());
            classe.setSelectedItem(tblcours.getValueAt(tblcours.getSelectedRow(), 2).toString());
            heure.setText(tblcours.getValueAt(tblcours.getSelectedRow(), 3).toString());
            Mn.REMOVE_TABLE(tblcours, mds);
        }
    }//GEN-LAST:event_tblcoursMousePressed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        Variables.ut = "";
        Employe emp = new Employe(null, rootPaneCheckingEnabled);
        emp.setVisible(true);
        matricule.setText(Variables.ut);
        matriculeKeyReleased(null);
    }//GEN-LAST:event_jButton1ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Professeur.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Professeur.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Professeur.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Professeur.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Professeur().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton add;
    private javax.swing.JComboBox classe;
    private javax.swing.JLabel deviz;
    private javax.swing.JMenuItem effacer;
    private javax.swing.JMenuItem enregistrer;
    private org.edisoncor.gui.textField.TextFieldRectBackground heure;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel32;
    private javax.swing.JLabel jLabel33;
    private javax.swing.JLabel jLabel34;
    private javax.swing.JMenu jMenu4;
    private javax.swing.JMenu jMenu5;
    private javax.swing.JMenuBar jMenuBar4;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JComboBox matiere;
    private javax.swing.JFormattedTextField matricule;
    private javax.swing.JLabel mesajel;
    private javax.swing.JMenuItem modifier;
    private javax.swing.JLabel nom;
    private javax.swing.JMenuItem nouveau3;
    private javax.swing.JButton rem;
    private javax.swing.JTable tblcours;
    private javax.swing.JTable tblmatiere;
    private javax.swing.JTable tblprofesseur;
    // End of variables declaration//GEN-END:variables
}
