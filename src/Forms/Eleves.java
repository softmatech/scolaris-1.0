package Forms;

import Consults.consultPerson_no_eleve;
import Consults.consult_no_eleves;
import Methods.DragListener;
import Methods.GenerateMatricule;
import Methods.Manipulation;
import Methods.Security;
import Methods.Theme;
import Methods.Variables;
import Processus.Entrer_note;
import com.ezware.oxbow.swingbits.table.filter.TableRowFilterSupport;
import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.EventQueue;
import java.awt.Image;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.dnd.DropTarget;
import java.awt.event.KeyEvent;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumnModel;
import org.edisoncor.gui.textField.TextField;
import org.edisoncor.gui.textField.TextFieldRectBackground;

public class Eleves extends javax.swing.JInternalFrame {

    Manipulation Mn = new Manipulation();
    Security sc = new Security();
    Theme th = new Theme();
    public static String path;
    int aaa,cod = 0;
    String codter = null;
    
    GenerateMatricule gm = new GenerateMatricule();
    String[][] data = {};
    String[] head = {"Nom", " Prénom", "Matricule", "Personne Responsable", "Téléphone", "Lien"};
    DefaultTableModel md = new DefaultTableModel(data, head);

    TableCellRenderer render = new TableCellRenderer() {
        public Component getTableCellRendererComponent(JTable table, Object value, boolean idSelected, boolean hasFocus, int row, int column) {
            JLabel lbl = new JLabel(value == null ? "" : value.toString());
            if (column == 2 && row >= 0) {
                lbl.setHorizontalAlignment(SwingConstants.LEFT);
                lbl.setOpaque(true);
                lbl.setBackground(Color.green);
            }
            return lbl;
        }
    };

    JTextField coode = new JTextField();
    JTextField code = new JTextField();

    public Eleves() {
        initComponents();
        estatusItemStateChanged(null);
        JComponent comp = this;
        secureState();
        this.setTitle("Scolaris: " + Variables.Empresa + " : Registre des Elèves");
        nouveau.doClick();
        matricules.setText(gm.MatriculeCode());
        code.setEditable(false);
        matricules.setEditable(false);
        tbleleve.setModel(md);
        
        nom.setEditable(false);
        prenom.setEditable(false);
        sexe.setEnabled(false);
        adresse.setEditable(false);
        teleleve.setEditable(false);
        sanguins.setEnabled(false);
        matricules.setEditable(false);
        naissance.setEnabled(false);
        
        Consulter();
        //---------------------------------------------------------------------
        TableColumnModel cm = tbleleve.getColumnModel();
        cm.getColumn(0).setPreferredWidth(50);
        cm.getColumn(1).setPreferredWidth(90);
        cm.getColumn(2).setPreferredWidth(20);
        cm.getColumn(3).setPreferredWidth(80);
        cm.getColumn(4).setPreferredWidth(40);
        cm.getColumn(5).setPreferredWidth(10);
        tbleleve.getColumnModel().getColumn(2).setCellRenderer(render);
        search();
        
        tbleleve.getSelectionModel().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        effacer.setEnabled(false);
        tableEleveChangeListener();
        TableRowFilterSupport.forTable(tbleleve).searchable(true).apply();
        prenom.setName("prenom");
        nom.setName("nom");
        adresse.setName("adresse");
        connectDragDrop();
        naissance.setLocale(Locale.FRENCH);
        
        String fichero  = System.getProperty("user.dir")+"/src/Pictures/icons8-detective.png";
        System.out.println("cd "+fichero);
        Image image = Toolkit.getDefaultToolkit().createImage(fichero); 
        Cursor cursor = Toolkit.getDefaultToolkit().createCustomCursor(image, new Point(10,10),"aa"); 
        nom.setCursor(cursor); prenom.setCursor(cursor);  responsable.setCursor(cursor);
    }
    
    private void photoIcon(){
        String fichero  = System.getProperty("user.dir")+"/src/Pictures/icons8-user_male_circle_filled.png";
        System.out.println("cd "+fichero);
        Image image = Toolkit.getDefaultToolkit().createImage(fichero); 
        ImageIcon imageicon = new ImageIcon(image);
        
        if(photo.getIcon() == null){
            photo.setIcon(imageicon);
            photo.setText("Double click pour Telecharger une Photo");
        }
    }
    
    private void focusLost(){
        
        try {
            String sql = "SELECT * FROM tercero_general_view where code = '"+Variables.code_tercero+"' ";
            System.out.println(sql);
            ResultSet rs = Mn.Rezulta(sql);
            if(rs.last()){
                nom.setText(rs.getString("nom"));
                prenom.setText(rs.getString("prenom"));
                sexe.setSelectedItem(rs.getString("sexe"));
                naissance.setDate(rs.getDate("date_naissance"));
                adresse.setText(rs.getString("adresse"));
                
                //image
                sql = "select imagecol from show_image where code = '"+Variables.code_tercero+"'  ";
                Mn.ShowPhotos(sql, photo);
                
                //sanguins
            ResultSet er = Mn.Rezulta("select sang from show_sanguins where code='"+Variables.code_tercero+"' ");
            if(er.first()){
                sanguins.setSelectedItem(er.getString("sang"));
            }
                //.................
            //telephones
                sql = "SELECT phone_number FROM show_telephones where code = '"+Variables.code_tercero+"' ";
                ResultSet rst = Mn.Rezulta(sql);
                if (rst.first()) {
                    teleleve.setText(rst.getString("phone_number"));
                }
            }   
        } catch (SQLException ex) {
            Logger.getLogger(Person.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private void photoNull(){
        try {
            if(photo.getIcon() == null || photo.getText().isEmpty()){
            photo.setText("Double Click pour Telecharger une photo");
        }
        } catch (NullPointerException e) {
        }
        
    }
    
    private void connectDragDrop(){
        DragListener d = new DragListener(photo, path);
        //SET TARGET TO JFRAME
         new DropTarget(this,d);
    }
    
     private void  autoCompleteEvent(KeyEvent evt,final TextFieldRectBackground textfield){
        switch(evt.getKeyCode()){
            case KeyEvent.VK_BACK_SPACE:
                break;
                case KeyEvent.VK_ENTER:
                    break;
                    default:
                        EventQueue.invokeLater(new Runnable() {

            @Override
            public void run() {
            String txt = textfield.getText();
                Mn.autoComplete(txt,"tercero",textfield.getName(),textfield);
            }
        });
        }
    }
    
    private void tableEleveChangeListener(){
        tbleleve.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                if(tbleleve.getRowCount() > 0){
                try {
                    //
                    String aa = "";
                      aa = tbleleve.getValueAt(tbleleve.getSelectedRow(), 2).toString();  
                    
                    System.out.println(tbleleve.getSelectedRow()+" "+aa);
                    String sql = "select code from show_eleve where matricule = '"+aa+"' ";
                    ResultSet rs = Mn.Rezulta(sql);
                    if(rs.last()){
                        Variables.code_tercero = rs.getString("code");
                    }
                    
                    focusLost();
                    String[] champ = {"estatus", "matricule"};
                    code.setText(aa);
                    estatus.setSelected(Mn.CONSULT_CHECKED("Show_eleves", code, champ));
                    prenom.requestFocus();
                    try {
                        Mn.ShowPhotos("SELECT * FROM image where code  = '"+aa+"' ", photo);
                    } catch (NullPointerException ex) {
                    }
                    
                    
                    //
                    
                    // Autres
                        String xc = "select cond_sante,responsable,lien_parente from show_eleves where matricule = '"+aa+"' ";
                        ResultSet rss  = Mn.Rezulta(xc);
                        if(rss.last()){
                        sante.setSelectedItem(rss.getString("cond_sante"));
                        responsable.setText(rss.getString("responsable"));
                        lien.setSelectedItem(rss.getString("lien_parente"));
                        }
                        
                        //telparent
                        String cv = "select telephone from show_eleve where matricule = '"+aa+"' ";
                        ResultSet rsd = Mn.Rezulta(cv);
                        if(rsd.last()){
                        telparent.setText(rsd.getString("telephone"));
                        }
                        
                } catch (SQLException ex) {
                    Logger.getLogger(Eleves.class.getName()).log(Level.SEVERE, null, ex);
                }
                        
                }
        }
        });
    }

    private Object identify() {

        int j = 0;
        int d = (jPanel1.getComponents().length);
        System.out.println(d);

        ArrayList<Object> compName = new ArrayList<>();

        for (int i = 0; i < d; i++) {
            if (jPanel1.getComponent(i) instanceof org.edisoncor.gui.textField.TextFieldRectBackground) {
                System.out.println(i + "yes");
                compName.add(jPanel1.getComponent(i).getName());
            } else if (jPanel1.getComponent(i) instanceof javax.swing.JComboBox) {
                compName.add(jPanel1.getComponent(i).getName());
            } else if (jPanel1.getComponent(i) instanceof javax.swing.JFormattedTextField) {
                compName.add(jPanel1.getComponent(i).getName());
            } else if (jPanel1.getComponent(i) instanceof com.toedter.calendar.JDateChooser) {
                compName.add(jPanel1.getComponent(i).getName());
            } else if (jPanel1.getComponent(i) instanceof javax.swing.JCheckBox) {
                compName.add(jPanel1.getComponent(i).getName());
            }

            j++;
        }

        System.out.println(compName);
        return compName;
    }

    private void secureState() {
        TextField textField;
        identify();
    }

    private void Nettoyer() {
        prenom.setText(null);
        nom.setText(null);
        adresse.setText(null);
        teleleve.setValue(null);
        Mn.PHONE_FROMAT(teleleve);
        telparent.setValue(null);
        Mn.PHONE_FROMAT(telparent);
        naissance.setCalendar(null);
        responsable.setText(null);
        estatus.setSelected(false);
        lien.setSelectedItem("Selectionnez");
        sexe.setSelectedItem("Selectionnez");
        sante.setSelectedItem("Selectionnez");
        sanguins.setSelectedItem("Selectionnez");
        matricules.setText(gm.MatriculeCode());
        photo.setIcon(null); photo.updateUI();
        photoIcon();
    }

    private void Consulter() {
        String query = "select code,nom,prenom,matricule,responsable,lien_parente as lien, institution \n" +
"from show_eleves where institution ='" + Variables.Empresa + "'"
                + " order by code desc";
        System.out.println("cx "+query);
        ResultSet rs = Mn.SEARCHDATA(query);
        try {
            Mn.removeDatosOntable(md);
            while (rs.next()) {
                String[] vect = {rs.getString("nom"), rs.getString("prenom"), rs.getString("matricule"),
                    rs.getString("responsable"),null, rs.getString("lien")};
                Mn.ADD_TABLE(md, vect);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Entrer_note.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPopupMenu1 = new javax.swing.JPopupMenu();
        jMenuItem2 = new javax.swing.JMenuItem();
        jSeparator1 = new javax.swing.JPopupMenu.Separator();
        jMenuItem3 = new javax.swing.JMenuItem();
        jPanel6 = new javax.swing.JPanel();
        recherche = new org.edisoncor.gui.textField.TextFieldRectBackground();
        jLabel8 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jLabel13 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        prenom = new org.edisoncor.gui.textField.TextFieldRectBackground();
        nom = new org.edisoncor.gui.textField.TextFieldRectBackground();
        sexe = new javax.swing.JComboBox();
        adresse = new org.edisoncor.gui.textField.TextFieldRectBackground();
        teleleve = new javax.swing.JFormattedTextField();
        sanguins = new javax.swing.JComboBox();
        jPanel4 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jPanel5 = new javax.swing.JPanel();
        matricules = new org.edisoncor.gui.textField.TextFieldRectBackground();
        naissance = new com.toedter.calendar.JDateChooser();
        sante = new javax.swing.JComboBox();
        responsable = new org.edisoncor.gui.textField.TextFieldRectBackground();
        telparent = new javax.swing.JFormattedTextField();
        lien = new javax.swing.JComboBox();
        estatus = new javax.swing.JCheckBox();
        jPanel8 = new javax.swing.JPanel();
        photo = new javax.swing.JLabel();
        jPanel7 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tbleleve = new javax.swing.JTable(){
            public boolean isCellEditable(int rowIndex,int colIndex){
                return false;
            }
        };
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        enregistrer = new javax.swing.JMenuItem();
        nouveau = new javax.swing.JMenuItem();
        effacer = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        modifier = new javax.swing.JMenuItem();
        jMenuItem1 = new javax.swing.JMenuItem();

        jMenuItem2.setText("Telecharger Photo....");
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem2ActionPerformed(evt);
            }
        });
        jPopupMenu1.add(jMenuItem2);
        jPopupMenu1.add(jSeparator1);

        jMenuItem3.setText("Effacer...");
        jMenuItem3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem3ActionPerformed(evt);
            }
        });
        jPopupMenu1.add(jMenuItem3);

        setClosable(true);
        setIconifiable(true);

        jPanel6.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        recherche.setBackground(new java.awt.Color(255, 255, 153));
        recherche.setDescripcion("Entrer le matricule de l'élève ici");
        recherche.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                rechercheKeyReleased(evt);
            }
        });

        jLabel8.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel8.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel8.setText("Recherche : ");

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addGap(29, 29, 29)
                .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(3, 3, 3)
                .addComponent(recherche, javax.swing.GroupLayout.PREFERRED_SIZE, 739, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(recherche, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(jLabel8))
        );

        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel2.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel13.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel13.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel13.setText("Gr. Sanguins : ");

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel4.setText("Telephone : ");

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel3.setText("Adresse : ");

        jLabel12.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel12.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel12.setText("Sexe : ");

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel2.setText("Nom : ");

        jLabel15.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel15.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel15.setText("Prénom : ");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 126, Short.MAX_VALUE)
            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel2Layout.createSequentialGroup()
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                            .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jLabel15, javax.swing.GroupLayout.Alignment.TRAILING)
                                .addComponent(jLabel2, javax.swing.GroupLayout.Alignment.TRAILING)
                                .addComponent(jLabel12, javax.swing.GroupLayout.Alignment.TRAILING)
                                .addComponent(jLabel3, javax.swing.GroupLayout.Alignment.TRAILING)
                                .addComponent(jLabel4, javax.swing.GroupLayout.Alignment.TRAILING))))
                    .addGap(3, 3, 3)))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 266, Short.MAX_VALUE)
            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel2Layout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addComponent(jLabel15)
                    .addGap(21, 21, 21)
                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(16, 16, 16)
                    .addComponent(jLabel12)
                    .addGap(28, 28, 28)
                    .addComponent(jLabel3)
                    .addGap(22, 22, 22)
                    .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(6, 6, 6)
                    .addComponent(jLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, Short.MAX_VALUE)))
        );

        jPanel1.add(jPanel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 10, 130, 270));

        jPanel3.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        prenom.setDescripcion("Entrer le nom de l'élève ici");
        prenom.setName("prenom"); // NOI18N
        prenom.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                prenomMouseClicked(evt);
            }
        });
        prenom.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                prenomActionPerformed(evt);
            }
        });
        prenom.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                prenomKeyReleased(evt);
            }
        });

        nom.setDescripcion("Entrer le prénom de l'élève ici");
        nom.setName("nom"); // NOI18N
        nom.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                nomMouseClicked(evt);
            }
        });
        nom.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                nomKeyReleased(evt);
            }
        });

        sexe.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        sexe.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Selectionnez", "Féminin", "Masculin", "Autres" }));
        sexe.setName("sexe"); // NOI18N

        adresse.setDescripcion("Entrer l' adresse ici");
        adresse.setName("adresse"); // NOI18N
        adresse.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                adresseKeyReleased(evt);
            }
        });

        teleleve.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        teleleve.setName("teleleve"); // NOI18N

        sanguins.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        sanguins.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Selectionnez", "A+", "A-", "B+", "B-", "O+", "O-", "AB+", "AB-", "Autres" }));
        sanguins.setName("sanguins"); // NOI18N

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 306, Short.MAX_VALUE)
            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel3Layout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(prenom, javax.swing.GroupLayout.PREFERRED_SIZE, 296, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(nom, javax.swing.GroupLayout.PREFERRED_SIZE, 296, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(sexe, javax.swing.GroupLayout.PREFERRED_SIZE, 296, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(adresse, javax.swing.GroupLayout.PREFERRED_SIZE, 296, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(jPanel3Layout.createSequentialGroup()
                            .addGap(1, 1, 1)
                            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(teleleve, javax.swing.GroupLayout.PREFERRED_SIZE, 295, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(sanguins, javax.swing.GroupLayout.PREFERRED_SIZE, 295, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGap(0, 0, Short.MAX_VALUE)))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 266, Short.MAX_VALUE)
            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel3Layout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addComponent(prenom, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(6, 6, 6)
                    .addComponent(nom, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(8, 8, 8)
                    .addComponent(sexe, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(13, 13, 13)
                    .addComponent(adresse, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(13, 13, 13)
                    .addComponent(teleleve, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(6, 6, 6)
                    .addComponent(sanguins, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, Short.MAX_VALUE)))
        );

        jPanel1.add(jPanel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 10, 310, 270));

        jPanel4.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel5.setText("Matricule : ");

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel6.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel6.setText("Naissance : ");

        jLabel7.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel7.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel7.setText("Santé : ");

        jLabel9.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel9.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel9.setText("Pers. Resp : ");

        jLabel10.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel10.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel10.setText("Telephone : ");

        jLabel11.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel11.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel11.setText("Lien  : ");

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 98, Short.MAX_VALUE)
            .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel4Layout.createSequentialGroup()
                    .addGap(0, 0, 0)
                    .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(jPanel4Layout.createSequentialGroup()
                            .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(jLabel9, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jLabel10, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jLabel6, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jLabel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                            .addGap(0, 0, Short.MAX_VALUE)))
                    .addContainerGap()))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 266, Short.MAX_VALUE)
            .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel4Layout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addComponent(jLabel5)
                    .addGap(21, 21, 21)
                    .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(16, 16, 16)
                    .addComponent(jLabel7)
                    .addGap(28, 28, 28)
                    .addComponent(jLabel9)
                    .addGap(28, 28, 28)
                    .addComponent(jLabel10)
                    .addGap(17, 17, 17)
                    .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, Short.MAX_VALUE)))
        );

        jPanel1.add(jPanel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(440, 10, 90, 270));

        jPanel5.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel5.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        matricules.setDescripcion("Entrer le matricule de l'élève ici");
        jPanel5.add(matricules, new org.netbeans.lib.awtextra.AbsoluteConstraints(5, 2, 319, 32));

        naissance.setDateFormatString("EEEE dd MMM yyyy");
        naissance.setFont(new java.awt.Font("Tahoma", 1, 12));
        naissance.setName("naissance"); // NOI18N
        naissance.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                naissanceFocusLost(evt);
            }
        });
        jPanel5.add(naissance, new org.netbeans.lib.awtextra.AbsoluteConstraints(5, 40, 319, 38));
        naissance.getAccessibleContext().setAccessibleName("naissance");

        sante.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        sante.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Selectionnez", "Allergique", "Non Allergique", "Malaria", "Typhoides", "Typho-malaria", "Autres" }));
        sante.setName("sante"); // NOI18N
        jPanel5.add(sante, new org.netbeans.lib.awtextra.AbsoluteConstraints(5, 84, 319, 37));
        sante.getAccessibleContext().setAccessibleName("sante");

        responsable.setDescripcion("Entrer le nom du parent ici");
        responsable.setName("responsable"); // NOI18N
        responsable.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                responsableMouseClicked(evt);
            }
        });
        jPanel5.add(responsable, new org.netbeans.lib.awtextra.AbsoluteConstraints(5, 133, 319, 32));
        responsable.getAccessibleContext().setAccessibleName("responsable");

        telparent.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        telparent.setName("telparent"); // NOI18N
        jPanel5.add(telparent, new org.netbeans.lib.awtextra.AbsoluteConstraints(6, 177, 318, 35));
        telparent.getAccessibleContext().setAccessibleName("telparent");

        lien.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lien.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Selectionnez", "Père", "Mère", "Soeur", "Frères", "Cousin", "Cousine", "Oncle", "Tante", "Voisin", "Voisine", "Amis", "Autres" }));
        lien.setName("lien"); // NOI18N
        jPanel5.add(lien, new org.netbeans.lib.awtextra.AbsoluteConstraints(5, 218, 241, 43));
        lien.getAccessibleContext().setAccessibleName("lien");

        estatus.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        estatus.setText("Status");
        estatus.setName("estatus"); // NOI18N
        estatus.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                estatusItemStateChanged(evt);
            }
        });
        jPanel5.add(estatus, new org.netbeans.lib.awtextra.AbsoluteConstraints(254, 218, -1, 43));

        jPanel1.add(jPanel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(530, 10, 340, 270));

        jPanel8.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        jPanel8.setLayout(new java.awt.BorderLayout());

        photo.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        photo.setText("Double click pour Telecharger une Photo");
        photo.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        photo.setComponentPopupMenu(jPopupMenu1);
        photo.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                photoMouseClicked(evt);
            }
        });
        jPanel8.add(photo, java.awt.BorderLayout.CENTER);

        jPanel1.add(jPanel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(880, 20, 270, 240));

        jPanel7.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel7.setLayout(new javax.swing.BoxLayout(jPanel7, javax.swing.BoxLayout.LINE_AXIS));

        tbleleve.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane2.setViewportView(tbleleve);

        jPanel7.add(jScrollPane2);

        jMenuBar1.setBackground(new java.awt.Color(102, 102, 102));

        jMenu1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/fichier.jpg"))); // NOI18N
        jMenu1.setText("Action");
        jMenu1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N

        enregistrer.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_E, java.awt.event.InputEvent.CTRL_MASK));
        enregistrer.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/icons8-save_close.png"))); // NOI18N
        enregistrer.setText("Enrégistrer");
        enregistrer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                enregistrerActionPerformed(evt);
            }
        });
        jMenu1.add(enregistrer);

        nouveau.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_N, java.awt.event.InputEvent.CTRL_MASK));
        nouveau.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/icons8-add_list.png"))); // NOI18N
        nouveau.setText("Nouveau");
        nouveau.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nouveauActionPerformed(evt);
            }
        });
        jMenu1.add(nouveau);

        effacer.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_D, java.awt.event.InputEvent.CTRL_MASK));
        effacer.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/icons8-erase.png"))); // NOI18N
        effacer.setText("Effacer");
        effacer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                effacerActionPerformed(evt);
            }
        });
        jMenu1.add(effacer);

        jMenuBar1.add(jMenu1);

        jMenu2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/editer.jpg"))); // NOI18N
        jMenu2.setText("Edition");
        jMenu2.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N

        modifier.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_M, java.awt.event.InputEvent.CTRL_MASK));
        modifier.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/icons8-refresh.png"))); // NOI18N
        modifier.setText("Modifier");
        modifier.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                modifierActionPerformed(evt);
            }
        });
        jMenu2.add(modifier);

        jMenuItem1.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F1, 0));
        jMenuItem1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/icons8-help.png"))); // NOI18N
        jMenuItem1.setText("Aide");
        jMenu2.add(jMenuItem1);

        jMenuBar1.add(jMenu2);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 1163, Short.MAX_VALUE)
            .addComponent(jPanel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, 252, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void nouveauActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nouveauActionPerformed
        JTextField[] txt = {code, prenom};
//        Mn.NEW_BUTTON(txt, "eleve", "code_eleve");
        Nettoyer();
    }//GEN-LAST:event_nouveauActionPerformed

    private void enregistrerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_enregistrerActionPerformed

        aaa = Mn.MAXCODE("code_eleve", "eleve");
        String bbb = matricules.getText();
        String ccc = "";
        String ddd = sante.getSelectedItem().toString();
        String fff = responsable.getText();
        String ggg = telparent.getText();
        String hhh = lien.getSelectedItem().toString();
        String iii = Mn.DATEHOURNOW();
        String jjj = Mn.INSERT_CHECKED(estatus);
        String pas = sc.password();
        String email = "";
        
        //eleves
        String query = "call insert_eleve ("+aaa+",'"+bbb+"',"+Variables.code+",'"+Variables.code_terceross+"','"+pas+"','"+ddd+"',"+jjj+" )";
        System.out.println("query "+query);
        Mn.MANIPULEDB(query);
        
        //eleve responsables
        query = "call insert_responsables ('"+bbb+"',"+Variables.codresp+",'"+Variables.codter_resp+"','"+hhh+"') ";
        System.out.println("qu "+query);
        Mn.MANIPULEDB(query);
        
       //eleves_has institution
        int idi = 0;
        query = "select idinstitution from institution where nom = '"+Variables.Empresa+"' ";
        ResultSet rsg = Mn.Rezulta(query);
        try {
            if(rsg.last()){
                idi = rsg.getInt("idinstitution");
            }
        } catch (SQLException ex) {
            Logger.getLogger(Eleves.class.getName()).log(Level.SEVERE, null, ex);
        }

        query = "call eleve_has_institution_procedure ('"+bbb+"',"+Variables.code+",'"+Variables.code_terceross+"','"+idi+"') ";
        System.out.println("institution : "+query);
        Mn.MANIPULEDB(query);
        
        Consulter();
        nouveau.doClick();
    }//GEN-LAST:event_enregistrerActionPerformed

    private void effacerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_effacerActionPerformed
        if (prenom.getText().isEmpty()) {
            JOptionPane.showConfirmDialog(null, "Désolé! fait double clique sur l'élève dans la table ci-dessous pour éffacer.");
        } else {
            String query = "delete from tercero where code_tercero='" + coode.getText() + "'";
            Mn.MANIPULEDB(query);
            Mn.DELETE_BUTTON(code, "eleve", "code_eleve");
        }
        Consulter();
        nouveau.doClick();
    }//GEN-LAST:event_effacerActionPerformed

    private void modifierActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_modifierActionPerformed
        int aa = Integer.parseInt(coode.getText());
        String bb = prenom.getText();
        String cc = nom.getText();
        String dd = adresse.getText();
        String ee = teleleve.getText();
        String ff = sexe.getSelectedItem().toString();
//        String gg=ReturnIstitution();
        String query = "call Insert_tercero(" + aa + ",'" + cc + "','" + bb + "','" + ff + "','" + dd + "','" + ee + "','" + Variables.Empresa + "')";
        Mn.MANIPULEDB(query);
        //-----------------------------------------------------------------------------
        int aaa = Integer.parseInt(code.getText());
        String bbb = matricules.getText();
        String ccc = Mn.INSERTDATECHOOSER(naissance);
        String ddd = sante.getSelectedItem().toString();
        String fff = responsable.getText();
        String ggg = telparent.getText();
        String hhh = lien.getSelectedItem().toString();
        String iii = Mn.DATEHOURNOW();
        String jjj = Mn.INSERT_CHECKED(estatus);
        String pas = sc.password();
        String email = "";
        query = "call Insert_eleve(" + aaa + "," + aa + ",'" + bbb + "','" + email + "','" + pas + "','" + ccc + "','" + ddd + "','" + fff + "','" + ggg + "','" + hhh + "','" + iii + "'," + jjj + ")";
        //System.out.println(query);
        Mn.MODIFIER(query);
        String san = sanguins.getSelectedItem().toString();
        Mn.MANIPULEDB("call Insert_sanguins (" + aa + ",'" + san + "')");
        Consulter();
        nouveau.doClick();
    }//GEN-LAST:event_modifierActionPerformed

    private void naissanceFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_naissanceFocusLost


    }//GEN-LAST:event_naissanceFocusLost

    private void rechercheKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_rechercheKeyReleased
        search();
    }//GEN-LAST:event_rechercheKeyReleased

    private void prenomKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_prenomKeyReleased
        autoCompleteEvent(evt, prenom);
    }//GEN-LAST:event_prenomKeyReleased

    private void nomKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_nomKeyReleased
        autoCompleteEvent(evt, nom);
    }//GEN-LAST:event_nomKeyReleased

    private void adresseKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_adresseKeyReleased
        autoCompleteEvent(evt, adresse);
    }//GEN-LAST:event_adresseKeyReleased

    private void photoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_photoMouseClicked
//        if(evt.getClickCount() == 2){
//            Mn.Photos(photo, matricules.getText().trim());
//        }
    }//GEN-LAST:event_photoMouseClicked

    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem2ActionPerformed
//        Mn.Photos(photo, matricules.getText().trim());
    }//GEN-LAST:event_jMenuItem2ActionPerformed

    private void jMenuItem3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem3ActionPerformed
        photo.setIcon(null);
        photo.updateUI();
        photoNull();
    }//GEN-LAST:event_jMenuItem3ActionPerformed

    private void prenomMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_prenomMouseClicked
        if(evt.getClickCount() == 2){
            consult_no_eleves cp = new consult_no_eleves(null, true);
        cp.show();
        cp.pack();
        code.setText(Variables.code_tercero);
            System.out.println("code "+Variables.code_tercero);
            cod = Variables.code;
            codter = Variables.code_terceross;
        focusLost();
        }
    }//GEN-LAST:event_prenomMouseClicked

    private void nomMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_nomMouseClicked
        prenomMouseClicked(evt);
    }//GEN-LAST:event_nomMouseClicked

    private void responsableMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_responsableMouseClicked
        if(evt.getClickCount() == 2){
            try {
                
                consultPerson_no_eleve cp = new consultPerson_no_eleve(null, true);
                cp.show();
                cp.pack();
                if(code.getText().trim().equals(Variables.code_terceros_resp)){
                    JOptionPane.showMessageDialog(this, "Vous ne pouvez pas choisir la même personne pour être responsable");
                    return;
                }
                else{
                responsable.setText(Variables.nomReponsab);
                //tel
                String sql = "SELECT phone_number FROM show_telephones where code = '"+Variables.code_terceros_resp+"' ";
                    System.out.println("Terceros : "+sql);
                ResultSet rst = Mn.Rezulta(sql);
                if (rst.first()) {
                    telparent.setText(rst.getString("phone_number"));
                }
        
//        focusLost();
                }
            } catch (SQLException ex) {
                Logger.getLogger(Eleves.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }//GEN-LAST:event_responsableMouseClicked

    private void prenomActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_prenomActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_prenomActionPerformed

    private void estatusItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_estatusItemStateChanged
        if(estatus.isSelected()){
            estatus.setText("Active");
        }
        else{
            estatus.setText("Inactive");
        }
    }//GEN-LAST:event_estatusItemStateChanged

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Eleves.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Eleves.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Eleves.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Eleves.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Eleves().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private org.edisoncor.gui.textField.TextFieldRectBackground adresse;
    private javax.swing.JMenuItem effacer;
    private javax.swing.JMenuItem enregistrer;
    private javax.swing.JCheckBox estatus;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JMenuItem jMenuItem3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPopupMenu jPopupMenu1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JPopupMenu.Separator jSeparator1;
    private javax.swing.JComboBox lien;
    private org.edisoncor.gui.textField.TextFieldRectBackground matricules;
    private javax.swing.JMenuItem modifier;
    private com.toedter.calendar.JDateChooser naissance;
    private org.edisoncor.gui.textField.TextFieldRectBackground nom;
    private javax.swing.JMenuItem nouveau;
    private javax.swing.JLabel photo;
    private org.edisoncor.gui.textField.TextFieldRectBackground prenom;
    private org.edisoncor.gui.textField.TextFieldRectBackground recherche;
    private org.edisoncor.gui.textField.TextFieldRectBackground responsable;
    private javax.swing.JComboBox sanguins;
    private javax.swing.JComboBox sante;
    private javax.swing.JComboBox sexe;
    private javax.swing.JTable tbleleve;
    private javax.swing.JFormattedTextField teleleve;
    private javax.swing.JFormattedTextField telparent;
    // End of variables declaration//GEN-END:variables

    private void search() {
        String query = "select code,nom,prenom,matricule,responsable,lien_parente as lien, institution \n" +
"from show_eleves where institution ='" + Variables.Empresa + "' and nom like '%" + recherche.getText() + "%' or prenom like '%" + recherche.getText() + "%'"
                + " or matricule like '%" + recherche.getText() + "%' or responsable like '%" + recherche.getText() + "%'"
                + " order by code desc";
        ResultSet rs = Mn.SEARCHDATA(query);
        try {
            md.setRowCount(0);
            while (rs.next()) {
                String[] vect = {rs.getString("nom"), rs.getString("prenom"), rs.getString("matricule"),
                    rs.getString("responsable"), null, rs.getString("lien")};
                Mn.ADD_TABLE(md, vect);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Entrer_note.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
