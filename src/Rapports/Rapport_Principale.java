
package Rapports;
import Start.*;
import Rapports.*;
import Start.*;
import Start.*;
import Methods.Manipulation;
import Methods.Rapport;
import Methods.Theme;
import Methods.Variables;
import java.awt.Color;
import java.awt.Component;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultListModel;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;

public class Rapport_Principale extends javax.swing.JInternalFrame {
Manipulation Mn= new Manipulation();
Theme th= new Theme();
Rapport Rp=new Rapport();
String [][] data={};
String[] cabez={"RAPPORTS DYNAMIQUES"};
String[] cabez1={"MODELES DES LETTRES"};
    DefaultTableModel md = new DefaultTableModel(data, cabez);
    DefaultTableModel mdd = new DefaultTableModel(data, cabez1);
    Waitt attend= new Waitt(null, rootPaneCheckingEnabled);
    String valeur="";
    public Rapport_Principale() {
        initComponents();
        this.setTitle("Scolaris: "+Variables.Empresa+" : Rapport Intelligent");
         tblmodel.setModel(mdd);
         tblrapport.setModel(md);
         AddList();
         Mn.FULLCOMBO(academique,"annee", "annee_academique");
         Mn.FULLCOMBO(klas, "description","section",section.getSelectedItem().toString(), "classe");  
    }

    private void AddList(){
     String[] vect={"Bulletins","Borderaux","Classes","Collections","Couverture des Bulletins","Elèves","Employers","Feuille de Présence","Formulaire d'inscription",
         "Horaires des Classes","Horaires des Professeurs","Inscriptions","Lettres","Matières","Notification","Nouveaux(elles)Elèves","Paiements","Professeurs",
         "Resultats","Resultats Nouveaux(elles)Elèves"};
       String[] fila = new String[1];
          for(int i=0;i<vect.length;i++){
            for(int j=0;j<1;j++){
              fila[j]=vect[i];
          }
            md.addRow(fila);
          }
                
         matricule.setEnabled(false);
         section.setEnabled(false);
         klas.setEnabled(false);
         de.setEnabled(false);
         ha.setEnabled(false);
         controle.setEnabled(false);
         matriculeobligatoria.setVisible(false);
         classeobligatoria.setVisible(false);
         deobligatoria.setVisible(false);
         aobligatoria.setVisible(false);
         controleobligatoria.setVisible(false);
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jSeparator1 = new javax.swing.JSeparator();
        jPanel1 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        de = new org.edisoncor.gui.textField.TextFieldRectBackground();
        jLabel8 = new javax.swing.JLabel();
        ha = new org.edisoncor.gui.textField.TextFieldRectBackground();
        matricule = new javax.swing.JFormattedTextField();
        jLabel10 = new javax.swing.JLabel();
        jSeparator2 = new javax.swing.JSeparator();
        academique = new javax.swing.JComboBox();
        section = new javax.swing.JComboBox();
        klas = new javax.swing.JComboBox();
        controle = new javax.swing.JComboBox();
        jSeparator3 = new javax.swing.JSeparator();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblmodel = new javax.swing.JTable(){
            public boolean isCellEditable(int rowIndex, int colIndex){
                return false;
            }
        };
        matriculeobligatoria = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        classeobligatoria = new javax.swing.JLabel();
        controleobligatoria = new javax.swing.JLabel();
        deobligatoria = new javax.swing.JLabel();
        aobligatoria = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tblrapport = new javax.swing.JTable(){
            public boolean isCellEditable(int rowIndex, int colIndex){
                return false;
            }
        };
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        imprimer = new javax.swing.JMenuItem();
        sortir1 = new javax.swing.JMenuItem();
        jMenu5 = new javax.swing.JMenu();
        jMenuItem1 = new javax.swing.JMenuItem();

        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel5.setText("Controle");

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel6.setText("Section ");

        jLabel9.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel9.setText("Année Académique");

        jLabel7.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel7.setText("Type");

        jLabel8.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel8.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel8.setText("Type");

        matricule.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        matricule.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                matriculeFocusLost(evt);
            }
        });
        matricule.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                matriculeKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                matriculeKeyTyped(evt);
            }
        });

        jLabel10.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel10.setText("Matricule");

        academique.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N

        section.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        section.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Selectionnez", "Petit", "Primaire", "Secondaire" }));
        section.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                sectionItemStateChanged(evt);
            }
        });
        section.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                sectionFocusLost(evt);
            }
        });

        klas.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        klas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                klasActionPerformed(evt);
            }
        });

        controle.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        controle.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Selectionnez", "1", "2", "3" }));

        tblmodel.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tblmodel);

        matriculeobligatoria.setBackground(new java.awt.Color(255, 255, 255));
        matriculeobligatoria.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/aceptar.png"))); // NOI18N

        jLabel2.setBackground(new java.awt.Color(255, 255, 255));
        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/aceptar.png"))); // NOI18N

        classeobligatoria.setBackground(new java.awt.Color(255, 255, 255));
        classeobligatoria.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/aceptar.png"))); // NOI18N

        controleobligatoria.setBackground(new java.awt.Color(255, 255, 255));
        controleobligatoria.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/aceptar.png"))); // NOI18N

        deobligatoria.setBackground(new java.awt.Color(255, 255, 255));
        deobligatoria.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/aceptar.png"))); // NOI18N

        aobligatoria.setBackground(new java.awt.Color(255, 255, 255));
        aobligatoria.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/aceptar.png"))); // NOI18N

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/stop_icon.png"))); // NOI18N

        jLabel3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/stop_icon.png"))); // NOI18N

        jLabel4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/stop_icon.png"))); // NOI18N

        jLabel11.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/stop_icon.png"))); // NOI18N

        jLabel12.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/stop_icon.png"))); // NOI18N

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSeparator2)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel10)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(matricule, javax.swing.GroupLayout.PREFERRED_SIZE, 213, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(matriculeobligatoria)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel1)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(de, javax.swing.GroupLayout.DEFAULT_SIZE, 410, Short.MAX_VALUE)
                            .addComponent(ha, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(aobligatoria)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jLabel12))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(deobligatoria)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jLabel11)))
                        .addGap(23, 23, 23))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel5)
                            .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(controle, 0, 216, Short.MAX_VALUE)
                            .addComponent(section, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(controleobligatoria)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(klas, javax.swing.GroupLayout.PREFERRED_SIZE, 201, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(classeobligatoria)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel4)))
                        .addContainerGap())))
            .addComponent(jSeparator3)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 572, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(academique, javax.swing.GroupLayout.PREFERRED_SIZE, 214, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel3)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(4, 4, 4)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(academique, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 14, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(13, 13, 13)))
                .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 6, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(matricule, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel10))
                    .addComponent(matriculeobligatoria, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel6)
                        .addComponent(section, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(klas, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(classeobligatoria, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(11, 11, 11)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(controleobligatoria, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(controle, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel5)))
                .addGap(20, 20, 20)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(de, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel7))
                    .addComponent(deobligatoria)
                    .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(ha, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel8))
                            .addComponent(jLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jSeparator3, javax.swing.GroupLayout.PREFERRED_SIZE, 2, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 226, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(aobligatoria)
                        .addGap(0, 0, Short.MAX_VALUE))))
        );

        tblrapport.setBackground(new java.awt.Color(255, 255, 204));
        tblrapport.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        tblrapport.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tblrapport.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblrapportMouseClicked(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                tblrapportMousePressed(evt);
            }
        });
        jScrollPane2.setViewportView(tblrapport);

        jMenu1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/editer.jpg"))); // NOI18N
        jMenu1.setText("Action");
        jMenu1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N

        imprimer.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_P, java.awt.event.InputEvent.CTRL_MASK));
        imprimer.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/Imprimir.png"))); // NOI18N
        imprimer.setText("Imprimer");
        imprimer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                imprimerActionPerformed(evt);
            }
        });
        jMenu1.add(imprimer);

        sortir1.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_S, java.awt.event.InputEvent.CTRL_MASK));
        sortir1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/salir.png"))); // NOI18N
        sortir1.setText("Annuler /Quitter");
        sortir1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sortir1ActionPerformed(evt);
            }
        });
        jMenu1.add(sortir1);

        jMenuBar1.add(jMenu1);

        jMenu5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/editer.jpg"))); // NOI18N
        jMenu5.setText("Edition");
        jMenu5.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N

        jMenuItem1.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F1, 0));
        jMenuItem1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/Ayuda.jpg"))); // NOI18N
        jMenuItem1.setText("Aide");
        jMenu5.add(jMenuItem1);

        jMenuBar1.add(jMenu5);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 244, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 523, Short.MAX_VALUE)
            .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void matriculeFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_matriculeFocusLost

    }//GEN-LAST:event_matriculeFocusLost

    private void matriculeKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_matriculeKeyReleased
        matricule.setText(matricule.getText().toUpperCase());
    }//GEN-LAST:event_matriculeKeyReleased

    private void matriculeKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_matriculeKeyTyped
//        Mn.ONLYNUMBERS(evt);
        matricule.requestFocus();
    }//GEN-LAST:event_matriculeKeyTyped

    private void sortir1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_sortir1ActionPerformed
       int aa=JOptionPane.showConfirmDialog(null,"Etes-vous sur de vouloir quitter cette page?","Question", JOptionPane.YES_OPTION);
        if(aa==JOptionPane.YES_OPTION){
            this.dispose();
        }else{
         tblrapport.setModel(md);
         matricule.setEnabled(false);
         section.setEnabled(false);
         klas.setEnabled(false);
         de.setEnabled(false);
         ha.setEnabled(false);
         controle.setEnabled(false);
        }
    }//GEN-LAST:event_sortir1ActionPerformed

    private void imprimerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_imprimerActionPerformed
        if(tblrapport.getSelectedRow()<0){
            JOptionPane.showMessageDialog(this,"Veuillez séléctionner une ligne dans la table de gauche. ");
        }else if(valeur.equals("Formulaire d'inscription")){
            attend.show();
           Rp.SENDQUERY("Feuille_inscription");
        }else if(valeur.equals("Couverture des Bulletins")){
            attend.show();
           Rp.SENDQUERY("Couverture");
        }else if(valeur.equals("Feuille de Présence")){
            attend.show();
           Rp.SENDQUERY("Presence",klas.getSelectedItem().toString(),academique.getSelectedItem().toString(),Variables.Empresa);
        }else if(valeur.equals("Bulletins")){
            attend.show();
           Rp.SENDQUERY("Bulletin",matricule.getText(),academique.getSelectedItem().toString(),
                   Integer.parseInt(controle.getSelectedItem().toString()),Variables.Empresa);  
        }else if(valeur.equals("Collections")){
            if(klas.getSelectedItem().equals("Selectionnez") ||klas.getSelectedItem().equals(null) ){
                attend.show();
                Rp.SENDQUERY("Collection",matricule.getText(),academique.getSelectedItem().toString(),Variables.Empresa);  
            }else{
                attend.show();
               Rp.SENDQUERY("Collecte",klas.getSelectedItem().toString(),academique.getSelectedItem().toString(),Variables.Empresa);
            }
          
        }else if(valeur.equals("Elèves")){
            if(matricule.getText().isEmpty()){
             attend.show();
             Rp.SENDQUERY("Eleves_1",klas.getSelectedItem().toString(),academique.getSelectedItem().toString(),Variables.Empresa);   
            }else{
            attend.show();
            Rp.SENDQUERYS("Eleves",matricule.getText(),Variables.Empresa);
            }
        }else if(valeur.equals("Classes")){
            attend.show();
           Rp.SENDQUERY("classe",klas.getSelectedItem().toString(),academique.getSelectedItem().toString(),Variables.Empresa);
        }else if(valeur.equals("Employers")){
            if(de.getText().isEmpty() && ha.getText().isEmpty()){
            String query="select t.nom,t.prenom,e.document,e.matricule,t.adresse,t.telephone,e.mail,e.estatuses\n" +
            ",i.nom as institution,i.direccion,i.telephone as tel,i.e_mail as email,e.document,e.type_document \n" +
            "as tpdocument,e.naissance,etat_civil,t.sexe ,et.type_employer as tpemployer from employer e \n" +
            "left join tercero t on t.code_tercero=e.code_tercero left join institution i on \n" +
            "i.nom=t.institution left join employer_vs_type et on et.code_employer=e.code_employer"
                    + " where e.matricule='"+matricule.getText()+"' and t.institution='"+Variables.Empresa+"' group by matricule";
            attend.show();
            Rp.SENDQUERY(query,"Employer");
            }else   if(de.getText()!=null && ha.getText().isEmpty()){
            String query="select t.nom,t.prenom,e.document,e.matricule,t.adresse,t.telephone,e.mail,e.estatuses\n" +
            ",i.nom as institution,i.direccion,i.telephone as tel,i.e_mail as email,e.document,e.type_document \n" +
            "as tpdocument,e.naissance,etat_civil,t.sexe ,et.type_employer as tpemployer from employer e \n" +
            "left join tercero t on t.code_tercero=e.code_tercero left join institution i on \n" +
            "i.nom=t.institution left join employer_vs_type et on et.code_employer=e.code_employer"
            + " where et.type_employer='"+de.getText()+"' and t.institution='"+Variables.Empresa+"' group by nom";
            attend.show();
            Rp.SENDQUERY(query,"Employer");
            }else   if(de.getText().isEmpty() && ha.getText()!=null){
            String query="select t.nom,t.prenom,e.document,e.matricule,t.adresse,t.telephone,e.mail,e.estatuses\n" +
            ",i.nom as institution,i.direccion,i.telephone as tel,i.e_mail as email,e.document,e.type_document \n" +
            "as tpdocument,e.naissance,etat_civil,t.sexe ,et.type_employer as tpemployer from employer e \n" +
            "left join tercero t on t.code_tercero=e.code_tercero left join institution i on \n" +
            "i.nom=t.institution left join employer_vs_type et on et.code_employer=e.code_employer"
            + " where et.type_employer='"+ha.getText()+"' and t.institution='"+Variables.Empresa+"' group by nom";
            attend.show();
            Rp.SENDQUERY(query,"Employer");
            }else   if(de.getText()!=null && ha.getText()!=null){
            String query="select t.nom,t.prenom,e.document,e.matricule,t.adresse,t.telephone,e.mail,e.estatuses\n" +
            ",i.nom as institution,i.direccion,i.telephone as tel,i.e_mail as email,e.document,e.type_document \n" +
            "as tpdocument,e.naissance,etat_civil,t.sexe ,et.type_employer as tpemployer from employer e \n" +
            "left join tercero t on t.code_tercero=e.code_tercero left join institution i on \n" +
            "i.nom=t.institution left join employer_vs_type et on et.code_employer=e.code_employer"
            + " where et.type_employer ='"+de.getText()+"' or et.type_employer ='"+ha.getText()+"' and t.institution='"+Variables.Empresa+"' group by nom";
            attend.show();
            Rp.SENDQUERY(query,"Employer");
            }
            
        }else if(valeur.equals("Borderaux")){
            attend.show();
           Rp.SENDQUERYS("Bordereaux",klas.getSelectedItem().toString(),Variables.Empresa);
        }else if(valeur.equals("Inscriptions")){
            if(matricule.getText().isEmpty() && !klas.getSelectedItem().toString().equals("Selectionnez")){
               attend.show();
              Rp.SENDQUERY("Inscris",klas.getSelectedItem().toString(),academique.getSelectedItem().toString(),Variables.Empresa);  
            }else if(matricule.getText().isEmpty() && klas.getSelectedItem().toString().equals("Selectionnez")){
               attend.show();
              Rp.SENDQUERYS("Inscris_1",academique.getSelectedItem().toString(),Variables.Empresa);  
            }else{
              attend.show();
              Rp.SENDQUERY("Inscription",matricule.getText(),academique.getSelectedItem().toString(),Variables.Empresa); 
            }
        }else if(valeur.equals("Matières")){
            attend.show();
           Rp.SENDQUERY("Matiere",klas.getSelectedItem().toString(),section.getSelectedItem().toString(),Variables.Empresa);
        }else if(valeur.equals("Paiements")){
         if(de.getText().isEmpty() && ha.getText().isEmpty()){
             attend.show();
            Rp.SENDQUERY("Paiements",matricule.getText(),academique.getSelectedItem().toString(),Variables.Empresa);
            }else{
             attend.show();
             Rp.SENDQUERYS("Paiement",Variables.Empresa);
         }
        }else if(valeur.equals("Professeurs")){
            attend.show();
           Rp.SENDQUERYS("Professeur",matricule.getText(),Variables.Empresa);
        }else if(valeur.equals("Resultats")){
            attend.show();
          Rp.SENDQUERY("Resultat",klas.getSelectedItem().toString(),academique.getSelectedItem().toString(),
                  Integer.parseInt(controle.getSelectedItem().toString()),Variables.Empresa);
        }
        
    }//GEN-LAST:event_imprimerActionPerformed

    private void sectionFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_sectionFocusLost
       
    }//GEN-LAST:event_sectionFocusLost

    private void tblrapportMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblrapportMousePressed
     valeur=tblrapport.getValueAt(tblrapport.getSelectedRow(),0).toString();
        if(valeur.equals("Matières")){
         section.setEnabled(true);
         klas.setEnabled(true);
         matricule.setEnabled(false);
         de.setEnabled(false);
         ha.setEnabled(false);
         controle.setEnabled(false);
         mdd.setRowCount(0);
         matriculeobligatoria.setVisible(false);
         classeobligatoria.setVisible(true);
         deobligatoria.setVisible(false);
         aobligatoria.setVisible(false);
         controleobligatoria.setVisible(false);
      }else if(valeur.equals("Classes")){
         section.setEnabled(true);
         klas.setEnabled(true);
         matricule.setEnabled(false);
         de.setEnabled(false);
         ha.setEnabled(false);
         controle.setEnabled(false);
         mdd.setRowCount(0);
         matriculeobligatoria.setVisible(false);
         classeobligatoria.setVisible(true);
         deobligatoria.setVisible(false);
         aobligatoria.setVisible(false);
         controleobligatoria.setVisible(false);
      }else if(valeur.equals("Borderaux")){
         section.setEnabled(true);
         klas.setEnabled(true);
         matricule.setEnabled(false);
         de.setEnabled(false);
         ha.setEnabled(false);
         controle.setEnabled(false); 
         mdd.setRowCount(0);
         matriculeobligatoria.setVisible(false);
         classeobligatoria.setVisible(true);
         deobligatoria.setVisible(false);
         aobligatoria.setVisible(false);
         controleobligatoria.setVisible(false);
      }else if(valeur.equals("Bulletins")){
         section.setEnabled(true);
         klas.setEnabled(true);
         matricule.setEnabled(true);
         de.setEnabled(false);
         ha.setEnabled(false);
         controle.setEnabled(true); 
         mdd.setRowCount(0);
         matriculeobligatoria.setVisible(true);
         classeobligatoria.setVisible(true);
         deobligatoria.setVisible(false);
         aobligatoria.setVisible(false);
         controleobligatoria.setVisible(true);
      }else if(valeur.equals("Collections")){
         section.setEnabled(true);
         klas.setEnabled(true);
         matricule.setEnabled(true);
         de.setEnabled(false);
         ha.setEnabled(false);
         controle.setEnabled(false); 
         mdd.setRowCount(0);
         matriculeobligatoria.setVisible(true);
         classeobligatoria.setVisible(true);
         deobligatoria.setVisible(false);
         aobligatoria.setVisible(false);
         controleobligatoria.setVisible(false);
      }else if(valeur.equals("Elèves")){
         section.setEnabled(true);
         klas.setEnabled(true);
         matricule.setEnabled(true);
         de.setEnabled(false);
         ha.setEnabled(false);
         controle.setEnabled(false); 
         mdd.setRowCount(0);
         matriculeobligatoria.setVisible(true);
         classeobligatoria.setVisible(true);
         deobligatoria.setVisible(false);
         aobligatoria.setVisible(false);
         controleobligatoria.setVisible(false);
      }else if(valeur.equals("Employers")){
         section.setEnabled(false);
         klas.setEnabled(false);
         matricule.setEnabled(false);
         de.setEnabled(true);
         ha.setEnabled(true);
         controle.setEnabled(false); 
         mdd.setRowCount(0);
         matriculeobligatoria.setVisible(false);
         classeobligatoria.setVisible(false);
         deobligatoria.setVisible(true);
         aobligatoria.setVisible(true);
         controleobligatoria.setVisible(false);
      }else if(valeur.equals("Feuille de Présence")){
         section.setEnabled(true);
         klas.setEnabled(true);
         matricule.setEnabled(false);
         de.setEnabled(false);
         ha.setEnabled(false);
         controle.setEnabled(false); 
         mdd.setRowCount(0);
         matriculeobligatoria.setVisible(false);
         classeobligatoria.setVisible(true);
         deobligatoria.setVisible(false);
         aobligatoria.setVisible(false);
         controleobligatoria.setVisible(false);
      }else if(valeur.equals("Formulaire d'inscription")){
         section.setEnabled(false);
         klas.setEnabled(false);
         matricule.setEnabled(false);
         de.setEnabled(false);
         ha.setEnabled(false);
         controle.setEnabled(false); 
         mdd.setRowCount(0);
         matriculeobligatoria.setVisible(false);
         classeobligatoria.setVisible(false);
         deobligatoria.setVisible(false);
         aobligatoria.setVisible(false);
         controleobligatoria.setVisible(false);
      }else if(valeur.equals("Horaires des Classes")){
         section.setEnabled(true);
         klas.setEnabled(true);
         matricule.setEnabled(false);
         de.setEnabled(false);
         ha.setEnabled(false);
         controle.setEnabled(false); 
         mdd.setRowCount(0);
         matriculeobligatoria.setVisible(false);
         classeobligatoria.setVisible(true);
         deobligatoria.setVisible(false);
         aobligatoria.setVisible(false);
         controleobligatoria.setVisible(false);
      }else if(valeur.equals("Horaires des Professeurs")){
         section.setEnabled(false);
         klas.setEnabled(false);
         matricule.setEnabled(true);
         de.setEnabled(false);
         ha.setEnabled(false);
         controle.setEnabled(false); 
         mdd.setRowCount(0);
         matriculeobligatoria.setVisible(true);
         classeobligatoria.setVisible(false);
         deobligatoria.setVisible(false);
         aobligatoria.setVisible(false);
         controleobligatoria.setVisible(false);
      }else if(valeur.equals("Inscriptions")){
         section.setEnabled(true);
         klas.setEnabled(true);
         matricule.setEnabled(true);
         de.setEnabled(false);
         ha.setEnabled(false);
         controle.setEnabled(false); 
         mdd.setRowCount(0);
         matriculeobligatoria.setVisible(true);
         classeobligatoria.setVisible(true);
         deobligatoria.setVisible(false);
         aobligatoria.setVisible(false);
         controleobligatoria.setVisible(false);
      }else if(valeur.equals("Notification")){
         section.setEnabled(true);
         klas.setEnabled(true);
         matricule.setEnabled(false);
         de.setEnabled(false);
         ha.setEnabled(false);
         controle.setEnabled(false); 
         mdd.setRowCount(0);
         matriculeobligatoria.setVisible(false);
         classeobligatoria.setVisible(true);
         deobligatoria.setVisible(false);
         aobligatoria.setVisible(false);
         controleobligatoria.setVisible(false);
      }else if(valeur.equals("Nouveaux(elles)Elèves")){
         section.setEnabled(true);
         klas.setEnabled(true);
         matricule.setEnabled(false);
         de.setEnabled(false);
         ha.setEnabled(false);
         controle.setEnabled(false); 
         mdd.setRowCount(0);
         matriculeobligatoria.setVisible(false);
         classeobligatoria.setVisible(true);
         deobligatoria.setVisible(false);
         aobligatoria.setVisible(false);
         controleobligatoria.setVisible(false);
      }else if(valeur.equals("Paiements")){
         section.setEnabled(true);
         klas.setEnabled(true);
         matricule.setEnabled(true);
         de.setEnabled(false);
         ha.setEnabled(false);
         controle.setEnabled(false); 
         mdd.setRowCount(0);
         matriculeobligatoria.setVisible(true);
         classeobligatoria.setVisible(true);
         deobligatoria.setVisible(false);
         aobligatoria.setVisible(false);
         controleobligatoria.setVisible(false);
      }else if(valeur.equals("Professeurs")){
         section.setEnabled(false);
         klas.setEnabled(false);
         matricule.setEnabled(true);
         de.setEnabled(false);
         ha.setEnabled(false);
         controle.setEnabled(false); 
         mdd.setRowCount(0);
         matriculeobligatoria.setVisible(true);
         classeobligatoria.setVisible(false);
         deobligatoria.setVisible(false);
         aobligatoria.setVisible(false);
         controleobligatoria.setVisible(false);
      }else if(valeur.equals("Resultats")){
         section.setEnabled(true);
         klas.setEnabled(true);
         matricule.setEnabled(false);
         de.setEnabled(false);
         ha.setEnabled(false);
         controle.setEnabled(true); 
         mdd.setRowCount(0);
         matriculeobligatoria.setVisible(false);
         classeobligatoria.setVisible(true);
         deobligatoria.setVisible(false);
         aobligatoria.setVisible(false);
         controleobligatoria.setVisible(true);
      }else if(valeur.equals("Resultats Nouveaux(elles)Elèves")){
         section.setEnabled(true);
         klas.setEnabled(true);
         matricule.setEnabled(false);
         de.setEnabled(false);
         ha.setEnabled(false);
         controle.setEnabled(false); 
         mdd.setRowCount(0);
         matriculeobligatoria.setVisible(true);
         classeobligatoria.setVisible(true);
         deobligatoria.setVisible(false);
         aobligatoria.setVisible(false);
         controleobligatoria.setVisible(false);
      }else if(valeur.equals("Lettres")){
          String query="select objectif from lettres where institution='"+Variables.Empresa+"'";
          Mn.FULLTABLE(query, tblmodel, mdd);
      }
    }//GEN-LAST:event_tblrapportMousePressed

    private void klasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_klasActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_klasActionPerformed

    private void sectionItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_sectionItemStateChanged
      try {
            if(section.getSelectedItem().toString().equals("Selectionnez")){
            klas.setSelectedItem(null);
        }else{
            Mn.FULLCOMBO(klas, "description","section",section.getSelectedItem().toString(), "classe");
        }
        } catch (Exception e) {
        }
    }//GEN-LAST:event_sectionItemStateChanged

    private void tblrapportMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblrapportMouseClicked
       
    }//GEN-LAST:event_tblrapportMouseClicked

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Rapport_Principale.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Rapport_Principale.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Rapport_Principale.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Rapport_Principale.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Rapport_Principale().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox academique;
    private javax.swing.JLabel aobligatoria;
    private javax.swing.JLabel classeobligatoria;
    private javax.swing.JComboBox controle;
    private javax.swing.JLabel controleobligatoria;
    private org.edisoncor.gui.textField.TextFieldRectBackground de;
    private javax.swing.JLabel deobligatoria;
    private org.edisoncor.gui.textField.TextFieldRectBackground ha;
    private javax.swing.JMenuItem imprimer;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu5;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JComboBox klas;
    private javax.swing.JFormattedTextField matricule;
    private javax.swing.JLabel matriculeobligatoria;
    private javax.swing.JComboBox section;
    private javax.swing.JMenuItem sortir1;
    private javax.swing.JTable tblmodel;
    private javax.swing.JTable tblrapport;
    // End of variables declaration//GEN-END:variables
}