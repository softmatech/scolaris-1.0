
package Menus;

import Methods.*;
import Menus.*;
import Methods.Theme;
import Methods.Variables;
import java.awt.Color;

public class Calendrier extends javax.swing.JInternalFrame {
    Manipulation Mn= new Manipulation();
Theme th= new Theme();
String gotdate="";
String gotmonth="";
String gotday="";
    public Calendrier() {
        initComponents();
        this.setTitle("Scolaris: "+Variables.Empresa+" : Calendrier");
        //this.setLocationRelativeTo(null);
         //th.Icone_JFrame(this);
         fetekonsa.setVisible(false);
         feteinteret.setVisible(false);
         AlerteEvents();
    }

private void AlerteEvents(){
  gotdate= almanak.getCalendar().get(java.util.Calendar.DATE)+" de "+(almanak.getCalendar().get(java.util.Calendar.MONTH)+1);
  gotmonth= almanak.getCalendar().get(java.util.Calendar.MONTH)+1+"";
  gotday= almanak.getCalendar().get(java.util.Calendar.DATE)+"";
 if(gotdate.equals("16 de 9")){
     fetekonsa.setVisible(true);
     fetekonsa.setText("Journée mondiale de l'alimentation."); 
     feteinteret.setVisible(false);
 }else if(gotdate.equals("17 de 10")){
     fetekonsa.setVisible(true);
     fetekonsa.setText("Journée internationale pour l'élimination de la pauvreté.");
     feteinteret.setVisible(true);
     feteinteret.setText("Mort de Déssalines."); 
 }else if(gotdate.equals("1 de 11")){
     feteinteret.setVisible(true);
     feteinteret.setText("Jour de la Toussaint.");
     fetekonsa.setVisible(false);
 }else if(gotdate.equals("2 de 11")){
     feteinteret.setVisible(true);
     feteinteret.setText("Jour des morts.");
     fetekonsa.setVisible(false);
 }else if(gotdate.equals("18 de 11")){
     feteinteret.setVisible(true);
     feteinteret.setText("Bataille de Vertières.");
     fetekonsa.setVisible(false);
 }else if(gotdate.equals("1 de 12")){
     fetekonsa.setVisible(true);
     fetekonsa.setText("Journée mondiale SIDA.");
     feteinteret.setVisible(false);
 }else if(gotdate.equals("3 de 12")){
     fetekonsa.setVisible(true);
     fetekonsa.setText("Journée internationale des personnes Handicapées.");
     feteinteret.setVisible(false);
 }else if(gotdate.equals("5 de 12")){
     feteinteret.setVisible(true);
     feteinteret.setText("Arrivée de Chistophe Colomb en Haiti.");
     fetekonsa.setVisible(false);
 }else if(gotdate.equals("1 de 1")){
     feteinteret.setVisible(true);
     feteinteret.setText("Jour de l'indépendance.");
     fetekonsa.setVisible(false);
 }else if(gotdate.equals("2 de 1")){
     feteinteret.setVisible(true);
     feteinteret.setText("Jour des Aieux.");
     fetekonsa.setVisible(false);
 }else if(gotdate.equals("6 de 2")){
     feteinteret.setVisible(true);
     feteinteret.setText("Les Rois.");
     fetekonsa.setVisible(false);
 }else if(gotdate.equals("8 de 3")){
     fetekonsa.setVisible(true);
     fetekonsa.setText("Journée internationale des femmes.");
     feteinteret.setVisible(false);
 }else if(gotdate.equals("21 de 3")){
     fetekonsa.setVisible(true);
     fetekonsa.setText("Journée internationale pour l'élimination de la discrimination raciale.");
     feteinteret.setVisible(false);
 }else if(gotmonth.equals("4")){
     int jour=Integer.parseInt(gotday);
     if(jour>=24){
     feteinteret.setVisible(true);
     feteinteret.setText("Vendredi Saint."); 
     fetekonsa.setVisible(true);
     fetekonsa.setText("Dimanche Pâques."); 
     }
 }else if(gotdate.equals("7 de 4")){
     fetekonsa.setVisible(true);
     fetekonsa.setText("Journée mondiale de la santé.");
     feteinteret.setVisible(false);
 }else if(gotdate.equals("22 de 4")){
     fetekonsa.setVisible(true);
     fetekonsa.setText("Journée de la Terre.");
     feteinteret.setVisible(false);
 }else if(gotdate.equals("1 de 5")){
     feteinteret.setVisible(true);
     feteinteret.setText("Fête du Travail et de l'Agriculture.");
     fetekonsa.setVisible(false);
 }else if(gotdate.equals("18 de 5")){
     feteinteret.setVisible(true);
     feteinteret.setText("Fête du Drapeau.");
     fetekonsa.setVisible(false);
 }else  if(gotmonth.equals("5")){
     int jour=Integer.parseInt(gotday);
     if(jour>=25){
     fetekonsa.setVisible(true);
     fetekonsa.setText("Fête des Mères."); 
     feteinteret.setVisible(false);
     }
 }else if(gotdate.equals("31 de 5")){
     feteinteret.setVisible(true);
     feteinteret.setText("Journée sans Tabac.");
     fetekonsa.setVisible(false);
 }else  if(gotdate.equals("5 de 6")){
     fetekonsa.setVisible(true);
     fetekonsa.setText("Journée mondiale de l'environnement.");
     feteinteret.setVisible(false);
 }else{
  fetekonsa.setVisible(false);
  feteinteret.setVisible(false);
 }
}
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jSeparator2 = new javax.swing.JSeparator();
        almanak = new com.toedter.calendar.JCalendar();
        clockFace1 = new org.edisoncor.gui.varios.ClockFace();
        jSeparator1 = new javax.swing.JSeparator();
        jLabel1 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jSeparator3 = new javax.swing.JSeparator();
        fetekonsa = new javax.swing.JLabel();
        feteinteret = new javax.swing.JLabel();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        sortir = new javax.swing.JMenuItem();

        almanak.setBackground(new java.awt.Color(255, 255, 255));
        almanak.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        almanak.addMouseWheelListener(new java.awt.event.MouseWheelListener() {
            public void mouseWheelMoved(java.awt.event.MouseWheelEvent evt) {
                almanakMouseWheelMoved(evt);
            }
        });
        almanak.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                almanakMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                almanakMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                almanakMouseExited(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                almanakMousePressed(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                almanakMouseReleased(evt);
            }
        });
        almanak.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseDragged(java.awt.event.MouseEvent evt) {
                almanakMouseDragged(evt);
            }
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                almanakMouseMoved(evt);
            }
        });
        almanak.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                almanakFocusGained(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel1.setText("Légende");

        jPanel2.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/jouferrier.jpg"))); // NOI18N
        jLabel5.setText("Jour Férrier sans congé scolaire. ");

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/jourstop.jpg"))); // NOI18N
        jLabel6.setText("Jour Férrier avec congé scolaire. ");

        fetekonsa.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        fetekonsa.setForeground(new java.awt.Color(0, 102, 0));
        fetekonsa.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/jouferrier.jpg"))); // NOI18N
        fetekonsa.setText("Légende");

        feteinteret.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        feteinteret.setForeground(new java.awt.Color(204, 0, 0));
        feteinteret.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/jourstop.jpg"))); // NOI18N
        feteinteret.setText("Légende");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 269, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(84, 84, 84)
                .addComponent(jLabel6)
                .addGap(95, 95, 95))
            .addComponent(jSeparator3)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(feteinteret, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(fetekonsa, javax.swing.GroupLayout.PREFERRED_SIZE, 693, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(jLabel6))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator3, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(fetekonsa, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(feteinteret, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jMenu1.setText("Action");

        sortir.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_S, java.awt.event.InputEvent.CTRL_MASK));
        sortir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/salir.png"))); // NOI18N
        sortir.setText("Quitter");
        sortir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sortirActionPerformed(evt);
            }
        });
        jMenu1.add(sortir);

        jMenuBar1.add(jMenu1);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSeparator1)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(almanak, javax.swing.GroupLayout.DEFAULT_SIZE, 519, Short.MAX_VALUE)
                        .addGap(28, 28, 28)
                        .addComponent(clockFace1, javax.swing.GroupLayout.PREFERRED_SIZE, 239, javax.swing.GroupLayout.PREFERRED_SIZE))))
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(clockFace1, javax.swing.GroupLayout.PREFERRED_SIZE, 215, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(67, 67, 67))
                    .addComponent(almanak, javax.swing.GroupLayout.PREFERRED_SIZE, 303, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(5, 5, 5)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 2, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void almanakMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_almanakMouseClicked

    }//GEN-LAST:event_almanakMouseClicked

    private void almanakMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_almanakMousePressed
       AlerteEvents();
    }//GEN-LAST:event_almanakMousePressed

    private void almanakFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_almanakFocusGained
        AlerteEvents();
    }//GEN-LAST:event_almanakFocusGained

    private void almanakMouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_almanakMouseMoved
        AlerteEvents();
    }//GEN-LAST:event_almanakMouseMoved

    private void almanakMouseDragged(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_almanakMouseDragged
       
    }//GEN-LAST:event_almanakMouseDragged

    private void almanakMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_almanakMouseEntered
       
    }//GEN-LAST:event_almanakMouseEntered

    private void almanakMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_almanakMouseReleased
      
    }//GEN-LAST:event_almanakMouseReleased

    private void almanakMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_almanakMouseExited
      //  AlerteEvents();
    }//GEN-LAST:event_almanakMouseExited

    private void almanakMouseWheelMoved(java.awt.event.MouseWheelEvent evt) {//GEN-FIRST:event_almanakMouseWheelMoved
        AlerteEvents();
    }//GEN-LAST:event_almanakMouseWheelMoved

    private void sortirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_sortirActionPerformed
        Mn.EXIT_BUTTON(this);
    }//GEN-LAST:event_sortirActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Calendrier.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Calendrier.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Calendrier.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Calendrier.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Calendrier().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private com.toedter.calendar.JCalendar almanak;
    private org.edisoncor.gui.varios.ClockFace clockFace1;
    private javax.swing.JLabel feteinteret;
    private javax.swing.JLabel fetekonsa;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JMenuItem sortir;
    // End of variables declaration//GEN-END:variables
}
