/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Menus;

import Forms.ClasseForm;
import Forms.ClasseMatiereForm;
import Forms.CollectionForm;
import Forms.ElevesForm;
import Forms.EmployerForm;
import Forms.HoraireForm;
import Forms.MatiereForm;
import Forms.NotificationForm;
import Forms.PaiementForm;
import Forms.PersonForm;
import Forms.ProgrammeForm;
import Forms.Themes;
import Methods.Manipulation;
import Methods.Theme;
import Methods.Variables;
import Processus.BulletinForm;
import Processus.Entrer_noteForm;
import Processus.InscriptionForm;
import java.awt.Color;
import java.awt.Cursor;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.UIManager;

/**
 *
 * @author josephandyfeidje
 */
public class MenuModerne extends javax.swing.JFrame {

    Manipulation Mn = new Manipulation();
    Theme th = new Theme();
    
    public MenuModerne() {
        identifyInstitution();
        initComponents();
        setExtendedState(MAXIMIZED_BOTH);
    }

    private void identifyInstitution() {
        try {
            String sql = "select nom from institution ";
            ResultSet rs = Mn.Rezulta(sql);
            if (rs.last()) {
                Variables.Empresa = rs.getString("nom");
            }
        } catch (SQLException ex) {
            Logger.getLogger(Menus.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        jPanel1 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        jPanel6 = new javax.swing.JPanel();
        classe = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jPanel5 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        jPanel7 = new javax.swing.JPanel();
        class_matiere = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        jPanel9 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        jPanel10 = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        jPanel11 = new javax.swing.JPanel();
        jPanel12 = new javax.swing.JPanel();
        jLabel7 = new javax.swing.JLabel();
        jPanel13 = new javax.swing.JPanel();
        jLabel8 = new javax.swing.JLabel();
        jPanel14 = new javax.swing.JPanel();
        jLabel9 = new javax.swing.JLabel();
        jPanel15 = new javax.swing.JPanel();
        jPanel16 = new javax.swing.JPanel();
        jLabel10 = new javax.swing.JLabel();
        jPanel17 = new javax.swing.JPanel();
        jLabel11 = new javax.swing.JLabel();
        jPanel18 = new javax.swing.JPanel();
        jLabel12 = new javax.swing.JLabel();
        jPanel19 = new javax.swing.JPanel();
        jPanel20 = new javax.swing.JPanel();
        jLabel13 = new javax.swing.JLabel();
        jPanel21 = new javax.swing.JPanel();
        jLabel14 = new javax.swing.JLabel();
        jPanel22 = new javax.swing.JPanel();
        jLabel15 = new javax.swing.JLabel();
        jPanel23 = new javax.swing.JPanel();
        jPanel24 = new javax.swing.JPanel();
        jLabel16 = new javax.swing.JLabel();
        jPanel25 = new javax.swing.JPanel();
        jLabel17 = new javax.swing.JLabel();
        jPanel26 = new javax.swing.JPanel();
        jLabel18 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        panelCurves1 = new org.edisoncor.gui.panel.PanelCurves();
        jLabel19 = new javax.swing.JLabel();
        jLabel20 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(new java.awt.CardLayout());

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jPanel1MouseClicked(evt);
            }
        });

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));
        jPanel3.setLayout(new java.awt.GridLayout(1, 0));

        jPanel6.setBackground(new java.awt.Color(255, 255, 255));
        jPanel6.setLayout(new java.awt.GridBagLayout());

        classe.setBackground(new java.awt.Color(87, 148, 210));
        classe.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        classe.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                classeMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                classeMouseEntered(evt);
            }
        });
        classe.setLayout(new java.awt.CardLayout());

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/ButtonFoto/icons8-google_classroom.png"))); // NOI18N
        jLabel1.setToolTipText("Registre de Classe");
        jLabel1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel1MouseClicked(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jLabel1MouseExited(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jLabel1MouseEntered(evt);
            }
        });
        classe.add(jLabel1, "card2");

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(4, 5, 0, 9);
        jPanel6.add(classe, gridBagConstraints);

        jPanel4.setBackground(new java.awt.Color(87, 148, 210));
        jPanel4.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        jPanel4.setLayout(new java.awt.CardLayout());

        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/ButtonFoto/icons8-book_filled.png"))); // NOI18N
        jLabel2.setToolTipText("Registre de Matière(s)");
        jLabel2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel2MouseClicked(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jLabel2MouseExited(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jLabel2MouseEntered(evt);
            }
        });
        jPanel4.add(jLabel2, "card2");

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(6, 5, 0, 9);
        jPanel6.add(jPanel4, gridBagConstraints);

        jPanel5.setBackground(new java.awt.Color(87, 148, 210));
        jPanel5.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        jPanel5.setLayout(new java.awt.CardLayout());

        jLabel3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/ButtonFoto/icons8-calendar_plus_filled.png"))); // NOI18N
        jLabel3.setToolTipText("Registre de programme de classe");
        jLabel3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel3MouseClicked(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jLabel3MouseExited(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jLabel3MouseEntered(evt);
            }
        });
        jPanel5.add(jLabel3, "card2");

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(6, 5, 13, 9);
        jPanel6.add(jPanel5, gridBagConstraints);

        jPanel3.add(jPanel6);

        jPanel7.setBackground(new java.awt.Color(255, 255, 255));
        jPanel7.setLayout(new java.awt.GridBagLayout());

        class_matiere.setBackground(new java.awt.Color(87, 148, 210));
        class_matiere.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        class_matiere.setLayout(new java.awt.CardLayout());

        jLabel4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/ButtonFoto/icons8-courses_filled.png"))); // NOI18N
        jLabel4.setToolTipText("Registre de Classe par matière");
        jLabel4.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel4MouseClicked(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jLabel4MouseExited(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jLabel4MouseEntered(evt);
            }
        });
        class_matiere.add(jLabel4, "card2");

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(4, 5, 0, 9);
        jPanel7.add(class_matiere, gridBagConstraints);

        jPanel9.setBackground(new java.awt.Color(87, 148, 210));
        jPanel9.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        jPanel9.setLayout(new java.awt.CardLayout());

        jLabel5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/ButtonFoto/icons8-google_alerts.png"))); // NOI18N
        jLabel5.setToolTipText("Envoyer des Notification(s)");
        jLabel5.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel5MouseClicked(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jLabel5MouseExited(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jLabel5MouseEntered(evt);
            }
        });
        jPanel9.add(jLabel5, "card2");

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(6, 5, 0, 9);
        jPanel7.add(jPanel9, gridBagConstraints);

        jPanel10.setBackground(new java.awt.Color(87, 148, 210));
        jPanel10.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        jPanel10.setLayout(new java.awt.CardLayout());

        jLabel6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/ButtonFoto/icons8-ballot_filled.png"))); // NOI18N
        jLabel6.setToolTipText("Generer Bulletin");
        jLabel6.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel6MouseClicked(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jLabel6MouseExited(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jLabel6MouseEntered(evt);
            }
        });
        jPanel10.add(jLabel6, "card2");

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(6, 5, 13, 9);
        jPanel7.add(jPanel10, gridBagConstraints);

        jPanel3.add(jPanel7);

        jPanel11.setBackground(new java.awt.Color(255, 255, 255));
        jPanel11.setLayout(new java.awt.GridBagLayout());

        jPanel12.setBackground(new java.awt.Color(87, 148, 210));
        jPanel12.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        jPanel12.setLayout(new java.awt.CardLayout());

        jLabel7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/ButtonFoto/icons8-money_bag_filled.png"))); // NOI18N
        jLabel7.setToolTipText("Collection");
        jLabel7.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel7MouseClicked(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jLabel7MouseExited(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jLabel7MouseEntered(evt);
            }
        });
        jPanel12.add(jLabel7, "card2");

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(4, 5, 0, 9);
        jPanel11.add(jPanel12, gridBagConstraints);

        jPanel13.setBackground(new java.awt.Color(87, 148, 210));
        jPanel13.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        jPanel13.setLayout(new java.awt.CardLayout());

        jLabel8.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/ButtonFoto/icons8-open_book_filled.png"))); // NOI18N
        jLabel8.setToolTipText("Registre d'ouvrage(s)");
        jPanel13.add(jLabel8, "card2");

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(6, 5, 0, 9);
        jPanel11.add(jPanel13, gridBagConstraints);

        jPanel14.setBackground(new java.awt.Color(87, 148, 210));
        jPanel14.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        jPanel14.setLayout(new java.awt.CardLayout());

        jLabel9.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/ButtonFoto/icons8-task_filled.png"))); // NOI18N
        jLabel9.setToolTipText("Entrer Notes");
        jLabel9.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel9MouseClicked(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jLabel9MouseExited(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jLabel9MouseEntered(evt);
            }
        });
        jPanel14.add(jLabel9, "card2");

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(6, 5, 13, 9);
        jPanel11.add(jPanel14, gridBagConstraints);

        jPanel3.add(jPanel11);

        jPanel15.setBackground(new java.awt.Color(255, 255, 255));
        jPanel15.setLayout(new java.awt.GridBagLayout());

        jPanel16.setBackground(new java.awt.Color(87, 148, 210));
        jPanel16.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        jPanel16.setLayout(new java.awt.CardLayout());

        jLabel10.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/ButtonFoto/icons8-student_registration_filled.png"))); // NOI18N
        jLabel10.setToolTipText("Registre d'élève(s)");
        jLabel10.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel10MouseClicked(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jLabel10MouseExited(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jLabel10MouseEntered(evt);
            }
        });
        jPanel16.add(jLabel10, "card2");

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(4, 5, 0, 9);
        jPanel15.add(jPanel16, gridBagConstraints);

        jPanel17.setBackground(new java.awt.Color(87, 148, 210));
        jPanel17.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        jPanel17.setLayout(new java.awt.CardLayout());

        jLabel11.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/ButtonFoto/icons8-school_filled.png"))); // NOI18N
        jLabel11.setToolTipText("Registre d'ouvrage par classe");
        jPanel17.add(jLabel11, "card2");

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(6, 5, 0, 9);
        jPanel15.add(jPanel17, gridBagConstraints);

        jPanel18.setBackground(new java.awt.Color(87, 148, 210));
        jPanel18.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        jPanel18.setLayout(new java.awt.CardLayout());

        jLabel12.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/ButtonFoto/icons8-inscription_filled.png"))); // NOI18N
        jLabel12.setToolTipText("Registre d'inscription");
        jLabel12.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel12MouseClicked(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jLabel12MouseExited(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jLabel12MouseEntered(evt);
            }
        });
        jPanel18.add(jLabel12, "card2");

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(6, 5, 13, 9);
        jPanel15.add(jPanel18, gridBagConstraints);

        jPanel3.add(jPanel15);

        jPanel19.setBackground(new java.awt.Color(255, 255, 255));
        jPanel19.setLayout(new java.awt.GridBagLayout());

        jPanel20.setBackground(new java.awt.Color(87, 148, 210));
        jPanel20.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        jPanel20.setLayout(new java.awt.CardLayout());

        jLabel13.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/ButtonFoto/icons8-groups_filled.png"))); // NOI18N
        jLabel13.setToolTipText("Registre d'employer");
        jLabel13.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel13MouseClicked(evt);
            }
        });
        jPanel20.add(jLabel13, "card2");

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(4, 5, 0, 9);
        jPanel19.add(jPanel20, gridBagConstraints);

        jPanel21.setBackground(new java.awt.Color(87, 148, 210));
        jPanel21.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        jPanel21.setLayout(new java.awt.CardLayout());

        jLabel14.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/ButtonFoto/icons8-merchant_account_filled.png"))); // NOI18N
        jLabel14.setToolTipText("Registre de Paiement");
        jLabel14.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel14MouseClicked(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jLabel14MouseExited(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jLabel14MouseEntered(evt);
            }
        });
        jPanel21.add(jLabel14, "card2");

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(6, 5, 0, 9);
        jPanel19.add(jPanel21, gridBagConstraints);

        jPanel22.setBackground(new java.awt.Color(87, 148, 210));
        jPanel22.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        jPanel22.setLayout(new java.awt.CardLayout());

        jLabel15.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/ButtonFoto/icons8-courses_filled.png"))); // NOI18N
        jPanel22.add(jLabel15, "card2");

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(6, 5, 13, 9);
        jPanel19.add(jPanel22, gridBagConstraints);

        jPanel3.add(jPanel19);

        jPanel23.setBackground(new java.awt.Color(255, 255, 255));
        jPanel23.setLayout(new java.awt.GridBagLayout());

        jPanel24.setBackground(new java.awt.Color(87, 148, 210));
        jPanel24.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        jPanel24.setLayout(new java.awt.CardLayout());

        jLabel16.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/ButtonFoto/icons8-overtime_filled.png"))); // NOI18N
        jLabel16.setToolTipText("Refistre d'horaire");
        jLabel16.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel16MouseClicked(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jLabel16MouseExited(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jLabel16MouseEntered(evt);
            }
        });
        jPanel24.add(jLabel16, "card2");

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(4, 5, 0, 9);
        jPanel23.add(jPanel24, gridBagConstraints);

        jPanel25.setBackground(new java.awt.Color(87, 148, 210));
        jPanel25.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        jPanel25.setLayout(new java.awt.CardLayout());

        jLabel17.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/ButtonFoto/icons8-add_administrator_filled.png"))); // NOI18N
        jLabel17.setToolTipText("Registre de personne");
        jLabel17.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel17MouseClicked(evt);
            }
        });
        jPanel25.add(jLabel17, "card2");

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(6, 5, 0, 9);
        jPanel23.add(jPanel25, gridBagConstraints);

        jPanel26.setBackground(new java.awt.Color(87, 148, 210));
        jPanel26.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        jPanel26.setLayout(new java.awt.CardLayout());

        jLabel18.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/ButtonFoto/icons8-courses_filled.png"))); // NOI18N
        jPanel26.add(jLabel18, "card2");

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(6, 5, 13, 9);
        jPanel23.add(jPanel26, gridBagConstraints);

        jPanel3.add(jPanel23);

        jPanel2.setBackground(new java.awt.Color(87, 148, 210));
        jPanel2.setLayout(new java.awt.CardLayout());

        panelCurves1.setBorder(new org.edisoncor.gui.util.DropShadowBorder());

        jLabel19.setFont(new java.awt.Font("Chalkduster", 1, 48)); // NOI18N
        jLabel19.setForeground(new java.awt.Color(255, 255, 255));
        jLabel19.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel19.setText("Scolaris 1.0");
        panelCurves1.add(jLabel19, java.awt.BorderLayout.CENTER);

        jPanel2.add(panelCurves1, "card3");

        jLabel20.setText("  Copyright (c) 2018 Softmatech. All rights reserved.");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(376, Short.MAX_VALUE)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(397, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, 440, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel20)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGap(44, 44, 44)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, 66, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(61, 61, 61)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, 484, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 116, Short.MAX_VALUE)
                .addComponent(jLabel20)
                .addContainerGap())
        );

        getContentPane().add(jPanel1, "card2");

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void classeMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_classeMouseClicked

    }//GEN-LAST:event_classeMouseClicked

    private void jLabel1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel1MouseClicked
            if(evt.getClickCount() == 2){
            setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
            new ClasseForm().show();
            setCursor(Cursor.getDefaultCursor());
        }
    }//GEN-LAST:event_jLabel1MouseClicked

    private void jLabel4MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel4MouseClicked
        if(evt.getClickCount() == 2){
            setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
            new ClasseMatiereForm().show();
            setCursor(Cursor.getDefaultCursor());
        }
    }//GEN-LAST:event_jLabel4MouseClicked

    private void jLabel7MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel7MouseClicked
        if(evt.getClickCount() == 2){
            setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
            new CollectionForm().show();
            setCursor(Cursor.getDefaultCursor());
        }
    }//GEN-LAST:event_jLabel7MouseClicked

    private void jLabel10MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel10MouseClicked
        if(evt.getClickCount() == 2){
            setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
            new ElevesForm().show();
            setCursor(Cursor.getDefaultCursor());
        }
    }//GEN-LAST:event_jLabel10MouseClicked

    private void jLabel16MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel16MouseClicked
        if(evt.getClickCount() == 2){
            setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
            new HoraireForm().show();
            setCursor(Cursor.getDefaultCursor());
        }
    }//GEN-LAST:event_jLabel16MouseClicked

    private void jLabel2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel2MouseClicked
        if(evt.getClickCount() == 2){
            setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
            new MatiereForm().show();
            setCursor(Cursor.getDefaultCursor());
        }
    }//GEN-LAST:event_jLabel2MouseClicked

    private void jLabel5MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel5MouseClicked
        if(evt.getClickCount() == 2){
            setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
            new NotificationForm().show();
            setCursor(Cursor.getDefaultCursor());
        }
    }//GEN-LAST:event_jLabel5MouseClicked

    private void jLabel3MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel3MouseClicked
       if(evt.getClickCount() == 2){
            setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
            new ProgrammeForm().show();
            setCursor(Cursor.getDefaultCursor());
        }
    }//GEN-LAST:event_jLabel3MouseClicked

    private void jLabel6MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel6MouseClicked
        if(evt.getClickCount() == 2){
            setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
            new BulletinForm().show();
            setCursor(Cursor.getDefaultCursor());
        }
    }//GEN-LAST:event_jLabel6MouseClicked

    private void jLabel9MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel9MouseClicked
        if(evt.getClickCount() == 2){
            setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
            new Entrer_noteForm().show();
            setCursor(Cursor.getDefaultCursor());
        }
    }//GEN-LAST:event_jLabel9MouseClicked

    private void jLabel12MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel12MouseClicked
        if(evt.getClickCount() == 2){
            setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
            new InscriptionForm().show();
            setCursor(Cursor.getDefaultCursor());
        }
    }//GEN-LAST:event_jLabel12MouseClicked

    private void jLabel1MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel1MouseEntered
        Mn.panelSetMouseEnteredMenu(classe);
    }//GEN-LAST:event_jLabel1MouseEntered

    private void classeMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_classeMouseEntered
        // TODO add your handling code here:
    }//GEN-LAST:event_classeMouseEntered

    private void jLabel1MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel1MouseExited
        Mn.panelSetMouseExitedMenu(classe);
    }//GEN-LAST:event_jLabel1MouseExited

    private void jLabel4MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel4MouseEntered
        Mn.panelSetMouseEnteredMenu(class_matiere);
    }//GEN-LAST:event_jLabel4MouseEntered

    private void jLabel4MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel4MouseExited
        Mn.panelSetMouseExitedMenu(class_matiere);
    }//GEN-LAST:event_jLabel4MouseExited

    private void jLabel7MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel7MouseEntered
        Mn.panelSetMouseEnteredMenu(jPanel12);
    }//GEN-LAST:event_jLabel7MouseEntered

    private void jLabel7MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel7MouseExited
        Mn.panelSetMouseExitedMenu(jPanel12);
    }//GEN-LAST:event_jLabel7MouseExited

    private void jLabel10MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel10MouseEntered
        Mn.panelSetMouseEnteredMenu(jPanel16);
    }//GEN-LAST:event_jLabel10MouseEntered

    private void jLabel10MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel10MouseExited
        Mn.panelSetMouseExitedMenu(jPanel16);
    }//GEN-LAST:event_jLabel10MouseExited

    private void jLabel16MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel16MouseEntered
        Mn.panelSetMouseEnteredMenu(jPanel24);
    }//GEN-LAST:event_jLabel16MouseEntered

    private void jLabel16MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel16MouseExited
        Mn.panelSetMouseExitedMenu(jPanel24);
    }//GEN-LAST:event_jLabel16MouseExited

    private void jLabel2MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel2MouseEntered
        Mn.panelSetMouseEnteredMenu(jPanel4);
    }//GEN-LAST:event_jLabel2MouseEntered

    private void jLabel2MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel2MouseExited
        Mn.panelSetMouseExitedMenu(jPanel4);
    }//GEN-LAST:event_jLabel2MouseExited

    private void jLabel5MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel5MouseEntered
        Mn.panelSetMouseEnteredMenu(jPanel9);
    }//GEN-LAST:event_jLabel5MouseEntered

    private void jLabel5MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel5MouseExited
        Mn.panelSetMouseExitedMenu(jPanel9);
    }//GEN-LAST:event_jLabel5MouseExited

    private void jLabel3MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel3MouseEntered
        Mn.panelSetMouseEnteredMenu(jPanel5);
    }//GEN-LAST:event_jLabel3MouseEntered

    private void jLabel3MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel3MouseExited
        Mn.panelSetMouseExitedMenu(jPanel9);
    }//GEN-LAST:event_jLabel3MouseExited

    private void jLabel6MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel6MouseEntered
        Mn.panelSetMouseEnteredMenu(jPanel10);
    }//GEN-LAST:event_jLabel6MouseEntered

    private void jLabel6MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel6MouseExited
        Mn.panelSetMouseExitedMenu(jPanel10);
    }//GEN-LAST:event_jLabel6MouseExited

    private void jLabel9MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel9MouseEntered
        Mn.panelSetMouseEnteredMenu(jPanel14);
    }//GEN-LAST:event_jLabel9MouseEntered

    private void jLabel9MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel9MouseExited
        Mn.panelSetMouseExitedMenu(jPanel14);
    }//GEN-LAST:event_jLabel9MouseExited

    private void jLabel12MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel12MouseEntered
        Mn.panelSetMouseEnteredMenu(jPanel18);
    }//GEN-LAST:event_jLabel12MouseEntered

    private void jLabel12MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel12MouseExited
        Mn.panelSetMouseExitedMenu(jPanel18);
    }//GEN-LAST:event_jLabel12MouseExited

    private void jLabel14MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel14MouseClicked
        if(evt.getClickCount() == 2){
            setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
            new PaiementForm().show();
            setCursor(Cursor.getDefaultCursor());
        }
    }//GEN-LAST:event_jLabel14MouseClicked

    private void jLabel14MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel14MouseEntered
        Mn.panelSetMouseEnteredMenu(jPanel21);
    }//GEN-LAST:event_jLabel14MouseEntered

    private void jLabel14MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel14MouseExited
        Mn.panelSetMouseExitedMenu(jPanel21);
    }//GEN-LAST:event_jLabel14MouseExited

    private void jPanel1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel1MouseClicked
        if(evt.getClickCount() == 4){
            setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
            this.dispose();
            new Themes(null, rootPaneCheckingEnabled).show();
            setCursor(Cursor.getDefaultCursor());
        }
    }//GEN-LAST:event_jPanel1MouseClicked

    private void jLabel17MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel17MouseClicked
        if(evt.getClickCount() == 2){
            setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
            new PersonForm().show();
            setCursor(Cursor.getDefaultCursor());
        }
    }//GEN-LAST:event_jLabel17MouseClicked

    private void jLabel13MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel13MouseClicked
        if(evt.getClickCount() == 2){
            setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
            new EmployerForm().show();
            setCursor(Cursor.getDefaultCursor());
        }
    }//GEN-LAST:event_jLabel13MouseClicked

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (UIManager.LookAndFeelInfo laf : UIManager
                .getInstalledLookAndFeels()) {
            if ("Nimbus".equals(laf.getName())) {
                UIManager.setLookAndFeel(laf.getClassName());
//                UIManager.getLookAndFeelDefaults().put("DesktopPane[Enabled].backgroundPainter", new DesktopPainter());
//                UIManager.getLookAndFeelDefaults().put("Button.background",new Color(0, 153, 153));
                //UIManager.getLookAndFeelDefaults().put("OptionPane.background", new Color(0, 102, 153));
                UIManager.getLookAndFeelDefaults().put("Button[Focused+MouseOver].backgroundPainter", new Color(0, 107, 163));
                UIManager.getLookAndFeelDefaults().put("Table.alternateRowColor", new Color(87,148,210));
                UIManager.getLookAndFeelDefaults().put("Table[Enabled+Selected].textBackground", Color.black);
                //UIManager.getLookAndFeelDefaults().put("Panel.background", Color.lightGray);
            }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MenuModerne.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MenuModerne.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MenuModerne.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MenuModerne.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new MenuModerne().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel class_matiere;
    private javax.swing.JPanel classe;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel11;
    private javax.swing.JPanel jPanel12;
    private javax.swing.JPanel jPanel13;
    private javax.swing.JPanel jPanel14;
    private javax.swing.JPanel jPanel15;
    private javax.swing.JPanel jPanel16;
    private javax.swing.JPanel jPanel17;
    private javax.swing.JPanel jPanel18;
    private javax.swing.JPanel jPanel19;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel20;
    private javax.swing.JPanel jPanel21;
    private javax.swing.JPanel jPanel22;
    private javax.swing.JPanel jPanel23;
    private javax.swing.JPanel jPanel24;
    private javax.swing.JPanel jPanel25;
    private javax.swing.JPanel jPanel26;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel9;
    private org.edisoncor.gui.panel.PanelCurves panelCurves1;
    // End of variables declaration//GEN-END:variables
}
