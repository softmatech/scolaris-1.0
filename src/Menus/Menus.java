package Menus;

import Methods.*;
import Consults.*;
import Forms.*;
import Processus.*;
import Rapports.*;
import Start.*;
import java.awt.Color;
import java.awt.Cursor;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Menus extends javax.swing.JFrame {

    Manipulation Mn = new Manipulation();
    Theme th = new Theme();

    public Menus() {
        identifyInstitution();
        initComponents();
        desktop.setBorder(new ImagenFondo());
        this.setTitle("Scolaris: " + Variables.Empresa + " : Configuration Principale");
        th.Icone_JFrame(this);
        th.Fond_JFrame(this);
        this.setExtendedState(MAXIMIZED_BOTH);
        utilisateur.setText(Variables.Utilisateurs);
        date.setText(Mn.DATEHOURNOW());
        premier.setEnabled(false);
        deuxieme.setEnabled(false);
        troisieme.setEnabled(false);
        Scolaire();
        GestiondeControle();
        DistribuerControle();
        
        if (Variables.Acces.equals("Moyen")) {
            paiement.setVisible(false);
            employer.setVisible(false);
            professeur.setVisible(false);
            utilisateuur.setVisible(false);
            horaire.setVisible(false);
            entrer_des_notes.setVisible(false);
            bulletin.setVisible(false);
            aide.setVisible(false);
            personaliser.setVisible(false);

        } else if (Variables.Acces.equals("Bas")) {
            paiement.setVisible(false);
            formulaire.setVisible(false);
            processus.setVisible(false);
            intelligente.setVisible(false);
            rapport.setVisible(false);
            aide.setVisible(false);
            personaliser.setVisible(false);
        }
        jToolBar1.setPreferredSize(this.getMaximumSize());
    }

    private void identifyInstitution() {
        try {
            String sql = "select nom from institution ";
            ResultSet rs = Mn.Rezulta(sql);
            if (rs.last()) {
                Variables.Empresa = rs.getString("nom");
            }
        } catch (SQLException ex) {
            Logger.getLogger(Menus.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void Scolaire() {
        String query = "select annee from annee_academique where code_annee\n"
                + "in (select max(code_annee) from annee_academique)";
        ResultSet rs = Mn.SEARCHDATA(query);
        try {
            if (rs.first()) {
                avan.setText("       | Année Académique: ");
                academique.setText(rs.getString("annee"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(Menus.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void GestiondeControle() {
        int val = 0;
        String query = "select max(controle) as con from detaille_note where controle in "
                + "(select controle from detaille_note where annee='" + academique.getText() + "')";
        ResultSet rs = Mn.SEARCHDATA(query);
        try {
            if (rs.first()) {
                val = rs.getInt("con");
                if (val == 1) {
                    premier.setSelected(false);
                    deuxieme.setSelected(true);
                    date1.setForeground(Color.yellow);
                    date2.setForeground(Color.red);
                    //System.out.println("looolll"); 
                } else if (val == 2) {
                    premier.setSelected(false);
                    date1.setForeground(Color.yellow);
                    deuxieme.setSelected(false);
                    date2.setForeground(Color.yellow);
                    troisieme.setSelected(true);
                    date3.setForeground(Color.red);
                } else {
                    premier.setSelected(true);
                    date1.setForeground(Color.red);
                }
            }
            if (val == 0) {
                premier.setSelected(true);
                date1.setForeground(Color.red);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Menus.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void DistribuerControle() {
        if (premier.isSelected()) {
            Variables.trimestre = 1;
        } else if (deuxieme.isSelected()) {
            Variables.trimestre = 2;
        } else if (troisieme.isSelected()) {
            Variables.trimestre = 3;
        }

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        popupMenu1 = new java.awt.PopupMenu();
        menuItem1 = new java.awt.MenuItem();
        panel1 = new org.edisoncor.gui.panel.Panel();
        jMenuItem6 = new javax.swing.JMenuItem();
        jMenuItem17 = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        jMenu1 = new javax.swing.JMenu();
        jMenu3 = new javax.swing.JMenu();
        jCheckBoxMenuItem1 = new javax.swing.JCheckBoxMenuItem();
        desktop = new javax.swing.JDesktopPane();
        jToolBar1 = new javax.swing.JToolBar();
        jLabel1 = new javax.swing.JLabel();
        utilisateur = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        premier = new javax.swing.JCheckBox();
        date1 = new javax.swing.JLabel();
        deuxieme = new javax.swing.JCheckBox();
        date2 = new javax.swing.JLabel();
        troisieme = new javax.swing.JCheckBox();
        date3 = new javax.swing.JLabel();
        troisieme1 = new javax.swing.JCheckBox();
        date4 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        date = new javax.swing.JLabel();
        jMenuBar1 = new javax.swing.JMenuBar();
        formulaire = new javax.swing.JMenu();
        matiere = new javax.swing.JMenuItem();
        classe = new javax.swing.JMenuItem();
        jMenuItem1 = new javax.swing.JMenuItem();
        eleve = new javax.swing.JMenuItem();
        employer = new javax.swing.JMenuItem();
        professeur = new javax.swing.JMenuItem();
        utilisateuur = new javax.swing.JMenuItem();
        horaire = new javax.swing.JMenuItem();
        jMenuItem3 = new javax.swing.JMenuItem();
        jMenuItem4 = new javax.swing.JMenuItem();
        programme = new javax.swing.JMenuItem();
        jMenuItem5 = new javax.swing.JMenuItem();
        processus = new javax.swing.JMenu();
        inscription = new javax.swing.JMenuItem();
        entrer_des_notes = new javax.swing.JMenuItem();
        bulletin = new javax.swing.JMenuItem();
        jMenuItem7 = new javax.swing.JMenuItem();
        economat = new javax.swing.JMenu();
        collection = new javax.swing.JMenuItem();
        paiement = new javax.swing.JMenuItem();
        intelligente = new javax.swing.JMenu();
        jMenuItem19 = new javax.swing.JMenuItem();
        rapport = new javax.swing.JMenu();
        rprincipal = new javax.swing.JMenuItem();
        outil = new javax.swing.JMenu();
        aide = new javax.swing.JMenuItem();
        personaliser = new javax.swing.JMenuItem();
        calendrier = new javax.swing.JMenuItem();
        notification = new javax.swing.JMenuItem();
        jMenu9 = new javax.swing.JMenu();
        verrouiller = new javax.swing.JMenuItem();
        sortir = new javax.swing.JMenuItem();
        jMenuItem2 = new javax.swing.JMenuItem();
        avan = new javax.swing.JMenu();
        academique = new javax.swing.JMenu();

        popupMenu1.setLabel("popupMenu1");

        menuItem1.setLabel("menuItem1");
        popupMenu1.add(menuItem1);

        javax.swing.GroupLayout panel1Layout = new javax.swing.GroupLayout(panel1);
        panel1.setLayout(panel1Layout);
        panel1Layout.setHorizontalGroup(
            panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );
        panel1Layout.setVerticalGroup(
            panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );

        jMenuItem6.setText("jMenuItem6");

        jMenuItem17.setText("jMenuItem17");

        jMenu2.setText("jMenu2");

        jMenu1.setText("jMenu1");

        jMenu3.setText("jMenu3");

        jCheckBoxMenuItem1.setSelected(true);
        jCheckBoxMenuItem1.setText("jCheckBoxMenuItem1");

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        desktop.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                desktopMouseMoved(evt);
            }
        });
        desktop.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                desktopMouseClicked(evt);
            }
        });

        jToolBar1.setBackground(new java.awt.Color(102, 102, 102));
        jToolBar1.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED, null, new java.awt.Color(51, 51, 51), null, null));
        jToolBar1.setRollover(true);
        jToolBar1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(0, 0, 153));
        jLabel1.setText("BIENVENUE: ");
        jToolBar1.add(jLabel1);

        utilisateur.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        utilisateur.setForeground(new java.awt.Color(0, 0, 153));
        utilisateur.setText("jLabel2   ");
        jToolBar1.add(utilisateur);

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel4.setText("     |    ");
        jToolBar1.add(jLabel4);
        jToolBar1.add(premier);

        date1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        date1.setText("1er Controle  ");
        jToolBar1.add(date1);
        jToolBar1.add(deuxieme);

        date2.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        date2.setText("2ème Controle   ");
        jToolBar1.add(date2);
        jToolBar1.add(troisieme);

        date3.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        date3.setText("3ème Controle");
        jToolBar1.add(date3);
        jToolBar1.add(troisieme1);

        date4.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        date4.setText("4ème Controle");
        jToolBar1.add(date4);

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(204, 0, 0));
        jLabel2.setText("            |  ");
        jToolBar1.add(jLabel2);

        date.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        date.setForeground(new java.awt.Color(204, 0, 0));
        date.setText("jLabel3");
        jToolBar1.add(date);

        javax.swing.GroupLayout desktopLayout = new javax.swing.GroupLayout(desktop);
        desktop.setLayout(desktopLayout);
        desktopLayout.setHorizontalGroup(
            desktopLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jToolBar1, javax.swing.GroupLayout.DEFAULT_SIZE, 1154, Short.MAX_VALUE)
        );
        desktopLayout.setVerticalGroup(
            desktopLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, desktopLayout.createSequentialGroup()
                .addGap(0, 495, Short.MAX_VALUE)
                .addComponent(jToolBar1, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        desktop.setLayer(jToolBar1, javax.swing.JLayeredPane.DEFAULT_LAYER);

        jMenuBar1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        formulaire.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/icons8-form.png"))); // NOI18N
        formulaire.setText("Formulaires");

        matiere.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_M, java.awt.event.InputEvent.ALT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        matiere.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/icons8-add_folder_2.png"))); // NOI18N
        matiere.setText("Matières");
        matiere.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                matiereActionPerformed(evt);
            }
        });
        formulaire.add(matiere);

        classe.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_C, java.awt.event.InputEvent.ALT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        classe.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/icons8-test_failed.png"))); // NOI18N
        classe.setText("Classes");
        classe.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                classeActionPerformed(evt);
            }
        });
        formulaire.add(classe);

        jMenuItem1.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_P, java.awt.event.InputEvent.ALT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/icons8-health_book.png"))); // NOI18N
        jMenuItem1.setText("Matières par Classe  ");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        formulaire.add(jMenuItem1);

        eleve.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_E, java.awt.event.InputEvent.ALT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        eleve.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/icons8-student.png"))); // NOI18N
        eleve.setText("Elèves");
        eleve.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                eleveActionPerformed(evt);
            }
        });
        formulaire.add(eleve);

        employer.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_A, java.awt.event.InputEvent.ALT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        employer.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/icons8-groups.png"))); // NOI18N
        employer.setText("Employers");
        employer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                employerActionPerformed(evt);
            }
        });
        formulaire.add(employer);

        professeur.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_Q, java.awt.event.InputEvent.ALT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        professeur.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/icons8-singing_teacher.png"))); // NOI18N
        professeur.setText("Professeurs");
        professeur.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                professeurActionPerformed(evt);
            }
        });
        formulaire.add(professeur);

        utilisateuur.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_U, java.awt.event.InputEvent.ALT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        utilisateuur.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/icons8-add_user.png"))); // NOI18N
        utilisateuur.setText("Utilisateurs");
        utilisateuur.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                utilisateuurActionPerformed(evt);
            }
        });
        formulaire.add(utilisateuur);

        horaire.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_H, java.awt.event.InputEvent.ALT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        horaire.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/icons8-overtime.png"))); // NOI18N
        horaire.setText("Horaire");
        horaire.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                horaireActionPerformed(evt);
            }
        });
        formulaire.add(horaire);

        jMenuItem3.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_O, java.awt.event.InputEvent.ALT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/icons8-literature.png"))); // NOI18N
        jMenuItem3.setText("Ouvrages");
        jMenuItem3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem3ActionPerformed(evt);
            }
        });
        formulaire.add(jMenuItem3);

        jMenuItem4.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_V, java.awt.event.InputEvent.ALT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/icons8-book_shelf.png"))); // NOI18N
        jMenuItem4.setText("Ouvrages par Matière(s)");
        jMenuItem4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem4ActionPerformed(evt);
            }
        });
        formulaire.add(jMenuItem4);

        programme.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_Y, java.awt.event.InputEvent.ALT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        programme.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/icons8-tasks.png"))); // NOI18N
        programme.setText("Programme");
        programme.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                programmeActionPerformed(evt);
            }
        });
        formulaire.add(programme);

        jMenuItem5.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_P, java.awt.event.InputEvent.ALT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/icons8-add_user_group.png"))); // NOI18N
        jMenuItem5.setText("Personnes");
        jMenuItem5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem5ActionPerformed(evt);
            }
        });
        formulaire.add(jMenuItem5);

        jMenuBar1.add(formulaire);

        processus.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/icons8-settings.png"))); // NOI18N
        processus.setText("Processus");

        inscription.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_I, java.awt.event.InputEvent.ALT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        inscription.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/icons8-edit_user.png"))); // NOI18N
        inscription.setText("Inscriptions");
        inscription.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                inscriptionActionPerformed(evt);
            }
        });
        processus.add(inscription);

        entrer_des_notes.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_N, java.awt.event.InputEvent.ALT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        entrer_des_notes.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/icons8-edit_property.png"))); // NOI18N
        entrer_des_notes.setText("Entrer des Notes");
        entrer_des_notes.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                entrer_des_notesActionPerformed(evt);
            }
        });
        processus.add(entrer_des_notes);

        bulletin.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_B, java.awt.event.InputEvent.ALT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        bulletin.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/icons8-overview_pages_2.png"))); // NOI18N
        bulletin.setText("Préparation des Bulletins");
        bulletin.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bulletinActionPerformed(evt);
            }
        });
        processus.add(bulletin);

        jMenuItem7.setText("jMenuItem7");
        processus.add(jMenuItem7);

        jMenuBar1.add(processus);

        economat.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/icons8-usd.png"))); // NOI18N
        economat.setText("Economat");

        collection.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_T, java.awt.event.InputEvent.ALT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        collection.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/icons8-coins.png"))); // NOI18N
        collection.setText("Collection");
        collection.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                collectionActionPerformed(evt);
            }
        });
        economat.add(collection);

        paiement.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_P, java.awt.event.InputEvent.ALT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        paiement.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/icons8-money_bag.png"))); // NOI18N
        paiement.setText("Paiement");
        paiement.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                paiementActionPerformed(evt);
            }
        });
        economat.add(paiement);

        jMenuBar1.add(economat);

        intelligente.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/icons8-find_matching_job.png"))); // NOI18N
        intelligente.setText("Consultations");

        jMenuItem19.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_K, java.awt.event.InputEvent.ALT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem19.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/icons8-binoculars.png"))); // NOI18N
        jMenuItem19.setText("Consultation Intélligente");
        jMenuItem19.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem19ActionPerformed(evt);
            }
        });
        intelligente.add(jMenuItem19);

        jMenuBar1.add(intelligente);

        rapport.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/icons8-parse_resumes.png"))); // NOI18N
        rapport.setText("Rapports");

        rprincipal.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_R, java.awt.event.InputEvent.ALT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        rprincipal.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/icons8-open_resume.png"))); // NOI18N
        rprincipal.setText("Rapport Principal");
        rprincipal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rprincipalActionPerformed(evt);
            }
        });
        rapport.add(rprincipal);

        jMenuBar1.add(rapport);

        outil.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/icons8-maintenance.png"))); // NOI18N
        outil.setText("Outils");

        aide.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_H, java.awt.event.InputEvent.ALT_MASK));
        aide.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/icons8-services.png"))); // NOI18N
        aide.setText("Paramètres");
        aide.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                aideActionPerformed(evt);
            }
        });
        outil.add(aide);

        personaliser.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_K, java.awt.event.InputEvent.ALT_MASK));
        personaliser.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/icons8-support.png"))); // NOI18N
        personaliser.setText("Personaliser");
        personaliser.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                personaliserActionPerformed(evt);
            }
        });
        outil.add(personaliser);

        calendrier.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_G, java.awt.event.InputEvent.ALT_MASK));
        calendrier.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/icons8-calendar_plus.png"))); // NOI18N
        calendrier.setText("Calendrier");
        calendrier.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                calendrierActionPerformed(evt);
            }
        });
        outil.add(calendrier);

        notification.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_N, java.awt.event.InputEvent.ALT_MASK));
        notification.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/icons8-advertising.png"))); // NOI18N
        notification.setText("Notification");
        notification.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                notificationActionPerformed(evt);
            }
        });
        outil.add(notification);

        jMenuBar1.add(outil);

        jMenu9.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/icons8-bulleted_list.png"))); // NOI18N
        jMenu9.setText("Options");

        verrouiller.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_L, java.awt.event.InputEvent.CTRL_MASK));
        verrouiller.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/icons8-forgot_password_1.png"))); // NOI18N
        verrouiller.setText("Vérrouiller");
        verrouiller.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                verrouillerActionPerformed(evt);
            }
        });
        jMenu9.add(verrouiller);

        sortir.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_C, java.awt.event.InputEvent.CTRL_MASK));
        sortir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/icons8-user_group_man_man.png"))); // NOI18N
        sortir.setText("Changer Utilisateur");
        sortir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sortirActionPerformed(evt);
            }
        });
        jMenu9.add(sortir);

        jMenuItem2.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_E, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/icons8-shutdown.png"))); // NOI18N
        jMenuItem2.setText("Fermer Le Système");
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem2ActionPerformed(evt);
            }
        });
        jMenu9.add(jMenuItem2);

        jMenuBar1.add(jMenu9);

        avan.setText("jMenu5");
        avan.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jMenuBar1.add(avan);

        academique.setText("jMenu5");
        academique.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jMenuBar1.add(academique);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(desktop)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(desktop)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void sortirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_sortirActionPerformed
        int aa = JOptionPane.showConfirmDialog(null, "Etes-vous sur de vouloir changer d'utilisateur?", "Question", JOptionPane.YES_OPTION);
        if (aa == JOptionPane.YES_OPTION) {
            this.dispose();
            Login lg = new Login();
            //Login.institution.setEnabled(false);
            lg.show();
            //Login.institution.setSelectedItem(Variables.Empresa);
        } else {
            if (Variables.Etteindre.equals("Autoriser")) {
                Verrouillage vr = new Verrouillage(this, rootPaneCheckingEnabled);
                vr.show();
                vr.toFront();
                vr.setAlwaysOnTop(true);
            }

        }

    }//GEN-LAST:event_sortirActionPerformed

    private void bulletinActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bulletinActionPerformed
        Bulletin bl = new Bulletin();
        desktop.add(bl);
        int x = (desktop.getWidth() / 2) - bl.getWidth() / 2;
        int y = (desktop.getHeight() / 2) - bl.getHeight() / 2;
        bl.setLocation(x, y);
        bl.toFront();
        bl.setVisible(true);

    }//GEN-LAST:event_bulletinActionPerformed

    private void verrouillerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_verrouillerActionPerformed
        Verrouillage vr = new Verrouillage(this, rootPaneCheckingEnabled);
        vr.show();
        vr.toFront();
        vr.setAlwaysOnTop(true);
    }//GEN-LAST:event_verrouillerActionPerformed

    private void classeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_classeActionPerformed
        Classe cl = new Classe();
        desktop.add(cl);
        int x = (desktop.getWidth() / 2) - cl.getWidth() / 2;
        int y = (desktop.getHeight() / 2) - cl.getHeight() / 2;
        cl.setLocation(x, y);
        cl.toFront();
        cl.setVisible(true);
    }//GEN-LAST:event_classeActionPerformed

    private void eleveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_eleveActionPerformed
        Eleves el = new Eleves();
        desktop.add(el);
        int x = (desktop.getWidth() / 2) - el.getWidth() / 2;
        int y = (desktop.getHeight() / 2) - el.getHeight() / 2;
        el.setLocation(x, y);
        el.toFront();
        el.setVisible(true);
    }//GEN-LAST:event_eleveActionPerformed

    private void inscriptionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_inscriptionActionPerformed
        Inscription in = new Inscription();
        desktop.add(in);
        int x = (desktop.getWidth() / 2) - in.getWidth() / 2;
        int y = (desktop.getHeight() / 2) - in.getHeight() / 2;
        in.setLocation(x, y);
        in.toFront();
        in.setVisible(true);
    }//GEN-LAST:event_inscriptionActionPerformed

    private void entrer_des_notesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_entrer_des_notesActionPerformed
        Entrer_note en = new Entrer_note();
        desktop.add(en);
        int x = (desktop.getWidth() / 2) - en.getWidth() / 2;
        int y = (desktop.getHeight() / 2) - en.getHeight() / 2;
        en.setLocation(x, y);
        en.toFront();
        en.setVisible(true);
    }//GEN-LAST:event_entrer_des_notesActionPerformed

    private void rprincipalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rprincipalActionPerformed
        Rapport_Principale rp = new Rapport_Principale();
        desktop.add(rp);
        int x = (desktop.getWidth() / 2) - rp.getWidth() / 2;
        int y = (desktop.getHeight() / 2) - rp.getHeight() / 2;
        rp.setLocation(x, y);
        rp.toFront();
        rp.setVisible(true);
    }//GEN-LAST:event_rprincipalActionPerformed

    private void matiereActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_matiereActionPerformed
        Matiere m = new Matiere();
        desktop.add(m);
        int x = (desktop.getWidth() / 2) - m.getWidth() / 2;
        int y = (desktop.getHeight() / 2) - m.getHeight() / 2;
        m.setLocation(x, y);
        m.toFront();
        m.setVisible(true);
    }//GEN-LAST:event_matiereActionPerformed

    private void employerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_employerActionPerformed
        Employer em = new Employer();
        desktop.add(em);
        int x = (desktop.getWidth() / 2) - em.getWidth() / 2;
        int y = (desktop.getHeight() / 2) - em.getHeight() / 2;
        em.setLocation(x, y);
        em.toFront();
        em.setVisible(true);
    }//GEN-LAST:event_employerActionPerformed

    private void utilisateuurActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_utilisateuurActionPerformed
        Utilisateurs ut = new Utilisateurs();
        desktop.add(ut);
        int x = (desktop.getWidth() / 2) - ut.getWidth() / 2;
        int y = (desktop.getHeight() / 2) - ut.getHeight() / 2;
        ut.setLocation(x, y);
        ut.toFront();
        ut.setVisible(true);
    }//GEN-LAST:event_utilisateuurActionPerformed

    private void professeurActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_professeurActionPerformed
        Professeur pr = new Professeur();
        desktop.add(pr);
        int x = (desktop.getWidth() / 2) - pr.getWidth() / 2;
        int y = (desktop.getHeight() / 2) - pr.getHeight() / 2;
        pr.setLocation(x, y);
        pr.toFront();
        pr.setVisible(true);
    }//GEN-LAST:event_professeurActionPerformed

    private void horaireActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_horaireActionPerformed
        Horaire pr = new Horaire();
        desktop.add(pr);
        int x = (desktop.getWidth() / 2) - pr.getWidth() / 2;
        int y = (desktop.getHeight() / 2) - pr.getHeight() / 2;
        pr.setLocation(x, y);
        pr.toFront();
        pr.setVisible(true);
    }//GEN-LAST:event_horaireActionPerformed

    private void collectionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_collectionActionPerformed
        Collection pr = new Collection();
        desktop.add(pr);
        int x = (desktop.getWidth() / 2) - pr.getWidth() / 2;
        int y = (desktop.getHeight() / 2) - pr.getHeight() / 2;
        pr.setLocation(x, y);
        pr.toFront();
        pr.setVisible(true);
    }//GEN-LAST:event_collectionActionPerformed

    private void paiementActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_paiementActionPerformed
        Paiement pr = new Paiement();
        desktop.add(pr);
        int x = (desktop.getWidth() / 2) - pr.getWidth() / 2;
        int y = (desktop.getHeight() / 2) - pr.getHeight() / 2;
        pr.setLocation(x, y);
        pr.toFront();
        pr.setVisible(true);
    }//GEN-LAST:event_paiementActionPerformed

    private void jMenuItem19ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem19ActionPerformed
        Cons_intelligente pr = new Cons_intelligente();
        desktop.add(pr);
        int x = (desktop.getWidth() / 2) - pr.getWidth() / 2;
        int y = (desktop.getHeight() / 2) - pr.getHeight() / 2;
        pr.setLocation(x, y);
        pr.toFront();
        pr.setVisible(true);
    }//GEN-LAST:event_jMenuItem19ActionPerformed

    private void desktopMouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_desktopMouseMoved
        date.setText(Mn.DATEHOURNOW());
    }//GEN-LAST:event_desktopMouseMoved

    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem2ActionPerformed
        int aa = JOptionPane.showConfirmDialog(null, "Etes-vous sur de vouloir quitter le systeme?", "Question", JOptionPane.YES_OPTION);
        if (aa == JOptionPane.YES_OPTION) {
            System.exit(0);
        }
    }//GEN-LAST:event_jMenuItem2ActionPerformed

    private void programmeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_programmeActionPerformed
        Programme cal = new Programme();
        desktop.add(cal);
        int x = (desktop.getWidth() / 2) - cal.getWidth() / 2;
        int y = (desktop.getHeight() / 2) - cal.getHeight() / 2;
        cal.setLocation(x, y);
        cal.toFront();
        cal.setVisible(true);
    }//GEN-LAST:event_programmeActionPerformed

    private void notificationActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_notificationActionPerformed
        Notification cal = new Notification();
        desktop.add(cal);
        int x = (desktop.getWidth() / 2) - cal.getWidth() / 2;
        int y = (desktop.getHeight() / 2) - cal.getHeight() / 2;
        cal.setLocation(x, y);
        cal.toFront();
        cal.setVisible(true);
    }//GEN-LAST:event_notificationActionPerformed

    private void calendrierActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_calendrierActionPerformed
        Calendrier cal = new Calendrier();
        desktop.add(cal);
        int x = (desktop.getWidth() / 2) - cal.getWidth() / 2;
        int y = (desktop.getHeight() / 2) - cal.getHeight() / 2;
        cal.setLocation(x, y);
        cal.toFront();
        cal.setVisible(true);
    }//GEN-LAST:event_calendrierActionPerformed

    private void personaliserActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_personaliserActionPerformed
        this.dispose();
        Personaliser eq = new Personaliser();
        eq.setVisible(true);
    }//GEN-LAST:event_personaliserActionPerformed

    private void aideActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_aideActionPerformed
        Aide ai = new Aide();
        desktop.add(ai);
        int x = (desktop.getWidth() / 2) - ai.getWidth() / 2;
        int y = (desktop.getHeight() / 2) - ai.getHeight() / 2;
        ai.setLocation(x, y);
        ai.toFront();
        ai.setVisible(true);
    }//GEN-LAST:event_aideActionPerformed

    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
        Classematiere cal = new Classematiere();
        desktop.add(cal);
        int x = (desktop.getWidth() / 2) - cal.getWidth() / 2;
        int y = (desktop.getHeight() / 2) - cal.getHeight() / 2;
        cal.setLocation(x, y);
        cal.toFront();
        cal.setVisible(true);
    }//GEN-LAST:event_jMenuItem1ActionPerformed

    private void jMenuItem3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem3ActionPerformed
        Ouvrages cal = new Ouvrages();
        desktop.add(cal);
        int x = (desktop.getWidth() / 2) - cal.getWidth() / 2;
        int y = (desktop.getHeight() / 2) - cal.getHeight() / 2;
        cal.setLocation(x, y);
        cal.toFront();
        cal.setVisible(true);
    }//GEN-LAST:event_jMenuItem3ActionPerformed

    private void jMenuItem4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem4ActionPerformed

        OuvragesClasse cal = new OuvragesClasse();
        desktop.add(cal);
        int x = (desktop.getWidth() / 2) - cal.getWidth() / 2;
        int y = (desktop.getHeight() / 2) - cal.getHeight() / 2;
        cal.setLocation(x, y);
        cal.toFront();
        cal.setVisible(true);
    }//GEN-LAST:event_jMenuItem4ActionPerformed

    private void jMenuItem5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem5ActionPerformed
        Person per = new Person();
        desktop.add(per);
        int x = (desktop.getWidth() / 2) - per.getWidth() / 2;
        int y = (desktop.getHeight() / 2) - per.getHeight() / 2;
        per.setLocation(x, y);
        per.toFront();
        per.show();
        per.pack();
    }//GEN-LAST:event_jMenuItem5ActionPerformed

    private void desktopMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_desktopMouseClicked
        if(evt.getClickCount() == 4){
            setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
            this.dispose();
            new Themes(null, rootPaneCheckingEnabled).show();
            setCursor(Cursor.getDefaultCursor());
        }
    }//GEN-LAST:event_desktopMouseClicked

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Menus.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Menus.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Menus.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Menus.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Menus().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenu academique;
    private javax.swing.JMenuItem aide;
    private javax.swing.JMenu avan;
    private javax.swing.JMenuItem bulletin;
    private javax.swing.JMenuItem calendrier;
    private javax.swing.JMenuItem classe;
    private javax.swing.JMenuItem collection;
    private javax.swing.JLabel date;
    private javax.swing.JLabel date1;
    private javax.swing.JLabel date2;
    private javax.swing.JLabel date3;
    private javax.swing.JLabel date4;
    public static javax.swing.JDesktopPane desktop;
    private javax.swing.JCheckBox deuxieme;
    private javax.swing.JMenu economat;
    private javax.swing.JMenuItem eleve;
    private javax.swing.JMenuItem employer;
    private javax.swing.JMenuItem entrer_des_notes;
    private javax.swing.JMenu formulaire;
    private javax.swing.JMenuItem horaire;
    private javax.swing.JMenuItem inscription;
    private javax.swing.JMenu intelligente;
    private javax.swing.JCheckBoxMenuItem jCheckBoxMenuItem1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenu jMenu3;
    private javax.swing.JMenu jMenu9;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem17;
    private javax.swing.JMenuItem jMenuItem19;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JMenuItem jMenuItem3;
    private javax.swing.JMenuItem jMenuItem4;
    private javax.swing.JMenuItem jMenuItem5;
    private javax.swing.JMenuItem jMenuItem6;
    private javax.swing.JMenuItem jMenuItem7;
    private javax.swing.JToolBar jToolBar1;
    private javax.swing.JMenuItem matiere;
    private java.awt.MenuItem menuItem1;
    private javax.swing.JMenuItem notification;
    private javax.swing.JMenu outil;
    private javax.swing.JMenuItem paiement;
    private org.edisoncor.gui.panel.Panel panel1;
    private javax.swing.JMenuItem personaliser;
    private java.awt.PopupMenu popupMenu1;
    private javax.swing.JCheckBox premier;
    private javax.swing.JMenu processus;
    private javax.swing.JMenuItem professeur;
    private javax.swing.JMenuItem programme;
    private javax.swing.JMenu rapport;
    private javax.swing.JMenuItem rprincipal;
    public static javax.swing.JMenuItem sortir;
    private javax.swing.JCheckBox troisieme;
    private javax.swing.JCheckBox troisieme1;
    private javax.swing.JLabel utilisateur;
    private javax.swing.JMenuItem utilisateuur;
    private javax.swing.JMenuItem verrouiller;
    // End of variables declaration//GEN-END:variables
}
