package Methods;
import com.mysql.jdbc.Blob;
import com.toedter.calendar.JDateChooser;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Point;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.DefaultTableModel;
import javax.swing.text.DefaultFormatterFactory;
import javax.swing.text.MaskFormatter;
import org.imgscalr.Scalr;
import org.imgscalr.Scalr.Method;
import org.imgscalr.Scalr.Mode;


public class Manipulation {
    
    public static String error_db="";
     public static Connection conexion;
     private int longueur;
     private JFileChooser ls;
     public static InputStream lp;
     public static int longeur;
    Connections db= new Connections().Conectar();
    
    String EMAIL_PATTERN = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
   //******************************* Insert, Update, Delete Method *************************************************

    public boolean MANIPULEDB(String query) {

        try {
            Statement sentencia = db.getConexion().createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            sentencia.executeUpdate(query);
        } catch (SQLException e) {
            Logger.getLogger(Manipulation.class.getName()).log(Level.SEVERE, null, e);
            return false;

        }
        return true;
    }

// *************************************** Select Method *********************************************************

    public ResultSet SEARCHDATA(String query) {
        ResultSet resultado;

        try {
            Statement sentencia = db.getConexion().createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
            resultado = sentencia.executeQuery(query);
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
        return resultado;
    }
// ********************************************** End *********************************************************
public int MAXCODE(String Campo, String Tabla) {
        int codigo = 0;
        try {
            ResultSet resultado;

            String query = "Select Max(" + Campo + ")as jcodigo from " + Tabla + "";
            System.out.println("qu "+query);
            Statement sentencia = db.getConexion().createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
            resultado = sentencia.executeQuery(query);
            if (resultado.first()) {
                int cod = resultado.getInt("jcodigo");
                codigo = cod + 1;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return codigo;
    }

// *********************************************************** End *******************************************************
public String SEARCHCODE(String Codigo, String Descripcion, String lcodigo, String Tabla) {
        String mcodigo = "";
        try {
            ResultSet resultado;
            String query = "Select " + Codigo + " from " + Tabla + " where " + illegalCharacterOnString(Descripcion) + " = '" + illegalCharacterOnString(lcodigo).trim() + "' ";
            System.out.println("df "+query);
            Statement sentencia = db.getConexion().createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
            System.out.println("query " + query);
            resultado = sentencia.executeQuery(query);
            if (resultado.first()) {
                mcodigo = resultado.getString(Codigo);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return mcodigo;
    }

    public int SEARCHCODES(String Codigo, String Descripcion, String lcodigo, String Tabla) {
        int mcodigo = 0;
        try {
            ResultSet resultado;
            String query = "Select " + Codigo + " from " + Tabla + " where " + Descripcion + " = '" + lcodigo + "'";
            Statement sentencia = db.getConexion().createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
            resultado = sentencia.executeQuery(query);
            if (resultado.first()) {
                mcodigo = resultado.getInt(Codigo);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return mcodigo;
    }

    public int SEARCHCODES(String query, String Codigo) {
        int mcodigo = 0;
        try {
            ResultSet resultado;
            Statement sentencia = db.getConexion().createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
            resultado = sentencia.executeQuery(query);
            if (resultado.first()) {
                mcodigo = resultado.getInt(Codigo);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return mcodigo;
    }
    public String CODESEARCH(String query, String Codigo) {
            String mcodigo ="";
            try {
                ResultSet resultado;
                Statement sentencia = db.getConexion().createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
                resultado = sentencia.executeQuery(query);
                if (resultado.first()) {
                    mcodigo = resultado.getString(Codigo);
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            return mcodigo;
        }
//--------------------------------------------------------------------------------------------------------
public int SEARCHCODE(String Codigo, String Descripcion, String Descripcion1, String lcodigo, String lcode, String Tabla) {
        int mcodigo = 0;
        try {
            ResultSet resultado;
            String query = "Select " + Codigo + " from " + Tabla + " where " + Descripcion + " = '" + lcodigo.trim() + "' and " + Descripcion1 + " = '" + lcode.trim().trim() + "' ";
            System.out.println("de "+query);
            Statement sentencia = db.getConexion().createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
            resultado = sentencia.executeQuery(query);
            if (resultado.first()) {
                mcodigo = resultado.getInt(Codigo);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return mcodigo;
    }

    public int SEARCHCODE(String query, int Codigo) {
        int mcodigo = 0;
        try {
            ResultSet resultado;
            Statement sentencia = db.getConexion().createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
            resultado = sentencia.executeQuery(query);
            if (resultado.first()) {
                mcodigo = resultado.getInt(Codigo);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return mcodigo;
    }

    public String SEARCHCODE(String query, String Codigo) {
        String mcodigo = "";
        try {
            ResultSet resultado;
            Statement sentencia = db.getConexion().createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
            resultado = sentencia.executeQuery(query);
            if (resultado.first()) {
                mcodigo = resultado.getString(Codigo);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return mcodigo;
    }
    
    public int ResultsetCount(String query) {
        int tot = 0;
        try {
            Statement comando = db.getConexion().createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            ResultSet rs = comando.executeQuery(query);
            if (rs.last()) {
                tot = rs.getRow();
            }
        } catch (SQLException ex) {
            Logger.getLogger(Manipulation.class.getName()).log(Level.SEVERE, null, ex);
            System.err.println("** Error Coneksyon #1 **\n" + ex.getMessage());
            JOptionPane.showMessageDialog(null, ex.getMessage());
        }
        return tot;
    }
// ******************************************************** End ***********************************


    public ResultSet Rezulta(String query) {
        try {
            Statement comando = db.getConexion().createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            ResultSet rs = comando.executeQuery(query);
            return rs;
        } catch (SQLException ex) {
            Logger.getLogger(Manipulation.class.getName()).log(Level.SEVERE, null, ex);
            System.err.println("** Error Coneksyon #1 **\n" + ex.getMessage());
            return null;
        }
    }
// ******************************************************** End ***********************************


//------------------------------------------------------------------
 public void ONLYLETTERS(KeyEvent ke) {
             char c=ke.getKeyChar();
          if(Character.isDigit(c)) {
              //getToolkit();
              ke.consume();
              JOptionPane.showMessageDialog(null,"Cette zone de texte accepte seulement les lettres de A a Z. ");
          }           
        }

//------------------------------------------------------------------
  public void ONLYNUMBERS(KeyEvent ke) {
            char c=ke.getKeyChar();
          if(Character.isLetter(c)) {
              //getToolkit();
              ke.consume();
              JOptionPane.showMessageDialog(null,"Cette zone de texte accepte seulement les chiffres de 0 a 9.");
          }
        } 
   //-------------------------------------------------------------------------- 
  public void PHONE_FROMAT(JFormattedTextField txt){
     try { 
         txt.setFormatterFactory(new DefaultFormatterFactory(new MaskFormatter("(###) #### ####")));
     } catch (ParseException ex) {
         Logger.getLogger(Manipulation.class.getName()).log(Level.SEVERE, null, ex);
     }
  }
  //----------------------------------------------------------------------------------------
   public void DATE_FORMAT(JFormattedTextField txt){
     try { 
         txt.setFormatterFactory(new DefaultFormatterFactory(new MaskFormatter("##/##/####")));
     } catch (ParseException ex) {
         Logger.getLogger(Manipulation.class.getName()).log(Level.SEVERE, null, ex);
     }
  }
   //------------------------------------------------------------------------------
   public String DATENOW(){
        SimpleDateFormat sdf = new SimpleDateFormat(" EEEE dd MMM yyyy");
        Calendar cal = Calendar.getInstance();
        String fch = sdf.format(cal.getTime());
        return fch;
     }
   //------------------------------------------------------------------------------
   public String DATEHOURNOW(){
        SimpleDateFormat sdf = new SimpleDateFormat(" EEEE dd MMM yyyy HH:mm:ss",Locale.FRANCE);
        Calendar cal = Calendar.getInstance();
        String fch = sdf.format(cal.getTime());
        return fch;
     }
   //---------------------------------------------------------------------------------------
     public void HOUR_FORMAT(JFormattedTextField txt){
     try { 
         txt.setFormatterFactory(new DefaultFormatterFactory(new MaskFormatter("##:##:##")));
     } catch (ParseException ex) {
         Logger.getLogger(Manipulation.class.getName()).log(Level.SEVERE, null, ex);
     }
  }
  
    //---------------------------------------------------------------------------------------
     public String GET_DATE(JDateChooser fecha){
     SimpleDateFormat fc= new SimpleDateFormat("dd-MM-yyyy");
     if(fecha.getDate()!=null){
     return fc.format(fecha.getDate());
     }else{
     return null;
     }
    }
     //-------------------------------------------------------------------------------
       public String HOURNOW(){
         SimpleDateFormat sdff = new SimpleDateFormat("HH:mm:ss");
         Calendar cal = Calendar.getInstance();
         String hr = sdff.format(cal.getTime());
        return hr; 
  }
     //------------------------------------------------------------------------------------
     public void DATENOW_FORMAT(JFormattedTextField txt){
     try {
        txt.setFormatterFactory(new DefaultFormatterFactory(new MaskFormatter("##/##/####")));
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Calendar cal = Calendar.getInstance();
        String fch = sdf.format(cal.getTime());
        txt.setText(fch);
     } catch (ParseException ex) {
         Logger.getLogger(Manipulation.class.getName()).log(Level.SEVERE, null, ex);
     }
     }
      //-----------------------------------------------------------------------------------
        public void HOURNOW_FORMAT(JFormattedTextField txt){
     try {
         txt.setFormatterFactory(new DefaultFormatterFactory(new MaskFormatter("##:##:##")));
         SimpleDateFormat sdff = new SimpleDateFormat("HH:mm:ss");
         Calendar cal = Calendar.getInstance();
         String hr = sdff.format(cal.getTime());
         txt.setText(hr); 
     } catch (ParseException ex) {
         Logger.getLogger(Manipulation.class.getName()).log(Level.SEVERE, null, ex);
     }
   } 
   //------------------------------------------------------------------------------------------
      public void DATEHOURNOW_FORMAT(JFormattedTextField txt){
     try {
        txt.setFormatterFactory(new DefaultFormatterFactory(new MaskFormatter("##/##/####  ##:##:##")));
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        SimpleDateFormat sdff = new SimpleDateFormat("HH:mm:ss");
        Calendar cal = Calendar.getInstance();
        String fch = sdf.format(cal.getTime());
        String hr = sdff.format(cal.getTime()); 
        txt.setText(fch +hr);
     } catch (ParseException ex) {
         Logger.getLogger(Manipulation.class.getName()).log(Level.SEVERE, null, ex);
     }
     }
//------------------------------------------------------------------------------------------------------
  public boolean VALIDATEMAIL(String email,JTextField k) {
    boolean rep = true;
    try{
	    // Compiles the given regular expression into a pattern.
	    Pattern pattern = Pattern.compile(EMAIL_PATTERN);
	    // Match the given input against this pattern
	    Matcher matcher = pattern.matcher(email);
	    if (!matcher.matches()){
                JOptionPane.showMessageDialog(null, "S'il vous plait vérifier bien le format du courrier .");
                rep = false;
                k.requestFocus();
            }

	}catch(Exception e){
		e.printStackTrace();
	}
        return rep;	
}
  
public boolean VALIDATUSER(String email,JTextField k) {
    boolean rep = true;
    try{
	    Pattern pattern = Pattern.compile(EMAIL_PATTERN);
	    Matcher matcher = pattern.matcher(email);
            if(!k.getText().trim().isEmpty()){
	    if (!matcher.matches()){
                k.setText(k.getText()+"@scolaris.edu");
                rep = true;
            }
            }
	}catch(Exception e){
		e.printStackTrace();
	}
        return rep;	
}
  //-------------------------------------------------------------------------------------------
    public int SELECT_BUTTON(JTable tbl,int aa){
        try {
            int selectedRow = tbl.getSelectedRow();
        int columna1 = (int) tbl.getValueAt(selectedRow, 0);
            aa=columna1;
       
        } catch (Exception e) {
           JOptionPane.showMessageDialog(null, "Vous devriez séléctionner une ligne dans la table.");  
        }
         return aa;
    }
    
     public String SELECTS_BUTTON(JTable tbl,String aa){
        try {
            int selectedRow = tbl.getSelectedRow();
        String columna1 = (String) tbl.getValueAt(selectedRow, 1);
            aa=columna1;
       
        } catch (Exception e) {
           JOptionPane.showMessageDialog(null, "Vous devriez séléctionner une ligne dans la table.");  
        }
         return aa;
    }
     
   public String SELECT_BUTTONS(JTable tbl,String aa){
        try {
            int selectedRow = tbl.getSelectedRow();
        String columna1 = (String) tbl.getValueAt(selectedRow, 0);
            aa=columna1;
       
        } catch (Exception e) {
           JOptionPane.showMessageDialog(null, "Vous devriez séléctionner une ligne dans la table.");  
        }
         return aa;
    }
    
   public String SELLECT_BUTTON(String aa,JTable tbl){
        try {
            int selectedRow = tbl.getSelectedRow();
            String columna1 = (String) tbl.getValueAt(selectedRow, 0);
            aa=columna1;
       
        } catch (Exception e) {
           JOptionPane.showMessageDialog(null, "Vous devriez séléctionner une ligne dans la table.");  
        }
         return aa;
    }
   
   //--------------------------------------------------------------------------------------------------------
    public void NEW_BUTTON(JTextField txt[],String Table,String code){
       int aa = MAXCODE(code, Table);
        txt[0].setText(String.valueOf(aa));
        txt[1].requestFocus();  
       
    }
    //--------------------------------------------------------------------------------------------
    public void SAVE_BUTTON(String Table, String vect[],String txt[]){
        String a="";
        String b="";
         String c="";
         int aa=JOptionPane.showConfirmDialog(null,"Etes-vous sur de vouloir enregistrer cet information?","Question", JOptionPane.YES_OPTION);
        for(int i=0;i<vect.length;i++){
            if(i==0){
                a+=vect[i]; 
                c+=txt[i];
            }else{
               a+=","+"'"+vect[i]+"'"; 
               b+=txt[i]+"="+"'"+vect[i]+"'";
               c+=","+txt[i]; 
               if(i<vect.length-1){
                   b+=",";
               }
        }
        }
        String query="Update "+Table+" set "+b+" where "+txt[0]+"="+vect[0]+"; insert into "+Table+"("+c+")"
                + " select "+a+" where not exists(select "+txt[0]+" from "+Table+" where "+txt[0]+"="+vect[0]+")";
        if(aa==JOptionPane.YES_OPTION){
           // System.out.println(query);
         MANIPULEDB(query);  
         JOptionPane.showMessageDialog(null,"Donnees enregistrer avec succes.");
        } 
    }
    //-----------------------------------------------------------------------------------------------
    public void REMOVE_TABLE(JTable tbl, DefaultTableModel md){
         try {
             System.out.println("line "+tbl.getSelectedRow());
            md.removeRow(tbl.getSelectedRow());
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Vous devriez selectionner une ligne dans la table.");
            e.printStackTrace();
        }
    }
    //---------------------------------------------------------------------------------------------------
    public void TOUCH_RELEASE(String query, JTable tbl,DefaultTableModel md){
        md.setRowCount(0);
        try {
            ResultSet rs= SEARCHDATA(query);
        while(rs.next())
        {
            Object[] fila = new Object[tbl.getColumnCount()];
            for (int i = 0; i < tbl.getColumnCount(); i++) 
            {
           fila[i]=rs.getObject(i+1);
            }
            md.addRow(fila);   
        }
       } catch (Exception e) {
       } 
    }
    //---------------------------------------------------------------------------------------------------
    public void ADD_TABLE(DefaultTableModel md,String vect[]){
          
          String[] fila = new String[vect.length];
          for(int i=0;i<vect.length;i++){
              fila[i]=vect[i];
          }
                md.addRow(fila);
    }
    //-----------------------------------------------------------------------------------------------
    
    public void DELETE_BUTTON(JTextField txt,String Table,String campo){
         int ab=JOptionPane.showConfirmDialog(null,"Etes-vous sur de vouloir effacer cet information?","Question", JOptionPane.YES_OPTION);
        if(ab==JOptionPane.YES_OPTION){
           int aa=Integer.parseInt(txt.getText());
           String query="delete from "+Table+" where "+campo+"='"+aa+"' ";
            System.out.println(query);
           MANIPULEDB(query);
           JOptionPane.showMessageDialog(null,"Information effacer avec succes.");
        }  
    }
    
     public void DELETE_BUTTON(String query){
         int ab=JOptionPane.showConfirmDialog(null,"Etes-vous sur de vouloir effacer cet information?","Question", JOptionPane.YES_OPTION);
        if(ab==JOptionPane.YES_OPTION){
           MANIPULEDB(query);
           JOptionPane.showMessageDialog(null,"Information effacer avec succes.");
        }  
    }
    
   public void DELETE_BUTTON(String Table, JTextField txt, String campo){
         int ab=JOptionPane.showConfirmDialog(null,"Etes-vous sur de vouloir effacer cet information?","Question", JOptionPane.YES_OPTION);
        if(ab==JOptionPane.YES_OPTION){
           String aa=txt.getText();
           String query="delete from "+Table+" where "+campo+"='"+aa+"' ";
           MANIPULEDB(query);
           JOptionPane.showMessageDialog(null,"Information effacer avec succes.");
        }  
    }
 
    public void DELETE_BUTTON(String Table, JTextField txt,JTextField txt1,String campo, String campo1){
      int ab=JOptionPane.showConfirmDialog(null,"Etes-vous sur de vouloir effacer cet information?","Question", JOptionPane.YES_OPTION);
        if(ab==JOptionPane.YES_OPTION){
           int aa=Integer.parseInt(txt.getText());
           String bb=txt1.getText();
           String query="delete from "+Table+" where "+campo+"="+aa+" and "+campo1+"='"+bb+"'";
           MANIPULEDB(query);
             JOptionPane.showMessageDialog(null,"Information effacer avec succes.");
        }
    }
    //-------------------------------------------------------------------------------------------------------------------
        public void FULLCOMBO(JComboBox combo,JComboBox combo1, String desc,String desc1, String Table,String Table1,String code){
           combo.removeAllItems();
           combo.addItem("Selectionnez");
            
        try {
         String query="Select "+Table+"."+desc+" from "+Table+" left join "+Table1+" on "+Table+"."+code+"="+Table1+"."+code+" "
                 + " where "+Table1+"."+desc1+"='"+combo1.getSelectedItem()+"' order by "+desc+" asc";
           ResultSet rs=SEARCHDATA(query);
           while(rs.next()){
           combo.addItem(rs.getString(desc));
           }
           combo.setSelectedItem("Selectionnez");
        } catch (SQLException ex) {}
    }
    
     public void FULLCOMBO(JComboBox combo, String desc, String Table){
           combo.removeAllItems();
           combo.addItem("Selectionnez");
        try {
         String query="Select "+desc+" from "+Table+" order by "+desc+" asc";
           ResultSet rs=SEARCHDATA(query);
           while(rs.next()){
           combo.addItem(rs.getString(desc));
           }
           combo.setSelectedItem("Selectionnez");
        } catch (SQLException ex) {}
    }
     

     
      public void FULLSCOMBO(JComboBox combo, String desc,String des,String par, String Table){
           combo.removeAllItems();
           combo.addItem("Selectionnez");
        try {
         String query="Select "+desc+" from "+Table+" where "+des+"='"+par+"' order by "+desc+" asc";
         //System.out.println(query);
           ResultSet rs=SEARCHDATA(query);
           while(rs.next()){
           combo.addItem(rs.getString(desc));
           }
           combo.setSelectedItem("Selectionnez");
        } catch (SQLException ex) {}
    }
      
      public void FULLCOMBOACADEMIQUE(JComboBox combo, String desc, String Table){
           combo.removeAllItems();
        try {
         String query="Select "+desc+" from "+Table+"  order by "+desc+" asc";
         //System.out.println(query);
           ResultSet rs=SEARCHDATA(query);
           while(rs.next()){
           combo.addItem(rs.getString(desc));
           }
        } catch (SQLException ex) {}
    }
      
       public void FULLCOMBO(JComboBox combo, String desc,String des,String par, String Table){
           combo.removeAllItems();
           combo.addItem("Selectionnez");
        try {
         String query="Select "+desc+" from "+Table+" where "+des+"='"+par+"' order by "+desc+" asc";
         //System.out.println(query);
           ResultSet rs=SEARCHDATA(query);
           while(rs.next()){
           combo.addItem(rs.getString(desc));
           }
           combo.setSelectedItem("Selectionnez");
        } catch (SQLException ex) {}
    }
      
      public void FULLCOMBO(JComboBox combo, String desc,String des,String des1,String par,String par1, String Table){
           combo.removeAllItems();
           combo.addItem("Selectionnez");
        try {
         String query="Select  "+desc+" from "+Table+" where "+des+"='"+par+"' and "+des1+"='"+par1+"'order by "+desc+" asc";
         //System.out.println(query);
           ResultSet rs=SEARCHDATA(query);
           while(rs.next()){
           combo.addItem(rs.getString(desc));
           }
           combo.setSelectedItem("Selectionnez");
        } catch (SQLException ex) {}
    }
   
       public void FULLCOMBO(String query,JComboBox combo,String desc ){
           combo.removeAllItems();
           combo.addItem("Selectionnez");
        try {
         //System.out.println(query);
           ResultSet rs=SEARCHDATA(query);
           while(rs.next()){
           combo.addItem(rs.getString(desc));
           }
           combo.addItem("Autres");
           combo.setSelectedItem("Selectionnez");
        } catch (SQLException ex) {}
    }
  
   public void FULLCOMBO_COMPOSE(String query,JComboBox combo,String desc,String des ){
           combo.removeAllItems();
           combo.addItem("Selectionnez");
        try {
         //System.out.println(query);
           ResultSet rs=SEARCHDATA(query);
           while(rs.next()){
           String val=rs.getString(des);
           if(val==""){
             combo.addItem(rs.getString(desc));
           }else{
            combo.addItem(rs.getString(desc));
             combo.addItem(rs.getString(des));
           }
           }
          combo.setSelectedItem("Selectionnez");
        } catch (SQLException ex) {}
    }   
    
       
       public void FULLCOMBO(String Table,JComboBox combo,String desc,String desc1 ){
           String query="Select "+desc+","+desc1+" from "+Table+" order by "+desc+" asc";
           combo.removeAllItems();
           combo.addItem("Selectionnez");
        try {
         //System.out.println(query);
           ResultSet rs=SEARCHDATA(query);
           while(rs.next()){
               try {
                   if(!rs.getString(desc).isEmpty() ){
                       if(rs.getString(desc1).isEmpty() || rs.getString(desc1).equals("null")){
            combo.addItem(rs.getString(desc)); System.out.println(rs.getString(desc));
                       }
                       else{
            combo.addItem(rs.getString(desc)+ " ("+rs.getString(desc1)+")");
                           System.out.println(rs.getString(desc)+ " ("+rs.getString(desc1)+")");
                       }
                   }
               } catch (NullPointerException e) {
               }
               
           }
           combo.setSelectedItem("Selectionnez");
        } catch (SQLException ex) {}
    }
    //------------------------------------------------------------------------------------
    public void EMPTYTABLE(JTable tbl){

       while(tbl.getRowCount()>0)
       {
         ((DefaultTableModel)tbl.getModel()).removeRow(0);
        }
    }
    //-----------------------------------------------------------------------------------------------------
    public void FULLTABLE(String query, JTable tbl,DefaultTableModel md){
            md.setRowCount(0);
          try {
              
            ResultSet rs= SEARCHDATA(query);
        while(rs.next())
        {
            Object[] fila = new Object[tbl.getColumnCount()];
            for (int i = 0; i < tbl.getColumnCount(); i++) 
            {
             fila[i]=rs.getObject(i+1);
            }
            md.addRow(fila);   
        }
       } catch (Exception e) {
       }
        
    }
       //-----------------------------------------------------------------------------------------------------
    public void FULLTABLECHECK(String query, JTable tbl,DefaultTableModel md){
            md.setRowCount(0);
          try {
              
            ResultSet rs= SEARCHDATA(query);
        while(rs.next())
        {
            Object[] fila = new Object[tbl.getColumnCount()];
            for (int i = 0; i < tbl.getColumnCount(); i++) 
            {
             fila[i]=rs.getObject(i+1);
            }
            md.addRow(fila);   
        }
       } catch (Exception e) {
       }
        
    }
   //-----------------------------------------------------------------------------------------------------
    public void EXIT_BUTTON(JFrame form){
        int aa=JOptionPane.showConfirmDialog(null,"Etes-vous sur de vouloir quitter cette page?","Question", JOptionPane.YES_OPTION);
        if(aa==JOptionPane.YES_OPTION){
            form.dispose();
        }
    }
    
    public void EXIT_BUTTON(JInternalFrame form){
        int aa=JOptionPane.showConfirmDialog(null,"Etes-vous sur de vouloir quitter cette page?","Question", JOptionPane.YES_OPTION);
        if(aa==JOptionPane.YES_OPTION){
            form.dispose();
        }
    }
    
    public void EXIT_BUTTON(JDialog form){
        int aa=JOptionPane.showConfirmDialog(null,"Etes-vous sur de vouloir quitter cette page?","Question", JOptionPane.YES_OPTION);
        if(aa==JOptionPane.YES_OPTION){
            form.dispose();
        }
    }
    //---------------------------------------------------------------------------------------------------------------------
   public String INSERT_CHECKED(JCheckBox chb){
    String lestado="";
    if(chb.isSelected()){
       lestado="1"; 
    }else{
        lestado="0";
    }
    return lestado;
   }
   
     public String INSERT_CHECKEDED(JCheckBox chb){
    String lestad="";
    if(chb.isSelected()){
       lestad="Active"; 
    }else{
        lestad="Inactive";
    }
    return lestad;
   }
   //---------------------------------------------------------------------------------------------------------------------
public Boolean CONSULT_CHECKED(String tbl,JTextField code,String[] champ){
    String lestadoo="";
  Boolean res=false;
    String query="select "+champ[0]+" from "+tbl+" where "+champ[1]+"='"+code.getText()+"'";
    System.out.println("Status check : "+query);
    ResultSet rs=SEARCHDATA(query);
        try {
            if(rs.first()){
               lestadoo=rs.getString(champ[0]); 
            }
             if(lestadoo.equals("1")){
                 res= true;
             } else {
                res= false;
             }     
        } catch (SQLException ex) {
            Logger.getLogger(Manipulation.class.getName()).log(Level.SEVERE, null, ex);
        }
   return res;
}
//------------------------------------------------------------------------------------------------------
  public void SAVE_BUTTON(String query){
         int ab=JOptionPane.showConfirmDialog(null,"Etes-vous sur de vouloir enrégistrer cette information?","Question", JOptionPane.YES_OPTION);
        if(ab==JOptionPane.YES_OPTION){
           MANIPULEDB(query);
           JOptionPane.showMessageDialog(null,"Information enrégistrer avec succès.");
        }  
    }
  //------------------------------------------------------------------------------------------------------
  public void MODIFIER(String query){
        int ab=JOptionPane.showConfirmDialog(null,"Etes-vous sur de vouloir modifier cette information?","Question", JOptionPane.YES_OPTION);
        if(ab==JOptionPane.YES_OPTION){
           MANIPULEDB(query);
           JOptionPane.showMessageDialog(null,"Information modifier avec succès.");
        }  
    }
   //------------------------------------------------------------------------------------------------------
  public String INSERTDATECHOOSER( JDateChooser jfnac){
      String formato=jfnac.getDateFormatString();
      Date date= jfnac.getDate();
      SimpleDateFormat sdf= new SimpleDateFormat(formato);
      String dat=String.valueOf(sdf.format(date));
      return dat;
    }
  
   public Date RETURNDATECHOOSER(String dat ){
       SimpleDateFormat fm= new SimpleDateFormat("EEEE dd MMM yyyy");
      Date jfnac=null;
        try {
            jfnac=fm.parse(dat);
        } catch (ParseException ex) {
            Logger.getLogger(Manipulation.class.getName()).log(Level.SEVERE, null, ex);
        }
      return jfnac;
    }
 
  //------------------------------------------------------------------------------------------------------
  public void MOUSEPRESSED(JTextField txt){
      MouseEvent Mouse_evt=null;
      JTable table= (JTable)Mouse_evt.getSource();
      Point pt= Mouse_evt.getPoint();
      int row=table.rowAtPoint(pt);
      if(Mouse_evt.getClickCount()==2){
        txt.setText(SELLECT_BUTTON(EMAIL_PATTERN, table));
      }
    }
  
    public String SELLECT_BUTTON(String aa,JTable tbl,int position){
        try {
            int selectedRow = tbl.getSelectedRow();
            String columna1 = (String) tbl.getValueAt(selectedRow, position);
            aa=columna1;
       
        } catch (Exception e) {
           JOptionPane.showMessageDialog(null, "Vous devriez séléctionner une ligne dans la table.");  
        }
         return aa;
    }
   
  public void MOUSEPRESSED(JTextField[] txt){
      MouseEvent Mouse_evt=null;
      JTable table= (JTable)Mouse_evt.getSource();
      Point pt= Mouse_evt.getPoint();
      int row=table.rowAtPoint(pt);
      if(Mouse_evt.getClickCount()==2){
          for(int i=0;i<txt.length;i++){
          txt[i].setText(SELLECT_BUTTON(EMAIL_PATTERN, table,i));
          }
      }
    }
    //------------------------------------------------------------------------------------------------------
  public String RANG(String select,String val){
      ArrayList<String> position= new ArrayList<String>();
      String pos="";
      ResultSet rs=SEARCHDATA(select);
        try {
            while(rs.next()){
            position.add(rs.getString(val));
            pos=String.valueOf(position);
            }
                
        } catch (SQLException ex) {
            Logger.getLogger(Manipulation.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println(pos);
     return pos;
    }
//------------------------------------------------------------------------------------------------------
 public void MATRICULE_FORMAT(JFormattedTextField txt){
      String sigle="";
      String query="select abrege from institution";
      ResultSet rs=SEARCHDATA(query);
          try {     
              if(rs.first()){
                   sigle=rs.getString("abrege");
               }else{
                sigle="DEF";
              }
          try {
              txt.setFormatterFactory(new DefaultFormatterFactory(new MaskFormatter(sigle+" ## ####")));
          } catch (ParseException ex) {
              Logger.getLogger(Manipulation.class.getName()).log(Level.SEVERE, null, ex);
          }
          } catch (SQLException ex) {
              Logger.getLogger(Manipulation.class.getName()).log(Level.SEVERE, null, ex);
          }
  }
 
 //------------------------------------------------------------------------------------------------------
 public void MATRICULES_FORMAT(JFormattedTextField txt){
      String sigle="";
      String query="select abrege from institution";
      ResultSet rs=SEARCHDATA(query);
          try {     
              if(rs.first()){
                   sigle=rs.getString("abrege");
               }else{
                sigle="DEF";
              }
          try {
              txt.setFormatterFactory(new DefaultFormatterFactory(new MaskFormatter(sigle+"EMP ####")));
          } catch (ParseException ex) {
              Logger.getLogger(Manipulation.class.getName()).log(Level.SEVERE, null, ex);
          }
          } catch (SQLException ex) {
              Logger.getLogger(Manipulation.class.getName()).log(Level.SEVERE, null, ex);
          }
  }
  //------------------------------------------------------------------------------------------------------
  public void HEUR_FORMAT(JFormattedTextField txt){
        try {
         txt.setFormatterFactory(new DefaultFormatterFactory(new MaskFormatter("##:##")));
         txt.setText("00:00");
     } catch (ParseException ex) {
         Logger.getLogger(Manipulation.class.getName()).log(Level.SEVERE, null, ex);
     }
  }
  //------------------------------------------------------------------------------------------------------
   public void USER_FORMAT(JFormattedTextField txt){
        try {
         txt.setFormatterFactory(new DefaultFormatterFactory(new MaskFormatter("***************@scolaris.edu")));
     } catch (ParseException ex) {
         Logger.getLogger(Manipulation.class.getName()).log(Level.SEVERE, null, ex);
     }
  }
  //------------------------------------------------------------------------------------------------------
public void CLASSE_PRIMAIRE_FORMAT(JFormattedTextField txt){
        try {
         txt.setFormatterFactory(new DefaultFormatterFactory(new MaskFormatter("******Année Fond.")));
         txt.setText("      Année Fond.");
     } catch (ParseException ex) {
         Logger.getLogger(Manipulation.class.getName()).log(Level.SEVERE, null, ex);
     }
  }
  //------------------------------------------------------------------------------------------------------
public void CLASSE_SECONDAIRE_FORMAT(JFormattedTextField txt){
        try {
         txt.setFormatterFactory(new DefaultFormatterFactory(new MaskFormatter("**************")));
     } catch (ParseException ex) {
         Logger.getLogger(Manipulation.class.getName()).log(Level.SEVERE, null, ex);
     }
  }
  //------------------------------------------------------------------------------------------------------
public void CLASSE_PETIT_FORMAT(JFormattedTextField txt){
        try {
         txt.setFormatterFactory(new DefaultFormatterFactory(new MaskFormatter("**************")));
     } catch (ParseException ex) {
         Logger.getLogger(Manipulation.class.getName()).log(Level.SEVERE, null, ex);
     }
  }
  //------------------------------------------------------------------------------------------------------
 //------------------------------------------------------------------------------------------------------
    public void autoComplete(String txt, String tabla, String campo, JTextField textField) {
        ArrayList name = new ArrayList();
        ResultSet rs = Rezulta("select " + campo + " from " + tabla + " ");
        try {
            while (rs.next()) {
                String cadena = rs.getString(campo);
                name.add(cadena);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Manipulation.class.getName()).log(Level.SEVERE, null, ex);
        }

        String complete = "";
        int start = txt.length();
        int last = txt.length();
        int a;

        for (a = 0; a < name.size(); a++) {
            if (name.get(a).toString().startsWith(txt) || name.get(a).toString().toLowerCase().startsWith(txt) || name.get(a).toString().toUpperCase().startsWith(txt)) {
                complete = name.get(a).toString();
                last = complete.length();
                break;
            }
        }
        if (last > start) {
            textField.setBackground(Color.WHITE);
            textField.setText(complete);
            textField.setCaretPosition(last);
            textField.moveCaretPosition(start);
        }
    }

    //------------------------------------------------------------------------------------------------------
            public Date HORA(String dat) {
                SimpleDateFormat fm = new SimpleDateFormat("HH:mm:ss");
                Date tim = null;
                try {
                    tim = fm.parse(dat);
                } catch (ParseException ex) {
                    Logger.getLogger(Manipulation.class.getName()).log(Level.SEVERE, null, ex);
                }
                return tim;
            }
            
            public String HOURFormat(JSpinner spin) {
                SimpleDateFormat sdff = new SimpleDateFormat("HH:mm:ss", Locale.FRENCH);
                Calendar cal = Calendar.getInstance();
                String hr = sdff.format(spin.getValue());
                return hr;
            }

            public String HOURFormat(Object object) {
                SimpleDateFormat sdff = new SimpleDateFormat("HH:mm:ss", Locale.FRENCH);
                Calendar cal = Calendar.getInstance();
                String hr = sdff.format(object);
                return hr;
            }

            //-----------------------------------------------------------------------------------------------------
            public void removeDatosOntable(DefaultTableModel model) {

                int a = model.getRowCount() - 1;

                for (int i = a; i >= 0; i--) {
                    model.removeRow(i);
                }

            }

            //------------------------------------------------------------------------------------

            public String illegalCharacterOnString(String cadena) {
                String cadenaString = "";
                for (int i = 0; i < cadena.trim().length(); i++) {
                    if (cadena.charAt(i) == '\'') {
                        cadenaString += '\'';
                    }
                    cadenaString += cadena.charAt(i);
                }

                return (cadenaString);
            }
            //.........................................................................................................

            public String checkString(JCheckBox checkBox, String enCaseSea, String enCaseNoSea) {
                String liricString = null;

                if (checkBox.isSelected()) {
                    liricString = enCaseSea;
                } else {
                    liricString = enCaseNoSea;
                }

                return liricString;
            }

            public InputStream Photos(JLabel label, String id) {
                Integer targetWidth = label.getWidth();
                Integer targetHeight = label.getHeight();

                ImageIcon imgcon = null;
                label.setText(null);
                ls = new JFileChooser();
                ls.setFileSelectionMode(JFileChooser.FILES_ONLY);
                //-------------------------------------------------
                javax.swing.filechooser.FileFilter filter = new FileNameExtensionFilter("JPG & GIF & PNG", "jpg", "gif", "png");
                ls.setFileFilter(filter);
                //--------------------------------------------------
                int gt = ls.showOpenDialog(null);
                if (gt == JFileChooser.APPROVE_OPTION) {
                    try {
                        lp = new FileInputStream(ls.getSelectedFile());
                        longueur = (int) ls.getSelectedFile().length();
                        longeur = longueur;
                        BufferedImage icn = ImageIO.read(ls.getSelectedFile());
                        BufferedImage thumbImg = Scalr.resize(icn, Method.QUALITY, Mode.FIT_EXACT, label.getWidth(), label.getHeight(), Scalr.OP_ANTIALIAS);

                        imgcon = new ImageIcon(CrearImage(thumbImg, label));
                        label.setIcon(imgcon);
                        label.updateUI();
                    } catch (FileNotFoundException ex) {
                        Logger.getLogger(Manipulation.class.getName()).log(Level.SEVERE, null, ex);
                        JOptionPane.showMessageDialog(null, ex.getMessage());
                    } catch (IOException ex) {
                        Logger.getLogger(Manipulation.class.getName()).log(Level.SEVERE, null, ex);
                        JOptionPane.showMessageDialog(null, ex.getMessage());
                    }

                }
                return lp;
            }

            public Image CrearImage(Image image, JLabel label) {
                image = new ImageIcon(image).getImage();
                BufferedImage bimage = new BufferedImage(label.getWidth(), label.getHeight(), BufferedImage.SCALE_DEFAULT);
                Graphics g = bimage.createGraphics();

                g.drawImage(image, 0, 0, null);
                g.dispose();
                return bimage;
            }

            public void InsertPhotos(int code,String codter, InputStream direct, int longitud,JLabel label) {

                try {
                    if (label.getIcon() != null) {
                        String sql = "call image_procedure (?,?,?)";
                        PreparedStatement statement = db.getConexion().prepareStatement(sql);
                        statement.setInt(1, code);
                        statement.setString(2, codter);
                        statement.setBlob(3, direct, longitud);
                        statement.executeUpdate();
                    } else {
                        JOptionPane.showMessageDialog(null, "Désolé Vous Le champ matricule ne doit pas etre vide");
                        return;
                    }
                } catch (SQLException ex) {
                    Logger.getLogger(Manipulation.class.getName()).log(Level.SEVERE, null, ex);
                    JOptionPane.showMessageDialog(null, ex.getMessage());
                }
            }

            public void ShowPhotos(String a, JLabel label) {
                System.out.println("show photo "+a);
                try {
                    ResultSet dz = Rezulta(a);
                    if(dz.absolute(1)){
                    if (dz.last()) {
                        Blob bl = (Blob) dz.getBlob("imagecol");
                        byte[] data = bl.getBytes(1, (int) bl.length());
                            Image img = ImageIO.read(new ByteArrayInputStream(data)).getScaledInstance(label.getWidth(), label.getHeight(), Image.SCALE_SMOOTH);
                            label.setText(null);
                            label.setIcon(new ImageIcon(img));
                            label.updateUI();
                        }
                    }
                     else {
                        label.setIcon(null);
                        label.updateUI();
                    }
                    
                } catch (SQLException | IOException ex) {
                    Logger.getLogger(Manipulation.class.getName()).log(Level.SEVERE, null, ex);
                    JOptionPane.showMessageDialog(null, ex.getMessage());
                }
            }
           
            public boolean isElementExistInGrid(JTable table, String[] text){
                boolean result = false;
                for (int i = 0; i < table.getRowCount(); i++) {
                    for (int j = 0; j < text.length; j++) {
                        
                        if(table.getValueAt(i, j).toString().equals(text[j])){
                       result =true; 
                    }
                    }
                    
                }
                return result;
            }
            
    public Date dateMathMonth(Date fecha, int month){
      Calendar calendar = Calendar.getInstance();
      calendar.setTime(fecha); 
      calendar.add(Calendar.MONTH, month);
      return calendar.getTime(); 
 }
    
    public String dateToMysqlDate(Date fecha) {
        String fechaReturn = "0000-00-00";
        java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd");
        if(fecha != null){
            fechaReturn = sdf.format(fecha);
        }
        return fechaReturn;
    }
    
    public int getCodeFromCombo(String table,String campoAbuscar,JComboBox combo){
        int code = 0;
        String sql = "select "+campoAbuscar+" from "+table+" where description = '"+illegalCharacterOnString(combo.getSelectedItem().toString().trim())+"' ";
        System.out.println(sql);
        ResultSet rs = Rezulta(sql);
        try {
            if(rs.last())
                code = rs.getInt(campoAbuscar);
        } catch (SQLException ex) {
            Logger.getLogger(Manipulation.class.getName()).log(Level.SEVERE, null, ex);
        }
        return code;
    }
     
    public int getCodeFromString(String table,String campoAbuscar,String campo){
        int code = 0;
        String sql = "select "+campoAbuscar+" from "+table+" where description = '"+illegalCharacterOnString(campo.trim())+"' ";
        System.out.println(sql);
        ResultSet rs = Rezulta(sql);
        try {
            if(rs.last())
                code = rs.getInt(campoAbuscar);
        } catch (SQLException ex) {
            Logger.getLogger(Manipulation.class.getName()).log(Level.SEVERE, null, ex);
        }
        return code;
    }
    
    public int getCodeFromString(String table,String campoAbuscar,String campo,String campoWhere){
        int code = 0;
        String sql = "select "+campoAbuscar+" from "+table+" where "+campoWhere+" = '"+illegalCharacterOnString(campo.trim())+"' ";
        System.out.println(sql);
        ResultSet rs = Rezulta(sql);
        try {
            if(rs.last())
                code = rs.getInt(campoAbuscar);
        } catch (SQLException ex) {
            Logger.getLogger(Manipulation.class.getName()).log(Level.SEVERE, null, ex);
        }
        return code;
    }
    
    //..........................................................................Panel Text
    public void setPanTxtColor(JPanel panel, JTextField label) {
        try {
            // set color to the panel and label acording to the parameters
        panel.setBorder(BorderFactory.createLineBorder(new Color(87, 148,210)));
        label.setBorder(BorderFactory.createLineBorder(new Color(0, 0,0)));
        } catch (NullPointerException e) {
        }
        
    }

    public void resetPanTxtColor(JPanel panel, JTextField label) {
        try {
            // set color to the panel and label acording to the parameters
        panel.setBorder(BorderFactory.createLineBorder(new Color(255, 255, 255)));
        label.setBorder(new org.jdesktop.swingx.border.DropShadowBorder());
        } catch (NullPointerException e) {
        }
       
    }
    
    public void panelSetMouseEntered(JPanel panel){
        panel.setBorder(BorderFactory.createLineBorder(new Color(87, 148,210)));
    }
    
    public void panelSetMouseExited(JPanel panel){
        panel.setBorder(BorderFactory.createLineBorder(new Color(255, 255,255)));
    }
    
    public void panelSetMouseEnteredMenu(JPanel panel){
        panel.setBorder(new org.jdesktop.swingx.border.DropShadowBorder());
    }
    
    public void panelSetMouseExitedMenu(JPanel panel){
        panel.setBorder(BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
    }
    
    public String tableRowcount(JTable table){
            return "Nombre(s) de registre(s) : "+table.getRowCount();
    }
}

