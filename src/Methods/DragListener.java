package Methods;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.dnd.DnDConstants;
import java.awt.dnd.DropTargetDragEvent;
import java.awt.dnd.DropTargetDropEvent;
import java.awt.dnd.DropTargetEvent;
import java.awt.dnd.DropTargetListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.List;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import org.imgscalr.Scalr;

/**
 *
 * @author User
 */
public class DragListener implements DropTargetListener{
    public static int longeur;
    public static InputStream stream;
    JLabel imageLabel ;
    public static String pathFoto;
    
    String pathe = null;
    
    public DragListener(JLabel image, String path){
        this.imageLabel = image;
        this.pathe = path;
    }

    @Override
    public void dragEnter(DropTargetDragEvent dtde) {

    }

    @Override
    public void dragOver(DropTargetDragEvent dtde) {

    }

    @Override
    public void dropActionChanged(DropTargetDragEvent dtde) {

    }

    @Override
    public void dragExit(DropTargetEvent dte) {

    }

    @Override
    public void drop(DropTargetDropEvent ev) {
        //ACCEPT DROPPED ITEMS
        ev.acceptDrop(DnDConstants.ACTION_COPY);

        //WE WANT DROPPED ITEMS
        Transferable t = ev.getTransferable();
        
        //GET DATA FORMATS OF THE ITEMS
        DataFlavor[] df = t.getTransferDataFlavors();
        
        //loop thru flavor
        
        for (DataFlavor f : df) {
            try {
                //CHECK IF ITEMS ARE FILE TYPE
                if(f.isFlavorJavaFileListType()){
                    //GET LIST OF THEM
                    List<File> files = (List<File>) t.getTransferData(f);
                    
                    //LOOP THRU THEM
                    for(File file : files){
                        displayImage(file.getPath());
                    }
                }
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, e);
            }
        }
        
    }
    
    public void displayImage(String path){
        stream = null;
        longeur = 0;
        
        BufferedImage img = null , thumbImg = null;
        try {
            img = ImageIO.read(new File(path));
            thumbImg = Scalr.resize(img, Scalr.Method.QUALITY,Scalr.Mode.FIT_EXACT,imageLabel.getWidth(),imageLabel.getHeight(), Scalr.OP_BRIGHTER);
            File fl = new File(path);
            stream = new FileInputStream(fl);
            longeur = (int) fl.length();
        } catch (Exception e) {
        }
        pathFoto = path;
        ImageIcon icon = new ImageIcon(CrearImage(thumbImg, imageLabel));
        imageLabel.setIcon(icon);
        imageLabel.updateUI();
    }
    
    
    public Image CrearImage(Image image, JLabel label) {
        image = new ImageIcon(image).getImage();
        BufferedImage bimage = new BufferedImage(label.getWidth(), label.getHeight(), BufferedImage.SCALE_SMOOTH);
        Graphics g = bimage.createGraphics();

        g.drawImage(image, 0, 0, null);
        g.dispose();
        return bimage;
    }
}
