
package Methods;
import Methods.Manipulation;
import Methods.Variables;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;

public class GenerateMatricule {
    Manipulation Mn= new Manipulation();
    String res="";
    String sigle="";
  
  public String abrege(){
      String query="select abrege from institution where nom='"+Variables.Empresa+"'";
      ResultSet rs=Mn.SEARCHDATA(query);
      try {
           if(rs.first()){
                sigle=rs.getString("abrege");
            }else{
             sigle="DEF";
           }
        } catch (SQLException ex) {
            Logger.getLogger(GenerateMatricule.class.getName()).log(Level.SEVERE, null, ex);
        }
      return sigle;
  }
  
  private String anio(String val){
   String rezilta="";
   char[] valeur;
   valeur=val.toCharArray();
   for(int i=2;i<valeur.length;i++){
       rezilta=rezilta+""+valeur[i];
   }
   return rezilta;
  }
  
  private String serialprof(){
      String code="";
      String rezilta="";
      char[] valeur;
      String res="";
      String query="select matricule from employer where code_employer in (select max(code_employer) from employer )";
      System.out.println("query "+query);
      ResultSet rs=Mn.SEARCHDATA(query);
        try {
            if(rs.first()){
             code=rs.getString("matricule");
             valeur=code.toCharArray();
             int ok=valeur.length-1;
             for(int i=ok,j=4;j>=1;i--,j--){
              rezilta=valeur[i]+""+rezilta;
               
             }
            }else{
              rezilta="0";  
            }
            int increment=Integer.parseInt(rezilta);
//            System.out.println(increment);
            if(increment<9){
                increment=increment+1;
             res="000"+increment;   
            }else if(increment>=9 && increment<99){
             increment=increment+1;
             res="00"+increment;
            }else if(increment>=99 && increment<999){
             increment=increment+1;
             res="0"+increment;
            }else if(increment>=999 && increment<9999){
             increment=increment+1;
             res=increment+"";
            }
        } catch (SQLException ex) {
            Logger.getLogger(GenerateMatricule.class.getName()).log(Level.SEVERE, null, ex);
        }
      return res;
  }
  
  
  private String serial(){
      String code="";
      String rezilta="";
      String rez="";
      char[] valeur;
      String res="";
      Calendar cal= Calendar.getInstance();
      String annee=String.valueOf(cal.get(Calendar.YEAR));
      String aux=anio(annee);
      String query = "select matricule from eleve where code_eleve in (select max(e.code_eleve) from eleve e left join tercero t on \n" +
      "(t.code = t.code and t.code_tercero = e.code_tercero)) ";
      System.out.println("vb "+query);
      ResultSet rs=Mn.SEARCHDATA(query);
        try {
            if(rs.first()){
             code=rs.getString("matricule");
             valeur=code.toCharArray();
             
             int ok=valeur.length-1;
             int okk=valeur.length-1;
             for(int i=ok,j=4;j>=1;i--,j--){
              rezilta=valeur[i]+""+rezilta;
               
             }
             
             for(int i=okk,j=6;j>=1;i--,j--){
                 if(j<=2){
                 rez=valeur[i]+""+rez;
                 }
             }
            }else{
              rezilta="0"; 
              rez="0";
            }
            
            int increment=Integer.parseInt(rezilta);
            
            if(increment==9999){
                aux=String.valueOf(Integer.parseInt(aux)+1);
                increment=0;
            }else{
                if(Integer.parseInt(aux) >Integer.parseInt(rez)){
                   increment=0; 
                }
            }
            if(increment<9){
                increment=increment+1;
             res=aux+"000"+increment;   
            }else if(increment>=9 && increment<99){
             increment=increment+1;
             res=aux+"00"+increment;
            }else if(increment>=99 && increment<999){
             increment=increment+1;
             res=aux+"0"+increment;
            }else if(increment>=999 && increment<9999){
             increment=increment+1;
             res=aux+""+increment;
            }
        } catch (SQLException ex) {
            Logger.getLogger(GenerateMatricule.class.getName()).log(Level.SEVERE, null, ex);
        }
      return res;
  }
  
  public String MatriculeCode(){
  String abreger = abrege();   
  res = abreger +""+ serial();
  return res;
  }
  
  public String MatriculeProf(){
   String abreger=abrege();   
  res=abreger+"EMP"+serialprof();
  return res;
  }
  
  public static void main(String[] args){
     GenerateMatricule ma= new GenerateMatricule();
      System.out.println(ma.MatriculeCode());
      
  }
  
}


