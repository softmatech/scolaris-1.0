package Methods;

import java.awt.Frame;
import java.io.File;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JRDesignQuery;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.view.JasperViewer;

public class Rapport {
     Connections db= new Connections().Conectar();
    private Map<String, Object> Parametro;
    public Rapport()
    {   
    }
    
    //-----------------------------------------------------------------------------------------------
public void SENDQUERY(String select,String nombre )
             {
                try
                 {  
                   String fileName = System.getProperty("user.dir")+"/src/Rapports/"+nombre+".jrxml";
                   
                    if (fileName == null) 
                    {                
                         System.out.println("No se encuentra el archivo");
                         System.exit(2);
                     } 
                 File theFile = new File(fileName);
                 JasperDesign jasperDesign = JRXmlLoader.load(theFile);
                 JRDesignQuery query = new JRDesignQuery();
                 query.setText(select);
                 jasperDesign.setQuery(query);
                  Map parametro = new HashMap();
                  parametro.put("userdir",fileName);
                  JasperReport jasperReport = JasperCompileManager.compileReport(jasperDesign);
                  JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport,parametro,db.getConexion());
                  JasperViewer jviewer = new JasperViewer(jasperPrint,false);
                  jviewer.setTitle("Scolaris: "+Variables.Empresa);
                  //jviewer.setExtendedState(Frame.MAXIMIZED_BOTH);
                  //jviewer.setSize(400, 741);
                  jviewer.setLocationRelativeTo(null);
                  jviewer.setVisible(true);
                  jviewer.setAlwaysOnTop(true);
 
                }
                catch (Exception e)
                 {
                     System.out.println(":"+e.getMessage());
                 }       
            }
  
//----------------------------------------------------------------------------------------------------------------
///------------------------------------------------------------------------------------------------------------------ 
public void SENDQUERY(String nombre )
             {
                try
                 {  
                   
                   String fileName = System.getProperty("user.dir")+"/src/Rapports/"+nombre+".jrxml";
  
                    if (fileName == null) 
                    {                
                         System.out.println("No se encuentra el archivo");
                         System.exit(2);
                     } 
                  JasperReport jasperReport = JasperCompileManager.compileReport(fileName);
                  JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport,null,db.getConexion());
                  JasperViewer jviewer = new JasperViewer(jasperPrint,false);
                  jviewer.setTitle("Scolaris: "+Variables.Empresa);
                  //jviewer.setExtendedState(Frame.MAXIMIZED_BOTH);
                 //jviewer.setSize(400, 741);
                  jviewer.setLocationRelativeTo(null);
                  jviewer.setVisible(true);
                  jviewer.setAlwaysOnTop(true);
                }
                catch (Exception e)
                 {
                     System.out.println(":"+e.getMessage());
                 }       
            }
//---------------------------------------------------------------------------------------------------------------
public void SENDQUERY(String nombre,int desde,int hasta )
             {
                try
                 {  
                   
                   String fileName = System.getProperty("user.dir")+"/src/Rapports/"+nombre+".jrxml";
  
                    if (fileName == null) 
                    {                
                         System.out.println("No se encuentra el archivo");
                         System.exit(2);
                     } 
                 Map parametro = new HashMap();
                 parametro.put("desde",desde); 
                 parametro.put("hasta",hasta);
                  JasperReport jasperReport = JasperCompileManager.compileReport(fileName);
                  JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport,parametro,db.getConexion());
                  JasperViewer jviewer = new JasperViewer(jasperPrint,false);
                  jviewer.setTitle("Scolaris: "+Variables.Empresa);
                  jviewer.setExtendedState(Frame.MAXIMIZED_BOTH);
                  //jviewer.setSize(400, 741);
                  jviewer.setLocationRelativeTo(null);
                  jviewer.setVisible(true);
                  jviewer.setAlwaysOnTop(true);
                }
 
                catch (Exception e)
                 {
                     System.out.println(":"+e.getMessage());
                 }       
            }
//---------------------------------------------------------------------------------------------------------------
public void SENDQUERYS(String nombre,String desde )
             {
                try
                 {  
                   
                   String fileName = System.getProperty("user.dir")+"/src/Rapports/"+nombre+".jrxml";
  
                    if (fileName == null) 
                    {                
                         System.out.println("No se encuentra el archivo");
                         System.exit(2);
                     } 
                 Map parametro = new HashMap();
                 parametro.put("desde",desde); 
                  JasperReport jasperReport = JasperCompileManager.compileReport(fileName);
                  JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport,parametro,db.getConexion());
                  JasperViewer jviewer = new JasperViewer(jasperPrint,false);
                  jviewer.setTitle("Scolaris: "+Variables.Empresa);
                  jviewer.setExtendedState(Frame.MAXIMIZED_BOTH);
                  //jviewer.setSize(400, 741);
                  jviewer.setLocationRelativeTo(null);
                  jviewer.setVisible(true);
                  jviewer.setAlwaysOnTop(true);
                }
 
                catch (Exception e)
                 {
                     System.out.println(":"+e.getMessage());
                 }       
            }
//-----------------------------------------------------------------------------------------------------------------
public void SENDQUERY(String nombre,String desde,int hasta )
             {
                try
                 {  
                   
                   String fileName = System.getProperty("user.dir")+"/src/Rapports/"+nombre+".jrxml";
  
                    if (fileName == null) 
                    {                
                         System.out.println("No se encuentra el archivo");
                         System.exit(2);
                     } 
                 Map parametro = new HashMap();
                 parametro.put("desde",desde); 
                 parametro.put("hasta",hasta);
                  JasperReport jasperReport = JasperCompileManager.compileReport(fileName);
                  JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport,parametro,db.getConexion());
                  JasperViewer jviewer = new JasperViewer(jasperPrint,false);
                  jviewer.setTitle("Scolaris: "+Variables.Empresa);
                  jviewer.setExtendedState(Frame.MAXIMIZED_BOTH);
                  //jviewer.setSize(400, 741);
                  jviewer.setLocationRelativeTo(null);
                  jviewer.setVisible(true);
                  jviewer.setAlwaysOnTop(true);
                }
 
                catch (Exception e)
                 {
                     System.out.println(":"+e.getMessage());
                 }       
            }
//-----------------------------------------------------------------------------------------------------------------
public void SENDQUERY(String nombre,int desde,String hasta )
             {
                try
                 {  
                   
                   String fileName = System.getProperty("user.dir")+"/src/Rapports/"+nombre+".jrxml";
  
                    if (fileName == null) 
                    {                
                         System.out.println("No se encuentra el archivo");
                         System.exit(2);
                     } 
                 Map parametro = new HashMap();
                 parametro.put("desde",desde); 
                 parametro.put("hasta",hasta);
                  JasperReport jasperReport = JasperCompileManager.compileReport(fileName);
                  JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport,parametro,db.getConexion());
                  JasperViewer jviewer = new JasperViewer(jasperPrint,false);
                  jviewer.setTitle("Scolaris: "+Variables.Empresa);
                  jviewer.setExtendedState(Frame.MAXIMIZED_BOTH);
                  //jviewer.setSize(400, 741);
                  jviewer.setLocationRelativeTo(null);
                  jviewer.setVisible(true);
                  jviewer.setAlwaysOnTop(true);
                }
 
                catch (Exception e)
                 {
                     System.out.println(":"+e.getMessage());
                 }       
            }
//-----------------------------------------------------------------------------------------------------------------
public void SENDQUERY(String nombre,String desde,String hasta )
             {
                try
                 {  
                   
                   String fileName = System.getProperty("user.dir")+"/src/Rapports/"+nombre+".jrxml";
  
                    if (fileName == null) 
                    {                
                         System.out.println("No se encuentra el archivo");
                         System.exit(2);
                     } 
                 Map parametro = new HashMap();
                 parametro.put("desde",desde); 
                 parametro.put("hasta",hasta);
                  JasperReport jasperReport = JasperCompileManager.compileReport(fileName);
                  JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport,parametro,db.getConexion());
                  JasperViewer jviewer = new JasperViewer(jasperPrint,false);
                  jviewer.setTitle("Scolaris: "+Variables.Empresa);
                  jviewer.setExtendedState(Frame.MAXIMIZED_BOTH);
                  //jviewer.setSize(400, 741);
                  jviewer.setLocationRelativeTo(null);
                  jviewer.setVisible(true);
                  jviewer.setAlwaysOnTop(true);
                }
 
                catch (Exception e)
                 {
                     System.out.println(":"+e.getMessage());
                 }       
            }
public void SENDQUERYS(String nombre,String desde,String institut )
             {
                try
                 {  
                   
                   String fileName = System.getProperty("user.dir")+"/src/Rapports/"+nombre+".jrxml";
  
                    if (fileName == null) 
                    {                
                         System.out.println("No se encuentra el archivo");
                         System.exit(2);
                     } 
                 Map parametro = new HashMap();
                 parametro.put("desde",desde); 
                 parametro.put("institut",institut);
                  JasperReport jasperReport = JasperCompileManager.compileReport(fileName);
                  JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport,parametro,db.getConexion());
                  JasperViewer jviewer = new JasperViewer(jasperPrint,false);
                  jviewer.setTitle("Scolaris: "+Variables.Empresa);
                  jviewer.setExtendedState(Frame.MAXIMIZED_BOTH);
                  //jviewer.setSize(400, 741);
                  jviewer.setLocationRelativeTo(null);
                  jviewer.setVisible(true);
                  jviewer.setAlwaysOnTop(true);
                }
 
                catch (Exception e)
                 {
                     System.out.println(":"+e.getMessage());
                 }       
            }

public void SENDQUERY(String nombre,String desde,String hasta,String institut )
             {
                try
                 {  
                   
                   String fileName = System.getProperty("user.dir")+"/src/Rapports/"+nombre+".jrxml";
  
                    if (fileName == null) 
                    {                
                         System.out.println("No se encuentra el archivo");
                         System.exit(2);
                     } 
                 Map parametro = new HashMap();
                 parametro.put("desde",desde); 
                 parametro.put("hasta",hasta);
                 parametro.put("institut",institut);
                  JasperReport jasperReport = JasperCompileManager.compileReport(fileName);
                  JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport,parametro,db.getConexion());
                  JasperViewer jviewer = new JasperViewer(jasperPrint,false);
                  jviewer.setTitle("Scolaris: "+Variables.Empresa);
                  jviewer.setExtendedState(Frame.MAXIMIZED_BOTH);
                  //jviewer.setSize(400, 741);
                  jviewer.setLocationRelativeTo(null);
                  jviewer.setVisible(true);
                  jviewer.setAlwaysOnTop(true);
                }
 
                catch (Exception e)
                 {
                     System.out.println(":"+e.getMessage());
                 }       
            }
//-----------------------------------------------------------------------------------------------------------------
public void SENDQUERY(String nombre,String desde,String hasta ,int depuis,String institut)
             {
                try
                 {  
                   
                   String fileName = System.getProperty("user.dir")+"/src/Rapports/"+nombre+".jrxml";
  
                    if (fileName == null) 
                    {                
                         System.out.println("No se encuentra el archivo");
                         System.exit(2);
                     } 
                 Map parametro = new HashMap();
                 parametro.put("desde",desde); 
                 parametro.put("hasta",hasta);
                 parametro.put("depuis",depuis);
                 parametro.put("institut",institut);
                  JasperReport jasperReport = JasperCompileManager.compileReport(fileName);
                  JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport,parametro,db.getConexion());
                  JasperViewer jviewer = new JasperViewer(jasperPrint,false);
                  jviewer.setTitle("Scolaris: "+Variables.Empresa);
                  jviewer.setExtendedState(Frame.MAXIMIZED_BOTH);
                  //jviewer.setSize(400, 741);
                  jviewer.setLocationRelativeTo(null);
                  jviewer.setVisible(true);
                  jviewer.setAlwaysOnTop(true);
                }
 
                catch (Exception e)
                 {
                     System.out.println(":"+e.getMessage());
                 }       
            }
//-----------------------------------------------------------------------------------------------------------------
      public void runReporte(String nombre,String desde, String hasta,String institut)
     {
        try
        {            

           URL  master= this.getClass().getResource("/src/Rapports/"+nombre+".jasper");
           String masters = System.getProperty("user.dir") + "/src/Rapports/"+nombre+".jasper" ;
            System.out.println("master : " + masters);
            if (masters == null) 
            {                
               System.out.println("No se encuentra el archivo");
                System.exit(2);
            } 
         String a=System.getProperty("user.dir");
            JasperReport masterReport = null;
            try 
            {
                masterReport = (JasperReport) JRLoader.loadObject(masters);
            } 
            catch (JRException e) 
            {
                System.out.println("Ere pandan sistem nan ap chaje achiv la: " + e.getMessage());
                System.exit(3);
            }              
            
            Map parametro = new HashMap();
            parametro.put("desde",desde); 
            parametro.put("hasta",hasta);
            parametro.put("institut",institut);
            JasperPrint jasperPrint = JasperFillManager.fillReport(masterReport,parametro,db.getConexion());
            parametro.put("SUBREPORT_DIR", System.getProperty("user.dir")+"/src/Rapports/");
            JasperViewer jviewer = new JasperViewer(jasperPrint,false);
            jviewer.setTitle("Hosanna Centre Medical");
            jviewer.setVisible(true);
            jviewer.setAlwaysOnTop(true);
            jviewer.setExtendedState(jviewer.MAXIMIZED_BOTH);
           
        }

        catch (Exception j)
        {
            System.out.println(":"+j.getMessage());
        }
        
    }

}
