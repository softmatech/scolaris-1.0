
package Methods;

import java.awt.Color;
import java.awt.Component;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

public class MiRender extends DefaultTableCellRenderer {
   public Component getTableCellRenderer(JTable table,Object value,boolean idSelected,boolean hasFocus,int row,int column){
        super.getTableCellRendererComponent(table, value, hasFocus, hasFocus, row, row);
        if(table.getRowCount()!=0){
            this.setOpaque(true);
            this.setBackground(Color.yellow);
            this.setForeground(Color.black);
        }else{
            this.setBackground(null);
        }
        return this;
    }
 
}
