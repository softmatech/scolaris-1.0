package Methods;

import java.awt.Component;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.image.BufferedImage;
import java.net.URL;
import javax.imageio.ImageIO;
import javax.swing.border.Border;

public class ImagenFondo implements Border{
    public BufferedImage back;
    
    public ImagenFondo(){
        try {
            URL imagePath= new URL(getClass().getResource("/Pictures/Menu.jpg").toString());
            back=ImageIO.read(imagePath);
        } catch (Exception e) {
        }
    }

    @Override
    public void paintBorder(Component c, Graphics g, int x, int y, int width, int heigth) {
        g.drawImage(back,(x * (width-back.getWidth())/2),(y * (heigth - back.getHeight())/2),null);
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public Insets getBorderInsets(Component c) {
        return new Insets(0, 0, 0, 0);
        //throw new UnsupportedOperationException("Not supported yet."); 
    }

    @Override
    public boolean isBorderOpaque() {
        return false;
      //  throw new UnsupportedOperationException("Not supported yet.");
    }
    
}
