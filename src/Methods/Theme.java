
package Methods;

import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JPanel;

public class Theme {
    public void Icone_JFrame(JFrame frm){
         frm.setIconImage(new ImageIcon(getClass().getResource("/Pictures/logo.jpg")).getImage());
    }
    
    public void Icone_JFrame(JDialog frm){
         frm.setIconImage(new ImageIcon(getClass().getResource("/Pictures/logo.jpg")).getImage());
    }
    //---------------------------------------------------------------------------------------------------
    
    public void Fond_JFrame(JFrame frm){
         ((JPanel)frm.getContentPane()).setOpaque(false);
        ImageIcon uno= new ImageIcon(this.getClass().getResource("/Pictures/Menu.jpg"));
        JLabel fondo= new JLabel();
        fondo.setIcon(uno);
        frm.getLayeredPane().add(fondo,JLayeredPane.FRAME_CONTENT_LAYER);
        fondo.setBounds(0,0,uno.getIconWidth(), uno.getIconHeight());

    }
    
     public void Fond_login_JFrame(JFrame frm){
         ((JPanel)frm.getContentPane()).setOpaque(false);
        ImageIcon uno= new ImageIcon(this.getClass().getResource("/Pictures/login.jpg"));
        JLabel fondo= new JLabel();
        fondo.setIcon(uno);
        frm.getLayeredPane().add(fondo,JLayeredPane.FRAME_CONTENT_LAYER);
        fondo.setBounds(0,0,uno.getIconWidth(), uno.getIconHeight());

    }
      public void Fond_personal_JFrame(JFrame frm){
         ((JPanel)frm.getContentPane()).setOpaque(false);
        ImageIcon uno= new ImageIcon(this.getClass().getResource("/Pictures/Personal.jpg"));
        JLabel fondo= new JLabel();
        fondo.setIcon(uno);
        frm.getLayeredPane().add(fondo,JLayeredPane.FRAME_CONTENT_LAYER);
        fondo.setBounds(0,0,uno.getIconWidth(), uno.getIconHeight());

    }
    
     public void Fond_JFrame(JDialog frm){
         ((JPanel)frm.getContentPane()).setOpaque(false);
        ImageIcon uno= new ImageIcon(this.getClass().getResource("/Pictures/Verrouillage.png"));
        JLabel fondo= new JLabel();
        fondo.setIcon(uno);
        frm.getLayeredPane().add(fondo,JLayeredPane.FRAME_CONTENT_LAYER);
        fondo.setBounds(0,0,uno.getIconWidth(), uno.getIconHeight());

    }
 //------------------------------------------------------------------------------------------------------------  
     public void Splash_Fond_JFrame(JFrame frm){
         ((JPanel)frm.getContentPane()).setOpaque(false);
        ImageIcon uno= new ImageIcon(this.getClass().getResource("/Pictures/Splash.jpg"));
        JLabel fondo= new JLabel();
        fondo.setIcon(uno);
        frm.getLayeredPane().add(fondo,JLayeredPane.FRAME_CONTENT_LAYER);
        fondo.setBounds(0,0,uno.getIconWidth(), uno.getIconHeight());

    }
    //---------------------------------------------------------------------------------------------------------
}
