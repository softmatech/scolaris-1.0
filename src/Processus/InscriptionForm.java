/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Processus;

import Methods.Manipulation;
import Methods.Rapport;
import Methods.Theme;
import Methods.Variables;
import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;
import java.awt.Color;
import java.awt.Component;
import java.awt.Point;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumnModel;

/**
 *
 * @author josephandyfeidje
 */
public class InscriptionForm extends javax.swing.JFrame {

    Manipulation Mn= new Manipulation();
Rapport Rp= new Rapport();
Theme th= new Theme();
String[][] data={};
String[] head={" Matricule"," Prénom"," Nom"," Dernière Classe","Moyenne","Status","Classe Sup."};
String[] heads={" Classe"," Section"};
String[] headd={" Matricule"," Nom"," Prénom"};
DefaultTableModel md= new DefaultTableModel(data,head);
DefaultTableModel mds= new DefaultTableModel(data,heads);
DefaultTableModel mdd= new DefaultTableModel(data,headd);
 TableCellRenderer render= new TableCellRenderer() {
 public Component getTableCellRendererComponent(JTable table,Object value,boolean idSelected,boolean hasFocus,int row,int column){
     JLabel lbl= new JLabel(value==null? "": value.toString());
     if(column==0 && row>=0){
         lbl.setHorizontalAlignment(SwingConstants.LEFT);
         lbl.setOpaque(true);
         lbl.setBackground(Color.green);
     }
if(column==4 && row>=0){
         lbl.setHorizontalAlignment(SwingConstants.LEFT);
         lbl.setOpaque(true);
         lbl.setBackground(Color.yellow);
     }
if(column==5 && value.toString().equals("Redoublé(e)") && row>=0){
         lbl.setHorizontalAlignment(SwingConstants.LEFT);
         lbl.setOpaque(true);
         lbl.setBackground(Color.red);
     }
     return lbl;
 }
};
 
    public InscriptionForm() {
        initComponents();
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        this.setTitle("Scolaris: "+Variables.Empresa+" : Registre d'Inscription");
//         Mn.FULLCOMBO(classe,"description","classe");
         Mn.FULLCOMBO(classe1,"description","classe");
         date.setText(Mn.DATEHOURNOW());
         tblinscription.setModel(md);
         tblclasse.setModel(mds);
         tbleleve.setModel(mdd);
         ancien.setSelected(true);
         etablissement.setEnabled(false);
         moyenne.setEnabled(false);
         lblmoyenne.setEnabled(false);
         moyenne1.setEnabled(false);
         motif1.setEnabled(false);
         motif.setEnabled(false);
         kinder.setEnabled(false);
         classe1.setEnabled(false);
         rexaminer.setEnabled(false);
         RemplirClasse();
         Scolaire();
         AnneAnterieur();
         RemplirTable();
     //----------------------------------------------------------------
   TableColumnModel cmy= tblinscription.getColumnModel();
        cmy.getColumn(0).setPreferredWidth(30);
        cmy.getColumn(1).setPreferredWidth(70);
        cmy.getColumn(2).setPreferredWidth(70);
        cmy.getColumn(3).setPreferredWidth(30);
        cmy.getColumn(4).setPreferredWidth(5);
        cmy.getColumn(5).setPreferredWidth(10);
        cmy.getColumn(6).setPreferredWidth(30);
     //----------------------------------------------------------------
   TableColumnModel cm= tbleleve.getColumnModel();
        cm.getColumn(0).setPreferredWidth(30);
        cm.getColumn(1).setPreferredWidth(90);
        cm.getColumn(2).setPreferredWidth(330);
        tblinscription.getColumnModel().getColumn(0).setCellRenderer(render);
        tbleleve.getColumnModel().getColumn(0).setCellRenderer(render);
        tblinscription.getColumnModel().getColumn(4).setCellRenderer(render);
        tblinscription.getColumnModel().getColumn(5).setCellRenderer(render);
         Nettoyer();
         setLocationRelativeTo(this);
    }

   private void Scolaire(){
        String query="select annee from annee_academique where code_annee\n" +
         "in (select max(code_annee) from annee_academique)";
        ResultSet rs=Mn.SEARCHDATA(query);
        try {
            if(rs.first()){
             academique.setText(rs.getString("annee"));

            }
        } catch (SQLException ex) {
            Logger.getLogger(Entrer_note.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

  private void Nettoyer(){
    matricule.setText(null);
    cantclasse.setText(null);
    cantsection.setText(null);
    classe1.setSelectedItem("Selectionnez");
    moyenne.setText(null);
    moyenne1.setText(null);
    etablissement.setText(null);
    motif.setText(null);
    nomeleve.setText("Nom de l'élève");
    acceseleve.setText(null);
    kinder.setSelected(false);
    rexaminer.setSelected(false);
  }
   
  private void InsertInscriptionAncien(){
      int aa = 0;
      String xs = "";
      String xx = "";
      double moy = 0.0;
      String ff = "0.0";
      String estatuses = "";
      double gg =0.0;
      String query = "select e.code,e.code_tercero from eleve e left join tercero t on  (t.code = e.code and t.code_tercero = e.code_tercero) "
          + "join eleve_has_institution ehi on (e.code = ehi.code and e.code_tercero = ehi.code_tercero) "
          + "join institution i on(ehi.idinstitution = i.idinstitution) where e.matricule='"+matricule.getText()+"' and i.nom = '"+Variables.Empresa+"'";
  
      ResultSet rs = Mn.Rezulta(query);
    try {
        if(rs.last()){
            aa = rs.getInt("code");
            xs = rs.getString("code_tercero");
        }
    } catch (SQLException ex) {
        Logger.getLogger(Inscription.class.getName()).log(Level.SEVERE, null, ex);
    }
    estatuses = tblinscription.getValueAt(0,5).toString();
    xx = tblinscription.getValueAt(0,6).toString();
    String cc = date.getText();
    String dd = academique.getText();
    
  String cmd="select code_classe from classe where description='"+xx+"' and institution='"+Variables.Empresa+"'";

  int bb = Mn.SEARCHCODES(cmd,"code_classe");
  String querys = "call Insert_inscrire('"+matricule.getText().trim()+"',"+aa+",'"+xs+"',"+bb+",'"+dd+"','"+estatuses+"')";
  System.out.println("querys "+querys);    
//  Mn.MANIPULEDB(querys);
//  RemplirTable();
  }
  
  public void Repecher(int kod, String tercero){
      if(rexaminer.isSelected()){
          moyenne.setText(moyenne1.getText());
          String query="call Insert_motif('"+matricule.getText()+"',"+kod+",'"+tercero+"','"+moyenne.getText()+"','"+motif.getText()+"')";
          Mn.MANIPULEDB(query);
      }
  }
  
  private void InsertInscriptionNouveau(){
      int aa = 0;
      String xs = "";
      String ff="0.0";
      String xx = "";
     double moy = 0.0;
     double gg=0.0;
     int cod = 0;
     String estatuses="";
     int ss=0;
     String ee ="";
     String dd ="";
     int bb =0;
     String cc = academique.getText();
     
  String query="select e.code,e.code_tercero from eleve e left join tercero t on  (t.code = e.code and t.code_tercero = e.code_tercero) "
          + "join eleve_has_institution ehi on (e.code = ehi.code and e.code_tercero = ehi.code_tercero) "
          + "join institution i on(ehi.idinstitution = i.idinstitution) where e.matricule='"+matricule.getText()+"'" +
" and i.nom = '"+Variables.Empresa+"'";
    
      ResultSet rs = Mn.Rezulta(query);
    try {
        if(rs.last()){
            aa = rs.getInt("code");
            xs = rs.getString("code_tercero");
        }
    } catch (SQLException ex) {
        Logger.getLogger(Inscription.class.getName()).log(Level.SEVERE, null, ex);
    }
    
    String sql = "select * from qualification where institution='"+Variables.Empresa+"'";
     ResultSet ds = Mn.SEARCHDATA(sql);
       try {
           if(ds.first()){
           moy = ds.getDouble("moyenne_aceptable");
           }
       } catch (SQLException ex) {
           Logger.getLogger(Inscription.class.getName()).log(Level.SEVERE, null, ex);
       }
       
    // si elev la se yon ti moun kap vin lekol pou premye fwa, tankou yon ti moun 2 ans
     if(kinder.isSelected() && classe1.getSelectedItem().toString().equals("Selectionnez")){
          xx="Première";
          estatuses = "Nouveau (elle)";
          //insert enskripsyon elev la
          String cmd = "select code_classe from classe where description='"+xx+"' and institution='"+Variables.Empresa+"'";
            bb = Mn.SEARCHCODES(cmd,"code_classe");
            String querys = "call Insert_inscrire('"+matricule.getText().trim()+"',"+aa+",'"+xs+"',"+bb+",'"+cc+"','"+estatuses+"')";
            System.out.println(querys);
            Mn.MANIPULEDB(querys);
          
     }else if(kinder.isSelected() && !classe1.getSelectedItem().toString().equals("Selectionnez")){// si se yon ti moun kite lekol deja men se nan kinder li te ye
              Repecher(aa, xs);
              ee = moyenne.getText();
              dd = etablissement.getText();
        if(dd.isEmpty() || ee.isEmpty()){
            JOptionPane.showMessageDialog(null,"S'il vous plaît veuillez remplir la zone de texte Etabliessement et Moyenne, ils sont obligatoire.");
            etablissement.requestFocus();
        }else{
            gg=Double.valueOf(ee);
            if(gg < moy){
            estatuses = "Redoublé(e)";
            xx = classe1.getSelectedItem().toString();
            }else if(gg >= moy){
             estatuses = "Admis(e)";
             String cca=classe1.getSelectedItem().toString();
             String quer="select code_classe from classe where description='"+cca+"' and institution='"+Variables.Empresa+"'"; 
             ss=Mn.SEARCHCODES(quer, "code_classe")+1;
             quer="select description from classe where code_classe="+ss+"";
             ResultSet r=Mn.SEARCHDATA(quer);
            try {
                if(r.first()){
                   xx = r.getString("description"); 
                }
            } catch (SQLException ex) {
                Logger.getLogger(Inscription.class.getName()).log(Level.SEVERE, null, ex);
           }
         }
        String cmd = "select code_classe from classe where description='"+xx+"' and institution='"+Variables.Empresa+"'";
        bb = Mn.SEARCHCODES(cmd,"code_classe");
        //insert enskripsyon elev la
        String querys = "call Insert_inscrire('"+matricule.getText().trim()+"',"+aa+",'"+xs+"',"+bb+",'"+cc+"','"+estatuses+"')";
        Mn.MANIPULEDB(querys);
        // Insert relve de not elev la te fe nan lot lekol la 
         querys="call Insert_decision("+cod+",'"+matricule.getText().trim()+"',"+bb+",'"+cc+"','"+dd+"','"+ff+"','"+ee+"','"+estatuses+"','"+xx+"')";
        Mn.MANIPULEDB(querys);
        //eleve has decision
        int coddec = Mn.getCodeFromString("decision", "code_decision",matricule.getText().trim(),"matricule");
        querys = "call eleve_has_decision_procedure('"+matricule.getText().trim()+"',"+aa+",'"+xs+"',"+coddec+",'"+matricule.getText().trim()+"') ";
            System.out.println("eledec "+querys);
            Mn.MANIPULEDB(querys);

        //eleve has institution
        int codel = Mn.getCodeFromString("institution", "idinstitution", Variables.Empresa,"nom");
        querys = "call eleve_has_institution_procedure ('"+matricule.getText().trim()+"',"+aa+",'"+xs+"',"+codel+")";
            System.out.println("querys "+querys);
            Mn.MANIPULEDB(querys);
      //eleve has classe
        querys = "call eleve_has_classe_procedure ('"+matricule.getText().trim()+"',"+aa+",'"+xs+"',"+bb+") ";
            System.out.println("elevclass "+querys);
            Mn.MANIPULEDB(querys);
       }
     }if(!kinder.isSelected()){//sa se nan ka yon nouvo elev ki soti nan yon lot lekol e ki soti nan klas a pati de premye ane
         Repecher(aa, xs);   
         ee = moyenne.getText();
         dd = etablissement.getText(); 
       if(dd.isEmpty() || ee.isEmpty()){
            JOptionPane.showMessageDialog(null,"S'il vous plaît veuillez remplir la zone de texte Etabliessement et Moyenne, ils sont obligatoire.");
            etablissement.requestFocus();
        }else{
           gg=Double.valueOf(ee);
            if(gg < moy){
            estatuses = "Redoublé(e)";
            xx = classe1.getSelectedItem().toString();
            }else if(gg >= moy){
             estatuses = "Admis(e)";
             String cca=classe1.getSelectedItem().toString();
             String quer="select code_classe from classe where description='"+cca+"' and institution='"+Variables.Empresa+"'"; 
             ss=Mn.SEARCHCODES(quer, "code_classe")+1;
             quer="select description from classe where code_classe="+ss+"";
             ResultSet r=Mn.SEARCHDATA(quer);
            try {
                if(r.first()){
                   xx = r.getString("description"); 
                }
            } catch (SQLException ex) {
                Logger.getLogger(Inscription.class.getName()).log(Level.SEVERE, null, ex);
           }
         }
            
         gg=Double.valueOf(ee);
            if(gg < moy){
            estatuses = "Redoublé(e)";
            xx = classe1.getSelectedItem().toString();
            }else if(gg >= moy){
             estatuses = "Admis(e)";
             String cca=classe1.getSelectedItem().toString();
             String quer="select code_classe from classe where description='"+cca+"' and institution='"+Variables.Empresa+"'"; 
             ss=Mn.SEARCHCODES(quer, "code_classe")+1;
             quer="select description from classe where code_classe="+ss+"";
             ResultSet r=Mn.SEARCHDATA(quer);
            try {
                if(r.first()){
                   xx = r.getString("description"); 
                }
            } catch (SQLException ex) {
                Logger.getLogger(Inscription.class.getName()).log(Level.SEVERE, null, ex);
           }
         }
            String cmd = "select code_classe from classe where description='"+xx+"' and institution='"+Variables.Empresa+"'";
            bb = Mn.SEARCHCODES(cmd,"code_classe");
            //insert enskripsyon elev la
            String querys;
            querys = "call Insert_inscrire('"+matricule.getText().trim()+"',"+aa+",'"+xs+"',"+bb+",'"+cc+"','"+estatuses+"')";
            if(Mn.MANIPULEDB(querys)){
            
            // Insert relve de not elev la te fe nan lot lekol la 
             querys="call Insert_decision("+cod+",'"+matricule.getText().trim()+"',"+bb+",'"+cc+"','"+dd+"','"+ff+"','"+ee+"','"+estatuses+"','"+xx+"')";
            Mn.MANIPULEDB(querys);
            //eleve has decision
            int coddec = Mn.getCodeFromString("decision", "code_decision",matricule.getText().trim(),"matricule");
            querys = "call eleve_has_decision_procedure('"+matricule.getText().trim()+"',"+aa+",'"+xs+"',"+coddec+",'"+matricule.getText().trim()+"') ";
                System.out.println("eledec "+querys);
                Mn.MANIPULEDB(querys);

            //eleve has institution
            int codel = Mn.getCodeFromString("institution", "idinstitution", Variables.Empresa,"nom");
            querys = "call eleve_has_institution_procedure ('"+matricule.getText().trim()+"',"+aa+",'"+xs+"',"+codel+")";
                System.out.println("querys "+querys);
                Mn.MANIPULEDB(querys);
          //eleve has classe
            querys = "call eleve_has_classe_procedure ('"+matricule.getText().trim()+"',"+aa+",'"+xs+"',"+bb+") ";
            System.out.println("elevclass "+querys);
            Mn.MANIPULEDB(querys); 
            }
       }
     }
   RemplirTable();
}
  
private void RemplirTable(){
    
 String query = "select matricule,nom,prenom,classe, moyenne_generale, estatus,superieur \n" +
"from show_eleve_decision where annee_academique = '"+academique1.getText()+"'";
 
 System.out.println("query : "+query);
 ResultSet rs = Mn.SEARCHDATA(query);
 
 try {
        md.setRowCount(0);
       while(rs.next()){
         String[] vect={rs.getString("matricule"),rs.getString("nom"),rs.getString("prenom"),
             rs.getString("classe"),rs.getString("moyenne_generale"),rs.getString("estatus"),rs.getString("superieur")};
         Mn.ADD_TABLE(md, vect);
        }
    } catch (SQLException ex) {
        Logger.getLogger(Entrer_note.class.getName()).log(Level.SEVERE, null, ex);
    }
}

private void RemplirClasse(){
String query="select description,section from classe where institution='"+Variables.Empresa+"'";
Mn.FULLTABLE(query, tblclasse, mds);
}

private void RemplirEleve(){
 String aa=tblclasse.getValueAt(tblclasse.getSelectedRow(),0).toString();
 String aaa=tblclasse.getValueAt(tblclasse.getSelectedRow(),1).toString();
 String cmd="select code_classe from classe where description='"+aa+"' and section='"+aaa+"' and institution='"+Variables.Empresa+"'";
 int bb=Mn.SEARCHCODES(cmd,"code_classe");
String query="select e.matricule,t.nom,t.prenom from inscription i left join eleve e on (e.code=i.code and e.code_tercero=i.code_tercero) \n" +
"left join tercero t on (t.code=i.code and t.code_tercero=i.code_tercero) where i.code_classe="+bb+" and i.annee_academique='"+academique.getText()+"' order by nom asc";
Mn.FULLTABLE(query, tbleleve, mdd);
}


private void controledeleveAdmis(){
    String estatuses="Admis(e)";
    String estatusese="Redoublé(e)";
    ResultSet rs=null;
    String klas=tblclasse.getValueAt(tblclasse.getSelectedRow(),0).toString();
    String seksyon=tblclasse.getValueAt(tblclasse.getSelectedRow(),1).toString();
    String sql="";
  int codeclasse=Mn.SEARCHCODE("code_classe","description","section",klas,seksyon, "classe");
  
  String querys="select annee from annee_academique where code_annee =(select max(code_annee)-1 as code from annee_academique)";
   rs=Mn.SEARCHDATA(querys);
    try {
        if(rs.first()){
         sql=" and d.annee_academique='"+rs.getString("annee")+"'";   
        }else{
            sql="";
        }
    } catch (SQLException ex) {
        Logger.getLogger(Inscription.class.getName()).log(Level.SEVERE, null, ex);
    }
   //kontinye la 
  String query="select count(d.matricule) as quantiteleve from decision d left join eleve e on e.matricule=d.matricule"
          + " where d.superieur='"+cantclasse.getText()+"' and d.estatus='"+estatuses+"' "+sql+"";  
  rs=Mn.SEARCHDATA(query);
    try {
        if(rs.first()){
            canteleve4.setText(rs.getString("quantiteleve"));
            canteleve5.setText(rs.getString("quantiteleve"));
        } else{
           canteleve4.setText("0");
           canteleve5.setText("0"); 
        }
    } catch (SQLException ex) {
        Logger.getLogger(Inscription.class.getName()).log(Level.SEVERE, null, ex);
    }
    
    String queryy="select count(d.matricule) as quantiteleve from decision d left join eleve e on e.matricule=d.matricule"
            + " where d.superieur='"+cantclasse.getText()+"' and d.estatus='"+estatusese+"' "+sql+""; 
//    System.out.println(queryy);
  rs=Mn.SEARCHDATA(queryy);
    try {
        if(rs.first()){
            canteleve6.setText(rs.getString("quantiteleve"));
            canteleve7.setText(rs.getString("quantiteleve"));
            canteleve9.setText(String.valueOf(Integer.parseInt(canteleve4.getText())+Integer.parseInt(canteleve7.getText())));
            canteleve11.setText(canteleve9.getText());
        } else{
           canteleve6.setText("0");
           canteleve6.setText("0"); 
        }
    } catch (SQLException ex) {
        Logger.getLogger(Inscription.class.getName()).log(Level.SEVERE, null, ex);
    }
}

private void controledeleveInscris(){
    String estatuses="Admis(e)";
    String estatusese="Redoublé(e)";
    ResultSet rs=null;
    String sql="";
    String klas=tblclasse.getValueAt(tblclasse.getSelectedRow(),0).toString();
    String seksyon=tblclasse.getValueAt(tblclasse.getSelectedRow(),1).toString();
    String cmd="select code_classe from classe where description='"+klas+"' and section='"+seksyon+"'"; 
  int codeclasse=Mn.SEARCHCODES(cmd,"code_classe");
    String querys="select annee from annee_academique where code_annee =(select max(code_annee)-1 as code from annee_academique)";
   rs=Mn.SEARCHDATA(querys);
    try {
        if(rs.first()){
         sql=" and i.annee_academique='"+rs.getString("annee")+"'";   
        }else{
            sql="";
        }
    } catch (SQLException ex) {
        Logger.getLogger(Inscription.class.getName()).log(Level.SEVERE, null, ex);
    }
  
  String query="select count(i.code_tercero) as quantiteleve from inscription i left join tercero t on (t.code=i.code and t.code_tercero=i.code_tercero)"
          + " where i.code_classe='"+codeclasse+"' and annee_academique='"+academique.getText()+"' and estatus='"+estatuses+"' ";  
//  System.out.println(query);
  rs=Mn.SEARCHDATA(query);
    try {
        if(rs.first()){
            canteleve.setText(rs.getString("quantiteleve"));
        } else{
           canteleve.setText("0"); 
        }
    } catch (SQLException ex) {
        Logger.getLogger(Inscription.class.getName()).log(Level.SEVERE, null, ex);
    }
    
    String queryy="select count(i.code_tercero) as quantiteleve from inscription i left join tercero t on (t.code=i.code and t.code_tercero=i.code_tercero)"
          + " where i.code_classe='"+codeclasse+"' and annee_academique='"+academique.getText()+"' and estatus='"+estatusese+"' ";  
//    System.out.println(queryy);
  rs=Mn.SEARCHDATA(queryy);
    try {
        if(rs.first()){
            canteleve2.setText(rs.getString("quantiteleve"));
            canteleve9.setText(String.valueOf(Integer.parseInt(canteleve4.getText())+Integer.parseInt(canteleve7.getText())));
            canteleve11.setText(canteleve9.getText());
        } else{
           canteleve2.setText("0");
        }
    } catch (SQLException ex) {
        Logger.getLogger(Inscription.class.getName()).log(Level.SEVERE, null, ex);
    }
}


private void AnneAnterieur(){
    int cc=Mn.MAXCODE("code_annee","annee_academique");
    int cod=cc-2;
    String query="select annee from annee_academique where code_annee="+cod+"";
    ResultSet rs=Mn.SEARCHDATA(query);
    try {
        if(rs.first()){
          academique1.setText(rs.getString("annee"));
        }
    } catch (SQLException ex) {
        Logger.getLogger(Inscription.class.getName()).log(Level.SEVERE, null, ex);
    }
}


    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel3 = new javax.swing.JPanel();
        jSeparator1 = new javax.swing.JSeparator();
        jPanel4 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblinscription = new javax.swing.JTable(){
            public boolean isCellEditable(int rowIndex, int colIndex){
                return false;
            }
        };
        jPanel5 = new javax.swing.JPanel();
        jLabel18 = new javax.swing.JLabel();
        academique1 = new javax.swing.JLabel();
        jPanel6 = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        recherche = new org.edisoncor.gui.textField.TextFieldRectBackground();
        jPanel7 = new javax.swing.JPanel();
        date = new javax.swing.JLabel();
        jPanel8 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        jPanel9 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jPanel10 = new javax.swing.JPanel();
        academique = new javax.swing.JLabel();
        jPanel11 = new javax.swing.JPanel();
        ancien = new javax.swing.JRadioButton();
        nouveau = new javax.swing.JRadioButton();
        ancien1 = new javax.swing.JRadioButton();
        jPanel12 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jPanel13 = new javax.swing.JPanel();
        matricule = new javax.swing.JTextField();
        nomeleve = new javax.swing.JLabel();
        moyenne = new org.edisoncor.gui.textField.TextFieldRectBackground();
        acceseleve = new javax.swing.JLabel();
        etablissement = new org.edisoncor.gui.textField.TextFieldRectBackground();
        classe1 = new javax.swing.JComboBox();
        kinder = new javax.swing.JCheckBox();
        jPanel16 = new javax.swing.JPanel();
        moyenne1 = new org.edisoncor.gui.textField.TextFieldRectBackground();
        rexaminer = new javax.swing.JCheckBox();
        lblmoyenne = new javax.swing.JLabel();
        jSeparator2 = new javax.swing.JSeparator();
        motif1 = new javax.swing.JLabel();
        motif = new org.edisoncor.gui.textField.TextFieldRectBackground();
        jPanel14 = new javax.swing.JPanel();
        jPanel15 = new javax.swing.JPanel();
        jLabel12 = new javax.swing.JLabel();
        cantsection = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        cantclasse = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        canteleve = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        canteleve4 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        superieur = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        canteleve1 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        canteleve5 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        superieur1 = new javax.swing.JLabel();
        jLabel21 = new javax.swing.JLabel();
        canteleve2 = new javax.swing.JLabel();
        jLabel22 = new javax.swing.JLabel();
        canteleve6 = new javax.swing.JLabel();
        jLabel23 = new javax.swing.JLabel();
        jLabel24 = new javax.swing.JLabel();
        canteleve3 = new javax.swing.JLabel();
        jLabel25 = new javax.swing.JLabel();
        canteleve7 = new javax.swing.JLabel();
        jLabel26 = new javax.swing.JLabel();
        jLabel27 = new javax.swing.JLabel();
        canteleve8 = new javax.swing.JLabel();
        jLabel28 = new javax.swing.JLabel();
        canteleve9 = new javax.swing.JLabel();
        jLabel29 = new javax.swing.JLabel();
        canteleve10 = new javax.swing.JLabel();
        jLabel30 = new javax.swing.JLabel();
        canteleve11 = new javax.swing.JLabel();
        jSeparator3 = new javax.swing.JSeparator();
        jSeparator4 = new javax.swing.JSeparator();
        jSeparator5 = new javax.swing.JSeparator();
        jPanel17 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tblclasse = new javax.swing.JTable(){
            public boolean isCellEditable(int rowIndex, int colIndex){
                return false;
            }
        };
        jPanel18 = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        tbleleve = new javax.swing.JTable(){
            public boolean isCellEditable(int rowIndex, int colIndex){
                return false;
            }
        };
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        assignere = new javax.swing.JMenuItem();
        jMenu5 = new javax.swing.JMenu();
        jMenuItem1 = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(new java.awt.CardLayout());

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));

        jPanel2.setBackground(new java.awt.Color(87, 148, 210));
        jPanel2.setBorder(new org.edisoncor.gui.util.DropShadowBorder());

        jLabel5.setBackground(new java.awt.Color(255, 255, 255));
        jLabel5.setFont(new java.awt.Font("Lucida Grande", 1, 18)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(255, 255, 255));
        jLabel5.setText("Inscription");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel5)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(26, Short.MAX_VALUE))
        );

        jTabbedPane1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));
        jPanel3.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseDragged(java.awt.event.MouseEvent evt) {
                jPanel3MouseDragged(evt);
            }
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                jPanel3MouseMoved(evt);
            }
        });
        jPanel3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                jPanel3MouseReleased(evt);
            }
        });

        jPanel4.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        tblinscription.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tblinscription.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                tblinscriptionMousePressed(evt);
            }
        });
        jScrollPane1.setViewportView(tblinscription);

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 1170, Short.MAX_VALUE)
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 232, Short.MAX_VALUE)
        );

        jPanel5.setBackground(new java.awt.Color(255, 255, 255));
        jPanel5.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel18.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel18.setForeground(new java.awt.Color(102, 0, 0));
        jLabel18.setText("Décision de fin d'année académique: ");

        academique1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        academique1.setForeground(new java.awt.Color(102, 0, 0));
        academique1.setText("0");

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel18)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(academique1, javax.swing.GroupLayout.PREFERRED_SIZE, 104, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(academique1, javax.swing.GroupLayout.DEFAULT_SIZE, 28, Short.MAX_VALUE)
            .addComponent(jLabel18, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        jPanel6.setBackground(new java.awt.Color(255, 255, 255));
        jPanel6.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel6.setText("Recherche:");

        recherche.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                rechercheKeyReleased(evt);
            }
        });

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addGap(1, 1, 1)
                .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(7, 7, 7)
                .addComponent(recherche, javax.swing.GroupLayout.PREFERRED_SIZE, 490, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(jLabel6)
                .addComponent(recherche, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        jPanel7.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        date.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        date.setText("date");

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(date, javax.swing.GroupLayout.DEFAULT_SIZE, 276, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(date, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        jPanel8.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel4.setText("Date:");

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel8Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel4)
                .addContainerGap())
        );
        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel4, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        jPanel9.setBackground(new java.awt.Color(87, 148, 210));
        jPanel9.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel1.setText("Année Académique: ");

        javax.swing.GroupLayout jPanel9Layout = new javax.swing.GroupLayout(jPanel9);
        jPanel9.setLayout(jPanel9Layout);
        jPanel9Layout.setHorizontalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel9Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 147, Short.MAX_VALUE))
        );
        jPanel9Layout.setVerticalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel9Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(jLabel1))
        );

        jPanel10.setBackground(new java.awt.Color(87, 148, 210));
        jPanel10.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        academique.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        academique.setForeground(new java.awt.Color(255, 255, 255));
        academique.setText("0");

        javax.swing.GroupLayout jPanel10Layout = new javax.swing.GroupLayout(jPanel10);
        jPanel10.setLayout(jPanel10Layout);
        jPanel10Layout.setHorizontalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel10Layout.createSequentialGroup()
                .addComponent(academique, javax.swing.GroupLayout.DEFAULT_SIZE, 94, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel10Layout.setVerticalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel10Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(academique))
        );

        jPanel11.setBackground(new java.awt.Color(87, 148, 210));
        jPanel11.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        ancien.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        ancien.setForeground(new java.awt.Color(255, 255, 255));
        ancien.setText("Elève existant(e) ");
        ancien.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                ancienItemStateChanged(evt);
            }
        });
        ancien.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                ancienMouseClicked(evt);
            }
        });

        nouveau.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        nouveau.setForeground(new java.awt.Color(255, 255, 255));
        nouveau.setText("Nouveau (elle) élève");
        nouveau.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                nouveauItemStateChanged(evt);
            }
        });
        nouveau.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                nouveauMouseClicked(evt);
            }
        });
        nouveau.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nouveauActionPerformed(evt);
            }
        });

        ancien1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        ancien1.setForeground(new java.awt.Color(255, 255, 255));
        ancien1.setText("Ancien (ne) élève ");
        ancien1.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                ancien1ItemStateChanged(evt);
            }
        });
        ancien1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                ancien1MouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanel11Layout = new javax.swing.GroupLayout(jPanel11);
        jPanel11.setLayout(jPanel11Layout);
        jPanel11Layout.setHorizontalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel11Layout.createSequentialGroup()
                .addGap(138, 138, 138)
                .addComponent(ancien)
                .addGap(101, 101, 101)
                .addComponent(nouveau)
                .addGap(141, 141, 141)
                .addComponent(ancien1)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel11Layout.setVerticalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(ancien, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                .addComponent(nouveau, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(ancien1, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        jPanel12.setBackground(new java.awt.Color(255, 255, 255));
        jPanel12.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel3.setText("Matricule:");

        jLabel7.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel7.setText("Dernière établissement:");

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel2.setText("Status:");

        jLabel17.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel17.setText("Dernière Classe:");

        jLabel8.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel8.setText("Moyenne:");

        javax.swing.GroupLayout jPanel12Layout = new javax.swing.GroupLayout(jPanel12);
        jPanel12.setLayout(jPanel12Layout);
        jPanel12Layout.setHorizontalGroup(
            jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel12Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel8, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel17, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel12Layout.createSequentialGroup()
                .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 154, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        jPanel12Layout.setVerticalGroup(
            jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel12Layout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel2)
                .addGap(18, 18, 18)
                .addComponent(jLabel7)
                .addGap(28, 28, 28)
                .addComponent(jLabel17)
                .addGap(34, 34, 34)
                .addComponent(jLabel8)
                .addGap(20, 20, 20))
        );

        jPanel13.setBackground(new java.awt.Color(255, 255, 255));
        jPanel13.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        matricule.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        matricule.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                matriculeFocusLost(evt);
            }
        });
        matricule.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                matriculeKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                matriculeKeyTyped(evt);
            }
        });

        nomeleve.setBackground(new java.awt.Color(87, 148, 210));
        nomeleve.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        nomeleve.setForeground(new java.awt.Color(255, 255, 255));
        nomeleve.setText("Nom de l'élève  ");
        nomeleve.setOpaque(true);

        acceseleve.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        acceseleve.setText("jLabel17");

        classe1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        classe1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Selectionnez", "Petit", "Primaire", "Secondaire" }));

        kinder.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        kinder.setText("Kindergaten");
        kinder.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        kinder.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                kinderItemStateChanged(evt);
            }
        });

        jPanel16.setBackground(new java.awt.Color(87, 148, 210));
        jPanel16.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        moyenne1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                moyenne1ActionPerformed(evt);
            }
        });

        rexaminer.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        rexaminer.setForeground(new java.awt.Color(255, 255, 255));
        rexaminer.setText("Re-Examiner");
        rexaminer.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                rexaminerItemStateChanged(evt);
            }
        });

        lblmoyenne.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        lblmoyenne.setForeground(new java.awt.Color(255, 255, 255));
        lblmoyenne.setText("Moyenne:");

        motif1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        motif1.setForeground(new java.awt.Color(255, 255, 255));
        motif1.setText("Motif:");

        motif.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                motifActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel16Layout = new javax.swing.GroupLayout(jPanel16);
        jPanel16.setLayout(jPanel16Layout);
        jPanel16Layout.setHorizontalGroup(
            jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSeparator2, javax.swing.GroupLayout.Alignment.TRAILING)
            .addGroup(jPanel16Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel16Layout.createSequentialGroup()
                        .addComponent(rexaminer)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(jPanel16Layout.createSequentialGroup()
                        .addComponent(motif1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(motif, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lblmoyenne, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(moyenne1, javax.swing.GroupLayout.PREFERRED_SIZE, 88, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        jPanel16Layout.setVerticalGroup(
            jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel16Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(rexaminer)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(moyenne1, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblmoyenne)
                    .addComponent(motif1)
                    .addComponent(motif, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel13Layout = new javax.swing.GroupLayout(jPanel13);
        jPanel13.setLayout(jPanel13Layout);
        jPanel13Layout.setHorizontalGroup(
            jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel13Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel13Layout.createSequentialGroup()
                        .addComponent(matricule, javax.swing.GroupLayout.PREFERRED_SIZE, 220, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(nomeleve, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel13Layout.createSequentialGroup()
                        .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel13Layout.createSequentialGroup()
                                .addComponent(acceseleve, javax.swing.GroupLayout.PREFERRED_SIZE, 220, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(10, 10, 10)
                                .addComponent(kinder))
                            .addComponent(etablissement, javax.swing.GroupLayout.PREFERRED_SIZE, 549, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 445, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel13Layout.createSequentialGroup()
                        .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(moyenne, javax.swing.GroupLayout.PREFERRED_SIZE, 154, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(classe1, javax.swing.GroupLayout.PREFERRED_SIZE, 282, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jPanel16, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel13Layout.setVerticalGroup(
            jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel13Layout.createSequentialGroup()
                .addContainerGap(13, Short.MAX_VALUE)
                .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(nomeleve, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(matricule, javax.swing.GroupLayout.DEFAULT_SIZE, 30, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(acceseleve)
                    .addComponent(kinder))
                .addGap(14, 14, 14)
                .addComponent(etablissement, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel13Layout.createSequentialGroup()
                        .addComponent(classe1, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(moyenne, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jPanel16, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, 359, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(jPanel9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addComponent(jPanel11, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(jPanel12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel13, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel3Layout.createSequentialGroup()
                    .addGap(0, 587, Short.MAX_VALUE)
                    .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 587, Short.MAX_VALUE)))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(jPanel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jPanel9, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel12, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(jPanel13, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel3Layout.createSequentialGroup()
                    .addGap(0, 278, Short.MAX_VALUE)
                    .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 280, Short.MAX_VALUE)))
        );

        jTabbedPane1.addTab("INSCRIPTION", jPanel3);

        jPanel14.setBackground(new java.awt.Color(255, 255, 255));

        jPanel15.setBackground(new java.awt.Color(87, 148, 210));
        jPanel15.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel12.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel12.setForeground(new java.awt.Color(255, 255, 255));
        jLabel12.setText("Section:");

        cantsection.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        cantsection.setForeground(new java.awt.Color(255, 255, 255));
        cantsection.setText("section");

        jLabel10.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel10.setForeground(new java.awt.Color(255, 255, 255));
        jLabel10.setText("Classe:");

        cantclasse.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        cantclasse.setForeground(new java.awt.Color(255, 255, 255));
        cantclasse.setText("classe");

        jLabel9.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel9.setForeground(new java.awt.Color(255, 255, 255));
        jLabel9.setText("Quantité élèves  inscris:  ");

        canteleve.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        canteleve.setForeground(new java.awt.Color(255, 255, 255));
        canteleve.setText("0");

        jLabel13.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel13.setForeground(new java.awt.Color(255, 255, 255));
        jLabel13.setText("Sur:");

        canteleve4.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        canteleve4.setForeground(new java.awt.Color(255, 255, 255));
        canteleve4.setText("0");

        jLabel15.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel15.setForeground(new java.awt.Color(255, 255, 255));
        jLabel15.setText("Admis en:");

        superieur.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        superieur.setForeground(new java.awt.Color(255, 255, 255));
        superieur.setText("Superieur");

        jLabel11.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel11.setForeground(new java.awt.Color(255, 255, 255));
        jLabel11.setText("Quantité élèves  non  inscris:  ");

        canteleve1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        canteleve1.setForeground(new java.awt.Color(255, 255, 255));
        canteleve1.setText("0");

        jLabel14.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel14.setForeground(new java.awt.Color(255, 255, 255));
        jLabel14.setText("Sur:");

        canteleve5.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        canteleve5.setForeground(new java.awt.Color(255, 255, 255));
        canteleve5.setText("0");

        jLabel16.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel16.setForeground(new java.awt.Color(255, 255, 255));
        jLabel16.setText("Admis en:");

        superieur1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        superieur1.setForeground(new java.awt.Color(255, 255, 255));
        superieur1.setText("Superieur");

        jLabel21.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel21.setForeground(new java.awt.Color(255, 255, 255));
        jLabel21.setText("Quantité élèves  inscris:  ");

        canteleve2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        canteleve2.setForeground(new java.awt.Color(255, 255, 255));
        canteleve2.setText("0");

        jLabel22.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel22.setForeground(new java.awt.Color(255, 255, 255));
        jLabel22.setText("Sur:");

        canteleve6.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        canteleve6.setForeground(new java.awt.Color(255, 255, 255));
        canteleve6.setText("0");

        jLabel23.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel23.setForeground(new java.awt.Color(255, 255, 255));
        jLabel23.setText("Redoublé(s) la.");

        jLabel24.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel24.setForeground(new java.awt.Color(255, 255, 255));
        jLabel24.setText("Quantité élèves  non  inscris:  ");

        canteleve3.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        canteleve3.setForeground(new java.awt.Color(255, 255, 255));
        canteleve3.setText("0");

        jLabel25.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel25.setForeground(new java.awt.Color(255, 255, 255));
        jLabel25.setText("Sur:");

        canteleve7.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        canteleve7.setForeground(new java.awt.Color(255, 255, 255));
        canteleve7.setText("0");

        jLabel26.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel26.setForeground(new java.awt.Color(255, 255, 255));
        jLabel26.setText("Redoublé(s) la.");

        jLabel27.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel27.setForeground(new java.awt.Color(255, 255, 255));
        jLabel27.setText("Total élève  inscris:  ");

        canteleve8.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        canteleve8.setForeground(new java.awt.Color(255, 255, 255));
        canteleve8.setText("0");

        jLabel28.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel28.setForeground(new java.awt.Color(255, 255, 255));
        jLabel28.setText("Sur:");

        canteleve9.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        canteleve9.setForeground(new java.awt.Color(255, 255, 255));
        canteleve9.setText("0");

        jLabel29.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel29.setForeground(new java.awt.Color(255, 255, 255));
        jLabel29.setText("Total élève  non  inscris:  ");

        canteleve10.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        canteleve10.setForeground(new java.awt.Color(255, 255, 255));
        canteleve10.setText("0");

        jLabel30.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel30.setForeground(new java.awt.Color(255, 255, 255));
        jLabel30.setText("Sur:");

        canteleve11.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        canteleve11.setForeground(new java.awt.Color(255, 255, 255));
        canteleve11.setText("0");

        javax.swing.GroupLayout jPanel15Layout = new javax.swing.GroupLayout(jPanel15);
        jPanel15.setLayout(jPanel15Layout);
        jPanel15Layout.setHorizontalGroup(
            jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel15Layout.createSequentialGroup()
                .addGap(11, 11, 11)
                .addGroup(jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jSeparator4, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jSeparator3)
                    .addComponent(jSeparator5, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel15Layout.createSequentialGroup()
                        .addGroup(jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(jLabel9)
                            .addComponent(jLabel11))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(jPanel15Layout.createSequentialGroup()
                                .addComponent(canteleve1, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel14, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(canteleve5, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel16, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addGroup(jPanel15Layout.createSequentialGroup()
                                .addComponent(canteleve, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(canteleve4, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel15)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(superieur, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(superieur1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addGroup(jPanel15Layout.createSequentialGroup()
                        .addGroup(jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel15Layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 48, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(cantsection, javax.swing.GroupLayout.PREFERRED_SIZE, 155, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, 53, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(cantclasse, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel15Layout.createSequentialGroup()
                                .addGroup(jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(jLabel21)
                                    .addComponent(jLabel24))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel15Layout.createSequentialGroup()
                                        .addComponent(canteleve2, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(jLabel22, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(canteleve6, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jLabel23))
                                    .addGroup(jPanel15Layout.createSequentialGroup()
                                        .addComponent(canteleve3, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(jLabel25, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(canteleve7, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jLabel26))))
                            .addGroup(jPanel15Layout.createSequentialGroup()
                                .addGap(29, 29, 29)
                                .addGroup(jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(jLabel27)
                                    .addComponent(jLabel29))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addGroup(jPanel15Layout.createSequentialGroup()
                                        .addComponent(canteleve8, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(jLabel28, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(canteleve9, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(jPanel15Layout.createSequentialGroup()
                                        .addComponent(canteleve10, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(jLabel30, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(canteleve11, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel15Layout.setVerticalGroup(
            jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel15Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel12)
                    .addGroup(jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(cantsection)
                        .addComponent(jLabel10)
                        .addComponent(cantclasse)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator3, javax.swing.GroupLayout.PREFERRED_SIZE, 7, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9)
                    .addComponent(canteleve)
                    .addComponent(jLabel13)
                    .addComponent(canteleve4)
                    .addComponent(jLabel15)
                    .addComponent(superieur))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel11)
                    .addComponent(canteleve1)
                    .addComponent(jLabel14)
                    .addComponent(canteleve5)
                    .addComponent(jLabel16)
                    .addComponent(superieur1))
                .addGap(5, 5, 5)
                .addComponent(jSeparator4, javax.swing.GroupLayout.PREFERRED_SIZE, 7, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel21)
                    .addComponent(canteleve2)
                    .addComponent(jLabel22)
                    .addComponent(canteleve6)
                    .addComponent(jLabel23))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel24)
                    .addComponent(canteleve3)
                    .addComponent(jLabel25)
                    .addComponent(canteleve7)
                    .addComponent(jLabel26))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator5, javax.swing.GroupLayout.PREFERRED_SIZE, 7, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel27)
                    .addComponent(canteleve8)
                    .addComponent(jLabel28)
                    .addComponent(canteleve9))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel29)
                    .addComponent(canteleve10)
                    .addComponent(jLabel30)
                    .addComponent(canteleve11))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel17.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        tblclasse.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tblclasse.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblclasseMouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(tblclasse);

        javax.swing.GroupLayout jPanel17Layout = new javax.swing.GroupLayout(jPanel17);
        jPanel17.setLayout(jPanel17Layout);
        jPanel17Layout.setHorizontalGroup(
            jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
        );
        jPanel17Layout.setVerticalGroup(
            jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
        );

        jPanel18.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        tbleleve.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane3.setViewportView(tbleleve);

        javax.swing.GroupLayout jPanel18Layout = new javax.swing.GroupLayout(jPanel18);
        jPanel18.setLayout(jPanel18Layout);
        jPanel18Layout.setHorizontalGroup(
            jPanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 1170, Short.MAX_VALUE)
        );
        jPanel18Layout.setVerticalGroup(
            jPanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 329, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout jPanel14Layout = new javax.swing.GroupLayout(jPanel14);
        jPanel14.setLayout(jPanel14Layout);
        jPanel14Layout.setHorizontalGroup(
            jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel14Layout.createSequentialGroup()
                .addComponent(jPanel17, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel15, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addComponent(jPanel18, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel14Layout.setVerticalGroup(
            jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel14Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel17, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel15, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel18, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        jTabbedPane1.addTab("CONSULTATION", jPanel14);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jTabbedPane1))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(9, Short.MAX_VALUE))
        );

        getContentPane().add(jPanel1, "card2");

        jMenuBar1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N

        jMenu1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/fichier.jpg"))); // NOI18N
        jMenu1.setText("Action");
        jMenu1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N

        assignere.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_E, java.awt.event.InputEvent.CTRL_MASK));
        assignere.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/ButtonFoto/icons8-ok_filled.png"))); // NOI18N
        assignere.setText("Executer");
        assignere.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                assignereActionPerformed(evt);
            }
        });
        jMenu1.add(assignere);

        jMenuBar1.add(jMenu1);

        jMenu5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/editer.jpg"))); // NOI18N
        jMenu5.setText("Edition");
        jMenu5.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N

        jMenuItem1.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F1, 0));
        jMenuItem1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/icons8-help.png"))); // NOI18N
        jMenuItem1.setText("Aide");
        jMenu5.add(jMenuItem1);

        jMenuBar1.add(jMenu5);

        setJMenuBar(jMenuBar1);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void assignereActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_assignereActionPerformed
        if(ancien.isSelected()){
          InsertInscriptionAncien();
       }else if(nouveau.isSelected()){
          InsertInscriptionNouveau();
       }else if(ancien1.isSelected() ){
           InsertInscriptionNouveau();
       }
//         Waitts attend= new Waitts(null, rootPaneCheckingEnabled);
//         attend.show();
//       Rp.SENDQUERY("Inscription",matricule.getText(),academique.getText());
        Nettoyer();
    }//GEN-LAST:event_assignereActionPerformed

    private void tblinscriptionMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblinscriptionMousePressed

        JTable table =(JTable) evt.getSource();
        Point pt = evt.getPoint();
        int row = table.rowAtPoint(pt);
        if (evt.getClickCount() == 2) {

            ancien.setSelected(true);
            nouveau.setSelected(false);
            ancien1.setSelected(false);
            etablissement.setEnabled(false);
            moyenne.setEnabled(true);

            matricule.setText(tblinscription.getValueAt(tblinscription.getSelectedRow(), 0).toString());
            //        classe.setSelectedItem(tblinscription.getValueAt(tblinscription.getSelectedRow(), 6).toString());
            classe1.setSelectedItem(tblinscription.getValueAt(tblinscription.getSelectedRow(), 3).toString());
            moyenne.setText(tblinscription.getValueAt(tblinscription.getSelectedRow(), 4).toString());

            String query="";
            if(matricule.getText().isEmpty()){
                Nettoyer();
            }else{
                if(ancien.isSelected()){
                    query="select t.nom,t.prenom,e.estatus from tercero t left join eleve e "
                    + "on e.code_tercero=t.code_tercero where e.matricule ='"+matricule.getText()+"' and t.institution='"+Variables.Empresa+"'";
                }else if(nouveau.isSelected() || ancien1.isSelected() ){
                    query="select t.nom,t.prenom,e.estatus from tercero t left join eleve e "
                    + "on e.code_tercero=t.code_tercero where e.matricule = '"+matricule.getText()+"' and t.institution='"+Variables.Empresa+"' ";
                }

                ResultSet rs=Mn.SEARCHDATA(query);
                try {
                    while(rs.next()){
                        nomeleve.setText(rs.getString("nom")+" "+rs.getString("prenom"));
                        if(rs.getString("estatus").equals("1")){
                            acceseleve.setText("Actif(ve).");
                            acceseleve.setForeground(Color.GREEN.darker());
                        }else{
                            acceseleve.setText("Inactif(ve).");
                            acceseleve.setForeground(Color.RED.darker());
                        }
                    }
                } catch (SQLException ex) {
                    Logger.getLogger(Entrer_note.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

            if(acceseleve.getText().equals("Inactif(ve).")){
                JOptionPane.showMessageDialog(this,"Cet élève est inactif(ve) on ne peut pas réaliser l’inscription."
                    + " Pour effectuer cette inscription il(elle) doit être réactivé.");
                matricule.requestFocus();
            }

        }
    }//GEN-LAST:event_tblinscriptionMousePressed

    private void rechercheKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_rechercheKeyReleased
        recherche.setText(recherche.getText().toUpperCase());
        String query="select t.nom,t.prenom,e.matricule,c.description as classe, d.moyenne, d.estatus,d.superieur \n" +
        "from decision d left join tercero t on t.code_tercero=d.code_tercero\n" +
        "left join classe c on c.code_classe=d.code_classe\n" +
        "left join eleve e on e.code_tercero=d.code_tercero "
        + "where t.nom like '%"+recherche.getText()+"%' or t.prenom like '%"+recherche.getText()+"%' "
        + "or e.matricule like '%"+recherche.getText()+"%' or c.description like '%"+recherche.getText()+"%' or "
        + "d.estatus like '%"+recherche.getText()+"%' and d.annee_academique='"+academique.getText()+"' and t.institution='"+Variables.Empresa+"' ";
        ResultSet rs=Mn.SEARCHDATA(query);
        try {
            md.setRowCount(0);
            while(rs.next()){
                String[] vect={rs.getString("matricule"),rs.getString("nom"),rs.getString("prenom"),
                    rs.getString("classe"),rs.getString("moyenne"),rs.getString("estatus"),rs.getString("superieur")};
                Mn.ADD_TABLE(md, vect);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Entrer_note.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_rechercheKeyReleased

    private void ancienItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_ancienItemStateChanged

    }//GEN-LAST:event_ancienItemStateChanged

    private void ancienMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_ancienMouseClicked
        ancien.setSelected(true);
        nouveau.setSelected(false);
        ancien1.setSelected(false);
        etablissement.setEnabled(false);
        moyenne.setEnabled(false);
        matricule.requestFocus();
        kinder.setEnabled(false);
    }//GEN-LAST:event_ancienMouseClicked

    private void nouveauItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_nouveauItemStateChanged

    }//GEN-LAST:event_nouveauItemStateChanged

    private void nouveauMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_nouveauMouseClicked
        ancien.setSelected(false);
        ancien1.setSelected(false);
        nouveau.setSelected(true);
        etablissement.setEnabled(true);
        moyenne.setEnabled(true);
        matricule.requestFocus();
        kinder.setEnabled(true);
        classe1.setEnabled(true);
        rexaminer.setEnabled(true);
    }//GEN-LAST:event_nouveauMouseClicked

    private void nouveauActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nouveauActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_nouveauActionPerformed

    private void ancien1ItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_ancien1ItemStateChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_ancien1ItemStateChanged

    private void ancien1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_ancien1MouseClicked
        ancien.setSelected(false);
        ancien1.setSelected(true);
        nouveau.setSelected(false);
        etablissement.setEnabled(true);
        moyenne.setEnabled(true);
        matricule.requestFocus();
        kinder.setEnabled(true);
        classe1.setEnabled(true);
        rexaminer.setEnabled(true);
    }//GEN-LAST:event_ancien1MouseClicked

    private void matriculeFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_matriculeFocusLost
        try {
            if(acceseleve.getText().equals("Inactif(ve).")){
                JOptionPane.showMessageDialog(this,"Cet élève est inactif(ve) on ne peut pas réaliser l’inscription."
                    + " Pour effectuer cette inscription il(elle) doit être réactivé.");
                matricule.requestFocus();
            }
        } catch (NullPointerException e) {
        }

    }//GEN-LAST:event_matriculeFocusLost

    private void matriculeKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_matriculeKeyReleased
        matricule.setText(matricule.getText().toUpperCase());
        String query="";
        if(matricule.getText().isEmpty()){
            Nettoyer();
            RemplirTable();
        }else{
            if(ancien.isSelected()){
                query="select t.nom,t.prenom,e.estatus from tercero t left join eleve e "
                + "on (t.code = e.code and t.code_tercero = e.code_tercero) where e.matricule ='"+matricule.getText()+"'";
            }else if(nouveau.isSelected() || ancien1.isSelected() ){
                query="select t.nom,t.prenom,e.estatus from tercero t left join eleve e "
                + "on (t.code = e.code and t.code_tercero = e.code_tercero) where e.matricule = '"+matricule.getText()+"'";
            }

            ResultSet rs=Mn.SEARCHDATA(query);
            try {
                while(rs.next()){
                    nomeleve.setText(rs.getString("nom")+" "+rs.getString("prenom"));
                    if(rs.getString("estatus").equals("1")){
                        acceseleve.setText("Actif(ve).");
                        acceseleve.setForeground(Color.GREEN.darker());
                    }else{
                        acceseleve.setText("Inactif(ve).");
                        acceseleve.setForeground(Color.RED.darker());
                    }
                }
            } catch (SQLException ex) {
                Logger.getLogger(Entrer_note.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        String query1 = "select matricule,nom,prenom,classe, moyenne_generale, estatus,superieur \n" +
        "from show_eleve_decision where matricule='"+matricule.getText()+"' and annee_academique = '"+academique1.getText()+"' and institution = '"+Variables.Empresa+"' ";

        // System.out.println("query : "+query);
        ResultSet rs = Mn.SEARCHDATA(query1);

        try {
            md.setRowCount(0);
            while(rs.next()){
                String[] vect={rs.getString("matricule"),rs.getString("nom"),rs.getString("prenom"),
                    rs.getString("classe"),rs.getString("moyenne_generale"),rs.getString("estatus"),rs.getString("superieur")};
                Mn.ADD_TABLE(md, vect);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Entrer_note.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_matriculeKeyReleased

    private void matriculeKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_matriculeKeyTyped
        matricule.requestFocus();
    }//GEN-LAST:event_matriculeKeyTyped

    private void kinderItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_kinderItemStateChanged
        //         if(kinder.isSelected()){
            //               etablissement.setEnabled(false);
            //               classe1.setEnabled(false);
            //               moyenne.setEnabled(false);
            //               rexaminer.setEnabled(false);
            //           }else{
            //               etablissement.setEnabled(true);
            //               classe1.setEnabled(true);
            //               moyenne.setEnabled(true);
            //               rexaminer.setEnabled(true);
            //           }
    }//GEN-LAST:event_kinderItemStateChanged

    private void moyenne1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_moyenne1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_moyenne1ActionPerformed

    private void rexaminerItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_rexaminerItemStateChanged
        if(rexaminer.isSelected()){
            lblmoyenne.setEnabled(true);
            moyenne1.setEnabled(true);
            motif1.setEnabled(true);
            motif.setEnabled(true);
        }else{
            lblmoyenne.setEnabled(false);
            moyenne1.setEnabled(false);
            motif1.setEnabled(false);
            motif.setEnabled(false);
        }
    }//GEN-LAST:event_rexaminerItemStateChanged

    private void motifActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_motifActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_motifActionPerformed

    private void jPanel3MouseDragged(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel3MouseDragged
        date.setText(Mn.DATEHOURNOW());
    }//GEN-LAST:event_jPanel3MouseDragged

    private void jPanel3MouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel3MouseMoved
        date.setText(Mn.DATEHOURNOW());
    }//GEN-LAST:event_jPanel3MouseMoved

    private void jPanel3MouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel3MouseReleased
        date.setText(Mn.DATEHOURNOW());
    }//GEN-LAST:event_jPanel3MouseReleased

    private void tblclasseMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblclasseMouseClicked
        RemplirEleve();
        cantclasse.setText(tblclasse.getValueAt(tblclasse.getSelectedRow(),0).toString());
        cantsection.setText(tblclasse.getValueAt(tblclasse.getSelectedRow(),1).toString());
        controledeleveInscris();
        controledeleveAdmis();
        superieur.setText(cantclasse.getText());
        superieur1.setText(cantclasse.getText());
        //       canteleve1.setText(String.valueOf(Integer.parseInt(canteleve4.getText())-Integer.parseInt(canteleve.getText())));
        canteleve8.setText(String.valueOf(Integer.parseInt(canteleve2.getText())+Integer.parseInt(canteleve.getText())));
        canteleve10.setText(String.valueOf(Integer.parseInt(canteleve1.getText())+Integer.parseInt(canteleve3.getText())));
    }//GEN-LAST:event_tblclasseMouseClicked

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(InscriptionForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(InscriptionForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(InscriptionForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(InscriptionForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new InscriptionForm().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel academique;
    private javax.swing.JLabel academique1;
    private javax.swing.JLabel acceseleve;
    private javax.swing.JRadioButton ancien;
    private javax.swing.JRadioButton ancien1;
    private javax.swing.JMenuItem assignere;
    private javax.swing.JLabel cantclasse;
    private javax.swing.JLabel canteleve;
    private javax.swing.JLabel canteleve1;
    private javax.swing.JLabel canteleve10;
    private javax.swing.JLabel canteleve11;
    private javax.swing.JLabel canteleve2;
    private javax.swing.JLabel canteleve3;
    private javax.swing.JLabel canteleve4;
    private javax.swing.JLabel canteleve5;
    private javax.swing.JLabel canteleve6;
    private javax.swing.JLabel canteleve7;
    private javax.swing.JLabel canteleve8;
    private javax.swing.JLabel canteleve9;
    private javax.swing.JLabel cantsection;
    private javax.swing.JComboBox classe1;
    private javax.swing.JLabel date;
    private org.edisoncor.gui.textField.TextFieldRectBackground etablissement;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel30;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu5;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel11;
    private javax.swing.JPanel jPanel12;
    private javax.swing.JPanel jPanel13;
    private javax.swing.JPanel jPanel14;
    private javax.swing.JPanel jPanel15;
    private javax.swing.JPanel jPanel16;
    private javax.swing.JPanel jPanel17;
    private javax.swing.JPanel jPanel18;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JSeparator jSeparator4;
    private javax.swing.JSeparator jSeparator5;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JCheckBox kinder;
    private javax.swing.JLabel lblmoyenne;
    private javax.swing.JTextField matricule;
    private org.edisoncor.gui.textField.TextFieldRectBackground motif;
    private javax.swing.JLabel motif1;
    private org.edisoncor.gui.textField.TextFieldRectBackground moyenne;
    private org.edisoncor.gui.textField.TextFieldRectBackground moyenne1;
    private javax.swing.JLabel nomeleve;
    private javax.swing.JRadioButton nouveau;
    private org.edisoncor.gui.textField.TextFieldRectBackground recherche;
    private javax.swing.JCheckBox rexaminer;
    private javax.swing.JLabel superieur;
    private javax.swing.JLabel superieur1;
    private javax.swing.JTable tblclasse;
    private javax.swing.JTable tbleleve;
    private javax.swing.JTable tblinscription;
    // End of variables declaration//GEN-END:variables
}
