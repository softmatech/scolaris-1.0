/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Processus;

import Methods.Manipulation;
import Methods.Rapport;
import Methods.Theme;
import Methods.Variables;
import Start.Waittss;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;
import javax.swing.JTable;
import java.awt.Point;
import javax.swing.JOptionPane;


public class BulletinForm extends javax.swing.JFrame {

Manipulation Mn= new Manipulation();
Theme th= new Theme();
Rapport Rp=new Rapport();
String[][] data={};
String[] head={" Matières"," Notes"," Sur"};
String[] headd={" Matricules"};
DefaultTableModel md= new DefaultTableModel(data,head);
DefaultTableModel mdd= new DefaultTableModel(data,headd);

    public BulletinForm() {
        initComponents();
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        this.setTitle("Scolaris: "+Variables.Empresa+" : Préparation des Bulletins");
        tblbulletin.setModel(md);
         matricule.setModel(mdd);
         premiercontrole.setSelected(true);
         Scolaire();
        //---------------------------------------------------------------------
         TableColumnModel cm= tblbulletin.getColumnModel();
        cm.getColumn(0).setPreferredWidth(320);
        cm.getColumn(1).setPreferredWidth(15);
        cm.getColumn(2).setPreferredWidth(15);
        setLocationRelativeTo(this);
    }

    private void Scolaire(){
    String query="select annee from annee_academique where code_annee\n" +
     "in (select max(code_annee) from annee_academique)";
    ResultSet rs=Mn.SEARCHDATA(query);
    try {
        if(rs.first()){
         academique.setText(rs.getString("annee"));
        }
    } catch (SQLException ex) {
        Logger.getLogger(Entrer_note.class.getName()).log(Level.SEVERE, null, ex);
    }
    }
   
   private void Nettoyer(){
    Mn.EMPTYTABLE(tblbulletin);
    nom.setText("Nom de l'élève");
    totalnote.setText("0.0");
    sur.setText("0.0");
    moyenne.setText("0.0");
    qualification.setText("Qualification");
    generale.setText("0.0");
    decision.setText("Décision de fin d'année");
//    mdd.setRowCount(0);
    section.setSelectedItem("Selecctionnez");
    classe.setSelectedItem(null);
    premierplace.setText("Nom de l'élève");
    premiermoyenne.setText("0.0");
    deuxiemeplace.setText("Nom de l'élève");
    deuxiememoyenne.setText("0.0");
   }
   
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jLabel16 = new javax.swing.JLabel();
        totalnote = new javax.swing.JLabel();
        moyenne = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        decision = new javax.swing.JLabel();
        sur = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        qualification = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        premierplace = new javax.swing.JLabel();
        deuxiemeplace = new javax.swing.JLabel();
        troisiemeplace = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        jLabel24 = new javax.swing.JLabel();
        jLabel25 = new javax.swing.JLabel();
        jLabel28 = new javax.swing.JLabel();
        premiermoyenne = new javax.swing.JLabel();
        deuxiememoyenne = new javax.swing.JLabel();
        troisiememoyenne = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        generale = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        academique = new javax.swing.JLabel();
        premiercontrole = new javax.swing.JRadioButton();
        deuxiemecontrole = new javax.swing.JRadioButton();
        troisiemecontrole = new javax.swing.JRadioButton();
        nom = new javax.swing.JLabel();
        jSeparator2 = new javax.swing.JSeparator();
        jPanel4 = new javax.swing.JPanel();
        section = new javax.swing.JComboBox();
        classe = new javax.swing.JComboBox();
        jPanel5 = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        matricule = new javax.swing.JTable(){
            public boolean isCellEditable(int rowIndex, int colIndex){
                return false;
            }
        };
        jPanel6 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblbulletin = new javax.swing.JTable(){
            public boolean isCellEditable(int rowIndex, int colIndex){
                return false;
            }
        };
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        assignere = new javax.swing.JMenuItem();
        jMenu5 = new javax.swing.JMenu();
        jMenuItem1 = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(new java.awt.CardLayout());

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));

        jPanel2.setBackground(new java.awt.Color(87, 148, 210));
        jPanel2.setBorder(new org.edisoncor.gui.util.DropShadowBorder());

        jLabel5.setBackground(new java.awt.Color(255, 255, 255));
        jLabel5.setFont(new java.awt.Font("Lucida Grande", 1, 18)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(255, 255, 255));
        jLabel5.setText("Bulletin");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel5)
                .addContainerGap(884, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(26, Short.MAX_VALUE))
        );

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));
        jPanel3.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel16.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel16.setText("Total Notes: ");
        jLabel16.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        totalnote.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        totalnote.setText("0.0");
        totalnote.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        moyenne.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        moyenne.setText("0.0");
        moyenne.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel17.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel17.setText("Moyenne: ");
        jLabel17.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel11.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel11.setText("Décision:");
        jLabel11.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        decision.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        decision.setForeground(new java.awt.Color(204, 0, 0));
        decision.setText("Décision de fin d'année");
        decision.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        sur.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        sur.setText("0.0");
        sur.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel18.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel18.setText("Sur: ");
        jLabel18.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        qualification.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        qualification.setForeground(new java.awt.Color(0, 102, 51));
        qualification.setText("Qualification");
        qualification.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel12.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel12.setText("1ère Place: ");
        jLabel12.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel13.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel13.setText("2ème Place: ");
        jLabel13.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel14.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel14.setText("3ème Place: ");
        jLabel14.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        premierplace.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        premierplace.setForeground(new java.awt.Color(0, 51, 255));
        premierplace.setText("Nom de l'élève");
        premierplace.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        deuxiemeplace.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        deuxiemeplace.setForeground(new java.awt.Color(0, 51, 255));
        deuxiemeplace.setText("Nom de l'élève");
        deuxiemeplace.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        troisiemeplace.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        troisiemeplace.setForeground(new java.awt.Color(0, 51, 255));
        troisiemeplace.setText("Nom de l'élève");
        troisiemeplace.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel24.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel24.setText("Moyenne: ");
        jLabel24.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel25.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel25.setText("Moyenne: ");
        jLabel25.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel28.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel28.setText("Moyenne: ");
        jLabel28.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        premiermoyenne.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        premiermoyenne.setForeground(new java.awt.Color(204, 0, 0));
        premiermoyenne.setText("0.0");
        premiermoyenne.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        deuxiememoyenne.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        deuxiememoyenne.setForeground(new java.awt.Color(204, 0, 0));
        deuxiememoyenne.setText("0.0");
        deuxiememoyenne.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        troisiememoyenne.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        troisiememoyenne.setForeground(new java.awt.Color(204, 0, 0));
        troisiememoyenne.setText("0.0");
        troisiememoyenne.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel15.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel15.setText("Moyenne Générale:");
        jLabel15.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        generale.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        generale.setForeground(new java.awt.Color(204, 0, 0));
        generale.setText("0.0");
        generale.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addComponent(jLabel16)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(totalnote, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addComponent(jLabel17)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(moyenne, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addComponent(jLabel18)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(sur, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 461, Short.MAX_VALUE)
                                .addComponent(jLabel15, javax.swing.GroupLayout.PREFERRED_SIZE, 122, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(generale, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(108, 108, 108))
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addComponent(qualification, javax.swing.GroupLayout.PREFERRED_SIZE, 104, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 63, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(decision, javax.swing.GroupLayout.PREFERRED_SIZE, 151, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel12)
                            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jLabel14)
                                .addComponent(jLabel13)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(deuxiemeplace, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 223, Short.MAX_VALUE)
                            .addComponent(troisiemeplace, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(premierplace, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addComponent(jLabel24)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(premiermoyenne, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addComponent(jLabel25)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(deuxiememoyenne, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addComponent(jLabel28)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(troisiememoyenne, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
            .addComponent(jSeparator1)
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel15)
                        .addComponent(generale))
                    .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel16)
                        .addComponent(totalnote)
                        .addComponent(jLabel18)
                        .addComponent(sur)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel11)
                        .addComponent(decision))
                    .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel17)
                        .addComponent(moyenne)
                        .addComponent(qualification)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 6, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(2, 2, 2)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel12)
                    .addComponent(premierplace)
                    .addComponent(jLabel24)
                    .addComponent(premiermoyenne))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel13)
                    .addComponent(deuxiemeplace)
                    .addComponent(jLabel25)
                    .addComponent(deuxiememoyenne))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel14)
                    .addComponent(troisiemeplace)
                    .addComponent(jLabel28)
                    .addComponent(troisiememoyenne))
                .addContainerGap())
        );

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel6.setText("Section:");
        jLabel6.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel4.setText("Année Académique: ");
        jLabel4.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        academique.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        academique.setText("0");
        academique.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        premiercontrole.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        premiercontrole.setText("1er Controle");
        premiercontrole.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                premiercontroleMouseClicked(evt);
            }
        });

        deuxiemecontrole.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        deuxiemecontrole.setText("2ème Controle");
        deuxiemecontrole.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                deuxiemecontroleMouseClicked(evt);
            }
        });

        troisiemecontrole.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        troisiemecontrole.setText("3ème Controle");
        troisiemecontrole.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                troisiemecontroleMouseClicked(evt);
            }
        });

        nom.setBackground(new java.awt.Color(255, 255, 255));
        nom.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        nom.setForeground(new java.awt.Color(87, 148, 210));
        nom.setText("Nom de l'élève");
        nom.setOpaque(true);

        jPanel4.setBackground(new java.awt.Color(87, 148, 210));
        jPanel4.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        section.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        section.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Selectionnez", "Petit", "Primaire", "Secondaire" }));
        section.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                sectionItemStateChanged(evt);
            }
        });
        section.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                sectionFocusLost(evt);
            }
        });

        classe.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        classe.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                classeItemStateChanged(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(section, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(classe, javax.swing.GroupLayout.PREFERRED_SIZE, 379, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(79, 79, 79))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(section, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addComponent(classe)
        );

        jPanel5.setBackground(new java.awt.Color(255, 255, 255));
        jPanel5.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        matricule.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        matricule.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                matriculeMousePressed(evt);
            }
        });
        jScrollPane3.setViewportView(matricule);

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 208, Short.MAX_VALUE)
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 343, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        jPanel6.setBackground(new java.awt.Color(255, 255, 255));
        jPanel6.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        tblbulletin.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tblbulletin);

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1)
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(premiercontrole)
                        .addGap(18, 18, 18)
                        .addComponent(deuxiemecontrole)
                        .addGap(18, 18, 18)
                        .addComponent(troisiemecontrole)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(academique, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jSeparator2)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(nom, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel6)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(premiercontrole)
                    .addComponent(deuxiemecontrole)
                    .addComponent(troisiemecontrole)
                    .addComponent(jLabel4)
                    .addComponent(academique))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(nom, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        getContentPane().add(jPanel1, "card2");

        jMenuBar1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N

        jMenu1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/fichier.jpg"))); // NOI18N
        jMenu1.setText("Action");
        jMenu1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N

        assignere.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_E, java.awt.event.InputEvent.CTRL_MASK));
        assignere.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/ButtonFoto/icons8-label_printer_filled.png"))); // NOI18N
        assignere.setText("Assigner");
        assignere.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                assignereActionPerformed(evt);
            }
        });
        jMenu1.add(assignere);

        jMenuBar1.add(jMenu1);

        jMenu5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/editer.jpg"))); // NOI18N
        jMenu5.setText("Edition");
        jMenu5.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N

        jMenuItem1.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F1, 0));
        jMenuItem1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/icons8-help.png"))); // NOI18N
        jMenuItem1.setText("Aide");
        jMenu5.add(jMenuItem1);

        jMenuBar1.add(jMenu5);

        setJMenuBar(jMenuBar1);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void premiercontroleMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_premiercontroleMouseClicked

        premiercontrole.setSelected(true);
        deuxiemecontrole.setSelected(false);
        troisiemecontrole.setSelected(false);
        Nettoyer();
        //---------------------------------------------------------------------------------------------------------
        int aaa=0;
        if(premiercontrole.isSelected()){
            aaa=1;
        }else if(deuxiemecontrole.isSelected()){
            aaa=2;
        }else if(troisiemecontrole.isSelected()){
            aaa=3;
        }
    }//GEN-LAST:event_premiercontroleMouseClicked

    private void deuxiemecontroleMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_deuxiemecontroleMouseClicked
        premiercontrole.setSelected(false);
        deuxiemecontrole.setSelected(true);
        troisiemecontrole.setSelected(false);
        Nettoyer();
        //---------------------------------------------------------------------------------------------------------
        int aaa=0;
        if(premiercontrole.isSelected()){
            aaa=1;
        }else if(deuxiemecontrole.isSelected()){
            aaa=2;
        }else if(troisiemecontrole.isSelected()){
            aaa=3;
        }
    }//GEN-LAST:event_deuxiemecontroleMouseClicked

    private void troisiemecontroleMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_troisiemecontroleMouseClicked
        premiercontrole.setSelected(false);
        deuxiemecontrole.setSelected(false);
        troisiemecontrole.setSelected(true);
        Nettoyer();
        //---------------------------------------------------------------------------------------------------------
        int aaa=0;
        if(premiercontrole.isSelected()){
            aaa=1;
        }else if(deuxiemecontrole.isSelected()){
            aaa=2;
        }else if(troisiemecontrole.isSelected()){
            aaa=3;
        }
    }//GEN-LAST:event_troisiemecontroleMouseClicked

    private void sectionItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_sectionItemStateChanged
        if(section.getSelectedItem().toString().equals("Selectionnez")){
            classe.addItem("Selectionnez");
            classe.setSelectedItem("Selecctionnez");
        }else{
            Mn.FULLCOMBO(classe, "description","section",section.getSelectedItem().toString(), "classe");
            classe.addItem("Selectionnez");
            classe.setSelectedItem("Selectionnez");
        }
    }//GEN-LAST:event_sectionItemStateChanged

    private void sectionFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_sectionFocusLost

    }//GEN-LAST:event_sectionFocusLost

    private void classeItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_classeItemStateChanged
        if(classe.getSelectedItem()==null){
            premierplace.setText("Nom de l'élève");
            premiermoyenne.setText("0.0");
            deuxiemeplace.setText("Nom de l'élève");
            deuxiememoyenne.setText("0.0");
            troisiemeplace.setText("Nom de l'élève");
            troisiememoyenne.setText("0.0");
        }else{
            matricule.setModel(mdd);
            String query="select e.matricule from inscription i left join eleve e on (i.code = e.code and i.code_tercero = e.code_tercero) \n" +
            "left join classe c on c.code_classe=i.code_classe left join tercero t on (t.code = i.code and t.code_tercero = i.code_tercero) "
            + " where c.section='"+section.getSelectedItem().toString()+"' and "
            + " c.description='"+classe.getSelectedItem().toString()+"' and i.annee_academique='"+academique.getText()+"'";
            // System.out.println(query);
            ResultSet rs=Mn.SEARCHDATA(query);
            try {
                mdd.setRowCount(0);
                while(rs.next()){
                    String[] vect={rs.getString("matricule")};
                    Mn.ADD_TABLE(mdd, vect);
                }
            } catch (SQLException ex) {
                Logger.getLogger(Entrer_note.class.getName()).log(Level.SEVERE, null, ex);
            }
            if(matricule.getRowCount()==0){
                nom.setText("Nom de l'élève");
            }
            int aaa=0;
            if(premiercontrole.isSelected()){
                aaa=1;
            }else if(deuxiemecontrole.isSelected()){
                aaa=2;
            }else if(troisiemecontrole.isSelected()){
                aaa=3;
            }
            int aa=Mn.SEARCHCODE("code_classe","description","section",classe.getSelectedItem().toString(),section.getSelectedItem().toString(), "classe");
            String querys="select t.nom,t.prenom,n.moyenne from note n left join eleve e on e.matricule=n.matricule\n" +
            "left join tercero t on (t.code = e.code and t.code_tercero = e.code_tercero) where n.code_classe="+aa+" and n.controle="+aaa+"\n" +
            "and n.annee='"+academique.getText()+"' order by moyenne desc limit 3";
            ResultSet rss=Mn.SEARCHDATA(querys);
            try {
                for(int i=0; rss.next();i++){
                    if(i==0){
                        premierplace.setText(rss.getString("nom")+" "+rss.getString("prenom"));
                        premiermoyenne.setText(rss.getString("moyenne"));
                    }else if(i==1){
                        deuxiemeplace.setText(rss.getString("nom")+" "+rss.getString("prenom"));
                        deuxiememoyenne.setText(rss.getString("moyenne"));
                    }else{
                        troisiemeplace.setText(rss.getString("nom")+" "+rss.getString("prenom"));
                        troisiememoyenne.setText(rss.getString("moyenne"));
                    }
                }
            } catch (SQLException ex) {
                Logger.getLogger(Entrer_note.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }//GEN-LAST:event_classeItemStateChanged

    private void matriculeMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_matriculeMousePressed
        JTable table =(JTable) evt.getSource();
        Point pt = evt.getPoint();
        int row = table.rowAtPoint(pt);
        if (evt.getClickCount() == 2) {
            if(matricule.getRowCount()==0){
                Nettoyer();
            }else{
                String querys="select t.nom,t.prenom from tercero t left join eleve e "
                + "on (t.code = e.code and t.code_tercero = e.code_tercero) where e.matricule like '%"+matricule.getValueAt(matricule.getSelectedRow(), 0).toString()+"%'";
                ResultSet rss=Mn.SEARCHDATA(querys);
                try {
                    while(rss.next()){
                        nom.setText(rss.getString("nom")+" "+rss.getString("prenom"));
                    }
                } catch (SQLException ex) {
                    Logger.getLogger(Entrer_note.class.getName()).log(Level.SEVERE, null, ex);
                }
                //---------------------------------------------------------------------------------------------------------
                int aaa=0;
                if(premiercontrole.isSelected()){
                    aaa=1;
                }else if(deuxiemecontrole.isSelected()){
                    aaa=2;
                }else if(troisiemecontrole.isSelected()){
                    aaa=3;
                }
                int aa=Mn.SEARCHCODE("code_classe","description","section",classe.getSelectedItem().toString(),section.getSelectedItem().toString(), "classe");
                String query="select e.matricule,t.nom,t.prenom,c.description as classe,c.section,dn.matiere,dn.note,dn.sur,\n" +
                "n.controle,n.total_note as total,n.total_sur as points,n.moyenne,n.status as qualification,n.annee,\n" +
                "d.moyenne_generale as generale,d.estatus as decision from note n\n" +
                "left join detaille_note dn on dn.matricule=n.matricule left join eleve e on e.matricule=n.matricule \n" +
                "left join tercero t on (t.code = e.code and t.code_tercero = e.code_tercero) left join decision d on d.matricule=e.matricule \n" +
                "left join  classe c on c.code_classe=n.code_classe where n.matricule like '%"+matricule.getValueAt(matricule.getSelectedRow(), 0).toString()+"%' and  n.code_classe="+aa+" "
                + "and n.annee='"+academique.getText()+"' and n.controle="+aaa+"  group by matiere";
                //System.out.println(query);
                ResultSet rs=Mn.SEARCHDATA(query);
                try {
                    md.setRowCount(0);
                    while(rs.next()){
                        String[] vect={rs.getString("matiere"),rs.getString("note"),rs.getString("sur")};
                        Mn.ADD_TABLE(md, vect);
                        totalnote.setText(rs.getString("total"));
                        sur.setText(rs.getString("points"));
                        moyenne.setText(rs.getString("moyenne"));
                        qualification.setText(rs.getString("qualification"));
                        if(troisiemecontrole.isSelected()){
                            generale.setText(rs.getString("generale"));
                        }else{
                            generale.setText("0.0");
                        }
                        decision.setText(rs.getString("decision"));
                    }
                } catch (SQLException ex) {
                    Logger.getLogger(Entrer_note.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }//GEN-LAST:event_matriculeMousePressed

    private void assignereActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_assignereActionPerformed
     int aaa=0;
        if(matricule.getSelectedRow()==-1){
           int mesaj=JOptionPane.showConfirmDialog(null,"Vous n’avez sélectionné aucune matricule de la table de gauche."
         + " Désirez-vous imprimer bulletin pour toute la classe sélectionnez ?",
                "Avertissement", JOptionPane.YES_OPTION, JOptionPane.WARNING_MESSAGE);
            if(mesaj==JOptionPane.YES_OPTION){
                  if(premiercontrole.isSelected()){
             aaa=1;
            }else if(deuxiemecontrole.isSelected()){
             aaa=2;
            }else if(troisiemecontrole.isSelected()){
             aaa=3;
            }
            for(int i=0;i<matricule.getRowCount();i++){
                Waittss attend= new Waittss(null, rootPaneCheckingEnabled);
                attend.show();
              Rp.SENDQUERY("Bulletin",matricule.getValueAt(i, 0).toString(),academique.getText(),aaa,Variables.Empresa);  
               Nettoyer();
            }
            }else{
              JOptionPane.showMessageDialog(null,"S'il vous plait veuillez sélectionner une matricule dan la table de droite.");   
            }
        }else{
            if(premiercontrole.isSelected()){
            aaa=1;
            }else if(deuxiemecontrole.isSelected()){
            aaa=2;
            }else if(troisiemecontrole.isSelected()){
            aaa=3;
            }
            Waittss attend= new Waittss(null, rootPaneCheckingEnabled);
            attend.show();
             Rp.SENDQUERY("Bulletin",matricule.getValueAt(matricule.getSelectedRow(), 0).toString(),academique.getText(),aaa,Variables.Empresa);
          Nettoyer();
        }        
    }//GEN-LAST:event_assignereActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(BulletinForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(BulletinForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(BulletinForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(BulletinForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new BulletinForm().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel academique;
    private javax.swing.JMenuItem assignere;
    private javax.swing.JComboBox classe;
    private javax.swing.JLabel decision;
    private javax.swing.JRadioButton deuxiemecontrole;
    private javax.swing.JLabel deuxiememoyenne;
    private javax.swing.JLabel deuxiemeplace;
    private javax.swing.JLabel generale;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu5;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JTable matricule;
    private javax.swing.JLabel moyenne;
    private javax.swing.JLabel nom;
    private javax.swing.JRadioButton premiercontrole;
    private javax.swing.JLabel premiermoyenne;
    private javax.swing.JLabel premierplace;
    private javax.swing.JLabel qualification;
    private javax.swing.JComboBox section;
    private javax.swing.JLabel sur;
    private javax.swing.JTable tblbulletin;
    private javax.swing.JLabel totalnote;
    private javax.swing.JRadioButton troisiemecontrole;
    private javax.swing.JLabel troisiememoyenne;
    private javax.swing.JLabel troisiemeplace;
    // End of variables declaration//GEN-END:variables
}
