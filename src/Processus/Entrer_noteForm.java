/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Processus;

import Methods.Decision;
import Methods.Manipulation;
import Methods.Theme;
import Methods.Variables;
import java.awt.Color;
import java.awt.Component;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumnModel;
import javax.swing.JOptionPane;

/**
 *
 * @author josephandyfeidje
 */
public class Entrer_noteForm extends javax.swing.JFrame {

    Manipulation Mn= new Manipulation();
Decision Ds= new Decision();
Theme th= new Theme();
String[][] data={};
String[] head={" Matières"," Notes"," Sur"};
String[] heads={" Matricule"," Nom"," Prénom"," Classe"," Controle"," Moyenne"};
String[] headss={" Matricules"};
DefaultTableModel md= new DefaultTableModel(data,head);
DefaultTableModel mds= new DefaultTableModel(data,heads);
DefaultTableModel mdss= new DefaultTableModel(data,headss);

TableCellRenderer render= new TableCellRenderer() {
 public Component getTableCellRendererComponent(JTable table,Object value,boolean idSelected,boolean hasFocus,int row,int column){
     JLabel lbl= new JLabel(value==null? "": value.toString());
     if(column==0 && row>=0){
         lbl.setHorizontalAlignment(SwingConstants.LEFT);
         lbl.setOpaque(true);
         lbl.setBackground(Color.green);
     }
if(column==5 && row>=0){
         lbl.setHorizontalAlignment(SwingConstants.LEFT);
         lbl.setOpaque(true);
         lbl.setBackground(Color.yellow);
     }
     return lbl;
 }
};


    public Entrer_noteForm() {
        initComponents();
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        this.setTitle("Scolaris: "+Variables.Empresa+" : Entrer des Notes");
         tblmoyenne.setModel(mds);
         RemplirMoyenne();
         tblnotes.setModel(md);
         tblmatricule.setModel(mdss);
         controle.setText(String.valueOf(Variables.trimestre));
         prcontrole.setSelected(true);
         Scolaire();
         Mn.FULLCOMBO(classe1, "description", "classe");
         Mn.FULLCOMBO(classe, "description","section",section.getSelectedItem().toString(), "classe");
         Nettoyer();
          
       //---------------------------------------------------------------------
         TableColumnModel cm= tblnotes.getColumnModel();
         TableColumnModel cmy= tblmoyenne.getColumnModel();
        cm.getColumn(0).setPreferredWidth(320);
        cm.getColumn(1).setPreferredWidth(15);
        cm.getColumn(2).setPreferredWidth(15);
        cmy.getColumn(2).setPreferredWidth(70);
        cmy.getColumn(1).setPreferredWidth(70);
        cmy.getColumn(0).setPreferredWidth(30);
        cmy.getColumn(3).setPreferredWidth(10);
        cmy.getColumn(4).setPreferredWidth(10);
        cmy.getColumn(5).setPreferredWidth(10);
        tblmoyenne.getColumnModel().getColumn(0).setCellRenderer(render);
        tblmoyenne.getColumnModel().getColumn(5).setCellRenderer(render);
        mdss.setRowCount(0);
        setLocationRelativeTo(this);
    }

    private void Nettoyer(){
    classe.setSelectedItem("Selectionnez");
    matiere.setSelectedItem("Selectionnez");
    classe1.setSelectedItem("Selectionnez");
    note.setEnabled(false);
    note.setText(null);
    sur.setText(null);
    Mn.EMPTYTABLE(tblnotes);
    }
    
    private void Scolaire(){
    String query="select annee from annee_academique where code_annee\n" +
     "in (select max(code_annee) from annee_academique)";
    ResultSet rs=Mn.SEARCHDATA(query);
    try {
        if(rs.first()){
         academique.setText(rs.getString("annee"));
        academique1.setText(rs.getString("annee"));
        }
    } catch (SQLException ex) {
        Logger.getLogger(Entrer_note.class.getName()).log(Level.SEVERE, null, ex);
    }
    }

//------------------------------------------------------------------------------------
private void InsertNote(){
    String xx="";
    int aas=0;
    int aaa=0;
    String xs="";
  String aa=Mn.SEARCHCODE("code_classe","description",classe.getSelectedItem().toString(), "classe");
  String query = "select e.code,e.code_tercero from eleve e left join tercero t on  (t.code = e.code and t.code_tercero = e.code_tercero) "
          + "join eleve_has_institution ehi on (e.code = ehi.code and e.code_tercero = ehi.code_tercero) "
          + "join institution i on(ehi.idinstitution = i.idinstitution) "
          + " where e.matricule='"+tblmatricule.getValueAt(tblmatricule.getSelectedRow(),0).toString()+"' and i.nom = '"+Variables.Empresa+"'";
    ResultSet rs = Mn.Rezulta(query);
    try {
        if(rs.last()){
            aas = rs.getInt("code");
            xs = rs.getString("code_tercero");
        }
    } catch (SQLException ex) {
        Logger.getLogger(Inscription.class.getName()).log(Level.SEVERE, null, ex);
    }
//  int aaa=Mn.SEARCHCODES("code_tercero","matricule",tblmatricule.getValueAt(tblmatricule.getSelectedRow(),0).toString(),"eleve");
  String ann=academique.getText();
  String mat=tblmatricule.getValueAt(tblmatricule.getSelectedRow(),0).toString();
  String totaln=noteeleve.getText();
  String totals=surnote.getText();
  double diviseur=Double.parseDouble(surnote.getText())/10;
  double dividende=Double.parseDouble(noteeleve.getText());
  double quotient= dividende /diviseur;
  String quot=quotient+"";
  BigDecimal big= new BigDecimal(quot);
  big=big.setScale(2, RoundingMode.HALF_UP);
  String biggg=big.toString();
  double bigg=big.doubleValue();
  String stat="";
  String rang="";
  double moy=0.0;
  //----------------------------------------------------------
  String sql="select * from qualification where institution='"+Variables.Empresa+"'";
  ResultSet ds= Mn.SEARCHDATA(sql);
       try {
           if(ds.first()){
           moy=ds.getDouble("moyenne_aceptable");
           }
       }catch (SQLException ex) {
           Logger.getLogger(Inscription.class.getName()).log(Level.SEVERE, null, ex);
       }

  if(bigg>=9.00){
      stat="Excéllent(te).";
  }else if(bigg<9.00 && bigg>=8.00){
      stat="Très Bien.";
  }else if(bigg<8.00 && bigg>6.50){
      stat="Assez Bien.";
  }else if(bigg==moy){
      stat="Bien.";
  }else if(bigg<moy){
      stat="Mal.";
  }

//-------------------------------------------------------------------------------------
  query="call Insert_note('"+mat+"',"+aa+",'"+totaln+"','"+totals+"','"+biggg+"','"+stat+"',"+controle.getText()+",'"+ann+"')";
  Mn.MANIPULEDB(query);
  
  if(controle.getText().equals("3")){
  int cant=0;
  double moyy=0.0;
  String ssql="select count(moyenne) as cant,sum(moyenne) as generale from note where matricule='"+mat+"' and annee='"+ann+"' ";
  ResultSet sds= Mn.SEARCHDATA(ssql);
       try {
            if(sds.first()){
            cant=sds.getInt("cant");
            moyy=sds.getDouble("generale");
            }
       } catch (SQLException ex) {
           Logger.getLogger(Inscription.class.getName()).log(Level.SEVERE, null, ex);
       }
  double fff=moyy/cant;
  BigDecimal biig= new BigDecimal(fff);
  biig=biig.setScale(2, RoundingMode.HALF_UP);
  String biiig=biig.toString();
  double ff=biig.doubleValue();
  String estatuses="";
  if(xx.equals("9ème Sec.")){
             if(ff<5.00){
             estatuses="Redoublé(e)";
             xx=classe.getSelectedItem().toString();
             }else if(ff>=5.00){
              estatuses="Admis(e)";
              xx=Ds.ClasseSuperieur(section.getSelectedItem().toString(),classe.getSelectedItem().toString());
            }
     }else{
         if(ff<moy){
             estatuses="Redoublé(e)";
             xx=classe.getSelectedItem().toString();
             }else if(ff>=moy){
              estatuses="Admis(e)";
              xx=Ds.ClasseSuperieur(section.getSelectedItem().toString(),classe.getSelectedItem().toString());
           }
  }
  String query1="call Insert_decision("+aaa+",'"+mat+"',"+aa+",'"+ann+"','"+Variables.Empresa+"','"+biggg+"','"+biiig+"','"+estatuses+"','"+xx+"')";
  Mn.SAVE_BUTTON(query1);
  }
}

private void InsertDetaille(){
    String mat=tblmatricule.getValueAt(tblmatricule.getSelectedRow(),0).toString();
    String cont=controle.getText();
    String ann=academique.getText();
    
   for(int i=0;i<tblnotes.getRowCount();i++){
     String aa=tblnotes.getValueAt(i, 0).toString();
     int bb=Integer.parseInt(tblnotes.getValueAt(i, 1).toString());
     int cc=Integer.parseInt(tblnotes.getValueAt(i, 2).toString());
     String query="call Insert_detaillenote('"+mat+"','"+aa+"','"+bb+"','"+cc+"','"+cont+"','"+ann+"','"+Variables.Empresa+"')";
     Mn.MANIPULEDB(query);
   }
}

private void SumNote(){
 int sumnote=0; double lol=0.0;
for(int i=0;i<tblnotes.getRowCount();i++){
  int aa=Integer.parseInt(tblnotes.getValueAt(i, 1).toString());
  int bb=Integer.parseInt(tblnotes.getValueAt(i, 2).toString());
  sumnote=sumnote+aa;
}
totalmatiere.setText(String.valueOf(tblnotes.getRowCount()));
noteeleve.setText(String.valueOf(sumnote));
}

private void online(){
 String mat=tblmatricule.getValueAt(tblmatricule.getSelectedRow(),0).toString();
 String cont=controle.getText();
 String ann=academique.getText();
 String con1="";String con2=""; String con3="";
 for(int i=0;i<tblnotes.getRowCount();i++){
  String aa=tblnotes.getValueAt(i, 0).toString();
  if(cont.equals("1")){
    con1=tblnotes.getValueAt(i, 1).toString(); 
 }else if(cont.equals("2")){
    con2=tblnotes.getValueAt(i, 1).toString(); 
    String sql="select controle1 from temp where matiere='"+aa+"' and annee='"+ann+"' and matricule='"+mat+"' and institution='"+Variables.Empresa+"'";
    ResultSet rs=Mn.SEARCHDATA(sql);
      try {
          if(rs.first()){
           con1=rs.getString("controle1");
          } 
      } catch (SQLException ex) {
          Logger.getLogger(Entrer_note.class.getName()).log(Level.SEVERE, null, ex);
      }
 }else if(cont.equals("3")){
    con3=tblnotes.getValueAt(i, 1).toString(); 
    String sql="select controle1,controle2 from temp where matiere='"+aa+"' and annee='"+ann+"' and matricule='"+mat+"' and institution='"+Variables.Empresa+"'";
    ResultSet rs=Mn.SEARCHDATA(sql);
      try {
          if(rs.first()){
           con1=rs.getString("controle1");
           con2=rs.getString("controle2");
          } 
      } catch (SQLException ex) {
          Logger.getLogger(Entrer_note.class.getName()).log(Level.SEVERE, null, ex);
      }
 }
  int cc=Integer.parseInt(tblnotes.getValueAt(i, 2).toString());
  String query="call Insert_temp('"+mat+"','"+ann+"','"+aa+"','"+cc+"','"+con1+"','"+con2+"','"+con3+"','"+Variables.Empresa+"')";
  Mn.MANIPULEDB(query);
}   
}
//-------------------------------------------------------------------------------------

private void RemplirMoyenne(){
    int aa=0;
    if(prcontrole.isSelected()){
     aa=1;
    }else if(decontrole.isSelected()){
     aa=2;
    }else if(trcontrole.isSelected()){
     aa=3;
    }
    String query = "select  e.matricule,t.nom,t.prenom,c.description as classse,n.controle,n.moyenne from eleve e left join tercero t \n" +
   "on (t.code = e.code and t.code_tercero = e.code_tercero) left join note n on e.matricule = n.matricule\n" +
   "left join classe c on c.code_classe = n.code_classe "
   + "where  n.annee = '"+academique1.getText()+"'  order by matricule asc ";
    Mn.FULLTABLE(query, tblmoyenne, mds);
}

// a modifye
private void RemplirPlas(){
    if(tblmoyenne.getRowCount()==0){
    premier.setText("Nom de l'élève");
    prclasse.setText("Année");
    prmoy.setText("0.0");
    deuxieme.setText("Nom de l'élève");
    declasse.setText("Année");
    demoy.setText("0.0");
    troisieme.setText("Nom de l'élève"); 
    trclasse.setText("Année");
    trmoy.setText("0.0");
    }else if(tblmoyenne.getRowCount()==1){
    premier.setText(tblmoyenne.getValueAt(0,1)+" "+tblmoyenne.getValueAt(0,2));
    prclasse.setText(tblmoyenne.getValueAt(0,3).toString());
    prmoy.setText(tblmoyenne.getValueAt(0,5).toString()); 
    }else if(tblmoyenne.getRowCount()==2){
    premier.setText(tblmoyenne.getValueAt(0,1)+" "+tblmoyenne.getValueAt(0,2));
    prclasse.setText(tblmoyenne.getValueAt(0,3).toString());
    prmoy.setText(tblmoyenne.getValueAt(0,5).toString());
    deuxieme.setText(tblmoyenne.getValueAt(1,1)+" "+tblmoyenne.getValueAt(1,2));
    declasse.setText(tblmoyenne.getValueAt(1,3).toString());
    demoy.setText(tblmoyenne.getValueAt(1,5).toString());
    }else{
    premier.setText(tblmoyenne.getValueAt(0,1)+" "+tblmoyenne.getValueAt(0,2));
    prclasse.setText(tblmoyenne.getValueAt(0,3).toString());
    prmoy.setText(tblmoyenne.getValueAt(0,5).toString());
    deuxieme.setText(tblmoyenne.getValueAt(1,1)+" "+tblmoyenne.getValueAt(1,2));
    declasse.setText(tblmoyenne.getValueAt(1,3).toString());
    demoy.setText(tblmoyenne.getValueAt(1,5).toString());
    troisieme.setText(tblmoyenne.getValueAt(2,1)+" "+tblmoyenne.getValueAt(2,2)); 
    trclasse.setText(tblmoyenne.getValueAt(2,3).toString());
    trmoy.setText(tblmoyenne.getValueAt(2,5).toString());
    }
 
}
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        jTabbedPane5 = new javax.swing.JTabbedPane();
        jPanel3 = new javax.swing.JPanel();
        jPanel4 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        academique = new javax.swing.JLabel();
        jPanel5 = new javax.swing.JPanel();
        jLabel22 = new javax.swing.JLabel();
        controle = new javax.swing.JTextField();
        jSeparator1 = new javax.swing.JSeparator();
        jPanel7 = new javax.swing.JPanel();
        jScrollPane5 = new javax.swing.JScrollPane();
        tblmatricule = new javax.swing.JTable(){
            public boolean isCellEditable(int rowIndex, int colIndex){
                return false;
            }
        };
        jPanel6 = new javax.swing.JPanel();
        jLabel23 = new javax.swing.JLabel();
        nom = new javax.swing.JLabel();
        jPanel8 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jLabel32 = new javax.swing.JLabel();
        jPanel9 = new javax.swing.JPanel();
        section = new javax.swing.JComboBox();
        classe = new javax.swing.JComboBox();
        matiere = new javax.swing.JComboBox();
        jLabel33 = new javax.swing.JLabel();
        note = new org.edisoncor.gui.textField.TextFieldRectBackground();
        jLabel34 = new javax.swing.JLabel();
        sur = new javax.swing.JLabel();
        add = new javax.swing.JButton();
        rem = new javax.swing.JButton();
        jPanel10 = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        tblnotes = new javax.swing.JTable(){
            public boolean isCellEditable(int rowIndex, int colIndex){
                return false;
            }
        };
        jPanel11 = new javax.swing.JPanel();
        jLabel30 = new javax.swing.JLabel();
        jLabel31 = new javax.swing.JLabel();
        jPanel12 = new javax.swing.JPanel();
        totalmatiere = new javax.swing.JLabel();
        noteeleve = new javax.swing.JLabel();
        jPanel13 = new javax.swing.JPanel();
        jLabel36 = new javax.swing.JLabel();
        jLabel35 = new javax.swing.JLabel();
        jPanel14 = new javax.swing.JPanel();
        surmatiere = new javax.swing.JLabel();
        surnote = new javax.swing.JLabel();
        jPanel15 = new javax.swing.JPanel();
        jPanel16 = new javax.swing.JPanel();
        jLabel10 = new javax.swing.JLabel();
        troisieme = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        trmoy = new javax.swing.JLabel();
        jLabel38 = new javax.swing.JLabel();
        trclasse = new javax.swing.JLabel();
        jPanel17 = new javax.swing.JPanel();
        jLabel8 = new javax.swing.JLabel();
        premier = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        prclasse = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        prmoy = new javax.swing.JLabel();
        jPanel18 = new javax.swing.JPanel();
        jLabel9 = new javax.swing.JLabel();
        deuxieme = new javax.swing.JLabel();
        jLabel21 = new javax.swing.JLabel();
        declasse = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        demoy = new javax.swing.JLabel();
        jPanel19 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tblmoyenne = new javax.swing.JTable(){
            public boolean isCellEditable(int rowIndex, int colIndex){
                return false;
            }
        };
        jPanel20 = new javax.swing.JPanel();
        prcontrole = new javax.swing.JRadioButton();
        decontrole = new javax.swing.JRadioButton();
        trcontrole = new javax.swing.JRadioButton();
        jPanel21 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        academique1 = new javax.swing.JLabel();
        jPanel22 = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel24 = new javax.swing.JLabel();
        jPanel23 = new javax.swing.JPanel();
        classe1 = new javax.swing.JComboBox();
        nom1 = new javax.swing.JLabel();
        matricule1 = new javax.swing.JTextField();
        jMenuBar3 = new javax.swing.JMenuBar();
        jMenu5 = new javax.swing.JMenu();
        enregistrer = new javax.swing.JMenuItem();
        jMenu6 = new javax.swing.JMenu();
        jMenuItem14 = new javax.swing.JMenuItem();
        jMenuItem15 = new javax.swing.JMenuItem();
        jMenuItem16 = new javax.swing.JMenuItem();
        jMenuItem17 = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(new java.awt.CardLayout());

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));

        jPanel2.setBackground(new java.awt.Color(87, 148, 210));
        jPanel2.setBorder(new org.edisoncor.gui.util.DropShadowBorder());

        jLabel5.setBackground(new java.awt.Color(255, 255, 255));
        jLabel5.setFont(new java.awt.Font("Lucida Grande", 1, 18)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(255, 255, 255));
        jLabel5.setText("Registre de Note(s)");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel5)
                .addContainerGap(807, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(26, Short.MAX_VALUE))
        );

        jTabbedPane5.setBackground(new java.awt.Color(255, 255, 255));
        jTabbedPane5.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jTabbedPane5.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTabbedPane5MouseClicked(evt);
            }
        });

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));

        jPanel4.setBackground(new java.awt.Color(87, 148, 210));
        jPanel4.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("Année Académique: ");

        academique.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        academique.setForeground(new java.awt.Color(255, 255, 255));
        academique.setText("0");

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(academique, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(12, Short.MAX_VALUE))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(academique, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        jPanel5.setBackground(new java.awt.Color(255, 255, 255));

        jLabel22.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel22.setText("Controle:");

        controle.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                controleActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel22)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(controle, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(jLabel22)
                .addComponent(controle, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        jPanel7.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        tblmatricule.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tblmatricule.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                tblmatriculeMousePressed(evt);
            }
        });
        jScrollPane5.setViewportView(tblmatricule);

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane5, javax.swing.GroupLayout.DEFAULT_SIZE, 228, Short.MAX_VALUE)
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane5, javax.swing.GroupLayout.DEFAULT_SIZE, 373, Short.MAX_VALUE)
        );

        jPanel6.setBackground(new java.awt.Color(87, 148, 210));
        jPanel6.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel23.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel23.setForeground(new java.awt.Color(255, 255, 255));
        jLabel23.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel23.setText("Elève:");

        nom.setBackground(new java.awt.Color(87, 148, 210));
        nom.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        nom.setForeground(new java.awt.Color(255, 255, 255));
        nom.setText("Nom de l'élève");
        nom.setOpaque(true);

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel23, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 44, Short.MAX_VALUE)
                .addComponent(nom, javax.swing.GroupLayout.PREFERRED_SIZE, 637, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel23, javax.swing.GroupLayout.DEFAULT_SIZE, 27, Short.MAX_VALUE)
            .addComponent(nom, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        jPanel8.setBackground(new java.awt.Color(255, 255, 255));
        jPanel8.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel2.setText("Section:");

        jLabel32.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel32.setText("Matière: ");

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel8Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2)
                    .addComponent(jLabel32, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );
        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 26, Short.MAX_VALUE)
                .addComponent(jLabel32)
                .addGap(20, 20, 20))
        );

        jPanel9.setBackground(new java.awt.Color(255, 255, 255));
        jPanel9.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        section.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        section.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Selectionnez", "Petit", "Primaire", "Secondaire" }));
        section.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                sectionItemStateChanged(evt);
            }
        });
        section.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                sectionFocusLost(evt);
            }
        });

        classe.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        classe.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                classeItemStateChanged(evt);
            }
        });
        classe.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                classeFocusGained(evt);
            }
        });

        matiere.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        matiere.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                matiereFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                matiereFocusLost(evt);
            }
        });

        jLabel33.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel33.setText("Note ");

        note.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                noteFocusLost(evt);
            }
        });
        note.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                noteKeyReleased(evt);
            }
        });

        jLabel34.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel34.setText("Sur: ");

        sur.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        sur.setText("0");

        add.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/icons8-circled_down.png"))); // NOI18N
        add.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addActionPerformed(evt);
            }
        });

        rem.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/icons8-up_circular_1.png"))); // NOI18N
        rem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                remActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel9Layout = new javax.swing.GroupLayout(jPanel9);
        jPanel9.setLayout(jPanel9Layout);
        jPanel9Layout.setHorizontalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(section, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(matiere, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel9Layout.createSequentialGroup()
                        .addComponent(jLabel33)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(note, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel34)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(sur, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(add, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(rem, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(classe, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel9Layout.setVerticalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(section)
                    .addComponent(classe))
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel9Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 23, Short.MAX_VALUE)
                        .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel33)
                                .addComponent(note, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel34, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(sur, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel9Layout.createSequentialGroup()
                                .addComponent(matiere, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE))))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel9Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 21, Short.MAX_VALUE)
                        .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(add, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(rem, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap())
        );

        jPanel10.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        tblnotes.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane3.setViewportView(tblnotes);

        javax.swing.GroupLayout jPanel10Layout = new javax.swing.GroupLayout(jPanel10);
        jPanel10.setLayout(jPanel10Layout);
        jPanel10Layout.setHorizontalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane3, javax.swing.GroupLayout.Alignment.TRAILING)
        );
        jPanel10Layout.setVerticalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
        );

        jPanel11.setBackground(new java.awt.Color(87, 148, 210));
        jPanel11.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel30.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel30.setForeground(new java.awt.Color(255, 255, 255));
        jLabel30.setText("Total Matière: ");

        jLabel31.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel31.setForeground(new java.awt.Color(255, 255, 255));
        jLabel31.setText("Total Note de l'élève: ");

        javax.swing.GroupLayout jPanel11Layout = new javax.swing.GroupLayout(jPanel11);
        jPanel11.setLayout(jPanel11Layout);
        jPanel11Layout.setHorizontalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel11Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(jLabel30)
                    .addComponent(jLabel31, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );
        jPanel11Layout.setVerticalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel11Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel30)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel31)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel12.setBackground(new java.awt.Color(87, 148, 210));
        jPanel12.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        totalmatiere.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        totalmatiere.setForeground(new java.awt.Color(255, 255, 255));
        totalmatiere.setText("0");

        noteeleve.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        noteeleve.setForeground(new java.awt.Color(255, 255, 255));
        noteeleve.setText("0");

        javax.swing.GroupLayout jPanel12Layout = new javax.swing.GroupLayout(jPanel12);
        jPanel12.setLayout(jPanel12Layout);
        jPanel12Layout.setHorizontalGroup(
            jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel12Layout.createSequentialGroup()
                .addContainerGap(11, Short.MAX_VALUE)
                .addGroup(jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(totalmatiere, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(noteeleve, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );
        jPanel12Layout.setVerticalGroup(
            jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel12Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(totalmatiere)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(noteeleve)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel13.setBackground(new java.awt.Color(87, 148, 210));
        jPanel13.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel36.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel36.setForeground(new java.awt.Color(255, 255, 255));
        jLabel36.setText("Sur: ");

        jLabel35.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel35.setForeground(new java.awt.Color(255, 255, 255));
        jLabel35.setText("Sur: ");

        javax.swing.GroupLayout jPanel13Layout = new javax.swing.GroupLayout(jPanel13);
        jPanel13.setLayout(jPanel13Layout);
        jPanel13Layout.setHorizontalGroup(
            jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel13Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel36)
                    .addComponent(jLabel35)))
        );
        jPanel13Layout.setVerticalGroup(
            jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel13Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel36)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel35)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel14.setBackground(new java.awt.Color(87, 148, 210));
        jPanel14.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        surmatiere.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        surmatiere.setForeground(new java.awt.Color(255, 255, 255));
        surmatiere.setText("0");

        surnote.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        surnote.setForeground(new java.awt.Color(255, 255, 255));
        surnote.setText("0");

        javax.swing.GroupLayout jPanel14Layout = new javax.swing.GroupLayout(jPanel14);
        jPanel14.setLayout(jPanel14Layout);
        jPanel14Layout.setHorizontalGroup(
            jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel14Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(surmatiere, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(surnote, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(11, Short.MAX_VALUE))
        );
        jPanel14Layout.setVerticalGroup(
            jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel14Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(surmatiere)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(surnote)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addComponent(jSeparator1)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel10, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(jPanel11, javax.swing.GroupLayout.PREFERRED_SIZE, 132, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel13, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel14, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(jPanel5, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(9, 9, 9)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jPanel9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel10, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jPanel11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jPanel13, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jPanel14, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jPanel12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addContainerGap())
                    .addComponent(jPanel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );

        jTabbedPane5.addTab("ENTRER DE NOTES", jPanel3);

        jPanel16.setBackground(new java.awt.Color(87, 148, 210));
        jPanel16.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel10.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel10.setForeground(new java.awt.Color(255, 255, 255));
        jLabel10.setText("3 ème Place:  ");

        troisieme.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        troisieme.setForeground(new java.awt.Color(255, 255, 255));
        troisieme.setText("Nom de l'élève");

        jLabel18.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel18.setForeground(new java.awt.Color(255, 255, 255));
        jLabel18.setText("Moyenne: ");

        trmoy.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        trmoy.setForeground(new java.awt.Color(255, 255, 255));
        trmoy.setText("0.0");

        jLabel38.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel38.setForeground(new java.awt.Color(255, 255, 255));
        jLabel38.setText("Classe: ");

        trclasse.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        trclasse.setForeground(new java.awt.Color(255, 255, 255));
        trclasse.setText("Année");

        javax.swing.GroupLayout jPanel16Layout = new javax.swing.GroupLayout(jPanel16);
        jPanel16.setLayout(jPanel16Layout);
        jPanel16Layout.setHorizontalGroup(
            jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel16Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel10)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(troisieme, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel38)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(trclasse)
                .addGap(142, 142, 142)
                .addComponent(jLabel18)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(trmoy)
                .addGap(89, 89, 89))
        );
        jPanel16Layout.setVerticalGroup(
            jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(jLabel18)
                .addComponent(trmoy))
            .addGroup(jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(jLabel38)
                .addComponent(trclasse))
            .addGroup(jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(jLabel10)
                .addComponent(troisieme))
        );

        jPanel17.setBackground(new java.awt.Color(87, 148, 210));
        jPanel17.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel8.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(255, 255, 255));
        jLabel8.setText("1 ère Place:  ");

        premier.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        premier.setForeground(new java.awt.Color(255, 255, 255));
        premier.setText("Nom de l'élève");

        jLabel19.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel19.setForeground(new java.awt.Color(255, 255, 255));
        jLabel19.setText("Classe: ");

        prclasse.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        prclasse.setForeground(new java.awt.Color(255, 255, 255));
        prclasse.setText("Année");

        jLabel16.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel16.setForeground(new java.awt.Color(255, 255, 255));
        jLabel16.setText("Moyenne: ");

        prmoy.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        prmoy.setForeground(new java.awt.Color(255, 255, 255));
        prmoy.setText("0.0");

        javax.swing.GroupLayout jPanel17Layout = new javax.swing.GroupLayout(jPanel17);
        jPanel17.setLayout(jPanel17Layout);
        jPanel17Layout.setHorizontalGroup(
            jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel17Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(premier, javax.swing.GroupLayout.DEFAULT_SIZE, 450, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel19)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(prclasse)
                .addGap(142, 142, 142)
                .addComponent(jLabel16)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(prmoy)
                .addGap(89, 89, 89))
        );
        jPanel17Layout.setVerticalGroup(
            jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(jLabel16)
                .addComponent(prmoy))
            .addGroup(jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(jLabel8)
                .addComponent(premier)
                .addComponent(jLabel19)
                .addComponent(prclasse))
        );

        jPanel18.setBackground(new java.awt.Color(87, 148, 210));
        jPanel18.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel9.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel9.setForeground(java.awt.Color.white);
        jLabel9.setText("2 ème Place:  ");

        deuxieme.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        deuxieme.setForeground(java.awt.Color.white);
        deuxieme.setText("Nom de l'élève");

        jLabel21.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel21.setForeground(java.awt.Color.white);
        jLabel21.setText("Classe: ");

        declasse.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        declasse.setForeground(java.awt.Color.white);
        declasse.setText("Année");

        jLabel17.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel17.setForeground(java.awt.Color.white);
        jLabel17.setText("Moyenne: ");

        demoy.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        demoy.setForeground(java.awt.Color.white);
        demoy.setText("0.0");

        javax.swing.GroupLayout jPanel18Layout = new javax.swing.GroupLayout(jPanel18);
        jPanel18.setLayout(jPanel18Layout);
        jPanel18Layout.setHorizontalGroup(
            jPanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel18Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel9)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(deuxieme, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel21)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(declasse)
                .addGap(142, 142, 142)
                .addComponent(jLabel17)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(demoy)
                .addGap(89, 89, 89))
        );
        jPanel18Layout.setVerticalGroup(
            jPanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel18Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addGroup(jPanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel17)
                        .addComponent(demoy))
                    .addGroup(jPanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel21)
                        .addComponent(declasse))
                    .addGroup(jPanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel9)
                        .addComponent(deuxieme))))
        );

        jPanel19.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        tblmoyenne.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tblmoyenne.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblmoyenneMouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(tblmoyenne);

        javax.swing.GroupLayout jPanel19Layout = new javax.swing.GroupLayout(jPanel19);
        jPanel19.setLayout(jPanel19Layout);
        jPanel19Layout.setHorizontalGroup(
            jPanel19Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 974, Short.MAX_VALUE)
        );
        jPanel19Layout.setVerticalGroup(
            jPanel19Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 190, Short.MAX_VALUE)
        );

        jPanel20.setBackground(new java.awt.Color(87, 148, 210));
        jPanel20.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        prcontrole.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        prcontrole.setForeground(new java.awt.Color(255, 255, 255));
        prcontrole.setText("1er Controle");
        prcontrole.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                prcontroleMouseClicked(evt);
            }
        });

        decontrole.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        decontrole.setForeground(new java.awt.Color(255, 255, 255));
        decontrole.setText("2ème Controle");
        decontrole.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                decontroleMouseClicked(evt);
            }
        });

        trcontrole.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        trcontrole.setForeground(new java.awt.Color(255, 255, 255));
        trcontrole.setText("3ème Controle");
        trcontrole.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                trcontroleMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanel20Layout = new javax.swing.GroupLayout(jPanel20);
        jPanel20.setLayout(jPanel20Layout);
        jPanel20Layout.setHorizontalGroup(
            jPanel20Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel20Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(prcontrole)
                .addGap(10, 10, 10)
                .addComponent(decontrole)
                .addGap(21, 21, 21)
                .addComponent(trcontrole)
                .addContainerGap())
        );
        jPanel20Layout.setVerticalGroup(
            jPanel20Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel20Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(prcontrole)
                .addComponent(decontrole)
                .addComponent(trcontrole))
        );

        jPanel21.setBackground(new java.awt.Color(87, 148, 210));
        jPanel21.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(255, 255, 255));
        jLabel4.setText("Année Académique: ");

        academique1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        academique1.setForeground(new java.awt.Color(255, 255, 255));
        academique1.setText("0");

        javax.swing.GroupLayout jPanel21Layout = new javax.swing.GroupLayout(jPanel21);
        jPanel21.setLayout(jPanel21Layout);
        jPanel21Layout.setHorizontalGroup(
            jPanel21Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel21Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(academique1, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel21Layout.setVerticalGroup(
            jPanel21Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel21Layout.createSequentialGroup()
                .addGroup(jPanel21Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(academique1))
                .addGap(0, 0, Short.MAX_VALUE))
        );

        jPanel22.setBackground(new java.awt.Color(255, 255, 255));
        jPanel22.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel6.setText("Classe:");

        jLabel7.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel7.setText("Matricule:");

        jLabel24.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel24.setText("Elève:  ");

        javax.swing.GroupLayout jPanel22Layout = new javax.swing.GroupLayout(jPanel22);
        jPanel22.setLayout(jPanel22Layout);
        jPanel22Layout.setHorizontalGroup(
            jPanel22Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel22Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(jLabel24))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel22Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel22Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel7, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel6, javax.swing.GroupLayout.Alignment.TRAILING)))
        );
        jPanel22Layout.setVerticalGroup(
            jPanel22Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel22Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel6)
                .addGap(28, 28, 28)
                .addComponent(jLabel7)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel24)
                .addContainerGap())
        );

        jPanel23.setBackground(new java.awt.Color(255, 255, 255));
        jPanel23.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        classe1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        classe1.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                classe1ItemStateChanged(evt);
            }
        });

        nom1.setBackground(new java.awt.Color(255, 255, 255));
        nom1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        nom1.setForeground(new java.awt.Color(87, 148, 210));
        nom1.setText("Nom de l'élève");
        nom1.setOpaque(true);

        matricule1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                matricule1KeyReleased(evt);
            }
        });

        javax.swing.GroupLayout jPanel23Layout = new javax.swing.GroupLayout(jPanel23);
        jPanel23.setLayout(jPanel23Layout);
        jPanel23Layout.setHorizontalGroup(
            jPanel23Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel23Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel23Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(classe1, javax.swing.GroupLayout.PREFERRED_SIZE, 328, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(nom1, javax.swing.GroupLayout.PREFERRED_SIZE, 807, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(matricule1, javax.swing.GroupLayout.PREFERRED_SIZE, 267, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel23Layout.setVerticalGroup(
            jPanel23Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel23Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(classe1, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(matricule1, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(nom1, javax.swing.GroupLayout.DEFAULT_SIZE, 19, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel15Layout = new javax.swing.GroupLayout(jPanel15);
        jPanel15.setLayout(jPanel15Layout);
        jPanel15Layout.setHorizontalGroup(
            jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel16, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel17, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel18, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel19, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel15Layout.createSequentialGroup()
                .addComponent(jPanel20, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel21, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGroup(jPanel15Layout.createSequentialGroup()
                .addComponent(jPanel22, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel23, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel15Layout.setVerticalGroup(
            jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel15Layout.createSequentialGroup()
                .addGroup(jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel20, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel21, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel23, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel22, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel19, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel17, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel18, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel16, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jTabbedPane5.addTab("CONSULTATION DE MOYENNE", jPanel15);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel1Layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(jTabbedPane5)
                    .addContainerGap()))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(481, Short.MAX_VALUE))
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                    .addContainerGap(77, Short.MAX_VALUE)
                    .addComponent(jTabbedPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 470, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap()))
        );

        getContentPane().add(jPanel1, "card2");

        jMenu5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/ButtonFoto/icons8-menu.png"))); // NOI18N
        jMenu5.setText("Action");

        enregistrer.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/ButtonFoto/icons8-assignment_turned_in.png"))); // NOI18N
        enregistrer.setText("Assigner");
        enregistrer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                enregistrerActionPerformed(evt);
            }
        });
        jMenu5.add(enregistrer);

        jMenuBar3.add(jMenu5);

        jMenu6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/ButtonFoto/icons8-menu_squared.png"))); // NOI18N
        jMenu6.setText("Edition");

        jMenuItem14.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/ButtonFoto/icons8-view_file.png"))); // NOI18N
        jMenuItem14.setText("Consulter");
        jMenu6.add(jMenuItem14);

        jMenuItem15.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/ButtonFoto/icons8-edit_file.png"))); // NOI18N
        jMenuItem15.setText("Modifier");
        jMenuItem15.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem15ActionPerformed(evt);
            }
        });
        jMenu6.add(jMenuItem15);

        jMenuItem16.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/ButtonFoto/icons8-new_by_copy.png"))); // NOI18N
        jMenuItem16.setText("Ajouter Matière");
        jMenu6.add(jMenuItem16);

        jMenuItem17.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/ButtonFoto/icons8-help.png"))); // NOI18N
        jMenuItem17.setText("Aide");
        jMenu6.add(jMenuItem17);

        jMenuBar3.add(jMenu6);

        setJMenuBar(jMenuBar3);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void tblmatriculeMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblmatriculeMousePressed
        if(tblmatricule.getRowCount()==0){
            nom.setText("Nom de l'élève");
            note.setEnabled(false);
        }else{
            note.setEnabled(true);
            String query="select t.nom,t.prenom,c.description as classe from tercero t left join eleve e "
            + "on (t.code = e.code and t.code_tercero = e.code_tercero) left join inscription i on (t.code = i.code and t.code_tercero = i.code_tercero) left join "
            + "classe c on c.code_classe=i.code_classe where e.matricule = '"+tblmatricule.getValueAt(tblmatricule.getSelectedRow(),0).toString()+"' ";
            ResultSet rs=Mn.SEARCHDATA(query);
            try {
                while(rs.next()){
                    if(classe.getSelectedItem().equals("Selectionnez")){
                        JOptionPane.showMessageDialog(this,"S'il vous plait veuillez séléctionner une classe.");
                    }else if(rs.getString("classe").equals(classe.getSelectedItem().toString())){
                        nom.setText(rs.getString("nom")+" "+rs.getString("prenom"));
                        matiere.setEnabled(true);
                    }else{
                        matiere.setEnabled(false);
                    }

                }
            } catch (SQLException ex) {
                Logger.getLogger(Entrer_noteForm.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }//GEN-LAST:event_tblmatriculeMousePressed

    private void sectionItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_sectionItemStateChanged

    }//GEN-LAST:event_sectionItemStateChanged

    private void sectionFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_sectionFocusLost
        if(section.getSelectedItem().toString().isEmpty()){
            classe.setSelectedItem("Selectionnez");
        }else{
            Mn.FULLCOMBO(classe, "description","section",section.getSelectedItem().toString(), "classe");
        }
    }//GEN-LAST:event_sectionFocusLost

    private void classeItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_classeItemStateChanged
        try {

            if(classe.getSelectedItem().toString().equals("Selectionnez") ||classe.getSelectedItem().toString().isEmpty()){
                surmatiere.setText("0");
            }else{
                int aa=Mn.SEARCHCODE("code_classe","description","section",classe.getSelectedItem().toString(),
                    section.getSelectedItem().toString(), "classe");
                String query="select count(cm.code_matiere) as quantite,sum(e.valeur) as sur from matiere_vs_classe cm left join matiere m \n" +
                "on m.code_matiere=cm.code_matiere left join classe c on c.code_classe=cm.code_classe left join equivalence e \n" +
                "on e.code_equivalence=m.code_equivalence where c.code_classe="+aa+"";
                ResultSet rrs=Mn.SEARCHDATA(query);
                try {

                    if(rrs.first()){
                        surmatiere.setText(rrs.getString("quantite"));
                        surnote.setText(rrs.getString("sur"));
                    }
                }catch (SQLException ex) {
                    Logger.getLogger(Entrer_note.class.getName()).log(Level.SEVERE, null, ex);
                }
                //matiere.removeAllItems();
                query="select e.matricule from inscription i left join eleve e on ((e.code = i.code) and (e.code_tercero = i.code_tercero)) \n" +
                "left join classe c on c.code_classe=i.code_classe where c.section='"+section.getSelectedItem().toString()+"' and "
                + " c.description='"+classe.getSelectedItem().toString()+"' and i.annee_academique='"+academique.getText()+"'";
                Mn.FULLTABLE(query, tblmatricule, mdss);
                nom.setText("Nom de l'élève");
                note.setEnabled(false);
            }
        } catch (Exception e) {
        }

    }//GEN-LAST:event_classeItemStateChanged

    private void classeFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_classeFocusGained
        if(!section.getSelectedItem().toString().equals("Selectionnez")){
            Mn.FULLCOMBO(classe, "description","section",section.getSelectedItem().toString(), "classe");
        }
    }//GEN-LAST:event_classeFocusGained

    private void matiereFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_matiereFocusGained
        String aaa="";
        if(!classe.getSelectedItem().equals("Selectionnez"))
        {
            aaa=classe.getSelectedItem().toString();
            int aa=Mn.SEARCHCODE("code_classe","description","section",classe.getSelectedItem().toString(), section.getSelectedItem().toString(), "classe");
            String query="select distinct m.description as matiere from matiere_vs_classe cm left join \n" +
            "matiere m on m.code_matiere=cm.code_matiere left join classe c \n" +
            "on c.code_classe=cm.code_classe where cm.code_classe="+aa+"  order by matiere asc";
            Mn.FULLCOMBO(query,matiere, "matiere");
        }else if(aaa.equals("Selectionnez")){
            matiere.setSelectedItem("Selectionnez") ;
        }
    }//GEN-LAST:event_matiereFocusGained

    private void matiereFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_matiereFocusLost
        try {
            if(matiere.getSelectedItem().equals("Selectionnez") || matiere.getSelectedItem().toString().isEmpty()){
                sur.setText("0");
            }else{
                String aa=matiere.getSelectedItem().toString();
                try {
                    String query="select e.valeur from matiere m left join equivalence e"
                    + " on e.code_equivalence=m.code_equivalence where m.description='"+aa+"'";
                    ResultSet rs=Mn.SEARCHDATA(query);
                    if(rs.first()){
                        sur.setText(rs.getString("valeur"));
                    }
                } catch (SQLException ex) {}
            }

        } catch (Exception e) {
        }

    }//GEN-LAST:event_matiereFocusLost

    private void noteFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_noteFocusLost

    }//GEN-LAST:event_noteFocusLost

    private void noteKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_noteKeyReleased
        int aa=0;
        int bb=0;
        if(!note.getText().isEmpty()){
            aa=Integer.parseInt(note.getText());  bb=Integer.parseInt(sur.getText());
        }
        if(aa>bb){
            JOptionPane.showMessageDialog(this,"Les Notes ne doivent pas supérieur aux équivalences.");
            note.setText(null);
            note.requestFocus();
        }
    }//GEN-LAST:event_noteKeyReleased

    private void addActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addActionPerformed
        String[] vect={matiere.getSelectedItem().toString(),note.getText(),sur.getText()};
        String mat=matiere.getSelectedItem().toString();
        String acte="";
        if(tblnotes.getRowCount()==0){
            Mn.ADD_TABLE(md, vect);
            SumNote();
            matiere.setSelectedItem(null);
            note.setText(null);
            sur.setText(null);
        }else {
            for(int i=0;i<tblnotes.getRowCount();i++)
            {
                if(tblnotes.getValueAt(i, 0).equals(mat))
                {
                    acte="lala";
                    JOptionPane.showMessageDialog(this,"Desole,vous avez deja donné note a "+ mat +". S'il vous plait selectionner une autre matiere");
                }
            }
            if(acte.equals("lala")){
                matiere.setSelectedItem(null);
                note.setText(null);
                sur.setText(null);
            }else{
                Mn.ADD_TABLE(md, vect);
                SumNote();
                matiere.setSelectedItem(null);
                note.setText(null);
                sur.setText(null);
            }

        }

    }//GEN-LAST:event_addActionPerformed

    private void remActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_remActionPerformed
        Mn.REMOVE_TABLE(tblnotes, md);
        SumNote();
    }//GEN-LAST:event_remActionPerformed

    private void tblmoyenneMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblmoyenneMouseClicked
        matricule1.setText(String.valueOf(tblmoyenne.getValueAt(tblmoyenne.getSelectedRow(),0)));
        nom1.setText(String.valueOf(tblmoyenne.getValueAt(tblmoyenne.getSelectedRow(),1)+" "+
            tblmoyenne.getValueAt(tblmoyenne.getSelectedRow(),2)));
    }//GEN-LAST:event_tblmoyenneMouseClicked

    private void prcontroleMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_prcontroleMouseClicked
        prcontrole.setSelected(true);
        decontrole.setSelected(false);
        trcontrole.setSelected(false);
        matricule1.setText(null);
        //----------------------------------------------------------------------------------
        int aa=0;
        int aaa=0;
        if(prcontrole.isSelected()){
            aaa=1;
        }else if(decontrole.isSelected()){
            aaa=2;
        }else if(trcontrole.isSelected()){
            aaa=3;
        }
        if(classe1.getSelectedItem().equals("Selectionnez")){
            RemplirMoyenne();
        }else {
            aa=Integer.parseInt(Mn.SEARCHCODE("code_classe","description",classe1.getSelectedItem().toString(), "classe"));
            String query="select e.matricule,t.nom,t.prenom,c.description as classe,n.controle,n.moyenne from eleve e \n" +
            "left join tercero t on (t.code = e.code and t.code_tercero = e.code_tercero) left join note n on e.matricule=n.matricule\n" +
            "left join classe c on c.code_classe=n.code_classe where  n.code_classe="+aa+" and n.controle="+aaa+" and n.annee='"+academique1.getText()+"'  order by moyenne desc";
            Mn.FULLTABLE(query, tblmoyenne, mds);
        }
        RemplirPlas();
    }//GEN-LAST:event_prcontroleMouseClicked

    private void decontroleMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_decontroleMouseClicked
        prcontrole.setSelected(false);
        decontrole.setSelected(true);
        trcontrole.setSelected(false);
        matricule1.setText(null);
        //------------------------------------------------------------------------------
        int aa=0;
        int aaa=0;
        if(prcontrole.isSelected()){
            aaa=1;
        }else if(decontrole.isSelected()){
            aaa=2;
        }else if(trcontrole.isSelected()){
            aaa=3;
        }
        if(classe1.getSelectedItem().equals("Selectionnez")){
            RemplirMoyenne();
        }else{
            aa=Integer.parseInt(Mn.SEARCHCODE("code_classe","description",classe1.getSelectedItem().toString(), "classe"));
            String query="select e.matricule,t.nom,t.prenom,c.description as classe,n.controle,n.moyenne from eleve e \n" +
            "left join tercero t on (t.code = e.code and t.code_tercero = e.code_tercero) left join note n on e.matricule=n.matricule\n" +
            "left join classe c on c.code_classe=n.code_classe\n" +
            "where  n.code_classe="+aa+" and n.controle="+aaa+" and n.annee='"+academique1.getText()+"'  order by moyenne desc";
            //System.out.println(query);
            Mn.FULLTABLE(query, tblmoyenne, mds);
        }
        RemplirPlas();
    }//GEN-LAST:event_decontroleMouseClicked

    private void trcontroleMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_trcontroleMouseClicked
        prcontrole.setSelected(false);
        decontrole.setSelected(false);
        trcontrole.setSelected(true);
        matricule1.setText(null);
        //---------------------------------------------------------------------------------
        int aa=0;
        int aaa=0;
        if(prcontrole.isSelected()){
            aaa=1;
        }else if(decontrole.isSelected()){
            aaa=2;
        }else if(trcontrole.isSelected()){
            aaa=3;
        }
        if(classe1.getSelectedItem().equals("Selectionnez")){
            RemplirMoyenne();
        }else{
            aa=Integer.parseInt(Mn.SEARCHCODE("code_classe","description",classe1.getSelectedItem().toString(), "classe"));
            String query="select e.matricule,t.nom,t.prenom,c.description as classe,n.controle,n.moyenne from eleve e \n" +
            "left join tercero t on (t.code = e.code and t.code_tercero = e.code_tercero) left join note n on e.matricule=n.matricule\n" +
            "left join classe c on c.code_classe=n.code_classe\n" +
            "where  n.code_classe="+aa+" and n.controle="+aaa+" and n.annee='"+academique1.getText()+"'  order by moyenne desc";
            //System.out.println(query);
            Mn.FULLTABLE(query, tblmoyenne, mds);
        }
        RemplirPlas();
    }//GEN-LAST:event_trcontroleMouseClicked

    private void classe1ItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_classe1ItemStateChanged
        int aa=0;
        int aaa=0;
        if(prcontrole.isSelected()){
            aaa=1;
        }else if(decontrole.isSelected()){
            aaa=2;
        }else if(trcontrole.isSelected()){
            aaa=3;
        }
        if(classe1.getSelectedItem().equals("Selectionnez")){
            RemplirMoyenne();
        }else{
            aa=Integer.parseInt(Mn.SEARCHCODE("code_classe","description",classe1.getSelectedItem().toString(), "classe"));
            String query="select e.matricule,t.nom,t.prenom,c.description as classe,n.controle,n.moyenne from eleve e \n" +
            "left join tercero t on (t.code = e.code and t.code_tercero = e.code_tercero) left join note n on e.matricule=n.matricule\n" +
            "left join classe c on c.code_classe=n.code_classe\n" +
            "where  n.code_classe="+aa+" and n.controle="+aaa+" and n.annee='"+academique1.getText()+"'  order by moyenne desc";
            //System.out.println(query);
            Mn.FULLTABLE(query, tblmoyenne, mds);
        }
        RemplirPlas();
    }//GEN-LAST:event_classe1ItemStateChanged

    private void matricule1KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_matricule1KeyReleased
        matricule1.setText(matricule1.getText().toUpperCase());
        String query="select t.nom,t.prenom from tercero t left join eleve e "
        + "on (t.code = e.code and t.code_tercero = e.code_tercero) where e.matricule like '%"+matricule1.getText()+"%'";
        ResultSet rs=Mn.SEARCHDATA(query);
        try {
            while(rs.next()){
                nom1.setText(rs.getString("nom")+" "+rs.getString("prenom"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(Entrer_note.class.getName()).log(Level.SEVERE, null, ex);
        }
        if(matricule1.getText().isEmpty()){
            nom1.setText("Nom de l'élève");
        }
        //--------------------------------------------------------------------------------------------------------
        int aa=0;
        if(prcontrole.isSelected()){
            aa=1;
        }else if(decontrole.isSelected()){
            aa=2;
        }else if(trcontrole.isSelected()){
            aa=3;
        }
        String querys="select  e.matricule,t.nom,t.prenom,c.description as classse,n.controle,n.moyenne from eleve e left join tercero t \n" +
        "on (t.code = e.code and t.code_tercero = e.code_tercero) left join note n on e.matricule=n.matricule\n" +
        "left join classe c on c.code_classe=n.code_classe where e.matricule like '%"+matricule1.getText()+"%' "
        + "and n.controle="+aa+" and n.annee='"+academique1.getText()+"'  order by matricule asc ";
        Mn.FULLTABLE(querys, tblmoyenne, mds);
    }//GEN-LAST:event_matricule1KeyReleased

    private void jTabbedPane5MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTabbedPane5MouseClicked
        //        RemplirMoyenne();
    }//GEN-LAST:event_jTabbedPane5MouseClicked

    private void controleActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_controleActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_controleActionPerformed

    private void enregistrerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_enregistrerActionPerformed
        InsertNote();
        InsertDetaille();
        online();
        md.setRowCount(0);
        totalmatiere.setText("0");
        noteeleve.setText("0");
    }//GEN-LAST:event_enregistrerActionPerformed

    private void jMenuItem15ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem15ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jMenuItem15ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Entrer_noteForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Entrer_noteForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Entrer_noteForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Entrer_noteForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Entrer_noteForm().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel academique;
    private javax.swing.JLabel academique1;
    private javax.swing.JButton add;
    private javax.swing.JComboBox classe;
    private javax.swing.JComboBox classe1;
    private javax.swing.JTextField controle;
    private javax.swing.JLabel declasse;
    private javax.swing.JRadioButton decontrole;
    private javax.swing.JLabel demoy;
    private javax.swing.JLabel deuxieme;
    private javax.swing.JMenuItem enregistrer;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel30;
    private javax.swing.JLabel jLabel31;
    private javax.swing.JLabel jLabel32;
    private javax.swing.JLabel jLabel33;
    private javax.swing.JLabel jLabel34;
    private javax.swing.JLabel jLabel35;
    private javax.swing.JLabel jLabel36;
    private javax.swing.JLabel jLabel38;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JMenu jMenu5;
    private javax.swing.JMenu jMenu6;
    private javax.swing.JMenuBar jMenuBar3;
    private javax.swing.JMenuItem jMenuItem14;
    private javax.swing.JMenuItem jMenuItem15;
    private javax.swing.JMenuItem jMenuItem16;
    private javax.swing.JMenuItem jMenuItem17;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel11;
    private javax.swing.JPanel jPanel12;
    private javax.swing.JPanel jPanel13;
    private javax.swing.JPanel jPanel14;
    private javax.swing.JPanel jPanel15;
    private javax.swing.JPanel jPanel16;
    private javax.swing.JPanel jPanel17;
    private javax.swing.JPanel jPanel18;
    private javax.swing.JPanel jPanel19;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel20;
    private javax.swing.JPanel jPanel21;
    private javax.swing.JPanel jPanel22;
    private javax.swing.JPanel jPanel23;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JTabbedPane jTabbedPane5;
    private javax.swing.JComboBox matiere;
    private javax.swing.JTextField matricule1;
    private javax.swing.JLabel nom;
    private javax.swing.JLabel nom1;
    private org.edisoncor.gui.textField.TextFieldRectBackground note;
    private javax.swing.JLabel noteeleve;
    private javax.swing.JLabel prclasse;
    private javax.swing.JRadioButton prcontrole;
    private javax.swing.JLabel premier;
    private javax.swing.JLabel prmoy;
    private javax.swing.JButton rem;
    private javax.swing.JComboBox section;
    private javax.swing.JLabel sur;
    private javax.swing.JLabel surmatiere;
    private javax.swing.JLabel surnote;
    private javax.swing.JTable tblmatricule;
    private javax.swing.JTable tblmoyenne;
    private javax.swing.JTable tblnotes;
    private javax.swing.JLabel totalmatiere;
    private javax.swing.JLabel trclasse;
    private javax.swing.JRadioButton trcontrole;
    private javax.swing.JLabel trmoy;
    private javax.swing.JLabel troisieme;
    // End of variables declaration//GEN-END:variables
}
