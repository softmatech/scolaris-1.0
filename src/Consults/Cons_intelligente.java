
package Consults;

import Consults.*;
import Methods.*;
import java.awt.Color;
import java.awt.Component;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumnModel;

public class Cons_intelligente extends javax.swing.JInternalFrame {
Manipulation Mn= new Manipulation();
Theme th= new Theme();
Rapport Rp= new Rapport();
    public Cons_intelligente() {
        initComponents();
        this.setTitle("Scolaris: "+Variables.Empresa+" : Consultation Intelligente");
         eleve.setSelected(true);
         StructureEleve();
         Mn.FULLCOMBOACADEMIQUE(academique,"annee", "annee_academique");
    }
 
 private void InactivateControle(){
     controle1.setEnabled(false);
     controle2.setEnabled(false);
     controle3.setEnabled(false);
 }
  private void ActivateControle(){
     controle1.setEnabled(true);
     controle1.setSelected(true);
     controle2.setEnabled(true);
     controle3.setEnabled(true);
 }  
  
//----------------------- Pwogramasyon  metod ki nan Radio Eleve la---------------------
private void StructureEleve(){
recherche.setEnabled(false);
InactivateControle();
qut.setVisible(false);
aux.setVisible(false);
reyisi.setVisible(false);
reyisil.setVisible(false);
nonreyisi.setVisible(false);
nonreyisil.setVisible(false);
classe.setSelected(false);
employer.setSelected(false);
professeur.setSelected(false);
resultat.setSelected(false);
collection.setSelected(false);
paiement.setSelected(false);
bulletin.setSelected(false);
matiere.setSelected(false);
eleve.setSelected(true);
//-------------------------
borderaux.setSelected(false);


reservation.setSelected(false);


inscription.setSelected(false);


//-------------------------------------------------
section.setSelectedItem("Selectionnez");
klas.setSelectedItem(null);
section.setEnabled(true);
klas.setEnabled(true);
//------------------------------------------------
  TableCellRenderer render= new TableCellRenderer() {
 public Component getTableCellRendererComponent(JTable table,Object value,boolean idSelected,boolean hasFocus,int row,int column){
     JLabel lbl= new JLabel(value==null? "": value.toString());
     if(column>=0 && column<4 && row>=0){
         lbl.setHorizontalAlignment(SwingConstants.LEFT);
         lbl.setOpaque(true);
         lbl.setBackground(Color.green);
     }
      if(column==4 && !value.toString().equals("Actif(ve)")&& row>=0){
         lbl.setHorizontalAlignment(SwingConstants.LEFT);
         lbl.setOpaque(true);
         lbl.setForeground(Color.red);
     }
     return lbl;
 }
};
String[][] data={};
String[] head={"Matricule","Nom","Prénom","Téléphone","Status"};
String[] heads={"Date de Naissance"," Sèxe"," Adresse"," Santé"," Grpe. Sanguin"," Pres. Responsable","Téléphone","Lien Parenté"};
DefaultTableModel md= new DefaultTableModel(data,head);
DefaultTableModel mds= new DefaultTableModel(data,heads);
tblpere.setModel(md);
tblfils.setModel(mds);
TableColumnModel cm= tblpere.getColumnModel();
        cm.getColumn(0).setPreferredWidth(30);
        cm.getColumn(1).setPreferredWidth(70);
        cm.getColumn(2).setPreferredWidth(78);
        cm.getColumn(3).setPreferredWidth(40);
        cm.getColumn(4).setPreferredWidth(10);
        tblpere.getColumnModel().getColumn(0).setCellRenderer(render);
        tblpere.getColumnModel().getColumn(3).setCellRenderer(render);
        tblpere.getColumnModel().getColumn(4).setCellRenderer(render);
TableColumnModel cmf= tblfils.getColumnModel();
cmf.getColumn(0).setPreferredWidth(90);
cmf.getColumn(1).setPreferredWidth(5);
cmf.getColumn(2).setPreferredWidth(100);
cmf.getColumn(3).setPreferredWidth(20);
cmf.getColumn(4).setPreferredWidth(70);
cmf.getColumn(5).setPreferredWidth(40);
cmf.getColumn(6).setPreferredWidth(10);

//--------------------------------------------------------------------------------------------------
String lestado="";
      String query="select distinct t.code AS code,t.code_tercero AS code_tercero,t.prenom AS prenom,t.nom AS nom,pd.sexe AS sexe,\n" +
"pd.adresse AS adresse,e.matricule AS matricule,pd.naissance AS naissance,e.cond_sante AS cond_sante,\n" +
"(select show_eleve_responsable.responsable AS responsable from show_eleve_responsable \n" +
"where (eht.eleve_matricule = show_eleve_responsable.matricule)) AS responsable,eht.lien_parente AS lien_parente,\n" +
"s.sang AS sang,e.estatus AS estatus,i.nom AS institution,tvt.phone_number as telephone from ((((((((eleve e left join tercero t\n" +
"on(((e.code = t.code) and (e.code_tercero = t.code_tercero)))) left join personal_data pd on(((t.code = pd.code)\n" +
"and (t.code_tercero = pd.code_tercero)))) left join tercero_vs_telephones tvt on(((t.code = tvt.code) \n" +
"and (t.code_tercero = tvt.code_tercero)))) left join eleve_has_tercero_responsable eht \n" +
"on((e.matricule = eht.eleve_matricule))) left join tercero_email te on(((t.code = te.code) and \n" +
"(t.code_tercero = te.code_tercero)))) left join sanguins s on(((t.code = s.code) and\n" +
"(t.code_tercero = s.code_tercero)))) left join eleve_has_institution ehi on(((e.matricule = ehi.matricule) and\n" +
"(e.code = ehi.code) and (e.code_tercero = ehi.code_tercero)))) left join institution i \n" +
"on((i.idinstitution = ehi.idinstitution)))  order by nom asc";
    ResultSet rs=Mn.SEARCHDATA(query);
    try {
        md.setRowCount(0);
       while(rs.next()){
           if(rs.getBoolean("estatus")==true){
              lestado="Actif(ve)"; 
           }else{
             lestado="Inactif(ve)";   
           }
         String[] vect={rs.getString("matricule"),rs.getString("nom"),rs.getString("prenom"),
        rs.getString("telephone"),lestado};
         Mn.ADD_TABLE(md, vect);
        }
    } catch (SQLException ex) {
        Logger.getLogger(Cons_intelligente.class.getName()).log(Level.SEVERE, null, ex);
    }
//----------------------------------------li fini la----------------------------------------------------------
}

//----------------------- Pwogramasyon  metod ki nan Radio Classe la---------------------
private void StructureClasse(){
 InactivateControle();
 recherche.setEnabled(true);
employer.setSelected(false);
professeur.setSelected(false);
resultat.setSelected(false);
collection.setSelected(false);
paiement.setSelected(false);
bulletin.setSelected(false);
matiere.setSelected(false);
eleve.setSelected(false);
classe.setSelected(true);
reyisi.setVisible(false);
reyisil.setVisible(false);
nonreyisi.setVisible(false);
nonreyisil.setVisible(false);
//--------------------------
borderaux.setSelected(false);


reservation.setSelected(false);


inscription.setSelected(false);

//-------------------------------------------------
section.setSelectedItem(null);
klas.setSelectedItem(null);
section.setEnabled(true);
klas.setEnabled(true);
//------------------------------------------------
String[][] data={};
String[] head={"  Classes","   Sections"};
String[] heads={" Matricule"," Nom"," Prénom"," Adresse"," Pres. Responsable","Téléphone","Lien Parenté"};
DefaultTableModel md= new DefaultTableModel(data,head);
DefaultTableModel mds= new DefaultTableModel(data,heads);
tblpere.setModel(md);
tblfils.setModel(mds);
TableColumnModel cmf= tblfils.getColumnModel();
cmf.getColumn(0).setPreferredWidth(30);
cmf.getColumn(1).setPreferredWidth(80);
cmf.getColumn(2).setPreferredWidth(100);
cmf.getColumn(3).setPreferredWidth(150);
cmf.getColumn(4).setPreferredWidth(70);
cmf.getColumn(5).setPreferredWidth(40);
cmf.getColumn(6).setPreferredWidth(10);

//--------------------------------------------------------------------------------------------------
      String query="select * from show_classe";
    ResultSet rs=Mn.SEARCHDATA(query);
    try {
        md.setRowCount(0);
       while(rs.next()){
         String[] vect={rs.getString("classe"),rs.getString("section")};
         Mn.ADD_TABLE(md, vect);
        }
    } catch (SQLException ex) {
        Logger.getLogger(Cons_intelligente.class.getName()).log(Level.SEVERE, null, ex);
    }
//--------------------------------------------------------------------------------------------------

}
//----------------------- Pwogramasyon  metod ki nan Radio Employer la---------------------
private void StructureEmployer(){
 InactivateControle();
 qut.setVisible(false);
aux.setVisible(false);
reyisi.setVisible(false);
reyisil.setVisible(false);
nonreyisi.setVisible(false);
nonreyisil.setVisible(false);
recherche.setEnabled(false);
professeur.setSelected(false);
resultat.setSelected(false);
collection.setSelected(false);
paiement.setSelected(false);
bulletin.setSelected(false);
matiere.setSelected(false);
eleve.setSelected(false);
classe.setSelected(false);
employer.setSelected(true);
//------------------
borderaux.setSelected(false);


reservation.setSelected(false);


inscription.setSelected(false);

//-------------------------------------------------
section.setSelectedItem(null);
klas.setSelectedItem(null);
section.setEnabled(false);
klas.setEnabled(false);
//-------------------------------------------------
  TableCellRenderer render= new TableCellRenderer() {
 public Component getTableCellRendererComponent(JTable table,Object value,boolean idSelected,boolean hasFocus,int row,int column){
     JLabel lbl= new JLabel(value==null? "": value.toString());
     if(column>=0 && column<4 && row>=0){
         lbl.setHorizontalAlignment(SwingConstants.LEFT);
         lbl.setOpaque(true);
         lbl.setBackground(Color.green);
     }
      if(column==4 && !value.toString().equals("Actif(ve)")&& row>=0){
         lbl.setHorizontalAlignment(SwingConstants.LEFT);
         lbl.setOpaque(true);
         lbl.setForeground(Color.red);
     }
     return lbl;
 }
};
String[][] data={};
String[] head={"Matricule","Nom","Prénom","Type Employer","Status"};
String[] heads={"Date de Naissance"," Sèxe"," Adresse","Document","Type_Document"," Email"," Etat Civil","Téléphone"};
DefaultTableModel md= new DefaultTableModel(data,head);
DefaultTableModel mds= new DefaultTableModel(data,heads);
tblpere.setModel(md);
tblfils.setModel(mds);
TableColumnModel cm= tblpere.getColumnModel();
        cm.getColumn(0).setPreferredWidth(30);
        cm.getColumn(1).setPreferredWidth(70);
        cm.getColumn(2).setPreferredWidth(78);
        cm.getColumn(3).setPreferredWidth(40);
        cm.getColumn(4).setPreferredWidth(10);
        tblpere.getColumnModel().getColumn(0).setCellRenderer(render);
        tblpere.getColumnModel().getColumn(3).setCellRenderer(render);
        tblpere.getColumnModel().getColumn(4).setCellRenderer(render);
TableColumnModel cmf= tblfils.getColumnModel();
cmf.getColumn(0).setPreferredWidth(90);
cmf.getColumn(1).setPreferredWidth(5);
cmf.getColumn(2).setPreferredWidth(100);
cmf.getColumn(3).setPreferredWidth(70);
cmf.getColumn(4).setPreferredWidth(30);
cmf.getColumn(5).setPreferredWidth(90);
cmf.getColumn(6).setPreferredWidth(10);
cmf.getColumn(6).setPreferredWidth(40);
//--------------------------------------------------------------------------------------------------
String lestado="";
      String query="select * from show_employes order by nom asc";
    ResultSet rs=Mn.SEARCHDATA(query);
    try {
        md.setRowCount(0);
       while(rs.next()){
           if(rs.getBoolean("estatuses")==true){
              lestado="Actif(ve)"; 
           }else{
             lestado="Inactif(ve)";   
           }
         String[] vect={rs.getString("matricule"),rs.getString("nom"),rs.getString("prenom"),
        rs.getString("type_employer"),lestado};
         Mn.ADD_TABLE(md, vect);
        }
    } catch (SQLException ex) {
        Logger.getLogger(Cons_intelligente.class.getName()).log(Level.SEVERE, null, ex);
    }
//--------------------------------------------------------------------------------------------------

}

//----------------------- Pwogramasyon  metod ki nan Radio Professeur la---------------------
private void StructureProfesseur(){
 InactivateControle();
 qut.setVisible(false);
aux.setVisible(false);
reyisi.setVisible(false);
reyisil.setVisible(false);
nonreyisi.setVisible(false);
nonreyisil.setVisible(false);
 recherche.setEnabled(true);
resultat.setSelected(false);
collection.setSelected(false);
paiement.setSelected(false);
bulletin.setSelected(false);
matiere.setSelected(false);
eleve.setSelected(false);
classe.setSelected(false);
employer.setSelected(false);
professeur.setSelected(true);
//------------------
borderaux.setSelected(false);


reservation.setSelected(false);


inscription.setSelected(false);

//----------------------------------------------------------
section.setSelectedItem(null);
klas.setSelectedItem(null);
section.setEnabled(false);
klas.setEnabled(false);
//-----------------------------------------------------------
          TableCellRenderer render= new TableCellRenderer() {
 public Component getTableCellRendererComponent(JTable table,Object value,boolean idSelected,boolean hasFocus,int row,int column){
     JLabel lbl= new JLabel(value==null? "": value.toString());
     if(column==3 && row>=0){
         lbl.setHorizontalAlignment(SwingConstants.LEFT);
         lbl.setOpaque(true);
         lbl.setBackground(Color.yellow);
     }
if(column==0 && row>=0){
         lbl.setHorizontalAlignment(SwingConstants.LEFT);
         lbl.setOpaque(true);
         lbl.setBackground(Color.green);
     }
     return lbl;
 }
};
String[][] data={};
String[] head={"Matricule","Nom","Prénom","Quant.Hres/Semaine"};
String[] heads={"Matières"," Classes"," Sections","Jours","Heures","Horaire"};
DefaultTableModel md= new DefaultTableModel(data,head);
DefaultTableModel mds= new DefaultTableModel(data,heads);
tblpere.setModel(md);
tblfils.setModel(mds);
TableColumnModel cm= tblpere.getColumnModel();
        cm.getColumn(0).setPreferredWidth(30);
        cm.getColumn(1).setPreferredWidth(75);
        cm.getColumn(2).setPreferredWidth(95);
        cm.getColumn(3).setPreferredWidth(70);
        tblpere.getColumnModel().getColumn(0).setCellRenderer(render);
        tblpere.getColumnModel().getColumn(3).setCellRenderer(render);
TableColumnModel cmf= tblfils.getColumnModel();
cmf.getColumn(0).setPreferredWidth(90);
cmf.getColumn(1).setPreferredWidth(70);
cmf.getColumn(2).setPreferredWidth(70);
cmf.getColumn(3).setPreferredWidth(70);
cmf.getColumn(4).setPreferredWidth(70);
//--------------------------------------------------------------------------------------------------
String lestado="";
      String query="select * from show_professeur order by nom asc";
    ResultSet rs=Mn.SEARCHDATA(query);
    try {
        md.setRowCount(0);
       while(rs.next()){
         String sql="select sum(heure) as quantite from professeur where matricule='"+rs.getString("matricule")+"'";
          ResultSet rss=Mn.SEARCHDATA(sql);
          if(rss.next()){
              lestado= rss.getString("quantite");
        String[] vect={rs.getString("matricule"),rs.getString("nom"),rs.getString("prenom"),lestado+" "+
        rs.getString("deviz")};
        Mn.ADD_TABLE(md, vect);
          }
        }
    } catch (SQLException ex) {
        Logger.getLogger(Cons_intelligente.class.getName()).log(Level.SEVERE, null, ex);
    }
//--------------------------------------------------------------------------------------------------

}

//----------------------- Pwogramasyon  metod ki nan Radio Resultat la---------------------
private void StructureResultat(){
ActivateControle();
qut.setVisible(false);
aux.setVisible(false);
recherche.setEnabled(true);
collection.setSelected(false);
paiement.setSelected(false);
bulletin.setSelected(false);
matiere.setSelected(false);
eleve.setSelected(false);
classe.setSelected(false);
employer.setSelected(false);
professeur.setSelected(false);
resultat.setSelected(true);
//------------------
borderaux.setSelected(false);


reservation.setSelected(false);


inscription.setSelected(false);

//------------------------------------------
section.setSelectedItem(null);
klas.setSelectedItem(null);
section.setEnabled(true);
klas.setEnabled(true);
//--------------------------------------------
String[][] data={};
String[] head={"Classes","Sections","Quant. Elèves"};
String[] heads={"Matricules"," Nom"," Prénom","Controle","Total Notes","Sur","Moyenne","Qualification","Moy. Générale","Decision"};
DefaultTableModel md= new DefaultTableModel(data,head);
DefaultTableModel mds= new DefaultTableModel(data,heads);
tblpere.setModel(md);
tblfils.setModel(mds);
TableColumnModel cm= tblpere.getColumnModel();
        cm.getColumn(0).setPreferredWidth(75);
        cm.getColumn(1).setPreferredWidth(75);
        cm.getColumn(2).setPreferredWidth(70);
TableColumnModel cmf= tblfils.getColumnModel();
cmf.getColumn(0).setPreferredWidth(30);
cmf.getColumn(1).setPreferredWidth(70);
cmf.getColumn(2).setPreferredWidth(90);
cmf.getColumn(3).setPreferredWidth(5);
cmf.getColumn(4).setPreferredWidth(5);
cmf.getColumn(5).setPreferredWidth(5);
cmf.getColumn(6).setPreferredWidth(5);
cmf.getColumn(7).setPreferredWidth(30);
cmf.getColumn(8).setPreferredWidth(5);
cmf.getColumn(9).setPreferredWidth(50);
//--------------------------------------------------------------------------------------------------
String lestado="";
      String query="select * from show_resultat order by classe asc";
    ResultSet rs=Mn.SEARCHDATA(query);
    try {
        md.setRowCount(0);
       while(rs.next()){
           int aa=Mn.SEARCHCODE("code_classe","description","section", rs.getString("classe"),rs.getString("section"), "classe");
         String sql="select count(code) as quantite from inscription where code_classe="+aa+"";
          ResultSet rss=Mn.SEARCHDATA(sql);
          if(rss.next()){
              lestado= rss.getString("quantite");
        String[] vect={rs.getString("classe"),rs.getString("section"),lestado+" "+"Elèves"};
        Mn.ADD_TABLE(md, vect);
          }
        }
    } catch (SQLException ex) {
        Logger.getLogger(Cons_intelligente.class.getName()).log(Level.SEVERE, null, ex);
    }
//--------------------------------------------------------------------------------------------------
}
//----------------------- Pwogramasyon  metod ki nan Radio Collection la---------------------
private void StructureCollection(){
 InactivateControle();
 qut.setVisible(false);
aux.setVisible(false);
reyisi.setVisible(false);
reyisil.setVisible(false);
nonreyisi.setVisible(false);
nonreyisil.setVisible(false);
 recherche.setEnabled(true);
paiement.setSelected(false);
bulletin.setSelected(false);
matiere.setSelected(false);
eleve.setSelected(false);
classe.setSelected(false);
employer.setSelected(false);
professeur.setSelected(false);
resultat.setSelected(false);
collection.setSelected(true);
//-------------------------
borderaux.setSelected(false);


reservation.setSelected(false);


inscription.setSelected(false);


//---------------------------------------------------------
section.setSelectedItem(null);
klas.setSelectedItem(null);
section.setEnabled(true);
klas.setEnabled(true);
//----------------------------------------------------------
String[][] data={};
String[] head={"Classes","Sections","Quant. Elèves"};
String[] heads={"Matricules"," Nom"," Prénom","Versement","Date","Balance"};
DefaultTableModel md= new DefaultTableModel(data,head);
DefaultTableModel mds= new DefaultTableModel(data,heads);
tblpere.setModel(md);
tblfils.setModel(mds);
TableColumnModel cmf= tblfils.getColumnModel();
cmf.getColumn(0).setPreferredWidth(30);
cmf.getColumn(1).setPreferredWidth(70);
cmf.getColumn(2).setPreferredWidth(100);
cmf.getColumn(3).setPreferredWidth(5);
cmf.getColumn(4).setPreferredWidth(150);
cmf.getColumn(5).setPreferredWidth(30);
//--------------------------------------------------------------------------------------------------
String lestado="";
      String query="select * from show_resultat order by classe asc";
    ResultSet rs=Mn.SEARCHDATA(query);
    try {
        md.setRowCount(0);
       while(rs.next()){
           int aa=Mn.SEARCHCODE("code_classe","description","section", rs.getString("classe"),rs.getString("section"), "classe");
         String sql="select count(code) as quantite from inscription where code_classe="+aa+"";
          ResultSet rss=Mn.SEARCHDATA(sql);
          if(rss.next()){
              lestado= rss.getString("quantite");
        String[] vect={rs.getString("classe"),rs.getString("section"),lestado+" "+"Elèves"};
        Mn.ADD_TABLE(md, vect);
          }
        }
    } catch (SQLException ex) {
        Logger.getLogger(Cons_intelligente.class.getName()).log(Level.SEVERE, null, ex);
    }
//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------
}
//----------------------- Pwogramasyon  metod ki nan Radio Paiement la---------------------
private void StructurePaiement(){
 InactivateControle();
 qut.setVisible(false);
aux.setVisible(false);
reyisi.setVisible(false);
reyisil.setVisible(false);
nonreyisi.setVisible(false);
nonreyisil.setVisible(false);
 recherche.setEnabled(false);
bulletin.setSelected(false);
matiere.setSelected(false);
eleve.setSelected(false);
classe.setSelected(false);
employer.setSelected(false);
professeur.setSelected(false);
resultat.setSelected(false);
collection.setSelected(false);
paiement.setSelected(true);
//-------------------------
borderaux.setSelected(false);


reservation.setSelected(false);


inscription.setSelected(false);

//-------------------------------------------------------------
section.setSelectedItem(null);
klas.setSelectedItem(null);
section.setEnabled(false);
klas.setEnabled(false);
//-------------------------------------------------------------
   TableCellRenderer render= new TableCellRenderer() {
 public Component getTableCellRendererComponent(JTable table,Object value,boolean idSelected,boolean hasFocus,int row,int column){
     JLabel lbl= new JLabel(value==null? "": value.toString());
     if(column>=0 && column<4 && row>=0){
         lbl.setHorizontalAlignment(SwingConstants.LEFT);
         lbl.setOpaque(true);
         lbl.setBackground(Color.green);
     }
      if(column==4 && !value.toString().equals("Actif(ve)")&& row>=0){
         lbl.setHorizontalAlignment(SwingConstants.LEFT);
         lbl.setOpaque(true);
         lbl.setForeground(Color.red);
     }
     return lbl;
 }
};
String[][] data={};
String[] head={"Matricules","Nom","Prénom","Type Employer","Status"};
String[] heads={"Date"};
DefaultTableModel md= new DefaultTableModel(data,head);
DefaultTableModel mds= new DefaultTableModel(data,heads);
tblpere.setModel(md);
tblfils.setModel(mds);
TableColumnModel cm= tblpere.getColumnModel();
        cm.getColumn(0).setPreferredWidth(30);
        cm.getColumn(1).setPreferredWidth(70);
        cm.getColumn(2).setPreferredWidth(100);
        cm.getColumn(2).setPreferredWidth(50);
        tblpere.getColumnModel().getColumn(0).setCellRenderer(render);
        tblpere.getColumnModel().getColumn(3).setCellRenderer(render);
        tblpere.getColumnModel().getColumn(4).setCellRenderer(render);

String lestado="";
      String query="select * from show_employes  order by nom asc";
    ResultSet rs=Mn.SEARCHDATA(query);
    try {
        md.setRowCount(0);
       while(rs.next()){
           if(rs.getBoolean("estatuses")==true){
              lestado="Actif(ve)"; 
           }else{
             lestado="Inactif(ve)";   
           }
         String[] vect={rs.getString("matricule"),rs.getString("nom"),rs.getString("prenom"),
        rs.getString("type_employer"),lestado};
         Mn.ADD_TABLE(md, vect);
        }
    } catch (SQLException ex) {
        Logger.getLogger(Cons_intelligente.class.getName()).log(Level.SEVERE, null, ex);
    }
//--------------------------------------------------------------------------------------------------
}

//----------------------- Pwogramasyon  metod ki nan Radio Paiement la---------------------
private void StructureBulletin(){
 ActivateControle();
 reyisi.setVisible(false);
reyisil.setVisible(false);
nonreyisi.setVisible(false);
nonreyisil.setVisible(false);
 recherche.setEnabled(false);
paiement.setSelected(false);
matiere.setSelected(false);
eleve.setSelected(false);
classe.setSelected(false);
employer.setSelected(false);
professeur.setSelected(false);
resultat.setSelected(false);
collection.setSelected(false);
bulletin.setSelected(true);
//-------------------------
borderaux.setSelected(false);


reservation.setSelected(false);


inscription.setSelected(false);

//----------------------------------------------------
section.setSelectedItem(null);
klas.setSelectedItem(null);
section.setEnabled(true);
klas.setEnabled(true);
//----------------------------------------------------------
String[][] data={};
String[] head={"Matricules","Nom","Prénom","Classes","Sections"};
String[] heads={" Matières"," Notes"," Sur"};
DefaultTableModel md= new DefaultTableModel(data,head);
DefaultTableModel mds= new DefaultTableModel(data,heads);
tblpere.setModel(md);
tblfils.setModel(mds);
TableColumnModel cm= tblpere.getColumnModel();
        cm.getColumn(0).setPreferredWidth(30);
        cm.getColumn(1).setPreferredWidth(70);
        cm.getColumn(2).setPreferredWidth(100);
        cm.getColumn(3).setPreferredWidth(50);
        cm.getColumn(4).setPreferredWidth(50);
//--------------------------------------------------------------------------------------------------
      String query="select * from show_resultats group by matricule order by nom asc";
    ResultSet rs=Mn.SEARCHDATA(query);
    try {
        md.setRowCount(0);
       while(rs.next()){
         String[] vect={rs.getString("matricule"),rs.getString("nom"),rs.getString("prenom"),rs.getString("classe"),rs.getString("section")};
        Mn.ADD_TABLE(md, vect);
        }
    } catch (SQLException ex) {
        Logger.getLogger(Cons_intelligente.class.getName()).log(Level.SEVERE, null, ex);
    }
//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------
}

//----------------------- Pwogramasyon  metod ki nan Radio Matiere la---------------------
private void StructureMatiere(){
    InactivateControle();
reyisi.setVisible(false);
reyisil.setVisible(false);
nonreyisi.setVisible(false);
nonreyisil.setVisible(false);
    recherche.setEnabled(false);
paiement.setSelected(false);
eleve.setSelected(false);
classe.setSelected(false);
employer.setSelected(false);
professeur.setSelected(false);
resultat.setSelected(false);
collection.setSelected(false);
bulletin.setSelected(false);
matiere.setSelected(true);
//-------------------------
borderaux.setSelected(false);


reservation.setSelected(false);


inscription.setSelected(false);

//----------------------------------------------------------
section.setSelectedItem(null);
klas.setSelectedItem(null);
section.setEnabled(true);
klas.setEnabled(true);
//----------------------------------------------------------
  TableCellRenderer render= new TableCellRenderer() {
 public Component getTableCellRendererComponent(JTable table,Object value,boolean idSelected,boolean hasFocus,int row,int column){
     JLabel lbl= new JLabel(value==null? "": value.toString());
    
      if(column==2 && !value.toString().equals("Active")&& row>=0){
         lbl.setHorizontalAlignment(SwingConstants.LEFT);
         lbl.setOpaque(true);
         lbl.setForeground(Color.red);
     }
     return lbl;
 }
};
String[][] data={};
String[] heads={" Classes"," Sections"};
String[] head={" Matières"," Sur"," Status"};
DefaultTableModel md= new DefaultTableModel(data,head);
DefaultTableModel mds= new DefaultTableModel(data,heads);
tblpere.setModel(md);
tblfils.setModel(mds);
tblpere.getColumnModel().getColumn(2).setCellRenderer(render);
//--------------------------------------------------------------------------------------------------
      String query="select * from show_matiere group by matiere order by matiere asc";
    ResultSet rs=Mn.SEARCHDATA(query);
    try {
        md.setRowCount(0);
       while(rs.next()){
         String[] vect={rs.getString("matiere"),rs.getString("valeur"),rs.getString("estatus")};
        Mn.ADD_TABLE(md, vect);
        }
    } catch (SQLException ex) {
        Logger.getLogger(Cons_intelligente.class.getName()).log(Level.SEVERE, null, ex);
    }
//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------
}

//----------------------- Pwogramasyon  metod ki nan Radio Borderaux la---------------------
private void StructureBorderaux(){
    InactivateControle();
reyisi.setVisible(false);
reyisil.setVisible(false);
nonreyisi.setVisible(false);
nonreyisil.setVisible(false);
paiement.setSelected(false);
eleve.setSelected(false);
classe.setSelected(false);
employer.setSelected(false);
professeur.setSelected(false);
resultat.setSelected(false);
collection.setSelected(false);
bulletin.setSelected(false);
matiere.setSelected(false);
//-------------------------
borderaux.setSelected(true);


reservation.setSelected(false);


inscription.setSelected(false);

//----------------------------------------------------------
section.setSelectedItem(null);
klas.setSelectedItem(null);
section.setEnabled(true);
klas.setEnabled(true);
recherche.setEnabled(true);
//----------------------------------------------------------
String[][] data={};
String[] head={" Classes"," Sections"};
String[] heads={" Ouvrages"," Auteurs"};
DefaultTableModel md= new DefaultTableModel(data,head);
DefaultTableModel mds= new DefaultTableModel(data,heads);
tblpere.setModel(md);
tblfils.setModel(mds);
//--------------------------------------------------------------------------------------------------
      String query="select o.description,o.auteur, cl.description as classes, cl.section from ouvrages o\n" +
"left join ouvrages_has_institution oi on oi.ouvrages_code=o.code\n" +
"left join institution i on i.idinstitution=oi.institution_idinstitution\n" +
"left join ouvrages_vs_matiere om on om.ouvrages_code=o.code \n" +
"left join matiere_vs_classe cm on om.code_matiere=cm.code_matiere \n" +
"left join classe cl on cl.code_classe=cm.code_classe group by classes order by classes asc";
    ResultSet rs=Mn.SEARCHDATA(query);
    try {
        md.setRowCount(0);
       while(rs.next()){
         String[] vect={rs.getString("classes"),rs.getString("section")};
        Mn.ADD_TABLE(md, vect);
        }
    } catch (SQLException ex) {
        Logger.getLogger(Cons_intelligente.class.getName()).log(Level.SEVERE, null, ex);
    }
//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------
}

//----------------------- Pwogramasyon  metod ki nan Radio Borderaux la---------------------
private void StructureInscris(){
    InactivateControle();
reyisi.setVisible(false);
reyisil.setVisible(false);
nonreyisi.setVisible(false);
nonreyisil.setVisible(false);
paiement.setSelected(false);
eleve.setSelected(false);
classe.setSelected(false);
employer.setSelected(false);
professeur.setSelected(false);
resultat.setSelected(false);
collection.setSelected(false);
bulletin.setSelected(false);
matiere.setSelected(false);
//-------------------------
borderaux.setSelected(false);
reservation.setSelected(false);

inscription.setSelected(true);

//----------------------------------------------------------
section.setSelectedItem(null);
klas.setSelectedItem(null);
section.setEnabled(true);
klas.setEnabled(true);
recherche.setEnabled(true);
//----------------------------------------------------------
String[][] data={};
String[] head={" Classes"," Sections"};
String[] heads={"Matricule","Nom","Prénom","Téléphone","Status"};
DefaultTableModel md= new DefaultTableModel(data,head);
DefaultTableModel mds= new DefaultTableModel(data,heads);
tblpere.setModel(md);
tblfils.setModel(mds);
//--------------------------------------------------------------------------------------------------
      String query="select description as classes, section from classe  group by classes order by classes asc";
    ResultSet rs=Mn.SEARCHDATA(query);
    try {
        md.setRowCount(0);
       while(rs.next()){
         String[] vect={rs.getString("classes"),rs.getString("section")};
        Mn.ADD_TABLE(md, vect);
        }
    } catch (SQLException ex) {
        Logger.getLogger(Cons_intelligente.class.getName()).log(Level.SEVERE, null, ex);
    }
//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------
}

private void StructureReservation(){
    InactivateControle();
reyisi.setVisible(false);
reyisil.setVisible(false);
nonreyisi.setVisible(false);
nonreyisil.setVisible(false);
paiement.setSelected(false);
eleve.setSelected(false);
classe.setSelected(false);
employer.setSelected(false);
professeur.setSelected(false);
resultat.setSelected(false);
collection.setSelected(false);
bulletin.setSelected(false);
matiere.setSelected(false);
//-------------------------
borderaux.setSelected(false);
reservation.setSelected(true);
inscription.setSelected(false);

//----------------------------------------------------------
section.setSelectedItem(null);
klas.setSelectedItem(null);
section.setEnabled(true);
klas.setEnabled(true);
recherche.setEnabled(true);
//----------------------------------------------------------
String[][] data={};
String[] head={"  Classes","  Sections"};
String[] heads={" Nom"," Prénom","Téléphone"," Adresse"," Pres. Responsable","Téléphone","Lien Parenté"};
DefaultTableModel md= new DefaultTableModel(data,head);
DefaultTableModel mds= new DefaultTableModel(data,heads);
tblpere.setModel(md);
tblfils.setModel(mds);
//--------------------------------------------------------------------------------------------------
      String query="select description as classes, section from classe  group by classes order by classes asc";
    ResultSet rs=Mn.SEARCHDATA(query);
    try {
        md.setRowCount(0);
       while(rs.next()){
         String[] vect={rs.getString("classes"),rs.getString("section")};
        Mn.ADD_TABLE(md, vect);
        }
    } catch (SQLException ex) {
        Logger.getLogger(Cons_intelligente.class.getName()).log(Level.SEVERE, null, ex);
    }
//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------
}




    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        recherche = new org.edisoncor.gui.textField.TextFieldRectBackground();
        jSeparator1 = new javax.swing.JSeparator();
        panelocultar = new javax.swing.JPanel();
        controle1 = new javax.swing.JRadioButton();
        controle2 = new javax.swing.JRadioButton();
        controle3 = new javax.swing.JRadioButton();
        jSeparator4 = new javax.swing.JSeparator();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jSeparator6 = new javax.swing.JSeparator();
        jLabel9 = new javax.swing.JLabel();
        Imprimer = new javax.swing.JButton();
        annuler = new javax.swing.JButton();
        section = new javax.swing.JComboBox();
        klas = new javax.swing.JComboBox();
        academique = new javax.swing.JComboBox();
        jSeparator2 = new javax.swing.JSeparator();
        eleve = new javax.swing.JRadioButton();
        classe = new javax.swing.JRadioButton();
        employer = new javax.swing.JRadioButton();
        professeur = new javax.swing.JRadioButton();
        resultat = new javax.swing.JRadioButton();
        collection = new javax.swing.JRadioButton();
        paiement = new javax.swing.JRadioButton();
        matiere = new javax.swing.JRadioButton();
        bulletin = new javax.swing.JRadioButton();
        jSeparator3 = new javax.swing.JSeparator();
        qut = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        aux = new javax.swing.JLabel();
        reyisi = new javax.swing.JLabel();
        reyisil = new javax.swing.JLabel();
        nonreyisi = new javax.swing.JLabel();
        nonreyisil = new javax.swing.JLabel();
        borderaux = new javax.swing.JRadioButton();
        inscription = new javax.swing.JRadioButton();
        reservation = new javax.swing.JRadioButton();
        jPanel1 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tblpere = new javax.swing.JTable(){
            public boolean isCellEditable(int rowIndex, int colIndex){
                return false;
            }
        };
        jLabel2 = new javax.swing.JLabel();
        recherche1 = new org.edisoncor.gui.textField.TextFieldRectBackground();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblfils = new javax.swing.JTable(){
            public boolean isCellEditable(int rowIndex, int colIndex){
                return false;
            }
        };
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu5 = new javax.swing.JMenu();
        jMenuItem1 = new javax.swing.JMenuItem();

        setClosable(true);
        setIconifiable(true);

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel1.setText("Recherche");
        jLabel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        recherche.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        recherche.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                rechercheKeyReleased(evt);
            }
        });

        panelocultar.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        controle1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        controle1.setText("1er Controle");
        controle1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                controle1MouseClicked(evt);
            }
        });

        controle2.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        controle2.setText("2ème Controle");
        controle2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                controle2MouseClicked(evt);
            }
        });

        controle3.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        controle3.setText("3ème Controle");
        controle3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                controle3MouseClicked(evt);
            }
        });

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel5.setText("Classe: ");
        jLabel5.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel6.setText("Section:");
        jLabel6.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel9.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel9.setText("Année Académique");
        jLabel9.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        Imprimer.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        Imprimer.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/Imprimir.png"))); // NOI18N
        Imprimer.setText("IMPRIMER");
        Imprimer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ImprimerActionPerformed(evt);
            }
        });

        annuler.setBackground(new java.awt.Color(255, 255, 255));
        annuler.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        annuler.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/actualizar.jpg"))); // NOI18N
        annuler.setText("ACTUALISER");
        annuler.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                annulerActionPerformed(evt);
            }
        });

        section.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        section.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Selectionnez", "Petit", "Primaire", "Secondaire" }));
        section.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                sectionItemStateChanged(evt);
            }
        });
        section.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                sectionFocusLost(evt);
            }
        });

        klas.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N

        academique.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N

        javax.swing.GroupLayout panelocultarLayout = new javax.swing.GroupLayout(panelocultar);
        panelocultar.setLayout(panelocultarLayout);
        panelocultarLayout.setHorizontalGroup(
            panelocultarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSeparator4)
            .addComponent(jSeparator6, javax.swing.GroupLayout.Alignment.TRAILING)
            .addGroup(panelocultarLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelocultarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(Imprimer, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(panelocultarLayout.createSequentialGroup()
                        .addGroup(panelocultarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(6, 6, 6)
                        .addGroup(panelocultarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(section, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(klas, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(18, 18, 18)
                        .addGroup(panelocultarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(academique, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(panelocultarLayout.createSequentialGroup()
                        .addComponent(controle1)
                        .addGap(10, 10, 10)
                        .addComponent(controle2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(controle3)
                        .addGap(0, 109, Short.MAX_VALUE))
                    .addComponent(annuler, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        panelocultarLayout.setVerticalGroup(
            panelocultarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelocultarLayout.createSequentialGroup()
                .addGap(8, 8, 8)
                .addGroup(panelocultarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(controle1)
                    .addComponent(controle2)
                    .addComponent(controle3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(panelocultarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelocultarLayout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(panelocultarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(section, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelocultarLayout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel9)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)))
                .addGroup(panelocultarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(academique, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(panelocultarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(klas, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(Imprimer, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(annuler, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        eleve.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        eleve.setText("Elèves");
        eleve.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                eleveMouseClicked(evt);
            }
        });

        classe.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        classe.setText("Classes");
        classe.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                classeMouseClicked(evt);
            }
        });

        employer.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        employer.setText("Employers");
        employer.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                employerMouseClicked(evt);
            }
        });

        professeur.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        professeur.setText("Professeurs");
        professeur.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                professeurMouseClicked(evt);
            }
        });

        resultat.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        resultat.setText("Resultats");
        resultat.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                resultatMouseClicked(evt);
            }
        });

        collection.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        collection.setText("Collections");
        collection.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                collectionMouseClicked(evt);
            }
        });

        paiement.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        paiement.setText("Paiements");
        paiement.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                paiementMouseClicked(evt);
            }
        });

        matiere.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        matiere.setText("Matières");
        matiere.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                matiereMouseClicked(evt);
            }
        });

        bulletin.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        bulletin.setText("Relvés de Notes");
        bulletin.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                bulletinMouseClicked(evt);
            }
        });

        qut.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        qut.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        qut.setText("0");
        qut.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel13.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel13.setForeground(new java.awt.Color(0, 0, 153));
        jLabel13.setText("|");
        jLabel13.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        aux.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        aux.setText("Recherche");
        aux.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        reyisi.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        reyisi.setForeground(new java.awt.Color(0, 102, 51));
        reyisi.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        reyisi.setText("0");
        reyisi.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        reyisil.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        reyisil.setForeground(new java.awt.Color(0, 102, 51));
        reyisil.setText("Elèves Réussir");
        reyisil.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        nonreyisi.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        nonreyisi.setForeground(new java.awt.Color(204, 51, 0));
        nonreyisi.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        nonreyisi.setText("0");
        nonreyisi.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        nonreyisil.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        nonreyisil.setForeground(new java.awt.Color(204, 51, 0));
        nonreyisil.setText("Elèves non Réussir");
        nonreyisil.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        borderaux.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        borderaux.setText("Borderaux");
        borderaux.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                borderauxMouseClicked(evt);
            }
        });

        inscription.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        inscription.setText("Inscription Par Classe");
        inscription.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                inscriptionMouseClicked(evt);
            }
        });

        reservation.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        reservation.setText("Réservation");
        reservation.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                reservationMouseClicked(evt);
            }
        });

        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        tblpere.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tblpere.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblpereMouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(tblpere);

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel2.setText("Recherche: ");
        jLabel2.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        recherche1.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        recherche1.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        recherche1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                recherche1KeyReleased(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(7, 7, 7)
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(recherche1, javax.swing.GroupLayout.PREFERRED_SIZE, 504, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(43, Short.MAX_VALUE))
            .addComponent(jScrollPane2, javax.swing.GroupLayout.Alignment.TRAILING)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(recherche1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, 32, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 202, Short.MAX_VALUE))
        );

        jPanel2.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        tblfils.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tblfils);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 1108, Short.MAX_VALUE)
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 202, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        jMenu5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/fichier.jpg"))); // NOI18N
        jMenu5.setText("Edition");
        jMenu5.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N

        jMenuItem1.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F1, 0));
        jMenuItem1.setText("Aide");
        jMenu5.add(jMenuItem1);

        jMenuBar1.add(jMenu5);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSeparator3, javax.swing.GroupLayout.PREFERRED_SIZE, 1112, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addGroup(layout.createSequentialGroup()
                .addComponent(panelocultar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(7, 7, 7)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 382, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 1112, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addGroup(layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(qut, javax.swing.GroupLayout.PREFERRED_SIZE, 72, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(8, 8, 8)
                        .addComponent(aux)
                        .addGap(24, 24, 24)
                        .addComponent(reyisi, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(6, 6, 6)
                        .addComponent(reyisil)
                        .addGap(19, 19, 19)
                        .addComponent(nonreyisi, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(6, 6, 6)
                        .addComponent(nonreyisil)
                        .addGap(38, 38, 38)
                        .addComponent(jLabel13)
                        .addGap(5, 5, 5)
                        .addComponent(jLabel1)
                        .addGap(7, 7, 7)
                        .addComponent(recherche, javax.swing.GroupLayout.PREFERRED_SIZE, 400, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(eleve)
                                .addGap(18, 18, 18)
                                .addComponent(classe)
                                .addGap(9, 9, 9))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addComponent(inscription)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)))
                        .addComponent(employer)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(professeur)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(resultat)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(collection)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(paiement)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bulletin)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(matiere)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(borderaux)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(reservation))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(19, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(eleve)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(classe)
                        .addComponent(employer)
                        .addComponent(professeur)
                        .addComponent(resultat)
                        .addComponent(collection)
                        .addComponent(paiement)
                        .addComponent(bulletin)
                        .addComponent(matiere)
                        .addComponent(borderaux)
                        .addComponent(reservation)))
                .addComponent(inscription)
                .addGap(4, 4, 4)
                .addComponent(jSeparator3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(panelocultar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(6, 6, 6)
                .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 0, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(2, 2, 2)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(qut, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(aux, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(reyisi, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(reyisil, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(nonreyisi, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(nonreyisil, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(recherche, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void rechercheKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_rechercheKeyReleased
        recherche.setText(recherche.getText().toUpperCase());
        if(classe.isSelected()){
            recherche.setEnabled(true);
 TableCellRenderer render= new TableCellRenderer() {
 public Component getTableCellRendererComponent(JTable table,Object value,boolean idSelected,boolean hasFocus,int row,int column){
     JLabel lbl= new JLabel(value==null? "": value.toString());
     if(column==0 && row>=0){
         lbl.setHorizontalAlignment(SwingConstants.LEFT);
         lbl.setOpaque(true);
         lbl.setBackground(Color.green);
     }
if(column==4 && row>=0){
         lbl.setHorizontalAlignment(SwingConstants.LEFT);
         lbl.setOpaque(true);
         lbl.setBackground(Color.yellow);
     }
if(column==5 && row>=0){
         lbl.setHorizontalAlignment(SwingConstants.LEFT);
         lbl.setOpaque(true);
         lbl.setBackground(Color.yellow);
     }
     return lbl;
 }
};
         String mat=tblpere.getValueAt(tblpere.getSelectedRow(),0).toString();
         String[][] data={};
         String[] heads={"Matricule"," Nom"," Prénom"," Adresse"," Pres. Responsable","Téléphone","Lien Parenté"};   
         DefaultTableModel mds= new DefaultTableModel(data,heads);
         tblfils.setModel(mds);
         TableColumnModel cmf= tblfils.getColumnModel();
         cmf.getColumn(0).setPreferredWidth(30);
         cmf.getColumn(1).setPreferredWidth(80);
         cmf.getColumn(2).setPreferredWidth(100);
         cmf.getColumn(3).setPreferredWidth(150);
         cmf.getColumn(4).setPreferredWidth(70);
         cmf.getColumn(5).setPreferredWidth(40);
         cmf.getColumn(6).setPreferredWidth(10);
         tblfils.getColumnModel().getColumn(0).setCellRenderer(render);
         tblfils.getColumnModel().getColumn(4).setCellRenderer(render);
         tblfils.getColumnModel().getColumn(5).setCellRenderer(render);
         String lestado="";
         String query="";
         if(recherche.getText().isEmpty()){
           query="select * from show_inscription where  classe='"+mat+"' and annee='"+academique.getSelectedItem().toString()+"'";  
         }else{
         query="select * from show_inscription where classe='"+mat+"' and annee='"+academique.getSelectedItem().toString()+"' and"
                 + " matricule like '%"+recherche.getText()+"%' or nom like '%"+recherche.getText()+"%' "
                 + " or prenom like '%"+recherche.getText()+"%' or adresse like '%"+recherche.getText()+"%' or responsable "
                 + " like '%"+recherche.getText()+"%' or lien_parente like '%"+recherche.getText()+"%'";
         }
         ResultSet rs=Mn.SEARCHDATA(query);
         try {
         mds.setRowCount(0);
         while(rs.next()){
           String[] vect={rs.getString("matricule"),rs.getString("nom"),rs.getString("prenom"),
           rs.getString("adresse"),rs.getString("responsable"),rs.getString("telephone"),rs.getString("lien_parente")};
           Mn.ADD_TABLE(mds, vect);
          }
         } catch (SQLException ex) {
        Logger.getLogger(Cons_intelligente.class.getName()).log(Level.SEVERE, null, ex);
        }  
            
        }else if(professeur.isSelected()){
            recherche.setEnabled(true);
TableCellRenderer render= new TableCellRenderer() {
 public Component getTableCellRendererComponent(JTable table,Object value,boolean idSelected,boolean hasFocus,int row,int column){
     JLabel lbl= new JLabel(value==null? "": value.toString());
     if(column==5 && row>=0){
         lbl.setHorizontalAlignment(SwingConstants.LEFT);
         lbl.setOpaque(true);
         lbl.setBackground(Color.yellow);
     }

     return lbl;
 }
};
       String mat=tblpere.getValueAt(tblpere.getSelectedRow(),0).toString();
            String[][] data={};
String[] heads={"Matières"," Classes"," Sections","Jours","Heures","Horaire"};
DefaultTableModel mds= new DefaultTableModel(data,heads);
tblfils.setModel(mds);
TableColumnModel cmf= tblfils.getColumnModel();
cmf.getColumn(0).setPreferredWidth(90);
cmf.getColumn(1).setPreferredWidth(70);
cmf.getColumn(2).setPreferredWidth(70);
cmf.getColumn(3).setPreferredWidth(70);
cmf.getColumn(4).setPreferredWidth(70);
tblfils.getColumnModel().getColumn(5).setCellRenderer(render);
//--------------------------------------------------------------------------------------------------
String lestado="";
String estado="";
      String query="select h.code_horraire AS matricules,h.matiere AS matiere,h.classe AS classe,c.section AS section,h.jour \n" +
"AS jour,h.nbre_heure AS nbre_heure,h.devise AS devise,ph.matricule,p.heure,h.heure_in,h.heure_out from (horaire h left join classe c \n" +
"on((c.description = h.classe)) left join professeur_has_horaire ph on(ph.code_horraire=h.code_horraire) left join professeur p \n" +
"on(p.matricule=ph.matricule)) where ph.matricule='"+mat+"' and h.matiere like '%"+recherche.getText()+"%' or "
              + "h.classe like '%"+recherche.getText()+"%' or c.section like '%"+recherche.getText()+"%' or h.jour like '%"+recherche.getText()+"%' group by heure_in order by matiere asc";
    ResultSet rs=Mn.SEARCHDATA(query);
    try {
        mds.setRowCount(0);
       while(rs.next()){
           lestado=rs.getString("nbre_heure")+" "+rs.getString("devise");
           estado=rs.getString("heure_in")+" "+rs.getString("heure_out");
        String[] vect={rs.getString("matiere"),rs.getString("classe"),rs.getString("section"),rs.getString("jour"),lestado,estado};  
        Mn.ADD_TABLE(mds, vect);
        }
    } catch (SQLException ex) {
        Logger.getLogger(Cons_intelligente.class.getName()).log(Level.SEVERE, null, ex);
    }
            
        }else if(resultat.isSelected()){
            recherche.setEnabled(true);
TableCellRenderer render= new TableCellRenderer() {
 public Component getTableCellRendererComponent(JTable table,Object value,boolean idSelected,boolean hasFocus,int row,int column){
     JLabel lbl= new JLabel(value==null? "": value.toString());
     if(column==0 && row>=0){
         lbl.setHorizontalAlignment(SwingConstants.LEFT);
         lbl.setOpaque(true);
         lbl.setBackground(Color.green);
     }
     if(column==6 && row>=0){
         lbl.setHorizontalAlignment(SwingConstants.LEFT);
         lbl.setOpaque(true);
         lbl.setBackground(Color.green);
     }
     if(column==8 && row>=0){
         lbl.setHorizontalAlignment(SwingConstants.LEFT);
         lbl.setOpaque(true);
         lbl.setBackground(Color.green);
     }
      if(column==7 && value.toString().equals("Mal.") && row>=0){
         lbl.setHorizontalAlignment(SwingConstants.LEFT);
         lbl.setOpaque(true);
         lbl.setForeground(Color.red);
     }
    
     return lbl;
 }
};
  String mat=tblpere.getValueAt(tblpere.getSelectedRow(),0).toString();
  int aa=0;
if(controle1.isSelected()){
    aa=1;
}else if(controle2.isSelected()){
    aa=2;
}else if(controle3.isSelected()){
    aa=3;
}
String[][] data={};
String[] heads={"Matricules"," Nom"," Prénom","Controle","Total Notes","Sur","Moyenne","Qualification","Moy. Générale","Decision"};
DefaultTableModel mds= new DefaultTableModel(data,heads);
tblfils.setModel(mds);
TableColumnModel cmf= tblfils.getColumnModel();
cmf.getColumn(0).setPreferredWidth(30);
cmf.getColumn(1).setPreferredWidth(70);
cmf.getColumn(2).setPreferredWidth(90);
cmf.getColumn(3).setPreferredWidth(5);
cmf.getColumn(4).setPreferredWidth(5);
cmf.getColumn(5).setPreferredWidth(5);
cmf.getColumn(6).setPreferredWidth(5);
cmf.getColumn(7).setPreferredWidth(30);
cmf.getColumn(8).setPreferredWidth(5);
cmf.getColumn(9).setPreferredWidth(50);
tblfils.getColumnModel().getColumn(0).setCellRenderer(render);
tblfils.getColumnModel().getColumn(6).setCellRenderer(render);
tblfils.getColumnModel().getColumn(7).setCellRenderer(render);
tblfils.getColumnModel().getColumn(8).setCellRenderer(render);

//--------------------------------------------------------------------------------------------------
 String query="select * from show_resultats where classe='"+mat+"' and controle="+aa+" and annee='"+academique.getSelectedItem().toString()+"'"
              + " and matricule like '%"+recherche.getText()+"%' or nom like '%"+recherche.getText()+"%' or prenom like '%"+recherche.getText()+"%'"
         + " or qualification like '%"+recherche.getText()+"%' group by matricule order by nom asc";
    ResultSet rs=Mn.SEARCHDATA(query);
    try {
        mds.setRowCount(0);
       while(rs.next()){
          String[] vect={rs.getString("matricule"),rs.getString("nom"),rs.getString("prenom"),rs.getString("controle"),
              rs.getString("total"),rs.getString("points"),rs.getString("moyenne"),rs.getString("qualification"),rs.getString("generale")
                  ,rs.getString("decision")};
        Mn.ADD_TABLE(mds, vect);
        }
    } catch (SQLException ex) {
        Logger.getLogger(Cons_intelligente.class.getName()).log(Level.SEVERE, null, ex);
    }
//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------
            
        }else if(collection.isSelected()){
           recherche.setEnabled(true);
 TableCellRenderer render= new TableCellRenderer() {
 public Component getTableCellRendererComponent(JTable table,Object value,boolean idSelected,boolean hasFocus,int row,int column){
     JLabel lbl= new JLabel(value==null? "": value.toString());
     if(column==0 && row>=0){
         lbl.setHorizontalAlignment(SwingConstants.LEFT);
         lbl.setOpaque(true);
         lbl.setBackground(Color.green);
     }
     if(column==5 && row>=0){
         lbl.setHorizontalAlignment(SwingConstants.LEFT);
         lbl.setOpaque(true);
         lbl.setBackground(Color.yellow);
     }
      if(column==5 && value.toString().equals("0 Gdes") && row>=0){
         lbl.setHorizontalAlignment(SwingConstants.LEFT);
         lbl.setOpaque(true);
         lbl.setBackground(Color.red);
     }
     return lbl;
 }
};
 String mat=tblpere.getValueAt(tblpere.getSelectedRow(),0).toString();
 String[][] data={};
String[] heads={"Matricules"," Nom"," Prénom","Versement","Date","Balance"};
DefaultTableModel mds= new DefaultTableModel(data,heads);
tblfils.setModel(mds);
TableColumnModel cmf= tblfils.getColumnModel();
cmf.getColumn(0).setPreferredWidth(30);
cmf.getColumn(1).setPreferredWidth(70);
cmf.getColumn(2).setPreferredWidth(100);
cmf.getColumn(3).setPreferredWidth(5);
cmf.getColumn(4).setPreferredWidth(150);
cmf.getColumn(5).setPreferredWidth(30);
tblfils.getColumnModel().getColumn(0).setCellRenderer(render);
tblfils.getColumnModel().getColumn(5).setCellRenderer(render);
//--------------------------------------------------------------------------------------------------
String query="";
if(recherche.getText().isEmpty()){
   query="select * from show_collecter where annee='"+academique.getSelectedItem().toString()+"' and classe='"+mat+"' order by nom asc"; 
}else{
      query="select * from show_collecter where annee='"+academique.getSelectedItem().toString()+"' and classe='"+mat+"'"
              + " and matricule like '%"+recherche.getText()+"%' or nom  like '%"+recherche.getText()+"%' or "
              + "prenom like '%"+recherche.getText()+"%'or date  like '%"+recherche.getText()+"%' or "
              + "balance  like '%"+recherche.getText()+"%' order by nom asc";
}
    ResultSet rs=Mn.SEARCHDATA(query);
    try {
        mds.setRowCount(0);
       while(rs.next()){
        String[] vect={rs.getString("matricule"),rs.getString("nom"),rs.getString("prenom"),rs.getString("versement")+" Gdes",
        rs.getString("date"),rs.getString("balance")+" Gdes"};
        Mn.ADD_TABLE(mds, vect);
        }
    } catch (SQLException ex) {
        Logger.getLogger(Cons_intelligente.class.getName()).log(Level.SEVERE, null, ex);
    }
//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------
        }else if(borderaux.isSelected()){
            String[][] data={};
String[] head={" Ouvrages"," Auteurs"};
DefaultTableModel md= new DefaultTableModel(data,head);
tblfils.setModel(md);
//--------------------------------------------------------------------------------------------------
      String query="select o.description,o.auteur, cl.description as classes, cl.section from ouvrages o\n" +
"left join ouvrages_has_institution oi on oi.ouvrages_code=o.code\n" +
"left join institution i on i.idinstitution=oi.institution_idinstitution\n" +
"left join ouvrages_vs_matiere om on om.ouvrages_code=o.code \n" +
"left join matiere_vs_classe cm on om.code_matiere=cm.code_matiere \n" +
"left join classe cl on cl.code_classe=cm.code_classe where o.description like '%"+recherche.getText()+"%' or "
              + " o.auteur like '%"+recherche.getText()+"%' group by description order by description asc";
    ResultSet rs=Mn.SEARCHDATA(query);
    try {
        md.setRowCount(0);
       while(rs.next()){
         String[] vect={rs.getString("description"),rs.getString("auteur")};
        Mn.ADD_TABLE(md, vect);
        }
    } catch (SQLException ex) {
        Logger.getLogger(Cons_intelligente.class.getName()).log(Level.SEVERE, null, ex);
    }
            
    }else if(inscription.isSelected()){
        String lestado=null;
     String[][] data={};
     String[] heads={"Matricule","Nom","Prénom","Téléphone","Status"};
     DefaultTableModel mds= new DefaultTableModel(data,heads);
     tblfils.setModel(mds);
//--------------------------------------------------------------------------------------------------
      String query="select i.matricule,t.nom,t.prenom,tt.phone_number,e.estatus from inscription i left join tercero t\n" +
"on (t.code=i.code and t.code_tercero=i.code_tercero) left join tercero_vs_telephones tt on \n" +
"(tt.code=t.code and tt.code_tercero=t.code_tercero) left join eleve e on (e.code=i.code and e.code_tercero=i.code_tercero)\n" +
"left join classe c on c.code_classe=i.code_classe where i.matricule like '%"+recherche.getText()+"%' or t.nom like '%"+recherche.getText()+"%'"
 + " or t.prenom like '%"+recherche.getText()+"%' or tt.phone_number like '%"+recherche.getText()+"%' order by nom asc";
    ResultSet rs=Mn.SEARCHDATA(query);
    try {
        mds.setRowCount(0);
       while(rs.next()){
          if(rs.getBoolean("estatus")==true){
              lestado="Actif(ve)"; 
           }else{
             lestado="Inactif(ve)";   
           } 
         String[] vect={rs.getString("matricule"),rs.getString("nom"),rs.getString("prenom"),rs.getString("phone_number"),lestado};
        Mn.ADD_TABLE(mds, vect);
        }
    } catch (SQLException ex) {
        Logger.getLogger(Cons_intelligente.class.getName()).log(Level.SEVERE, null, ex);
    }
     qut.setVisible(true);
     aux.setVisible(true);
     qut.setText(String.valueOf(tblfils.getRowCount()));
     aux.setText("Éleves");
//--------------------------------------------------------------------------------------------------
    }else if(reservation.isSelected()){
        String lestado=null;
     String[][] data={};
     String[] heads={" Nom"," Prénom","Téléphone"," Adresse"," Pres. Responsable","Téléphone","Lien Parenté"};
     DefaultTableModel mds= new DefaultTableModel(data,heads);
     tblfils.setModel(mds);
//--------------------------------------------------------------------------------------------------
       String query="select r.nom,r.prenom,r.sexe,r.adresse,r.telephone,r.sang,r.naissance,r.sante,r.responsable,r.telephone_res,r.lien,r.photo,\n" +
"r.estatus,c.description as classe,c.section from reservation r left join reservation_vs_classe rc on rc.code_reservation=r.code_reservation left join classe c \n" +
"on c.code_classe=rc.code_classe where c.description='"+klas.getSelectedItem().toString()+"' order by nom asc";
    ResultSet rs=Mn.SEARCHDATA(query);
    try {
        mds.setRowCount(0);
       while(rs.next()){
         String[] vect={rs.getString("nom"),rs.getString("prenom"),rs.getString("telephone"),rs.getString("adresse"),rs.getString("responsable")
         ,rs.getString("telephone_res"),rs.getString("lien")};
        Mn.ADD_TABLE(mds, vect);
        }
    } catch (SQLException ex) {
        Logger.getLogger(Cons_intelligente.class.getName()).log(Level.SEVERE, null, ex);
    }
     qut.setVisible(true);
     aux.setVisible(true);
     qut.setText(String.valueOf(tblfils.getRowCount()));
     aux.setText("Réservation(s)");
//--------------------------------------------------------------------------------------------------
    }
    }//GEN-LAST:event_rechercheKeyReleased

    private void recherche1KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_recherche1KeyReleased
      recherche1.setText(recherche1.getText().toUpperCase());
        if(eleve.isSelected()){
           
 TableCellRenderer render= new TableCellRenderer() {
 public Component getTableCellRendererComponent(JTable table,Object value,boolean idSelected,boolean hasFocus,int row,int column){
     JLabel lbl= new JLabel(value==null? "": value.toString());
     if(column>=0 && column<4 && row>=0){
         lbl.setHorizontalAlignment(SwingConstants.LEFT);
         lbl.setOpaque(true);
         lbl.setBackground(Color.green);
     }
      if(column==4 && !value.toString().equals("Actif(ve)")&& row>=0){
         lbl.setHorizontalAlignment(SwingConstants.LEFT);
         lbl.setOpaque(true);
         lbl.setForeground(Color.red);
     }
     return lbl;
 }
};
String[][] data={};
String[] head={"Matricule","Nom","Prénom","Téléphone","Status"};
String[] heads={"Date de Naissance"," Sèxe"," Adresse"," Santé"," Grpe. Sanguin"," Pres. Responsable","Téléphone","Lien Parenté"};
DefaultTableModel md= new DefaultTableModel(data,head);
DefaultTableModel mds= new DefaultTableModel(data,heads);
tblpere.setModel(md);
tblfils.setModel(mds);
TableColumnModel cm= tblpere.getColumnModel();
        cm.getColumn(0).setPreferredWidth(30);
        cm.getColumn(1).setPreferredWidth(70);
        cm.getColumn(2).setPreferredWidth(78);
        cm.getColumn(3).setPreferredWidth(40);
        cm.getColumn(4).setPreferredWidth(10);
        tblpere.getColumnModel().getColumn(0).setCellRenderer(render);
        tblpere.getColumnModel().getColumn(3).setCellRenderer(render);
        tblpere.getColumnModel().getColumn(4).setCellRenderer(render);
TableColumnModel cmf= tblfils.getColumnModel();
cmf.getColumn(0).setPreferredWidth(90);
cmf.getColumn(1).setPreferredWidth(5);
cmf.getColumn(2).setPreferredWidth(100);
cmf.getColumn(3).setPreferredWidth(20);
cmf.getColumn(4).setPreferredWidth(70);
cmf.getColumn(5).setPreferredWidth(40);
cmf.getColumn(6).setPreferredWidth(10);

//--------------------------------------------------------------------------------------------------
String lestado="";
      String query="select distinct t.code AS code,t.code_tercero AS code_tercero,t.prenom AS prenom,t.nom AS nom,pd.sexe AS sexe,\n" +
"pd.adresse AS adresse,e.matricule AS matricule,pd.date_naissance AS naissance,e.cond_sante AS cond_sante,\n" +
"(select show_eleve_responsable.responsable AS responsable from show_eleve_responsable \n" +
"where (eht.eleve_matricule = show_eleve_responsable.matricule)) AS responsable,eht.lien_parente AS lien_parente,\n" +
"s.sang AS sang,e.estatus AS estatus,i.nom AS institution,tvt.phone_number as telephone,tv.phone_number as tele from ((((((((eleve e left join tercero t\n" +
"on(((e.code = t.code) and (e.code_tercero = t.code_tercero)))) left join personal_data pd on(((t.code = pd.code)\n" +
"and (t.code_tercero = pd.code_tercero)))) left join tercero_vs_telephones tvt on(((t.code = tvt.code) \n" +
"and (t.code_tercero = tvt.code_tercero)))) left join eleve_has_tercero_responsable eht \n" +
"on((e.matricule = eht.eleve_matricule))) left join tercero_email te on(((t.code = te.code) and \n" +
"(t.code_tercero = te.code_tercero)))) left join sanguins s on(((t.code = s.code) and\n" +
"(t.code_tercero = s.code_tercero)))) left join eleve_has_institution ehi on(((e.matricule = ehi.matricule) and\n" +
"(e.code = ehi.code) and (e.code_tercero = ehi.code_tercero)))) \n" +
"left join institution i on((i.idinstitution = ehi.idinstitution)) \n" +
"left join tercero_vs_telephones tv on (((eht.code = tv.code) \n" +
"and (eht.code_tercero = tv.code_tercero)))) where e.matricule like '%"+recherche1.getText()+"%' or t.nom like '%"+recherche1.getText()+"%'"
              + " or t.prenom like '%"+recherche1.getText()+"%' order by nom asc";
    ResultSet rs=Mn.SEARCHDATA(query);
    try {
        md.setRowCount(0);
       while(rs.next()){
           if(rs.getBoolean("estatus")==true){
              lestado="Actif(ve)"; 
           }else{
             lestado="Inactif(ve)";   
           }
         String[] vect={rs.getString("matricule"),rs.getString("nom"),rs.getString("prenom"),
        rs.getString("telephone"),lestado};
         Mn.ADD_TABLE(md, vect);
        }
    } catch (SQLException ex) {
        Logger.getLogger(Cons_intelligente.class.getName()).log(Level.SEVERE, null, ex);
    }
//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------
            
        }else if(classe.isSelected()){
 String[][] data={};
String[] head={"  Classes","   Sections"};
String[] heads={" Matricule"," Nom"," Prénom"," Adresse"," Pres. Responsable","Téléphone","Lien Parenté"};
DefaultTableModel md= new DefaultTableModel(data,head);
DefaultTableModel mds= new DefaultTableModel(data,heads);
tblpere.setModel(md);
tblfils.setModel(mds);
TableColumnModel cmf= tblfils.getColumnModel();
cmf.getColumn(0).setPreferredWidth(30);
cmf.getColumn(1).setPreferredWidth(80);
cmf.getColumn(2).setPreferredWidth(100);
cmf.getColumn(3).setPreferredWidth(150);
cmf.getColumn(4).setPreferredWidth(70);
cmf.getColumn(5).setPreferredWidth(40);
cmf.getColumn(6).setPreferredWidth(10);

//--------------------------------------------------------------------------------------------------
      String query="select * from show_classe where classe like '%"+recherche1.getText()+"%' or section like '%"+recherche1.getText()+"%'";
    ResultSet rs=Mn.SEARCHDATA(query);
    try {
        md.setRowCount(0);
       while(rs.next()){
         String[] vect={rs.getString("classe"),rs.getString("section")};
         Mn.ADD_TABLE(md, vect);
        }
    } catch (SQLException ex) {
        Logger.getLogger(Cons_intelligente.class.getName()).log(Level.SEVERE, null, ex);
    }
//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------
            
        }else if(employer.isSelected()){
 TableCellRenderer render= new TableCellRenderer() {
 public Component getTableCellRendererComponent(JTable table,Object value,boolean idSelected,boolean hasFocus,int row,int column){
     JLabel lbl= new JLabel(value==null? "": value.toString());
     if(column>=0 && column<4 && row>=0){
         lbl.setHorizontalAlignment(SwingConstants.LEFT);
         lbl.setOpaque(true);
         lbl.setBackground(Color.green);
     }
      if(column==4 && !value.toString().equals("Actif(ve)")&& row>=0){
         lbl.setHorizontalAlignment(SwingConstants.LEFT);
         lbl.setOpaque(true);
         lbl.setForeground(Color.red);
     }
     return lbl;
 }
};
String[][] data={};
String[] head={"Matricule","Nom","Prénom","Type Employer","Status"};
String[] heads={"Date de Naissance"," Sèxe"," Adresse","Document","Type_Document"," Email"," Etat Civil","Téléphone"};
DefaultTableModel md= new DefaultTableModel(data,head);
DefaultTableModel mds= new DefaultTableModel(data,heads);
tblpere.setModel(md);
tblfils.setModel(mds);
TableColumnModel cm= tblpere.getColumnModel();
        cm.getColumn(0).setPreferredWidth(30);
        cm.getColumn(1).setPreferredWidth(70);
        cm.getColumn(2).setPreferredWidth(78);
        cm.getColumn(3).setPreferredWidth(40);
        cm.getColumn(4).setPreferredWidth(10);
        tblpere.getColumnModel().getColumn(0).setCellRenderer(render);
        tblpere.getColumnModel().getColumn(3).setCellRenderer(render);
        tblpere.getColumnModel().getColumn(4).setCellRenderer(render);
TableColumnModel cmf= tblfils.getColumnModel();
cmf.getColumn(0).setPreferredWidth(90);
cmf.getColumn(1).setPreferredWidth(5);
cmf.getColumn(2).setPreferredWidth(100);
cmf.getColumn(3).setPreferredWidth(70);
cmf.getColumn(4).setPreferredWidth(30);
cmf.getColumn(5).setPreferredWidth(90);
cmf.getColumn(6).setPreferredWidth(10);
cmf.getColumn(6).setPreferredWidth(40);
//--------------------------------------------------------------------------------------------------
String lestado="";
      String query="select * from show_employes where matricule like '%"+recherche1.getText()+"%' or nom like '%"+recherche1.getText()+"%'"
              + " or prenom like '%"+recherche1.getText()+"%' or type_employer like '%"+recherche1.getText()+"%' order by nom asc";
    ResultSet rs=Mn.SEARCHDATA(query);
    try {
        md.setRowCount(0);
       while(rs.next()){
           if(rs.getBoolean("estatuses")==true){
              lestado="Actif(ve)"; 
           }else{
             lestado="Inactif(ve)";   
           }
         String[] vect={rs.getString("matricule"),rs.getString("nom"),rs.getString("prenom"),
        rs.getString("type_employer"),lestado};
         Mn.ADD_TABLE(md, vect);
        }
    } catch (SQLException ex) {
        Logger.getLogger(Cons_intelligente.class.getName()).log(Level.SEVERE, null, ex);
    }
//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------
            
        }else if(professeur.isSelected()){
TableCellRenderer render= new TableCellRenderer() {
 public Component getTableCellRendererComponent(JTable table,Object value,boolean idSelected,boolean hasFocus,int row,int column){
     JLabel lbl= new JLabel(value==null? "": value.toString());
     if(column==3 && row>=0){
         lbl.setHorizontalAlignment(SwingConstants.LEFT);
         lbl.setOpaque(true);
         lbl.setBackground(Color.yellow);
     }
if(column==0 && row>=0){
         lbl.setHorizontalAlignment(SwingConstants.LEFT);
         lbl.setOpaque(true);
         lbl.setBackground(Color.green);
     }
     return lbl;
 }
};
String[][] data={};
String[] head={"Matricule","Nom","Prénom","Quant.Hres/Semaine"};
String[] heads={"Matières"," Classes"," Sections","Jours","Heures","Horaire"};
DefaultTableModel md= new DefaultTableModel(data,head);
DefaultTableModel mds= new DefaultTableModel(data,heads);
tblpere.setModel(md);
tblfils.setModel(mds);
TableColumnModel cm= tblpere.getColumnModel();
        cm.getColumn(0).setPreferredWidth(30);
        cm.getColumn(1).setPreferredWidth(75);
        cm.getColumn(2).setPreferredWidth(95);
        cm.getColumn(3).setPreferredWidth(70);
        tblpere.getColumnModel().getColumn(0).setCellRenderer(render);
        tblpere.getColumnModel().getColumn(3).setCellRenderer(render);
TableColumnModel cmf= tblfils.getColumnModel();
cmf.getColumn(0).setPreferredWidth(90);
cmf.getColumn(1).setPreferredWidth(70);
cmf.getColumn(2).setPreferredWidth(70);
cmf.getColumn(3).setPreferredWidth(70);
cmf.getColumn(4).setPreferredWidth(70);
//--------------------------------------------------------------------------------------------------
String lestado="";
      String query="select * from show_professeur where matricule like '%"+recherche1.getText()+"%' or nom like '%"+recherche1.getText()+"%'"
              + " or prenom like '%"+recherche1.getText()+"%' order by nom asc";
    ResultSet rs=Mn.SEARCHDATA(query);
    try {
        md.setRowCount(0);
       while(rs.next()){
         String sql="select sum(heure) as quantite from professeur where matricule='"+rs.getString("matricule")+"'";
          ResultSet rss=Mn.SEARCHDATA(sql);
          if(rss.next()){
              lestado= rss.getString("quantite");
        String[] vect={rs.getString("matricule"),rs.getString("nom"),rs.getString("prenom"),lestado+" "+
        rs.getString("deviz")};
        Mn.ADD_TABLE(md, vect);
          }
        }
    } catch (SQLException ex) {
        Logger.getLogger(Cons_intelligente.class.getName()).log(Level.SEVERE, null, ex);
    }
//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------
            
        }else if(resultat.isSelected()){
String[][] data={};
String[] head={"Classes","Sections","Quant. Elèves"};
String[] heads={"Matricules"," Nom"," Prénom","Controle","Total Notes","Sur","Moyenne","Qualification","Moy. Générale","Decision"};
DefaultTableModel md= new DefaultTableModel(data,head);
DefaultTableModel mds= new DefaultTableModel(data,heads);
tblpere.setModel(md);
tblfils.setModel(mds);
TableColumnModel cm= tblpere.getColumnModel();
        cm.getColumn(0).setPreferredWidth(75);
        cm.getColumn(1).setPreferredWidth(75);
        cm.getColumn(2).setPreferredWidth(70);
TableColumnModel cmf= tblfils.getColumnModel();
cmf.getColumn(0).setPreferredWidth(30);
cmf.getColumn(1).setPreferredWidth(70);
cmf.getColumn(2).setPreferredWidth(90);
cmf.getColumn(3).setPreferredWidth(5);
cmf.getColumn(4).setPreferredWidth(5);
cmf.getColumn(5).setPreferredWidth(5);
cmf.getColumn(6).setPreferredWidth(5);
cmf.getColumn(7).setPreferredWidth(30);
cmf.getColumn(8).setPreferredWidth(5);
cmf.getColumn(9).setPreferredWidth(50);
//--------------------------------------------------------------------------------------------------
String lestado="";
      String query="select * from show_resultat where classe like '%"+recherche1.getText()+"%' or section like '%"+recherche1.getText()+"%' order by classe asc";
    ResultSet rs=Mn.SEARCHDATA(query);
    try {
        md.setRowCount(0);
       while(rs.next()){
           int aa=Mn.SEARCHCODE("code_classe","description","section", rs.getString("classe"),rs.getString("section"), "classe");
         String sql="select count(code_tercero) as quantite from inscription where code_classe="+aa+"";
          ResultSet rss=Mn.SEARCHDATA(sql);
          if(rss.next()){
              lestado= rss.getString("quantite");
        String[] vect={rs.getString("classe"),rs.getString("section"),lestado+" "+"Elèves"};
        Mn.ADD_TABLE(md, vect);
          }
        }
    } catch (SQLException ex) {
        Logger.getLogger(Cons_intelligente.class.getName()).log(Level.SEVERE, null, ex);
    }
//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------
            
        }else if(collection.isSelected()){
String[][] data={};
String[] head={"Classes","Sections","Quant. Elèves"};
String[] heads={"Matricules"," Nom"," Prénom","Versement","Date","Balance"};
DefaultTableModel md= new DefaultTableModel(data,head);
DefaultTableModel mds= new DefaultTableModel(data,heads);
tblpere.setModel(md);
tblfils.setModel(mds);
TableColumnModel cmf= tblfils.getColumnModel();
cmf.getColumn(0).setPreferredWidth(30);
cmf.getColumn(1).setPreferredWidth(70);
cmf.getColumn(2).setPreferredWidth(100);
cmf.getColumn(3).setPreferredWidth(5);
cmf.getColumn(4).setPreferredWidth(150);
cmf.getColumn(5).setPreferredWidth(30);
//--------------------------------------------------------------------------------------------------
String lestado="";
      String query="select * from show_resultat where classe like '%"+recherche1.getText()+"%' or section like '%"+recherche1.getText()+"%' order by classe asc";
    ResultSet rs=Mn.SEARCHDATA(query);
    try {
        md.setRowCount(0);
       while(rs.next()){
           int aa=Mn.SEARCHCODE("code_classe","description","section", rs.getString("classe"),rs.getString("section"), "classe");
         String sql="select count(code_tercero) as quantite from inscription where code_classe="+aa+"";
          ResultSet rss=Mn.SEARCHDATA(sql);
          if(rss.next()){
              lestado= rss.getString("quantite");
        String[] vect={rs.getString("classe"),rs.getString("section"),lestado+" "+"Elèves"};
        Mn.ADD_TABLE(md, vect);
          }
        }
    } catch (SQLException ex) {
        Logger.getLogger(Cons_intelligente.class.getName()).log(Level.SEVERE, null, ex);
    }
//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------
            
        }else if(paiement.isSelected()){
TableCellRenderer render= new TableCellRenderer() {
 public Component getTableCellRendererComponent(JTable table,Object value,boolean idSelected,boolean hasFocus,int row,int column){
     JLabel lbl= new JLabel(value==null? "": value.toString());
     if(column>=0 && column<4 && row>=0){
         lbl.setHorizontalAlignment(SwingConstants.LEFT);
         lbl.setOpaque(true);
         lbl.setBackground(Color.green);
     }
      if(column==4 && !value.toString().equals("Actif(ve)")&& row>=0){
         lbl.setHorizontalAlignment(SwingConstants.LEFT);
         lbl.setOpaque(true);
         lbl.setForeground(Color.red);
     }
     return lbl;
 }
};
String[][] data={};
String[] head={"Matricules","Nom","Prénom","Type Employer","Status"};
String[] heads={"Date"};
DefaultTableModel md= new DefaultTableModel(data,head);
DefaultTableModel mds= new DefaultTableModel(data,heads);
tblpere.setModel(md);
tblfils.setModel(mds);
TableColumnModel cm= tblpere.getColumnModel();
        cm.getColumn(0).setPreferredWidth(30);
        cm.getColumn(1).setPreferredWidth(70);
        cm.getColumn(2).setPreferredWidth(100);
        cm.getColumn(2).setPreferredWidth(50);
        tblpere.getColumnModel().getColumn(0).setCellRenderer(render);
        tblpere.getColumnModel().getColumn(3).setCellRenderer(render);
        tblpere.getColumnModel().getColumn(4).setCellRenderer(render);

String lestado="";
      String query="select * from show_employes  where matricule like '%"+recherche1.getText()+"%' or nom like '%"+recherche1.getText()+"%'"
              + " or prenom like '%"+recherche1.getText()+"%' or type_employer like '%"+recherche1.getText()+"%' order by nom asc";
    ResultSet rs=Mn.SEARCHDATA(query);
    try {
        md.setRowCount(0);
       while(rs.next()){
           if(rs.getBoolean("estatuses")==true){
              lestado="Actif(ve)"; 
           }else{
             lestado="Inactif(ve)";   
           }
         String[] vect={rs.getString("matricule"),rs.getString("nom"),rs.getString("prenom"),
        rs.getString("type_employer"),lestado};
         Mn.ADD_TABLE(md, vect);
        }
    } catch (SQLException ex) {
        Logger.getLogger(Cons_intelligente.class.getName()).log(Level.SEVERE, null, ex);
    }
//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------
            
        }else if(bulletin.isSelected()){
String[][] data={};
String[] head={"Matricules","Nom","Prénom","Classes"};
String[] heads={" Matières"," Notes"," Sur"};
DefaultTableModel md= new DefaultTableModel(data,head);
DefaultTableModel mds= new DefaultTableModel(data,heads);
tblpere.setModel(md);
tblfils.setModel(mds);
TableColumnModel cm= tblpere.getColumnModel();
        cm.getColumn(0).setPreferredWidth(30);
        cm.getColumn(1).setPreferredWidth(70);
        cm.getColumn(2).setPreferredWidth(100);
        cm.getColumn(2).setPreferredWidth(50);
//--------------------------------------------------------------------------------------------------
      String query="select * from show_resultats where matricule like '%"+recherche1.getText()+"%' or nom like '%"+recherche1.getText()+"%'"
              + " or prenom like '%"+recherche1.getText()+"%' or classe like '%"+recherche1.getText()+"%' group by matricule order by nom asc";
    ResultSet rs=Mn.SEARCHDATA(query);
    try {
        md.setRowCount(0);
       while(rs.next()){
         String[] vect={rs.getString("matricule"),rs.getString("nom"),rs.getString("prenom"),rs.getString("classe")};
        Mn.ADD_TABLE(md, vect);
        }
    } catch (SQLException ex) {
        Logger.getLogger(Cons_intelligente.class.getName()).log(Level.SEVERE, null, ex);
    }
//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------
            
        }else if(matiere.isSelected()){
 TableCellRenderer render= new TableCellRenderer() {
 public Component getTableCellRendererComponent(JTable table,Object value,boolean idSelected,boolean hasFocus,int row,int column){
     JLabel lbl= new JLabel(value==null? "": value.toString());
    
      if(column==2 && !value.toString().equals("Active")&& row>=0){
         lbl.setHorizontalAlignment(SwingConstants.LEFT);
         lbl.setOpaque(true);
         lbl.setForeground(Color.red);
     }
     return lbl;
 }
};
String[][] data={};
String[] heads={" Classes"," Sections"};
String[] head={" Matières"," Sur"," Status"};
DefaultTableModel md= new DefaultTableModel(data,head);
DefaultTableModel mds= new DefaultTableModel(data,heads);
tblpere.setModel(md);
tblfils.setModel(mds);
tblpere.getColumnModel().getColumn(2).setCellRenderer(render);
//--------------------------------------------------------------------------------------------------
      String query="select * from show_matiere where matiere like '%"+recherche1.getText()+"%' or "
              + " valeur like '%"+recherche1.getText()+"%' group by matiere order by matiere asc";
    ResultSet rs=Mn.SEARCHDATA(query);
    try {
        md.setRowCount(0);
       while(rs.next()){
         String[] vect={rs.getString("matiere"),rs.getString("valeur"),rs.getString("estatus")};
        Mn.ADD_TABLE(md, vect);
        }
    } catch (SQLException ex) {
        Logger.getLogger(Cons_intelligente.class.getName()).log(Level.SEVERE, null, ex);
    }
//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------
            
        }else if(borderaux.isSelected()){
String[][] data={};
String[] heads={" Classes"," Sections"};
String[] head={" Ouvrages"," Auteurs"};
DefaultTableModel md= new DefaultTableModel(data,head);
DefaultTableModel mds= new DefaultTableModel(data,heads);
tblpere.setModel(md);
tblfils.setModel(mds);
//--------------------------------------------------------------------------------------------------
      String query="select o.description,o.auteur, cl.description as classes, cl.section from ouvrages o\n" +
"left join ouvrages_has_institution oi on oi.ouvrages_code=o.code\n" +
"left join institution i on i.idinstitution=oi.institution_idinstitution\n" +
"left join ouvrages_vs_matiere om on om.ouvrages_code=o.code \n" +
"left join matiere_vs_classe cm on om.code_matiere=cm.code_matiere \n" +
"left join classe cl on cl.code_classe=cm.code_classe where cl.description like '%"+recherche1.getText()+"%' or "
              + " cl.section like '%"+recherche1.getText()+"%' group by classes order by classes asc";
    ResultSet rs=Mn.SEARCHDATA(query);
    try {
        md.setRowCount(0);
       while(rs.next()){
         String[] vect={rs.getString("classes"),rs.getString("section")};
        Mn.ADD_TABLE(md, vect);
        }
    } catch (SQLException ex) {
        Logger.getLogger(Cons_intelligente.class.getName()).log(Level.SEVERE, null, ex);
    }
//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------
            
        }else if(inscription.isSelected()){
          String[][] data={};
String[] head={" Classes"," Sections"};
String[] heads={"Matricule","Nom","Prénom","Téléphone","Status"};
DefaultTableModel md= new DefaultTableModel(data,head);
DefaultTableModel mds= new DefaultTableModel(data,heads);
tblpere.setModel(md);
tblfils.setModel(mds);
//--------------------------------------------------------------------------------------------------
      String query="select description as classes, section from classe  where description like '%"+recherche1.getText()+"%' or "
              + " section like '%"+recherche1.getText()+"%' group by classes order by classes asc";
    ResultSet rs=Mn.SEARCHDATA(query);
    try {
        md.setRowCount(0);
       while(rs.next()){
         String[] vect={rs.getString("classes"),rs.getString("section")};
        Mn.ADD_TABLE(md, vect);
        }
    } catch (SQLException ex) {
        Logger.getLogger(Cons_intelligente.class.getName()).log(Level.SEVERE, null, ex);
    }  
            
        }else if(reservation.isSelected()){
          String[][] data={};
   String[] head={" Classes"," Sections"};
     DefaultTableModel mds= new DefaultTableModel(data,head);
     tblpere.setModel(mds);
//--------------------------------------------------------------------------------------------------
      String query="select description as classes, section from classe  where description like '%"+recherche1.getText()+"%' or "
              + " section like '%"+recherche1.getText()+"%' group by classes order by classes asc";
    ResultSet rs=Mn.SEARCHDATA(query);
    try {
        mds.setRowCount(0);
       while(rs.next()){
         String[] vect={rs.getString("classes"),rs.getString("section")};
        Mn.ADD_TABLE(mds, vect);
        }
    } catch (SQLException ex) {
        Logger.getLogger(Cons_intelligente.class.getName()).log(Level.SEVERE, null, ex);
    }
     qut.setVisible(true);
     aux.setVisible(true);
     qut.setText(String.valueOf(tblfils.getRowCount()));
     aux.setText("Réservation(s)");
            
        }    
    }//GEN-LAST:event_recherche1KeyReleased

    private void classeMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_classeMouseClicked
       StructureClasse();
    }//GEN-LAST:event_classeMouseClicked

    private void eleveMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_eleveMouseClicked
     StructureEleve();
    }//GEN-LAST:event_eleveMouseClicked

    private void employerMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_employerMouseClicked
       StructureEmployer();
    }//GEN-LAST:event_employerMouseClicked

    private void professeurMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_professeurMouseClicked
       StructureProfesseur();
    }//GEN-LAST:event_professeurMouseClicked

    private void resultatMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_resultatMouseClicked
        StructureResultat();
    }//GEN-LAST:event_resultatMouseClicked

    private void collectionMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_collectionMouseClicked
        StructureCollection();
    }//GEN-LAST:event_collectionMouseClicked

    private void paiementMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_paiementMouseClicked
       StructurePaiement();
    }//GEN-LAST:event_paiementMouseClicked

    private void bulletinMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_bulletinMouseClicked
        StructureBulletin();
    }//GEN-LAST:event_bulletinMouseClicked

    private void matiereMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_matiereMouseClicked
        StructureMatiere();
    }//GEN-LAST:event_matiereMouseClicked

    private void tblpereMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblpereMouseClicked
        if(eleve.isSelected()){
        String mat=tblpere.getValueAt(tblpere.getSelectedRow(),0).toString();
        String[][] data={};
        String[] heads={"Date de Naissance"," Sèxe"," Adresse"," Santé"," Grpe. Sanguin"," Pres. Responsable","Téléphone","Lien Parenté"};  
        DefaultTableModel mds= new DefaultTableModel(data,heads);
       tblfils.setModel(mds);
       TableColumnModel cmf= tblfils.getColumnModel();
       cmf.getColumn(0).setPreferredWidth(90);
       cmf.getColumn(1).setPreferredWidth(5);
       cmf.getColumn(2).setPreferredWidth(100);
       cmf.getColumn(3).setPreferredWidth(20);
       cmf.getColumn(4).setPreferredWidth(70);
       cmf.getColumn(5).setPreferredWidth(40);
       cmf.getColumn(6).setPreferredWidth(10);
       String lestado="";
       String query="select distinct t.code AS code,t.code_tercero AS code_tercero,t.prenom AS prenom,t.nom AS nom,pd.sexe AS sexe,\n" +
"pd.adresse AS adresse,e.matricule AS matricule,pd.date_naissance AS naissance,e.cond_sante AS cond_sante,\n" +
"(select show_eleve_responsable.responsable AS responsable from show_eleve_responsable \n" +
"where (eht.eleve_matricule = show_eleve_responsable.matricule)) AS responsable,eht.lien_parente AS lien_parente,\n" +
"s.sang AS sang,e.estatus AS estatus,i.nom AS institution,tvt.phone_number as telephone,tv.phone_number as telparent from ((((((((eleve e left join tercero t\n" +
"on(((e.code = t.code) and (e.code_tercero = t.code_tercero)))) left join personal_data pd on(((t.code = pd.code)\n" +
"and (t.code_tercero = pd.code_tercero)))) left join tercero_vs_telephones tvt on(((t.code = tvt.code) \n" +
"and (t.code_tercero = tvt.code_tercero)))) left join eleve_has_tercero_responsable eht \n" +
"on((e.matricule = eht.eleve_matricule))) left join tercero_email te on(((t.code = te.code) and \n" +
"(t.code_tercero = te.code_tercero)))) left join sanguins s on(((t.code = s.code) and\n" +
"(t.code_tercero = s.code_tercero)))) left join eleve_has_institution ehi on(((e.matricule = ehi.matricule) and\n" +
"(e.code = ehi.code) and (e.code_tercero = ehi.code_tercero)))) \n" +
"left join institution i on((i.idinstitution = ehi.idinstitution)) \n" +
"left join tercero_vs_telephones tv on (((eht.code = tv.code) \n" +
"and (eht.code_tercero = tv.code_tercero)))) where e.matricule='"+mat+"'";
        ResultSet rs=Mn.SEARCHDATA(query);
        try {
         mds.setRowCount(0);
         while(rs.next()){
           if(rs.getBoolean("estatus")==true){
              lestado="Actif(ve)"; 
           }else{
             lestado="Inactif(ve)";   
           }
           String[] vect={rs.getString("naissance"),rs.getString("sexe"),rs.getString("adresse"),
           rs.getString("cond_sante"),rs.getString("sang"),rs.getString("responsable"),rs.getString("telparent"),rs.getString("lien_parente")};
           Mn.ADD_TABLE(mds, vect);
          }
         } catch (SQLException ex) {
        Logger.getLogger(Cons_intelligente.class.getName()).log(Level.SEVERE, null, ex);
       } 
        
        
    }else if(classe.isSelected()){
        
String matt=tblpere.getValueAt(tblpere.getSelectedRow(),0).toString();
String mattt=tblpere.getValueAt(tblpere.getSelectedRow(),1).toString();
section.setSelectedItem(mattt);
sectionFocusLost(null);
klas.setSelectedItem(matt);
  TableCellRenderer render= new TableCellRenderer() {
 public Component getTableCellRendererComponent(JTable table,Object value,boolean idSelected,boolean hasFocus,int row,int column){
     JLabel lbl= new JLabel(value==null? "": value.toString());
     if(column==0 && row>=0){
         lbl.setHorizontalAlignment(SwingConstants.LEFT);
         lbl.setOpaque(true);
         lbl.setBackground(Color.green);
     }
if(column==4 && row>=0){
         lbl.setHorizontalAlignment(SwingConstants.LEFT);
         lbl.setOpaque(true);
         lbl.setBackground(Color.yellow);
     }
if(column==5 && row>=0){
         lbl.setHorizontalAlignment(SwingConstants.LEFT);
         lbl.setOpaque(true);
         lbl.setBackground(Color.yellow);
     }
     return lbl;
 }
};
         String mat=tblpere.getValueAt(tblpere.getSelectedRow(),0).toString();
         String[][] data={};
         String[] heads={"Matricule"," Nom"," Prénom"," Adresse"," Pres. Responsable","Téléphone","Lien Parenté"};   
         DefaultTableModel mds= new DefaultTableModel(data,heads);
         tblfils.setModel(mds);
         TableColumnModel cmf= tblfils.getColumnModel();
         cmf.getColumn(0).setPreferredWidth(30);
         cmf.getColumn(1).setPreferredWidth(80);
         cmf.getColumn(2).setPreferredWidth(100);
         cmf.getColumn(3).setPreferredWidth(150);
         cmf.getColumn(4).setPreferredWidth(70);
         cmf.getColumn(5).setPreferredWidth(40);
         cmf.getColumn(6).setPreferredWidth(10);
         tblfils.getColumnModel().getColumn(0).setCellRenderer(render);
         tblfils.getColumnModel().getColumn(4).setCellRenderer(render);
         tblfils.getColumnModel().getColumn(5).setCellRenderer(render);
         String lestado="";
         String query="select e.matricule AS matricule,t.nom AS nom,t.prenom AS prenom,pd.adresse AS adresse,\n" +
"(select concat(t.nom,_utf8' ',t.prenom) \n" +
"from tercero t join eleve_has_tercero_responsable ehtr on(t.code = ehtr.code and t.code_tercero = ehtr.code_tercero) \n" +
"where ehtr.eleve_matricule = e.matricule ) AS responsable,\n" +
"tvt.phone_number AS telephone,eht.lien_parente AS lien_parente,i.annee_academique AS annee,c.description AS classe\n" +
" from ((((((inscription i left join tercero t on(((t.code = i.code) and (t.code_tercero = i.code_tercero)))) \n" +
" left join eleve e on(((e.code = t.code) and (e.code_tercero = t.code_tercero)))) \n" +
" left join classe c on((c.code_classe = i.code_classe))) \n" +
" left join personal_data pd on(((t.code = pd.code) and (t.code_tercero = pd.code_tercero)))) \n" +
" left join eleve_has_tercero_responsable eht on(e.matricule = eht.eleve_matricule)) \n" +
" left join tercero_vs_telephones tvt on(((eht.code = tvt.code) \n" +
" and (eht.code_tercero = tvt.code_tercero)))) where  c.description='"+mat+"' and i.annee_academique='"+academique.getSelectedItem().toString()+"'";  
         ResultSet rs=Mn.SEARCHDATA(query);
         try {
         mds.setRowCount(0);
         while(rs.next()){
           String[] vect={rs.getString("matricule"),rs.getString("nom"),rs.getString("prenom"),
           rs.getString("adresse"),rs.getString("responsable"),rs.getString("telephone"),rs.getString("lien_parente")};
           Mn.ADD_TABLE(mds, vect);
          }
         } catch (SQLException ex) {
        Logger.getLogger(Cons_intelligente.class.getName()).log(Level.SEVERE, null, ex);
        }  
     qut.setVisible(true);
     aux.setVisible(true);
     qut.setText(String.valueOf(tblfils.getRowCount()));
     aux.setText("Elèves");
     
     
       }else if(employer.isSelected()){
            
String mat=tblpere.getValueAt(tblpere.getSelectedRow(),0).toString();
String[][] data={};
String[] heads={"Date de Naissance"," Sèxe"," Adresse","Document","Type_Document"," Email"," Etat Civil","Téléphone"};
DefaultTableModel mds= new DefaultTableModel(data,heads);
tblfils.setModel(mds);
TableColumnModel cmf= tblfils.getColumnModel();
cmf.getColumn(0).setPreferredWidth(90);
cmf.getColumn(1).setPreferredWidth(5);
cmf.getColumn(2).setPreferredWidth(100);
cmf.getColumn(3).setPreferredWidth(70);
cmf.getColumn(4).setPreferredWidth(30);
cmf.getColumn(5).setPreferredWidth(90);
cmf.getColumn(6).setPreferredWidth(10);
cmf.getColumn(6).setPreferredWidth(40);
//--------------------------------------------------------------------------------------------------
String lestado="";
      String query="select * from show_employes where matricule='"+mat+"' group by sexe";
    ResultSet rs=Mn.SEARCHDATA(query);
    try {
        mds.setRowCount(0);
       while(rs.next()){
         String[] vect={rs.getString("naissance"),rs.getString("sexe"),rs.getString("adresse"),
        rs.getString("document"),rs.getString("tpdocument"),rs.getString("mail"),rs.getString("etat_civil"),rs.getString("telephone")};
         Mn.ADD_TABLE(mds, vect);
        }
    } catch (SQLException ex) {
        Logger.getLogger(Cons_intelligente.class.getName()).log(Level.SEVERE, null, ex);
    }
//--------------------------------------------------------------------------------------------------  
    
    
        }else if(professeur.isSelected()){
            
      TableCellRenderer render= new TableCellRenderer() {
 public Component getTableCellRendererComponent(JTable table,Object value,boolean idSelected,boolean hasFocus,int row,int column){
     JLabel lbl= new JLabel(value==null? "": value.toString());
     if(column==5 && row>=0){
         lbl.setHorizontalAlignment(SwingConstants.LEFT);
         lbl.setOpaque(true);
         lbl.setBackground(Color.yellow);
     }

     return lbl;
 }
};
       String mat=tblpere.getValueAt(tblpere.getSelectedRow(),0).toString();
            String[][] data={};
String[] heads={"Matières"," Classes"," Sections","Jours","Heures","Horaire"};
DefaultTableModel mds= new DefaultTableModel(data,heads);
tblfils.setModel(mds);

TableColumnModel cmf= tblfils.getColumnModel();
cmf.getColumn(0).setPreferredWidth(90);
cmf.getColumn(1).setPreferredWidth(70);
cmf.getColumn(2).setPreferredWidth(70);
cmf.getColumn(3).setPreferredWidth(70);
cmf.getColumn(4).setPreferredWidth(70);
tblfils.getColumnModel().getColumn(5).setCellRenderer(render);
//--------------------------------------------------------------------------------------------------
String lestado="";
String estado="";
      String query="select h.code_horraire AS matricules,h.matiere AS matiere,h.classe AS classe,c.section AS section,h.jour \n" +
"AS jour,h.nbre_heure AS nbre_heure,h.devise AS devise,ph.matricule,p.heure,h.heure_in,h.heure_out from (horaire h left join classe c \n" +
"on((c.description = h.classe)) left join professeur_has_horaire ph on(ph.code_horraire=h.code_horraire) left join professeur p \n" +
"on(p.matricule=ph.matricule)) where ph.matricule='"+mat+"' group by heure_in  order by matiere asc";
    ResultSet rs=Mn.SEARCHDATA(query);
    try {
        mds.setRowCount(0);
       while(rs.next()){
           lestado=rs.getString("nbre_heure")+" "+rs.getString("devise");
           estado=rs.getString("heure_in")+" - "+rs.getString("heure_out");
        String[] vect={rs.getString("matiere"),rs.getString("classe"),rs.getString("section"),rs.getString("jour"),lestado,estado};  
        Mn.ADD_TABLE(mds, vect);
        }
    } catch (SQLException ex) {
        Logger.getLogger(Cons_intelligente.class.getName()).log(Level.SEVERE, null, ex);
    }
    
    
        }else if(resultat.isSelected()){
            
String matt=tblpere.getValueAt(tblpere.getSelectedRow(),0).toString();
String mattt=tblpere.getValueAt(tblpere.getSelectedRow(),1).toString();
section.setSelectedItem(mattt);
sectionFocusLost(null);
klas.setSelectedItem(matt);
             String moyenne="";
     TableCellRenderer render= new TableCellRenderer() {
 public Component getTableCellRendererComponent(JTable table,Object value,boolean idSelected,boolean hasFocus,int row,int column){
     JLabel lbl= new JLabel(value==null? "": value.toString());
     if(column==0 && row>=0){
         lbl.setHorizontalAlignment(SwingConstants.LEFT);
         lbl.setOpaque(true);
         lbl.setBackground(Color.green);
     }
     if(column==6 && row>=0){
         lbl.setHorizontalAlignment(SwingConstants.LEFT);
         lbl.setOpaque(true);
         lbl.setBackground(Color.green);
     }
     if(column==8 && row>=0){
         lbl.setHorizontalAlignment(SwingConstants.LEFT);
         lbl.setOpaque(true);
         lbl.setBackground(Color.green);
     }
      if(column==7 && value.toString().equals("Mal.") && row>=0){
         lbl.setHorizontalAlignment(SwingConstants.LEFT);
         lbl.setOpaque(true);
         lbl.setForeground(Color.red);
     }
    
     return lbl;
 }
};
  String mat=tblpere.getValueAt(tblpere.getSelectedRow(),0).toString();
  int aa=0;
if(controle1.isSelected()){
    aa=1;
}else if(controle2.isSelected()){
    aa=2;
}else if(controle3.isSelected()){
    aa=3;
}
String[][] data={};
String[] heads={"Matricules"," Nom"," Prénom","Controle","Total Notes","Sur","Moyenne","Qualification","Moy. Générale","Decision"};
DefaultTableModel mds= new DefaultTableModel(data,heads);
tblfils.setModel(mds);
TableColumnModel cmf= tblfils.getColumnModel();
cmf.getColumn(0).setPreferredWidth(30);
cmf.getColumn(1).setPreferredWidth(70);
cmf.getColumn(2).setPreferredWidth(90);
cmf.getColumn(3).setPreferredWidth(5);
cmf.getColumn(4).setPreferredWidth(5);
cmf.getColumn(5).setPreferredWidth(5);
cmf.getColumn(6).setPreferredWidth(5);
cmf.getColumn(7).setPreferredWidth(30);
cmf.getColumn(8).setPreferredWidth(5);
cmf.getColumn(9).setPreferredWidth(50);
tblfils.getColumnModel().getColumn(0).setCellRenderer(render);
tblfils.getColumnModel().getColumn(6).setCellRenderer(render);
tblfils.getColumnModel().getColumn(7).setCellRenderer(render);
tblfils.getColumnModel().getColumn(8).setCellRenderer(render);

//--------------------------------------------------------------------------------------------------
 String query="select * from show_resultats where classe='"+mat+"' and controle="+aa+" and annee='"+academique.getSelectedItem().toString()+"'"
              + "group by matricule order by nom asc";
    ResultSet rs=Mn.SEARCHDATA(query);
    try {
        mds.setRowCount(0);
       while(rs.next()){
          String[] vect={rs.getString("matricule"),rs.getString("nom"),rs.getString("prenom"),rs.getString("controle"),
              rs.getString("total"),rs.getString("points"),rs.getString("moyenne"),rs.getString("qualification"),rs.getString("generale")
                  ,rs.getString("decision")};
        Mn.ADD_TABLE(mds, vect);
        }
    } catch (SQLException ex) {
        Logger.getLogger(Cons_intelligente.class.getName()).log(Level.SEVERE, null, ex);
    }
//--------------------------------------------------------------------------------------------------
   
    String querys="select moyenne_aceptable from qualification";
    ResultSet rss=Mn.SEARCHDATA(querys);
    try {
       if(rss.first()){
        moyenne=rss.getString("moyenne_aceptable");
        }
    } catch (SQLException ex) {
        Logger.getLogger(Cons_intelligente.class.getName()).log(Level.SEVERE, null, ex);
    }
    
    String querrys="select count(moyenne)as reyisi, c.description as classe from note n left join classe c on n.code_classe=c.code_classe "
            + "where moyenne >='"+moyenne+"' and c.description='"+mat+"' and n.controle="+aa+"";
    ResultSet rrss=Mn.SEARCHDATA(querrys);
    try {
       if(rrss.first()){
        reyisi.setText(rrss.getString("reyisi"));
        reyisi.setVisible(true);
        reyisil.setVisible(true);
        }
    } catch (SQLException ex) {
        Logger.getLogger(Cons_intelligente.class.getName()).log(Level.SEVERE, null, ex);
    }
    
    String querrrys="select count(moyenne)as nonreyisi, c.description as classe from note n left join classe c on n.code_classe=c.code_classe "
            + "where moyenne <'"+moyenne+"' and c.description='"+mat+"' and n.controle="+aa+"";
    ResultSet rrrss=Mn.SEARCHDATA(querrrys);
    try {
       if(rrrss.first()){
        nonreyisi.setText(rrrss.getString("nonreyisi"));
        nonreyisi.setVisible(true);
        nonreyisil.setVisible(true);
        }
    } catch (SQLException ex) {
        Logger.getLogger(Cons_intelligente.class.getName()).log(Level.SEVERE, null, ex);
    }
//----------------------------------------------------------------------------------------------------------------
    
    
        }else if(collection.isSelected()){
            
String matt=tblpere.getValueAt(tblpere.getSelectedRow(),0).toString();
String mattt=tblpere.getValueAt(tblpere.getSelectedRow(),1).toString();
section.setSelectedItem(mattt);
sectionFocusLost(null);
klas.setSelectedItem(matt);
  TableCellRenderer render= new TableCellRenderer() {
 public Component getTableCellRendererComponent(JTable table,Object value,boolean idSelected,boolean hasFocus,int row,int column){
     JLabel lbl= new JLabel(value==null? "": value.toString());
     if(column==0 && row>=0){
         lbl.setHorizontalAlignment(SwingConstants.LEFT);
         lbl.setOpaque(true);
         lbl.setBackground(Color.green);
     }
     if(column==3 && row>=0){
         lbl.setHorizontalAlignment(SwingConstants.LEFT);
         lbl.setOpaque(true);
         lbl.setBackground(Color.yellow);
     }
     if(column==5 && row>=0){
         lbl.setHorizontalAlignment(SwingConstants.LEFT);
         lbl.setOpaque(true);
         lbl.setBackground(Color.yellow);
     }
      if(column==5 && value.toString().equals("0 Gdes") && row>=0){
         lbl.setHorizontalAlignment(SwingConstants.LEFT);
         lbl.setOpaque(true);
         lbl.setBackground(Color.red);
     }
     return lbl;
 }
};
 String mat=tblpere.getValueAt(tblpere.getSelectedRow(),0).toString();
 String[][] data={};
String[] heads={"Matricules"," Nom"," Prénom","Versement","Date","Balance"};
DefaultTableModel mds= new DefaultTableModel(data,heads);
tblfils.setModel(mds);
TableColumnModel cmf= tblfils.getColumnModel();
cmf.getColumn(0).setPreferredWidth(30);
cmf.getColumn(1).setPreferredWidth(70);
cmf.getColumn(2).setPreferredWidth(100);
cmf.getColumn(3).setPreferredWidth(5);
cmf.getColumn(4).setPreferredWidth(150);
cmf.getColumn(5).setPreferredWidth(30);
tblfils.getColumnModel().getColumn(0).setCellRenderer(render);
tblfils.getColumnModel().getColumn(3).setCellRenderer(render);
tblfils.getColumnModel().getColumn(5).setCellRenderer(render);
//--------------------------------------------------------------------------------------------------
      String query="select * from show_collecter where annee='"+academique.getSelectedItem().toString()+"' and classe='"+mat+"' order by date asc";
    ResultSet rs=Mn.SEARCHDATA(query);
    try {
        mds.setRowCount(0);
       while(rs.next()){
        String[] vect={rs.getString("matricule"),rs.getString("nom"),rs.getString("prenom"),rs.getString("versement")+" Gdes",
        rs.getString("date"),rs.getString("balance")+" Gdes"};
        Mn.ADD_TABLE(mds, vect);
        }
    } catch (SQLException ex) {
        Logger.getLogger(Cons_intelligente.class.getName()).log(Level.SEVERE, null, ex);
    }
//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------
    
        }else if(paiement.isSelected()){
            
  TableCellRenderer render= new TableCellRenderer() {
 public Component getTableCellRendererComponent(JTable table,Object value,boolean idSelected,boolean hasFocus,int row,int column){
     JLabel lbl= new JLabel(value==null? "": value.toString());
      if(column>=0 && !value.toString().isEmpty()&& row>=0){
         lbl.setHorizontalAlignment(SwingConstants.LEFT);
         lbl.setOpaque(true);
         lbl.setBackground(Color.red);
         lbl.setForeground(Color.yellow);
     }
     return lbl;
 }
};
         String mat=tblpere.getValueAt(tblpere.getSelectedRow(),0).toString();  
  String[][] data={};
String[] heads={"Date"};
DefaultTableModel mds= new DefaultTableModel(data,heads);
tblfils.setModel(mds);
      String query="select * from show_paiement where annee='"+academique.getSelectedItem()+"' and matricule='"+mat+"'  order by matricule asc";
    ResultSet rs=Mn.SEARCHDATA(query);
    try {
        mds.setRowCount(0);
       while(rs.next()){
         String[] vect={rs.getString("date")};
         Mn.ADD_TABLE(mds, vect);
        }
    } catch (SQLException ex) {
        Logger.getLogger(Cons_intelligente.class.getName()).log(Level.SEVERE, null, ex);
    }
        tblfils.getColumnModel().getColumn(0).setCellRenderer(render);

//--------------------------------------------------------------------------------------------------
        
        }else if(bulletin.isSelected()){
            
String mat=tblpere.getValueAt(tblpere.getSelectedRow(),0).toString();
String matt=tblpere.getValueAt(tblpere.getSelectedRow(),3).toString();
String mattt=tblpere.getValueAt(tblpere.getSelectedRow(),4).toString();
section.setSelectedItem(mattt);
sectionFocusLost(null);
klas.setSelectedItem(matt);
  int aa=0;
if(controle1.isSelected()){
    aa=1;
}else if(controle2.isSelected()){
    aa=2;
}else if(controle3.isSelected()){
    aa=3;
}
String[][] data={};
String[] heads={"Matières","  Notes"," Sur"};
DefaultTableModel mds= new DefaultTableModel(data,heads);
tblfils.setModel(mds);
//--------------------------------------------------------------------------------------------------
 String query="select * from show_resultats where matricule='"+mat+"' and controle="+aa+" "
         + "and annee='"+academique.getSelectedItem().toString()+"'"
              + "group by matiere order by matiere asc";
    ResultSet rs=Mn.SEARCHDATA(query);
    try {
        mds.setRowCount(0);
       while(rs.next()){
          String[] vect={rs.getString("matiere"),rs.getString("note"),rs.getString("sur")};
        Mn.ADD_TABLE(mds, vect);
        }
    } catch (SQLException ex) {
        Logger.getLogger(Cons_intelligente.class.getName()).log(Level.SEVERE, null, ex);
    }
    qut.setVisible(true);
     aux.setVisible(true);
     qut.setText(String.valueOf(tblfils.getRowCount()));
     aux.setText("Matières");
//--------------------------------------------------------------------------------------------------
 
     
    }else if(matiere.isSelected()){
        
String mat=tblpere.getValueAt(tblpere.getSelectedRow(),0).toString();
     String[][] data={};
String[] heads={" Classes"," Sections"};
DefaultTableModel mds= new DefaultTableModel(data,heads);
tblfils.setModel(mds);
//--------------------------------------------------------------------------------------------------
      String query="select * from show_matiere where matiere='"+mat+"' group by classe  order by classe asc";
    ResultSet rs=Mn.SEARCHDATA(query);
    try {
        mds.setRowCount(0);
       while(rs.next()){
         String[] vect={rs.getString("classe"),rs.getString("section")};
        Mn.ADD_TABLE(mds, vect);
        }
    } catch (SQLException ex) {
        Logger.getLogger(Cons_intelligente.class.getName()).log(Level.SEVERE, null, ex);
    }
    
     qut.setVisible(true);
     aux.setVisible(true);
     qut.setText(String.valueOf(tblfils.getRowCount()));
     aux.setText("Classes");
//--------------------------------------------------------------------------------------------------       
    }else if(borderaux.isSelected()){
        String mat=tblpere.getValueAt(tblpere.getSelectedRow(),0).toString();
     String[][] data={};
String[] heads={" Ouvrages"," Auteurs"};
DefaultTableModel mds= new DefaultTableModel(data,heads);
tblfils.setModel(mds);
//--------------------------------------------------------------------------------------------------
      String query="select o.description,o.auteur, cl.description as classes, cl.section from ouvrages o\n" +
"left join ouvrages_has_institution oi on oi.ouvrages_code=o.code\n" +
"left join institution i on i.idinstitution=oi.institution_idinstitution\n" +
"left join ouvrages_vs_matiere om on om.ouvrages_code=o.code \n" +
"left join matiere_vs_classe cm on om.code_matiere=cm.code_matiere \n" +
"left join classe cl on cl.code_classe=cm.code_classe where cl.description='"+mat+"' group by description  order by description asc";
    ResultSet rs=Mn.SEARCHDATA(query);
    try {
        mds.setRowCount(0);
       while(rs.next()){
         String[] vect={rs.getString("description"),rs.getString("auteur")};
        Mn.ADD_TABLE(mds, vect);
        }
    } catch (SQLException ex) {
        Logger.getLogger(Cons_intelligente.class.getName()).log(Level.SEVERE, null, ex);
    }
    qut.setVisible(true);
     aux.setVisible(true);
     qut.setText(String.valueOf(tblfils.getRowCount()));
     aux.setText("Ouvrages");
//--------------------------------------------------------------------------------------------------
    }else if(inscription.isSelected()){
        String lestado=null;
     String mat=tblpere.getValueAt(tblpere.getSelectedRow(),0).toString();
     String matt=tblpere.getValueAt(tblpere.getSelectedRow(),0).toString();
        String mattt=tblpere.getValueAt(tblpere.getSelectedRow(),1).toString();
        section.setSelectedItem(mattt);
        sectionFocusLost(null);
        klas.setSelectedItem(matt);
     String[][] data={};
     String[] heads={"Matricule","Nom","Prénom","Téléphone","Status"};
     DefaultTableModel mds= new DefaultTableModel(data,heads);
     tblfils.setModel(mds);
//--------------------------------------------------------------------------------------------------
      String query="select i.matricule,t.nom,t.prenom,tt.phone_number,e.estatus from inscription i left join tercero t\n" +
"on (t.code=i.code and t.code_tercero=i.code_tercero) left join tercero_vs_telephones tt on \n" +
"(tt.code=t.code and tt.code_tercero=t.code_tercero) left join eleve e on (e.code=i.code and e.code_tercero=i.code_tercero)\n" +
"left join classe c on c.code_classe=i.code_classe where c.description='"+mat+"' order by nom asc";
    ResultSet rs=Mn.SEARCHDATA(query);
    try {
        mds.setRowCount(0);
       while(rs.next()){
          if(rs.getBoolean("estatus")==true){
              lestado="Actif(ve)"; 
           }else{
             lestado="Inactif(ve)";   
           } 
         String[] vect={rs.getString("matricule"),rs.getString("nom"),rs.getString("prenom"),rs.getString("phone_number"),lestado};
        Mn.ADD_TABLE(mds, vect);
        }
    } catch (SQLException ex) {
        Logger.getLogger(Cons_intelligente.class.getName()).log(Level.SEVERE, null, ex);
    }
     qut.setVisible(true);
     aux.setVisible(true);
     qut.setText(String.valueOf(tblfils.getRowCount()));
     aux.setText("Éleves");
//--------------------------------------------------------------------------------------------------
    }else if(reservation.isSelected()){
     String mat=tblpere.getValueAt(tblpere.getSelectedRow(),0).toString();
     String matt=tblpere.getValueAt(tblpere.getSelectedRow(),0).toString();
        String mattt=tblpere.getValueAt(tblpere.getSelectedRow(),1).toString();
        section.setSelectedItem(mattt);
        sectionFocusLost(null);
        klas.setSelectedItem(matt);
     String[][] data={};
     String[] heads={" Nom"," Prénom","Téléphone"," Adresse"," Pres. Responsable","Téléphone","Lien Parenté"};
     DefaultTableModel mds= new DefaultTableModel(data,heads);
     tblfils.setModel(mds);
//--------------------------------------------------------------------------------------------------
      String query="select r.nom,r.prenom,r.sexe,r.adresse,r.telephone,r.sang,r.naissance,r.sante,r.responsable,r.telephone_res,r.lien,r.photo,\n" +
"r.estatus,c.description as classe,c.section from reservation r left join reservation_vs_classe rc on rc.code_reservation=r.code_reservation left join classe c \n" +
"on c.code_classe=rc.code_classe where c.description='"+mat+"' order by nom asc";
    ResultSet rs=Mn.SEARCHDATA(query);
    try {
        mds.setRowCount(0);
       while(rs.next()){
         String[] vect={rs.getString("nom"),rs.getString("prenom"),rs.getString("telephone"),rs.getString("adresse"),rs.getString("responsable")
         ,rs.getString("telephone_res"),rs.getString("lien")};
        Mn.ADD_TABLE(mds, vect);
        }
    } catch (SQLException ex) {
        Logger.getLogger(Cons_intelligente.class.getName()).log(Level.SEVERE, null, ex);
    }
     qut.setVisible(true);
     aux.setVisible(true);
     qut.setText(String.valueOf(tblfils.getRowCount()));
     aux.setText("Réservation(s)");
//--------------------------------------------------------------------------------------------------
    }
    }//GEN-LAST:event_tblpereMouseClicked

    private void controle1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_controle1MouseClicked
        controle1.setSelected(true);
        controle2.setSelected(false);
        controle3.setSelected(false);
    }//GEN-LAST:event_controle1MouseClicked

    private void controle2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_controle2MouseClicked
        controle1.setSelected(false);
        controle2.setSelected(true);
        controle3.setSelected(false);
    }//GEN-LAST:event_controle2MouseClicked

    private void controle3MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_controle3MouseClicked
        controle1.setSelected(false);
        controle2.setSelected(false);
        controle3.setSelected(true);
    }//GEN-LAST:event_controle3MouseClicked

    private void ImprimerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ImprimerActionPerformed
       if(bulletin.isSelected()){
           if(tblpere.getSelectedRows().length>0){
               int aa=0;
            if(controle1.isSelected()){
              aa=1;
              }else if(controle2.isSelected()){
               aa=2;
             }else if(controle3.isSelected()){
              aa=3;
             }
           String mat=tblpere.getValueAt(tblpere.getSelectedRow(),0).toString();
           Rp.SENDQUERY("Bulletin",mat,academique.getSelectedItem().toString(),aa,Variables.Empresa);  
           }else{
               JOptionPane.showMessageDialog(null,"S'il vous plait séléctionner un élève dans la table ci déssous");
           }
             
       }else if(collection.isSelected()){
          if(tblfils.getSelectedRows().length>0){
            String mat=tblfils.getValueAt(tblfils.getSelectedRow(),0).toString();
            Rp.SENDQUERY("Collection",mat,academique.getSelectedItem().toString());  
            }else{
               Rp.SENDQUERY("Collecte",klas.getSelectedItem().toString(),academique.getSelectedItem().toString());
            }
        }else if(eleve.isSelected()){
           Rp.SENDQUERY("classe",klas.getSelectedItem().toString(),academique.getSelectedItem().toString());
        }else if(classe.isSelected()){
           Rp.SENDQUERY("classe",klas.getSelectedItem().toString(),academique.getSelectedItem().toString());
        }else if(employer.isSelected()){
//            if(de.getText().isEmpty() && ha.getText().isEmpty()){
//               if(tblpere.getSelectedRows().length>0){
//                   String mat=tblpere.getValueAt(tblpere.getSelectedRow(),0).toString();
//            String query="select t.nom,t.prenom,e.document,e.matricule,t.adresse,t.telephone,e.mail,e.estatuses\n" +
//            ",i.nom as institution,i.direccion,i.telephone as tel,i.e_mail as email,e.document,e.type_document \n" +
//            "as tpdocument,e.naissance,etat_civil,t.sexe ,et.type_employer as tpemployer from employer e \n" +
//            "left join tercero t on t.code_tercero=e.code_tercero left join institution i on \n" +
//            "i.nom=t.institution left join employer_vs_type et on et.code_employer=e.code_employer"
//                    + " where e.matricule='"+mat+"' group by matricule";
//            Rp.SENDQUERY(query,"Employer");
//            }
//            }else   if(de.getText()!=null && ha.getText().isEmpty()){
//            String query="select t.nom,t.prenom,e.document,e.matricule,t.adresse,t.telephone,e.mail,e.estatuses\n" +
//            ",i.nom as institution,i.direccion,i.telephone as tel,i.e_mail as email,e.document,e.type_document \n" +
//            "as tpdocument,e.naissance,etat_civil,t.sexe ,et.type_employer as tpemployer from employer e \n" +
//            "left join tercero t on t.code_tercero=e.code_tercero left join institution i on \n" +
//            "i.nom=t.institution left join employer_vs_type et on et.code_employer=e.code_employer"
//            + " where et.type_employer='"+de.getText()+"' group by nom";
//            Rp.SENDQUERY(query,"Employer");
//            }else   if(de.getText().isEmpty() && ha.getText()!=null){
//            String query="select t.nom,t.prenom,e.document,e.matricule,t.adresse,t.telephone,e.mail,e.estatuses\n" +
//            ",i.nom as institution,i.direccion,i.telephone as tel,i.e_mail as email,e.document,e.type_document \n" +
//            "as tpdocument,e.naissance,etat_civil,t.sexe ,et.type_employer as tpemployer from employer e \n" +
//            "left join tercero t on t.code_tercero=e.code_tercero left join institution i on \n" +
//            "i.nom=t.institution left join employer_vs_type et on et.code_employer=e.code_employer"
//            + " where et.type_employer='"+ha.getText()+"' group by nom";
//            Rp.SENDQUERY(query,"Employer");
//            }else   if(de.getText()!=null && ha.getText()!=null){
//            String query="select t.nom,t.prenom,e.document,e.matricule,t.adresse,t.telephone,e.mail,e.estatuses\n" +
//            ",i.nom as institution,i.direccion,i.telephone as tel,i.e_mail as email,e.document,e.type_document \n" +
//            "as tpdocument,e.naissance,etat_civil,t.sexe ,et.type_employer as tpemployer from employer e \n" +
//            "left join tercero t on t.code_tercero=e.code_tercero left join institution i on \n" +
//            "i.nom=t.institution left join employer_vs_type et on et.code_employer=e.code_employer"
//            + " where et.type_employer ='"+de.getText()+"' or et.type_employer ='"+ha.getText()+"' group by nom";
//            Rp.SENDQUERY(query,"Employer");
            //}
        
        }else  if(matiere.isSelected()){
            if(klas.getSelectedItem()==null){
                JOptionPane.showMessageDialog(this,"S'il vous plait veuillez séléctionner une classe dans le "
                        + "carré de gauche.");
            }else{
                Rp.SENDQUERY("Matiere",klas.getSelectedItem().toString(),section.getSelectedItem().toString()); 
            }
        }else if(paiement.isSelected()){
//         if(de.getText().isEmpty() && ha.getText().isEmpty()){
//            Rp.SENDQUERY("Paiement");
//            }
        }else if(professeur.isSelected()){
            if(tblpere.getSelectedRows().length>0){
               String mat=tblpere.getValueAt(tblpere.getSelectedRow(),0).toString();
              Rp.SENDQUERYS("Professeur",mat);  
            }else{
             JOptionPane.showMessageDialog(null,"S'il vous plait séléctionner un élève dans la table ci déssous");   
            }
            
        }else if(resultat.isSelected()){
            int aa=0;
              if(controle1.isSelected()){
              aa=1;
              }else if(controle2.isSelected()){
               aa=2;
             }else if(controle3.isSelected()){
              aa=3;
             }
            Rp.SENDQUERY("Resultat",klas.getSelectedItem().toString(),academique.getSelectedItem().toString(),aa,Variables.Empresa);
        }

    }//GEN-LAST:event_ImprimerActionPerformed

    private void annulerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_annulerActionPerformed
       StructureEleve();
    }//GEN-LAST:event_annulerActionPerformed

    private void sectionItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_sectionItemStateChanged
        
    }//GEN-LAST:event_sectionItemStateChanged

    private void sectionFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_sectionFocusLost
        if(section.getSelectedItem().toString().isEmpty()){
            klas.setSelectedItem(null);
        }else{
            Mn.FULLCOMBO(klas, "description","section",section.getSelectedItem().toString(), "classe");
        }
    }//GEN-LAST:event_sectionFocusLost

    private void borderauxMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_borderauxMouseClicked
       StructureBorderaux();
    }//GEN-LAST:event_borderauxMouseClicked

    private void inscriptionMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_inscriptionMouseClicked
       StructureInscris();
    }//GEN-LAST:event_inscriptionMouseClicked

    private void reservationMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_reservationMouseClicked
       StructureReservation();
    }//GEN-LAST:event_reservationMouseClicked

    /** 
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Cons_intelligente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Cons_intelligente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Cons_intelligente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Cons_intelligente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Cons_intelligente().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton Imprimer;
    private javax.swing.JComboBox academique;
    private javax.swing.JButton annuler;
    private javax.swing.JLabel aux;
    private javax.swing.JRadioButton borderaux;
    private javax.swing.JRadioButton bulletin;
    private javax.swing.JRadioButton classe;
    private javax.swing.JRadioButton collection;
    private javax.swing.JRadioButton controle1;
    private javax.swing.JRadioButton controle2;
    private javax.swing.JRadioButton controle3;
    private javax.swing.JRadioButton eleve;
    private javax.swing.JRadioButton employer;
    private javax.swing.JRadioButton inscription;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JMenu jMenu5;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JSeparator jSeparator4;
    private javax.swing.JSeparator jSeparator6;
    private javax.swing.JComboBox klas;
    private javax.swing.JRadioButton matiere;
    private javax.swing.JLabel nonreyisi;
    private javax.swing.JLabel nonreyisil;
    private javax.swing.JRadioButton paiement;
    private javax.swing.JPanel panelocultar;
    private javax.swing.JRadioButton professeur;
    private javax.swing.JLabel qut;
    private org.edisoncor.gui.textField.TextFieldRectBackground recherche;
    private org.edisoncor.gui.textField.TextFieldRectBackground recherche1;
    private javax.swing.JRadioButton reservation;
    private javax.swing.JRadioButton resultat;
    private javax.swing.JLabel reyisi;
    private javax.swing.JLabel reyisil;
    private javax.swing.JComboBox section;
    private javax.swing.JTable tblfils;
    private javax.swing.JTable tblpere;
    // End of variables declaration//GEN-END:variables
}
