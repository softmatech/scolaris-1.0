package Consults;
import Forms.Classe;
import Methods.Manipulation;
import Methods.Theme;
import Methods.Variables;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;

public class Cons_classe extends javax.swing.JInternalFrame {
Manipulation Mn= new Manipulation();
Theme th= new Theme();
String[][] data={};
String[] head={"Classes", " Section"};
DefaultTableModel md= new DefaultTableModel(data,head);
String[] heads={"Matières", " Sur",};
DefaultTableModel mds= new DefaultTableModel(data,heads);

    public Cons_classe() {
        initComponents();
        this.setTitle("Scolaris: "+Variables.Empresa+" : Consultation des Classes");
        tblclasse.setModel(md);
        TableColumnModel cm= tblclasse.getColumnModel();
        cm.getColumn(0).setPreferredWidth(220);
        cm.getColumn(1).setPreferredWidth(15);
        Remplir();
    }
private void Remplir(){
String query="select description,section from classe where institution='"+Variables.Empresa+"' order by code_classe asc";
Mn.FULLTABLE(query, tblclasse, md);
}

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        select = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        tblclasse = new javax.swing.JTable(){
            public boolean isCellEditable(int rowIndex, int colIndex){
                return false;
            }
        };
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();

        setClosable(true);
        setIconifiable(true);

        select.setBackground(new java.awt.Color(255, 255, 255));
        select.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        select.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/aceptar.png"))); // NOI18N
        select.setText("Séléctionner");
        select.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                selectActionPerformed(evt);
            }
        });

        tblclasse.setBorder(javax.swing.BorderFactory.createEtchedBorder(null, new java.awt.Color(0, 0, 0)));
        tblclasse.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tblclasse.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblclasseMouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(tblclasse);

        jMenu1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/fichier.jpg"))); // NOI18N
        jMenu1.setText("Action");
        jMenu1.setFont(new java.awt.Font("Verdana", 1, 14)); // NOI18N
        jMenuBar1.add(jMenu1);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 527, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(select, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(select, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 296, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void selectActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_selectActionPerformed
        String aa=Mn.SELECT_BUTTONS(tblclasse, Variables.Seleccionado2);
        String bb=Mn.SELECTS_BUTTON(tblclasse, Variables.Seleccionado3);
        int aaa=Mn.SEARCHCODE("code_classe","description","section", aa,bb, "classe");
        Classe.code.setText(String.valueOf(aaa));
        Classe.section.setSelectedItem(tblclasse.getValueAt(tblclasse.getSelectedRow(),1).toString());
         if(Classe.section.getSelectedItem()==null){
            Classe.classe.setValue(null);
        }else if(Classe.section.getSelectedItem().equals("Petit")){
            Classe.classe.setValue(null);
            Mn.CLASSE_PETIT_FORMAT(Classe.classe);
        }else if(Classe.section.getSelectedItem().equals("Primaire")){
            Classe.classe.setValue(null);
            Mn.CLASSE_PRIMAIRE_FORMAT(Classe.classe);
        }else if(Classe.section.getSelectedItem().equals("Secondaire")){
            Classe.classe.setValue(null);
            Mn.CLASSE_SECONDAIRE_FORMAT(Classe.classe);
        }
        Classe.classe.setText(tblclasse.getValueAt(tblclasse.getSelectedRow(),0).toString());
        Classe.acces="libre";
        Classe.add.doClick();
        this.dispose();
    }//GEN-LAST:event_selectActionPerformed

    private void tblclasseMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblclasseMouseClicked

    }//GEN-LAST:event_tblclasseMouseClicked

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JButton select;
    private javax.swing.JTable tblclasse;
    // End of variables declaration//GEN-END:variables
}
