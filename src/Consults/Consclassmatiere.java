package Consults;
import Forms.*;
import Methods.Manipulation;
import Methods.Theme;
import Methods.Variables;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;

public class Consclassmatiere extends javax.swing.JInternalFrame {
Manipulation Mn= new Manipulation();
Theme th= new Theme();
String[][] data={};
String[] head={"Classes", " Section"};
DefaultTableModel md= new DefaultTableModel(data,head);
String[] heads={"Matières", " Sur",};
DefaultTableModel mds= new DefaultTableModel(data,heads);

    public Consclassmatiere() {
        initComponents();
        this.setTitle("Scolaris: "+Variables.Empresa+" : Consultation des Classes");
        tblclasse.setModel(md);
        tblmatiere.setModel(mds);
        TableColumnModel cm= tblmatiere.getColumnModel();
        cm.getColumn(0).setPreferredWidth(220);
        cm.getColumn(1).setPreferredWidth(15);
        Remplir();
    }
private void Remplir(){
String query="select description,section from classe where institution='"+Variables.Empresa+"'";
Mn.FULLTABLE(query, tblclasse, md);
cantclasse.setText(String.valueOf(tblclasse.getRowCount()));
}

private void RemplirMatiere(){
 String aa=tblclasse.getValueAt(tblclasse.getSelectedRow(), 0).toString();
 String bb=tblclasse.getValueAt(tblclasse.getSelectedRow(), 1).toString();
 int aaa=Mn.SEARCHCODE("code_classe","description","section", aa,bb, "classe");
 String query="select m.description as matiere,e.valeur from classe_vs_matiere cm "
         + " join matiere m on cm.code_matiere=m.code_matiere "
         + " join equivalence e on m.code_equivalence=e.code_equivalence "
         + " join classe c on cm.code_classe=c.code_classe "
         + "where cm.code_classe="+aaa+" and m.institution='"+Variables.Empresa+"' group by matiere ";
 Mn.FULLTABLE(query, tblmatiere, mds);
 cantmat.setText(String.valueOf(tblmatiere.getRowCount()));
}
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        select = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tblclasse = new javax.swing.JTable(){
            public boolean isCellEditable(int rowIndex, int colIndex){
                return false;
            }
        };
        jLabel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblmatiere = new javax.swing.JTable(){
            public boolean isCellEditable(int rowIndex, int colIndex){
                return false;
            }
        };
        jLabel4 = new javax.swing.JLabel();
        cantmat = new javax.swing.JLabel();
        cantclasse = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();

        setClosable(true);
        setIconifiable(true);

        select.setBackground(new java.awt.Color(255, 255, 255));
        select.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        select.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/aceptar.png"))); // NOI18N
        select.setText("Séléctionner");
        select.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                selectActionPerformed(evt);
            }
        });

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel2.setText("_________Classes_________");

        tblclasse.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tblclasse.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblclasseMouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(tblclasse);

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("________________ Matières _______________");

        tblmatiere.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tblmatiere);

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel4.setText("Matières");

        cantmat.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        cantmat.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        cantmat.setText("0");

        cantclasse.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        cantclasse.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        cantclasse.setText("0");

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel5.setText("Classes");

        jMenu1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Pictures/fichier.jpg"))); // NOI18N
        jMenu1.setText("Action");
        jMenuBar1.add(jMenu1);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(select, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(cantmat, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jLabel4))
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 324, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(55, 55, 55)
                                .addComponent(jLabel2))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(cantclasse, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jLabel5)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 57, Short.MAX_VALUE)
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 324, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 293, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(340, Short.MAX_VALUE)))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(select, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jLabel2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 388, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(cantmat)
                    .addComponent(jLabel5)
                    .addComponent(cantclasse))
                .addContainerGap())
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                    .addContainerGap(84, Short.MAX_VALUE)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 389, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(36, 36, 36)))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void selectActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_selectActionPerformed
        Classematiere.section.setSelectedItem(tblclasse.getValueAt(tblclasse.getSelectedRow(),1).toString());
        Classematiere.classe.setSelectedItem(tblclasse.getValueAt(tblclasse.getSelectedRow(),0).toString());
        Classematiere.filtrer.doClick();
        this.dispose();
    }//GEN-LAST:event_selectActionPerformed

    private void tblclasseMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblclasseMouseClicked
        Mn.EMPTYTABLE(tblmatiere);
        RemplirMatiere();
    }//GEN-LAST:event_tblclasseMouseClicked

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel cantclasse;
    private javax.swing.JLabel cantmat;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JButton select;
    private javax.swing.JTable tblclasse;
    private javax.swing.JTable tblmatiere;
    // End of variables declaration//GEN-END:variables
}
